Dear {!Contact.FirstName},

I recently came across a report from Bain and Co., which contains valuable information I think you will find interesting.  

The report titled, "Tightly Manage Cash Flows and Liquidity: Strategies for Turbulent Times", Bain & Co. expert, Darrell Rigby, presents a concrete plan for more efficiently managing cash and capital resources.

Darrell Rigby states, "As the global financial crisis has choked off credit ... many senior executives have come to recognize the importance of understanding in detail how resources flow through the enterprise when the margins of error are severely limited.  Focusing attention on which levers can be pulled to the greatest effect is one of the key benefits of cash flow and liquidity analysis."

Click the link below to view the entire report.
http://www.qlikview.com/qvform.aspx?id=11784&langtype=1033

If you're looking for a more systematic, aggressive approach to managing cash flow, download the report with our compliments. The report:

-Reveals how much cash is necessary to preserve and protect your business under different conditions

-Identifies the critical choke points and opportunities to improve processes

-Highlights which levers to focus on and when to pull them, by pinpointing differences in efficiency between product lines, customer channels, and vendors

Click the link below to access your complimentary report.
http://www.qlikview.com/qvform.aspx?id=11784&langtype=1033

Read how real companies are putting these strategies into practice and the benefits they're achieving.  Find out:

-How a well-known big-box retailer is thriving against all odds, by focusing on 3 key defensive actions that prevent cash lockup 

-How a high-tech industrial group freed up several hundred million in cash, just by cracking down on receivables, increasing just-in-time deliverables, and tweaking a few other practices 

-How an auto rental company freed over $200 million in trapped cash—by killing an idea that sounded like sheer marketing genius, but wasn't

In addition to the report I'd like to share with you a case study from FE Global Electronics.  The case study reveals how this international distributor used aggressive, systematic cash management to decrease aging receivables by 20 percent and reduce inventory by 15 percent.

The report and case study are yours, compliments from QlikTech, makers of QlikView – business intelligence (BI) software that works the way the mind works.  With it, countless businesses around the world are uncovering hidden cash reserves tied up in working capital and taking advantage of key opportunities by easily achieving deeper insight into the information they already have.   

Click the link below to access your free report and case study.
http://www.qlikview.com/qvform.aspx?id=11784&langtype=1033

Once you have reviewed the materials, I would welcome the opportunity to discuss your specific company requirements.  I can be reached at the email and phone number listed below.  

Sincerely,

{!Lead.OwnerFullName}

{!Lead.OwnerEmail}
{!Lead.OwnerPhone}

QlikTech, Inc.

To download your copy of the report and case studies click below....
http://www.qlikview.com/qvform.aspx?id=11784&langtype=1033