({
    
    showCreateRecordModal : function(component, recordTypeId, entityApiName, zuoraOrSales) {
        
        var createRecordEvent = $A.get("e.force:createRecord");
        
        if(createRecordEvent)//checking if the event is supported	
        { 
            if(recordTypeId)//if recordTypeId is supplied, then set recordTypeId parameter
            {
                if(zuoraOrSales)
                {    
                    createRecordEvent.setParams({
                        "entityApiName": entityApiName,
                        "recordTypeId": recordTypeId,
                        "defaultFieldValues": {
                            "AccountId": component.get("v.recordId"),
                            "Type": "Existing Customer",
                            "StageName": "Goal Identified",
                            "Probability": 0
                        }});
                } 
                else //else create record under other recordTypes
                {
                    createRecordEvent.setParams({
                        "entityApiName": entityApiName,
                        "recordTypeId": recordTypeId,
                        "defaultFieldValues": {
                            "AccountId": component.get("v.recordId")
                        }});
                }
            }    
            else //else create record under master recordType
            {
                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "defaultFieldValues": {
                        "AccountId": component.get("v.recordId")
                    }});
            }
            
            createRecordEvent.fire();
        } 
        else
        {
            alert('This event is not supported');
        }
    },
    
    closeModal : function(){
        var closeEvent = $A.get("e.force:closeQuickAction");
        if(closeEvent)
        {
            closeEvent.fire();
        } else
        {    
            alert('force:closeQuickAction event is not supported in this Ligthning Context');   
        }
    }
})