({
	init : function(component, event, helper) {
		var oppId = component.get("v.recordId");
        var action = component.get("c.isPartnerContactNull");
        action.setParams({
            "oppId" : oppId 
        });
        action.setCallback(this, function(a){
            if(a.getReturnValue()){
                alert('Partner contact information is missing.'); 
            } else if(confirm("Are you sure you want to send Quote information?") == true){ 
                var action2 = component.get("c.sendEmail");
                action2.setParams({
                    "oppId" : oppId
                });
                action2.setCallback(this,function(b){
                   alert(b.getReturnValue());
                });
                $A.enqueueAction(action2);
            }
        });
        $A.enqueueAction(action);
	}
})