import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import newQuoteButtonValidation from '@salesforce/apex/Z_NewQuoteButtonValidation.newQuoteButtonValidation';
import communityUrl from '@salesforce/apex/Z_NewQuoteButtonValidation.communityUrl';
import internalUrl from '@salesforce/apex/Z_NewQuoteButtonValidation.internalUrl';

export default class NewZuoraQuoteButton extends LightningElement {    
    @api 
    recordId;

    handleClick(){
        newQuoteButtonValidation({oppId: this.recordId})
        .then((result) => {
            console.log(result);
            if(result.success && result.community){
                console.log('Community success');

                communityUrl().then(url => {
                    console.log(url);
                    window.location = url + encodeURIComponent('/apex/zqu__QuoteOption?&id=') + this.recordId;
                });
                
            }
            else if(result.success && result.community == false){
                console.log('Internal success');

                internalUrl().then(url => {
                    console.log(url);
                    window.location = url + '?&id=' + this.recordId;
                });
                
            }
            else if(result.success == false){
                result.errorMessages.forEach(errorMessage =>{
                    this.showToast(errorMessage.title, errorMessage.message);
                });
                
                const lwcCloseEvent = new CustomEvent('lwcClose', {});
                this.dispatchEvent(lwcCloseEvent);
            }
        });

    }

    showToast(title, message) {     
        const event = new ShowToastEvent({
            mode: 'sticky',
            title: title,
            message: message,
            variant: 'error',
        });
        this.dispatchEvent(event);
    }
	
    hasRendered = false;

    renderedCallback(){
        if(this.hasRendered){
            return
        }
 
        this.hasRendered = true;
        this.handleClick();
    }
}