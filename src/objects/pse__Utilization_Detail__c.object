<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Historical_Standard_Utilization_Billable__c</fullName>
        <description>The Historical Billable Hours divided by Standard Hours, for reporting based on standard 2080 hours/year</description>
        <externalId>false</externalId>
        <formula>IF
(
AND ((pse__Historical_Billable_Hours__c &gt; 0), (Standard_Hours__c = 0)),
1,
IF
(
AND ((pse__Historical_Billable_Hours__c = 0), (Standard_Hours__c = 0)),
0,
(pse__Historical_Billable_Hours__c / Standard_Hours__c)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Historical Billable Hours divided by Standard Hours, for reporting based on standard 2080 hours/year</inlineHelpText>
        <label>Historical Standard Utilization-Billable</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Historical_Standard_Utilization__c</fullName>
        <description>The Historical Billable Hours plus Historical Credited Hours divided by Standard Hours, to allow reporting based on standard 2080 hours/year</description>
        <externalId>false</externalId>
        <formula>IF
(
AND ((pse__Historical_Billable_Hours__c + pse__Historical_Credited_Hours__c &gt; 0), (Standard_Hours__c = 0)),
1,
IF
(
AND ((pse__Historical_Billable_Hours__c + pse__Historical_Credited_Hours__c = 0), (Standard_Hours__c = 0)),
0,
((pse__Historical_Billable_Hours__c + pse__Historical_Credited_Hours__c) / Standard_Hours__c)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Historical Billable Hours plus Historical Credited Hours divided by Standard Hours, to allow reporting based on standard 2080 hours/year.</inlineHelpText>
        <label>Historical Standard Utilization</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Scheduled_Standard_Utilization_Billable__c</fullName>
        <description>Scheduled Billable Hours divided by Standard Hours, for utilization reporting based on standard 2080 hours/year</description>
        <externalId>false</externalId>
        <formula>IF
(
AND ((pse__Scheduled_Billable_Hours__c &gt; 0), (Standard_Hours__c = 0)),
1,
IF
(
AND ((pse__Scheduled_Billable_Hours__c = 0), (Standard_Hours__c = 0)),
0,
(pse__Scheduled_Billable_Hours__c / Standard_Hours__c)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Scheduled Billable Hours divided by Standard Hours, for utilization reporting based on standard 2080 hours/year</inlineHelpText>
        <label>Scheduled Standard Utilization-Billable</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Scheduled_Standard_Utilization__c</fullName>
        <description>Scheduled Billable Hours plus Scheduled Credited Hours divided by Standard Hours, for utilization reporting based on standard 2080 hours/year</description>
        <externalId>false</externalId>
        <formula>IF
(
AND ((pse__Scheduled_Billable_Hours__c + pse__Scheduled_Credited_Hours__c &gt; 0), (Standard_Hours__c = 0)),
1,
IF
(
AND ((pse__Scheduled_Billable_Hours__c + pse__Scheduled_Credited_Hours__c = 0), (Standard_Hours__c = 0)),
0,
((pse__Scheduled_Billable_Hours__c + pse__Scheduled_Credited_Hours__c) / Standard_Hours__c)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Scheduled Billable Hours plus Scheduled Credited Hours divided by Standard Hours, for utilization reporting based on standard 2080 hours/year</inlineHelpText>
        <label>Scheduled Standard Utilization</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Standard_Hours__c</fullName>
        <description>The number of Standard Hours in the average time period of this type, for reporting on utilization based on a standard of 2080 hours/year. For Year time periods, 2080; Quarter 520; Month 160 or 200 depending on whether a 4 or 5 week period; Week 40.</description>
        <externalId>false</externalId>
        <formula>IF(
ISPICKVAL(pse__Time_Period__r.pse__Type__c, &quot;Year&quot;), 2080
, if( ISPICKVAL(pse__Time_Period__r.pse__Type__c, &quot;Quarter&quot;), 520
, if( ISPICKVAL(pse__Time_Period__r.pse__Type__c, &quot;Month&quot;), If(pse__Time_Period__r.pse__End_Date__c - pse__Time_Period__r.pse__Start_Date__c==34, 200, 160), 40)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The number of Standard Hours in the average time period of this type, for reporting on utilization based on a standard of 2080 hours/year. For Year time periods, 2080; Quarter 520; Month 160 or 200 depending on whether a 4 or 5 week period; Week 40.</inlineHelpText>
        <label>Standard Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Time_Period_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Time Period Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Week</fullName>
                    <default>false</default>
                    <label>Week</label>
                </value>
                <value>
                    <fullName>Month</fullName>
                    <default>false</default>
                    <label>Month</label>
                </value>
                <value>
                    <fullName>Quarter</fullName>
                    <default>false</default>
                    <label>Quarter</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
</CustomObject>
