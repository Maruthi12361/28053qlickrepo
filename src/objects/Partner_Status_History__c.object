<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used for tracking the changes applied to a partner status.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Change_Reason__c</fullName>
        <externalId>false</externalId>
        <label>Change Reason</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>Comment to be made as to why the partner status change was made.</description>
        <externalId>false</externalId>
        <inlineHelpText>Comment to be made as to why the partner status change is being made.</inlineHelpText>
        <label>Comment</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Effective_From__c</fullName>
        <description>Date the partner status was from.</description>
        <externalId>false</externalId>
        <label>Effective From</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Effective_To__c</fullName>
        <externalId>false</externalId>
        <label>Effective To</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Partner_Account__c</fullName>
        <externalId>false</externalId>
        <label>Partner Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Partner Status History</relationshipLabel>
        <relationshipName>Partner_Status_History</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Partner_Category__c</fullName>
        <externalId>false</externalId>
        <label>Partner Category</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Solution Provider</fullName>
                    <default>false</default>
                    <label>Solution Provider</label>
                </value>
                <value>
                    <fullName>Implementing &amp; Consulting</fullName>
                    <default>false</default>
                    <label>Implementing &amp; Consulting</label>
                </value>
                <value>
                    <fullName>Master Reseller &amp; Distribution</fullName>
                    <default>false</default>
                    <label>Master Reseller &amp; Distribution</label>
                </value>
                <value>
                    <fullName>Technology Partner</fullName>
                    <default>false</default>
                    <label>Technology Partner</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Partner_Margin__c</fullName>
        <externalId>false</externalId>
        <label>Partner Margin</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Partner_Resell_Training_Margin__c</fullName>
        <externalId>false</externalId>
        <label>Partner Resell Training Margin</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Partner_Status__c</fullName>
        <externalId>false</externalId>
        <label>Partner Status</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Partner_Training_Discount__c</fullName>
        <description>The discounted amount a partner receives when purchasing training for themselves.</description>
        <externalId>false</externalId>
        <inlineHelpText>The discounted amount a partner receives when purchasing training for themselves.</inlineHelpText>
        <label>Partner Training Discount</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Partner_Type__c</fullName>
        <description>CR# 63310 - Label Change</description>
        <externalId>false</externalId>
        <label>Partner Level</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>SP - Registered Partner</fullName>
                    <default>false</default>
                    <label>SP - Registered Partner</label>
                </value>
                <value>
                    <fullName>SP - Elite</fullName>
                    <default>false</default>
                    <label>SP - Elite</label>
                </value>
                <value>
                    <fullName>SP - Solution Provider</fullName>
                    <default>false</default>
                    <label>SP - Solution Provider</label>
                </value>
                <value>
                    <fullName>SP - Strategic</fullName>
                    <default>false</default>
                    <label>SP - Strategic</label>
                </value>
                <value>
                    <fullName>SP - Sub-Reseller</fullName>
                    <default>false</default>
                    <label>SP - Sub-Reseller</label>
                </value>
                <value>
                    <fullName>I&amp;C - Registered Partner</fullName>
                    <default>false</default>
                    <label>I&amp;C - Registered Partner</label>
                </value>
                <value>
                    <fullName>I&amp;C - Implementation Partner</fullName>
                    <default>false</default>
                    <label>I&amp;C - Implementation Partner</label>
                </value>
                <value>
                    <fullName>I&amp;C - SI/BC</fullName>
                    <default>false</default>
                    <label>I&amp;C - SI/BC</label>
                </value>
                <value>
                    <fullName>I&amp;C - Strategic</fullName>
                    <default>false</default>
                    <label>I&amp;C - Strategic</label>
                </value>
                <value>
                    <fullName>TP - Registered Partner</fullName>
                    <default>false</default>
                    <label>TP - Registered Partner</label>
                </value>
                <value>
                    <fullName>TP - Technology Partner</fullName>
                    <default>false</default>
                    <label>TP - Technology Partner</label>
                </value>
                <value>
                    <fullName>TP - Elite</fullName>
                    <default>false</default>
                    <label>TP - Elite</label>
                </value>
                <value>
                    <fullName>TP - Strategic</fullName>
                    <default>false</default>
                    <label>TP - Strategic</label>
                </value>
                <value>
                    <fullName>MR - Country Reseller</fullName>
                    <default>false</default>
                    <label>MR - Country Reseller</label>
                </value>
                <value>
                    <fullName>MR - Distributor</fullName>
                    <default>false</default>
                    <label>MR - Distributor</label>
                </value>
                <value>
                    <fullName>MR - Master Reseller</fullName>
                    <default>false</default>
                    <label>MR - Master Reseller</label>
                </value>
                <value>
                    <fullName>MR - Elite Reseller</fullName>
                    <default>false</default>
                    <label>MR - Elite Reseller</label>
                </value>
                <value>
                    <fullName>MR - Sub-Reseller</fullName>
                    <default>false</default>
                    <label>MR - Sub-Reseller</label>
                </value>
                <value>
                    <fullName>SP - Exception</fullName>
                    <default>false</default>
                    <label>SP - Exception</label>
                </value>
                <value>
                    <fullName>Reseller (old)</fullName>
                    <default>false</default>
                    <label>Reseller (old)</label>
                </value>
                <value>
                    <fullName>Value Added Distributor</fullName>
                    <default>false</default>
                    <label>Value Added Distributor</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Qonnect_Fee__c</fullName>
        <description>CR# 63310 - Label Change</description>
        <externalId>false</externalId>
        <label>Program Fee</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Partner Status History</label>
    <nameField>
        <displayFormat>PSH-{000000}</displayFormat>
        <label>Partner Status History Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Partner Status History</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
