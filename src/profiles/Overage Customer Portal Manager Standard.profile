<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <categoryGroupVisibilities>
        <dataCategoryGroup>Attunity_Data_Categories</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <categoryGroupVisibilities>
        <dataCategoryGroup>Product</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <categoryGroupVisibilities>
        <dataCategoryGroup>Version</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <custom>false</custom>
    <fieldPermissions>
        <editable>true</editable>
        <field>Health_Check_Activity__c.Opportunity__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>false</editable>
        <field>Product_Trial__c.End_Date_Formula__c</field>
        <readable>false</readable>
    </fieldPermissions>
    <layoutAssignments>
        <layout>Case-ScheduleOnce Support Layout</layout>
        <recordType>Case.ScheduleOnce_Support_Record_Type</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseClose-ScheduleOnce Close Layout</layout>
        <recordType>Case.ScheduleOnce_Support_Record_Type</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Health_Check_Activity__c-Health Check Activity Layout</layout>
    </layoutAssignments>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Case.ScheduleOnce_Support_Record_Type</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <userLicense>Overage Customer Portal Manager Standard</userLicense>
</Profile>