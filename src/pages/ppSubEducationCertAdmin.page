<!--
Change log: July 08 2013  PGE  Modified page content  CR#8436 https://eu1.salesforce.com/a0CD000000Z9b4X 
Change log: May 9 2014  PGE  Modified page layout with new version including related resource, content, banner update  CR# 12390 /a0CD000000i1rZs
20150318 - JNR CR 29276 - added redirect notice                   
-->

<apex:page title="QlikView System Administrator Certification">

<h1>QlikView 11 System Administrator Certification</h1>

<!-- SUB NAV  -->
<div style="width:225px; padding:10px; border:1px solid #efefef; float:right; margin-left:25px;">
<img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/bg_bullets.gif" />&nbsp;<b>Certification Exams</b><br />
<img style="margin-left:15px;" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/bg_bullets.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertDesign">Designer</apex:outputlink><br />
<img style="margin-left:15px;" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/bg_bullets.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertDev">Developer</apex:outputlink><br />
<img style="margin-left:15px;" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/bg_bullets.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertAdmin">System Administrator</apex:outputlink><br />
<img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/bg_bullets_dark_gray.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertGain">Gain Competitive Advantage</apex:outputlink><br />
<img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/bg_bullets_dark_gray.gif" />&nbsp;<a target="_blank" class="external" href="http://www.pearsonvue.com/qlikview/">Register for an Exam</a><br />
<img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/bg_bullets_dark_gray.gif" />&nbsp;<a target="_blank" href="http://www.qlik.com/us/~/media/Files/training/certification/QV11_Certification_Program_FAQs_V11.ashx">Certification FAQs</a>
</div>

<!-- MAIN CONTENT -->

<div style="background-color:#def1fc; border:1px solid #98c3dc; padding:10px 10px 0px 10px; width:600px;"><p style="color: #047dc5; font-weight: bold;">Updated Qlik Certification Information</p> 
<p style="color:#333333;">
To ensure the most up to date and accurate information for Partners on the full range of Qlik Certifications available, all information is now available through the training and certification pages of the Qlik website. Please go <a style="color:#007fc0;" target="_blank" href="http://www.qlik.com/us/services/training/certification/system-admin">here</a> for information on Qlikview System Admnistrator Certification.</p>
<p style="color:#333333;">
We will be retiring this page soon, so please update your bookmarks.
</p></div>

<p>The QlikView System Administrator Certification Exam tests your knowledge of managing a QlikView Server and Publisher environment.</p>

<p>This exam tests your ability to install and configure QlikView Server and Publisher, perform administrative tasks, and troubleshoot problems and errors. The QlikView System Administrator Certification Exam is a scenario-based, 
multiple choice knowledge test delivered at <a target="_blank" class="external" href="http://www.pearsonvue.com/qlikview/">Pearson VUE</a> Test Centers globally and is currently available in English only.</p>
    
    <h3>Exam Prerequisites</h3><br />
    
    <ul>
        <li>Experience working with QlikView Server and Publisher</li>
        <li>Knowledge and experience with Microsoft Windows Server based environments, Windows Firewall, Windows<br />IIS and IIS Management Console</li>
        <li>Experience with tools to troubleshoot connectivity, interoperability, configuration and performance, such as<br />nslookup, telnet, ping, netstat, etc.</li>
        <li>Basic SQL and ODBC/OLEDB connections</li>
        <li>Windows Services Applet</li>
        <li>Knowledge of basic NTFS rights and Windows Network fileshare setup</li>
    </ul>
    
    <h3>Recommended Preparation Resources</h3>
    
    <ul>
      <li>eLearning: <a target="_blank" class="external" href="http://qvtraining.qlikview.com/DOWNLOADS_FREE-ELEARNING/How_Does_QlikView_Work/index.html">How Does QlikView Work?</a>, <a target="_blank" href="http://qvtraining.qlikview.com/DOWNLOADS_FREE-ELEARNING/System_management_overview/index.html">System Management Overview</a></li>
    <li>Classroom: <a class="external" target="_blank" href="http://www.qlikview.com/us/services/training/find-your-training?region=&city=&language=&cl=1&ol=1&q=server%20publisher%20version%2011">Server Publisher Version 11</a></li>
    <li><a target="_blank" class="external" href="http://community.qlikview.com/index.jspa ">QlikCommunity</a></li>
      <li>QlikView Online Help (Select “Help – Contents” in QlikView Desktop)</li>
    </ul>
    
    <h3>Exam Domain Areas</h3>
    
    <p style="margin-bottom:5px;"><strong>Install and Configure the Environment (22% of the exam)</strong></p>
    
    <ul>
        <li>Explain the purpose of different QlikView services</li>
        <li>Determine the appropriate service installation to meet customer requirements</li>
        <li>Determine how to share system resources between QlikView services</li>
        <li>Determine the implications and outcomes of a specific configuration</li>
        </ul>
        
        
    <p style="margin-bottom:5px;"><strong>Administer the Environment (35% of the exam)</strong></p>
    
    <ul>
        <li>Differentiate between functionality of licensed and unlicensed Publisher</li>
        <li>Determine the appropriate task type to meet customer requirements</li>
        <li>Determine an appropriate method to monitor tasks</li>
        <li>Given a scenario, determine the appropriate options for configuring a task</li>
        <li>Given a scenario, determine the appropriate CAL type to use and describe methods<br />to apply CAL types</li>
        <li>Describe how to proactively manage QlikView platform infrastructure</li>
        <li>Determine an approach to proactively manage Windows platform<br />infrastructure (e.g., IIS, event logs, folder permissions)</li>
        </ul>
        
    <p style="margin-bottom:5px;"><strong>Troubleshoot the Environment (43% of the exam)</strong></p>
    
    <ul>
        <li>Identify an error based on log file information</li>
        <li>Determine the appropriate steps to identify issues (e.g., user reported, dialog box)</li>
        <li>Determine the cause of common performance issues</li>
        <li>Determine the appropriate system resource allocation to meet requirements</li>
        <li>Determine an appropriate strategy to resolve a performance issue</li>
        <li>Determine the cause of an environmental issue</li>
        <li>Describe how to handle client-server communications problems</li>
</ul>

</apex:page>