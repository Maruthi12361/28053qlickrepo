<apex:page standardController="pse__Billing_Event__c" 
           extensions="PseBillingEventsCustomExtension"
           recordSetVar="events"
           tabStyle="Billing_Event_Export__tab">
    <apex:form >
        <apex:pageBlock title="Export Billing Events">
            <apex:pageMessages />
            <apex:pageBlockButtons >
                <apex:commandButton action="{!preview}" value="Preview" 
                        rerender="previewPanel" status="status" />
                <apex:commandButton action="{!export}" value="Export" />
                <apex:commandButton action="{!cancel}" value="Cancel" />
            </apex:pageBlockButtons>

            <apex:pageBlockSection title="Billing Events to Export" columns="1">
                <apex:outputText >
                    <p>Below is a list of Billing Events that have been released but not yet invoiced or exported.
                       Please select the checkbox for each released Billing Event you would like to export.</p>
                    <ul>
                        <li><strong>Preview</strong> will display a preview of the data to be exported to CSV.</li>  
                        <li><strong>Export</strong> will download the data to CSV, named after the Export Label.</li>
                    </ul>
                </apex:outputText>

                <apex:pageBlockTable value="{!eventList}" var="e" styleClass="customPageBlockTable" width="1020px">
                    <apex:column headerValue="Select">
                        <apex:facet name="header">
                            <apex:inputCheckbox onclick="toggleAll(this)" title="Toggle All Rows" />
                        </apex:facet>
                        <apex:inputCheckbox value="{!e.selected}" id="exportCheckbox" 
                                title="Select {!e.event.Name}" />
                    </apex:column>
                    <apex:column headerValue="Event Name">
                        <a href="/{!e.event.Id}">{!e.event.Name}</a>
                    </apex:column>
                    <apex:column value="{!e.event.pse__Is_Released__c}" /> 
                    <apex:column value="{!e.event.Exported__c}" />
                    <apex:column value="{!e.event.pse__Invoiced__c}" />
                    <apex:column value="{!e.event.pse__Billing_Event_Batch__r.pse__Account__c}" />
                    <apex:column value="{!e.event.pse__Billing_Event_Batch__r.pse__Account__r.OwnerId}" />
                    <apex:column value="{!e.event.pse__Project__c}" />
                    <apex:column value="{!e.event.pse__Date__c}" />
                    <apex:column value="{!e.event.pse__Summary_Amount__c}" />
                </apex:pageBlockTable>
            </apex:pageBlockSection>

            <apex:pageBlockSection title="Export Information" columns="1">
                <apex:inputField value="{!exportEvent.Export_Label__c}" />
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>

    <apex:outputPanel id="previewPanel">
        <apex:actionstatus id="status">
            <apex:facet name="start">
                <apex:outputPanel >
                    <apex:image value="/img/loading.gif" style="margin-right: 25px;" />
                    <apex:outputText value="Gathering Export Details..." />
                </apex:outputPanel>
            </apex:facet>
            <apex:facet name="stop">
                <apex:pageBlock rendered="{!selectedEvents.size > 0}" title="Export Preview">
                    <apex:pageBlockTable value="{!selectedEventItems}" var="i">
                        <apex:column ></apex:column>
                        <apex:column value="{!i.pse__Billing_Event__r.pse__Billing_Event_Batch__r.pse__Account__r.Id}" />
                        <apex:column headerValue="Account Name" value="{!i.pse__Billing_Event__r.pse__Billing_Event_Batch__r.pse__Account__c}" />
                        <apex:column value="{!i.pse__Billing_Event__r.pse__Billing_Event_Batch__r.pse__Account__r.OwnerId}" />
                        <apex:column headerValue="Project Name" value="{!i.pse__Billing_Event__r.pse__Project__c}" />
                        <apex:column headerValue="Activity Code">{!CASE(i.pse__Billing_Event__r.pse__Project__r.pse__Project_Type__c, 'Services', 'CNS', 'Consulting', 'CNS', 'Training', 'TRN', 'SiB', 'SIB', 'SIB', 'SIB', 'TAM', 'TAM', 'Other')}</apex:column>
                        <apex:column value="{!i.pse__Billing_Event__r.pse__Project__r.Invoice_Notes__c}" />
                        <apex:column value="{!i.pse__Billing_Event__r.pse__Project__r.PO_Number__c}" />
                        <apex:column headerValue="Timecard">
                            <apex:outputText rendered="{!i.pse__Timecard_Splits__r.size > 0}" value="True" />
                        </apex:column>
                        <apex:column headerValue="Milestone">
                            <apex:outputText rendered="{!i.pse__Milestones__r.size > 0}" value="True" />
                        </apex:column>
                        <apex:column headerValue="MiscAdjustment">
                            <apex:outputText rendered="{!i.pse__Miscellaneous_Adjustments__r.size > 0}" value="True" />
                        </apex:column>
                        <apex:column headerValue="Budget">
                            <apex:outputText rendered="{!i.pse__Budgets__r.size > 0}" value="True" />
                        </apex:column>
                        <apex:column headerValue="Expense">
                            <apex:outputText rendered="{!i.pse__Expenses__r.size > 0}" value="True" />
                        </apex:column>
                        <apex:column headerValue="Resource Name">
                            <apex:repeat value="{!i.pse__Timecard_Splits__r}" var="timecard">
                                <apex:outputField value="{!timecard.pse__Resource__c}" />
                            </apex:repeat>
                        </apex:column>
                        <apex:column headerValue="Start Date" value="{!i.pse__Date__c}" />
                        <apex:column headerValue="Hours" value="{!i.pse__Quantity__c}" />
                        <apex:column headerValue="Hourly Rate" value="{!i.pse__Unit_Price__c}" />
                        <apex:column footerValue="Not Included" value="{!i.pse__Billing_Event__r.Name}" styleClass="omitted" headerClass="omitted" footerClass="omitted" />
                        <apex:column value="{!i.pse__Category__c}" styleClass="omitted" headerClass="omitted" footerClass="omitted" />
                        <apex:column headerValue="Total Amount" value="{!i.pse__Amount__c}" styleClass="omitted" headerClass="omitted" footerClass="omitted" />
                        <apex:column headerValue="End Date" styleClass="omitted" headerClass="omitted" footerClass="omitted">
                            <apex:repeat value="{!i.pse__Timecard_Splits__r}" var="timecard">
                                <apex:outputField value="{!timecard.pse__End_Date__c}" />
                            </apex:repeat>
                        </apex:column>
                        <apex:column value="{!i.pse__Description__c}" styleClass="omitted" headerClass="omitted" footerClass="omitted" />
                    </apex:pageBlockTable>
                </apex:pageBlock> 
            </apex:facet>
        </apex:actionstatus>
    </apex:outputPanel>


    <script>
        function toggleAll(cb) {
            var inputElem = document.getElementsByTagName("input");
            for (var i = 0; i < inputElem.length; i++) {
                if (inputElem[i].id.indexOf("exportCheckbox") != -1)
                    inputElem[i].checked = cb.checked;
            }
        }
    </script>
    <style>
        .omitted { background-color: #EEE; color: #666 !important; font-style: italic}
        .customPageBlockTable { max-width: 1020px; width: 1020px !important;}
    </style>
</apex:page>