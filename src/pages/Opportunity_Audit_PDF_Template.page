<apex:page standardController="Opportunity" extensions="OpportunityAuditPDFTemplateCtrl" showHeader="false" standardStylesheets="false" applyBodyTag="false" applyhtmltag="false" renderAs="pdf">
<head>
<style>
    @page {
        size:landscape;

        @top-right {
        content: "Page " counter(page);
        }
    }

    body { font-family: 'Arial Unicode MS'; }
    .heading {font: bold 16px;}
    .fieldName {font: bold 14px;}
    .fieldValue {font: 14px;}
    .content {font: 14px;}

    .Panel4Colcol1 {width:25%;}
    .Panel4Colcol2 {width:25%;}
    .Panel4Colcol3 {width:25%;}
    .Panel4Colcol4 {width:25%;}

    .Panel2Colcol1 {width:50%;}
    .Panel2Colcol2 {width:50%;}
</style>
</head> 
    <apex:pageMessages />
    <body>
        <center>
        <apex:outputText styleClass="heading" value="Order Sheet"/>
        </center>
        <br />
        <apex:outputText styleClass="fieldName" value="Opportunity link:  " /><apex:outputText styleClass="fieldValue" value="{!$CurrentPage.parameters.detail_url}" />
        <br /><br />
        <apex:panelGrid columns="4" columnClasses="Panel4Colcol1, Panel4Colcol2, Panel4Colcol3, Panel4Colcol4" width="100%" border="1" cellpadding="5" cellspacing="0">
            <apex:outputlabel styleClass="fieldName" value="Date"/>
            <apex:outputText styleClass="fieldValue" value="{!NOW()}"/>
            <apex:outputlabel styleClass="fieldName" value="Opportunity"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Name}"/>
            <apex:outputlabel styleClass="fieldName" value="Account"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Account.Name}"/>
            <apex:outputlabel styleClass="fieldName" value="Currency"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.CurrencyIsoCode}"/>           
            <apex:outputlabel styleClass="fieldName" value="Address"/>
            <apex:outputText styleClass="fieldValue" escape="false" value="{!Opportunity.Account.BillingStreet}<br/>{!Opportunity.Account.BillingCity}<br/>{!Opportunity.Account.BillingState}<br/>{!Opportunity.Account.BillingPostalCode}<br/>{!Opportunity.Account.BillingCountry}"/>           
            <apex:outputlabel styleClass="fieldName" value="License Amount"/>
            <apex:outputText styleClass="fieldValue" value="{!ROUND(Opportunity.License_Amount_USD__c,2)}"/>           
            
            <apex:outputlabel styleClass="fieldName" value="Navision Customer Number"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Navision_Cust_Num_from_Acc__c}"/>
            <apex:outputlabel styleClass="fieldName" value="Total Opportunity Amount"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Total_Opportunity_Amount__c}"/>                         

            <apex:outputlabel styleClass="fieldName" value="Payment terms (Days)"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Payment_Terms_Days__c}"/>
            <apex:outputlabel styleClass="fieldName" value="Payment Method"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Payment_Method__c}"/> 

            <apex:outputlabel styleClass="fieldName" value="Quote Reference number"/>
            <apex:outputText styleClass="fieldValue" value="{!quote_ref}"/>
            <apex:outputlabel styleClass="fieldName" value=""/>
            <apex:outputText styleClass="fieldValue" value=""/>
        </apex:panelGrid>
        <br />
        <apex:panelGrid columns="4" columnClasses="Panel4Colcol1, Panel4Colcol2, Panel4Colcol3, Panel4Colcol4" width="100%" border="1" cellpadding="5" cellspacing="0">
            <apex:outputlabel styleClass="fieldName" value="Opportunity Record Type"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.RecordType.Name}"/>
            <apex:outputlabel styleClass="fieldName" value="Through Partner"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Sell_Through_Partner__r.Name}"/>
            <apex:outputlabel styleClass="fieldName" value="Opportunity Owner"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Owner.Name}"/>
            <apex:outputlabel styleClass="fieldName" value="Second Partner"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Second_Partner__r.Name}"/>
        </apex:panelGrid>
        <br />       
        <apex:dataTable border="1" cellpadding="5" cellspacing="0" rendered="{!showItems1}" width="100%" title="Products" value="{!lItems}" var="item" rowClasses="odd,even" headerClass="content" styleClass="content">
                <apex:column headerValue="Product" value="{!item.Name__c}" />                
                <apex:column headerValue="License No" value="{!item.Account_License_RO__c}" />                
                <apex:column headerValue="Qty" value="{!item.Quantity}" />
                <apex:column headerValue="List" value="{!item.ListPrice}" />                
                <apex:column headerValue="QlikBuy List" value="{!item.CPQ_List_Price__c}" />                
                <apex:column headerValue="Discount" value="{!item.Discount__c}" />                
                <apex:column headerValue="Partner Margin" value="{!item.Partner_Margin_RO__c}" />                
                <apex:column headerValue="Total Price" value="{!item.TotalPrice}" />
        </apex:dataTable>   
        <br />       
        <apex:panelGrid columns="2" columnClasses="Panel2Colcol1, Panel2Colcol2" width="100%" border="1" cellpadding="5" cellspacing="0">
            <apex:outputlabel styleClass="fieldName" value="Partner Bonus Calculation"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Partner_Bonus_Calculation__c}"/>
        </apex:panelGrid>
        <br />
        <apex:panelGrid columns="4" columnClasses="Panel4Colcol1, Panel4Colcol2, Panel4Colcol3, Panel4Colcol4" width="100%" border="1" cellpadding="5" cellspacing="0">
            <apex:outputlabel styleClass="fieldName" value="Maintenance Calculation"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Maintenance_Calculation__c}"/>
            <apex:outputlabel styleClass="fieldName" value="Maintenance Percentage"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Maintenance_Percentage_if_non_Std__c}"/>
            
            <apex:outputlabel styleClass="fieldName" value="Maintenance type"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Maintenance_Type__c}"/>
            <apex:outputlabel styleClass="fieldName" value="Maintenance Free Period"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Maintenance_Free_Period__c}"/>
            
            <apex:outputlabel styleClass="fieldName" value="Maintenance Term"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Maintenance_Term_months__c}"/>
            <apex:outputlabel styleClass="fieldName" value="Maintenance Free Months"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Maintenance_Free_Months__c}"/>
            
            <apex:outputlabel styleClass="fieldName" value="Default Maintenance Percentage"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Default_Maintance_Percentage__c}"/>
            <apex:outputlabel styleClass="fieldName" value="Maintenance Amount"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Total_Maintenance_Amount__c}"/>            
        </apex:panelGrid>
        <br />               
        <apex:panelGrid columns="2" columnClasses="Panel2Colcol1, Panel2Colcol2" width="100%" border="1" cellpadding="5" cellspacing="0">
            <apex:outputlabel styleClass="fieldName" value="Accepted by"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Accepted_by__c}"/>

            <apex:outputlabel styleClass="fieldName" value="PO Number"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Customer_PO_Number__c}"/>
            
            <apex:outputlabel styleClass="fieldName" value="Closed Won date"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.CloseDate}"/>            

            <apex:outputlabel styleClass="fieldName" value="Financed Approved"/>
            <apex:outputText styleClass="fieldValue" value="{!Opportunity.Finance_Approved__c}"/>
        </apex:panelGrid>
        <br />
        <apex:outputlabel styleClass="fieldName" value="Last Modified by: "/>
        <apex:outputText styleClass="fieldValue" value="{!Opportunity.LastModifiedBy.Name}"/>
    </body>                    
</apex:page>