<!-----------------------------
*Author: SAN Initial development
* Change log
* 2014-10-31 customer portal project
* 2014-11-13 Changes to layout - WGO
* 2014-11-17 Changes to layout - WGO
* 2019-05-05 AIN Updated for Support Portal Redesign
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
* 2020-05-08 extcqb IT-2799 Hide Support Level/Support SLA from the Support Portal
 ---------------------------- -->
<apex:page cache="false"
  standardStylesheets="false"
  showHeader="false"
  sidebar="false"  
  language="{!$CurrentPage.parameters.l}"
  applyBodyTag="false"
  applyHtmlTag="false" controller="QS_LicenseController">
    <apex:composition template="QS_PageTemplate">  
        <apex:stylesheet value="{!URLFOR($Resource.JQuerydataTableCSS)}" />
        <apex:includeScript value="{!URLFOR($Resource.JQueryJavaScript)}"/>
        <apex:includeScript value="{!URLFOR($Resource.JQuerydataTableJS)}"/>
        <script type="text/javascript">
            $(document).ready(SetupShlTable); 
        
            function SetupShlTable() {
              $("#shlTable").DataTable({"lengthChange": false, bFilter: false})
            }
        </script>
        <apex:define name="body">
            <section>
                <div class="wrapper">
                    <apex:include pageName="QS_LicenseListPageJS"/>
                    <div class="qs-page-licensedetails qs-page">
                        <div>
                            <apex:form >
                                <div class="qs-module qs-clearfix">
                                    <h1>License Detail</h1>
                                    <apex:outputText value="{!myLicense.Name}  -  {!myLicense.Status}" styleClass="qs-intro"/>
                                    <apex:outputText value="{!myLicense.License_Account_NetSuite_Id__c}" id="hidAccountNetSuiteId" style="display:none;"/>
                                    <apex:outputText value="{!myLicense.Name}" id="hidLicenseName" style="display:none;"/>
                                    <apex:outputText value="{!myLicense.License_Netsuite_Internal_Id__c}" id="hidLicenseNetSuiteId" style="display:none;"/>  
                                    <apex:outputText value="{!userIpAddress}" id="hidIpAddress" style="display:none;"/>
                                    <apex:outputText value="{!myLicense.Support_Provided_By_Id__c}" id="hidLicSupportProvBy" style="display:none;"/>
                                    <apex:pageMessages escape="false" />
                                    <apex:outputPanel layout="block">
                                        <apex:outputLink value="{!$Page.QS_LicenseListPage}" styleClass="btn btn-secondary">Back</apex:outputLink>
                                        <apex:commandButton value="Save" action="{!SaveLicense}" id="saveButton2" styleClass="btn" />
                                        <apex:outputText value="Changes saved" rendered="{!savedChanges}"/>
                                    </apex:outputPanel>   
                                </div>      
                                <ul class="qs-chart">
                                    <h3>General information</h3>
                                    <li>
                                        <span class="category">Description</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Description__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">Account name</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Account.Name}"/></span>
                                    </li>
                                    <li>
                                        <span class="category">License Reference</span>
                                        <span class="data"><apex:inputText value="{!myLicense.License_Reference__c}"/></span>
                                    </li>
                                    <li>
                                        <span class="category">Support Provided By</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Support_Provided_By_Text__c}"/></span>
                                    </li>
                                    <apex:variable rendered="{!myLicense.Legacy_License_Key__c != null}" var="showLegacyLicenseKey" value="1">
                                        <li>
                                            <span class="category">Legacy License Key</span>
                                            <span class="data"><apex:outputText value="{!myLicense.Legacy_License_Key__c}"/></span>
                                        </li>
                                    </apex:variable>
                                    <apex:variable rendered="{!DSEName != null && DSEAccess}" var="showDSE" value="1">
                                        <li>
                                            <span class="category">Designated Support Engineer</span>
                                            <span class="data"><apex:outputText value="{!DSEName}"/></span>
                                        </li>
                                    </apex:variable>
                                    <li>
                                        <span class="category">Business Hours</span>
                                        <span class="data"><apex:outputText value="{!myLicense.BusinessHours.Name}"/></span>
                                    </li>
                                    <li>
                                        <span class="category">Support From</span>
                                        <span class="data">
                                            <apex:outputText value="{0,date, MMMM d,  yyyy}">
                                                <apex:param value="{!myLicense.StartDate}"/>
                                            </apex:outputText>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="category">Support To</span>
                                        <span class="data">
                                            <apex:outputText value="{0,date, MMMM d,  yyyy}">
                                                <apex:param value="{!myLicense.EndDate}"/>
                                            </apex:outputText>
                                        </span> 
                                    </li>
                                    <apex:variable rendered="{!myLicense.Subscription_Start_Date__c!=null}" var="showSubFrom" value="1">
                                        <li>
                                            <span class="category">Subscription From</span>
                                            <span class="data"><apex:outputText value="{!myLicense.Subscription_Start_Date__c}"/></span> 
                                        </li>
                                    </apex:variable>
                                    <apex:variable rendered="{!myLicense.Subscription_End_Date__c!=null}" var="showSubTo" value="1">
                                        <li>
                                            <span class="category">Subscription To</span>
                                            <span class="data"><apex:outputText value="{!myLicense.Subscription_End_Date__c}"/></span> 
                                        </li>
                                    </apex:variable>
                                    <li>
                                        <span class="category">Environment</span>
                                        <span class="data"><a href="{!$Page.QS_EnvironmentDetails}?Id={!myEnv.id}">{!myEnv.Name}</a></span> 
                                    </li>
                                    <h3>CALs and Tokens</h3>
                                    <li>
                                        <span class="category">Named User CAL</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Named_User_CAL__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">No. of Servers in Cluster</span>
                                        <span class="data">
                                            <apex:outputText value="{0, number, 0}">
                                                <apex:param value="{!myLicense.No_of_Servers_in_Cluster__c}" />
                                            </apex:outputText>
                                        </span> 
                                    </li>
                                    <li>
                                        <span class="category">Session CAL</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Session_CAL__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">No. of Documents</span>
                                        <span class="data">
                                            <apex:outputText value="{0, number, 0}">
                                                <apex:param value="{!myLicense.No_of_Documents__c}" />
                                            </apex:outputText>
                                        </span> 
                                    </li>
                                    <li>
                                        <span class="category">Document CAL</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Document_CAL__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">Dynamic Update (RT5)</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Dynamic_update_RTS__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">Usage CAL</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Usage_CAL__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">Web Parts</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Web_Parts__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">Tokens</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Tokens__c}"/></span> 
                                    </li>
                                    <li>
                                        <span class="category">Workbench</span>
                                        <span class="data"><apex:outputText value="{!myLicense.Workbench__c}"/></span> 
                                    </li>
                                </ul>
                            </apex:form>
                            <div class="qs-module">
                                <div class="qs-section-quicklinks">
                                    <apex:form >
                                        <h3 class="qs-case-quickactions">Quick Actions</h3>     
                                        <apex:outputPanel >
                                            <ul class="qs-quicklinks">
                                                <li>
                                                    <apex:commandLink id="btnGenerateLEF" onclick="dwldLEF(true);"  oncomplete="this.form.target='_blank'" value="Download LEF" styleClass="qs-btn-download"/>
                                                </li>
                                                <li>
                                                    <apex:commandLink id="btnControlNumberRequest" onclick="initDialog(true);return false;"  styleClass="qs-btn-request" value="Request Control Number"/>
                                                </li>
                                            </ul>
                                        </apex:outputPanel>  
                                    </apex:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </apex:define>
    </apex:composition>
</apex:page>