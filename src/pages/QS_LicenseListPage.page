<!--/************************************************************************
 * @author  SAN
 * 2014-10-30 customer portal project
 *
 * page for License List View QS_LicenseListPage
 *  Change log:
  2014-11-13 Changes to layout - WGO
  2014-11-17 Changes to layout - WGO
  2017-02-01 UIN Changes for request control number project CR# 90369 Moved js to a separate file
  2017-04-15 UIN Changes for request control number project CHG0030333
  2019-05-05 AIN Updated for Support Portal Redesign
  2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
  2019-11-21 AIN IT-2320 Added Coveo filter expression
  2020-05-08 extcqb IT-2799 Hide Support Level/Support SLA from the Support Portal
  2020-05-27 extcqb IT-2941 Hide Support Level/Support SLA from the Support Portal #2
 *************************************************************************/ -->
<apex:page cache="false" standardstylesheets="false" showheader="false" sidebar="false" 
            language="{!$CurrentPage.parameters.l}" applybodytag="false" applyhtmltag="false" 
                controller="QS_LicenseController">
    <CoveoV2:JsSearch stylesheet="CoveoFullSearchNewDesign.css" filter="{!CoveoFilterExpression}"/>
    <apex:composition template="QS_PageTemplate">
        <apex:define name="body">
            <section>
                <div class="wrapper">
                    <div class="heading section-title">
                        <div>
                            <h1>Licenses</h1>
                        </div>
                        <div class="CoveoSearchbox" data-enable-omnibox="true" data-placeholder="Search for answers" data-design="new" data-trigger-query-on-clear="false"></div>
                        <script type="text/javascript">
                            $(function () {
                                Coveo.initSearchbox(document.querySelector('.CoveoSearchbox'), "/apex/QS_CoveoSearch");
                            });
                        </script>
                        <script  type="text/javascript">
                            function bindEvent(el, eventName, eventHandler) {
                              if (el.addEventListener){
                                el.addEventListener(eventName, eventHandler, false); 
                              } else if (el.attachEvent){
                                el.attachEvent('on'+eventName, eventHandler);
                              }
                            }

                            function isie() {

                                var msie = window.navigator.userAgent.indexOf("MSIE ");

                                return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
                            }
                            bindEvent(document, 'DOMContentLoaded', function(){
                              var searchbox = document.querySelector('.CoveoSearchbox');
                              Coveo.$$(searchbox).on(Coveo.StandaloneSearchInterfaceEvents.beforeRedirect, function(e, args) {
                                    if (isie())
                                        args.searchPageUri = window.location.hostname + args.searchPageUri;
                              });
                            })
                        </script>   
                    </div>
                    <div class="qs-page-licenses qs-page">
                        <div> 
                            <p class="qs-intro">
                                Browse - Filter<br />
                                Request a control number or download a LEF.
                            </p>  
                            <apex:pagemessages escape="false" />
                            <apex:form id="caselist">
                                <ul class="qs-filter">
                                    <apex:inputHidden value="{!userIpAddress}" id="hidIpAddress"/>
                                    <apex:variable rendered="{!(accountListSize>0)  && IsPartner}" var="renderAccountListSection" value="1">
                                        <li>
                                            <label for="accountLookup" > Account </label>
                                            <div class="search-filter">
                                                <apex:variable rendered="{!(accountListSize>0)  && IsPartner}" var="renderAccountListSection" value="1">
                                                    <apex:variable rendered="{!(accountListSize>20)}" var="renderFilter" value="1">
                                                        <div class="field searchbox">
                                                            <div class="textfield">
                                                                <apex:inputText id="AccountFilterTextInput" value="{!AccountFilterText}"></apex:inputText>
                                                            </div>
                                                            <apex:commandButton styleclass="btn" rendered="{!(accountListSize>20)}"  action="{!FilterAccounts}" value="Filter"/>
                                                        </div>
                                                    </apex:variable>
                                                    <div>
                                                        <div class="field" >
                                                            <div class="selectfield">
                                                                <apex:selectList id="accountLookup1" size="1" value="{!selectedAccountName}" onchange="refreshLicenseList(); reloadTables();">
                                                                    <apex:selectOptions id="accountOptionList" value="{!accountOptionListFiltered}"  />
                                                                </apex:selectList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </apex:variable>
                                            </div>
                                        </li>
                                    </apex:variable>
                                    <apex:variable rendered="{!!IsPartner}" var="renderAccountListSection" value="1">
                                        <li>
                                            <label for="Support Partner" > Supporting Partner </label> 
                                            <apex:variable rendered="{!!IsPartner}" var="renderAccountListSection" value="1">
                                                <apex:variable rendered="{!(SpartnerListSize>20)}" var="renderAccountListSection" value="1">
                                                    <div class="field searchbox">
                                                        <div class="textfield">
                                                            <apex:inputText rendered="{!(SpartnerListSize>20)}" id="AccountFilterTextInput1" value="{!PartnerFilterText}"></apex:inputText>
                                                        </div>
                                                        <apex:commandButton styleclass="btn" rendered="{!(SpartnerListSize>20)}"  action="{!FilterPartners}" value="Filter" onclick="this.form.target=''"/>
                                                    </div>
                                                </apex:variable>
                                                <div class="field" >
                                                    <div class="selectfield">  
                                                        <apex:selectList id="SupportPId" size="1" value="{!selectedPartnerName}"  onchange="refreshLicenseList();" onclick="this.form.target=''">
                                                            <apex:selectOptions id="SupportPId1" value="{!accountOptionListFiltered2}"  />
                                                        </apex:selectList>
                                                    </div>
                                                </div>
                                            </apex:variable>
                                        </li>
                                    </apex:variable>
                                </ul>
                                
                                <Apex:outputPanel id="LicensePanel">
                                <div id="CaseTabs">
                                    <div>
                                        <ul>
                                            <li>
                                                <apex:outputlink value="#licenseTabActive" id="ActiveTab">Active
                                                    <apex:actionSupport event="onclick" action="{!SetSelectedLicenseListToActive}" reRender="licenseSelector"/>
                                                </apex:outputlink>
                                            </li>
                                            <li>
                                                <apex:outputlink value="#licenseTabExpired" id="ExpiredTab">Expired
                                                    <apex:actionSupport event="onclick" action="{!SetSelectedLicenseListToExpired}" reRender="licenseSelector"/>
                                                </apex:outputlink>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="licenseTabActive">
                                        <apex:variable var="k" value="{!1}" />
                                        <table id="caseTable1" class="display">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Product License</th>
                                                    <th>Description</th>
                                                    <apex:outputPanel rendered="{!IsPartner}">
                                                        <th>Account Name</th>
                                                    </apex:outputPanel>
                                                    <th>End Date</th>
                                                    <th>License Reference</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <apex:repeat var="Lic" value="{!resultsLicenses}">
                                                    <tr id="caseTable1_row_{!k}">
                                                        <td>
                                                            <apex:inputcheckbox value="{!Lic.checked}" id="licenseChecklist">
                                                                <apex:actionSupport event="onchange,onclick" reRender="licenseChecklist,licenseTabActive"/>
                                                            </apex:inputcheckbox>
                                                            <apex:inputHidden value="{!Lic.checked}" id="hidLicenseChecklist"/>
                                                            <apex:outputText value="{!Lic.cat.License_Netsuite_Internal_Id__c}" id="hidLicenseNetSuiteId" style="display:none;"/>
                                                            <apex:outputText value="{!Lic.cat.License_Account_NetSuite_Id__c}" id="hidAccountNetSuiteId" style="display:none;"/>
                                                            <apex:outputText value="{!Lic.cat.Support_Provided_By_Id__c}" id="hidLicSupportProvBy" style="display:none;"/>
                                                            <apex:outputText value="{!Lic.cat.Name}" id="hidLicenseName" style="display:none;"/>
                                                        </td>
                                                        <td> 
                                                            <a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">{!Lic.cat.Name}</a> <apex:image id="theImage" title="{!IF((Lic.cat.status == 'Inactive'), 'Inactive' , '' )}"  value="{!IF((Lic.cat.status == 'Inactive'), $Resource.Inactivesymbol , '' )}" />
                                                        </td>
                                                        <td>
                                                            <a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">{!IF(IsNumber(LEFT(Lic.cat.Description__c, 4)), RIGHT(Lic.cat.Description__c, LEN(Lic.cat.Description__c)-4), Lic.cat.Description__c)}</a> 
                                                        </td>
                                                        <apex:outputPanel rendered="{!IsPartner}">
                                                            <td><a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">{!Lic.cat.Account.Name}</a> </td>
                                                        </apex:outputPanel>
                                                        <td>
                                                            <a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">
                                                                <apex:outputtext value="{0,date, MMMM d,  yyyy}">
                                                                    <apex:param value="{!Lic.cat.EndDate}" />
                                                                </apex:outputtext>
                                                            </a>
                                                        </td>
                                                        <td><a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}"> {!Lic.cat.License_Reference__c} </a> </td>
                                                      <td style="display:none;">                        
                                                        {!Lic.cat.Legacy_License_Key__c}>
                                                        </td>  
                                                   
                                                    </tr>
                                                    <apex:variable var="k" value="{!k + 1}" />
                                                </apex:repeat>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="licenseTabExpired">
                                        <apex:variable var="j" value="{!1}" />
                                        <table id="caseTable2" class="display">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Product License</th>
                                                    <th>Description</th>
                                                    <apex:outputPanel rendered="{!IsPartner}">
                                                        <th>Account Name</th>
                                                    </apex:outputPanel>
                                                    <th>End Date</th>
                                                    <th>License Reference</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <apex:repeat var="Lic" value="{!resultsExpiredLicenses}">
                                                    <tr id="caseTable2_row_{!j}">
                                                        <td>
                                                            <apex:inputcheckbox value="{!Lic.checked}" id="licenseChecklist">
                                                            </apex:inputcheckbox>
                                                            <apex:inputHidden value="{!Lic.checked}" id="hidLicenseChecklist"/>
                                                            <apex:outputText value="{!Lic.cat.License_Netsuite_Internal_Id__c}" id="hidLicenseNetSuiteId_expired" style="display:none;"/>
                                                            <apex:outputText value="{!Lic.cat.License_Account_NetSuite_Id__c}" id="hidAccountNetSuiteId_expired" style="display:none;"/>
                                                            <apex:outputText value="{!Lic.cat.Support_Provided_By_Id__c}" id="hidLicSupportProvBy_expired" style="display:none;"/>
                                                            <apex:outputText value="{!Lic.cat.Name}" id="hidLicenseName_expired" style="display:none;"/>
                                                        </td>
                                                        <td><a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">{!Lic.cat.Name}</a> </td>
                                                        <td><a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">{!IF(IsNumber(LEFT(Lic.cat.Description__c, 4)), RIGHT(Lic.cat.Description__c, LEN(Lic.cat.Description__c)-4), Lic.cat.Description__c)}</a> </td>
                                                        <apex:outputPanel rendered="{!IsPartner}">
                                                            <td><a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">{!Lic.cat.Account.Name}</a> </td>
                                                        </apex:outputPanel>
                                                        <td>
                                                            <a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}">
                                                                <apex:outputtext value="{0,date, MMMM d,  yyyy}">
                                                                    <apex:param value="{!Lic.cat.EndDate}" />
                                                                </apex:outputtext>
                                                            </a>
                                                        </td>
                                                        <td><a href="{!$Page.QS_LicenseDetailsPage }?LicId={!Lic.cat.id}"> {!Lic.cat.License_Reference__c} </a> </td>
                                                        
                                                        <td style="display:none;">                        
                                                            {!Lic.cat.Legacy_License_Key__c}>
                                                        </td>
                                                    </tr>
                                                    <apex:variable var="j" value="{!j + 1}" />
                                                </apex:repeat>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <apex:outputpanel id="buttonTable" >
                                    <div id="ButtonTab" class="qs-module qs-clearfix" style="display:none">
                                        <apex:commandbutton id="btnGenerateLEF2" value="Download LEF" styleclass="btn" onclick="dwldLEF(false);" oncomplete="this.form.target='_blank'"/>
                                        <input type="button" class="btn btn-secondary" onclick="initDialog(false);return false;" value="Request Control Number"/>
                                    </div>
                                    </apex:outputpanel>
                                </apex:outputPanel>
                                <apex:actionFunction name="refreshLicenseList" rerender="LicensePanel,buttonTable" oncomplete="initTables();"/>
                            </apex:form>
                        </div>
                    </div>
                    <style type="text/css">
                    .custPopup {
                        background-color: white;
                        border-width: 2px;
                        border-style: solid;
                        z-index: 9999;
                        left: 50%;
                        padding: 10px;
                        position: absolute;
                        width: 500px;
                        margin-left: -250px;
                        top: 100px;
                    }

                    .popupBackground {
                        background-color: black;
                        opacity: 0.20;
                        filter: alpha(opacity = 20);
                        position: absolute;
                        width: 100%;
                        height: 100%;
                        top: 0;
                        left: 0;
                        z-index: 9998;
                    }
                </style>
                </div>
            </section>
            <apex:include pageName="QS_LicenseListPageJS"/>
        </apex:define>
    </apex:composition>
</apex:page>