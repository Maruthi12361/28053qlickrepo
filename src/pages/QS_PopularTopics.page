<!--************************************************************
* 2019-05-05 AIN Created for Support Portal Redesign
* 2019-11-21 AIN IT-2320 Added Coveo filter expression
* 2020-02-12 ext_bad IT-2202 Moved QS_Resources to the https://support-files.qlik.com
*************************************************************-->
<apex:page cache="false" standardStylesheets="false" showHeader="false" sidebar="false" 
language="{!$CurrentPage.parameters.l}" applyBodyTag="false" applyHtmlTag="false" controller="QS_MiniController">
    <head>
        <link rel="canonical" href="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Login_Url__c}/QS_PopularTopics"/>
    </head>
    <CoveoV2:JsSearch stylesheet="CoveoFullSearchNewDesign.css" filter="{!CoveoFilterExpression}"/>
    <apex:composition template="QS_PageTemplate">
        <apex:define name="body">
            <section>
                <div class="wrapper">
                    <div class="heading section-title">
                        <div><h1>Browse Popular Topics</h1></div>
                        <div class="CoveoSearchbox field textfield searchfield" data-enable-omnibox="true" data-placeholder="Search for answers" data-design="new" data-trigger-query-on-clear="false"></div>
                        <script type="text/javascript">
                            $(function () {
                                Coveo.initSearchbox(document.querySelector('.CoveoSearchbox'), "/apex/QS_CoveoSearch");
                            });
                            var anchor = window.top.location.hash.substr(1);
                            window.onload = function(){
                                if(anchor != '') {
                                    anchor = '#' + anchor;
                                    $('html, body').animate({
                                        scrollTop: $(anchor).offset().top
                                    });
                                }
                            }
                        </script>
                        <script  type="text/javascript">
                            function bindEvent(el, eventName, eventHandler) {
                              if (el.addEventListener){
                                el.addEventListener(eventName, eventHandler, false); 
                              } else if (el.attachEvent){
                                el.attachEvent('on'+eventName, eventHandler);
                              }
                            }

                            function isie() {

                                var msie = window.navigator.userAgent.indexOf("MSIE ");

                                return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
                            }
                            bindEvent(document, 'DOMContentLoaded', function(){
                              var searchbox = document.querySelector('.CoveoSearchbox');
                              Coveo.$$(searchbox).on(Coveo.StandaloneSearchInterfaceEvents.beforeRedirect, function(e, args) {
                                    if (isie())
                                        args.searchPageUri = window.location.hostname + args.searchPageUri;
                              });
                            })
                        </script>
                    </div>
                    <div class="listing">
                        <article class="post">
                            <div class="inner target" id="qlikid">
                                <img src="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Resources_URL__c}/img/icon_idbadge.svg" width="48" height="48" alt=""/>
                                <h3>Qlik Account</h3>
                                <ul class="alt-list">
                                    <li><a href="articles/000002823" target="_blank"> How to register</a></li>
                                    <li><a href="articles/000046697" target="_blank"> Qlik ID (Qlik Account) Requirements and Restrictions</a></li>
                                    <li><a href="articles/000002830" target="_blank"> Reset your password</a></li>
                                    <li><a href="articles/000015561" target="_blank"> No Password/activation email received</a></li>
                                    <li class="all"><a href="QS_CoveoSearch#q=Qlik%20ID&t=All&sort=relevancy"> See All </a></li>
                                </ul>
                            </div>
                        </article>
                        <article class="post">
                            <div class="inner target" id="license">
                                <img src="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Resources_URL__c}/img/icon_license.svg" width="48" height="48" alt=""/>
                                <h3>License Information</h3>
                                <ul class="alt-list">
                                    <li><a href="articles/000002480" target="_blank"> How to lease a license</a></li>
                                    <li><a href="articles/000016836" target="_blank"> How to request Control number or License Enablement File &#40;LEF&#41;</a></li>
                                    <li><a href="articles/000005532" target="_blank"> Update or apply QlikView license</a></li>
                                    <li><a href="articles/000005501" target="_blank"> How to license QlikView Desktop</a></li>
                                    <li class="all"><a href="QS_CoveoSearch#q=License&t=All&sort=relevancy"> See All </a></li>
                                </ul>
                            </div>
                        </article>
                        <article class="post">
                            <div class="inner target" id="install">
                                <img src="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Resources_URL__c}/img/icon_boxopen.svg" width="48" height="48" alt=""/>
                                <h3> Install and Upgrade </h3>
                                <ul class="alt-list">
                                    <li><a href="https://help.qlik.com/en-US/sense/February2019/Subsystems/PlanningQlikSenseDeployments/Content/Sense_Deployment/Planning-your-deployment.htm"> Planning your Qlik Sense Deployment </a></li>
                                    <li><a href="articles/000047422" target="_blank"> QlikView 11.20 to November 2017 and 2018</a></li>
                                    <li><a href="articles/000024083" target="_blank"> Qlik Sense Monitoring Apps problems post upgrade</a></li>
                                    <li><a href="articles/000064633" target="_blank"> AntiVirus exclusions for Qlik Products</a></li>
                                    <li class="all"><a href="QS_CoveoSearch#q=install%20and%20upgrade&t=All&sort=relevancy"> See All </a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="listing">
                        <article class="post">
                            <div class="inner target" id="performance">
                                <img src="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Resources_URL__c}/img/icon_analyticsdesktop.svg" width="48" height="48" alt=""/>
                                <h3> Performance and Stability </h3>
                                <ul class="alt-list">
                                    <li><a href="articles/000034822" target="_blank"> Qlik Engines and Resource Consumption</a></li>
                                    <li><a href="articles/000045714" target="_blank"> Troubleshoot Qlik Sense Engine</a></li>
                                    <li><a href="articles/000047819" target="_blank"> Troubleshoot Qlik Sense Hub and Qlik Management Console (QCM) access</a></li>
                                    <li><a href="articles/000059862" target="_blank"> Troubleshoot QlikView Engine</a></li>
                                    <li class="all"><a href="QS_CoveoSearch#q=performance&t=All&sort=relevancy"> See All </a></li>
                                </ul>
                            </div>
                        </article>
                        <article class="post">
                            <div class="inner target" id="design">
                                <img src="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Resources_URL__c}/img/icon_desktopdesign.svg" width="48" height="48" alt=""/>
                                <h3> Designing Apps </h3>
                                <ul class="alt-list">
                                    <li><a href="https://help.qlik.com/en-US/sense/February2019/Subsystems/Hub/Content/Sense_Hub/Visualizations/creating-visualization.htm" target="_blank"> Create Visualizations</a></li>
                                    <li><a href="articles/000036003" target="_blank"> Sense: Failing reload and Backus-Naur Formalism &#40;BNF&#41; script reload mode</a></li>
                                    <li><a href="articles/000004090" target="_blank"> Section Access Best Practice Guidance</a></li>
                                    <li><a href="articles/000039813" target="_blank"> Data Connections not loading</a></li>
                                    <li class="all"><a href="QS_CoveoSearch#q=chart%20visualizations%20expressions%20color&t=All&sort=relevancy"> See All </a></li>
                                </ul>
                            </div>
                        </article>
                        <article class="post">
                            <div class="inner target" id="monitoring">
                                <img src="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Resources_URL__c}/img/icon_target.svg" width="48" height="48" alt=""/>
                                <h3> Monitoring and Auditing </h3>
                                <ul class="alt-list">
                                    <li><a href="https://help.qlik.com/en-US/sense/February2019/Subsystems/Monitoring/Content/Sense_Monitoring/Introduction/Monitor-Qlik-Sense-site.htm"> Qlik Sense Monitoring Apps</a></li>
                                    <li><a href="articles/000030068" target="_blank"> Qlik Sense Performance Monitoring</a></li>
                                    <li><a href="https://help.qlik.com/en-US/governance-dashboard/Content/What.htm"> QlikView Governance Dashboard</a></li>
                                    <li><a href="articles/000033032" target="_blank"> Trace User in QlikView</a></li>
                                    <li class="all"><a href="QS_CoveoSearch#q=Monitoring%20and%20Audit&t=All&sort=relevancy"> See All </a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="listing-title target" id="logfiles">
                        <img src="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Resources_URL__c}/img/icon_sprockets.svg" width="48" height="48" alt=""/>
                        <h3> Collecting Log Files and System Information </h3>
                    </div>
                    <div class="listing">
                        <article class="post">
                            <div class="inner">
                                <ul class="alt-list">
                                    <li><a href="articles/000003712" target="_blank"> How to Collect QlikView Server Log Files</a></li>
                                    <li><a href="articles/000006144" target="_blank"> How to Collect Qlik Sense Log Files</a></li>
                                    <li><a href="articles/000007495" target="_blank"> How to Collect Event Logs</a></li>
                                </ul>
                            </div>
                        </article>
                        <article class="post">
                            <div class="inner">
                                <ul class="alt-list">
                                    <li><a href="articles/000002551" target="_blank"> How to Upload Large Files to Qlik Support Over FTP</a></li>
                                    <li><a href="articles/000004240" target="_blank"> How to gather additional hardware and system information</a></li>
                                </ul>
                            </div>
                        </article>
                        <article class="post">
                            <div class="inner">
                                <ul class="alt-list">
                                    <li><a href="articles/000002960" target="_blank"> How to gather additional Technical information for QlikView Support Cases</a></li>
                                    <li><a href="articles/000053334" target="_blank"> How to collect NPrinting 18.x log files</a></li>
                                    <li><a href="articles/000026186" target="_blank"> How to collect NPrinting 17.x log files</a></li>
                                    <li class="all"><a href="QS_CoveoSearch#q=collecting%20log%20files&t=All&sort=relevancy"> See All </a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                </div>
            </section>
            <c:QS_GetAnswers />
            <c:QS_StillLooking />
        </apex:define>
    </apex:composition>
</apex:page>