<apex:page >

<a style="VISIBILITY: hidden; TEXT-DECORATION: none" id="BA" class="bookmark" title="BA" name="BA" alias="BA"></a>

<h1>Certification FAQs</h1>

<!-- SUB NAV  -->
<div style="width:225px; padding:10px; border:1px solid #efefef; float:right; margin-left:25px;">
<img src="http://global.qlikview.com/reskin/images/bg_bullets_dark_gray.gif" />&nbsp;<b>Certification Exams</b><br />
<img style="margin-left:15px;" src="http://global.qlikview.com/reskin/images/bg_bullets.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertDesign">Designer</apex:outputlink><br />
<img style="margin-left:15px;" src="http://global.qlikview.com/reskin/images/bg_bullets.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertDev">Developer</apex:outputlink><br />
<img style="margin-left:15px;" src="http://global.qlikview.com/reskin/images/bg_bullets.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertAdmin">System Administrator</apex:outputlink><br />
<img src="http://global.qlikview.com/reskin/images/bg_bullets_dark_gray.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertGain">Gain Competitive Advantage</apex:outputlink><br />
<img src="http://global.qlikview.com/reskin/images/bg_bullets_dark_gray.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertExam">Register for an Exam</apex:outputlink><br />
<!--<img src="http://global.qlikview.com/reskin/images/bg_bullets_dark_gray.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertCreds">Manage Your Credentials</apex:outputlink><br />-->
<img src="http://global.qlikview.com/reskin/images/bg_bullets_dark_gray.gif" />&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertFAQ">Certification FAQs</apex:outputlink><br />
</div>

<p>Please see our Frequently Asked Questions below for answers to some of the most common questions related to QlikView Certification.</p>

<ul>
    <li><a href="#REGISTER">Exam Registration Questions</a></li>
    <li><a href="#PAY">Payment Options</a></li>
    <li><a href="#TAKE">Taking the Exam</a></li>
    <li><a href="#EXAMINFO">Exam Information and Objectives</a></li>
    <li><a href="#CERTCRED">Certification Credentials</a></li>
</ul>

<p style="LINE-HEIGHT: 24px; COLOR: #62ac1e; FONT-SIZE: 14px;"><strong>General Questions</strong></p>

<h3>Which certifications are available from QlikView?</h3>

<ul>
    <li>QlikView 11 Certified Designer</li>
    <li>QlikView 11 Certified Developer</li>
    <li>QlikView 11 Certified System Administrator</li>
</ul>

<h3>How will the QlikView 11 Certification Exams be offered?</h3>

<p>QlikView 11 Designer and QlikView 11 Developer Certification examinations are scenario-based, multiple choice knowledge tests delivered at thousands of <a class="external" href="http://www.pearsonvue.com/qlikview/">Pearson VUE</a> test centers globally. You can register for the Designer or Developer exam at the Pearson VUE web site.</p>

<p>The QlikView 11 System Administrator Certification examination is an online exam proctored in a virtual laboratory environment provided by ReadyTech*. You can register for the System Administrator exam within <a class="external" href="http://www.qlikview.com/us/services/training/my-training">MyTraining</a> (our Learning Management System) and can take the examination from your home or office.</p>

<h3>What languages are exams offered in?</h3>

<p>The QlikView 11 Designer and Developer Certification Exams are offered in English, Spanish, Chinese (Simplified), Japanese, French, German, Brazilian Portuguese and Russian.</p>

<h3>What are the Qonnect Certification Requirements?</h3>

<p>Please refer to the certification requirements within your current Partner Program &amp; Contract.</p>

<h3>If I've taken training do I also need to take the certification exam?</h3>

<p>Yes. Training and field experience will help you prepare for the certification exam, but training in and of itself does not substitute for taking the certification exam. You must still take the certification exam to fulfill your Qonnect requirements.</p>

<h3>How much do the exams cost?</h3>

<p>The QlikView Designer and QlikView Developer Certification Examinations cost the equivalent of $250 USD, paid for by credit card when you register through Pearson VUE. QlikView System Administrator Certification Examinations are priced regionally and can be paid via Purchase Order when you register through MyTraining. Please refer to your regional training price list for QlikView System Administrator Certification Examination pricing in your region.</p>

<h3>Where can I find a Pearson VUE test center?</h3>

<p>Pearson VUE delivers certification and licensure tests through the world's largest global network of test centers. To locate a testing center near you, <a class="external" href="http://www.pearsonvue.com/qlikview/">log in</a> to your Pearson VUE account and click Locate a Test Center.</p>

<h3>What is the timeline to fulfill the education and certification requirements?</h3>

<p>All partners need to complete the education and certifications within 6 months after signing the program agreement or within 6 months after being promoted from Solution Provider to Elite Solution Provider.</p>

<h3>Do I have to re-certify for a new product release?</h3>

<p>In order for you to remain current with the QlikView products and to provide a competitive advantage, Qonnect Partners should plan on re-certifying within six months of the release of a new exam. For example, the QlikView 10 Certified Developer should plan to re-certify by taking the new exam prior to September 2, 2012. QlikView Designer and QlikView Developer re-certification is charged at the equivalent of $250 USD. QlikView System Administrator re-certification is priced regionally.</p>

<h3>Can I take a Delta exam when a new product version is released?</h3>

<p>We will not be offering Delta exams for partners holding Version 10 certification who wish to upgrade to QlikView 11. All partners who would like to hold QlikView 11 certification must take the full QlikView 11 online certification exam.</p>

<h3>What are the QlikView 11 Developer and QlikView 11 Designer exams like?</h3>

<ul>
    <li>Exams are proctored, timed, and delivered in a secure environment. Most exams last approximately one to two hours.</li>
    <li>Candidates must acknowledge the QlikView Candidate Nondisclosure Agreement online when they register to take a QlikView Developer or Designer Certification exam. Signing this legal agreement is required in order to register for the certification exam.</li>
    <li>Exams can be very challenging, going beyond simple recall and requiring candidates to engage in on-the-job types of problem solving. Question types are multiple-choice single answer and multiple-choice multiple answers.</li>
    <li>At the completion of an exam, candidates receive their score and a Pass or Fail grade.</li>
</ul>

<h3>Why must I accept the Nondisclosure before registering for the exam?</h3>

<p>The contents of QlikView Certification exams are confidential and disclosure of exam content and information could compromise the integrity of the exams, and ultimately the certification itself. By accepting the terms of the Nondisclosure (NDA), you are legally prohibited from disclosing content and information that is in or is directly related to QlikView Certification exams. The NDA and the terms and conditions may be viewed in advance <a href="http://www.vue.com/qlikview/qlikview_nda.pdf" target="_blank">here</a>.</p>

<p><a style="LINE-HEIGHT: 24px; COLOR: #62ac1e; FONT-SIZE: 14px; TEXT-DECORATION: none" id="REGISTER" class="bookmark" title="REGISTER" name="REGISTER" alias="REGISTER"><strong>Exam Registration Questions</strong></a></p>

<h3>How do I Register for a QlikView Certification Examination?</h3>

<p>Refer to&nbsp;<apex:outputlink value="PPEducation?SubPage=ppSubEducationCertExam">Register for an Exam</apex:outputlink>.</p>

<h3>How can I check my QlikView Designer or QlikView Developer certification registration status?</h3>

<p>From the Pearson VUE site, log in and click on Current Activity: <a href="http://www.vue.com/qlikview">http://www.vue.com/qlikview</a></p>

<h3>How can I reschedule a QlikView Designer or QlikView Developer Certification exam?</h3>

<p>If you wish to reschedule your exam, from the Pearson VUE site, click on Reschedule a Test at <a href="http://www.vue.com/qlikview">http://www.vue.com/qlikview</a>, or <a href="http://www.vue.com/qlikview/contact/">contact Pearson VUE</a>, two business days prior to your scheduled appointment.</p>

<h3>How can I cancel a QlikView Designer or QlikView Developer Certification exam?</h3>

<p>From the Pearson VUE site, click on Cancel a Test: <a href="http://www.vue.com/qlikview">http://www.vue.com/qlikview</a></p>

<h3>How can I get help registering for a QlikView Designer or QlikView Developer Certification Examination?</h3>

<p>You may email, phone, or Live Chat if you are in the Americas, with Pearson VUE <a href="http://www.vue.com/qlikview/contact/">Customer Service Representatives</a>.</p>

<h3>How can I check my QlikView System Administrator certification registration status?</h3>

<p>From <a href="http://www.qlikview.com/training">www.qlikview.com/training</a>, click on MyTraining.</p>

<h3>How can I schedule or cancel a QlikView System Administrator Certification Examination?</h3>

<p>Contact your <a href="http://www.qlikview.com/us/services/training/training-contacts">Local Training Center</a>.</p>

<h3>How can I get help registering for a QlikView System Administrator Certification Exam?</h3>

<p>Contact <a href="mailto:qlikviewtraining@qlikview.com">qlikviewtraining@qlikview.com</a> or your <a href="http://www.qlikview.com/us/services/training/training-contacts">Local Training Center</a>.</p>

<div style="TEXT-ALIGN: right; PADDING-BOTTOM: 5px; FONT-SIZE: 12px"><a href="#BA">Top</a></div>

<p><a style="LINE-HEIGHT: 24px; COLOR: #62ac1e; FONT-SIZE: 14px; TEXT-DECORATION: none" id="PAY" class="bookmark" title="PAY" name="PAY" alias="PAY"><strong>Payment Options</strong></a></p>

<h3>How much do the exams cost?</h3>

<p>The QlikView Designer and QlikView Developer Certification Examinations cost the equivalent of $250 USD, paid for by credit card when you register through Pearson VUE. QlikView System Administrator Certification Examinations are priced regionally equivalent to $250 USD and can be paid via Purchase Order when you register through MyTraining. Please contact your <a href="http://www.qlikview.com/us/services/training/training-contacts">Local Training Center</a> for System Administrator exam pricing in your region.</p>

<h3>How do I pay for my QlikView Designer and QlikView Certified Developer Certification exams?</h3>

<p>When you are logged into the <a href="http://www.vue.com/qlikview">Pearson VUE</a> site and have selected your testing location, exam, and appointment time, you will be asked to pay for the exam by credit card (AMEX, JCB, MC or VISA). A voucher may also be purchased prior to scheduling your exam. Click on "Purchase Test Vouchers" under Related Links on the Pearson VUE page and follow the instructions.</p>

<h3>How do I pay for my QlikView System Administrator Certification Examination?</h3>

<p>Provide a Purchase Order to your <a href="http://www.qlikview.com/us/services/training/training-contacts">Local Training Center</a>.</p>

<h3>Can I get a receipt for the fee paid for my QlikView Designer and QlikView Certified Developer Certification exams?</h3>

<p>After you register for your exam, you will receive a confirmation email from Pearson VUE which serves as your receipt. If you require a PDF version of your receipt, contact Pearson VUE.</p>

<h3>Will my prior certifications be in CertTracker?</h3>
<p>No, only certifications earned by passing exams taken at a Pearson VUE testing center will be shown in CertTracker.</p>

<div style="TEXT-ALIGN: right; PADDING-BOTTOM: 5px; FONT-SIZE: 12px"><a href="#BA">Top</a></div>

<p><a style="LINE-HEIGHT: 24px; COLOR: #62ac1e; FONT-SIZE: 14px; TEXT-DECORATION: none" id="TAKE" class="bookmark" title="TAKE" name="TAKE" alias="TAKE"><strong>Taking the Exam</strong></a></p>

<h3>What should I bring to the Pearson VUE test center?</h3>

<p>You are required to provide two forms of identification at the testing center, including one photo identification, such as a driver's license or a valid passport. Both forms must display your signature. To maintain the security of the test environment, you are not permitted to bring reference materials of any kind into the testing center.</p>

<h3>When should I arrive?</h3>

<p>You should plan to arrive 15 minutes prior to their scheduled exam time. If you arrive more than 15 minutes late, you are not guaranteed exam availability or a refund.</p>

<h3>Can I retake an exam? Is there a quarantine period?</h3>

<p>If you do not pass an exam, you can retake it as many times as you wish. Each retake is at full-charge and you must wait for seven days between retakes.</p>

<h3>Why must I pay to retake the exam?</h3>

<p>QlikView exams are administered by a third party and there are costs associated with each instance of an exam being taken.</p>

<h3>Can I find out whether I answered a specific question correctly, or whether a particular answer affected my Pass/Fail status?</h3>

<p>For exam security purposes, we do not provide item-level score reporting.</p>

<div style="TEXT-ALIGN: right; PADDING-BOTTOM: 5px; FONT-SIZE: 12px"><a href="#BA">Top</a></div>

<p><a style="LINE-HEIGHT: 24px; COLOR: #62ac1e; FONT-SIZE: 14px; TEXT-DECORATION: none" id="EXAMINFO" class="bookmark" title="EXAMINFO" name="EXAMINFO" alias="EXAMINFO"><strong>Exam Information and Objectives</strong></a></p>

<h3>How are the QlikView Designer and Developer exam scores calculated?</h3>

<ul>
    <li>There is no penalty for providing incorrect answers. If you choose an incorrect answer, you simply do not earn credit for that item. No points are deducted for incorrect answers or answers left blank.</li>
    <li>If a question specifies that you must choose multiple correct answers, you must choose the exact number of correct answers specified in the question in order to earn the credit for a correct answer. Partial credit is not given.</li>
    <li>Some questions on the exam might not be included in the calculation of your score. To gather data to update and improve the quality of QlikView certification exams, we need to present new content to candidates without counting the results toward their score.</li>
    <li>Scoring is calculated by totaling up the number of correct answers and determining if this is above or below the established passing score for this particular exam.</li>
</ul>

<h3>What are the passing scores for the QlikView Designer and Developer exams?</h3>

<p>The passing score for the QlikView Designer Certification Exam is 65%; for the QlikView Developer Certification Exam, it is 75%.</p>

<h3>Why do I only get a Pass or Fail grade for the System Administrator Certification Exam?</h3>

<p>The QlikView System Administration exam is a hands-on lab session in a virtual environment monitored by a member of QlikTech’s Education Services team, called a censor. The scenarios to test your knowledge require certain actions to be performed, which your censor verifies, but the measures do not have a numerical score value.</p>

<h3>When will I know if I've passed the exam?</h3>

<p>If you are taking a QlikView Designer or QlikView Developer Certification Examination, you will receive your Pass/Fail grade immediately upon completing the online examination.</p>

<p>If you are taking a QlikView System Administrator Certification Examination, you will receive your Pass/Fail grade within five business days.</p>

<h3>Will my certification expire?</h3>

<p>There is no expiration on a certification. However, as part of your partnership agreement, candidates must re-certify when an updated exam is released to validate their skills on the latest technology.</p>

<h3>How should I prepare for the exam?</h3>

<p>It is strongly recommended that you prepare by attending the applicable QlikTech Training course(s), and through active use and experience in the field using QlikView. For more information on prerequisites, recommended preparation and exam domain areas, visit the page for the specific certification:</p>

  <ul class="linkedlist">
      <li><apex:outputlink value="PPEducation?SubPage=ppSubEducationCertDesign">QlikView 11 Designer</apex:outputlink></li>
      <li><apex:outputlink value="PPEducation?SubPage=ppSubEducationCertDev">QlikView 11 Developer</apex:outputlink></li>
      <li><apex:outputlink value="PPEducation?SubPage=ppSubEducationCertAdmin">QlikView 11 System Administrator</apex:outputlink></li>
    </ul>

<h3>Who will have access to my exam results?</h3>

<p>Except for administration purposes within QlikView Education Services and the QlikView Alliances team, exam results and personal information will remain confidential and will not be disclosed by QlikView to any party other than the candidate. Your score will not be forwarded to your employer. If you wish to share your QlikView Designer or Developer Certification with your employer, you can use the "Publish Credential" feature in Qlik CertTracker. If you are a QlikView System Administrator, you can print your certificate from My Training.</p>

<div style="TEXT-ALIGN: right; PADDING-BOTTOM: 5px; FONT-SIZE: 12px"><a href="#BA">Top</a></div>

<p><a style="LINE-HEIGHT: 24px; COLOR: #62ac1e; FONT-SIZE: 14px; TEXT-DECORATION: none" id="CERTCRED" class="bookmark" title="CERTCRED" name="CERTCRED" alias="CERTCRED"><strong>Certification Credentials</strong></a></p>

<h3>How can I track my QlikView Designer or Developer credentials?</h3>

<p>Access <a href="https://i7lp.integral7.com/qliktech">CertTracker</a> and log in. If you do not know your username and password for CertTracker, you can recover your account details with your Last Name and Email Address in the "Reset Your CertTracker Password” link. Once you are logged in, you may track your existing credentials for QlikView 11 and later, update your demographic information, download the certification logo, download your certificate and make your certification credentials viewable for someone you designate.</p>

<h3>How can I track my QlikView System Administrator (previously named Server/Publisher) credentials prior to QlikView 11?</h3>

<p>Log into <a href="http://www.qlikview.com/us/services/training/find-your-training">My Training</a>. Once you are logged in, you can track your existing QlikView 10 and QlikView 11 System Administrator credentials and download your certificate.</p>

<div style="TEXT-ALIGN: right; PADDING-BOTTOM: 5px; FONT-SIZE: 12px"><a href="#BA">Top</a></div>

<p style="FONT-SIZE: 11px">*ReadyTech is a hosted lab environment candidates enter to complete their exam assignment.</p>

</apex:page>