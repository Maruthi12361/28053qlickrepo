<!-- **********************************************************
ChangeLog
    2013-08-07  CCE Initial creation for CR# 8441.
    2018-01-30  BAD fixed styling for Collaboration button CHG0030508 / Jira IT-91

*********************************************************** -->
<apex:page standardcontroller="Case" extensions="CaseCollaborationExtension" standardstylesheets="false">
    <body style="margin:0; padding:0">
        <apex:stylesheet value="{!URLFOR($Resource.QlikPubAction, 'QlikPubAction.css')}" />
        <apex:includescript value="/support/console/28.0/integration.js" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>        

        <script type="text/javascript">
            //Used to refresh the page (takes into account whether it is in the Service Cloud Console or not)
            function refreshPage() {
                if (sforce.console.isInConsole()) {
                    sforce.console.getFocusedSubtabId(showTabId);
                } else {
                    parent.location.href="/" + "{!Case.Id}";
                }
            }

            var showTabId = function showTabId(result) {
                //Subtab name obtained from the getFocusedSubtabId function
                sforce.console.refreshSubtabById(result.id, true, null);
            }
            window.onload = function() {
                onPageLoad();
            };

            //retrieves the text data from the textarea field to pass to the page(and hence to the controller) and
            //works out if we need to render the button or not
            function onPageLoad(){
                checkValueRequest();
                checkValueDelay();
            }

            function checkValueRequest() {

                var myTextBox = document.getElementById("{!$Component.frmMain.pbSupportRequestCollaboration.pbsInput.txtRequestIssueDescription}");
                var myButton = document.getElementById("{!$Component.frmMain.pbSupportRequestCollaboration.btnRequestCollaboration}");

                if(myTextBox && myButton)
                {
                    if (!myTextBox.value) {
                        myButton.disabled = true;
                        document.getElementById("j_id0:frmMain:pbSupportRequestCollaboration:btnRequestCollaboration").className = ('qlik-case-collab-commandButton qlik-case-collab-commandButtonDisabled');
                    }
                    else if(myButton){
                        myButton.disabled = false;
                        document.getElementById("j_id0:frmMain:pbSupportRequestCollaboration:btnRequestCollaboration").className = ('qlik-case-collab-commandButton qlik-case-collab-commandButtonEnabled');
                    }
                }

            }

            function checkValueDelay() {

                var myTextBox = document.getElementById("{!$Component.frmMain.pbSupportDelayCollaboration.pbsInput2.txtDelayIssueDescription}");
                var myButton = document.getElementById("{!$Component.frmMain.pbSupportDelayCollaboration.btnDelayCollaboration}");

                if(myTextBox && myButton)
                {
                    if (!myTextBox.value) {
                        myButton.disabled = true;
                        document.getElementById("j_id0:frmMain:pbSupportDelayCollaboration:btnDelayCollaboration").className = ('qlik-case-collab-commandButton qlik-case-collab-commandButtonDisabled');                    
                    }
                    else if(myButton){
                        myButton.disabled = false;
                        document.getElementById("j_id0:frmMain:pbSupportDelayCollaboration:btnDelayCollaboration").className = ('qlik-case-collab-commandButton qlik-case-collab-commandButtonEnabled');                    
                    }
                }

            }

            function DisableReqCollaborationClick() {
                this.disabled=true;
                document.getElementById("j_id0:frmMain:pbSupportRequestCollaboration:btnRequestCollaboration").className = ('qlik-case-collab-commandButton qlik-case-collab-commandButtonDisabled');
            }

            function DisableCanCollaborationClick() {
                this.disabled=true;
                document.getElementById("j_id0:frmMain:pbSupportDelayCollaboration:btnDelayCollaboration").className = ('qlik-case-collab-commandButton qlik-case-collab-commandButtonDisabled');             
            }
        </script>

        <style>
            .qlik-case-collab-commandButton {
                position:absolute; 
                right:10px; 
                background:#e0e5ee; 
                color:#fff; 
                font-size:12px; 
                font-weight: normal; 
                font-family: 'SalesforceSans-Regular',Helvetica,Arial,sans-serif; 
                height: auto; 
                white-space: nowrap; 
                line-height: 27px; 
                padding: 0px 16px 0px 16px; 
                border-radius: 4px; 
                border: 1px solid #e0e5ee;                
            }
            .qlik-case-collab-commandButtonDisabled{
                background:#e0e5ee;
                border: none !important;
                outline: none !important;
            }
            .qlik-case-collab-commandButtonEnabled{
                background:#0070d2;
                border: none !important;
                outline: none !important;
            }
            .qlik-case-collab-commandButtonEnabled:enabled:hover {
                background-color: #005fb2 !important;
            }
            .qlik-case-collab-commandButtonEnabled:enabled:active {
                background-color: #16325c !important;
                border: none !important;
                outline: none !important;
            }
        </style>
        <apex:form id="frmMain">
            <apex:outputpanel id="headerPanel" layout="block" style="margin:0; padding:0">
                <apex:outputpanel rendered="{!caze.Request_Collaboration_at__c == null || (caze.Request_Collaboration_at__c > Now())}">
                    <apex:pageblock id="pbSupportRequestCollaboration" mode="maindetail">
                        <div style="height:110px">
                            <apex:pageblocksection id="pbsInput" showheader="false" columns="1">
                                <apex:outputtext escape="false" value="If you wish to ask for assistance on your case, click 'Request Collaboration'. The case will be added to the Collaboration Queue.">
                                </apex:outputtext>
                                
                                <apex:inputTextarea value="{!issueDescription}" styleclass="qlik-pub-action-inputTextarea" cols="88" rows="4" id="txtRequestIssueDescription" html-placeholder="Describe what the issue is, what's been done to resolve it and what you need help with" onkeyup="javascript: checkValueRequest();" style="overflow-x: hidden;" />
                            </apex:pageblocksection>
                        </div>
                        <div style="height:30px; line-height: 30px; padding: 9px; background:#f5f6f6; border: 1px solid #c7ccce; border-radius: 0 0 3px 3px; border-top-left-radius: 0; border-top-right-radius: 0; margin: 0">
                            <apex:outputpanel id="butPanel" style="background: #f4f6f9;">
                                <!-- -->
                                <apex:commandbutton id="btnRequestCollaboration" value="Request Collaboration" onclick="javascript: DisableReqCollaborationClick()" action="{!requestCollaboration}" oncomplete="checkValueDelay();" rerender="headerPanel" style="position:absolute; right:10px;">
                                </apex:commandbutton>
                            </apex:outputpanel>
                        </div>
                    </apex:pageblock>    
                </apex:outputpanel>
                <apex:outputpanel rendered="{!caze.Request_Collaboration_at__c != null && (caze.Request_Collaboration_at__c <= Now())}">
                    <apex:pageblock id="pbSupportDelayCollaboration" mode="maindetail">
                        <div style="height:110px">
                            <apex:pageblocksection id="pbsInput2" showheader="false" columns="1">
                                <!--<apex:outputtext escape="false" value="I have agreed with the Collaboration team to postpone collaboration. Clicking &quot;Delay Collaboration&quot; removes the case from the collaboration queue">
                                </apex:outputtext>-->
                                <apex:outputtext escape="false" value="To remove this case from the collaboration queue, agree with your manager and click 'Cancel Collaboration'. ">
                                </apex:outputtext>
                                
                                <apex:inputTextarea value="{!issueDescription}" styleclass="qlik-pub-action-inputTextarea" cols="88" rows="4" id="txtDelayIssueDescription" html-placeholder="Describe in short why there is no longer a need for collaboration." onkeyup="javascript: checkValueDelay();" style="overflow-x: hidden;" />
                            </apex:pageblocksection>
                        </div>
                        <div style="height:30px; line-height: 30px; padding: 9px; background:#f5f6f6; border: 1px solid #c7ccce; border-radius: 0 0 3px 3px; border-top-left-radius: 0; border-top-right-radius: 0; margin: 0">
                            <apex:outputpanel id="startCollaborationPanel" style="background: #f4f6f9;">
                                <apex:commandbutton id="btnDelayCollaboration" style="position:absolute; right:10px;" value="Cancel Collaboration" onclick="javascript: DisableCanCollaborationClick()" action="{!delayCollaboration}" oncomplete="checkValueRequest();"  rerender="headerPanel" />
                            </apex:outputpanel>
                        </div>
                    </apex:pageblock>
                </apex:outputpanel>
            </apex:outputpanel>
        </apex:form>
    </body>
</apex:page>