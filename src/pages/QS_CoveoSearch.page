<!----------------------------------------------------------------------------------------------------------
Purpose: Qlik Support Search Page
Created Date: Oct 2014
Change log:
2014-11-13 Layout and styling changes - WGO
2017-01-30 LCE-47 Add Login prompt for unauthenticated users - ext_bad
2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
2019-05-15 ext_vos CHG0036132: remove Coveo logos
2019-11-21 AIN IT-2320 Added filter expression for Coveo
-------------------------------------------------------------------------------------------------------------->
<apex:page showHeader="false" controller="QS_CoveoSearch" sidebar="false" title="Search Results" applyBodyTag="false" applyHtmlTag="false" docType="html-5.0" standardStylesheets="false">
    <head>
        <link rel="canonical" href="{!$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Login_Url__c}/QS_CoveoSearch"/>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    </head>
    <CoveoV2:JsSearch searchHub="QlikCommunitySearch" stylesheet="blabla.css" filter="{!CoveoFilterExpression}"/>
    <apex:composition template="QS_PageTemplate">
        <apex:define name="body">
            <section>
                <style>
                    /* remove coveo logo from loading animation */
                    .coveo-logo {
                      background-image: none;
                      background-position: 0 0;
                      background-size: auto;
                    }
                    /* remove coveo logo from list of results */
                    .CoveoLogo {
                      display: none !important; 
                    }
                </style>
                <div class="wrapper">
                    <apex:outputPanel rendered="{!loginUrl != null}">
                        <div style="padding-top: 15px; padding-left:30px; margin:0 auto 0 auto; max-width: 800px; color:grey">
                            Please <a href="{!loginUrl}">login</a> to get full access to all our Knowledge articles. <br/>
                        </div>
                    </apex:outputPanel>
                    <CoveoV2:SearchInterface searchHub="QlikCommunitySearch" filter="{!CoveoFilterExpression}"/>
                    <apex:stylesheet value="{!$Resource.QlikCoveoSearchCSS}"/>
                    <script type="text/javascript">
                        String.toLocaleString({
                            "en": {
                                ShowingResultsOf: "Result<pl>s</pl> {0}<pl>-{1}</pl> of about {2}"
                            }
                        });

                        Coveo.$("[id^=SearchInterface]").on('buildingQuery', function (e, args) {
                            var accountid = "{! userContact.id}";
                            var accountname = "{! userContact.name}";
                            var userid = "{!$User.id}";
                            var accountType = "{! accounttype}";
                            var advQuery = 'NOT @source=("kb","salesforce knowledge") OR (@source=("kb","salesforce knowledge") (@sfisvisibleinpkb=true';
                            if (accountType.toLowerCase() == "partner") {
                                advQuery += ' OR @sfisvisibleinprm=true';
                            }
                            else if (accountType.toLowerCase() == "customer") {
                                advQuery += ' OR @sfisvisibleincsp=true';
                            }

                            advQuery += "))";
                            args.queryBuilder.advancedExpression.add(advQuery);
                        });
                    </script>
                </div>
            </section>
            <c:QS_GetAnswers />
        </apex:define>
    </apex:composition>
</apex:page>