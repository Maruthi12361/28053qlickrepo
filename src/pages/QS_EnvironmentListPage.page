<!-----------------------------
    Author: MTM Initial development
    2014-09-26 customer portal project
    Change log:
    2014-10-28 Added divs for styling - WGO
    2014-11-17 Layout changes - WGO
    2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
    2019-11-21 AIN IT-2320 Added Coveo filter expression
 ------------------------------ -->
<apex:page cache="false"
    standardStylesheets="false"
    showHeader="false"
    sidebar="false"  
    language="{!$CurrentPage.parameters.l}"
    applyBodyTag="false"
    applyHtmlTag="false" controller="QS_EnvironmentController">

    <CoveoV2:JsSearch stylesheet="CoveoFullSearchNewDesign.css" filter="{!CoveoFilterExpression}"/>
    <apex:composition template="QS_PageTemplate">
        <apex:define name="body">
            <section>
                <div class="wrapper">
                    <div class="heading section-title">

                        <div>
                            <h1>Environments</h1>
                        </div>
                        <div class="CoveoSearchbox" data-enable-omnibox="true" data-placeholder="Search for answers" data-design="new" data-trigger-query-on-clear="false"></div>
                        <script type="text/javascript">
                            $(function () {
                                Coveo.initSearchbox(document.querySelector('.CoveoSearchbox'), "/apex/QS_CoveoSearch");
                            });
                        </script>
                        <script  type="text/javascript">
                            function bindEvent(el, eventName, eventHandler) {
                              if (el.addEventListener){
                                el.addEventListener(eventName, eventHandler, false); 
                              } else if (el.attachEvent){
                                el.attachEvent('on'+eventName, eventHandler);
                              }
                            }

                            function isie() {

                                var msie = window.navigator.userAgent.indexOf("MSIE ");

                                return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
                            }
                            bindEvent(document, 'DOMContentLoaded', function(){
                              var searchbox = document.querySelector('.CoveoSearchbox');
                              Coveo.$$(searchbox).on(Coveo.StandaloneSearchInterfaceEvents.beforeRedirect, function(e, args) {
                                    if (isie())
                                        args.searchPageUri = window.location.hostname + args.searchPageUri;
                              });
                            })
                        </script>
                    </div>
                    <div class="qs-page-environment qs-page">
                        <apex:form >
                            <apex:outputPanel id="environmentPanel">
                                <p>
                                    The Qlik Environment describes where the Qlik products are installed. 
                                    An environment is typically based on a server license and contains information about e.g. operating system, version number etc.
                                    <span class="vfHelpText">
                                        <apex:outputlink value="javascript:return false;">
                                            <img src="https://s3.amazonaws.com/qliksupport/img/icon-tooltip-questionmark.png" alt=""   />
                                            <span>When creating the environment give it a name that makes sense to you, e.g. “My production environment”. Make sure to always review (and update as needed) the environment details when creating a case against it.
                                                <br/><br/>
                                                   You can add other products to an environment, such as connectors.
                                                <br/><br/>
                                                An environment can be created/maintained here, or during the case creation process.                                       
                                            </span>
                                       </apex:outputlink>
                                    </span>
                                </p>  
                                <div class="qs-module">
                                    <c:QS_CreateEnvironment />
                                </div> 
                                <div class="qs-module">
                                    <c:QS_ListEnvironments />
                                </div>                         
                            </apex:outputPanel>
                        </apex:form>
                    </div>
                </div>
            </section>
        </apex:define>       
    </apex:composition>      
</apex:page>