/**************************************************
* CaseUtils
*
* Change Log:
* 2012-09-19 Alberto de Diego: CR# 5107
*        Initial Development
* 2012-11-20 SAN:CR#6714 adding Sales Ops record types to internal record types
* 2012-11-30 CCE:CR#6763 #Quick - Small change to CaseClosedSendSurvey trigger
*                       Added querySendSurveyRecordTypeIds()
* 2013-06-05 MTM:CR#7974 Added Sales Ops QlikHelp, ServiceOperations, 
*                        Article Feedback and SI/Partner Presales Support record types to internal record types
* 2014-10-06 CCE CR# 17120 rename ServiceOperations and add ConsultingServiceOperations
* 2016-05-09 OSO CR# 84047 Added Support Ops Requests to internal record types
* 2020-07-06 AIN IT-3097 Added ScheduleOnce record type to Case Closed Survey
**************************************************/

public with sharing class CaseUtils {
    
    public static Set<Id> queryInternalRecordTypeIds() {
        Set<String> recordTypeNames = 
        new Set<String>{'General IT Support Record Type','Hardware Request Record Type','New User Request Record Type',
                        'Software Request Record Type','Webmaster Support Layout','QlikMarket Case Record Type',
                        'Sales Ops APAC Requests','Sales Ops Americas Requests','Sales Ops Data',
                        'Sales Ops Internal Apps','Sales Ops Requests','Sales Ops Training','Sales Ops QlikHelp',
                        'EducationServicesOperations','Article Feedback','SI/Partner Presales Support','ConsultingServiceOperations','Support Ops Requests'};    //CR 17120
        //                'ServiceOperations','Article Feedback','SI/Partner Presales Support'};
        List<RecordType> internalRecordTypes = [SELECT Id FROM RecordType WHERE Name IN :recordTypeNames];
        Set<Id> internalRecordTypeIds = new Set<Id>();
        for (RecordType recordType : internalRecordTypes) internalRecordTypeIds.add(recordType.Id);
        return internalRecordTypeIds;
    }
    
    public static Set<Id> querySendSurveyRecordTypeIds() {
        Set<String> recordTypeNames = 
        new Set<String>{'QlikTech Master Support Record Type','QT Support Customer Portal Record Type','QT Support Partner Portal Record Type','ScheduleOnce Support Record Type'};
        List<RecordType> sendSurveyRecordTypes = [SELECT Id FROM RecordType WHERE Name IN :recordTypeNames];
        Set<Id> sendSurveyRecordTypeIds = new Set<Id>();
        for (RecordType recordType : sendSurveyRecordTypes) sendSurveyRecordTypeIds.add(recordType.Id);
        return sendSurveyRecordTypeIds;
    }
}