/********************************************************
* CLASS: ConvertInvitedUsers
* DESCRIPTION: Hanldes Lead Conversion for Invited Users on a trial
*
*
* CHANGELOG:
*   2019-10-17 - BAD - BMW-1792 - Added Initial logic
*********************************************************/
public class ConvertInvitedUsers {
    
    public ConvertInvitedUsers() {
    }

    public static void convertLeads() {
        list<Id> leadIDs = new list<Id>();
        map<Id, Product_Trial__c> mapLeadToTrial = new map<Id, Product_Trial__c>();
        list<Product_Trial__c> TrialsConverted = new list<Product_Trial__c>();
        String msg = '';

        system.debug('Start Lead conversion for Invited Users');         

        //Get product trials which have the Pending Conversion flag set to True
        for (Product_Trial__c trial : [select id, Name, Contact_Account__c, Contact_Account__r.Navision_Status__c, Contact_Account__r.AccountSource, Contact_Account__r.RecordTypeID, Contact_Account__r.OwnerId, Contact_Account__r.E_Mail_Domain__c, Pending_Conversion__c, 
                                        (select name, Lead__c, Lead__r.Email from Product_Users__r where Status__c = 'Active' and Lead__c != null) 
                                            from Product_Trial__c where Pending_Conversion__c = true LIMIT 10])
        {
            //Check limit for Lead Conversion, we don't want to exceed 100 Leads 
            if((leadIDs.Size() + trial.Product_Users__r.size()) > 100){
                break; 
            }

            if(!TrialsConverted.contains(trial)) {TrialsConverted.add(trial);}

            //Validate Lead
            for(Product_User__c pu : trial.Product_Users__r){
                //Check if Account belongs to a Customer or Partner, then we need to make sure the Lead has the correct email domain
                Boolean isLeadValid = true;
                if(string.isNotBlank(trial.Contact_Account__r.Navision_Status__c) && trial.Contact_Account__r.Navision_Status__c == 'Partner'){
                    isLeadValid = IsEmailDomainMatched(pu.Lead__r.Email, trial.Contact_Account__r.E_Mail_Domain__c);
                }
                else if(string.isNotBlank(trial.Contact_Account__r.Navision_Status__c) &&  trial.Contact_Account__r.Navision_Status__c == 'Customer' && trial.Contact_Account__r.AccountSource != 'Trial Conversion'){
                    isLeadValid = IsEmailDomainMatched(pu.Lead__r.Email, trial.Contact_Account__r.E_Mail_Domain__c);
                }                
                if(isLeadValid){
                    leadIDs.add(pu.Lead__c);
                    if(!mapLeadToTrial.containsKey(pu.Lead__c)) {mapLeadToTrial.put(pu.Lead__c, trial);} 
                }
            }
        }

        //Prep Leads for conversion
        list<Lead> leadsToConvert = [select id, name, Owner.Type, RecordTypeID from Lead where Id in :leadIDs and isConverted = false];
        system.debug('Leads to Convert: ' + leadsToConvert);
       
        try {
            list<Database.LeadConvert> leadConverts = new list<Database.LeadConvert>();
            for(Lead l : LeadsToConvert){
                Database.LeadConvert lc = new database.LeadConvert();
                lc.setLeadId(l.Id);
                lc.setAccountId(mapLeadToTrial.get(l.Id).Contact_Account__c);
                lc.convertedStatus = 'Lead - Converted';
                lc.setDoNotCreateOpportunity(true);
                if(l.Owner.Type == 'Queue'){
                    lc.setOwnerId(mapLeadToTrial.get(l.Id).Contact_Account__r.OwnerId); //Grab the Account owner if the Lead was assigend to an OM queue
                }
                else {
                    lc.setOwnerId(l.OwnerId);
                }
                leadConverts.add(lc);
            }
        
            //Bypass Dupe Rules
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.AllowSave = true;

            //Start conversion
            Database.LeadConvertResult[] lcrList = Database.convertLead(leadConverts, dml);    
            for(Database.LeadConvertResult lcr : lcrList){
                if(!lcr.isSuccess()){
                    //Remove Trial from list so we don't reset the coversion flag on the Trial record
                    Product_Trial__c trialToRemove = mapLeadToTrial.get(lcr.getLeadId());
                    if(TrialsConverted.contains(trialToRemove)){
                        TrialsConverted.remove(TrialsConverted.indexOf(trialToRemove));
                    }
                    //Build error message for email
                    msg += 'Lead Conversion Error - LeadID: ' + lcr.getLeadId() + '<br/>';
                    for(Database.Error err: lcr.getErrors()){  
                        msg += err.statusCode + ': ' + err.message + '<br/><br/>';
                    }                     
                }
                else {
                    system.debug('Lead converted: ' + lcr.getLeadId() + ' to Contact: ' + lcr.getContactId());
                }
            }
        } catch (Exception ex) {
            msg += 'Lead Conversion Error (TC):';
            msg += ex.getMessage();
            System.debug('Lead Conversion Error (TC): ' + ex.getMessage());
        }                

        //Send Exception email
        if(string.isNotBlank(msg)) {
            sendEmail(msg);
            system.debug('Lead Conv Error:' +  msg);
        }
        
        //Reset Pending Conversion flag on Trials         
        for(Product_Trial__c trial : TrialsConverted){
            trial.Pending_Conversion__c = false;
        }
        update TrialsConverted;
       
        //Update Zuora Subscription ID on Invited Users
        if(TrialsConverted.Size() > 0){
            updateProductUsersWithSubsriptionID(TrialsConverted);
        }

    }

    public static void updateProductUsersWithSubsriptionID(list<Product_Trial__c> TrialsConverted){
        Set<Id> TrialIDs = new Set<Id>();
        Map<String,Product_User__c> mapProductUserSAO = new Map<String,Product_User__c>();
        List<Product_User__c> productUsersToUpdate = new List<Product_User__c>();

        for (Product_Trial__c trial : TrialsConverted) {
            TrialIDs.add(trial.id);  
        }        

        //Retrieve Product Users from Trials
        List<Product_User__c> ProductUsers = [SELECT Id, User_Role__c, Zuora_Subscription_ID__c, Product_Trial__c FROM Product_User__c WHERE Product_Trial__c in :TrialIDs];

        //Get the Service Account Owners, records will have the ZuoraSubscriptionID
        for (Product_User__c pu : ProductUsers) {
            if(pu.User_Role__c == 'Service Account Owner' && !mapProductUserSAO.containsKey(pu.Product_Trial__c)){
                mapProductUserSAO.put(pu.Product_Trial__c, pu);
            }
        }

        //Get the Invited users and set Zuora Subscription ID from SAO
        for (Product_User__c pu : ProductUsers) {
            if(pu.User_Role__c == 'Invited' && mapProductUserSAO.containsKey(pu.Product_Trial__c)){
                if(mapProductUserSAO.get(pu.Product_Trial__c).Zuora_Subscription_ID__c != null ){
                    pu.Zuora_Subscription_ID__c = mapProductUserSAO.get(pu.Product_Trial__c).Zuora_Subscription_ID__c;
                }
                else {
                    pu.Zuora_Subscription_ID__c = '';
                }
                productUsersToUpdate.add(pu);
            }
        }        
        
        //Update
        System.debug('Product Users to Update: ' + productUsersToUpdate);
        if(productUsersToUpdate.size() > 0){
            Update productUsersToUpdate;
        }

    }

    public static void sendEmail(string emailBody){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.addAll((Label.InvitedUsersConversionEmail).split(';'));
        email.setToAddresses(sendTo);
        email.setSubject('Invited Users - Lead Conversion Error(s)');     
        email.setHtmlBody(emailBody);
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });            
        }
        catch (system.EmailException ex) {
            // If the sending of the email fails, due to non existing
            // email address, do nothing
        }        
        
    }

    public static Boolean IsEmailDomainMatched(string leadEmail, string emailDomain){
        if(string.isNotBlank(emailDomain) && string.isNotBlank(leadEmail)){
            string leadEmailDomain = leadEmail.right(leadEmail.length() - leadEmail.lastIndexOf('@'));
            leadEmailDomain = leadEmailDomain.replace('@', '');
            if(emailDomain.equalsIgnoreCase(leadEmailDomain))
                return true;
        }
        return false;
    }

   
}