// CR# 10936
// test class to test OpportunityPDFCreator, OpportunityAuditPDFTemplateCtrl and visualforce page: Opportunity_Audit_PDF_Template
// Change log:
// February 04, 2013 - Initial Implementation - Madhav Kakani - Fluido Oy
//07.02.2017   RVA :   changing methods
//       24.03.2017 Rodion Vakulovskyi 
@isTest
private class OpportunityAuditPDFCreatorTest {
    static testMethod void testOpportunityAuditPDFTemplateCtrl() {

        QTTestUtils.GlobalSetUp();
		Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
       // User mockSysAdmin = Util.createUser('TstSAdminNme', 'Testab1', testProfile.Id, testUserRole.Id);
       // insert mockSysAdmin;
        QTTestUtils.SetupNSPriceBook('GBP');
		User userTest = [Select id From User where id =: UserInfo.getUserId()];
		
		/*Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c qtc = new QlikTech_Company__c();
        qtc.Name = 'SWE';
        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
        qtc.Country_Name__c = 'Sweden';
        qtc.CurrencyIsoCode = 'SEK';
		qtc.Subsidiary__c = testSubs1.id;
        insert qtc;*/
		QlikTech_Company__c qtc = QTTestUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'Sweden');
			qtc.Country_Name__c = 'Sweden';
			qtc.CurrencyIsoCode = 'SEK';
		update qtc;
			
		QlikTech_Company__c qtc2 = QTTestUtils.createMockQTCompany('QlikTech Inc', 'USA', 'United States');
			   qtc2.Country_Name__c = 'USA';
			   qtc2.CurrencyIsoCode = 'USD'; 
		update qtc2;
		/*
        QlikTech_Company__c qtc2 = new QlikTech_Company__c();
        qtc2.Name = 'USA';
        qtc2.QlikTech_Company_Name__c = 'QlikTech Inc';
        qtc2.Country_Name__c = 'USA';
        qtc2.CurrencyIsoCode = 'USD'; 
		qtc2.Subsidiary__c = testSubs1.Id;
        insert qtc2;
		*/
        
       
        System.RunAs(userTest) {   
           
        
            Account act = new Account(name='Test Account');
            act.OwnerId = userTest.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry ='United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry ='United States';
            act.ShippingCity = 'SanFrancisco';           
            act.Account_Creation_Reason__c = 'Test';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
            
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
                
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
			Test.startTest();
            //Create test Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp',
                Name = 'TestOpp',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = rt.Id,
                CurrencyIsoCode = 'GBP',
                AccountId = ct.AccountId,
                Signature_Type__c = 'Digital Signature',
                ForecastCategoryName = 'Omitted'
            );
            insert testNewOpp;
            Test.stopTest();
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;         
            insert LineItem;    
            
            system.assert(testNewOpp.Id != null);

          
            
            String sResp = OpportunityPDFCreator.CreateAuditPDF(testNewOpp.Id, testNewOpp.Name,'http://salesforce.com/'+testNewOpp.Id);
            String pdfId = [SELECT Id from Attachment a where a.ParentId = :testNewOpp.Id LIMIT 1].Id;
            system.assert(sResp == pdfId);

            // Test the OpportunityAuditPDFTemplateCtrl
            PageReference pdf = Page.Opportunity_Audit_PDF_Template;        
            pdf.getParameters().put('id', testNewOpp.id);
            pdf.getParameters().put('detail_url', 'http://salesforce.com/'+testNewOpp.Id);
            Test.setCurrentPageReference(pdf);

            ApexPages.StandardController sc = new ApexPages.standardController(testNewOpp);
            OpportunityAuditPDFTemplateCtrl pdf_x = new OpportunityAuditPDFTemplateCtrl(sc);
            system.assert(pdf_x.showItems1 == true);
            List<Non_Licence_Related_ProductsCO_Non_Li__c> listOfTEst = pdf_x.lNRPItems;                
             
        }
    } // testOpportunityAuditPDFTemplateCtrl  
    
} // class OpportunityAuditPDFCreatorTest