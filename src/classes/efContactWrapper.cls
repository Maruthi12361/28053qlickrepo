//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efContactWrapper {
    private Contact c;
    private Account a;
    
    public efContactWrapper(Contact c){
        this.c = c;
        if(c!=null && c.Account!=null) 
            this.a = c.Account;
    }
    
    public efContactWrapper(){
        this.c = new Contact();
    }
    
    public Contact getContact(){
        return c;
    }
    
    public String getFirstName(){        
        return efUtility.giveValue(c.FirstName);
    }
    
    public void setFirstName(String firstName){
        c.FirstName = firstName;
    }
    
    public String getTitle(){         
        return efUtility.giveValue(c.Title);
    }
    
    public void setTitle(String title){        
        c.Title = title;
    }
    
    public String getMailingStreet(){        
        return efUtility.giveValue(c.MailingStreet);
    }
    
    public void setMailingStreet(String ma) {       
        c.MailingStreet = ma;
    }
    
    public String getMailingCity(){        
        return efUtility.giveValue(c.MailingCity);
    }
    
    public void setMailingCity(String mc){      
        c.MailingCity = mc;
    }
    
    public String getMailingState(){        
        return efUtility.giveValue(c.MailingState);
    }
    
    public void setMailingState(String ms){      
        c.MailingState = ms;
    }
    
    public String getMailingCountry(){        
        return efUtility.giveValue(c.MailingCountry);
    }
    
    public void setMailingCountry(String mco){        
        c.MailingCountry = mco;
    }
    
    public String getMailingPostalCode(){        
        return efUtility.giveValue(c.MailingPostalCode);
    }
    
    public void setMailingPostalCode(String mzc){        
        c.MailingPostalCode = mzc;
    }
    
    public String getJobFunction(){        
        return efUtility.giveValue(c.Job_Function__c);
    }
    
    public void setJobFunction(String jobFunction){        
        c.Job_Function__c = JobFunction;
    }
    
    public String getJobTitle(){        
        return efUtility.giveValue(c.Job_Title__c);
    }
    
    public void setJobTitle(String jobTitle){        
        c.Job_Title__c = jobTitle;
    }
    
    public String getJobLevel(){        
        return efUtility.giveValue(c.Job_Level__c);
    }
    
    public void setJobLevel(String jobLevel){       
        c.Job_Level__c = JobLevel;
    }
    
    public String getLastName(){        
        return efUtility.giveValue(c.LastName);
    }
    
    public void setLastName(String lastName){        
        c.LastName = lastName;
    }
    
    public String getEmail(){        
        return efUtility.giveValue(c.Email);
    }
    
    public void setEmail(String email){        
        c.Email = email;
    }
    
    public String getContactPhone(){        
        return efUtility.giveValue(c.Phone);
    }
    
    public void setContactPhone(String contactPhone){
        c.Phone = contactPhone;
    }
    
    public string getCardEmailHotel(){        
        return efUtility.giveValue(c.Email);
    }
    
    public String getNameToDisplay(){
        return efUtility.getWrapString(getFirstName() + ' ' + getLastName(),30);
    }
    
    public string getCardEmail(){
        return efUtility.giveValue(c.Email);
    }
    
    public Account getAccount(){
        return a;
    }
    
    public String getAccountName(){
        return a.Name;
    }
}