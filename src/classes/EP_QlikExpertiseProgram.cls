public with Sharing class EP_QlikExpertiseProgram {
    /*The only condition where steps 2 – 4 are visible is when the logged in user has the PRMDT/IND/JAPAN privileges and
     * there is one or more “Approved” Expertise Application
     * */
    public Boolean isPRMBase {get; set;}
    public Boolean isPartner {get; set;}
    public Boolean isDTorIND {get; set;}
    public Boolean isAdmin {get; set;}
    public Boolean isOpAdmin {get; set;}
    public Account acc {get; set;}

    public EP_QlikExpertiseProgram()
    {
        InitProfileSettings();
    }

    public void InitProfileSettings()
    {
        acc = new Account();
        HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
        User currUser=[select AccountId,ContactId,FirstName,Id, UserRoleId from User where id=:System.UserInfo.getUserId()];
        acc.Id =currUser.AccountId;
        String ProfileId = UserInfo.getProfileId();

        List<Profile> ProfileList = [Select Name FROM Profile where Id=:ProfileId];
        String ProfileName = ProfileList.size()>0?''+ProfileList.get(0):'';
        //PRM-BASE profile
        isPRMBase =  String.isNotBlank(ProfileName) && ProfileName.contains('PRM - Base')? true: false;
        isPartner = String.isNotBlank(ProfileName) && ProfileName.Contains('PRM')?true:false;
        isDTorIND = isPartner && !isPRMBase;
        //System Administrator = 00e20000000yyUzAAI
        isAdmin = settings.Sys_Admin_ProfileId__c.contains(ProfileId)?true:false;
        //Operations Administrator = 00e20000001ODnH
        isOpAdmin = ProfileId.contains('00e20000001ODnH')?true:false;
    }

    public Boolean ExpertiseAreaExists()
    {
    	List<Partner_Expertise_Area__c> pe = [Select p.Partner_Account_Name__c,
    	                                             p.Name,
    	                                             p.Id From Partner_Expertise_Area__c p
    	                                             where Partner_Account_Name__c =: acc.Id
    	                                             and Expertise_Application_Eligibility__c = 'Accepted'];
        return pe.size() > 0? true:false;

    }
    public Boolean PEVisibility
    {
        get{

            return (!isPRMBase) && ExpertiseAreaExists();
        }
    }

}