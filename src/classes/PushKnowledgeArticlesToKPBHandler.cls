/**
* PushKnowledgeArticlesToKPBHandler
* 
* Description:  Handles Knowledge Articles - push to the public knowledge base: unpublish existed article, create and publish draft.
* Added:        11-07-2018 - ext_vos - CHG0034282
*
* Change log:
* 09-11-2018 - ext_vos - CHG0034844 - update conditions for scheduled publishing; add handler for article triggers; add methods for "manual publishing".
* 14-03-2019 - ext_bad - CHG0035668 - update channels population.
* 28-10-2019 - AIN     - IT-2229 - Exclude attunity articles from being published
* 13-07-2020 - ext_cqb - IT-2939 disable automation which sets IsVisibleInPKB
*/
public class PushKnowledgeArticlesToKPBHandler {

    public static final Datetime DATE_RANGE = Datetime.newInstance(2018, 01, 01);
    public static final String SEPARATOR = ',';
    public static final Map<String,String> ARTICLE_TYPES = new Map<String,String>{'Diagnostic__kav' => 'Diagnostic', 'Basic__kav' => 'Basic'};

    public static Boolean TEST_EMAIL_SENDING = false;

    public static String findActualArticleIds(String articleType) {
        String ids = '';
        try {
            boolean excludeAttunity = getExcludeAttunity();
            List<Id> idsToExlude = new List<Id>();
            if(excludeAttunity){
                List<SObject> excludeIds = Database.query('select id from ' + articleType + ' With DATA CATEGORY Attunity_Data_Categories__c Below Attunity_Product__c');
                if(!excludeIds.isEmpty()){
                    for (SObject a : excludeIds) {
                        idsToExlude.add((ID)a.get('Id'));
                    }
                }
            }

            String queryStr = 'select id, KnowledgeArticleId from ' + articleType 
                                + ' where PublishStatus = \'Online\' ' 
                                + ' and IsVisibleInCsp = true '
                                + ' and IsVisibleInPkb = false '
                                + ' and ArticleCaseAttachCount > 0';
            if(!idsToExlude.isEmpty()){
                boolean first = true;
                queryStr = queryStr + ' and Id not in (';
                for(string id : idsToExlude) {
                    if(!first)
                        queryStr = queryStr + ',';
                    queryStr = queryStr + '\'' + id + '\'';
                    first = true;
                }
                queryStr = queryStr + ')';
            }
            System.debug('Querystr: ' + queryStr);
            List<SObject> allArticles = Database.query(queryStr);

            if (!allArticles.isEmpty()) {
                List<String> kaIds = new List<String>();
                for (SObject a : allArticles) {
                    kaIds.add((ID)a.get('KnowledgeArticleId'));
                }                                   
                List<AggregateResult> attachmentInfo = [select MIN(CreatedDate) date, KnowledgeArticleId kaId
                                                        from CaseArticle 
                                                        where KnowledgeArticleId in : kaIds 
                                                        group by KnowledgeArticleId];
                Set<String> actualIds = new Set<String>();
                for (AggregateResult att : attachmentInfo) {
                    if (checkIsActual(att)) {
                        actualIds.add((String) att.get('kaId'));
                    }
                }                
                for (SObject ar : allArticles) {
                    if (actualIds.contains((ID)ar.get('KnowledgeArticleId'))) {
                        if (ids != '') {
                            ids += SEPARATOR;
                        }
                        ids += '\'' + ar.Id + '\'';
                    }
                }        
            }  
        } catch (Exception ex) {
            generateErrorReport(articleType, ex.getMessage());
            System.debug('#################### findActualArticleIds error: ' + ex.getMessage());
        }
        return ids;  
    }

    public static String findArticleIdsByNumbers(String articleType, List<String> articleNumbers) {
        String ids = '';        
        try {
            String articleNumberString = '';
            for (String num : articleNumbers) {               
                if (articleNumberString != '') {
                    articleNumberString += SEPARATOR;
                }
                articleNumberString += '\'' + num + '\'';            
            }
            String queryStr = 'select id, KnowledgeArticleId from ' + articleType 
                                + ' where PublishStatus = \'Online\' ' 
                                + ' and ArticleNumber in (' + articleNumberString + ') '
                                + ' and IsVisibleInPkb = false ';
            List<SObject> allArticles = Database.query(queryStr);
            for (SObject ar : allArticles) {               
                if (ids != '') {
                    ids += SEPARATOR;
                }
                ids += '\'' + ar.Id + '\'';            
            }
        } catch (Exception ex) {
            generateErrorReport(articleType, ex.getMessage());
            System.debug('#################### findArticleIdsByNumbers error: ' + ex.getMessage());
        }
        return ids;  
    }

    public static Set<String> publish(List<SObject> articles, String articleType) {
        Set<String> errorArticleNumbers = new Set<String>();
        
        if (!String.isEmpty(articleType)) {
            // unpublish existed acticle and create draft for it
            String draftIds = '';  
            String draftId = '';             
            for (SObject art : articles) {
                try {
                    draftId = KbManagement.PublishingService.editOnlineArticle((ID)art.get('KnowledgeArticleId'), true);
                    if (draftIds != '') {
                        draftIds += ',';
                    }
                    draftIds += '\'' + draftId + '\'';
                } catch (Exception ex) {
                    errorArticleNumbers.add((String)art.get('ArticleNumber'));
                    System.debug('#################### editOnlineArticle error: ' + ex.getMessage());
                }           
            }                    
            // update draft and publish it
            if (!String.isEmpty(draftIds)) {
                String queryStr = 'select id, KnowledgeArticleId, ArticleNumber from ' + articleType + ' where Id in (' + draftIds + ')';
                List<SObject> articlestoPublish = Database.query(queryStr);

                if (!articlestoPublish.isEmpty()) {
                    for (SObject item : articlestoPublish) {
                        item.put('IsVisibleInPkb', true);
                    }
                    update articlestoPublish;                    

                    for (SObject item1 : articlestoPublish) {
                        try {
                            KbManagement.PublishingService.publishArticle((ID)item1.get('KnowledgeArticleId'), true);
                        } catch (Exception ex) {
                            errorArticleNumbers.add((String)item1.get('ArticleNumber'));
                            System.debug('#################### publishArticle error: ' + ex.getMessage());
                        }
                    }
                }
            }            
        }
        return errorArticleNumbers;
    }    

    public static void generateReport(String articleType, String allArticleIds, Set<String> failedArticles) {
        // add debug info
        if (failedArticles.isEmpty()) {
            System.debug('#################### ' + ARTICLE_TYPES.get(articleType) + ' job was successful');
        } else {
            System.debug('#################### ' + ARTICLE_TYPES.get(articleType) + ' errors occured for next articles: ' + failedArticles);
        }
        // inform users
        if (!String.isEmpty(allArticleIds)) {            
            // calculate all handled articles
            String queryAll = 'select id, ArticleNumber from ' + articleType  + ' where Id in (' + allArticleIds + ')';
            List<SObject> all = Database.query(queryAll);
            Set<String> successNumbers = new Set<String>();
            for (SObject ar : all) {
                if ((String)ar.get('ArticleNumber') != null) {
                    successNumbers.add((String)ar.get('ArticleNumber'));
                }
            }
            Integer count = successNumbers.size();
            
            // select successful articles
            if (!failedArticles.isEmpty()) {
                successNumbers.removeAll(failedArticles);
            }            
            // generate report email
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                
            
            String fromId = getFromAddresses();
            if (!String.isEmpty(fromId)) {
                mail.setOrgWideEmailAddressId(fromId);
            }    
            mail.setToAddresses(getToAddresses());
            mail.setSubject('Publishing of ' + ARTICLE_TYPES.get(articleType) + ' knowledge articles to PKB was completed.');            
            String htmlBody = 'Batch Process has handled ' + count + ' knowledge article(s).';

            // add successful items            
            if (!successNumbers.isEmpty()) {
                htmlBody += '<br/><br/>Successful articles (' + successNumbers.size() + ' items): <br/><ul>';
                for (String num : successNumbers) {
                    htmlBody += '<li>' + num + '</li>';
                }
                htmlBody += '</ul>';
            }           
            // add failed items
            
            if (!failedArticles.isEmpty()) {
                htmlBody += '<br/><br/>Failed articles (' + failedArticles.size() + ' items): <br/><ul>';
                for (String num2 : failedArticles) {
                    htmlBody += '<li>' + num2 + '</li>';
                }
                htmlBody += '</ul>';
            }
            htmlBody += '<br/><br/>Best regards,<br/>Qlik Support<br/>';
            mail.setHtmlBody(htmlBody);
            mails.add(mail); 

            if (!mails.isEmpty()) {
                if (Test.isRunningTest()) {
                    TEST_EMAIL_SENDING = true;
                }
                Messaging.sendEmail(mails);
            }
        }
    }

    public static void generateErrorReport(String articleType, String errorMessage) {
        // add debug info
        if (!String.isEmpty(errorMessage)) {
            System.debug('#### Something went wrong during finding the ' + ARTICLE_TYPES.get(articleType) + ' actual articles. Error message: ' + errorMessage);
        }
        // inform users
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                
        
        String fromId = getFromAddresses();
        if (!String.isEmpty(fromId)) {
            mail.setOrgWideEmailAddressId(fromId);
        }            
        mail.setToAddresses(getToAddresses());
        mail.setSubject('Publishing of ' + ARTICLE_TYPES.get(articleType) + ' knowledge articles to PKB was completed with error.');            
        mail.setHtmlBody('There was error during batch process: ' + errorMessage + '<br/><br/>Best regards,<br/>Qlik Support<br/>');
        mails.add(mail); 

        if (!mails.isEmpty()) {
            if (Test.isRunningTest()) {
                TEST_EMAIL_SENDING = true;
            }
            Messaging.sendEmail(mails);
        }
    }

    public static void articleChannelsPopulation(List<SObject> articles, string articleType) {
        
        boolean excludeAttunity = getExcludeAttunity();
        List<Id> idsToExlude = new List<Id>();
        if(excludeAttunity){
            string query = 'select id from ' + articleType + ' WHERE PublishStatus=\'draft\' With DATA CATEGORY Attunity_Data_Categories__c Below Attunity_Product__c';
            system.debug('Query 1: ' + query);
            List<SObject> excludeIds = Database.query(query);
            if(!excludeIds.isEmpty()){
                for (SObject a : excludeIds) {
                    idsToExlude.add((ID)a.get('Id'));
                }
            }
            query = 'select id from ' + articleType + ' WHERE PublishStatus=\'online\' With DATA CATEGORY Attunity_Data_Categories__c Below Attunity_Product__c';
            system.debug('Query 2: ' + query);
            excludeIds = Database.query(query);
            if(!excludeIds.isEmpty()){
                for (SObject a : excludeIds) {
                    idsToExlude.add((ID)a.get('Id'));
                }
            }
        }
        System.debug('Ids to exclude: ' + idsToExlude);
                    
        if (articles != null && !articles.isEmpty()) {
            for (SObject item : articles) {
                system.debug('Id: ' + (Id)item.get('Id'));
                if (!idsToExlude.Contains((Id)item.get('Id'))){
                    system.debug('Why are you here?');
                    if ((Boolean)item.get('IsVisibleInPkb')) {
                        item.put('IsVisibleInCsp', true);
                        item.put('IsVisibleInPrm', true);
                    } else if ((Boolean)item.get('IsVisibleInCsp')) {
                        //item.put('IsVisibleInPkb', true);
                        item.put('IsVisibleInPrm', true);
                    }
                }
            }
        }
    }

    private static  Boolean checkIsActual(AggregateResult ar) {
        Boolean isActual = false; 
        try {
            Datetime createdDate = (Datetime) ar.get('date');
            isActual = createdDate > DATE_RANGE;
        } catch (Exception ex) {
            System.debug('############## PushKnowledgeArticlesToKPBHandler.checkIsActual() - Smth went wrong for next article: ' + ar);
        }
        return isActual;
    }

    private static List<String> getToAddresses() {
        List<String> addresses = new List<String>();
        QTCustomSettings__c all = QTCustomSettings__c.getvalues('Default');
        if (all != null && !String.isEmpty(all.PushKnowledgeArticlesEmail__c)) {
            addresses = all.PushKnowledgeArticlesEmail__c.split(SEPARATOR);
        } 
        return addresses;
    }

    private static String getFromAddresses() {
        String id = '';        
        String qlikNoreply = '';
        if (QTCustomSettings__c.getInstance('Default') != null) {
            qlikNoreply = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
        }
        OrgwideEmailAddress[] orgwideaddress = [select Id from OrgwideEmailAddress where Displayname = :qlikNoreply Limit 1]; 
        if (!orgwideaddress.isEmpty()) {
            id = orgwideaddress[0].Id;
        }
        return id;
    }
    private static boolean getExcludeAttunity(){
        QS_Partner_Portal_Urls__c cs = QS_Partner_Portal_Urls__c.getInstance();
        boolean excludeAttunity = cs.ExcludeAttunityProductOnPublishToPKB__c;
        return excludeAttunity;
    }
}