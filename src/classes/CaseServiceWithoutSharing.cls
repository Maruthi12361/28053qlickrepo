/**********************************************************
* Class: CaseServiceWithoutSharing
* Description: To set necessary Case.BusinessHours during creation\updating, user should have Read (at least) access to Entitlments. 
*				To allow this logic for Guest Portal users, this class has 'without sharing' setting.
*
* Changes Log:
*   2018-12-17	ext_vos	INC0154629: move fixSeverity3 logic from CaseService class.
*   2019-12-26 extbad IT-2363 Added Signature Support Entitlement Process
*   2020-21-02 extbad IT-2497 Add Legacy Enterprise Support Entitlement Process
*************************************************************/
public without sharing class CaseServiceWithoutSharing {
    //aaron added 1 method to fix severity 3 business hours on Cases
    //alberto: changed this method for CR# 5160  
    public static void fixSeverity3 (List<Case> cases) {
        if (cases.isEmpty()) {
        	return;
        }         
        Set<Id> entitlementIds = new Set<Id>();
        for (Case c : cases) {
        	entitlementIds.add(c.EntitlementId);
        }      
        Map<Id, Entitlement> entitlements = 
            new Map<Id, Entitlement>([SELECT Id, AccountId, BusinessHoursId, SlaProcessId FROM Entitlement WHERE Id IN :entitlementIds]);
        
        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        SlaProcess standardSla = [SELECT Id, Name FROM SlaProcess WHERE Name =: es.Standard_Entitlement_Process_Name__c LIMIT 1];
        Id twentyFourSevenBusinessHoursId = [SELECT Id FROM BusinessHours WHERE Name =: es.X24x7_Business_Hours_Name__c LIMIT 1].Id;
        
        for (Case c : cases) {
            Entitlement entitlement = entitlements.get(c.EntitlementId);
            if (entitlement != null) {
                // If Enterprise AND Severity 1 then Case.Business Hours = 24x7
                // If Enterprise AND Severity 2 or 3 then Case.Business Hours = Entitlement.Business Hours
                // If Basic then Case.Business Hours = Entitlement.Business Hours
                if ((entitlement.SlaProcessId != standardSla.Id && (c.Severity__c == '3' || c.Severity__c == '2'))
                        || entitlement.SlaProcessId == standardSla.Id) {
                    c.BusinessHoursId = entitlement.BusinessHoursId;
                } else if (entitlement.SlaProcessId != standardSla.Id && c.Severity__c == '1') {
                    c.BusinessHoursId = twentyFourSevenBusinessHoursId;
                }
            }              
        }          
    }
}