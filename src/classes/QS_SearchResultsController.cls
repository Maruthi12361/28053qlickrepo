/*-----------------------------
* Change Log:
* 05-08-2018 - ext_vos - Outdated page. Add redirect to actual search page: QS_CoveoSearch
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 -----------------------------*/
public with sharing class QS_SearchResultsController {

    public QS_SearchResultsController() {
    }
    
    public PageReference redirectToActualSearchPage() {
        PageReference retURL = new PageReference('/apex/QS_CoveoSearch');
        retURL.setRedirect(true);
        return retURL;
    }  
}