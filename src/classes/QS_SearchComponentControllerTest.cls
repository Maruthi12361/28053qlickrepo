/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
public with sharing class QS_SearchComponentControllerTest {
    @isTest
    static void TestJive() {

        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();

        
        List<ULC_Level__c> levels = new List<ULC_Level__c>();

        ULC_Level__c level1 = new ULC_Level__c();
        level1.Name = 'CPLOG';
        level1.Status__c = 'Active';
        levels.add(level1);

        ULC_Level__c level2 = new ULC_Level__c();
        level2.Name = 'CPVIEW';
        level2.Status__c = 'Active';
        levels.add(level2);
        insert levels;

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if (searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Page_Size__c = 20;
            ss.Jive_DefaultPlaces__c ='/places/217522';
            ss.Jive_EndPoint__c ='https://community.qlik.com/api/core/v3/search/contents?startIndex=index&callback=?&count=pageSize&filter=search(searchTerms)';
            ss.Instance_Name__c = 'cs86';
            ss.Jive_EndPoint_Personas__c = 'https://community.qlik.com/api/core/v3/contents/trending?&callback=?&count=pageSize&startIndex=index&filter=place(placeTerms)';
            insert ss;
        }

        QS_Search_Setting__c ss1 = new QS_Search_Setting__c();
        ss1.Name = 'Jive_Search';
        ss1.Active__c = true;
        insert ss1;

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker;Business User';
        insert testContact;

        String profileId = [select id from Profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1].Id;

        

        List<Assigned_ULC_Level__c> assignedLevels = new List<Assigned_ULC_Level__c>();
        Assigned_ULC_Level__c assignedULCLevel1 = new Assigned_ULC_Level__c();
        assignedULCLevel1.ContactId__c = testContact.Id;
        assignedULCLevel1.Status__c = 'Approved';
        assignedULCLevel1.ULCLevelId__c = level1.Id;
        assignedLevels.add(assignedULCLevel1);

        Assigned_ULC_Level__c assignedULCLevel2 = new Assigned_ULC_Level__c();
        assignedULCLevel2.ContactId__c = testContact.Id;
        assignedULCLevel2.Status__c = 'Rejected';
        assignedULCLevel2.ULCLevelId__c = level2.Id;
        assignedLevels.add(assignedULCLevel2);
        insert assignedLevels;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        QS_JiveSearchHTTPMock mock = QS_JiveSearchHTTPMock.CreateHTTPMock(15);
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();

        QS_SearchComponentController controller = new QS_SearchComponentController();
        controller.searchString = 'Hello';
        controller.searchType = 'JIVE';
        controller.numberResults = 2;
        controller.personasBased = false;
        controller.getSearchResultSet();

        //Authenticated
        system.runas(communityUser)
        {
            controller = new QS_SearchComponentController();
            controller.searchString = 'Hello';
            controller.searchType = 'JIVE';
            controller.numberResults = 2;

            controller.personasBased = false;
            controller.getSearchResultSet();
            
            controller.personasBased = true;
            controller.getSearchResultSet();
        }

        QS_JiveSearch js = new QS_JiveSearch('Hello', 1, true);
        js.getPageNumber();
        js.getDisableNext();
        js.getTotalPages();
        js.pageSize = 10;
        js.getDisableNext();
        js.getTotalPages();
        Test.stopTest();
    }
    @isTest
    static void TestLithium() {

        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();

        
        List<ULC_Level__c> levels = new List<ULC_Level__c>();

        ULC_Level__c level1 = new ULC_Level__c();
        level1.Name = 'CPLOG';
        level1.Status__c = 'Active';
        levels.add(level1);

        ULC_Level__c level2 = new ULC_Level__c();
        level2.Name = 'CPVIEW';
        level2.Status__c = 'Active';
        levels.add(level2);
        insert levels;

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if (searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Page_Size__c = 20;
            ss.Lithium_EndPoint__c ='http://qlik:fYN~Rp6hY@migration29.stage.lithium.com/api/2.0/search?q=SELECT+*+FROM+messages+where+conversation.style=+\'forum\'+and+depth=0+and+conversation.last_post_time>={dtFrom}+order+by+kudos.sum(weight)+desc+limit+{limit}';
            ss.Instance_Name__c = 'cs86';
            ss.Lithium_EndPoint_Personas__c = 'http://qlik:fYN~Rp6hY@migration29.stage.lithium.com/api/2.0/search?q=SELECT+*+FROM+messages+where+conversation.style=+\'forum\'+and+depth=0+and+conversation.last_post_time>={dtFrom}+order+by+kudos.sum(weight)+desc+limit+{limit}';
            ss.Lithium_Default_Forum__c = '';
            ss.Lithium_Popular_Posts_Days_Back__c = 30;
            insert ss;
        }

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker;Business User';
        insert testContact;

        String profileId = [select id from Profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1].Id;

        

        List<Assigned_ULC_Level__c> assignedLevels = new List<Assigned_ULC_Level__c>();
        Assigned_ULC_Level__c assignedULCLevel1 = new Assigned_ULC_Level__c();
        assignedULCLevel1.ContactId__c = testContact.Id;
        assignedULCLevel1.Status__c = 'Approved';
        assignedULCLevel1.ULCLevelId__c = level1.Id;
        assignedLevels.add(assignedULCLevel1);

        Assigned_ULC_Level__c assignedULCLevel2 = new Assigned_ULC_Level__c();
        assignedULCLevel2.ContactId__c = testContact.Id;
        assignedULCLevel2.Status__c = 'Rejected';
        assignedULCLevel2.ULCLevelId__c = level2.Id;
        assignedLevels.add(assignedULCLevel2);
        insert assignedLevels;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        QS_LithiumSearchHTTPMock mock = QS_LithiumSearchHTTPMock.CreateHTTPMock(15);
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();

        QS_SearchComponentController controller = new QS_SearchComponentController();
        controller.searchString = 'Hello';
        controller.searchType = 'LITHIUM';
        controller.numberResults = 2;
        controller.personasBased = false;
        controller.getSearchResultSet();

        //Authenticated
        system.runas(communityUser)
        {
            controller = new QS_SearchComponentController();
            controller.searchString = 'Hello';
            controller.searchType = 'LITHIUM';
            controller.numberResults = 2;

            controller.personasBased = false;
            controller.getSearchResultSet();
            
            controller.personasBased = true;
            controller.getSearchResultSet();
        }

        QS_LithiumSearch js = new QS_LithiumSearch('Hello', 1, true);
        js.getPageNumber();
        js.getDisableNext();
        js.getTotalPages();
        js.pageSize = 10;
        js.getDisableNext();
        js.getTotalPages();
        Test.stopTest();
    }
    @isTest
    static void TestPartnerSearch() {

        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();

        QS_SearchSettings__c ss = new QS_SearchSettings__c();
        ss.Name = 'SearchSettings';
        ss.Page_Size__c = 20;
        ss.Lithium_EndPoint__c ='http://qlik:fYN~Rp6hY@migration29.stage.lithium.com/api/2.0/search?q=SELECT+*+FROM+messages+where+conversation.style=+\'forum\'+and+depth=0+and+conversation.last_post_time>={dtFrom}+order+by+kudos.sum(weight)+desc+limit+{limit}';
        ss.Instance_Name__c = 'cs86';
        ss.Lithium_EndPoint_Personas__c = 'http://qlik:fYN~Rp6hY@migration29.stage.lithium.com/api/2.0/search?q=SELECT+*+FROM+messages+where+conversation.style=+\'forum\'+and+depth=0+and+conversation.last_post_time>={dtFrom}+order+by+kudos.sum(weight)+desc+limit+{limit}';
        ss.Lithium_Default_Forum__c = '';
        ss.Lithium_Popular_Posts_Days_Back__c = 30;
        ss.Qlik_com_Login_URL__c = 'test.salesforce.com';
        ss.Instance_Name__c = 'cs87';
        insert ss;

        ContentVersion cv=new Contentversion();
        cv.title='Hello';
        cv.Description = 'Hello';
        cv.PathOnClient ='test';
        cv.Framework_Category__c = 'Development';
        cv.versiondata=EncodingUtil.base64Decode('Hello');
        insert cv;

        List<Id> fixedSearchResults= new List<Id>();
        List<ContentVersion> cvs = [select id from contentversion];
        for(ContentVersion c : cvs){
            fixedSearchResults.add(c.id);
        }
        
        Test.setFixedSearchResults(fixedSearchResults);


        Test.startTest();

        QS_SearchComponentController controller = new QS_SearchComponentController();
        controller.searchString = 'Hello';
        controller.numberResults = 2;
        controller.getSearchResultSet();
        controller.numberResults = 0;
        controller.getSearchResultSet();
        controller.searchString = '';
        controller.numberResults = 2;
        controller.getSearchResultSet();
    }
}