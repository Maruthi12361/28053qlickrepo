/********
* NAME: BatchProcessorTest
* Description: Test class for BatchProcessor
*
*Change Log:
    extcqb  2020-02-28  IT-2488 Mass update of Cases
******/
@IsTest
public with sharing class BatchProcessorTest {
    @IsTest
    public static void testBatchProcessor() {
        String END_USER_ACCOUNT_RT = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('End User Account').getRecordTypeId();

        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name = 'Test1', RecordTypeId = END_USER_ACCOUNT_RT));
        accounts.add(new Account(Name = 'Test2', RecordTypeId = END_USER_ACCOUNT_RT));

        Test.startTest();
        BatchProcessor batch = new BatchProcessor(accounts);
        Database.executeBatch(batch, 21);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM Account].size());

    }
}