/**************************************************************
* This class contains unit tests for validating the behavior of
* UpdateSpringCMUsersBatch batch job for User object.
*
* Log History:
* 29.06.2020   ext_bjd  ITRM-464 Initial Development
***************************************************************/

@IsTest
private class UpdateSpringCMUsersBatchTest {

    @TestSetup
    static void setup() {
        CustomUserTriggerHandler.ENABLE_USER_TRIGGER = false;
        List<User> testUsers = new List<User>();
        List<User> updatedTestUsers = new List<User>();

        testUsers.add(QTTestUtils.createMockUserForProfile('PRM - Base', true));
        testUsers.add(QTTestUtils.createMockUserForProfile('PRM - Base', true));

        User thisUserAdmin = QTTestUtils.createMockSystemAdministrator();
        System.runAs (thisUserAdmin) {
            for (Integer i = 0; i < testUsers.size(); i++) {
                testUsers[i].IsActive = true;
                testUsers[i].LastName = testUsers[i].LastName + 'spingCmUser';
                testUsers[i].SpringCMEos__SpringCM_User__c = true;
                testUsers[i].Email = testUsers[i].Email + '.invalid';
                testUsers[i].activeOnlyInSandboxes__c = false;
                testUsers[i].Country = (i >= 1) ? 'Sweden' : 'USA';
                updatedTestUsers.add(testUsers[i]);
            }
            update updatedTestUsers;
        }
    }

    @IsTest
    public static void inActiveUserTest() {
        Map<Id, User> exeptionalIdsByUsers = new Map<Id, User>([
                SELECT Id, Name, LastName, Country, Email, Last_Profile__c, Profile.Name, IsActive,
                        activeOnlyInSandboxes__c, SpringCMEos__SpringCM_User__c
                FROM User
                WHERE activeOnlyInSandboxes__c = TRUE AND IsActive = FALSE
        ]);

        Test.startTest();
        UpdateSpringCMUsersBatch usBatch = new UpdateSpringCMUsersBatch(exeptionalIdsByUsers.keySet());
        Database.executeBatch(usBatch);
        Test.stopTest();

        List<User> restUsrWithoutSpringCM = [
                SELECT Id, Username, Name, Country, Email, Profile.Name, Last_Profile__c, IsActive, activeOnlyInSandboxes__c, SpringCMEos__SpringCM_User__c
                FROM User
                WHERE Country = 'Sweden' AND SpringCMEos__SpringCM_User__c = FALSE AND Profile.Name = 'PRM - Base'
                AND LastName LIKE '%spingCmUser'
        ];
        for (User us : restUsrWithoutSpringCM) {
            System.assertEquals(true, us.IsActive);
            System.assertEquals(false, us.SpringCMEos__SpringCM_User__c);
            System.assertEquals('Sweden', us.Country);
            System.assertEquals('PRM - Base', us.Profile.Name);
            System.assertEquals(true, us.Email.contains('.invalid'));
        }

        List<User> restUsrWithSpringCM = [
                SELECT Id, Username, Name, Country, Email, Profile.Name, Last_Profile__c, IsActive, activeOnlyInSandboxes__c, SpringCMEos__SpringCM_User__c
                FROM User
                WHERE Country = 'USA' AND SpringCMEos__SpringCM_User__c = FALSE AND Profile.Name = 'PRM - Base'
                AND LastName LIKE '%spingCmUser'
        ];
        for (User us : restUsrWithSpringCM) {
            System.assertEquals(true, us.IsActive);
            System.assertEquals('USA', us.Country);
            System.assertEquals('PRM - Base', us.Profile.Name);
            System.assertEquals(true, us.Email.contains('.invalid'));
            System.assertEquals(false, us.SpringCMEos__SpringCM_User__c);
        }
    }
}