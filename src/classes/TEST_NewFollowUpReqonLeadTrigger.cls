/*********************************************
TEST_NewFollowUpReqonLeadTrigger
Description: Test trigger to set New Follow up field to be
             set to true on Lead record when below contions are met
             
             If lead score < 50 points, 
             and > 49 points AND
             Lead is owned by a queue AND
             Lead was created in past 30 days AND
             Last User Activity Date is blank AND
             New Follow-Up Required is false AND
             Event Follow-Up Required is false AND
             Email Address does not contain qliktech, qlikview, qlikmex


Log History:
2013-07-05    KMH    Initial Development
2013-11-04    CCE    Added method to test for when email field is null
2014-03-03    CCE    CR# 11365 - https://eu1.salesforce.com/a0CD000000fmr5s Added new test classes for change in threshold
2014-04-01    CCE    CR# 12068 - https://eu1.salesforce.com/a0CD000000h7Glk Modified test classes to account for multiple QT Companies in threshold
2014-07-28    CCE    CR# 15415 - https://eu1.salesforce.com/a0CD000000k2IUt Added test class Test_NewFollowUpReqOnLeadTriggerWebLeadInterestNotExistingPartner
2015-06-09    CCE    Trigger was Deactivated as part of CR# 35713 - https://eu1.salesforce.com/a0CD000000slrKm so commenting out the test class

**********************************************/
@isTest
public class TEST_NewFollowUpReqonLeadTrigger {
/*
//******************************************************

    Test_NewFollowUpReqOnLeadTrigger
  
    This method test New Follow up Field is set to True
    as all the above condition are met.
    
    Changelog:
      2013-07-05  KMH    Created method
        
//******************************************************  
  
  static testMethod void Test_NewFollowUpReqOnLeadTrigger() {
  
  Group g1 = new Group(Name='Queue', type='Queue');
  insert g1;
 
  QueueSObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
 
  System.runAs(new User(Id = UserInfo.getUserId()))
  {insert q1;}
            
  Lead testlead= new Lead(  LastName='Test', 
              FirstName='NewLeadScore', 
              Country='UK',
              Email='lds@test.com',             
              LeadSource = 'LEADSOURCE',
              OwnerId = q1.QueueID,
              mkto2__Lead_Score__c = 48,              
              Company ='LeadScoreTesting',
              Event_Follow_up_Required__c = false,
              New_Follow_Up_Required__c = false);
              
              
  insert testlead;
   
  test.startTest();     
    
  // New Followup Field Required set to True 
  testlead.mkto2__Lead_Score__c = 52;
  update(testlead);   
  
  testlead= [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c from Lead where Id = :testlead.Id];
  System.assertEquals(testlead.New_Follow_Up_Required__c , true);
    
  // New Follow Up Field Required = FALSE, as previous Lead Score was 52
  testlead.mkto2__Lead_Score__c = 50;
  update(testlead);
  
  testlead= [select Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c from Lead where Id = :testlead.Id];
  System.assertNotEquals(testlead.New_Follow_Up_Required__c , false);
  
  //New Follow Up Field Required = FALSE , as Email is qlikview.com
  testlead.mkto2__Lead_Score__c = 40;
  testlead.Email = 'Lead_test@qlikview.com';
  update(testlead);
  
  testlead= [select Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c from Lead where Id = :testlead.Id];
  System.assertNotEquals(testlead.New_Follow_Up_Required__c , false);
  
  //New Follow Up Field Required = TRUE , as Email is qlikview.com
  testlead.mkto2__Lead_Score__c = 40;
  testlead.Email = 'Lead_test@test.com';
  update(testlead);
  
  testlead= [select Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c from Lead where Id = :testlead.Id];
  System.assertNotEquals(testlead.New_Follow_Up_Required__c , false);
  
  test.stopTest();
      
  }
  
  //******************************************************

    Test_NewFollowUpReqOnLeadTrigger
  
    This method test New Follow up Field is set to True
    as all the above condition are met but email field is null.
    
    Changelog:
      2013-11-04  CCE    Created method
        
  //******************************************************  
  
  static testMethod void Test_NewFollowUpReqOnLeadTriggerNoEmail() {
  
  Group g1 = new Group(Name='Queue', type='Queue');
  insert g1;
 
  QueueSObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
 
  System.runAs(new User(Id = UserInfo.getUserId()))
  {insert q1;}
            
  Lead testlead= new Lead(  LastName='Test', 
              FirstName='NewLeadScore', 
              Country='UK',
              Email=null,             
              Phone = '123456',
              LeadSource = 'LEADSOURCE',
              OwnerId = q1.QueueID,
              mkto2__Lead_Score__c = 48,              
              Company ='LeadScoreTesting',
              Event_Follow_up_Required__c = false,
              New_Follow_Up_Required__c = false);
              
              
  insert testlead;
   
  test.startTest();     
    
  // New Followup Field Required set to True 
  testlead.mkto2__Lead_Score__c = 52;
  update(testlead);   
  
  testlead= [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c from Lead where Id = :testlead.Id];
  System.assertEquals(testlead.New_Follow_Up_Required__c , true);

  test.stopTest();
      
  }

  //******************************************************

    Test_NewFollowUpReqOnLeadTriggerGBRThresholdGtr75
  
    This method test New Follow up Field is set to True
    when QlikTech Company Name is QlikTech UK Ltd and the threshold is >= 75
    i.e. the QT Company is in the threshold list so the trigger will use the higher value of 75
    
    Changelog:
      2014-03-03  CCE    Created method
        
  //******************************************************  
  
  static testMethod void Test_NewFollowUpReqOnLeadTriggerGBRThresholdGtr75() {
  
  //Populate the Custom setting that holds the list of QlikTech Companies that require a threshold equal or greater than 75
  string QTCompaniesGtrThanThreshold = 'QlikTech Benelux,QlikTech GmbH,QlikTech Finland OY,QlikTech Iberia SL,QlikTech Nordic AB,QlikTech UK Ltd';
  QTCustomSettings__c mycs = QTCustomSettings__c.getValues('Default');
  if(mycs == null) {             
    mycs = new QTCustomSettings__c(Name = 'Default');
    mycs.NewFollowUpReqQTCompanyGtrThanThreshold1__c = QTCompaniesGtrThanThreshold;
    mycs.NewFollowUpReqLeadScoreThreshold__c = 75;
    insert mycs;
  }

  Group g1 = new Group(Name='Queue', type='Queue');
  insert g1;
 
  QueueSObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
 
  System.runAs(new User(Id = UserInfo.getUserId()))
  {insert q1;}
            
  
  //set up a QT Company that is also held in the Custom Settings so the trigger will get a match
  QlikTech_Company__c qtc = new QlikTech_Company__c();
  qtc.name = 'GBR';
  qtc.QlikTech_Company_Name__c = 'QlikTech UK Ltd';
  qtc.Country_Name__c = 'United Kingdom';
  qtc.CurrencyIsoCode = 'GBP';
  insert qtc;

  Lead testlead = new Lead(  LastName='Test', 
              FirstName='NewLeadScore', 
              Country='United Kingdom',
              Country_Code__c = qtc.Id,
              Email=null,             
              Phone = '123456',
              LeadSource = 'LEADSOURCE',
              OwnerId = q1.QueueID,
              mkto2__Lead_Score__c = 65,              
              Company ='LeadScoreTesting',
              Event_Follow_up_Required__c = false,
              New_Follow_Up_Required__c = false);
              
              
  insert testlead;
   
  test.startTest();     
  Lead retlead = [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c,QlikTech_Company__c from Lead where Id = :testlead.Id];
  System.debug('Test_NewFollowUpReqOnLeadTriggerGBRThresholdGtr75: 1 retlead = ' + retlead);
  System.assertEquals(false, retlead.New_Follow_Up_Required__c);
  
  // New Followup Field Required set to True 
  testlead.mkto2__Lead_Score__c = 75;
  update(testlead);   
  
  retlead = [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c,QlikTech_Company__c from Lead where Id = :testlead.Id];
  System.debug('Test_NewFollowUpReqOnLeadTriggerGBRThresholdGtr75: 2 retlead = ' + retlead);
  System.assertEquals(true, retlead.New_Follow_Up_Required__c);

  test.stopTest();
      
  }

  //******************************************************

    Test_NewFollowUpReqOnLeadTriggerUSAThresholdGtr50
  
    This method test New Follow up Field is set to True
    when QlikTech Company Name is QlikTech Inc and the threshold is >= 50
    i.e. the QT Company is not in the threshold list so the trigger will use the default value of 50
    
    Changelog:
      2014-03-03  CCE    Created method
        
  //******************************************************  
  
  static testMethod void Test_NewFollowUpReqOnLeadTriggerUSAThresholdGtr50() {
  
  //Populate the Custom setting that holds the list of QlikTech Companies that require a threshold equal or greater than 75
  string QTCompaniesGtrThanThreshold = 'QlikTech Benelux,QlikTech GmbH,QlikTech Finland OY,QlikTech Iberia SL,QlikTech Nordic AB,QlikTech UK Ltd';
  QTCustomSettings__c mycs = QTCustomSettings__c.getValues('Default');
  if(mycs == null) {             
    mycs = new QTCustomSettings__c(Name = 'Default');
    mycs.NewFollowUpReqQTCompanyGtrThanThreshold1__c = QTCompaniesGtrThanThreshold;
    insert mycs;
  }

  Group g1 = new Group(Name='Queue', type='Queue');
  insert g1;
 
  QueueSObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
 
  System.runAs(new User(Id = UserInfo.getUserId()))
  {insert q1;}
            
  
  //set up a QT Company that is not held in the Custom Settings so the trigger will not get a match and therefore use the Default threshold value
  QlikTech_Company__c qtc = new QlikTech_Company__c();
  qtc.name = 'USA';
  qtc.QlikTech_Company_Name__c = 'QlikTech Inc';
  qtc.Country_Name__c = 'USA';
  qtc.CurrencyIsoCode = 'USD';
  insert qtc;

  Lead testlead = new Lead(  LastName='Test', 
              FirstName='NewLeadScore', 
              Country='USA',
              Country_Code__c = qtc.Id,
              Email=null,             
              Phone = '123456',
              LeadSource = 'LEADSOURCE',
              OwnerId = q1.QueueID,
              mkto2__Lead_Score__c = 40,              
              Company ='LeadScoreTesting',
              Event_Follow_up_Required__c = false,
              New_Follow_Up_Required__c = false);
              
              
  insert testlead;
   
  test.startTest();     
  Lead retlead = [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c,QlikTech_Company__c from Lead where Id = :testlead.Id];
  System.debug('Test_NewFollowUpReqOnLeadTriggerUSAThresholdGtr50: 1 retlead = ' + retlead);
  System.assertEquals(false, retlead.New_Follow_Up_Required__c);
  
  // New Followup Field Required set to True 
  testlead.mkto2__Lead_Score__c = 50;
  update(testlead);   
  
  retlead = [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c,QlikTech_Company__c from Lead where Id = :testlead.Id];
  System.debug('Test_NewFollowUpReqOnLeadTriggerUSAThresholdGtr50: 2 retlead = ' + retlead);
  System.assertEquals(true, retlead.New_Follow_Up_Required__c);

  test.stopTest();
      
  }

  //******************************************************

    Test_NewFollowUpReqOnLeadTriggerWebLeadInterestNotExistingPartner
  
    This method test New Follow up Field is not set to True if the Web Lead Interest field is set to Existing Partner
    
    Changelog:
      2014-07-28  CCE    Created method
        
  //******************************************************  
  
  static testMethod void Test_NewFollowUpReqOnLeadTriggerWebLeadInterestNotExistingPartner() {
  
  //Populate the Custom setting that holds the list of QlikTech Companies that require a threshold equal or greater than 75
  string QTCompaniesGtrThanThreshold = 'QlikTech Benelux,QlikTech GmbH,QlikTech Finland OY,QlikTech Iberia SL,QlikTech Nordic AB,QlikTech UK Ltd';
  QTCustomSettings__c mycs = QTCustomSettings__c.getValues('Default');
  if(mycs == null) {             
    mycs = new QTCustomSettings__c(Name = 'Default');
    mycs.NewFollowUpReqQTCompanyGtrThanThreshold1__c = QTCompaniesGtrThanThreshold;
    insert mycs;
  }

  Group g1 = new Group(Name='Queue', type='Queue');
  insert g1;
 
  QueueSObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
 
  System.runAs(new User(Id = UserInfo.getUserId()))
  {insert q1;}
            
  
  //set up a QT Company that is not held in the Custom Settings so the trigger will not get a match and therefore use the Default threshold value
  QlikTech_Company__c qtc = new QlikTech_Company__c();
  qtc.name = 'USA';
  qtc.QlikTech_Company_Name__c = 'QlikTech Inc';
  qtc.Country_Name__c = 'USA';
  qtc.CurrencyIsoCode = 'USD';
  insert qtc;

  Lead testlead = new Lead(  LastName='Test', 
              FirstName='NewLeadScore', 
              Country='USA',
              Country_Code__c = qtc.Id,
              Email=null,             
              Phone = '123456',
              LeadSource = 'LEADSOURCE',
              OwnerId = q1.QueueID,
              mkto2__Lead_Score__c = 40,
              Company ='LeadScoreTesting',
              Event_Follow_up_Required__c = false,
              New_Follow_Up_Required__c = false,
              Web_Lead_Interest__c = 'Existing Partner'); //so we don't set the follow up required flag
              
              
  insert testlead;
   
  test.startTest();     
  Lead retlead = [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c,QlikTech_Company__c from Lead where Id = :testlead.Id];
  System.debug('Test_NewFollowUpReqOnLeadTriggerWebLeadInterestNotExistingPartner: 1 retlead = ' + retlead);
  System.assertEquals(false, retlead.New_Follow_Up_Required__c);
  
  // New Followup Field Required would normally be set to True but as Web_Lead_Interest__c = 'Existing Partner' it should not get set
  testlead.mkto2__Lead_Score__c = 50; //so we are above the triggering threshold
  update(testlead);   
  
  retlead = [select Id,Email,OwnerId,Event_Follow_up_Required__c,New_Follow_Up_Required__c,QlikTech_Company__c from Lead where Id = :testlead.Id];
  System.debug('Test_NewFollowUpReqOnLeadTriggerWebLeadInterestNotExistingPartner: 2 retlead = ' + retlead);
  System.assertEquals(false, retlead.New_Follow_Up_Required__c);

  test.stopTest();
      
  }
*/
}