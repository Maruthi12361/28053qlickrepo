/*
* File CloneQuoteLineItemsTest
    * @description : Unit test for CloneQuoteLineItems
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       01.01.2018   Pramod Kumar V      Created Class
    2       18.07.2018   ext_bad             add GlobalSetUp

*/
@isTest(seeAlldata=false)
private class CloneQuoteLineItemsTest{
    static testMethod void Test_CloneQuoteLineItems()
    {
        QTTestUtils.GlobalSetUp();

        Steelbrick_Settings__c settingQuote =new Steelbrick_Settings__c();
        settingQuote.Name ='SteelbrickSettingsDetails';
        settingQuote.BoomiBaseURL__c= 'Whatever';
        insert settingQuote;

        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        Account  testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        insert testContact;
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'and SobjectType ='SBQQ__Quote__c'] ;
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
        quoteForTest.Bypass_Rules__c= true;
        insert quoteForTest;
        ApexPages.StandardController sc=new ApexPages.StandardController(quoteForTest);
        CloneQuoteLineItems cl=new CloneQuoteLineItems(sc);
    }
}