/*
* File SandboxNameFinderTest
    * @description : Unit test for SandboxNameFinderTest
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       06.04.2018   Pramod Kumar V      Created Class

*/
@isTest(seeAlldata=false)
public class SandboxNameFinderTest {
    static testMethod void TestSandboxNameFinder() {
        Test.startTest();

        SandboxNameFinder snf = new SandboxNameFinder();
        snf.SandboxNameHelper ();
        System.assertNotEquals(snf.SandboxNameHelper (), NULL);

        Test.stopTest();
    }
}