/*
@author: Anthony Victorio, Model Metrics
@date: 02/02/2012
@description: Controller for the QVM "product preview" page

    2013-03-14  CCE Added goToQVMExpertiseProgram() method re CR# 7563 https://eu1.salesforce.com/a0CD000000XNdTD
    2013-12-06	MLN CR#9513 https://eu1.salesforce.com/a0CD000000b9oLU #Quick :: QlikMarket Product Preview Style Sheet Incorrect
    2017-01-24  CCE CR# 103173 - Remove Magento Connections from SFDC 
    
*/
public without sharing class QVM_ProductPreviewCon_QC {

    public User accID { get; set; }
    
    public QVM_Product_Data__c product {get;set;}
    public String productOverview {get;set;}
    public QVM_Partner_Data__c partner {get;set;}
    public Id pId {get;set;} //product ID
    
    public Boolean updateSuccess {get;set;}
    
    public S3.AmazonS3 as3 {get; private set;}
    public String bucketOwnerId {get;set;}
    
    public QVM_Settings__c settings {get; private set;}
    
    public QVM_ProductPreviewCon_QC() {
    accID = [select AccountId from User where id = :System.UserInfo.getUserId()];
    
    pId = ApexPages.currentPage().getParameters().get('id');
        
        product = (pId == null) ? new QVM_Product_Data__c() : getProduct();
        
        productOverview = (product.Overview__c == null) ? '' : QVM.getLineBreaksToBR(product.Overview__c);
        
        partner = [select Id, Partner_Display_Name__c, Partner_Website__c, Support_Email__c, 
        Support_Phone__c, Support_URL__c, Partner_Profile__c
        from QVM_Partner_Data__c where Id = :QVM.getUserPartnerDataId()];
        
        settings = QVM_Settings__c.getOrgDefaults();
        Datetime now = Datetime.now();
        
        as3 = new S3.AmazonS3(settings.S3_Key__c,settings.S3_Secret__c);
        
        if(Test.isRunningTest()) {
            bucketOwnerId = '123456';
        } else {
            S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));
            bucketOwnerId = allBuckets.Owner.Id;
        }
        
    }
    
    public QVM_Product_Data__c getProduct() {
        return [select Id, QVM_Partner__c, Product_Name__c, Overview__c, Short_Description__c, Type__c, 
            Industry__c, Job_Function__c, Version__c, QlikView_Version__c, QlikView_Version_Id__c, Referral_Link__c, Product_Download_Link__c,
            Image_1__c, Image_2__c, Image_3__c, Image_4__c, Image_5__c, Magento_Product_Id__c, Magento_Product_URL__c,
            Price__c, Weight__c, Magento_SKU__c, Magento_Last_Updated__c, Product_Type_Id__c, Industry_Id__c, 
            Job_Function_Id__c, Status__c, Submitted_For_Approval__c, Approved_Rejected_On__c, Magento_Status__c, Ajax_Demo_Link__c
            from QVM_Product_Data__c where Id = :pId];
    }
    
    public list<QVM.newException> getSFToMagentoExceptions() {
        list<QVM.newException> exceptions = new list<QVM.newException>();
        
        if(product.Product_Name__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_Product_Name_Exception));
        }
        if(product.Overview__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_Product_Overview_Exception));
        }
        if(product.Short_Description__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_Product_Short_Description_Exception));
        }
        if(product.Type__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_Type_Exception));
        }
        if(product.Industry__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_Industry_Exception));
        }
        if(product.Job_Function__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_Function_Exception));
        }
        if(product.Version__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_Version_Exception));
        }
        if(product.QlikView_Version__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_QlikView_Version_Exception));
        }
        
        if(product.Referral_Link__c == null && product.Product_Download_Link__c == null) {
            exceptions.add(new QVM.newException(Label.QVM_PB_Preview_File_Exception));
        }
        
        return exceptions;
    }
    
    //public String publishToMagento() {
        
    //    String MAGENTO_SESSION_ID = QVM.getMagentoSessionId();
        
    //    String DEFAULT_TYPE = 'simple'; //simple magento product
    //    String DEFAULT_SET = '10'; //default magento attribute set
    //    String DEFAULT_STATUS = '1'; //a status of '1' means active
    //    String DEFAULT_TAX_CLASS = '0'; //default value of '0' means 'none'
    //    String DEFAULT_VISIBILITY = '4'; //default value of '4' means 'catalog, search'
        
    //    String IMAGE1 = (product.Image_1__c != null) ? '&arg2[data][prod_img_1]=' + EncodingUtil.urlEncode(product.Image_1__c,'UTF-8') : '&arg2[data][prod_img_1]=';
    //    String IMAGE2 = (product.Image_2__c != null) ? '&arg2[data][prod_img_2]=' + EncodingUtil.urlEncode(product.Image_2__c,'UTF-8') : '&arg2[data][prod_img_2]=';
    //    String IMAGE3 = (product.Image_3__c != null) ? '&arg2[data][prod_img_3]=' + EncodingUtil.urlEncode(product.Image_3__c,'UTF-8') : '&arg2[data][prod_img_3]=';
    //    String IMAGE4 = (product.Image_4__c != null) ? '&arg2[data][prod_img_4]=' + EncodingUtil.urlEncode(product.Image_4__c,'UTF-8') : '&arg2[data][prod_img_4]=';
    //    String IMAGE5 = (product.Image_5__c != null) ? '&arg2[data][prod_img_5]=' + EncodingUtil.urlEncode(product.Image_5__c,'UTF-8') : '&arg2[data][prod_img_5]=';
        
    //    String REFERRAL_LINK = (product.Referral_Link__c != null) ? EncodingUtil.urlEncode(product.Referral_Link__c,'UTF-8') : '';
    //    String DOWNLOAD_LINK = (product.Product_Download_Link__c != null) ? EncodingUtil.urlEncode(product.Product_Download_Link__c,'UTF-8') : '';
    //    String AJAX_DEMO_LINK = (product.Ajax_Demo_Link__c != null) ? EncodingUtil.urlEncode(product.Ajax_Demo_Link__c,'UTF-8') : '';
        
    //    String SF_TYPE = (product.Type__c != null) ? EncodingUtil.urlEncode(product.Type__c, 'UTF-8') : '';
    //    String SF_FUNCTION = (product.Job_Function__c != null) ? EncodingUtil.urlEncode(product.Job_Function__c.replace(';',','), 'UTF-8') : '';
    //    String SF_INDUSTRY = (product.Industry__c != null) ? EncodingUtil.urlEncode(product.Industry__c.replace(';',','), 'UTF-8') : '';
    //    String SF_VERSION = (product.Version__c != null) ? EncodingUtil.urlEncode(product.Version__c.replace(';',','), 'UTF-8') : '';
        
    //    Date now = system.today();
    //    STRING NEW_FROM = now.month().format() + '/' + now.day().format() + '/' + now.year().format().replace(',', '');
        
    //    Date till = system.today().addDays(30);
    //    STRING NEW_UNTIL = till.month().format() + '/' + till.day().format() + '/' + till.year().format().replace(',', '');
        
    //    STRING META_KEYWORDS = Label.QVM_Product_Meta_Keywords;
    //    META_KEYWORDS = EncodingUtil.urlEncode(META_KEYWORDS,'UTF-8');
        
    //    STRING META_DESCRIPTION = (product.Short_Description__c.length() > 160) ? product.Short_Description__c.substring(0,160) : product.Short_Description__c;
    //    META_DESCRIPTION = EncodingUtil.urlEncode(META_DESCRIPTION,'UTF-8');
        
    //    String API_ARGUMENTS = '&arg2[product]=' + product.Magento_Product_Id__c +
    //      '&arg2[data][name]=' + EncodingUtil.urlEncode(product.Product_Name__c,'UTF-8') +
    //      '&arg2[data][meta_title]=' + EncodingUtil.urlEncode(product.Product_Name__c,'UTF-8') + 
    //      '&arg2[data][meta_keyword]=' + META_KEYWORDS +
    //      '&arg2[data][meta_description]=' + META_DESCRIPTION +
    //      '&arg2[data][short_description]=' + EncodingUtil.urlEncode(product.Short_Description__c,'UTF-8') +
    //      '&arg2[data][description]=' + EncodingUtil.urlEncode(product.Overview__c,'UTF-8') +
    //      '&arg2[data][status]=' + DEFAULT_STATUS +
    //      '&arg2[data][sf_product_id]=' + EncodingUtil.urlEncode(product.Id,'UTF-8') + 
    //      '&arg2[data][sf_referral_link]=' + REFERRAL_LINK + 
    //      '&arg2[data][sf_download_link]=' + DOWNLOAD_LINK + 
    //      '&arg2[data][sf_ajax_demo_link]=' + AJAX_DEMO_LINK + 
    //      '&arg2[data][sf_product_version]=' + SF_VERSION + 
    //      '&arg2[data][sf_partner_id]=' + EncodingUtil.urlEncode(product.QVM_Partner__c,'UTF-8') + 
    //      IMAGE1 + IMAGE2 + IMAGE3 + IMAGE4 + IMAGE5 +
    //      '&arg2[data][price]=' + EncodingUtil.urlEncode(product.Price__c,'UTF-8') +
    //      '&arg2[data][weight]=' + EncodingUtil.urlEncode(product.Weight__c,'UTF-8') +
    //      '&arg2[data][visibility]=' + DEFAULT_VISIBILITY +
    //      '&arg2[data][tax_class_id]=' + DEFAULT_TAX_CLASS +
    //      QVM.getMagentoCategoryAPIString(product.QlikView_Version_Id__c + ';' + 
    //      product.Product_Type_Id__c + ';' + product.Industry_Id__c + ';' + 
    //      product.Job_Function_Id__c, 'data');
        
    //    Dom.Document doc = QVM.getMagentoCallDocument(MAGENTO_SESSION_ID, 'product.update', API_ARGUMENTS);
    //    Dom.XMLNode root = doc.getRootElement();
    //    Dom.XMLNode call = root.getChildElements()[0];
        
    //    String updateResult;
        
    //    for(Dom.XMLNode node : call.getChildElements()) {
    //        if('status' == node.getName()) {
    //            updateResult = node.getText();
    //        }
    //    }
        
    //    return updateResult;
        
    //}
    
    public PageReference performPublishToMagento() {
        if(getSFToMagentoExceptions().size() > 0) {
            for(QVM.newException e : getSFToMagentoExceptions()) {
                ApexPages.addMessages(e);
            }
            return null;
        } else {
            try {
                
                if(product.Product_Download_Link__c != null) {
                    if(this.makeLiveS3(product.Product_Download_Link__c)) {
                        product.Product_Download_Link__c = product.Product_Download_Link__c.replace('draft', 'live');
                    }
                }
                
                if(product.Image_1__c != null) {
                    if(this.makeLiveS3(product.Image_1__c)) {
                        product.Image_1__c = product.Image_1__c.replace('draft', 'live');
                    }
                }
                
                if(product.Image_2__c != null) {
                    if(this.makeLiveS3(product.Image_2__c)) {
                        product.Image_2__c = product.Image_2__c.replace('draft', 'live');
                    }
                }
                
                if(product.Image_3__c != null) {
                    if(this.makeLiveS3(product.Image_3__c)) {
                        product.Image_3__c = product.Image_3__c.replace('draft', 'live');
                    }
                }
                
                if(product.Image_4__c != null) {
                    if(this.makeLiveS3(product.Image_4__c)) {
                        product.Image_4__c = product.Image_4__c.replace('draft', 'live');
                    }
                }
                
                if(product.Image_5__c != null) {
                    if(this.makeLiveS3(product.Image_5__c)) {
                        product.Image_5__c = product.Image_5__c.replace('draft', 'live');
                    }
                }
                
                //String updateResult = publishToMagento();
                
                //if(updateResult == 'success') {
                    product.Status__c = 'Active';
                    product.Magento_Status__c = 'Published';
                    product.Magento_Last_Updated__c = System.now();
                    update product;
                //}
                
                PageReference next;
                next = Page.QVM_ControlPanelMain_QC;
                return next;
                
            } catch(Exception e) { ApexPages.addMessages(e); return null; }
        }
    }
    
    public PageReference editProduct() {
        try {
                if(product.Status__c != 'Draft')
                        product.Edit_Wizard__c = true;
            product.Status__c = 'Draft';
            
            update product;
            
            PageReference edit;
            edit = Page.QVM_ProductBuilderStep1_QC;
            edit.getParameters().put('Id', product.Id);
            return edit;
        } catch (Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
            
    }
    
    public PageReference submitApproval() {
        if(getSFToMagentoExceptions().size() > 0) {
            for(QVM.newException e : getSFToMagentoExceptions()) {
                ApexPages.addMessages(e);
            }
            return null;
        } else {
            try {
                product.Status__c = 'Under Review';
                update product;
                
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Submitting product for approval.');
                req.setObjectId(product.id);
                Approval.ProcessResult result = Approval.process(req);
                
                PageReference next;
                next = Page.QVM_ControlPanelMain_QC;
                return next;
                
            } catch (Exception e) { ApexPages.addMessages(e); return null; }
        }
            
    }
    
    public PageReference goToQVMControlPanel() {
        PageReference next;
        next = Page.QVM_ControlPanelMain_QC;
        return next;
    }
    
    public PageReference goToQVMExpertiseProgram() {    //added by CCE 2013-03-14 re CR# 7563 
        PageReference next = new PageReference(Page.ppQonnectProgramPage.getUrl() + '?SubPage=PEP');
        System.debug('goToQVMExpertiseProgram = ' + next);
        return next;
    }
    

    public Boolean makeLiveS3(String draftKey) {
        
        Integer baseLength = ('http://' + settings.S3_Bucket__c + '.s3.amazonaws.com/').length();
        
        String SourceBucket = settings.S3_Bucket__c;
        String SourceKey = draftKey.substring(baseLength);
        
        String DestinationBucket = settings.S3_Bucket__c;
        String DestinationKey = SourceKey.replace('draft', 'live');
        
        Datetime now = System.now();
        
        Boolean copyObjResult = (Test.isRunningTest()) ? true : as3.CopyObject_ACL(SourceBucket, SourceKey, DestinationBucket, DestinationKey, 'COPY', null, 'public-read', bucketOwnerId, 'STANDARD', as3.key, now, as3.signature('CopyObject',now), as3.secret);
        
        return copyObjResult;

    }

}