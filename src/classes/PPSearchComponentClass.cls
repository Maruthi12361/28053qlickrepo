/*************************************************************************************************************
 Name:PPSearchComponentClass
 Author: Santoshi Mishra
 Purpose: This class is responsible for searching records in salesforce object and knowledge database.
 Created Date : March 2012
*************************************************************************************************************/
public with sharing class PPSearchComponentClass {
	public String srchtext {get;set;}
	public String srchknowledge {get;set;}
	public String srchContent {get;set;}
	public String URL {get;set;}
	public Pagereference redirectSearch() //redirects to standard salesforce search
	{
		Pagereference p = new Pagereference ('/search/SearchResults?searchType=2&sen=0&setLast=1&sbstr='+srchtext);
		return p;
	}
	public Pagereference redirectKnowledge()// redirects to standard salesforce knowledge search
	{
		Pagereference p = new Pagereference ('/knowledge/knowledgeHome.apexp?searchtype=C&search='+srchknowledge);
		return p;
	}
	public PageReference redirectContent() // redirects to standard salesforce content search
	{
		Pagereference p = new Pagereference ('/sfc/#search?searchTerm='+srchContent);
		p.setredirect(true);
		return p;
		
	}

}