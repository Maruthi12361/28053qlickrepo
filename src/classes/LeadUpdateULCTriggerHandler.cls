/***************************************************
Class: LeadUpdateULCTriggerHandler
Object: Lead
Description: When a Lead is changed/updated, especially the fields
			 that was used by ULC, we need to make the ulc record dirty so heroku
			 connect will synch data to heroku postgres db
Change Log:
	201510026	SAN 
 	2017-10-25 AYS BMW-402 : Migrated from LeadUpdateULCTrigger.trigger. 
******************************************************/

public class LeadUpdateULCTriggerHandler {
	public LeadUpdateULCTriggerHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
		Set<Id> LeadsId = new Set<Id>();
		List<ULC_Details__c> ULCsToBeUpdated = new List<ULC_Details__c>();

		for(Integer i = 0; i<triggerNew.size();i++)
		{
			Lead ln = triggerNew[i];
			Lead lo = triggerOld[i];
			if(	ln.Email != lo.Email ||
				ln.FirstName != lo.FirstName ||
				ln.LastName != lo.LastName ||
				ln.City != lo.City ||
				ln.Country != lo.Country ||
				ln.PostalCode != lo.PostalCode ||
				ln.State != lo.State ||
				ln.Street != lo.Street ||
				ln.QCloudID__c != lo.QCloudID__c ||
				ln.Country_Code__r.Name != lo.Country_Code__r.Name ||
				ln.Phone != lo.Phone ) 
			{
	                LeadsId.add(ln.Id);
	        }         
	    }
	    if(!LeadsId.isEmpty()){ //UIN code improvement    
		    for ( ULC_Details__c ulcList : [select Id, Toggle_dirty__c from ULC_Details__c where ULCStatus__c = 'Active' AND LeadId__c in : LeadsId])
		    {
		    	ulcList.Toggle_dirty__c = !ulcList.Toggle_dirty__c;
		    	ULCsToBeUpdated.add(ulcList);

		    }
			if (ULCsToBeUpdated.size() > 0)
			{
				update ULCsToBeUpdated;		
			}
		}//UIN code improvement
	}
}