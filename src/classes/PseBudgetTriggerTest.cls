/***************************************************************************************************************************************
	Changelog:
		2012-02-15  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
							to TestPseMarkProjectBillableTrigger() and testUpdateBudgets()
							(Moved from TestPseMarkProjectBillableTrigger)
		2017-11-09  ext_vos Add test for new logic: Populates the Project PO number when budget was approved.   
		2017-11-17	ext_vos	Add the RunUs(user) with corporate currency.
		2017-12-14	ext_vos	CHG0031444 - Update test for new PO number logic (when Budget pse__Include_In_Financials__c change from false to true).
		2019-03-25	ext_vos CHG0034979 - Update testNewBudgets() with new Billable logic. 
****************************************************************************************************************************************/
@isTest
private class PseBudgetTriggerTest {
	
	@testSetup
	static void createTestData() { 
		ID adminProfileId = [select id from profile where name = 'System Administrator' limit 1].Id;
		User admin = new User(alias = 'tPseBudg', email = 'adminadmin@testorg.com', emailencodingkey = 'UTF-8', lastname = 'Testing', 
			languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = adminProfileId, 
			CurrencyIsoCode = 'USD', DefaultCurrencyIsoCode = 'USD', timezonesidkey = 'America/Los_Angeles', username = 'adminadmin@testorg.com');
		insert admin;
	}

	static testMethod void testNewBudgets() {
		User user = [select Id from User where alias = 'tPseBudg'];
		System.runAs(user) {
	        // setup account
	        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
			QlikTech_Company__c QTComp = new QlikTech_Company__c(
				Name = 'GBR',
				QlikTech_Company_Name__c = 'QlikTech UK Ltd',
				Country_Name__c = 'United Kingdom',
				Subsidiary__c = testSubs1.id						
			);
			insert QTComp;
			QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
			
			Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
	        insert testAccount;

			pse__Practice__c practice = new pse__Practice__c(Name = 'test');
			insert practice;
	        // setup Project
	        List<pse__Proj__c> projects = new List<pse__Proj__c>();
	        for (Integer i = 0; i < 2; i++) {
	            pse__Proj__c project = new pse__Proj__c(
	                Name = 'Test Project ' + i, 
	                pse__Is_Active__c = true,
	                pse__Allow_Timecards_Without_Assignment__c = true,
	                pse__Is_Billable__c = false, 
	                pse__Account__c = testAccount.Id, 
	                pse__Stage__c = 'In Progress',
	                pse__Start_Date__c = Date.today().addDays(-10),
	                pse__End_Date__c = Date.today().addDays(30),
	                Key_Engagement_Features__c = 'QlikView',
	                Purpose_of_Engagement__c = 'Test',
	                Sales_Classification__c = 'Internal',
	                Customer_Critial_Success_Factors__c = 'Test',
					pse__Practice__c = practice.Id,
	                Invoicing_Type__c = 'Deferred');
	            projects.add(project);
	        }
	        insert projects;
	        System.assert(![select Id, pse__Is_Billable__c from pse__Proj__c where id =: projects[0].Id].pse__Is_Billable__c);

	        // Setup budgets
	        List<pse__Budget__c> budgets = new List<pse__Budget__c>();
            pse__Budget__c budget1 = new pse__Budget__c(Name = 'Test Budget 0', 
            											pse__Project__c = projects[0].Id,
                										pse__Status__c = 'Approved',
                										pse__Approved__c = true, 
                										CurrencyIsoCode = 'USD');
            budgets.add(budget1);
            pse__Budget__c budget2 = new pse__Budget__c(Name = 'Test Budget 1', 
            											pse__Project__c = projects[1].Id,
                										pse__Status__c = 'Approved',
                										CurrencyIsoCode = 'USD');
            budgets.add(budget2);
	        Semaphores.PseBudgetTriggerAfterInsert = false;
	        insert budgets;

	        System.assert([select pse__Is_Billable__c from pse__Proj__c where Id =: projects[0].Id].pse__Is_Billable__c);
	        System.assert([select pse__Is_Billable__c from pse__Proj__c where Id =: projects[1].Id].pse__Is_Billable__c);
	    }
    }

    static testMethod void testUpdateBudgets() {
        User user = [select Id from User where alias = 'tPseBudg'];
		System.runAs(user) {
	        // setup account
	        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
			QlikTech_Company__c QTComp = new QlikTech_Company__c(
				Name = 'GBR',
				QlikTech_Company_Name__c = 'QlikTech UK Ltd',
				Country_Name__c = 'United Kingdom',
				Subsidiary__c = testSubs1.id						
			);
			insert QTComp;
			QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
			
			Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
	        insert testAccount;

			pse__Practice__c practice = new pse__Practice__c(Name = 'test');
			insert practice;
	        // setup Project
	        List<pse__Proj__c> projects = new List<pse__Proj__c>();
	        for (Integer i = 0; i < 20; i++) {
	            pse__Proj__c project = new pse__Proj__c(
	                Name = 'Test Project ' + i, 
	                pse__Is_Active__c = true, 
	                pse__Allow_Timecards_Without_Assignment__c = true,
	                pse__Is_Billable__c = false, 
	                pse__Account__c = testAccount.Id,
	                pse__Stage__c = 'In Progress',
	                pse__Start_Date__c = Date.today().addDays(-10),
	                pse__End_Date__c = Date.today().addDays(30),
	                Key_Engagement_Features__c = 'QlikView',
	                Purpose_of_Engagement__c = 'Test',
	                Sales_Classification__c = 'Internal',
	                Customer_Critial_Success_Factors__c = 'Test',
					pse__Practice__c = practice.Id,
	                Invoicing_Type__c = 'Deferred');
	            projects.add(project);
	        }
	        insert projects;

	        // Setup budgets
	        List<pse__Budget__c> budgets = new List<pse__Budget__c>();
	        Integer i = 0;
	        for (pse__Proj__c project : projects) {
	            pse__Budget__c budget = new pse__Budget__c(Name = 'Test Budget ' + i++, pse__Project__c = project.Id, CurrencyIsoCode = 'USD');
	            budgets.add(budget);
	        }
	        Semaphores.PseBudgetTriggerAfterInsert = false;
	        insert budgets;

	        for (pse__Budget__c budget : budgets) {
	            budget.pse__Status__c = 'Approved';
	            budget.pse__Approved__c = true;
	        }
	        Semaphores.PseBudgetTriggerAfterUpdate = false;
	        update budgets;

	        List<pse__Proj__c> updatedProjects = [SELECT Name FROM pse__Proj__c WHERE Id in :projects];
	        for (pse__Proj__c updatedProject : updatedProjects) {
	            System.assert(updatedProject.pse__Is_Billable__c = true);
	        }
	    }
    }

    static testMethod void testPopulatingPONumberFromBudgets() {
        User user = [select Id from User where alias = 'tPseBudg'];
		System.runAs(user) {
	        // setup test data
	        Subsidiary__c sub = TestQuoteUtil.createSubsidiary(); 
	        QlikTech_Company__c comp = new QlikTech_Company__c(
	            Name = 'GBR',
	            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
	            Country_Name__c = 'United Kingdom',
	            Subsidiary__c = sub.id                        
	        );
	        insert comp;
	        
	        Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = comp.QlikTech_Company_Name__c, Billing_Country_Code__c = comp.Id);
	        insert testAccount;

			pse__Practice__c practice = new pse__Practice__c(Name = 'test');
			insert practice;
	        pse__Proj__c project = new pse__Proj__c(
	                                    Name = 'Test Project ', 
	                                    pse__Is_Active__c = true,
	                                    pse__Allow_Timecards_Without_Assignment__c = true,
	                                    pse__Is_Billable__c = false, 
	                                    pse__Account__c = testAccount.Id, 
	                                    pse__Stage__c = 'In Progress',
	                                    pse__Start_Date__c = Date.today().addDays(-10),
	                                    pse__End_Date__c = Date.today().addDays(30),
	                                    Key_Engagement_Features__c = 'QlikView',
	                                    Purpose_of_Engagement__c = 'Test',
	                                    Sales_Classification__c = 'Internal',
	                                    Customer_Critial_Success_Factors__c = 'Test',
										pse__Practice__c = practice.Id,
	                                    Invoicing_Type__c = 'Deferred');
	        insert project;
	        
	        pse__Proj__c toTest = [select PO_Number__c from pse__Proj__c where Id = :project.Id];
	        System.assertEquals('na', toTest.PO_Number__c);

	        List<pse__Budget__c> budgets = new List<pse__Budget__c>();
	        pse__Budget__c budgetOpen = new pse__Budget__c(Name = 'Test Budget ONE', 
	                                        pse__Project__c = project.Id,
	                                        pse__Status__c = 'Open',
	                                        PO_Number__c = '11111111',
	                                        CurrencyIsoCode = 'USD');
	        budgets.add(budgetOpen);
	        pse__Budget__c budgetApproved = new pse__Budget__c(Name = 'Test Budget TWO', 
	                                        pse__Project__c = project.Id,
	                                        pse__Status__c = 'Approved',
	                                        pse__Approved__c = true,
	                                        PO_Number__c = '222222222',
	                                        CurrencyIsoCode = 'USD',
	                                        pse__Include_In_Financials__c = true
	                                        );
	        budgets.add(budgetApproved);
	        Semaphores.PseBudgetTriggerAfterUpdate = true;	// to prevent any calls from third-party triggers
	        Semaphores.PseBudgetTriggerAfterInsert = false;
	        insert budgets;

	        toTest = [select PO_Number__c from pse__Proj__c where Id = :project.Id];
	        System.assertNotEquals(budgetOpen.PO_Number__c, toTest.PO_Number__c);
	        System.assertEquals(budgetApproved.PO_Number__c, toTest.PO_Number__c);

	        budgetOpen.pse__Include_In_Financials__c = false;
	        Semaphores.PseBudgetTriggerAfterUpdate = false;
	        update budgetOpen;

	        budgetOpen.pse__Include_In_Financials__c = true;
	        Semaphores.PseBudgetTriggerAfterUpdate = false;	        
	        update budgetOpen;

	        toTest = [select PO_Number__c from pse__Proj__c where Id = :project.Id];
	        System.assertEquals(budgetOpen.PO_Number__c, toTest.PO_Number__c);
	    }
	}	
}