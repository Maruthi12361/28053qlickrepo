/******************************************************

	Class: devQlikViewCom
	
	This class is responsible to call a WebService on
	dev.qlikview.com. It will be invoked by a trigger
	on the Lead object. 	
	
	Changelog:
		2010-12-23	MHG		Created file
		2011-07-28  RDZ     Recreating file from new wsdl (Qollaborate: CR#2416)
				            https://eu1.salesforce.com/a0CD000000EVUPw
		2011-08-23  RDZ		Changing web service url for http://staging.qlikview.com/_external/lead.svc
		2011-10-14	MHG		Autoselection of endpoint URL updated Test Cases due to new validation rules				            
******************************************************/
public class devQlikViewCom {
     
    //From tempuriOrg.cls Generated from the wsdl
    public class BasicHttpBinding_Ilead {
        public String endpoint_x = (UserInfo.getOrganizationId().startsWith('00D20000000IGPX') ? 'http://staging.qlikview.com/_external/lead.svc' : 'http://dev.qlikview.com/_external/lead.svc');
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 60000;
        public TokenHeader TokenHeader;
        private String TokenHeader_hns = 'TokenHeaderValue=http://tempuri.org/';
        private String[] ns_map_type_info = new String[]{'http://schemas.datacontract.org/2004/07/QlikView.Sitecore.WebServices', 'devQlikViewCom', 'http://schemas.datacontract.org/2004/07/QkvSitecore.WcfServiceLibrary', 'devQlikViewCom', 'http://schemas.microsoft.com/2003/10/Serialization/', 'devQlikViewCom', 'http://tempuri.org/', 'devQlikViewCom'};
        public InitializeApexResult InitializeApexTrigger(ArrayOfLeadIdentifiers LeadIdentifiers) {
            InitializeApexTriggerRequest_element request_x = new InitializeApexTriggerRequest_element();
            InitializeApexTriggerResponse_element response_x;
            request_x.LeadIdentifiers = LeadIdentifiers;
            Map<String, InitializeApexTriggerResponse_element> response_map_x = new Map<String, InitializeApexTriggerResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/Ilead/InitializeApexTrigger',
              'http://tempuri.org/',
              'InitializeApexTriggerRequest',
              'http://tempuri.org/',
              'InitializeApexTriggerResponse',
              'devQlikViewCom.InitializeApexTriggerResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.Result;
        }
    }
    public class InitializeApexTriggerRequest_element {
        public ArrayOfLeadIdentifiers LeadIdentifiers;
        private String[] LeadIdentifiers_type_info = new String[]{'LeadIdentifiers','http://schemas.datacontract.org/2004/07/QkvSitecore.WcfServiceLibrary','ArrayOfLeadIdentifiers','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'LeadIdentifiers'};
    }
    public class InitializeApexTriggerResponse_element {
        public InitializeApexResult Result;
        private String[] Result_type_info = new String[]{'Result','http://schemas.datacontract.org/2004/07/QlikView.Sitecore.WebServices','InitializeApexResult','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'Result'};
    }
    
    //From SchemasDatacontractOrg200407Qkvsite.cls
    public class LeadIdentifiers {
        public String Email;
        public Boolean IsContact;
        public String SalesforceID;
        private String[] Email_type_info = new String[]{'Email','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] IsContact_type_info = new String[]{'IsContact','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] SalesforceID_type_info = new String[]{'SalesforceID','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/QkvSitecore.WcfServiceLibrary','true','false'};
        private String[] field_order_type_info = new String[]{'Email','IsContact','SalesforceID'};
    }
    public class ArrayOfLeadIdentifiers {
        public LeadIdentifiers[] LeadIdentifiers;
        private String[] LeadIdentifiers_type_info = new String[]{'LeadIdentifiers','http://schemas.datacontract.org/2004/07/QkvSitecore.WcfServiceLibrary','LeadIdentifiers','0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/QkvSitecore.WcfServiceLibrary','true','false'};
        private String[] field_order_type_info = new String[]{'LeadIdentifiers'};
    }
    public class TokenHeader {
        public String Signature;
        public String Timestamp;
        public String UserID;
        private String[] Signature_type_info = new String[]{'Signature','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] Timestamp_type_info = new String[]{'Timestamp','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] UserID_type_info = new String[]{'UserID','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/QkvSitecore.WcfServiceLibrary','true','false'};
        private String[] field_order_type_info = new String[]{'Signature','Timestamp','UserID'};
    }
    
    //From schemasDatacontractOrg200407Qlikvie.cls
    
    public class InitializeApexResult {
        public String PayloadXml;
        private String[] PayloadXml_type_info = new String[]{'PayloadXml','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/QlikView.Sitecore.WebServices','true','false'};
        private String[] field_order_type_info = new String[]{'PayloadXml'};
    }
    
        
    @future (callout=true)
	public static void CallInitializeApexTrigger(Map<string, string> Leads, Boolean IsContact)
	{
		System.debug('CallInitializeApexTrigger: Starting');
		if (Leads.size() == 0)
		{
			System.debug('CallInitializeApexTrigger: Nothing to do, exiting');
			return;
		}

		// Populate Array of LeadID and Email
		ArrayOfLeadIdentifiers ArrayOfLeads = new ArrayOfLeadIdentifiers();
		BasicHttpBinding_Ilead service = new BasicHttpBinding_Ilead();
		ArrayOfLeads.LeadIdentifiers = new List<LeadIdentifiers>();
		for (string Id : Leads.keySet())
		{
			LeadIdentifiers LeadId = new LeadIdentifiers();
			LeadId.SalesforceID = Id;
			LeadId.Email = Leads.get(Id);
			LeadId.IsContact = IsContact;
			ArrayOfLeads.LeadIdentifiers.Add(LeadId);
			System.debug('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');
			System.debug('Id: ' + Id + ' IsContact: ' + IsContact);
		}

		// Create Token 
		service.TokenHeader = new TokenHeader();
		service.TokenHeader.UserID = 'SfApexTriggerDev';
		service.TokenHeader.Timestamp = System.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss');
		string CryptoText = service.TokenHeader.UserId+'|aF342e8eDf2298eB|'+service.TokenHeader.Timestamp;
		Blob PreHash = Blob.ValueOf(CryptoText);
		Blob Sig = Crypto.generateDigest('SHA1', PreHash);
		service.TokenHeader.Signature = EncodingUtil.base64Encode(Sig);
		System.debug('CallInitializeApexTrigger: UserId: ' + service.TokenHeader.UserID);
		System.debug('CallInitializeApexTrigger: Timestamp: ' + service.TokenHeader.Timestamp);
		System.debug('CallInitializeApexTrigger: CryptoText: ' + CryptoText);
		System.debug('CallInitializeApexTrigger: Signature: ' + service.TokenHeader.Signature);
		
		// Call webservice
		try
		{
			InitializeApexResult Result = service.InitializeApexTrigger(ArrayOfLeads);
			System.debug('CallInitializeApexTrigger: Result was [' + Result.PayloadXml + ']');
		}
		catch (System.Exception Ex)
		{
			System.debug('CallInitializeApexTrigger: Cought exception - ' + ex.getMessage());
		}
		
		System.debug('CallInitializeApexTrigger: Exiting');
	}
	
	public static TestMethod void TEST_CallInitializeApexTrigger_Trigger()
	{
		
		Lead LeadToWorkOn = New Lead (
        	FirstName = 'Lead',
        	LastName = 'Test1001',
        	Company = 'Test',
        	Country = 'Sweden',
        	Email = 'test@test.com',
        	Trigger_Webservice__c = true
        );		
		insert LeadToWorkOn;	
		
		Contact ContactToWorkOn = New Contact (
        	FirstName = 'ContactTest',
        	LastName = 'Test1001',
        	Email = 'test@test.com',
        	Trigger_Webservice__c = true
        );		
		insert ContactToWorkOn;	
	} 	
}