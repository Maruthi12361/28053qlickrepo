/**
 * @author  Tquila Support
 * @date    14/10/2014
 *  
 * Test class for QS_CaseWizard Page and QS_CaseWizard_Controller controller.
 * Also, covers QS_CaseWizardKnowledgeArticle Page and the related controller in a separate unit test method
 *  * 
 * Change Log:
 * 2016-02-19  NAD     Updated Responsible_Partner__c references and related test code per CR# 33068 (Responsible Partner change).
 * 2019-05-05  AIN     IT-1597 Updated for Support Portal Redesign
 */

@isTest
private class QS_CaseWizard_ControllerTest {
    
    private static Id recTypeId;
    
    // Unit Test for Case Wizard Page
    static testMethod void myUnitTestCaseWizard() {

        QTTestUtils.GlobalSetUp();
        
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        
        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;

        List<Basic__kav> basicKavs = new List<Basic__kav>(); 

        Basic__kav basicKav1 = TestDataFactory.createBasicKnowledgeArticle('first apex', 'first from apex', 'first-test-apex-Basic', 'en_US');
        basicKav1.IsVisibleInCsp = true;
        basicKav1.IsVisibleInPkb = true;
        basicKav1.IsVisibleInPRM = true;
        basicKavs.add(basicKav1);

        Basic__kav basicKav2 = TestDataFactory.createBasicKnowledgeArticle('second apex', 'second from apex', 'second-test-apex-Basic', 'en_US');
        basicKav2.IsVisibleInCsp = true;
        basicKav2.IsVisibleInPkb = true;
        basicKav2.IsVisibleInPRM = true;
        basicKavs.add(basicKav2);

        insert basicKavs;

        Set<ID> basicKavIds = new Set<ID>();
        basicKavIds.Add(basicKav1.ID);
        basicKavIds.Add(basicKav2.ID);

        basicKavs = [SELECT KnowledgeArticleId, ArticleNumber FROM Basic__kav WHERE Id in :basicKavIds];

        KbManagement.PublishingService.publishArticle(basicKavs[0].KnowledgeArticleId, true);
        KbManagement.PublishingService.publishArticle(basicKavs[1].KnowledgeArticleId, true);
        
        List<KnowledgeArticleVersion> knowledgeArticleVersion = [Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                                                                    From KnowledgeArticleVersion
                                                                    Where Language = 'en_US' AND PublishStatus = 'Online'
                                                                    LIMIT 2];

        system.debug('knowledgeArticleVersion.Size(): ' + knowledgeArticleVersion.Size());
        
        if(knowledgeArticleVersion != null && !knowledgeArticleVersion.isEmpty()) {
            System.assertEquals(knowledgeArticleVersion[0].IsVisibleInPrm, true);
            System.assertEquals(knowledgeArticleVersion[0].IsVisibleInCsp, true);
            System.assertEquals(knowledgeArticleVersion[0].IsVisibleInApp, true);
            
            System.assertNotEquals(knowledgeArticleVersion[0].Id, null);
            System.assertNotEquals(knowledgeArticleVersion[0].ArticleNumber, null);
            System.assertEquals(knowledgeArticleVersion[0].PublishStatus, 'Online');
        }
        
        
        // Create Different Categories for 'QS_CaseWizard' Page 
        List<Category_Lookup__c> categoryLookupList = new List<Category_Lookup__c> ();
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level','Third Category Level Type', true, 'Please email urgently', 'question1', 'question2', 'question3', 'question4', 'question5', knowledgeArticleVersion[0].ArticleNumber + ', ' +knowledgeArticleVersion[1].ArticleNumber, null, false, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level','Fourth Category Level Type', true, 'Please call us immediately', 'Steps to reproduce:', 'Number of users affected:', null, null, null, knowledgeArticleVersion[0].ArticleNumber, '1', false, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Fifth Category Level',null, false, 'channel', 'question1', 'question2', 'question3', 'question4', 'question5', knowledgeArticleVersion[0].ArticleNumber, null, false, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'Sixth Category Level', 'Nineth Category Level',null, true, 'channel', 'question1', 'question2', 'question3', 'question4', null, knowledgeArticleVersion[0].ArticleNumber, null, false, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'Seventh Category Level', 'Tenth Category Level',null, false, 'channel', 'question1', 'question2', null, null, null, knowledgeArticleVersion[0].ArticleNumber, null, true, 'QlikTech Master Support Record Type', 'Systems Queue'));
        
        if(categoryLookupList != null && !categoryLookupList.isEmpty())
            insert categoryLookupList;
        
        
        // Running as the current user
        QS_CaseWizard_Controller caseWizard = new QS_CaseWizard_Controller();
        caseWizard.selectedFilter1 = 'First Category Level';
        caseWizard.filterCategoryLookupList();
        caseWizard.selectedFilter2 = 'Second Category Level';
        caseWizard.filterCategoryLookup2List();
        
        caseWizard.selectedFilter3 = 'Third Category Level Type';
        caseWizard.filterCategoryLookup3List();
        caseWizard.nextCaseDetail();
        System.assertEquals(caseWizard.caseObj.Severity__c, null);
        
        
        //Changing to the second category level
        
        caseWizard.selectedFilter3 = 'Fourth Category Level Type';
        caseWizard.filterCategoryLookup3List();
        caseWizard.nextCaseDetail();
        System.assertEquals(caseWizard.caseObj.Severity__c, null);
        
        
        caseWizard.selectedFilter2 = 'Fifth Category Level';
        caseWizard.filterCategoryLookup2List();
        caseWizard.selectedFilter1 = 'Seventh Category Level';
        caseWizard.filterCategoryLookupList();
        caseWizard.selectedFilter2 = 'Tenth Category Level';
        caseWizard.filterCategoryLookup2List();
  
        System.assert(caseWizard.categoryLookupSelectedRecord != null);
        System.assert(caseWizard.knowledgeArticleVersionRecordList != null);
        System.assertEquals(caseWizard.showQonciergeSubmission, true);
        
        caseWizard.nextCaseDetail();
        
        System.assert(caseWizard.caseObj != null);
        System.assert(caseWizard.unauthenticatedUser == true);
        
        
        caseWizard.getThis();
        
        Responsible_Partner__c responsiblePartner = new Responsible_Partner__c();
        responsiblePartner.Partner__c = partnerAccount.Id;
        responsiblePartner.End_User__c = testAccount.Id;
        
        insert responsiblePartner;

        testAccount = [SELECT Responsible_Partner_count__c FROM Account WHERE Id=:testAccount.Id];

        Test.startTest();
        
        // Running as the community user
        System.runAs(communityUser) {
            
            caseWizard = new QS_CaseWizard_Controller();
        }
        
        delete responsiblePartner;
            
        System.runAs(communityUser) {  
            
            caseWizard = new QS_CaseWizard_Controller();
            
            caseWizard.selectedFilter1 = 'First Category Level';
            caseWizard.filterCategoryLookupList();
            caseWizard.selectedFilter2 = 'Second Category Level';
            caseWizard.filterCategoryLookup2List();
            
            caseWizard.selectedFilter3 = 'Third Category Level Type';
            caseWizard.filterCategoryLookup3List();
            caseWizard.nextCaseDetail();
            System.assertNotEquals(caseWizard.caseObj.Severity__c, null);
            caseWizard.caseObj.Subject = 'Test Subject';
            caseWizard.caseObj.Description = 'Test Description';
            caseWizard.selectedSeverityLevel = '1';
            caseWizard.setSeverityLevelFields();
            caseWizard.nextCaseDetail();
            
            caseWizard.selectedSeverityLevel = null;
            caseWizard.setSeverityLevelFields();
            
            caseWizard.selectedSeverityLevel = '3';
            caseWizard.setSeverityLevelFields();
            caseWizard.nextCaseDetail();
            
            
            caseWizard.selectedSeverityLevel =  caseWizard.caseObj.Severity__c;
            caseWizard.setSeverityLevelFields();
            caseWizard.searchKnowledgeArticle();
            caseWizard.nextCaseDetail();
            
            //Changing to the second category level
            
            caseWizard.selectedFilter3 = 'Fourth Category Level Type';
            caseWizard.filterCategoryLookup3List();
            caseWizard.nextCaseDetail();
            System.assertNotEquals(caseWizard.caseObj.Severity__c, null);
           
            
            caseWizard.selectedFilter2 = 'Fifth Category Level';
            caseWizard.filterCategoryLookup2List();
            caseWizard.selectedFilter1 = 'Seventh Category Level';
            caseWizard.filterCategoryLookupList();
            caseWizard.selectedFilter2 = 'Tenth Category Level';
            caseWizard.filterCategoryLookup2List();
      
            System.assert(caseWizard.categoryLookupSelectedRecord != null);
            
            System.assert(caseWizard.knowledgeArticleVersionRecordList != null && !caseWizard.knowledgeArticleVersionRecordList.isEmpty());
            
            caseWizard.nextCaseDetail();
            System.assert(caseWizard.caseObj != null);
            System.assert(caseWizard.unauthenticatedUser != true);
            
            caseWizard.caseObj.Severity__c = '1';
            caseWizard.populateSupportDetailInfo();
            caseWizard.selectedCountry = 'United Kingdom';
            caseWizard.caseObj.Subject = null;
            caseWizard.caseObj.Description = null;
            
            caseWizard.populateSupportDetailInfo();
            caseWizard.nextCaseDetail();
            
            caseWizard.caseObj.Severity__c = '3';
            caseWizard.caseObj.Subject = 'Test Subject';
            caseWizard.caseObj.Description = 'Test Description';
            caseWizard.nextCaseDetail();
            
            // Seting the environment
            QS_EnvironmentController environmentController = new QS_EnvironmentController();
            environmentController.caseWizardRef = caseWizard;
            environmentController.selectedEnvironment = environment.Id;
            environmentController.setEnviron();
            
            
            caseWizard.setEnvironmentRelatedInfo();
            caseWizard.caseObj.Subject = null;
            caseWizard.submitCase();
            caseWizard.subjectText = 'Test Subject';
            caseWizard.descriptionText = 'Test Description - New';
            caseWizard.caseObj.Subject = 'Test Subject';
            caseWizard.caseObj.Description = 'Test Description - New';
            caseWizard.selectedSeverityLevel = '3';

            caseWizard.nextCaseDetail();
            // Seting the environment
            environmentController = new QS_EnvironmentController();
            environmentController.caseWizardRef = caseWizard;
            environmentController.selectedEnvironment = environment.Id;
            environmentController.setEnviron();
            caseWizard.selectedEnvironement = environment.Id;
            caseWizard.showEnvironment = true;
            caseWizard.setEnvironmentRelatedInfo();
            PageReference caseDetailPageRef = caseWizard.submitCase();
            
            
            QS_CaseWizard_Controller.getAccountLookupList();
            QS_CaseWizard_Controller.selectedAccountOrigin = partnerAccount.Id;
            caseWizard.setSelectedAccountContactOrigin();
            QS_CaseWizard_Controller.getRelatedContactLookupList(String.valueOf(partnerAccount.Id));
            QS_CaseWizard_Controller.selectedContactOrigin = partnerContact.Id;
            caseWizard.setSelectedAccountContactOrigin();
            
            ApexPages.currentPage().getParameters().put('selectedCategory1', 'First Category Level');
            ApexPages.currentPage().getParameters().put('selectedCategory2', 'Second Category Level');
            ApexPages.currentpage().getParameters().put('selectedCategory3', 'Third Category Level');
            
            caseWizard = new QS_CaseWizard_Controller();
            
            String insertedCaseId = caseDetailPageRef.getParameters().get('caseId');
            String referredCategoryId = caseDetailPageRef.getParameters().get('categoryId');
            ApexPages.currentPage().getParameters().put('caseId', insertedCaseId);
            ApexPages.currentPage().getParameters().put('categoryId', referredCategoryId);

            // JUST to cover code
            caseWizard.redirect();
            caseWizard.checkCaseBeforeCreation();
            caseWizard.FilterAccounts();
            caseWizard.refreshContactList();
            caseWizard.loadgetContactOriginList();            
        }
                    
        Test.stopTest();
        
    }
    static testMethod void partnerTest() {

        QTTestUtils.GlobalSetUp();
        
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;
        
        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile partnerProfileRec = [select id from profile where Name = 'PRM - Base' limit 1]; 
        String partnerProfileId = partnerProfileRec != null && partnerProfileRec.Id != null ? partnerProfileRec.Id : null;
        
        // Create Community User
        User partnerUser = TestDataFactory.createUser(partnerProfileId, 'testSandboxPartner@qlikTech.com', 'tPart', String.valueOf(partnerContact.Id));
        insert partnerUser; 

        System.runAs(partnerUser) {
            QS_CaseWizard_Controller caseWizard = new QS_CaseWizard_Controller();
            
            caseWizard.selectedFilter1 = 'Seventh Category Level';
            caseWizard.filterCategoryLookupList();
            caseWizard.selectedFilter2 = 'Tenth Category Level';
            caseWizard.filterCategoryLookup2List();
            caseWizard.nextCaseDetail();
        }
    } 
        
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}