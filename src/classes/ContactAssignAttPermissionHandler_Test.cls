/********************************************************
* CLASS: ContactAssignAttPermissionHandler_Test
* DESCRIPTION: This is the test class for ContactAssignAttPermissionHandler.
* Code Coverage 96%
* CHANGELOG:
*   2020-06-22 - UIN - ADded initial test class logic
*********************************************************/   
@isTest
public class ContactAssignAttPermissionHandler_Test{

    //Test method for assigning permission set to users
    @isTest    
    public static void testingPermissionAssingment() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) { 
                ContactAssignAttPermissionHandler ctap = new ContactAssignAttPermissionHandler();
                QuoteTestHelper.createCustomSettings();
                
                QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
                insert qtc;
                Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
                insert testAccount;
                Contact testContactExt = TestDataFactory.createContact('testContactExt', 'test_LName', 'testSandbox999@qlikTech.com', '+44-7878787878', testAccount.Id);
                insert testContactExt;
                Profile p = [SELECT Id FROM Profile WHERE Name = 'PRM - Independent Territory + QlikBuy'];
                User u = new User(Alias = 'admtus', Email = 'testSandbox999@qlikTech.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US', ProfileId = p.Id, Country = 'Sweden', contactid = testContactExt.Id,
                TimeZonesIdKey = 'America/Los_Angeles', Username = 'admtus' + System.now().millisecond() + '@chemtest.com',
                UserPreferencesDisableAllFeedsEmail = false,FederationIdentifier ='qlik_testpsa');
                insert u;
                ULC_Details__c ulcd = new ULC_Details__c(ContactId__c = testContactExt.Id, ULCStatus__c ='Active', 
                                                        ULCName__c='qlik_testpsa');
                insert ulcd;
            Test.StartTEst();  
                Map<Id, Contact> mTestcontact1 = new  Map<Id, Contact>([select id, email,SFDCAccessGranted__c from Contact where id = :testContactExt.id]);
                system.debug('iAMHEREDOG'+mTestcontact1);
                testContactExt.SFDCAccessGranted__c = 'DI Support Portal';   
                update testContactExt;
                Map<Id, Contact> mTestcontact2 = new  Map<Id, Contact>([select id, email,SFDCAccessGranted__c from Contact where id = :testContactExt.id]);
                system.debug('iAMHEREDOG2'+mTestcontact2 );
                List<Contact> tctList = new List<Contact>{mTestcontact2.values()};
                List<Contact> tctoldList = new List<Contact>{mTestcontact1.values()};
                ContactAssignAttPermissionHandler.handleAfterUpdate(tctList, tctoldList);
            Test.StopTEst();  
        }
    
    }
    
    //Test method for removing permission set assigned to users
    @isTest    
    public static void testingPermissionRemoval() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) { 
                ContactAssignAttPermissionHandler ctap = new ContactAssignAttPermissionHandler();
                QuoteTestHelper.createCustomSettings();
                
                QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
                insert qtc;
                Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
                insert testAccount;
                Contact testContactExt = TestDataFactory.createContact('testContactExt', 'test_LName', 'testSandbox999@qlikTech.com', '+44-7878787878', testAccount.Id);
                testContactExt.SFDCAccessGranted__c = 'DI Support Portal';
                insert testContactExt;
                Profile p = [SELECT Id FROM Profile WHERE Name = 'PRM - Independent Territory + QlikBuy'];
                User u = new User(Alias = 'admtus', Email = 'testSandbox999@qlikTech.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US', ProfileId = p.Id, Country = 'Sweden', contactid = testContactExt.Id,
                TimeZonesIdKey = 'America/Los_Angeles', Username = 'admtus' + System.now().millisecond() + '@chemtest.com',
                UserPreferencesDisableAllFeedsEmail = false,FederationIdentifier ='qlik_testpsa');
                insert u;
                ULC_Details__c ulcd = new ULC_Details__c(ContactId__c = testContactExt.Id, ULCStatus__c ='Active', 
                                                        ULCName__c='qlik_testpsa');
                insert ulcd;
                PermissionSet ps = [select id,name from permissionset where name = 'Access_to_Attunity_Customer_Portal' limit 1];
                system.debug('iAMHEREDOGps'+ps);
                PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = u.Id);
                insert psa;
                system.debug('iAMHEREDOGpsa'+psa);
            Test.StartTEst();  
                Map<Id, Contact> mTestcontact1 = new  Map<Id, Contact>([select id, email,SFDCAccessGranted__c from Contact where id = :testContactExt.id]);
                system.debug('iAMHEREDOG'+mTestcontact1);
                testContactExt.SFDCAccessGranted__c = 'LeadsOpps';   
                update testContactExt;
                Map<Id, Contact> mTestcontact2 = new  Map<Id, Contact>([select id, email,SFDCAccessGranted__c from Contact where id = :testContactExt.id]);
                system.debug('iAMHEREDOG2'+mTestcontact2 );
                List<Contact> tctList = new List<Contact>{mTestcontact2.values()};
                List<Contact> tctoldList = new List<Contact>{mTestcontact1.values()};
                ContactAssignAttPermissionHandler.handleAfterUpdate(tctList, tctoldList);
            Test.StopTEst();  
        }
    
    }

}