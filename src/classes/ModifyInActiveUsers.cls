/*
* File ModifyInActiveUsers
    * @description : create the necessary logic to work with user profile
    * @author : Maksim Karpitski
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       18.07.2018   ext_bjd             Created Class
    2       25.01.2019   ext_bjd             ITRM-301 Added logic for activation of users with license limit exceeded
    3       30.03.2020   ext_bjd             ITRM-409 Activate SPRING CM Users, code refactoring
    4       04.05.2020   ext_bjd             ITRM-463 code refactoring to decrease long-running tests
    5       29.06.2020   ext_bjd             ITRM-464 Initial Development
    6       01.07.2020   ext_bjd             ITRM-487 Initial Development
  */

public class ModifyInActiveUsers {
    public static final String DEVQ_SANDBOX = 'DEVQ';
    public static final String QA_SANDBOX = 'QA';

    public static void runUserUpdatesWithLicenses(String sandboxName) {
        Map<Id, User> activeUsersByIds = new Map<Id, User>();
        Map<Id, User> inActiveUsersByIds = new Map<Id, User>();
        List<User> activeOnlyInSandboxesUsers = [
                SELECT Id, Name, Country, Email, Last_Profile__c, Profile.Name, IsActive,
                        activeOnlyInSandboxes__c, SpringCMEos__SpringCM_User__c
                FROM User
                WHERE activeOnlyInSandboxes__c = TRUE AND (NOT Name LIKE '%retired%')
        ];
        for (User us : activeOnlyInSandboxesUsers) {
            if (us.IsActive) {
                activeUsersByIds.put(us.Id, us);
            } else {
                inActiveUsersByIds.put(us.Id, us);
            }
        }


        if (!activeUsersByIds.isEmpty() || inActiveUsersByIds.isEmpty()) {
            //Deactivate execution trigger on User object
            CustomUserTriggerHandler.ENABLE_USER_TRIGGER = false;
            if (sandboxName.toUpperCase() == DEVQ_SANDBOX || sandboxName.toUpperCase() == QA_SANDBOX) {
                //Deactivate SpringCM Users in DEVQ and QA
                Database.executeBatch(new UpdateSpringCMUsersBatch(inActiveUsersByIds.keySet()));
                //Activate Active users and change invalid emails
                activateUsers(activeUsersByIds.values());
            }

            Integer amountInActiveUsersForActivation = inActiveUsersByIds.size();

            //Define the list of profiles which are can use in order not to exceed the limit on available licenses
            List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name = 'Custom: QlikBuy Sales Std User'];

            //Define the list of users to be identified as an exceptional users
            Set<Id> exceptionalUserIds = exceptionalUserIds();

            //Define the number of available user licenses
            Integer availableLicenses = availableLicenses();

            //Set previous Profile
            List<User> inActiveUsersWithModifiedProfiles = ModifyInActiveUsers.setPreviousProfile(inActiveUsersByIds.values());

            //Deactivate necessary users for get additional licenses
            if (availableLicenses < amountInActiveUsersForActivation) {
                Integer amountAdditionalLicenses = amountInActiveUsersForActivation - availableLicenses;
                List<User> notUsedUsers = identifyNotUsedUsers(inActiveUsersByIds.keySet(), profiles, exceptionalUserIds, amountAdditionalLicenses);

                //Deactivation process take a lot of time during tests executions, added isRunningTest to exclude this issue
                if (!Test.isRunningTest()) {
                    deactivateUsers(notUsedUsers);
                }
            }

            //Activate inActive users and change invalid emails
            activateUsers(inActiveUsersWithModifiedProfiles);
        }
    }

    public static void activateUsers(List<User> activateUsers) {
        List<User> updateUsers = new List<User>();
        Database.SaveResult[] srList = null;
        String addedPhraseInvalid = '.invalid';
        for (User us : activateUsers) {
            us.IsActive = true;
            us.SpringCMEos__SpringCM_User__c = true;
            us.Email = us.Email.remove(addedPhraseInvalid);
            updateUsers.add(us);
        }
        if (!updateUsers.isEmpty()) {
            srList = Database.update(updateUsers, false);
        }
        // Iterate through each returned result
        if (!srList.isEmpty()) {
            for (Integer i = 0; i < srList.size(); i++) {
                getDebugSaveResults(srList.get(i));
            }
        }
    }

    public static List<User> identifyNotUsedUsers(Set<Id> activeUsersIds, List<Profile> profiles, Set<Id> excludedUserIds, Integer amountAdditionalLicenses) {
        Set<Id> profileIds = new Set<Id>();
        for (Profile prf : profiles) {
            profileIds.add(prf.Id);
        }
        List<User> notUsedUsers = new List<User>([
                SELECT Id, Name, Country, IsActive, Approval_Manager__c, Prime_User__c
                FROM User
                WHERE Id NOT IN :activeUsersIds
                AND IsActive = TRUE
                AND Id NOT IN :excludedUserIds
                AND Profile.Id IN :profileIds
                LIMIT :amountAdditionalLicenses
        ]);
        return notUsedUsers;
    }

    public static void deactivateUsers(List<User> deactivateUsers) {
        List<User> updateUsers = new List<User>();
        Database.SaveResult[] srList = null;
        for (User us : deactivateUsers) {
            us.Approval_Manager__c = null;
            us.Quote_Approval_Role__c = 'CEO';
            us.IsActive = false;
            updateUsers.add(us);
        }
        if (!updateUsers.isEmpty()) {
            srList = Database.update(updateUsers, false);
        }
        // Iterate through each returned result
        if (!srList.isEmpty()) {
            for (Integer i = 0; i < srList.size(); i++) {
                getDebugSaveResults(srList.get(i));
            }
        }
    }

    public static Set<Id> exceptionalUserIds() {
        Set<Id> userIds = new Set<Id>();
        for (Exceptional_User__mdt exceptionalUserUser : [SELECT User_Id__c FROM Exceptional_User__mdt]) {
            userIds.add(exceptionalUserUser.User_Id__c);
        }
        return userIds;
    }

    public static Integer availableLicenses() {
        UserLicense userLicenses = [SELECT TotalLicenses, UsedLicenses FROM UserLicense WHERE Name = 'Salesforce' LIMIT 1];
        Integer availableLicenses = (!Test.isRunningTest()) ? (userLicenses.TotalLicenses - userLicenses.UsedLicenses) : 0;
        return availableLicenses;
    }

    public static void getDebugSaveResults(Database.SaveResult sr) {
        if (sr.isSuccess()) {
            // Operation was successful, get the ID of the record that was processed
            System.debug('Successfully update user. User ID: ' + sr.getId());
        } else if (!sr.isSuccess()) {
            System.debug('Error in update user. User ID: ' + sr.getId());
            // Operation failed, get all errors
            for (Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('User fields that affected this error: ' + err.getFields());
            }
        }
    }

    public static List<User> setPreviousProfile(List<User> usersWithPreviousProfiles) {
        List<User> usersForUpdate = new List<User>();
        List<String> profilesNames = new List<String>();
        for (User us : usersWithPreviousProfiles) {
            if (String.isNotBlank(us.Last_Profile__c)) {
                profilesNames.add(us.Last_Profile__c);
            }
        }
        Map<String, Id> profileNameById = new Map<String, Id>();
        List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name IN :profilesNames];
        for (Profile p : profiles) {
            profileNameById.put(p.Name, p.Id);
        }
        for (User us : usersWithPreviousProfiles) {
            if (String.isNotBlank(us.Last_Profile__c) && profileNameById.containsKey(us.Last_Profile__c)) {
                us.ProfileId = profileNameById.get(us.Last_Profile__c);
                usersForUpdate.add(us);
            }
        }
        return usersForUpdate;
    }
}