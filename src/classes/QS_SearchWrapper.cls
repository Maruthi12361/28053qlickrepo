/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_SearchWrapper {
		public string   title       {get; set;}
		public string   description {get; set;}
		public string   url         {get; set;}
		//public string   author      {get; set;}
		public string   dateAdded   {get; set;}
		public datetime dateAddedDT {get; set;}
		public boolean isVisibleInPkb {get;set;}
		public string articleId {get;set;}
		public double views {get;set;}
		public double rating {get;set;}
}