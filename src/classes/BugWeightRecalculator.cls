/*********************************************
Change Log:

2020-05-05 AIN IT-2776 created to recalculate the bug weight in real time
********************************************/
public class BugWeightRecalculator{
    public static void recalculateBugWeight(List<Bugs__c> triggerNew, Map<Id, Bugs__c> triggerOldMap) {
        Support_Bug_Weight_Calculations__mdt bugWeightMdt = null;
        
        system.debug('BugWeightRecalculation.recalculateBugWeight start');
        Map<Id, Bugs__c> bugsToRecalculate = new Map<Id, Bugs__c>();
        
        for(Bugs__c b : triggerNew){
            //Bug weight on the case, update bug weight
            if(b.Bug_Weight_Recalculate__c) {
                bugsToRecalculate.put(b.id, b);
                system.debug('Bug to recalculate: ' + b.id);
            }
            b.Bug_Weight_Recalculate__c = false;
        }
        
        if(bugsToRecalculate.size() > 0){
            Map<id, AggregateResult> cases = new Map<Id, AggregateResult>([SELECT Bug__c Id, max(Bug_Weight_Case__c) BugWeightValue, Count(Id) CaseCount FROM Case where Bug__c in :bugsToRecalculate.keySet() group by Bug__c]);
            system.debug('cases.size(): ' + cases.size());
            
            for(Id bid : bugsToRecalculate.keySet()) {
                
                if(cases.containsKey(bid)) {
                    system.debug('Getting bug weight from cases');
                    for (AggregateResult c : cases.Values()){
                        
                        Id bugId = (id)c.get('Id');
                        Integer caseCount = (Integer)c.get('CaseCount');
                        if(bugId == bid){
                            Decimal caseBugWeightValue = (decimal)c.get('BugWeightValue');
                            system.debug('BugId: ' + bugId);
                            system.debug('Case bug weight: ' + caseBugWeightValue);
                            system.debug('Number of cases: ' + caseCount);
                            if(bugsToRecalculate.get(bugId).Bug_Weight_Case__c != caseBugWeightValue){
                                bugsToRecalculate.get(bugId).Bug_Weight_Case__c = caseBugWeightValue;
                                system.debug('Setting new bug weight on bug ' + bugId + ' to ' + caseBugWeightValue);
                            }
                            if(bugsToRecalculate.get(bugId).Number_of_Cases__c != caseCount){
                                bugsToRecalculate.get(bugId).Number_of_Cases__c = caseCount;
                                system.debug('Setting number of cases on ' + bugId + ' to ' + caseCount);
                            }
                        }
                    }
                }
                else{
                    system.debug('Setting new bug weight on bug ' + bid + ' to ' + 0);
                    bugsToRecalculate.get(bid).Number_of_Cases__c = 0;
                    bugsToRecalculate.get(bid).Bug_Weight_Case__c = 0;
                }
            }
        }
        system.debug('BugWeightRecalculation.recalculateBugWeight end');
    }
}