/**************************************************
*
* Change Log:
* 
* 2017-03-06   Created by external vendor as part of the NVM Replacement project - tests RecordingControlsController
*              CCE - Populated extra task fields as required to work with our Task record type.
*
**************************************************/
@isTest
public class RecordingControlsControllerTest {
    public static testMethod void retrieveControlButtonsLabels_Test() {
        String labelsJSON = RecordingControlsController.retrieveControlButtonsLabels();

        System.assert(labelsJSON.contains('\"RecordingRecordTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingRecordText\"'));
        System.assert(labelsJSON.contains('\"RecordingPauseTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingPauseText\"'));
        System.assert(labelsJSON.contains('\"RecordingResumeTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingResumeText\"'));
        System.assert(labelsJSON.contains('\"RecordingRestartTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingRestartText\"'));
        System.assert(labelsJSON.contains('\"RecordingDeleteTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingDeleteText\"'));
        System.assert(labelsJSON.contains('\"RecordingMetadataTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingMetadataText\"'));
        System.assert(labelsJSON.contains('\"RecordingStartTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingStartText\"'));
        System.assert(labelsJSON.contains('\"RecordingStopTitle\"'));
        System.assert(labelsJSON.contains('\"RecordingStopText\"'));
        System.assert(labelsJSON.contains('\"RecordingDisabledText\"'));
        System.assert(labelsJSON.contains('\"RecordingControls\"'));
    }

    public static testMethod void retrieveUserInfo_Test() {
        User u = new user();

        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;

        Profile p = [select id from profile where name='Standard User'];

        u.cnx__Agent_PasswordEncrypted__c = 'cnxAgentPasswordEncrypted';
        u.cnx__Agent_Id__c = 'cnxAgentId';
        u.cnx__Agent_Phone_Extension__c = 'cnxAgentPhoneExtension';
        u.FirstName = 'AgentFirstName';
        u.LastName = 'AgentLastName';
        u.Alias = 'AgAlias';
        u.CommunityNickname = 'AgentCommunityNickname2';
        u.Email = uniqueName + '@test' + orgId + '.org';
        u.Username = uniqueName + '@test' + orgId + '.org';
        u.ProfileId = p.Id;
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        insert u;

        System.runAs(u) {
            String userInfoJSON = RecordingControlsController.retrieveUserInfo();

            // --- FIELD NAMES ---
            System.assert(userInfoJSON.contains('\"cnx__Agent_PasswordEncrypted__c\"'));
            System.assert(userInfoJSON.contains('\"cnx__Agent_Phone_Extension__c\"'));
            System.assert(userInfoJSON.contains('\"FirstName\"'));
            System.assert(userInfoJSON.contains('\"LastName\"'));
            System.assert(userInfoJSON.contains('\"Alias\"'));
            System.assert(userInfoJSON.contains('\"CommunityNickname\"'));

            // --- FIELD NAMES ---
            System.assert(userInfoJSON.contains('\"cnxAgentPasswordEncrypted\"'));
            System.assert(userInfoJSON.contains('\"cnxAgentId\"'));
            System.assert(userInfoJSON.contains('\"cnxAgentPhoneExtension\"'));
            System.assert(userInfoJSON.contains('\"AgentFirstName\"'));
            System.assert(userInfoJSON.contains('\"AgentLastName\"'));
            System.assert(userInfoJSON.contains('\"AgAlias\"'));
            System.assert(userInfoJSON.contains('\"AgentCommunityNickname2\"'));
        }
    }

    public static testMethod void writeRecordingLink_Test() {
        Task t = new Task();
        t.Subject = 'Call 2017-02-15 11:29:46';
        t.CallType = 'Inbound';
        t.cnx__CTIInfo__c = 'ANI: 46709623241, DNIS: 71000, DialedNumber: 011397';
        insert t;

        String id = t.Id;
        RecordingControlsController.writeRecordingLink(id, 'CallDisposition', 'https://www.example.com/recording');

        Task result = [SELECT
            CallDisposition
            FROM Task
            WHERE Id = :id
            LIMIT 1];

        System.assertEquals(result.CallDisposition, 'https://www.example.com/recording');
    }

    public static testMethod void writeRecordingLink_TaskDoesNotExist_Test() {
        Task t = new Task();
        t.Subject = 'Call 2017-02-15 11:29:46';
        t.CallType = 'Inbound';
        t.cnx__CTIInfo__c = 'ANI: 46709623241, DNIS: 71000, DialedNumber: 011397';
        insert t;

        String id = t.Id;

        //Id does not exist
        try {
            RecordingControlsController.writeRecordingLink('123', 'CallDisposition', 'https://www.example.com/recording');
            System.assert(false);
        } catch (RecordingControlsController.RecordingControlsException ex) {
            System.assert(ex.getMessage().startsWith('Task with id 123 not found. Exception: '));
        }

        //Field does not exist
        try {
            RecordingControlsController.writeRecordingLink(id, 'FieldDoesNotExist', 'https://www.example.com/recording');
            System.assert(false);
        } catch (RecordingControlsController.RecordingControlsException ex) {
            System.assert(ex.getMessage().startsWith('Task with id ' + id + ' not found. Exception: '));
        }
    }

    public static testMethod void writeRecordingLink_WrongFieldType_Test() {
        Task t = new Task();
        t.Subject = 'Call 2017-02-15 11:29:46';
        t.CallType = 'Inbound';
        t.cnx__CTIInfo__c = 'ANI: 46709623241, DNIS: 71000, DialedNumber: 011397';
        insert t;

        String id = t.Id;

        try {
            RecordingControlsController.writeRecordingLink(id, 'CallDurationInSeconds', 'https://www.example.com/recording');
            System.assert(false);
        } catch (RecordingControlsController.RecordingControlsException ex) {
            System.assertEquals(ex.getMessage(), 'Field CallDurationInSeconds is not of type String or URL');
        }
    }

    public static testMethod void writeRecordingLink_LinkToLong_Test() {
        Task t = new Task();
        t.Subject = 'Call 2017-02-15 11:29:46';
        t.CallType = 'Inbound';
        t.cnx__CTIInfo__c = 'ANI: 46709623241, DNIS: 71000, DialedNumber: 011397';
        insert t;

        String id = t.Id;
        String link = '';

        Integer count = 0;
        while(count < 300) {
            link += 'a';
            count++;
        }

        try {
            RecordingControlsController.writeRecordingLink(id, 'CallDisposition', link);
            System.assert(false);
        } catch (RecordingControlsController.RecordingControlsException ex) {
            System.assertEquals(ex.getMessage(), 'Field CallDisposition is not long enough (must be at least 300 to save that link)');
        }
    }
}