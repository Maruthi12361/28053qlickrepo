/*****************************************************************************************************************
Change Log:
20150202	CCE Initial development - test class for UpdateFollowUpRequiredType.trigger
20150724    CCE CR# 49405 - Update test class
20160422    CCE CR# 82611 - https://eu1.salesforce.com/a0CD000000xSrSI Velocify Lead Lifecycle Changes - Removed test for "Not Follow-Up Required Reason" field update as field no longer exists.
20180613    CCE CHG0034173 - updated test_newLead() to take account of changes made to LeadUpdateFollowUpRequiredTypeHandler.cls
20200303    CCE To be deactivated/removed as part of CHG0037726 DGM-55 FUR Revamp 2020
			 
******************************************************************************************************************/
@isTest
private class UpdateFollowUpRequiredTypeTest {
	
  @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
       }
       
	static testMethod void test_newLead() {
		LeadUpdateFollowUpRequiredTypeHandler lfuh = new LeadUpdateFollowUpRequiredTypeHandler();
		// Lead testlead = new Lead( LastName='Test', 
        //       FirstName='FollowUpRequiredType', 
        //       Country='UK',
        //       Email='lds@test.com',             
        //       LeadSource = 'PART - Partner',
        //       Company ='FUTesting',
        //       New_Follow_Up_Required__c = true);
        
        // insert testlead;

		// Lead retLead = [select Id, Follow_Up_Required_Type__c, Status from Lead where Id = :testlead.Id];
		// //If we are creating a Lead and the Overall Follow-Up Required field is true then we would expect the Follow-Up Required Type field to be set to New
		// System.assertEquals('New', retLead.Follow_Up_Required_Type__c);
		// System.assertEquals('Follow-Up Required', retLead.Status);
	}
	
/*	@isTest static void test_updatedLead() {
		
		Lead testlead = new Lead( LastName='Test2', 
              FirstName='FollowUpRequiredType', 
              Country='UK',
              Email='lds@test.com',             
              LeadSource = 'PART - Partner',
              Company ='FUTesting',
              Overall_Follow_up_Required__c = false,
              Status = 'Follow-Up Disqualified',
              Follow_Up_Disqualified_Reason__c = 'No Interest');
        
        insert testlead;
		Lead retLead = [select Id, Follow_Up_Required_Type__c, Status from Lead where Id = :testlead.Id];
		System.assertEquals(null, retLead.Follow_Up_Required_Type__c);

		test.startTest();
    Semaphores.LeadTriggerHandlerBeforeUpdate = false;
    Semaphores.LeadTriggerHandlerBeforeUpdate2 = false;

		testlead.New_Follow_Up_Required__c = true;
		update testlead;
		
		retLead = [select Id, Follow_Up_Required_Type__c, Status from Lead where Id = :testlead.Id];
		//If we are updating a Lead and the Overall Follow-Up Required field is changing from false to true and the Lead Status
		// is set to Follow-Up Disqualified (or Follow-Up Rejected) then we would expect the Follow-Up Required Type field to be set to Revived
		System.assertEquals('Follow-Up Required', retLead.Status);
		System.assertEquals('Revived', retLead.Follow_Up_Required_Type__c);
		test.stopTest();
	}
    
    @isTest static void test_ClearFollowUpDisqualified() {
        
        Lead testlead = new Lead( LastName='Test2', 
              FirstName='FollowUpRequiredType', 
              Country='UK',
              Email='lds@test.com',             
              LeadSource = 'PART - Partner',
              Company ='FUTesting',
              Overall_Follow_up_Required__c = false,
              Status = 'Follow-Up Disqualified',
              Follow_Up_Disqualified_Reason__c = 'No Response');
        
        insert testlead;
        Lead retLead = [select Id, Follow_Up_Required_Type__c, Status from Lead where Id = :testlead.Id];
        System.assertEquals(null, retLead.Follow_Up_Required_Type__c);
        
        test.startTest();

        //setting the Status field to Unknown should cause the trigger to clear the Follow_Up_Disqualified_Reason__c field.           
        testlead.Status = 'Unknown';
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update testlead;
        
        retLead = [select Id, Follow_Up_Disqualified_Reason__c, Status from Lead where Id = :testlead.Id];
        System.assertEquals(null, retLead.Follow_Up_Disqualified_Reason__c);
        test.stopTest();
    }
    
    @isTest static void test_ClearFollowUpRejected() {
        
        Lead testlead = new Lead( LastName='Test2', 
              FirstName='FollowUpRequiredType', 
              Country='UK',
              Email='lds@test.com',             
              LeadSource = 'PART - Partner',
              Company ='FUTesting',
              Overall_Follow_up_Required__c = false,
              Status = 'Follow-Up Rejected',
              Follow_Up_Rejected_Reason__c = 'Already Working');
        
        insert testlead;
        Lead retLead = [select Id, Follow_Up_Required_Type__c, Status from Lead where Id = :testlead.Id];
        System.assertEquals(null, retLead.Follow_Up_Required_Type__c);
        
        test.startTest();

        //setting the Status field to Unknown should cause the trigger to clear the Follow_Up_Rejected_Reason__c field.           
        testlead.Status = 'Unknown';
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update testlead;
        
        retLead = [select Id, Follow_Up_Rejected_Reason__c, Status from Lead where Id = :testlead.Id];
        System.assertEquals(null, retLead.Follow_Up_Rejected_Reason__c);
        test.stopTest();
    }*/
    
/*    @isTest static void test_ClearFollowUpNotFollowUpRequiredReason() {
        
        Lead testlead = new Lead( LastName='Test2', 
              FirstName='FollowUpRequiredType', 
              Country='UK',
              Email='lds@test.com',             
              LeadSource = 'PART - Partner',
              Company ='FUTesting',
              Overall_Follow_up_Required__c = false,
              Status = 'Follow-Up Rejected',
              Follow_Up_Rejected_Reason__c = 'Not Follow-Up Required',
              Not_Follow_Up_Required_Reason__c = 'Student');
        
        insert testlead;
        Lead retLead = [select Id, Follow_Up_Required_Type__c, Status from Lead where Id = :testlead.Id];
        System.assertEquals(null, retLead.Follow_Up_Required_Type__c);
        
        test.startTest();

        //setting the Follow_Up_Rejected_Reason__c field to Already Working should cause the trigger to clear the Not_Follow_Up_Required_Reason__c field.           
        testlead.Follow_Up_Rejected_Reason__c = 'Already Working';
        update testlead;
        
        retLead = [select Id, Follow_Up_Rejected_Reason__c, Not_Follow_Up_Required_Reason__c, Status from Lead where Id = :testlead.Id];
        System.assertEquals(null, retLead.Not_Follow_Up_Required_Reason__c);
        test.stopTest();
    }*/
	
}