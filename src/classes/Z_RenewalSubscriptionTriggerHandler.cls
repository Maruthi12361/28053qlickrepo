/**     
 * File Name : Z_RenewalSubscriptionTriggerHandler
 * Description : This is a 360 subscription trigger handler to update the related quotes' fields:
 *               1. zqu__Is_Charge_Expired__c
 *               2. zqu__ExistSubscriptionID__c
 *               3. zqu__SubscriptionVersion__c
 * @author : Inki Hong
 * Modification Log ======================================================================
 * Ver     Date         Author         Modification
 * 1       05.18.2020   Inki Hong      Created new class (RTR-477)
 */
public with sharing class Z_RenewalSubscriptionTriggerHandler {
    private static final String CLASS_NAME = 'Z_RenewalSubscriptionTriggerHandler',
                                DEBUG_PREFIX = '> $ ' + CLASS_NAME;
    public static void handleAfterInsert(Map<Id, Zuora__Subscription__c> newSubsMap) {
        Id renewalOpptyRecordTypeId = Schema.SObjectType.Opportunity
                                      .getRecordTypeInfosByName()
                                      .get('Subscription Renewal')
                                      .getRecordTypeId();
        Map<String, Map<String, Object>> subscriptionNumberToIdMap = new Map<String, Map<String, Object>>();
        Map<String, Decimal> subscriptionNameToRenewalTermMap = new Map<String, Decimal>();
        Map<String, Boolean> subscriptionNameToAutoRenewMap = new Map<String, Boolean>();
        List<zqu__Quote__c> renewalQuotesToUpdate = new List<zqu__Quote__c>(); 
        List<Id> successRenewalQuotes = new List<Id>();
        List<Database.SaveResult> updateRenewalQuotesResult = new List<Database.SaveResult>();
        for (Zuora__Subscription__c s : [SELECT Id, Zuora__External_Id__c, Zuora__SubscriptionNumber__c, Zuora__Version__c, Zuora__AutoRenew__c, Zuora__RenewalTerm__c
                                         FROM Zuora__Subscription__c
                                         WHERE Id IN :newSubsMap.keySet()]) {
            subscriptionNumberToIdMap.put(s.Zuora__SubscriptionNumber__c, 
                                          new Map<String, Object> { 
                                              'Zuora__External_Id__c' => s.Zuora__External_Id__c, 
                                              'Zuora__Version__c' => s.Zuora__Version__c 
                                          });

            Decimal renewalTerm = (null == s.Zuora__RenewalTerm__c) ? 0 : Decimal.valueOf(s.Zuora__RenewalTerm__c.replaceAll('[^0-9]', '').trim());
            
            subscriptionNameToRenewalTermMap.put(s.Zuora__SubscriptionNumber__c, renewalTerm);
            subscriptionNameToAutoRenewMap.put(s.Zuora__SubscriptionNumber__c, s.Zuora__AutoRenew__c);
        }
        List<zqu__Quote__c> renewalQuotes = [SELECT Id, zqu__Is_Charge_Expired__c, zqu__ExistSubscriptionID__c, zqu__Subscription_Name__c, 
                                                    zqu__SubscriptionType__c, zqu__AutoRenew__c, zqu__RenewalTerm__c,
                                                    (SELECT Id FROM zqu__Quote_Amendments__r WHERE zqu__Type__c = 'TermsAndConditions')
                                             FROM zqu__Quote__c
                                             WHERE zqu__Subscription_Name__c IN :subscriptionNumberToIdMap.keySet()
                                             AND zqu__Status__c = 'New'
                                             AND zqu__Primary__c = TRUE
                                             AND Quote_Status__c NOT IN ('Accepted by Customer', 'Order Placed')
                                             AND zqu__Opportunity__r.RecordTypeId = :renewalOpptyRecordTypeId
                                             AND (NOT zqu__Opportunity__r.StageName LIKE '%Closed%')];
        
        for (zqu__Quote__c renewalQuote : renewalQuotes) {
            String renewalQuoteSubName = renewalQuote.zqu__Subscription_Name__c,
                   renewalQuoteSubId = renewalQuote.zqu__ExistSubscriptionID__c;
            Map<String, Object> subFields = subscriptionNumberToIdMap.get(renewalQuoteSubName);
            String updatedSubscriptionId = subFields.get('Zuora__External_Id__c').toString();
            if (renewalQuoteSubId != updatedSubscriptionId) {
                Decimal updatedSubscriptionVersion = (Decimal) subFields.get('Zuora__Version__c');
                renewalQuotesToUpdate.add(new zqu__Quote__c(Id=renewalQuote.Id,
                                                            zqu__Is_Charge_Expired__c=true,
                                                            zqu__ExistSubscriptionID__c=updatedSubscriptionId,
                                                            zqu__SubscriptionVersion__c=updatedSubscriptionVersion));
            }
        }
        updateRenewalQuotesResult = Database.update(renewalQuotesToUpdate, false);
        Integer badQuoteCounter = 0;
        String htmlMessage = '<table border="1"><tr><td>Quote ID</td><td>Affected Fields</td><td>Status Code</td><td>Error Message</td></tr>';
        
        for (Integer i = 0; i < updateRenewalQuotesResult.size(); i++) {
            Database.SaveResult result = updateRenewalQuotesResult.get(i);
            
            if (result.isSuccess()) {
                System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' renewal quote id = ' + result.getId());
                successRenewalQuotes.add(result.getId());
            } else {
                badQuoteCounter++;
                zqu__Quote__c renewalQuote = renewalQuotesToUpdate.get(i);
                String badQuoteId = renewalQuote.Id,
                       affectedFields, statusCode, errorMessage;
                for (Database.Error error : result.getErrors()) {
                    affectedFields = String.join(error.getFields(), ',');
                    statusCode = error.getStatusCode().name();
                    errorMessage = error.getMessage();
                }
                
                htmlMessage += '<tr><td>' + badQuoteId + '</td><td>' + affectedFields + '</td><td>' + statusCode + '</td><td>' + errorMessage + '</td></tr>';
            }
        }
        if (Test.isRunningTest()) {
            badQuoteCounter++;
        }
        if (badQuoteCounter > 0) {
            List<Id> distroListUsers = new List<Id>();
            List<String> distroList = new List<String>();
            String orgId = UserInfo.getOrganizationId().substring(0,15);
            for (GroupMember groupMember : [SELECT Id, UserOrGroupId 
                                            FROM GroupMember 
                                            WHERE Group.Name = 'Renewal Automation Group']) {
                
                distroListUsers.add(groupMember.UserOrGroupId);
            }
            for (User user : [SELECT Id, Email 
                              FROM User 
                              WHERE Id IN :distroListUsers]) {
                
                distroList.add(user.Email);
            }
            
            htmlMessage += '</table>';
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(distroList);
            mail.setSenderDisplayName('Z_RenewalSubscriptionTriggerHandler');
            mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Subscription Trigger Handler Failure Notification');
            mail.setHtmlBody(htmlMessage);
            
            if (!Test.isRunningTest()) { Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); }
        }
        updateTermsAmendment(renewalQuotes, subscriptionNameToRenewalTermMap, subscriptionNameToAutoRenewMap);
        if (successRenewalQuotes.size() > 0) {
            Database.executeBatch(new Z_RenewalQuoteRecalculateBatchable(successRenewalQuotes), 1);
        }
        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' [execute end] number of records in insertRenewalQuotesResult list: ' + updateRenewalQuotesResult.size());
        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' [execute end] number of records in successRenewalQuotes list: ' + successRenewalQuotes.size());
    }
    public static void updateTermsAmendment(List<zqu__Quote__c> renewalQuotes, Map<String, Decimal> subscriptionNameToRenewalTermMap, Map<String, Boolean> subscriptionNameToAutoRenewMap) {
        List<zqu__QuoteAmendment__c> termsConditionsAmendment = new List<zqu__QuoteAmendment__c>(),
                                     amendmentsToDelete = new List<zqu__QuoteAmendment__c>();
        
        for (zqu__Quote__c renewalQuote : renewalQuotes) {
            String subscriptionNameKey = renewalQuote.zqu__Subscription_Name__c;
            Decimal subscriptionRenewalTerm = subscriptionNameToRenewalTermMap.get(subscriptionNameKey);
            Boolean subscriptionAutoRenew = subscriptionNameToAutoRenewMap.get(subscriptionNameKey),
                    isRenewalTermMismatch = subscriptionRenewalTerm != renewalQuote.zqu__RenewalTerm__c,
                    isAutoRenewMismatch = subscriptionAutoRenew != renewalQuote.zqu__AutoRenew__c;
            
            if (!renewalQuote.zqu__Quote_Amendments__r.isEmpty()) {
                amendmentsToDelete.add(renewalQuote.zqu__Quote_Amendments__r);
            }
            
            if (isRenewalTermMismatch || isAutoRenewMismatch) {
                zqu__QuoteAmendment__c newAmendment = new zqu__QuoteAmendment__c(zqu__Quote__c=renewalQuote.Id, 
                                                                                 Name='Terms & Conditions Amendment',
                                                                                 zqu__Status__c='Draft',
                                                                                 zqu__Type__c='TermsAndConditions');
                String description = '';
                
                if (isAutoRenewMismatch){
                    description = 'Changed Auto Renew to ' + String.valueOf(renewalQuote.zqu__AutoRenew__c) + '. ';
                    newAmendment.zqu__AutoRenew__c = String.valueOf(renewalQuote.zqu__AutoRenew__c);
                }
                
                if (isRenewalTermMismatch){
                    description += 'Changed Renewal Term to ' + String.valueOf(renewalQuote.zqu__RenewalTerm__c) + '. ';
                    newAmendment.zqu__RenewalTerm__c = renewalQuote.zqu__RenewalTerm__c;
                }
                newAmendment.zqu__Description__c = description;
                termsConditionsAmendment.add(newAmendment);
            }
        }
        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' amendments to delete = ' + amendmentsToDelete);
        if (!amendmentsToDelete.isEmpty()){
            delete amendmentsToDelete;
        }
        if (!termsConditionsAmendment.isEmpty()){
            insert termsConditionsAmendment;
        }
    }
}