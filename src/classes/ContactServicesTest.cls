/******************************************************

    Class: ContactServicesTest

    This class test the ContactServices

    Changelog:
        2019-05-30  extbad
******************************************************/
@isTest
public class ContactServicesTest {

    public static testMethod void testCreateEmails() {
        EmailTemplate templ = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Closed_Case_Survey_SV'];
        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.Support_Survey_Template_Swedish__c = templ.Id;
        settings.Support_Feedback_Email_address__c = 'test@test.com';
        insert settings;

        Account testAct = createAndInsertAccount();

        Contact cont1 = new Contact( Email = 'testEmail1@gmail.com', FirstName = 'TestFN1', LastName = 'TestLN1',
                AccountId = testAct.Id, LeadSource='WEB - Web Activity', Preferred_Language__c = 'sv');
        insert cont1;
        Case c1 = createAndInsertCase(testAct, cont1);

        Contact cont2 = new Contact( Email = 'testEmail2@gmail.com', FirstName = 'TestFN2', LastName = 'TestLN2',
                AccountId = testAct.Id, LeadSource='WEB - Web Activity', Preferred_Language__c = 'Swedish');
        insert cont2;
        Case c2 = createAndInsertCase(testAct, cont2);

        Test.startTest();
        List<Messaging.SingleEmailMessage> mails =
                ContactServices.createEmails(new List<Contact>{cont1, cont2}, new List<Case>{c1, c2});
        Test.stopTest();

        system.assertEquals(2, mails.size());
        for (Messaging.SingleEmailMessage mail : mails) {
            system.assertEquals(templ.Id, mail.templateId);
        }
    }

    private static Account createAndInsertAccount() {
        Account testAct = new Account( Name = 'Test Account', Navision_Status__c = 'Something',
                QlikTech_Company__c = 'QlikTech Nordic AB');
        insert testAct;
        return testAct;
    }

    private static Case createAndInsertCase(Account testAct, Contact ctct) {
        Case c1 = new Case( Status = 'Closed', AccountId = testAct.Id, ContactId = ctct.Id);
        insert c1;
        return c1;
    }

    private static Contact createAndInsertContact(Account testAct) {
        Contact ctct = new Contact( Email = 'testEmail@gmail.com', FirstName = 'TestFN', LastName = 'TestLN',
                AccountId = testAct.Id, LeadSource='WEB - Web Activity', Preferred_Language__c = 'sv');
        insert ctct;
        return ctct;
    }
}