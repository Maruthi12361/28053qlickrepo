/********************************************************
* CLASS: LeanDataPopulateFieldonMatchinghandler
* DESCRIPTION: Hanldes transfer of values from Leandata matched account fields to the original lead fields.
*
*
* CHANGELOG:
*   2019-10-22 - CRW - Added Initial logic

*********************************************************/
public class LeanDataPopulateFieldonMatchinghandler {
    
    public static void handle(List<Lead> triggerNew,List<Lead> triggerOld){
        system.debug('PopulateFieldonMatchinghandler.Start');
        map <String,String> States = createStatesMap();

        for (integer i = 0; i < triggerNew.size(); i++){
            if (triggerNew[i].IsConverted == false && triggerNew[i].recordTypeid == '01220000000DOzw'){
                system.debug('value' +triggerNew[i].LeanData__Reporting_Matched_Account__c);
                if((String.isNotBlank(triggerNew[i].LeanData__Reporting_Matched_Account__c) && (triggerOld[i].LeanData__Reporting_Matched_Account__c != triggerNew[i].LeanData__Reporting_Matched_Account__c))
                  || (String.isNotBlank(triggerNew[i].LeanData_Matched_Account__c) && (triggerOld[i].LeanData_Matched_Account__c != triggerNew[i].LeanData_Matched_Account__c))){
                    system.debug('inloop');
                    triggerNew[i].Named_Account_Lead__c = triggerNew[i].LeanData_Named_Account_Lead__c;
                    triggerNew[i].OEM_Named_Account__c = triggerNew[i].LeanData_OEM_Named_Account__c;
                    if (String.isNotBlank(triggerNew[i].LeanData_Segment__c)){
                        triggerNew[i].Segment__c = triggerNew[i].LeanData_Segment__c;
                    }
                    if (String.isNotBlank(triggerNew[i].LeanData_ABM_Tier__c)){
                        triggerNew[i].ABM_Tier__c = triggerNew[i].LeanData_ABM_Tier__c;
                    }
                    if (String.isNotBlank(triggerNew[i].LeanData_Domestic_Ultimate_Dunsnumber__c)){
                        triggerNew[i].Domestic_Ultimate_DUNS_Number__c = triggerNew[i].LeanData_Domestic_Ultimate_Dunsnumber__c;
                    }
                    if (triggerNew[i].LeanData_No_of_Employees__c != null){
                        triggerNew[i].No_of_Employees__c = triggerNew[i].LeanData_No_of_Employees__c;
                    }
                    if (triggerNew[i].LeanData_CompanyDunsnumber__c != null){
                        triggerNew[i].CompanyDunsNumber = triggerNew[i].LeanData_CompanyDunsnumber__c;
                    }
                    Boolean overwrite = true;
                    system.debug('endvalue'+triggerNew[i].Country_Code__r.Name + triggerNew[i].State);
                    //Address fields check if state is same or not
                    if ((String.isNotBlank(triggerNew[i].Country) && (triggerNew[i].Country.toUpperCase() == 'USA' || triggerNew[i].Country.toUpperCase() == 'UNITED STATES' || triggerNew[i].Country.toUpperCase() == 'US' || triggerNew[i].Country.toUpperCase() == 'CANADA')) &&
                        String.isNotBlank(triggerNew[i].State)){
                            system.debug('in for state check' + triggerNew[i].State + triggerNew[i].LeanData_Mailing_State__c + States.get(triggerNew[i].State.toUpperCase()) );
                            if(String.isNotBlank(triggerNew[i].LeanData_Mailing_State__c)){
                                
                                if(triggerNew[i].State.length() == 2 && triggerNew[i].LeanData_Mailing_State__c.length() > 2 && (States.get(triggerNew[i].State.toUpperCase()).toLowerCase() != triggerNew[i].LeanData_Mailing_State__c.toLowerCase())){
                                    overwrite = false;
                                }else if(triggerNew[i].LeanData_Mailing_State__c.length() == 2 && triggerNew[i].State.length() > 2 && (States.get(triggerNew[i].LeanData_Mailing_State__c.toUpperCase()).toLowerCase() != triggerNew[i].State.toLowerCase())){
                                    overwrite = false;
                                }else if(triggerNew[i].LeanData_Mailing_State__c.length() == 2 && triggerNew[i].State.length() == 2 && (States.get(triggerNew[i].LeanData_Mailing_State__c.toUpperCase()).toLowerCase() != States.get(triggerNew[i].State.toUpperCase()).toLowerCase())){
                                    overwrite = false;
                                }else if(triggerNew[i].State.length() > 2 && triggerNew[i].LeanData_Mailing_State__c.length() > 2 && triggerNew[i].State.toLowerCase() != triggerNew[i].LeanData_Mailing_State__c.toLowerCase()){
                                    overwrite = false;
                                }
                            }else {overwrite = false;}
                }system.debug('endvalue'+triggerNew[i].Named_Account_Lead__c + triggerNew[i].OEM_Named_Account__c + triggerNew[i].Segment__c);
                    if(overwrite){
                        triggerNew[i].Country = triggerNew[i].LeanData_Mailing_Country__c;
                        triggerNew[i].State = triggerNew[i].LeanData_Mailing_State__c;
                        triggerNew[i].City = triggerNew[i].LeanData_Mailing_City__c;
                        triggerNew[i].Street = triggerNew[i].LeanData_Mailing_Street__c;
                        triggerNew[i].PostalCode = triggerNew[i].LeanData_Mailing_Postcode__c;
                    }
            }
        }
    }
    }
    
    public static map<String,String> createStatesMap(){
        map <String,String> mapS = new map <String,String>();
        List <String> stateFullname = new List<String>{'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming','Alberta','British Columbia','Manitoba','New Brunswick','Newfoundland and Labrador','Nova Scotia','Northwest Territories','Nunavut','Ontario','Prince Edward Island','Quebec','Saskatchewan','Yukon'};
        List <String> state2LetterName = new List<String>{'AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY','AB','BC','MB','NB','NL','NS','NT','NU','ON','PE','QC','SK','YT'};
            for(integer i = 0; i < 62; i++){
                mapS.put(state2LetterName[i],stateFullname[i]);
            }
        system.debug('chekmap'+mapS);
        return mapS;
    }
}