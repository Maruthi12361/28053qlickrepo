/********************************************************
* CLASS: WebActivityLeadGeneration_Test
* DESCRIPTION: Test class for WebActivityLeadGeneration
*
*
* CHANGELOG:    
*   2019-02-26 - BAD - Added Initial logic
*********************************************************/
@isTest
private class WebActivityLeadGeneration_Test
{

    @testSetup static void setup() {

        Semaphores.SetAllSemaphoresToTrue();

        List<QlikTech_Company__c> QlikCompanies = new List<QlikTech_Company__c>();
        SIC_Code__c code = new SIC_Code__c(Name ='aaa', Industry__c='bbbb');
        insert code;

        QlikTech_Company__c qc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
            qc.Country_Name__c = 'Sweden';
            qc.QlikTech_Region__c = 'NORDIC';
            qc.QlikTech_Sub_Region__c = 'NORDIC';
            qc.QlikTech_Operating_Region__c = 'N EUROPE';
            qc.Language__c = 'Swedish';
        QlikCompanies.add(qc);
        upsert QlikCompanies;

        List<ULC_Ignore_Domain__c> IgnoreDomains = new List<ULC_Ignore_Domain__c>();
        ULC_Ignore_Domain__c iq = new ULC_Ignore_Domain__c();
        iq.Name = 'yahoo.com';
        IgnoreDomains.add(iq);
        insert IgnoreDomains;

        List<Account> Accounts = new List<Account>();
        Account a = new Account();
        a.Name = 'The Test Customer';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Test Partner';
        a.Navision_Status__c = 'Partner';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        a.E_Mail_Domain__c = 'thetestpartner.com';
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Test Customer 2';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Test Customer Dup 1';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Test Customer Dup 2';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Base Company';
        a.Navision_Status__c = '';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        insert Accounts;

        List<Contact> Contacts = new List<Contact>();
        Contact c = new Contact();
        c.AccountId = Accounts[0].Id;
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Email = 'first.last@thetestcustomer.com';
        c.No_Auto_CPView__c = false;
        Contacts.add(c);
        c = new Contact();
        c.AccountId = Accounts[1].Id;
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Email = 'first.last@thetestpartner.com';
        c.ActiveULC__c = true;
        c.No_Auto_CPView__c = false;
        Contacts.add(c);
        c = new Contact();
        c.AccountId = Accounts[2].Id;
        c.FirstName = 'First 2';
        c.LastName = 'Last 2';
        c.Email = 'first.last@thetestpartner.com';
        c.ActiveULC__c = true;
        c.No_Auto_CPView__c = false;
        Contacts.add(c);
        c = new Contact();
        c.AccountId = Accounts[3].Id;
        c.FirstName = 'First Dup 1';
        c.LastName = 'Last Dup 1';
        c.Email = 'first.last.1@thetestpartner-dup.com';
        c.No_Auto_CPView__c = false;
        Contacts.add(c);
        c = new Contact();
        c.AccountId = Accounts[4].Id;
        c.FirstName = 'First Dup 2';
        c.LastName = 'Last Dup 2';
        c.Email = 'first.last.2@thetestpartner-dup.com';
        c.No_Auto_CPView__c = false;
        Contacts.add(c);
        c = new Contact();
        c.AccountId = Accounts[5].Id;
        c.FirstName = 'First Base';
        c.LastName = 'Last Base';
        c.Email = 'first.last@thetestbase.com';
        c.No_Auto_CPView__c = false;
        Contacts.add(c);
        insert Contacts;

        List<Lead> Leads = new List<Lead>();
        Lead l = new Lead();
        l.FirstName = 'First';
        l.LastName = 'Last';
        l.Email = 'first.last@lead.com';
        l.Country_Code__c = QlikCompanies[0].Id;
        l.Country_Name__c = QlikCompanies[0].Id;
        l.Country = 'Sweden';
        l.Company = 'The Lead';
        Leads.add(l);

        Lead l1 = new Lead();
        l1.FirstName = 'FirstTest';
        l1.LastName = 'LastTest';
        l1.Email = 'first.last2@lead.com';
        l1.Country_Code__c = QlikCompanies[0].Id;
        l1.Country_Name__c = QlikCompanies[0].Id;
        l1.Country = 'Sweden';
        l1.Company = 'The Lead';
        Leads.add(l1);
        insert Leads;

        List<ULC_Details__c> ULCDetails = new List<ULC_Details__c>();
        ULC_Details__c ulc = new ULC_Details__c();
        ulc.ULCName__c = 'thecontactuser';
        ulc.ULCStatus__c = 'Active';
        List<Contact> Contacts1 = [SELECT Id FROM Contact WHERE Email = 'first.last@thetestcustomer.com'];
        ulc.ContactId__c = Contacts1[0].Id;
        ULCDetails.add(ulc);
        insert ULCDetails;

        List<ULC_Details__c> ULCDetailsForLead = new List<ULC_Details__c>();
        ULC_Details__c ulcForLead = new ULC_Details__c();
        ulcForLead.ULCName__c = 'theleaduser';
        ulcForLead.ULCStatus__c = 'Active';
        //get Lead object
        List<Lead> Leads1 = [SELECT Id FROM Lead WHERE Email = 'first.last2@lead.com'];
        ulcForLead.LeadId__c = Leads1[0].Id;
        ULCDetailsForLead.add(ulcForLead);
        insert ULCDetailsForLead;

        List<Campaign> Campaigns = new List<Campaign>();
        Campaign cam = new Campaign();
        cam.Name = 'My Campaign';
        Campaigns.add(cam);
        insert Campaigns;

        List<Campaign_Incentive__c> CampaignIncentives = new List<Campaign_Incentive__c>();
        Campaign_Incentive__c ci = new Campaign_Incentive__c();
        ci.Campaign_Name__c = Campaigns[0].Id;
        ci.Name = 'incentive-value';
        CampaignIncentives.add(ci);
        insert CampaignIncentives;

        List<ULC_Level__c> ULCLevels = new List<ULC_Level__c>();
        ULC_Level__c ulclev = new ULC_Level__c();
        ulclev.Name = 'CPBASE';
        ulclev.Description__c = 'Customer Portal Base Access';
        ulclev.Status__c = 'Active';
        ULCLevels.add(ulclev);
        ULC_Level__c ulclev2 = new ULC_Level__c();
        ulclev2.Name = 'CPLOG';
        ulclev2.Description__c = 'Customer Portal Ticket Logging Access';
        ulclev2.Status__c = 'Active';
        ULCLevels.add(ulclev2);
        insert ULCLevels;

        QuoteTestHelper.createCustomSettings();
        QTCustomSettings__c setting = QTCustomSettings__c.getInstance('Default');
        setting.ULC_QT_SSO_Account__c = Accounts[5].Id;
        update setting;

        List<ULC_Assets__c> ULCAssets = new List<ULC_Assets__c>();
        ULC_Assets__c ulca = new ULC_Assets__c();
        ulca.Name = 'TestAsset';
        ulca.Activity_Name__c = 'This is the Activity';
        ulca.Lead_Source__c = 'TestSource';
        ulca.Lead_Source_Detail__c = 'TestSourceDetail';
        ULCAssets.add(ulca);
        insert ULCAssets;

        Group ziftGroup = new Group();
        ziftGroup.Name = 'TestZiftQueue';
        ziftGroup.Type = 'Queue';
        insert ziftGroup;

        QueueSobject mappingObject = new QueueSobject(QueueId = ziftGroup.Id, SobjectType = 'Lead');
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}        

        WebActivitySettings__c csWebActivity = new WebActivitySettings__c();
        csWebActivity.ZiftGroup__c = [select Id from Group where Name = 'TestZiftQueue' and Type = 'Queue'].Id;
        csWebActivity.RT_PartnerSyndLead__c = '012D0000000khDF';
        csWebActivity.RT_ServiceAccountLead__c = '012D0000000kiDQ';
        csWebActivity.RT_TaskRecordType__c = '01220000000DRGT';
        insert csWebActivity;




        Semaphores.SetAllSemaphoresToFalse();
    }

    private static WebActivityUtils.AdditionalParams additionalParamsSetUp_QlikID(Boolean popZiftData)
    {
        Campaign cam = [SELECT Id FROM Campaign WHERE Name = 'My Campaign'];
        Id sicId = [select id from SIC_Code__c].Id;
        Id cmId = [select id from Campaign].Id;
        WebActivityUtils.AdditionalParams ap = new WebActivityUtils.AdditionalParams();
        ap.SupplimentalData = 'CAMPAIGNID~'+cmId+'~KEYWORDS~aaaa~SUB_INDUSTRY~'+ sicId + '~LEADSOURCEDETAILMIRROR~lsrmirror~EMPLOYEE_RANGE~25-49~INDUSTRY~abc~DB_COMPANY_NAME~fffff~REGISTRY_STATE~~REGISTRY_COUNTRY~Sweden~JOB_FUNCTION~gggg~ref~QSDW~TCACCEPTED~true~WEB_ACTIVITY_SOURCE~VA~SOURCEURL~google.com~SourceULC~source-ulc~SourceEmpl~source-employee~SourcePartner~source-partner~SourceID1~source-id-1~SourceID2~source-id-2~Incentive~'+ cam.Id +'~FreeText~ThisIsMyFreeText~_Mkto_Trk~MarketoTracking~webleadinterest~Potential Customer~leadsourcedetail~leadsource-detail~partnersource~partner-source';
        ap = WebActivityUtils.AdditionalParamsSplit(ap);
        if(popZiftData) 
            ap.ZiftData = 'ZIFTPARTNERID~pziftpartnerid~ZIFTPARTNERNAME~pziftpartnername';
        return ap;
    }

    private static List<WebActivityUtils.WebActivityContent> CreateWebActivityContent()
    {
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        return lstWAContent;
    }

    private static WebActivityUtils.ULCUser OUserSetUp()
    {
        WebActivityUtils.ULCUser us = new WebActivityUtils.ULCUser();
        us.JobTitle = 'Manager';
        us.EmailOptOut = true;
        us.City = 'City';
        us.State = 'State';
        us.Country = 'Sweden';
        us.CountryCode = 'SWE';
        us.PostalCode = '12345';
        us.Address1 = 'First Address Line';
        us.LastName = 'Second';
        us.FirstName = 'User';
        us.Phone = '+46123456789';
        us.Department = 'Information Intelligence, II';
        us.ClaimedQTRel = '';
        return us;
    }

    public static ULC_Details__c CreateMockULCDetail(string status, string username) {
        ULC_Details__c ulcd = new ULC_Details__c();
        ulcd.ULCName__c = (username == null ? 'username' : username);
        ulcd.ULCStatus__c = (status == null ? 'Active' : status);
        insert ulcd;
        return ulcd;
    }    

    @isTest 
    static void test_SetUser_Leads() {
    	WebActivityLeadGeneration dummy = new WebActivityLeadGeneration();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.WebActivityContent waContent1 = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.WebActivityContent waContent2 = new WebActivityUtils.WebActivityContent();
        ULC_Details__c ulc1 = CreateMockULCDetail('Awaiting Activation', 'username1');        
        ULC_Details__c ulc2 = CreateMockULCDetail('Awaiting Activation', 'username2');        

        // Partner syndication Lead (Zift)
        WebActivityUtils.ULCUser us1 = OUserSetUp();
        us1.EmailAddress = 'second.user@thelead1-post.com'; 
        us1.UserName = 'username1';
        us1.Account = 'The Lead 88';
        waContent1.MessageID = 'msg1';
        waContent1.OUser = us1;
        waContent1.AdditParams = AdditionalParamsSetUp_QlikID(true);
        lstWAContent.add(waContent1);

        //Service account Lead 
        WebActivityUtils.ULCUser us2 = OUserSetUp();
        us2.EmailAddress = 'second.user@thelead2-post.com'; 
        us2.UserName = 'username2';
        us2.Account = 'The Lead 89';
        us2.Type = 'service_account';
        waContent2.MessageID = 'msg2';
        waContent2.OUser = us2;
        waContent2.AdditParams = AdditionalParamsSetUp_QlikID(false);
        lstWAContent.add(waContent2);

        Test.startTest();
        lstWAContent = WebActivityLeadGeneration.SetUser(lstWAContent);
	    Test.stopTest();   

        for (WebActivityUtils.WebActivityContent wa : lstWAContent)
        {
            system.assertNotEquals('Failed', wa.Status);
        }
    }

    @isTest 
    static void test_SetUser_Existing_Lead() {
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser us = new WebActivityUtils.ULCUser();
        us.EmailAddress = 'first.last@lead.com';
        us.JobTitle = 'Manager';
        us.EmailOptOut = true;
        us.City = 'City';
        us.State = 'State';
        us.Country = 'Sweden';
        us.CountryCode = 'SWE';
        us.PostalCode = '12345';
        us.Address1 = 'First Address Line';
        us.LastName = 'Second';
        us.FirstName = 'User';
        us.Phone = '+46123456789';
        us.Department = 'Information Intelligence, II';
        us.ClaimedQTRel = '';
        us.UserName = 'thetestLead88';
        us.Account = 'The Lead 88';
        waContent.OUser = us;
        waContent.AdditParams = new WebActivityUtils.AdditionalParams();
        waContent.AdditParams = AdditionalParamsSetUp_QlikID(false);
        waContent.AdditParams.Asset = 'TestAsset';
        lstWAContent.add(waContent);
        lstWAContent = WebActivityLeadGeneration.SetUser(lstWAContent);

        if (lstWAContent.size() > 0)
            system.assertNotEquals('Failed', lstWAContent[0].Status);
    }    

    @isTest 
    static void test_SetUser_Existing_Customer() {
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser us = new WebActivityUtils.ULCUser();
        us.EmailAddress = 'first.last@thetestcustomer.com';
        us.JobTitle = 'Manager';
        us.EmailOptOut = true;
        us.City = 'City';
        us.State = 'State';
        us.Country = 'Sweden';
        us.CountryCode = 'SWE';
        us.PostalCode = '12345';
        us.Address1 = 'First Address Line';
        us.LastName = 'Second';
        us.FirstName = 'User';
        us.Phone = '+46123456789';
        us.Department = 'Information Intelligence, II';
        us.ClaimedQTRel = '';
        us.UserName = 'ulcusername';
        us.Account = 'The Customer 88';
        waContent.OUser = us;
        waContent.AdditParams = AdditionalParamsSetUp_QlikID(false);
        //waContent.AdditParams.ZiftData += '~asd';
        lstWAContent.add(waContent);
        ULC_Details__c ulc = QTTestUtils.createMockULCDetail(null, 'Awaiting Activation');      
        lstWAContent = WebActivityLeadGeneration.SetUser(lstWAContent);

        if (lstWAContent.size() > 0)
            system.assertNotEquals('Failed', lstWAContent[0].Status);
    }

    @isTest 
    static void test_SetUser_Existing_Partner() {
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser us = new WebActivityUtils.ULCUser();
        waContent.Status = 'Pending';
        us.EmailAddress = 'first.lastp@thetestpartner.com';
        us.JobTitle = 'Manager';
        us.EmailOptOut = true;
        us.City = 'City';
        us.State = 'State';
        us.Country = 'Sweden';
        us.CountryCode = 'SWE';
        us.PostalCode = '12345';
        us.Address1 = 'First Address Line';
        us.LastName = 'Second';
        us.FirstName = 'User';
        us.Phone = '+46123456789';
        us.Department = 'Information Intelligence, II';
        us.ClaimedQTRel = '';
        us.UserName = 'thetestcustomer88';
        us.Account = 'The Customer 88';
        waContent.OUser = us;
        
        waContent.AdditParams = new WebActivityUtils.AdditionalParams();
        lstWAContent.add(waContent);
        lstWAContent = WebActivityLeadGeneration.SetUser(lstWAContent);

        if (lstWAContent.size() > 0)
            system.assertNotEquals('Failed', lstWAContent[0].Status);
    }

    @isTest 
    static void test_Fail_Lead() {
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser us = new WebActivityUtils.ULCUser();
        us.EmailAddress = 'first.last@lead3.com';
        us.JobTitle = 'Manager';
        us.EmailOptOut = true;
        us.City = 'City';
        us.State = 'State';
        us.Country = 'Sweden';
        us.CountryCode = 'SWE';
        us.PostalCode = '12345';
        us.Address1 = 'First Address Line';
        us.LastName = 'Second';
        us.FirstName = 'User';
        us.Phone = '+46123456789';
        us.Department = 'Information Intelligence, II';
        us.ClaimedQTRel = '';
        us.UserName = 'thetestLead90';
        us.Account = 'The Lead 90';
        us.OwnerID = ''; //Failing paramSuccess
        us.QliktechCompId = 'CorruptID';
        waContent.OUser = us;
        waContent.AdditParams = new WebActivityUtils.AdditionalParams();
        WebActivityUtils.retStruct ret = WebActivityLeadGeneration.InitCreateLead(waContent, waContent.AdditParams);
        system.assertEquals(false, ret.Success);
    }     

    @isTest 
    static void test_Fail_Contact() {
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser us = new WebActivityUtils.ULCUser();
        us.EmailAddress = 'first.last@contact3.com';
        us.JobTitle = 'Manager';
        us.EmailOptOut = true;
        us.City = 'City';
        us.State = 'State';
        us.Country = 'Sweden';
        us.CountryCode = 'SWE';
        us.PostalCode = '12345';
        us.Address1 = 'First Address Line';
        us.LastName = 'Second';
        us.FirstName = 'User';
        us.Phone = '+46123456789';
        us.Department = 'Information Intelligence, II';
        us.ClaimedQTRel = '';
        us.UserName = 'thetestContact90';
        us.AccountId = 'corruptAcc'; //Failing param
        waContent.OUser = us;
        waContent.AdditParams = new WebActivityUtils.AdditionalParams();
        WebActivityUtils.retStruct ret = WebActivityLeadGeneration.InitCreateContact(waContent);
        system.assertEquals(false, ret.Success);
    }   

    @isTest 
    static void test_ValidateEmail() {
        system.assertEquals(false, WebActivityLeadGeneration.ValidateEmail(null));
        system.assertEquals(false, WebActivityLeadGeneration.ValidateEmail('test-no-at.com'));
        system.assertEquals(false, WebActivityLeadGeneration.ValidateEmail('test@test.c'));
        system.assertEquals(true, WebActivityLeadGeneration.ValidateEmail('test@test.com'));
    }

    @isTest 
    static void test_ValidateUsername() {
        system.assertEquals(false, WebActivityLeadGeneration.ValidateUsername(null));
        system.assertEquals(false, WebActivityLeadGeneration.ValidateUsername('asd!!asdasdsad'));
        system.assertEquals(true, WebActivityLeadGeneration.ValidateUsername('SteveVai2019'));
    }

    @isTest 
    static void test_ValidateFullUserObject() {
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();

        waContent.AdditParams = new WebActivityUtils.AdditionalParams();
        waContent.AuditTrail = '';
        waContent.Status = 'Pending';
        waContent.OUser =  OUserSetUp();
        waContent.OUser.EmailAddress = 'test@qlik.com';
        waContent.OUser.UserName = '!!!!asdqlikcom12345678910hujiuhygtfrdftgyhujinjygtfcrvjiiugtcxuknhfessfolmnbcrsdtuked';
        waContent.OUser.LastName = '';
        waContent.OUser.CountryCode = '';
        waContent.OUser.Country = 'Bosnia';
        waContent.WebActivityType = 'LeadGeneration';
        lstWAContent.add(waContent);

        lstWAContent = WebActivityLeadGeneration.ValidateFullUserObject(lstWAContent);
        System.debug('BAD 11111 FULL: ' + lstWAContent[0]);
        system.assertEquals('Failed', lstWAContent[0].Status);
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Registration seems to be an Employee according to email'));
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Username contains illegal characters'));
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Employee already registered'));
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Username contains too many characters'));
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Company is missing'));
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Lastname is missing'));
        system.assertEquals(true, lstWAContent[0].OUser.Country == 'Bosnia & Herzegovena');

        lstWAContent[0].AuditTrail = '';
        lstWAContent[0].Status = 'Pending';
        lstWAContent[0].OUser.Username = 'TooFew';
        lstWAContent[0].OUser.Country = 'Laos';
        lstWAContent[0].OUser.EmailAddress = 'test-no-at.com';
        lstWAContent = WebActivityLeadGeneration.ValidateFullUserObject(lstWAContent);
        system.assertEquals('Failed', lstWAContent[0].Status);
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Email contains illegal characters'));
        system.assertEquals(true, lstWAContent[0].AuditTrail.containsIgnoreCase('Username contains too few characters'));
        system.assertEquals(true, lstWAContent[0].OUser.Country == 'Laos, People\'s Democratic Republic');

        lstWAContent[0].AuditTrail = '';
        lstWAContent[0].OUser.Country = 'Cote';
        lstWAContent = WebActivityLeadGeneration.ValidateFullUserObject(lstWAContent);
        system.assertEquals(true, lstWAContent[0].OUser.Country == 'Cote d\'Ivoire');        
    }
    
    @isTest
    static void New_Lead_With_Product_User_Creation(){
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        
        ulcuser.LeadID = [SELECT Id FROM Lead WHERE Email = 'first.last@lead.com'].id;
        List<ULC_Details__c> Culc = [SELECT Id,ContactId__c,qlikid__c FROM ULC_Details__c where ULCName__c = 'thecontactuser'];
        Culc[0].qlikid__c = 'a0825foigkjj894ukjf84hkfhkfsk00000FlSYwdsuihdsuchdsucdh78uivdigddj';
        update Culc;
        ulcuser.ulcqlikId = Culc[0].qlikid__c;
        Product_Trial__c PT = new Product_Trial__c();
        PT.Trial_Status__c = 'Started';
        PT.Trial_License_Key__c = 'ujnskncksdnc';
        PT.Product__c = 'Qlik Sense Team';
        PT.Contact__c = Culc[0].ContactId__c;
        PT.Start_Date__c = date.today();
        PT.End_Date__c = date.today();
        insert(PT);
        
        ulcuser.eventType = 'invited.user.detail';
        waContent.oUser = ulcuser;
        waContent.WebActivityType = 'invited.user.detail';
        waContent.Status = 'Completed';
        lstWAContent.add(waContent);
        
        WebActivityUtils.WebActivityContent waContent1 = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser ulcuser1 = new WebActivityUtils.ULCUser();
        ulcuser1.LeadID = [SELECT Id FROM Lead WHERE Email = 'first.last2@lead.com'].id;
        Product_User__c pU = new Product_User__c();
        pU.Lead__c = ulcuser1.LeadID;
        pU.Status__c = 'Active';
        pU.User_Role__c = 'Invited';
        pU.Status_Change_Date__c = date.today();
        insert(pu);
        ulcuser1.eventType = 'invited.user.detail';
        waContent1.oUser = ulcuser1;
        waContent1.WebActivityType = 'invited.user.detail';
        waContent1.Status = 'Completed';
        lstWAContent.add(waContent1);
        
        WebActivityUtils.WebActivityContent waContent2 = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser ulcuser2 = new WebActivityUtils.ULCUser();
        ulcuser2.ContactID = [SELECT Id FROM Contact WHERE Email = 'first.last@thetestbase.com'].id;
        Product_User__c pU2 = new Product_User__c();
        pU2.Contact__c = ulcuser2.ContactID;
        pU2.Status__c = 'Active';
        pU2.User_Role__c = 'Invited';
        pU2.Status_Change_Date__c = date.today();
        insert(pu2);
        ulcuser2.eventType = 'invited.user.detail';
        waContent2.oUser = ulcuser2;
        waContent2.WebActivityType = 'invited.user.detail';
        waContent2.Status = 'Completed';
        lstWAContent.add(waContent2);
        
        WebActivityUtils.WebActivityContent waContent3 = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser ulcuser3 = new WebActivityUtils.ULCUser();
        ulcuser3.ContactID = [SELECT Id FROM Contact WHERE Email = 'first.last@thetestcustomer.com'].id;
        List<ULC_Details__c> C = [SELECT Id,LeadId__c,qlikid__c FROM ULC_Details__c where ULCName__c = 'theleaduser'];
        C[0].qlikid__c = 'a082500000FlSYwdsuihdsuchdsucdh78uivdigddj';
        update C;
        ulcuser3.ulcqlikId = C[0].qlikid__c;
        waContent3.WebActivityType = 'invited.user.detail';
        ulcuser3.eventType = 'invited.user.detail';
        Product_Trial__c PT1 = new Product_Trial__c();
        PT1.Trial_Status__c = 'Started';
        PT1.Trial_License_Key__c = 'ujnskhuyncksdnc';
        PT1.Product__c = 'Qlik Sense Team';
        PT1.Lead__c = C[0].LeadId__c;
        PT1.Start_Date__c = date.today();
        PT1.End_Date__c = date.today();
        insert(PT1);
        waContent3.oUser = ulcuser3;
        waContent3.Status = 'Completed';
        lstWAContent.add(waContent3);
        
        List<String> lst = new List<String>();
        lst.add(Culc[0].qlikid__c);
        lst.add(C[0].qlikid__c);
        
        lstWAContent = WebActivityPartnerUserCreation.PartnerUserCreation(lstWAContent,lst);
        system.assertEquals(true, !lstWAContent[0].AuditTrail.contains('Error'));
        system.assertEquals(true, !lstWAContent[3].AuditTrail.contains('Error'));
    }
    /*
    @isTest
    static void Contact_Selection_Test(){
        List<Contact> CList = new List<Contact>();
        Contact c1 = [SELECT Id,Left_Company__c,QlikID__c,RecordTypeid,Email FROM Contact WHERE Email = 'first.last@thetestcustomer.com'];
        c1.Left_Company__c = false;
        c1.QlikID__c = 'fsf478refh784rgf44';
        c1.RecordTypeId = '012D0000000KE2E';
        update c1;
        CList.add(c1);
        Contact c2 = [SELECT Id,Left_Company__c,QlikID__c,RecordTypeid,Email FROM Contact WHERE Email = 'first.last@thetestbase.com'];
        c2.Left_Company__c = false;
        c2.QlikID__c = 'fsf478refh784rgf44';
        update c2;
        CList.add(c2);
        String QlikId = 'fsf478refh784rgf44';
        Test.startTest();
        map <String,Contact> result = WebActivityLeadGeneration.findCorrectEixistingRecord(CList,QlikId);
        Test.stopTest();
        
        system.assertEquals(true,result.containsKey(c2.Email));
    }*/

    @isTest
    static void test_InvitedUser_Product(){
        List<Contact> c = [SELECT Id, QlikID__c FROM Contact WHERE Email = 'first.last@thetestcustomer.com'];
        c[0].QlikID__c = '123456';
        update c;
        
        Zuora__Subscription__c zsub = new Zuora__Subscription__c();
        zsub.name = 'sub1';
        insert zsub;

        Product_User__c pU = new Product_User__c();
        pU.Contact__c = c[0].Id;
        pU.Status__c = 'Active';
        pU.User_Role__c = 'Service Account Owner';
        pU.Subscription__c = zsub.Id;
        pU.Zuora_Subscription_ID__c = zsub.Name;
        insert(pu);

        List<Zuora__Product__c> zp = new List<Zuora__Product__c>();
        Zuora__Product__c zp1 = new Zuora__Product__c();
        zp1.name = 'Qlik Sense Enterprise';
        zp.add(zp1);
        Zuora__Product__c zp2 = new Zuora__Product__c();
        zp2.name = 'Qlik Sense Business';
        zp.add(zp2);
        insert zp;

        List<Zuora__SubscriptionProductCharge__c> zspc = new List<Zuora__SubscriptionProductCharge__c>();
        Zuora__SubscriptionProductCharge__c zspc1 = new Zuora__SubscriptionProductCharge__c();
        zspc1.name = 'QSE';
        zspc1.Zuora__Product__c = zp[0].Id;
        zspc1.Zuora__Subscription__c = zsub.Id;
        zspc.add(zspc1);
        Zuora__SubscriptionProductCharge__c zspc2 = new Zuora__SubscriptionProductCharge__c();
        zspc2.name = 'QSB';
        zspc2.Zuora__Product__c = zp[1].Id;
        zspc2.Zuora__Subscription__c = zsub.Id;
        zspc.add(zspc2);
        insert zspc;

        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        waContent.AdditParams = new WebActivityUtils.AdditionalParams();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        ulcuser.eventType = 'invited.user.detail';
        ulcuser.ulcqlikId = '123456';
        waContent.oUser = ulcuser;
        waContent.WebActivityType = 'invited.user.detail';
        waContent.Status = 'Pending';
        lstWAContent.add(waContent);        

        List<String> invitedBy = new List<String>();
        invitedBy.add('123456');
      
        lstWAContent = WebActivityProduct.GetProductsForInvitedBy(lstWAContent, invitedBy);
        system.assertEquals('Qlik Sense Enterprise', lstWAContent[0].OUser.Product);
        system.assertEquals(zsub.Id, lstWAContent[0].OUser.Subscription);

        
    }

}