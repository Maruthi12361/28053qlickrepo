/**************************************************
* Author: Alberto de Diego from Fluido Oy
*
* Change Log:
* 2018-12-03 CTS SCU-5 Removal of variables used in Inactive Triggers.
* 2016-06-07 Andreas Nilsson: Class reactivated after winter 16, moved logic to helper class.
* 2015-04-09 Madhav Kakani: CR# 20197 - add topic to the related parent
* 2013-05-28 Madhav Kakani: Placed a check for empty map
* 2013-01-16 Madhav Kakani: Made changes to use Case object
* 2012-11-15 Alberto de Diego: CR# 4623
*        Initial Development
* 2019-05-13 extbad Retrieve ArticleFeedback Record Type for Case object
**************************************************/
public without sharing class FeedItemArticleComment {
	public static boolean RunTriggerWhenTest = false;
	public static void HandleTrigger(List<FeedItem> triggerNew) {

		if(test.isRunningTest() && !RunTriggerWhenTest)
        	return;

        // CR# 20197 - START
    	// Assign any new topics in the feed to the related cases.
    	// Currently this is not being done by Salesforce automatically

		Set<Id> caseIds = new Set<Id>(); // case ids
		Set<Id> parentIds = new Set<Id>();
		for(FeedItem feedItem : triggerNew)
    		parentIds.add(feedItem.ParentId);

    	/*for(FeedItem feedItem : triggerNew) {
    		parentIds.add(feedItem.ParentId);
	        ConnectApi.TopicPage feedPage = ConnectApi.Topics.getTopics(null, feedItem.Id);
	        if(feeditem.ParentId == null) continue;

	        // Assign only to cases
	        String obName = feeditem.ParentId.getSObjectType().getDescribe().getName();
	        if(obName != null && obName.endsWithIgnoreCase('Case')) {
	            for(ConnectApi.Topic t : feedPage.topics) {
	                ConnectApi.Topics.assignTopic(null, feeditem.ParentId, t.id);
	            }
	            caseIds.add(feeditem.ParentId);
	        }
	    }*/   
	    // CR# 20197 - END
	    
	    //Select all knowledge articles and article versions related to the feed items
	    Map<Id, KnowledgeArticle> articles = new Map<Id, KnowledgeArticle>([SELECT Id, ArticleNumber FROM KnowledgeArticle WHERE Id IN :parentIds]);
	    if( (articles == null) || (articles.size() <= 0) ) return;
	    
	    Set<Id> articleIds = new Set<Id>();
	    for (KnowledgeArticle ka : articles.values()) articleIds.add(ka.Id); 
	    List<KnowledgeArticleVersion> versions = [SELECT Id, Title, KnowledgeArticleId FROM KnowledgeArticleVersion 
	                                                WHERE PublishStatus = 'Online' AND KnowledgeArticleId IN :articleIds
	                                                ORDER BY CreatedDate];
	    
	    //put the newest version of the article in the map  
	    Map<Id, KnowledgeArticleVersion> versionsMap = new Map<Id, KnowledgeArticleVersion> ();
	    for (KnowledgeArticleVersion version : versions) {
	        if (versionsMap.get(version.KnowledgeArticleId) == null) versionsMap.put(version.KnowledgeArticleId, version);
	    }
	    
	    List<QueueSobject> unassigned = [SELECT Id, QueueId FROM QueueSobject WHERE Queue.Name = 'Unassigned'];
	        
	    List<FeedItem> knowledgeArticleFeedItems = new List<FeedItem>();
	    List<Case> newComments = new List<Case>();
	    //create article comments for all feed items that have a knowledge article as their parent
	    User userRec = [Select Id, ContactId, Contact.AccountId
            From User 
            where Id = :userinfo.getUserId() 
            LIMIT 1];

		String articleFeedbackId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Article Feedback')
				.getRecordTypeId();

	    for (FeedItem item : triggerNew) {
	        KnowledgeArticle article = articles.get(item.ParentId);
	        if (article == null) continue;
	        
	        KnowledgeArticleVersion version = versionsMap.get(article.Id);
	        Case comment = new Case(Description=item.Body.replaceAll('<[^>]+>',' '),
                RecordTypeId=articleFeedbackId,
                Title__c=version != null ? version.Title : '',
                Subject=version != null ? version.Title : '',
                Submitted_by__c=item.CreatedById,
                Knowledge_article__c=article.Id,
                Origin='Internal',
	            Feed_Item__c=item.Id,
	            AccountId = userRec.Contact.AccountId,
	            ContactId = userRec.ContactId);

	        if (unassigned.size() > 0) comment.OwnerId = unassigned[0].QueueId;
	        	newComments.add(comment);       
	    }
	    
        //CTS SCU-5 Removal of Inactive Triggers
        //Semaphores.CaseUpdateSupportContact_HasRunAfterUpdate= true;
	    
        system.debug('Before insert');
	    insert newComments; 
	   	system.debug('After insert');
	}
}