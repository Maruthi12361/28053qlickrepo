/**
* UpdateArticleImageLinks
*
* Added: 		28-12-2018 - ext_bad - CHG0035127
*
* Change log:
* 28-12-2018 - ext_bad - CHG0035127 - update image links that lead to EU1 in articles.
*
*/
global class UpdateArticleImageLinksBatch implements Database.Batchable<sObject>, Database.Stateful {
    global boolean TEST_EMAIL_SENDING = false;
    global Set<String> errorIds;
    private String articleNumbers;
    private String articleType;
    private Boolean updateDomainLinks;
    private String imgSrcString = 'src="';
    private static final String EU1_LINK = 'https://c.eu1.content.force.com';
    private static final String QLIK_EU1_LINK = 'https://qlik--c.eu1.content.force.com';
    private static final String SERVLET_LINK_PART = '/servlet/servlet.ImageServer';
    private static final String NEW_DOMAIN_LINK = 'https://qlik.my.salesforce.com';
    private static final String FILE_DOWNLOAD_LINK = '/servlet/servlet.FileDownload?file=';
    private static final String IMG_SERVER_LINK = '/servlet/servlet.ImageServer?id=';
    private static final String IMG_SERVER_OID = '&amp;oid=';

    global UpdateArticleImageLinksBatch() {

    }


    global UpdateArticleImageLinksBatch(String articles, String type) {
        articleNumbers = articles;
        articleType = type;
        updateDomainLinks = true;
    }

    global UpdateArticleImageLinksBatch(String articles, String type, Boolean updateFromDomain) {
        articleNumbers = articles;
        articleType = type;
        updateDomainLinks = updateFromDomain;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, ArticleNumber, KnowledgeArticleId, Cause__c, Description__c, Resolution__c FROM ' + articleType
                + ' WHERE PublishStatus = \'Online\' AND ArticleNumber IN (' + articleNumbers + ')';
        Database.QueryLocator ql = Database.getQueryLocator(query);
        return ql;
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        errorIds = publishArticles(scope, articleType);
    }

    global void finish(Database.BatchableContext BC) {
        generateErrorReport(errorIds);
    }

    private Set<String> publishArticles(List<SObject> articles, String articleType) {
        Set<String> errorArticleNumbers = new Set<String>();
        System.debug('#################### publish articles: ' + articles);
        if (!String.isEmpty(articleType)) {
            // unpublish existed acticle and create draft for it
            String draftIds = '';
            String draftId = '';
            for (SObject art : articles) {
                try {
                    draftId = KbManagement.PublishingService.editOnlineArticle((Id) art.get('KnowledgeArticleId'), true);
                    if (draftIds != '') {
                        draftIds += ',';
                    }
                    draftIds += '\'' + draftId + '\'';
                } catch (Exception ex) {
                    errorArticleNumbers.add((String) art.get('ArticleNumber'));
                    System.debug('#################### editOnlineArticle error: ' + ex.getMessage());
                }
            }
            // update draft and publish it
            if (!String.isEmpty(draftIds)) {
                String orgId = [SELECT Id FROM Organization LIMIT 1].Id;

                String queryStr = 'SELECT Id, KnowledgeArticleId, ArticleNumber, Cause__c, Description__c, Resolution__c FROM '
                        + articleType + ' WHERE Id in (' + draftIds + ')';
                List<SObject> articlesToPublish = Database.query(queryStr);

                if (!articlesToPublish.isEmpty()) {
                    if (Test.isRunningTest()) {
                        imgSrcString = 'src=&quot;'; // set '"' into rich text field in code convert it to '&quot;'
                    }

                    for (SObject item : articlesToPublish) {
                        String cause = (String) item.get('Cause__c');
                        if (cause != null) {
                            item.put('Cause__c', replaceImageLinks(cause, orgId));
                        }

                        String descr = (String) item.get('Description__c');
                        if (descr != null) {
                            item.put('Description__c', replaceImageLinks(descr, orgId));
                        }

                        String resolution = (String) item.get('Resolution__c');
                        if (resolution != null) {
                            item.put('Resolution__c', replaceImageLinks(resolution, orgId));
                        }
                    }
                    update articlesToPublish;

                    for (SObject item1 : articlesToPublish) {
                        try {
                            KbManagement.PublishingService.publishArticle((Id) item1.get('KnowledgeArticleId'), true);
                        } catch (Exception ex) {
                            errorArticleNumbers.add((String) item1.get('ArticleNumber'));
                            System.debug('#################### publishArticle error: ' + ex.getMessage());
                        }
                    }
                }
            }
        }
        return errorArticleNumbers;
    }

    public void generateErrorReport(Set<String> failedArticles) {
        // add debug info
        if (!failedArticles.isEmpty()) {
            System.debug('#### Something went wrong during update image links for articles. Failed numbers: ' + failedArticles);

            // inform users
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            String fromId = getFromAddresses();
            if (!String.isEmpty(fromId)) {
                mail.setOrgWideEmailAddressId(fromId);
            }
            mail.setToAddresses(new List<String>{
                    'karin.berglund@qlik.com', 'susan.verona@qlik.com'
            });
            mail.setBccAddresses(new List<String>{
                    'hanna.holubava@qlik.com'
            });
            mail.setSubject('Update image links for knowledge articles.');
            String htmlBody = 'There was error during update image links process. Failed articles:<br/><br/> ';
            for (String numb : failedArticles) {
                htmlBody += numb + '<br/>';
            }
            htmlBody += '<br/><br/>Best regards,<br/>Qlik Support<br/>';
            mail.setHtmlBody(htmlBody);
            mails.add(mail);

            if (!mails.isEmpty()) {
                if (Test.isRunningTest()) {
                    TEST_EMAIL_SENDING = true;
                }
                Messaging.sendEmail(mails);
            }
        }

    }

    private String replaceImageLinks(String field, String orgId) {
        if (updateDomainLinks) {

            if (field.contains(imgSrcString + EU1_LINK)) {
                field = field.replaceAll(imgSrcString + EU1_LINK, imgSrcString + NEW_DOMAIN_LINK);
            }
            if (field.contains(FILE_DOWNLOAD_LINK)) {
                Integer index = field.indexOf(FILE_DOWNLOAD_LINK);
                while (index != -1) {
                    Integer idIndex = index + FILE_DOWNLOAD_LINK.length();
                    String id = field.substring(idIndex, idIndex + 15);
                    field = field.replace(FILE_DOWNLOAD_LINK + id,
                            IMG_SERVER_LINK + id + IMG_SERVER_OID + orgId);

                    index = field.indexOf(FILE_DOWNLOAD_LINK);
                }
            }
            if (field.contains(imgSrcString + QLIK_EU1_LINK)) {
                field = field.replaceAll(imgSrcString + QLIK_EU1_LINK, imgSrcString + NEW_DOMAIN_LINK);
            }
        }
        if (!updateDomainLinks && field.contains(imgSrcString + SERVLET_LINK_PART)) {
            field = field.replaceAll(imgSrcString + SERVLET_LINK_PART, imgSrcString + NEW_DOMAIN_LINK + SERVLET_LINK_PART);
        }
        return field;
    }

    private String getFromAddresses() {
        String id = '';
        String qlikNoreply = '';
        if (QTCustomSettings__c.getInstance('Default') != null) {
            qlikNoreply = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
        }
        OrgwideEmailAddress[] orgwideaddress = [SELECT Id FROM OrgwideEmailAddress WHERE Displayname = :qlikNoreply LIMIT 1];
        if (!orgwideaddress.isEmpty()) {
            id = orgwideaddress[0].Id;
        }
        return id;
    }
}