/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_PersonaController {
    public string UserName{get{return (UserInfo.getName());}}
    public Boolean noEmail4me { get; set; }
    public String profileImageUrl { get; set; }
    private Contact myContact;
    private Id contactId;
    //all
    public string UserId{get{return (UserInfo.getUserId());}}
    public String[] SelectedPersona {get;set;}
    String[] Persona= new String[]{};


    public QS_PersonaController() {
        if (null == noEmail4me) {
            getMyContact();
            noEmail4me  = null == myContact ? false : (null == myContact.QS_Opt_out_case_emails__c ? false : myContact.QS_Opt_out_case_emails__c);
        }
        if(myContact != null)
        {
            SelectedPersona = myContact.Persona__c == null ? new string[]{} : myContact.Persona__c.split(';');
            for(Integer j = 0;j<SelectedPersona.size();j++)
                SelectedPersona[j] = SelectedPersona[j].Trim();
        }
        getPersonasList();
    }

    //Persona
    public String getUserPersona() {   
        system.debug('QS_Controller: UserID'+ Userid);  
        String Personat;    
        if (null != contactId) {
            getMyContact(); 
            if (null != myContact) {
                Persona.add(myContact.Persona__c);
                Personat= myContact.Persona__c;
            }
        }
        return Personat;
    }         
     
    public list<SelectOption> getPersonasList() {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult fieldResult = contact.Persona__c.getDescribe();

        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : plvalues ) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
     
    public PageReference SaveBut() {
        // upload User photo to Chatter
        system.debug('SaveBut start');
        UploadUserProfilePic();
      
        //update Persona on Contact object 
        getMyContact();
        string selectedPersonaString = '';
        for(string persona : SelectedPersona)
        {
            if(selectedPersonaString != '')
            {
                selectedPersonaString += ';';
            }
            selectedPersonaString += persona.Trim(); 
        }
        myContact.Persona__c = selectedPersonaString;
        myContact.QS_Opt_out_case_emails__c=noEmail4me;
        system.debug('before updateMyContact');
        system.debug('selectedPersonaString: ' + selectedPersonaString);
        system.debug('noEmail4me: ' + noEmail4me);
        update myContact;
        system.debug('SaveBut end');

        return null;
    }

    public String getUserprofilePic() {
        User  lstuser = [select FullPhotoUrl from User where Id =: UserInfo.getUserId()];
        String luser= lstuser.FullPhotoUrl;
        String[] parts = luser.split('/');
        Boolean isUploadedPhoto = parts[parts.size() - 2].length() == 15;
        
        if(isUploadedPhoto) {
            profileImageUrl=lstuser.FullPhotoUrl;
        }
        return  profileImageUrl;
    }
    
    private Attachment v_profilePicture;
    public Attachment ProfilePicture {
        get {
            if(v_profilePicture == null)
                v_profilePicture = new Attachment();
            return v_profilePicture;
        }
        set{
            v_profilePicture = value;
        }
    }
      
    private List<string> ImageHexCodes =  new List<string>{'FFD8FFE0', '474946383761' , '474946383961', '49492A00', '4D4D002A', '89504E470D0A1A0A', '424D'};
    public  PageReference UploadUserProfilePic() {

        Blob b;
        system.debug('ProfilePicture.Name: ' + ProfilePicture.Name);
        if (ProfilePicture.Name !=null)
        {
                  
            ProfilePicture.OwnerId = UserId;
            ProfilePicture.ParentId = contactId; // put it in running user's folder

       
            try{
                string description = 'Profile picture uploaded ' + datetime.now();
                ProfilePicture.description = description;
                if(!test.isrunningtest()){
                    insert ProfilePicture;
                    b = ProfilePicture.body;    
                }
                else 
                    b = Blob.valueof('Some random String');
                
            } 
            catch (DMLException e) {
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
                system.debug (e.getMessage());
                return null;
            } 
            finally {
                //document.body = null; // clears the viewstate
                ProfilePicture = new Attachment();
            }

            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully : '+b));
            String communityId = null;
            String userId= UserId;
           
            //ConnectApi.Photo photo = ConnectApi.ChatterUsers.setPhoto(communityId, userId,  new ConnectApi.BinaryInput(b,'image/jpg','userImage.jpg'));
            system.debug('Before setPhoto');
            try
            {
                system.debug('Before BinaryInput');
                string hex = BlobToHex(b);
                
                boolean isImage = false;
                if(hex.Length() > 20) {
                    hex = hex.substring(0,20);
                    system.debug('Hex: ' + hex);
                    for(string s : ImageHexCodes) {
                        if(hex.startswith(s))
                        {
                            isImage = true;
                            break;
                        }
                    }
                }
                if(isImage) {
                    ConnectApi.BinaryInput bi = new ConnectApi.BinaryInput(b,'image/jpg','userImage2.jpg');
                    system.debug('After BinaryInput');
                    ConnectApi.Photo photo = ConnectApi.ChatterUsers.setPhoto(communityId, userId, bi);
                }   
                else
                    system.debug('File is not an image, aborting');
            }
            catch (Exception ex) {
                system.debug('Exception in setPhoto');
            }
            system.debug('After setPhoto');
        }
        return null;
    }
    public static string BlobToHex(Blob input){
        Map<string, Integer> base64 = new map<string, Integer>{'A'=>0,'B'=>1,'C'=>2,'D'=>3,'E'=>4,'F'=>5,'G'=>6,'H'=>7,'I'=>8,'J'=>9,'K'=>10,'L'=>11,'M'=>12,'N'=>13,'O'=>14,'P'=>15,'Q'=>16,'R'=>17,'S'=>18,'T'=>19,'U'=>20,'V'=>21,'W'=>22,'X'=>23,'Y'=>24,'Z'=>25,'a'=>26,'b'=>27,'c'=>28,'d'=>29,'e'=>30,'f'=>31,'g'=>32,'h'=>33,'i'=>34,'j'=>35,'k'=>36,'l'=>37,'m'=>38,'n'=>39,'o'=>40,'p'=>41,'q'=>42,'r'=>43,'s'=>44,'t'=>45,'u'=>46,'v'=>47,'w'=>48,'x'=>49,'y'=>50,'z'=>51,'0'=>52,'1'=>53,'2'=>54,'3'=>55,'4'=>56,'5'=>57,'6'=>58,'7'=>59,'8'=>60,'9'=>61,'+'=>62,'/'=>63};
        Map<integer, string> hex = new Map<integer, string>{0=>'00',1=>'01',2=>'02',3=>'03',4=>'04',5=>'05',6=>'06',7=>'07',8=>'08',9=>'09',10=>'0A',11=>'0B',12=>'0C',13=>'0D',14=>'0E',15=>'0F',16=>'10',17=>'11',18=>'12',19=>'13',20=>'14',21=>'15',22=>'16',23=>'17',24=>'18',25=>'19',26=>'1A',27=>'1B',28=>'1C',29=>'1D',30=>'1E',31=>'1F',32=>'20',33=>'21',34=>'22',35=>'23',36=>'24',37=>'25',38=>'26',39=>'27',40=>'28',41=>'29',42=>'2A',43=>'2B',44=>'2C',45=>'2D',46=>'2E',47=>'2F',48=>'30',49=>'31',50=>'32',51=>'33',52=>'34',53=>'35',54=>'36',55=>'37',56=>'38',57=>'39',58=>'3A',59=>'3B',60=>'3C',61=>'3D',62=>'3E',63=>'3F',64=>'40',65=>'41',66=>'42',67=>'43',68=>'44',69=>'45',70=>'46',71=>'47',72=>'48',73=>'49',74=>'4A',75=>'4B',76=>'4C',77=>'4D',78=>'4E',79=>'4F',80=>'50',81=>'51',82=>'52',83=>'53',84=>'54',85=>'55',86=>'56',87=>'57',88=>'58',89=>'59',90=>'5A',91=>'5B',92=>'5C',93=>'5D',94=>'5E',95=>'5F',96=>'60',97=>'61',98=>'62',99=>'63',100=>'64',101=>'65',102=>'66',103=>'67',104=>'68',105=>'69',106=>'6A',107=>'6B',108=>'6C',109=>'6D',110=>'6E',111=>'6F',112=>'70',113=>'71',114=>'72',115=>'73',116=>'74',117=>'75',118=>'76',119=>'77',120=>'78',121=>'79',122=>'7A',123=>'7B',124=>'7C',125=>'7D',126=>'7E',127=>'7F',128=>'80',129=>'81',130=>'82',131=>'83',132=>'84',133=>'85',134=>'86',135=>'87',136=>'88',137=>'89',138=>'8A',139=>'8B',140=>'8C',141=>'8D',142=>'8E',143=>'8F',144=>'90',145=>'91',146=>'92',147=>'93',148=>'94',149=>'95',150=>'96',151=>'97',152=>'98',153=>'99',154=>'9A',155=>'9B',156=>'9C',157=>'9D',158=>'9E',159=>'9F',160=>'A0',161=>'A1',162=>'A2',163=>'A3',164=>'A4',165=>'A5',166=>'A6',167=>'A7',168=>'A8',169=>'A9',170=>'AA',171=>'AB',172=>'AC',173=>'AD',174=>'AE',175=>'AF',176=>'B0',177=>'B1',178=>'B2',179=>'B3',180=>'B4',181=>'B5',182=>'B6',183=>'B7',184=>'B8',185=>'B9',186=>'BA',187=>'BB',188=>'BC',189=>'BD',190=>'BE',191=>'BF',192=>'C0',193=>'C1',194=>'C2',195=>'C3',196=>'C4',197=>'C5',198=>'C6',199=>'C7',200=>'C8',201=>'C9',202=>'CA',203=>'CB',204=>'CC',205=>'CD',206=>'CE',207=>'CF',208=>'D0',209=>'D1',210=>'D2',211=>'D3',212=>'D4',213=>'D5',214=>'D6',215=>'D7',216=>'D8',217=>'D9',218=>'DA',219=>'DB',220=>'DC',221=>'DD',222=>'DE',223=>'DF',224=>'E0',225=>'E1',226=>'E2',227=>'E3',228=>'E4',229=>'E5',230=>'E6',231=>'E7',232=>'E8',233=>'E9',234=>'EA',235=>'EB',236=>'EC',237=>'ED',238=>'EE',239=>'EF',240=>'F0',241=>'F1',242=>'F2',243=>'F3',244=>'F4',245=>'F5',246=>'F6',247=>'F7',248=>'F8',249=>'F9',250=>'FA',251=>'FB',252=>'FC',253=>'FD',254=>'FE',255=>'FF'};
     
        string sIn = EncodingUtil.base64Encode(input);
        
        Integer srtIdx = 0;
        string strOut = '';
        for(Integer idx=0; idx < sIn.length()-4; idx+=4){
            strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)) 
                + hex.get(((base64.get(sIn.substring(idx+1, idx+2)) & 15)<<4) | (base64.get(sIn.substring(idx+2, idx+3)) >>> 2)) 
                + hex.get(((base64.get(sIn.substring(idx+2, idx+3)) & 3)<<6) | base64.get(sIn.substring(idx+3, idx+4)));
        }
     
        if(sIn.substring(sIn.length()-1, sIn.length()) != '='){
          Integer idx = sIn.length() - 4;
          strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)) 
                + hex.get(((base64.get(sIn.substring(idx+1, idx+2)) & 15)<<4) | (base64.get(sIn.substring(idx+2, idx+3)) >>> 2)) 
                + hex.get(((base64.get(sIn.substring(idx+2, idx+3)) & 3)<<6) | base64.get(sIn.substring(idx+3, idx+4)));
          
        } 
        else if(sIn.substring(sIn.length()-2, sIn.length()-1) != '='){
            Integer idx = sIn.length() - 4;
            strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)) 
                + hex.get(((base64.get(sIn.substring(idx+1, idx+2)) & 15)<<4) | (base64.get(sIn.substring(idx+2, idx+3)) >>> 2)); 
        } 
        else {
            Integer idx = sIn.length() - 4;
            strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)); 
        }
        return strOut;
    }
    private void getMyContact() {
        getContactId();
        if (null == myContact && null != contactId)
            myContact = [Select Id,Persona__c, QS_Opt_out_case_emails__c from Contact where ID= :contactId limit 1];
    }
    private void getContactId() {
        //getUserId();
        if (null == contactId)
            contactId = [select contactId from User where Id = :Userid limit 1].ContactId;
    }
    public PageReference setMyEmailOption() {
        return null;
    }
}