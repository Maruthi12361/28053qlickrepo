/************************************************************************
 * @author  KMH
 * 2015-06-16 Suppor Portal Project
 *  
 * On Partner Portal on Account detail page Product Licenses related list is replaced with
   Custom VF page(related list) to allow user to Navigate to new Support Portal
   from Product Licenses links.
 *
 * 2019-11-18 - BAD - BMW-1843 - added filter for support provided by
 *************************************************************************/
public class PPLicenseFromAccount{
    public Account acc;
    public List <Entitlement> Lics{get; set;}
    public PPLicenseFromAccount (ApexPages.StandardController controller) {
        this.acc = (Account)controller.getRecord();
    }

    public List <Entitlement> getLics1() {
        
        List<User> u = [SELECT Name, AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        id partAccId = null;
        if(u != null && u[0].AccountId != null) {partAccId = u[0].AccountId;}

        Lics= new List <Entitlement> ();
        lics= [SELECT ID,
                             AccountId,                             
                             Name,
                             Description__c,
                             StartDate,
                             EndDate,
                             Status
                             
                             FROM Entitlement
                             WHERE AccountId = :acc.ID  
                             and Entitlement_Check__c = TRUE 
                                    and RecordType.Name != 'Obsolete'
                                        and Account_License__r.Support_Provided_By__c = :partAccId
                             ];
            System.debug('Contactlist' + lics);
            return lics;
 
    }
}