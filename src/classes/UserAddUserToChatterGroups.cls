/*******************************************************************
* Class UserAddUserToChatterGroups
*
* Helper class for Trigger UserAddUserToChatterGroups
* 
* Change Log:
* 2014-04-25   AIN (Andreas Nilsson)	 	Initial Development
*											CR# 11324
* 2017-04-26   AIN (Andreas Nilsson)        Added logic to avoid SOQL queries in handler, adapted test class
**********************************************************************/

public without sharing class UserAddUserToChatterGroups {
	public static List<string> ChatterGroups = new List<string> {'All QlikTech', 'Customer Success Chatter Group'};
	public static List<string> EmailExceptions = new List<string> {'helpdesk@qlikview.com', 'systems@qlikview.com'}; 

	public static void AddUserToChatterGroups(Set<Id> idsFromTrigger, List<string> chatterGroupsText) {
		Map<Id, Set<Id>> idsInChatterGroupAndTrigger = new Map<Id, Set<Id>>();
		Set<Id> idsNotInChatterGroup = new Set<Id>();
		List<CollaborationGroupMember> groupMembersInChatterGroupAndTrigger = new List<CollaborationGroupMember>();
		Map<Id, Set<Id>> idsToInsert = new Map<Id, Set<Id>>();
		List<CollaborationGroupMember> groupMembersToInsert = new List<CollaborationGroupMember>();
		CollaborationGroupMember cgm;
		List<CollaborationGroup> cgs;

		system.debug('Searching for chatter groups ' + chatterGroupsText);
		cgs = [select id, name from collaborationgroup where name in :chatterGroupsText];
		
		if(cgs.size() <= 0)
		{
			system.debug('No chatter groups found with the names ' + chatterGroupsText + ', exiting');
			return;
		}
		List<Id> chatterGroupIds = new List<Id>();
		for(CollaborationGroup cg : cgs)
		{
			chatterGroupIds.Add(cg.Id);
		}

		system.debug('Ids from trigger: ' + idsFromTrigger);
		groupMembersInChatterGroupAndTrigger = [select MemberId, CollaborationGroupId from CollaborationGroupMember where CollaborationGroupId in :chatterGroupIds and MemberId in :idsFromTrigger];

		system.debug('There were ' + groupMembersInChatterGroupAndTrigger.size() + ' users present in both trig and the groups');

		for(CollaborationGroupMember cgmObj : groupMembersInChatterGroupAndTrigger)
		{
			if(!idsInChatterGroupAndTrigger.containsKey(cgmObj.CollaborationGroupId))
				idsInChatterGroupAndTrigger.put(cgmObj.CollaborationGroupId, new Set<Id>{cgmObj.memberid});
			else
				idsInChatterGroupAndTrigger.get(cgmObj.CollaborationGroupId).add(cgmObj.memberid);
		}

		for(Id groupId : chatterGroupIds)
		{
			if(!idsInChatterGroupAndTrigger.containsKey(groupId))
			{
				//No group members found in group, add all members to group
				idsToInsert.put(groupId,  new Set<Id>(idsFromTrigger));
			}
			else
			{
				for(Id userId : idsFromTrigger)
				{
					if(!idsInChatterGroupAndTrigger.get(groupId).contains(userId))
					{
						if(!idsToInsert.containsKey(groupId))
							idsToInsert.put(groupId, new Set<Id>{userId});
						else
							idsToInsert.get(groupId).add(userId);
					}
					system.debug('Id ' + userId + ' is not present in chatter group ' + groupId + ', will be added');				
				}
			}
		}

		if(idsToInsert.Size() == 0)
		{
			system.debug('Users were already present in ' + chatterGroupsText + ', no insert needed');
			return; 
		}

		for(Id groupId : idsToInsert.keySet())
		{
			for(Id userId : idsToInsert.get(groupId))
			{
				cgm = new CollaborationGroupMember();
				cgm.CollaborationGroupId = groupId;
				cgm.MemberId = userId;
				cgm.CollaborationRole = 'Standard';
				cgm.NotificationFrequency = 'D';
				groupMembersToInsert.add(cgm);
				system.debug('CollaborationGroupMember object for ' + userId + ' to group ' + groupid +'created');
			}

			
		}	
		
		
		try
		{
			if(groupMembersToInsert.size() > 0)
			{
				insert groupMembersToInsert;
				system.debug('Inserting ' + groupMembersToInsert.size() + ' CollaborationGroupMember objects');
			}
		}
		catch (DmlException ex)
		{
			system.debug('Insert of group CollaborationGroupMember objects failed');
		}
	}
}