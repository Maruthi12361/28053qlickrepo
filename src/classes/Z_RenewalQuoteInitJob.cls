/**     
 * File Name : Z_RenewalQuoteInitJob
 * Description : This is a schedulable job to create renewal quotes, that triggers the Z_RenewalQuoteBatchable job.
 * @author : Inki Hong
 * Modification Log ====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

global class Z_RenewalQuoteInitJob implements Schedulable {

    global void execute(SchedulableContext ctx) {
        run();
    }

    global void run() {
        Renewal_Automation_Settings__c settings = Renewal_Automation_Settings__c.getOrgDefaults();
        Integer batchSize = (null == settings.QuoteCreationBatchSize__c) ? null : (Integer) settings.QuoteCreationBatchSize__c;
        Map<String, Map<String, Object>> subscriptionFieldsMap = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();
        Map<String, String> renewalOpportunityNamesMap = new Map<String, String>();
        List<String> previousOpptyIDs = new List<String>();
        Id renewalOpptyRecordTypeId = Schema.SObjectType.Opportunity
                                      .getRecordTypeInfosByName()
                                      .get('Subscription Renewal')
                                      .getRecordTypeId();
        
        for (Opportunity renewalOpportunity : [SELECT Id, Previous_Opportunity__c, Name, (SELECT Id FROM zqu__Quotes__r) 
                                               FROM Opportunity 
                                               WHERE Is_Manual_Process__c = FALSE
                                               AND Next_Renewal_Opportunity__c = NULL
                                               AND Previous_Opportunity__c != NULL
                                               AND CloseDate <= :Date.today().addDays(90)
                                               AND RecordTypeId = :renewalOpptyRecordTypeId
                                               AND (NOT StageName LIKE '%Closed%')
                                               ORDER BY CloseDate ASC]) {
            
            if (renewalOpportunity.zqu__Quotes__r.isEmpty()) {
                previousOpptyIDs.add(renewalOpportunity.Previous_Opportunity__c);
                renewalOpportunityNamesMap.put(renewalOpportunity.Previous_Opportunity__c, renewalOpportunity.Name);
            }
        }

        Z_RenewalQuoteBatchable batchable = new Z_RenewalQuoteBatchable(subscriptionFieldsMap, renewalOpportunityNamesMap, previousOpptyIDs);

        if (null != batchSize) {
            Database.executeBatch(batchable, batchSize);
        } else {
            Database.executeBatch(batchable);
        }
    }
}