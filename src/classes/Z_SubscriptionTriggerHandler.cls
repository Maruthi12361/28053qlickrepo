/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Linus Löfberg
* @version        1.0
* @created        2019-06-04
* @modified       2019-06-04
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
*
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class Z_SubscriptionTriggerHandler {

    private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;

    public Z_SubscriptionTriggerHandler(boolean isExecuting, integer size) {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void OnBeforeInsert(Zuora__Subscription__c[] newRecords) {

    }

    public void OnAfterInsert(Zuora__Subscription__c[] newRecords) {
        Map<String, Zuora__Subscription__c> subscriptionMap = new Map<String, Zuora__Subscription__c>();
        for (Zuora__Subscription__c sub : newRecords) {
            if (sub.Name != null) {
                subscriptionMap.put(sub.Name, sub);
            }
        }

        if (!subscriptionMap.isEmpty()) {
            List<Product_User__c> productUserList = [SELECT Id, Zuora_Subscription_ID__c, Subscription__c, Status__c FROM Product_User__c WHERE Status__c = 'Active' AND Zuora_Subscription_ID__c IN : subscriptionMap.keySet()];
            for (Product_User__c pu : productUserList) {
                pu.Subscription__c = subscriptionMap.get(pu.Zuora_Subscription_ID__c).Id;
            }
            update productUserList;
        }
    }

    // public void OnBeforeUpdate(Zuora__Subscription__c[] oldRecords, Zuora__Subscription__c[] updatedRecords, Map<ID, Zuora__Subscription__c> recordMap) {
    //
    // }
    //
    // public void OnAfterUpdate(Zuora__Subscription__c[] oldRecords, Zuora__Subscription__c[] updatedRecords, Map<ID, Zuora__Subscription__c> recordMap) {
    //
    // }
}