/***************************************************
Trigger: ContactStatusAge_Trigger
Object: Contact
Description: CR# 19391 – Add in Counters to Calculate Days and Date Stamp Entry Point into Waterfall Stage
Change Log:
 20141213   Madhav Kakani - Fluido Denmark
 20160421   CCE CR# 82611 - Replacing Lead Status value "Follow-Up Accepted" with new values "Follow-Up Attempt 1,2,3"
 2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
******************************************************/

public with sharing class ContactStatusAgeHelper {
	
	public static void conStatusAgeUpdate(List<Contact> triggernew, Map<Id,Contact> triggeroldmap){
		for(Contact c : triggernew) {
        if(c.Inquiry_Start_Date_Current__c == null) c.Inquiry_Start_Date_Current__c = c.Inquiry_Start_Date__c; // first time only

        // Initialize any variables with null values
        if(c.Inquiry_to_AQL_Age__c == null) c.Inquiry_to_AQL_Age__c = 0;
        if(c.AQL_to_MQL_Age__c == null) c.AQL_to_MQL_Age__c = 0;
        if(c.MQL_to_SAL_Age__c == null) c.MQL_to_SAL_Age__c = 0;
        if(c.SAL_to_SQL_Age__c == null) c.SAL_to_SQL_Age__c = 0;
        if(c.SQL_to_Closed_Won_Age__c == null) c.SQL_to_Closed_Won_Age__c = 0;

        Contact cOld = triggeroldmap.get(c.Id);
        if(c.Contact_Status__c == cOld.Contact_Status__c) continue; // no change in status

        // do not start time stamping yet if the following condition is true as Goal Discovery is the starting point
        if(c.Contact_Status__c == 'Goal Discovery' && cOld.Contact_Status__c == 'Follow-Up Required') continue;

        // HANDLE GOING BACKWARDS - START
        // Reset all time stamps and counters if status changed from Closed Won/Lost/Goal Rejected to something else
        if( (c.Contact_Status__c != 'Closed Won' && c.Contact_Status__c != 'Closed Lost' && c.Contact_Status__c != 'Goal Rejected')
            && (cOld.Contact_Status__c == 'Closed Won' || cOld.Contact_Status__c == 'Closed Lost' || cOld.Contact_Status__c == 'Goal Rejected') ) {

            c.AQL_to_MQL__c = c.MQL_to_SAL__c = c.SAL_to_SQL__c = c.SQL_to_Closed_Won__c = false;
            
            c.AQL_Start_Date__c = c.MQL_Start_Date__c = c.SAL_Start_Date__c = c.SQL_Start_Date__c = c.Closed_Won_Date__c = null;
            c.AQL_Start_Date_Current__c = c.MQL_Start_Date_Current__c = c.SAL_Start_Date_Current__c = c.SQL_Start_Date_Current__c = c.Closed_Won_Date__c = null;
            c.AQL_to_MQL_Age__c = c.MQL_to_SAL_Age__c = c.SAL_to_SQL_Age__c = c.SQL_to_Closed_Won_Age__c = 0;
        }

        // Reset stage flags only so the old counter values are preserved if going back from stages other than Closed Won / Closed Lost
        // Going back to SAL / MQL (they have the same contact status)
        if(c.Contact_Status__c == 'Goal Identified'
                && (cOld.Contact_Status__c == 'Goal Confirmed' || cOld.Contact_Status__c == 'Goal Rejected'
                || cOld.Contact_Status__c == 'Champion' || cOld.Contact_Status__c == 'Prove Value'
                || cOld.Contact_Status__c == 'Negotiating')) {
            c.MQL_to_SAL__c = c.SAL_to_SQL__c = false;
            c.MQL_Start_Date_Current__c = Date.today();
            c.SAL_Start_Date_Current__c = null;
            c.SQL_Start_Date_Current__c = c.Closed_Won_Date__c = null;
        }
        // Going back to AQL
        else if(c.Contact_Status__c == 'Follow-Up Required'
                && (cOld.Contact_Status__c == 'Archived' || cOld.Contact_Status__c == 'Goal Identified'
                || cOld.Contact_Status__c == 'Goal Confirmed' || cOld.Contact_Status__c == 'Goal Rejected'
                || cOld.Contact_Status__c == 'Champion' || cOld.Contact_Status__c == 'Prove Value'
                || cOld.Contact_Status__c == 'Negotiating')) {
            c.AQL_to_MQL__c = c.MQL_to_SAL__c = c.SAL_to_SQL__c = false;
            c.AQL_Start_Date_Current__c = Date.today();
            c.MQL_Start_Date_Current__c = c.SAL_Start_Date_Current__c = c.SQL_Start_Date_Current__c = c.Closed_Won_Date__c = null;
        }
        // Going back to Inquiry
        else if( (c.Contact_Status__c == 'Unknown' || c.Contact_Status__c == 'Suspect' || c.Contact_Status__c == 'Raw - Suspect')
                && (cOld.Contact_Status__c == 'Follow-Up Required' || cOld.Contact_Status__c == 'Follow-Up Attempt 1'
                || cOld.Contact_Status__c == 'Follow-Up Attempt 2' || cOld.Contact_Status__c == 'Follow-Up Attempt 3'
                || cOld.Contact_Status__c == 'Follow-Up Rejected' || cOld.Contact_Status__c == 'Follow-Up Disqualified'
                || cOld.Contact_Status__c == 'Contacted-Additional Work Required' || cOld.Contact_Status__c == 'Archived'
                || cOld.Contact_Status__c == 'Goal Discovery' || cOld.Contact_Status__c == 'Goal Identified'
                || cOld.Contact_Status__c == 'Goal Confirmed' || cOld.Contact_Status__c == 'Goal Rejected'
                || cOld.Contact_Status__c == 'Champion' || cOld.Contact_Status__c == 'Prove Value'
                || cOld.Contact_Status__c == 'Negotiating')) {
            c.AQL_to_MQL__c = c.MQL_to_SAL__c = c.SAL_to_SQL__c = c.SQL_to_Closed_Won__c = false;
            c.AQL_Start_Date_Current__c = c.MQL_Start_Date_Current__c = c.SAL_Start_Date_Current__c = c.SQL_Start_Date_Current__c = c.Closed_Won_Date__c = null;
        }
        // HANDLE GOING BACKWARDS - END
        
        // HANDLE GOING FORWARDS - START
        // Deal with closed won status first to account for any skipped statuses
        if(c.Contact_Status__c == 'Closed Won' && cOld.Contact_Status__c != 'Closed Won') {
            c.Closed_Won_Date__c = Date.today();

            // Handle the skipped stages
            if(c.Inquiry_Start_Date_Current__c == null) c.Inquiry_Start_Date_Current__c = Date.today();

            if(c.AQL_Start_Date__c == null) c.AQL_Start_Date__c = Date.today(); // historical value
            if(c.AQL_Start_Date_Current__c == null) c.AQL_Start_Date_Current__c = Date.today();

            if(c.MQL_Start_Date__c == null) c.MQL_Start_Date__c = Date.today(); // historical value
            if(c.MQL_Start_Date_Current__c == null) c.MQL_Start_Date_Current__c = Date.today();

            if(c.SAL_Start_Date__c == null) c.SAL_Start_Date__c = Date.today(); // historical value
            if(c.SAL_Start_Date_Current__c == null) c.SAL_Start_Date_Current__c = Date.today();

            if(c.SQL_Start_Date__c == null) c.SQL_Start_Date__c = Date.today(); // historical value
            if(c.SQL_Start_Date_Current__c == null) c.SQL_Start_Date_Current__c = Date.today();
        }
        
        // SQL Start
        if( (c.Contact_Status__c == 'Goal Confirmed' && cOld.Contact_Status__c != 'Goal Confirmed')
            || (c.Contact_Status__c == 'Champion' && cOld.Contact_Status__c != 'Champion')
            || (c.Contact_Status__c == 'Prove Value' && cOld.Contact_Status__c != 'Prove Value')            
            || (c.Contact_Status__c == 'Negotiating' && cOld.Contact_Status__c != 'Negotiating') ) {
            if(c.SQL_Start_Date__c == null) c.SQL_Start_Date__c = Date.today(); // historical value
            c.SQL_Start_Date_Current__c = Date.today(); // current value
        }

        // SQL End
        if( (c.Contact_Status__c == 'Closed Won' && cOld.Contact_Status__c != 'Closed Won')
            || (c.Contact_Status__c == 'Closed Lost' && cOld.Contact_Status__c != 'Closed Lost') ) {
            if(c.SQL_to_Closed_Won__c == false) {
                c.SQL_to_Closed_Won__c = true;
                if(c.SQL_Start_Date__c == null) c.SQL_Start_Date__c = Date.today(); // historical value
                if(c.SQL_Start_Date_Current__c == null) c.SQL_Start_Date_Current__c = Date.today();
                c.SQL_to_Closed_Won_Age__c += c.SQL_Start_Date_Current__c.daysBetween(Date.today());
            }
            
            // Handle the skipped stages
            if(c.Inquiry_Start_Date_Current__c == null) c.Inquiry_Start_Date_Current__c = Date.today();

            if(c.AQL_Start_Date__c == null) c.AQL_Start_Date__c = Date.today(); // historical value
            if(c.AQL_Start_Date_Current__c == null) c.AQL_Start_Date_Current__c = Date.today();

            if(c.MQL_Start_Date__c == null) c.MQL_Start_Date__c = Date.today(); // historical value
            if(c.MQL_Start_Date_Current__c == null) c.MQL_Start_Date_Current__c = Date.today();

            if(c.SAL_Start_Date__c == null) c.SAL_Start_Date__c = Date.today(); // historical value
            if(c.SAL_Start_Date_Current__c == null) c.SAL_Start_Date_Current__c = Date.today();
        }

        // SAL Start
        // SAL_Start_Date_Current__c is set in the Opportunity trigger when GI_Acceptance_Status__c is set to GI Accepted
        // Cannot check for GI_Acceptance_Status__c in this trigger because a contact can be part of SOI for many opportunities
        
        // SAL End
        if( (c.Contact_Status__c == 'Goal Confirmed' && cOld.Contact_Status__c != 'Goal Confirmed')
            || (c.Contact_Status__c == 'Champion' && cOld.Contact_Status__c != 'Champion')
            || (c.Contact_Status__c == 'Prove Value' && cOld.Contact_Status__c != 'Prove Value')
            || (c.Contact_Status__c == 'Negotiating' && cOld.Contact_Status__c != 'Negotiating')
            || (c.Contact_Status__c == 'Closed Won' && cOld.Contact_Status__c != 'Closed Won')
            || (c.Contact_Status__c == 'Closed Lost' && cOld.Contact_Status__c != 'Closed Lost') ) {
            if(c.SAL_to_SQL__c == false) {
                c.SAL_to_SQL__c = true;
                if(c.SAL_Start_Date__c == null) c.SAL_Start_Date__c = Date.today(); // historical value
                if(c.SAL_Start_Date_Current__c == null) c.SAL_Start_Date_Current__c = Date.today();
                c.SAL_to_SQL_Age__c += c.SAL_Start_Date_Current__c.daysBetween(Date.today());
            }
            
            // Handle the skipped stages
            if(c.Inquiry_Start_Date_Current__c == null) c.Inquiry_Start_Date_Current__c = Date.today();

            if(c.AQL_Start_Date__c == null) c.AQL_Start_Date__c = Date.today(); // historical value
            if(c.AQL_Start_Date_Current__c == null) c.AQL_Start_Date_Current__c = Date.today();

            if(c.MQL_Start_Date__c == null) c.MQL_Start_Date__c = Date.today(); // historical value
            if(c.MQL_Start_Date_Current__c == null) c.MQL_Start_Date_Current__c = Date.today();
        }

        // MQL Start
        if(c.Contact_Status__c == 'Goal Identified' && cOld.Contact_Status__c != 'Goal Identified') {
            if(c.MQL_Start_Date__c == null) c.MQL_Start_Date__c = Date.today(); // historical value
            c.MQL_Start_Date_Current__c = Date.today(); // current value
        }
        
        // MQL End
        if( (c.Contact_Status__c == 'Goal Confirmed' && cOld.Contact_Status__c != 'Goal Confirmed')
            || (c.Contact_Status__c == 'Goal Rejected' && cOld.Contact_Status__c != 'Goal Rejected')
            || (c.Contact_Status__c == 'Champion' && cOld.Contact_Status__c != 'Champion')
            || (c.Contact_Status__c == 'Prove Value' && cOld.Contact_Status__c != 'Prove Value')
            || (c.Contact_Status__c == 'Negotiating' && cOld.Contact_Status__c != 'Negotiating')
            || (c.Contact_Status__c == 'Closed Won' && cOld.Contact_Status__c != 'Closed Won')
            || (c.Contact_Status__c == 'Closed Lost' && cOld.Contact_Status__c != 'Closed Lost') ) {
            if(c.MQL_to_SAL__c == false) {
                c.MQL_to_SAL__c = true;
                if(c.MQL_Start_Date__c == null) c.MQL_Start_Date__c = Date.today(); // historical value
                if(c.MQL_Start_Date_Current__c == null) c.MQL_Start_Date_Current__c = Date.today();
                c.MQL_to_SAL_Age__c += c.MQL_Start_Date_Current__c.daysBetween(Date.today());
            }

            // Handle the skipped stages
            if(c.Inquiry_Start_Date_Current__c == null) c.Inquiry_Start_Date_Current__c = Date.today();

            if(c.AQL_Start_Date__c == null) c.AQL_Start_Date__c = Date.today(); // historical value
            if(c.AQL_Start_Date_Current__c == null) c.AQL_Start_Date_Current__c = Date.today();
        }
        
        // AQL Start
        if( (c.Contact_Status__c == 'Follow-Up Required' && cOld.Contact_Status__c != 'Follow-Up Required') 
            || (c.Contact_Status__c == 'Follow-Up Attempt 1' && cOld.Contact_Status__c != 'Follow-Up Attempt 1') 
            || (c.Contact_Status__c == 'Follow-Up Attempt 2' && cOld.Contact_Status__c != 'Follow-Up Attempt 2') 
            || (c.Contact_Status__c == 'Follow-Up Attempt 3' && cOld.Contact_Status__c != 'Follow-Up Attempt 3')
            || (c.Contact_Status__c == 'Goal Discovery' && cOld.Contact_Status__c != 'Goal Discovery') ) {
            if(c.AQL_Start_Date__c == null) c.AQL_Start_Date__c = Date.today(); // historical value
            c.AQL_Start_Date_Current__c = Date.today(); // current value
        }

        // AQL End
        if( (c.Contact_Status__c == 'Archived' && cOld.Contact_Status__c != 'Archived')
            || (c.Contact_Status__c == 'Goal Identified' && cOld.Contact_Status__c != 'Goal Identified')
            || (c.Contact_Status__c == 'Goal Confirmed' && cOld.Contact_Status__c != 'Goal Confirmed')
            || (c.Contact_Status__c == 'Goal Rejected' && cOld.Contact_Status__c != 'Goal Rejected')
            || (c.Contact_Status__c == 'Champion' && cOld.Contact_Status__c != 'Champion')
            || (c.Contact_Status__c == 'Prove Value' && cOld.Contact_Status__c != 'Prove Value')
            || (c.Contact_Status__c == 'Negotiating' && cOld.Contact_Status__c != 'Negotiating')
            || (c.Contact_Status__c == 'Closed Won' && cOld.Contact_Status__c != 'Closed Won')
            || (c.Contact_Status__c == 'Closed Lost' && cOld.Contact_Status__c != 'Closed Lost') ) {
            if(c.AQL_to_MQL__c == false) {
                c.AQL_to_MQL__c = true;
                if(c.AQL_Start_Date__c == null) c.AQL_Start_Date__c = Date.today(); // historical value
                if(c.AQL_Start_Date_Current__c == null) c.AQL_Start_Date_Current__c = Date.today();
                c.AQL_to_MQL_Age__c += c.AQL_Start_Date_Current__c.daysBetween(Date.today());
            }

            // Handle the skipped stages
            if(c.Inquiry_Start_Date_Current__c == null) c.Inquiry_Start_Date_Current__c = Date.today();
        }

        // Inquiry End
        if( (c.Contact_Status__c == 'Follow-Up Required' && cOld.Contact_Status__c != 'Follow-Up Required')
            || (c.Contact_Status__c == 'Follow-Up Attempt 1' && cOld.Contact_Status__c != 'Follow-Up Attempt 1')
            || (c.Contact_Status__c == 'Follow-Up Attempt 2' && cOld.Contact_Status__c != 'Follow-Up Attempt 2')
            || (c.Contact_Status__c == 'Follow-Up Attempt 3' && cOld.Contact_Status__c != 'Follow-Up Attempt 3')
            || (c.Contact_Status__c == 'Follow-Up Rejected' && cOld.Contact_Status__c != 'Follow-Up Rejected')
            || (c.Contact_Status__c == 'Follow-Up Disqualified' && cOld.Contact_Status__c != 'Follow-Up Disqualified')
            || (c.Contact_Status__c == 'Contacted-Additional Work Required' && cOld.Contact_Status__c != 'Contacted-Additional Work Required')
            || (c.Contact_Status__c == 'Archived' && cOld.Contact_Status__c != 'Archived')
            || (c.Contact_Status__c == 'Goal Discovery' && cOld.Contact_Status__c != 'Goal Discovery')
            || (c.Contact_Status__c == 'Goal Identified' && cOld.Contact_Status__c != 'Goal Identified')
            || (c.Contact_Status__c == 'Goal Confirmed' && cOld.Contact_Status__c != 'Goal Confirmed')
            || (c.Contact_Status__c == 'Goal Rejected' && cOld.Contact_Status__c != 'Goal Rejected')
            || (c.Contact_Status__c == 'Champion' && cOld.Contact_Status__c != 'Champion')
            || (c.Contact_Status__c == 'Prove Value' && cOld.Contact_Status__c != 'Prove Value')
            || (c.Contact_Status__c == 'Negotiating' && cOld.Contact_Status__c != 'Negotiating')
            || (c.Contact_Status__c == 'Closed Won' && cOld.Contact_Status__c != 'Closed Won')
            || (c.Contact_Status__c == 'Closed Lost' && cOld.Contact_Status__c != 'Closed Lost') ) {
            
            if(c.Inquiry_to_AQL__c == false) {
                c.Inquiry_to_AQL__c = true;
                if(c.Inquiry_Start_Date_Current__c == null) c.Inquiry_Start_Date_Current__c = Date.today(); // if it is still null
                c.Inquiry_to_AQL_Age__c += c.Inquiry_Start_Date_Current__c.daysBetween(Date.today());
            }
        }
        // HANDLE GOING FORWARDS - END      

    	}
	}
}