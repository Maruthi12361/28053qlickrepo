/*
* File UpdateDealSplitChildOppsSchedulableTest
    * @description : Unit test for UpdateDealSplitChildOppsSchedulable
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       03.01.2018    Pramod Kumar V     Created Class

*/
@isTest(seeAlldata=false)
private class UpdateDealSplitChildOppsSchedulableTest{
    
   
   static testMethod void test_UpdateDealSplitChildOppsSchedulable() {   
          //UpdateDealSplitChildOpportunities batch = new UpdateDealSplitChildOpportunities();
          //Database.executeBatch(batch);
          UpdateDealSplitChildOppsSchedulable udcOpp=new UpdateDealSplitChildOppsSchedulable();
          String sch = '20 30 8 10 2 ?';
          String jobID = system.schedule('Merge Job', sch, udcOpp); 
    }
  }