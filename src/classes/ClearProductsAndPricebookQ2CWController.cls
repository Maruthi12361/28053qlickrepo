/************************************************************************
*
*   ClearProductsAndPricebookQ2CWController
*   
*   This controller is reached from a button (via a pass-through vf page) and is used to
*   convert an non steel brick opp to steel brick type by removing any products, resetting the Pricebook 
*   and changing the Opp Record Type to Steelbrick record type 
*       
*   Changelog:
*       2016-06-16 IRN created the file
*       2017-07-18 1.1 Srinivasan PR- Changes for QCW-2955 - Nullifying License Recipient field for OEM orders
*		2017-07-27 Sergii Grushai QCW-2579 - lines 62-64 - checking checkbox for bypass validation rule
*************************************************************************/
public with sharing class ClearProductsAndPricebookQ2CWController {
     public Opportunity Opp {get; private set;}

      Id academic_program_OppRecordType;
      Id deal_split_child_OppRecordType;
      Id oem_order_OppRecordType;
      Id oem_order_deal_split_child_OppRecordType;
      Id oem_prepay_OppRecordType;
      Id oem_recruitment_OppRecordType;
      Id oem_recruitment_deal_split_child_OppRecordType;
      Id qlik_online_purchase_OppRecordType;
      Id steelbrick_OppRecordType;
      Id nfr_OppRecordType;

    public ClearProductsAndPricebookQ2CWController(ApexPages.StandardController controller) {
        System.debug('ClearProductsAndPricebookQ2CWController');
        this.Opp = (Opportunity)controller.getRecord();
        
        //Get the stuff we need to get at OpportunityLineItems for this Opportunity.
        this.Opp = [SELECT Id, Amount, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, 
                            Total_Training_Amount__c, Is_Services_Opportunity__c, RecordTypeId, Revenue_Type__c,
                            Pricebook2Id,Closed_date_for_conga__c,License_Recipient__c, //1.1 added License Recipient
                               (SELECT Quote_Id__c FROM NS_Quotes__r),
                               (select Id, OpportunityId, Name__c, PricebookEntryId, UnitPrice from OpportunityLineItems) 
                          FROM Opportunity 
                          WHERE Id = :Opp.Id LIMIT 1];               
    }

    public pagereference doTheClear() // do the clear
    {
        populateRecordTypesVariables();
        Id newRecordTypeId = getNewRecordType(opp.RecordTypeId);

        opp.Revenue_Type__c = getNewRevenueType(opp.RecordTypeId, opp.Revenue_Type__c);

        Id ID_SB_Pricebook;
        if(Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails') != null) {
            ID_SB_Pricebook = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').SBPricebookId__c;
        }
        
        /** changes for 1.1 start **/
        if((opp.RecordTypeId==oem_order_OppRecordType||opp.RecordTypeId==oem_recruitment_OppRecordType) && opp.License_Recipient__c!=null){
            opp.License_Recipient__c=null;
        }
        /** changes for 1.1 end**/

        if(Opp.RecordTypeId != newRecordTypeId){
            Opp.RecordTypeId = newRecordTypeId;
            if(Opp.RecordTypeId == steelbrick_OppRecordType) {
                Opp.Bypass_Rules__c = true;
            }
        }   

        if ((Opp.Pricebook2Id == null) || (Opp.Pricebook2Id != ID_SB_Pricebook)) { //Pricebook not set to "SteelBrick Pricebook so we need to change pricebook to steelbrick and remove line items"
            //check if we already have quotes to the 
            list<NS_Quote__c> q = Opp.NS_Quotes__r;
            if(q.size()>0){
                system.debug('Remove all quotes ( '+ q.size() +' ) on the opportunity');
                try{
                    delete q;
                    }catch(Exception e){
                        system.debug('IN CATCH ' + e.getMessage());
                    }

            }
            System.debug('After quote deletion');
            //Check if we already have Products added - if so we want to delete them
            list<OpportunityLineItem> existing_oli = Opp.OpportunityLineItems;
            if (existing_oli.size() > 0) {
                System.debug('ClearProductsAndPricebooQ2CWController existing_oli.size() is: ' + existing_oli.size());
                delete existing_oli;    //delete the existing products      
            }              

            //If closed date is already populated then remove that otherwise it will close the opportunity.
            if(opp.Closed_date_for_conga__c != null){
                opp.Closed_date_for_conga__c = null;
            }
            
            Opp.Pricebook2Id = ID_SB_Pricebook; 
            //Opportunity_ManageForecastProductsHasRun may have already run so we need to clear the flags as we want it to run again for the update - this adds the Amount to the LFA field
            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
            System.debug('ClearProductsAndPricebookController Before update');
            update Opp; //Update the Opp

        }
        return new PageReference('/'+ Opp.Id);
    }

    public void populateRecordTypesVariables(){
        if(Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails') != null) {
            academic_program_OppRecordType = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Academic_program_OppRecordType__c;
            oem_order_OppRecordType = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Oem_order_OppRecordType__c;
            oem_prepay_OppRecordType = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Oem_prepay_OppRecordType__c;
            oem_recruitment_OppRecordType = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Oem_recruitment_OppRecordType__c;
            steelbrick_OppRecordType = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Steelbrick_OppRecordType__c;
            deal_split_child_OppRecordType =Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Deal_split_child_OppRecordType__c;
            oem_order_deal_split_child_OppRecordType =Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').OEM_Order_Deal_Split_Child_OppRecordType__c;
            oem_recruitment_deal_split_child_OppRecordType =Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').OEM_Recruitment_Deal_Split_Child_Id__c;
            qlik_online_purchase_OppRecordType =Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Qlik_online_purchase_OppRecordType__c;
            nfr_OppRecordType =Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Partner_Not_For_Resale_OppRecordType__c;
        }
    }

    //method to find which new recordtype to use
    public Id getNewRecordType (Id oldRecordType){
        Id newRecordTypeId; 
        
        if(oldRecordType == academic_program_OppRecordType || oldRecordType == oem_order_OppRecordType || oldRecordType == oem_recruitment_OppRecordType 
            ||oldRecordType == deal_split_child_OppRecordType || oldRecordType == qlik_online_purchase_OppRecordType 
            || oldRecordType == oem_order_deal_split_child_OppRecordType || oldRecordType == oem_recruitment_deal_split_child_OppRecordType || oldRecordType == nfr_OppRecordType)//these recortypes should remain the same
        {
            newRecordTypeId = oldRecordType;

        }else if(oldRecordType == oem_prepay_OppRecordType ){//this record type should be changed to oem order
            newRecordTypeId = oem_order_OppRecordType;
        }else{//everyone else should be changed to the new steelbrick record type Sales QCCS
            newRecordTypeId = steelbrick_OppRecordType;
        }
        
        return newRecordTypeId;
    }

    public String getNewRevenueType (Id oldRecordType, String oldRevenueType){
        String newRevenueType; 
        if(oldRecordType == oem_prepay_OppRecordType || oldRecordType == oem_order_OppRecordType){//this revenue type should be changed to oem 
            newRevenueType = 'OEM';
        }else if(oldRecordType == oem_recruitment_OppRecordType || oldRecordType == oem_recruitment_deal_split_child_OppRecordType){//this revenue type should be changed to oem 
            newRevenueType = 'OEM Recruitment';
        }else{
            newRevenueType = oldRevenueType;
        }
        return newRevenueType;
    }
}