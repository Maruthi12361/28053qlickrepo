@isTest
public class Z_QuoteRatePlanChargeTriggerHandlerTest {
    @isTest(SeeAllData=false)
    static void testCharges(){    
        QTTestUtils.GlobalSetUp();
        Product2 prod = Z_TestFactory.makeProduct();
        zqu__ProductRatePlan__c prp = Z_TestFactory.makeProductRatePlan(String.valueOf(prod.Id), 'prp');
        zqu__ProductRatePlanCharge__c prpc = Z_TestFactory.makeProductRatePlanCharge(String.valueOf(prp.Id), 'Recurring', 'Flat Fee', 'Test');
         
        //Create Account/Opportunity/Quote
        Account acc = Z_TestFactory.makePartnerAccount('country', 'cName', 'nState', 'zipcode');
        acc.Pending_Validation__c = false;
         insert acc;
        
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(acc, 'testBill');
        insert bill;
        
        Test.startTest();
        
        Zuora__Subscription__c sub = Z_TestFactory.MakeSubscription(bill, acc, 'testSub');
        insert sub;
        
        Zuora__SubscriptionProductCharge__c spc = Z_TestFactory.MakeSPC(sub, acc, 'testSPC');
        spc.Zuora__External_Id__c = '1';
        insert spc;

        Contact contact = Z_TestFactory.makeContact(acc);
        insert contact;

        Opportunity opp = Z_TestFactory.buildOpportunity(acc);
        opp.Revenue_Type__c='Direct';
         insert opp;
        zqu__Quote__c quote = Z_TestFactory.makeQuote(opp, acc);
        quote.zqu__BillToContact__c = contact.id;
        quote.zqu__SoldToContact__c = contact.id;
        quote.Billing_Frequency__c = 'Prepaid';
        insert quote;
        
        zqu__QuoteAmendment__c qa = Z_TestFactory.makeQuoteAmendment(quote);
         insert qa;
        zqu__QuoteRatePlan__c qrp = Z_TestFactory.makeQRP(quote, qa, prp);
         insert qrp;
        zqu__QuoteRatePlanCharge__c qrpc = Z_TestFactory.makeQRPC(quote, qrp, prpc, 'Test');
        qrpc.zqu__SubscriptionRatePlanChargeZuoraId__c = '1';
         insert qrpc;
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false)
    static void testChargesFail(){
        QTTestUtils.GlobalSetUp();
        Product2 prod = Z_TestFactory.makeProduct();
        zqu__ProductRatePlan__c prp = Z_TestFactory.makeProductRatePlan(String.valueOf(prod.Id), 'prp');
        zqu__ProductRatePlanCharge__c prpc = Z_TestFactory.makeProductRatePlanCharge(String.valueOf(prp.Id), 'Recurring', 'Flat Fee', 'Test');
         
        //Create Account/Opportunity/Quote
        Account acc = Z_TestFactory.makePartnerAccount('country', 'cName', 'nState', 'zipcode');
        acc.Pending_Validation__c = false;
         insert acc;
        /*
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(acc, 'testBill');
        insert bill;
        
        Zuora__Subscription__c sub = Z_TestFactory.MakeSubscription(bill, acc, 'testSub');
        insert sub;
        
        Zuora__SubscriptionProductCharge__c spc = Z_TestFactory.MakeSPC(sub, acc, 'testSPC');
        spc.Zuora__ChargeNumber__c = '1';
        insert spc;
        */
        Test.startTest();
        Contact contact = Z_TestFactory.makeContact(acc);
        insert contact;

        Opportunity opp = Z_TestFactory.buildOpportunity(acc);
        opp.Revenue_Type__c='Direct';
         insert opp;
        zqu__Quote__c quote = Z_TestFactory.makeQuote(opp, acc);
        quote.zqu__BillToContact__c = contact.id;
        quote.zqu__SoldToContact__c = contact.id;
        quote.Billing_Frequency__c = 'Prepaid';
        insert quote;
        
        zqu__QuoteAmendment__c qa = Z_TestFactory.makeQuoteAmendment(quote);
         insert qa;
        zqu__QuoteRatePlan__c qrp = Z_TestFactory.makeQRP(quote, qa, prp);
         insert qrp;
        zqu__QuoteRatePlanCharge__c qrpc = Z_TestFactory.makeQRPC(quote, qrp, prpc, 'Test');
        //qrpc.zqu__ChargeNumber__c = '1';
         insert qrpc;
         Test.stopTest();
    }
}