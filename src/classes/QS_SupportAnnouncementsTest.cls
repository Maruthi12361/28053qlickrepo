/**
* QS_SupportAnnouncementsTest 
*
* Change log:
*
* 16-11-2018 - ext_vos - CHG0034901: Add component for showing SupportAnnouncements.
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*
*/
@isTest
public class QS_SupportAnnouncementsTest {

	static testMethod void test() {
    	QS_SupportAnnouncementsController contr = new QS_SupportAnnouncementsController();
		System.assertEquals(0, contr.getNumberOfAnnouncements());

    	Support_Announcement__c newSA = 
    				new Support_Announcement__c(Announcement_Header__c = 'test header', Announcement_Text__c = 'test text', Active__c = true, End_Date__c = null);
    	insert newSA;
    	Support_Announcement__c newSA2 = 
    				new Support_Announcement__c(Announcement_Header__c = 'test2 header', Announcement_Text__c = 'test2 text', Active__c = true, End_Date__c = null);
    	insert newSA2;

		contr = new QS_SupportAnnouncementsController();
		System.assertEquals('test header', contr.announcements[0].Announcement_Header__c);
		System.assertEquals(2, contr.getNumberOfAnnouncements());
    }

}