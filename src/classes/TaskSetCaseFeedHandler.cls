/**************************************************
*
* Change Log:
* 
* 2017-11-23	ext_vos	CR#CHG0030519	Created.
*										Populate Task.Comments value to linked Case feed (for Cisco B&S calls task).
* 2017-12-13	ext_vos CR#CHG0032770	Changed the type of case feed item: the new "log a call" task is created, add "is custom" flag.
**************************************************/
public class TaskSetCaseFeedHandler {
	
	public static void handle(List<Task> newTasks, Map<Id,Task> mapOldTasks) {
		List<Task> items = new List<Task>();
		
		ID supportTaskRTId = Task.SObjectType.getDescribe().getRecordTypeInfosByName().get('Support Tasks').getRecordTypeId();

		for (Task task : newTasks) {
			Task oldTask = mapOldTasks == null ? null : mapOldTasks.get(task.Id);

			if (!task.Is_Custom_Log_Call__c && !String.isEmpty(task.Subject) && task.Subject.startsWith('Call')
					&& (task.CallType == 'Inbound' || task.CallType == 'Outbound')
					&& !String.isEmpty(task.Description) && task.WhatId != null && 'Case'.equals(task.WhatId.getSobjectType() + '')
					&& ((oldTask == null) // insert
						|| (oldTask != null // update
							&& (oldTask.WhatId != task.WhatId || oldTask.Description != task.Description)))) {

				items.add(new Task(Description = task.CallType + ' call with comment: ' + task.Description,
									RecordTypeId = supportTaskRTId,
									WhatId = task.WhatId,
					                TaskSubtype = 'Call',
					                Status = 'Completed', 
					                Type = 'Call',
					                Subject = task.Subject + ' B+S Case Feed', 
					                ActivityDate = Date.today(),
					                CallType = task.CallType,
					                CallObject = 'B+S Internal log',
					                Is_Custom_Log_Call__c = true)
							);
			}
		}
		insert items;
	}
}