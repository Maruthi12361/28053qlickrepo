/*******************************************************************
* File UpdateCustomSettingsTest
  * @description :
  * @author : Pramod Kumar V
  * Modification Log ===============================================================
  Ver     Date         Author              Modification
  1       09.01.2018   Pramod Kumar V      Created Class
  2       28.04.2018   Pramod Kumar v      ITRM-60 Added assert methods
  3       06.03.2019   ext_bjd             ITRM-320 Added method getUrlForInstance to determine instance url and update
                                           custom settings Support_Portal_Login_Url__c and Support_Portal_Url__c
  4       03.02.2020   ext_bjd             ITRM-380 Added new custom settings, code refactored
********************************************************************/
@IsTest
public class UpdateCustomSettingsTest {

    @TestSetup
    static void setup() {
        // Custom Settings creation
        QTTestUtils.createQTCustomSettingsTestData();
        QTTestUtils.createSteelBrickSettingsTestData();
        QTTestUtils.createQVMSettingsTestData();
        QTTestUtils.createQS_PartnerPortalUrlsTestData();
        QTTestUtils.createQTCustomSettingsHierTestData();
    }

    @IsTest
    static void testingCustomSettingsforQA() {
        Test.startTest();

        UpdateCustomSettings cs = new UpdateCustomSettings();
        RefreshSandboxData__mdt refeshData = cs.getRefreshSandboxDataBySandboxName('QA');
        UpdateCustomSettings.updateCustomSettingsFromMdt(refeshData);

        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();
        QTCustomSettings__c updatedQT = [SELECT BoomiBaseURL__c, ULC_Base_URL__c FROM QTCustomSettings__c];
        Steelbrick_Settings__c updatedSB = [SELECT BoomiBaseURL__c, BoomiToken__c, SpringAccountPrefix__c, CMInstanceUrl__c FROM Steelbrick_Settings__c];
        QVM_Settings__c updatedQVM = [SELECT QlikMarket_Website__c FROM QVM_Settings__c];
        QS_Partner_Portal_Urls__c updatedQS = [SELECT Attunity_Portal_Url__c, Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c, Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c FROM QS_Partner_Portal_Urls__c];
        QTCustomSettingsHier__c updatedQTHier = [SELECT Base_URL__c, Base_Content_URL__c, Close_Chat_Survey_Id__c, Close_Chat_Survey_Id_QA__c, Survey_Link_For_Live_Chat__c, Survey_Link_For_Live_Chat_QA__c, Survey_Link__c, Survey_Link_QA__c FROM QTCustomSettingsHier__c];

        Test.stopTest();

        assertsForCustomSettings(updatedQT, updatedQTHier, updatedSB, updatedQVM, updatedQS, urlsInstance, refeshData);
    }

    @IsTest
    static void testingCustomSettingsforDefault() {
        Test.startTest();

        UpdateCustomSettings cs = new UpdateCustomSettings();
        RefreshSandboxData__mdt refeshData = cs.getRefreshSandboxDataBySandboxName('test');
        UpdateCustomSettings.updateCustomSettingsFromMdt(refeshData);

        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();
        QTCustomSettings__c updatedQT = [SELECT BoomiBaseURL__c, ULC_Base_URL__c FROM QTCustomSettings__c];
        Steelbrick_Settings__c updatedSB = [SELECT BoomiBaseURL__c, BoomiToken__c, SpringAccountPrefix__c, CMInstanceUrl__c FROM Steelbrick_Settings__c];
        QVM_Settings__c updatedQVM = [SELECT QlikMarket_Website__c FROM QVM_Settings__c];
        QS_Partner_Portal_Urls__c updatedQS = [SELECT Attunity_Portal_Url__c, Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c, Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c FROM QS_Partner_Portal_Urls__c];
        QTCustomSettingsHier__c updatedQTHier = [SELECT Base_URL__c, Base_Content_URL__c, Close_Chat_Survey_Id__c, Close_Chat_Survey_Id_QA__c, Survey_Link_For_Live_Chat__c, Survey_Link_For_Live_Chat_QA__c, Survey_Link__c, Survey_Link_QA__c FROM QTCustomSettingsHier__c];

        Test.stopTest();

        assertsForCustomSettings(updatedQT, updatedQTHier, updatedSB, updatedQVM, updatedQS, urlsInstance, refeshData);
    }

    @IsTest
    static void testingCustomSettingsforNull() {
        Test.startTest();

        UpdateCustomSettings cs = new UpdateCustomSettings();
        RefreshSandboxData__mdt refeshData = cs.getRefreshSandboxDataBySandboxName('');
        UpdateCustomSettings.updateCustomSettingsFromMdt(refeshData);

        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();
        QTCustomSettings__c updatedQT = [SELECT BoomiBaseURL__c, ULC_Base_URL__c FROM QTCustomSettings__c];
        Steelbrick_Settings__c updatedSB = [SELECT BoomiBaseURL__c, BoomiToken__c, SpringAccountPrefix__c, CMInstanceUrl__c FROM Steelbrick_Settings__c];
        QVM_Settings__c updatedQVM = [SELECT QlikMarket_Website__c FROM QVM_Settings__c];
        QS_Partner_Portal_Urls__c updatedQS = [SELECT Attunity_Portal_Url__c, Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c, Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c FROM QS_Partner_Portal_Urls__c];
        QTCustomSettingsHier__c updatedQTHier = [SELECT Base_URL__c, Base_Content_URL__c, Close_Chat_Survey_Id__c, Close_Chat_Survey_Id_QA__c, Survey_Link_For_Live_Chat__c, Survey_Link_For_Live_Chat_QA__c, Survey_Link__c, Survey_Link_QA__c FROM QTCustomSettingsHier__c];

        Test.stopTest();

        assertsForCustomSettings(updatedQT, updatedQTHier, updatedSB, updatedQVM, updatedQS, urlsInstance, refeshData);
    }

    @IsTest
    static void testingCustomSettingsforProd() {
        QTCustomSettings__c qtSetting = QTTestUtils.createQTCustomSettingsTestDataForLive();
        QTCustomSettingsHier__c qtSettingHier = QTTestUtils.createQTCustomSettingsHierTestDataForLive();
        Steelbrick_Settings__c sbSetting = QTTestUtils.createSteelBrickSettingsTestDataForLive();
        QVM_Settings__c qvmSetting = QTTestUtils.createQVMSettingsTestDataForLive();
        QS_Partner_Portal_Urls__c qsSetting = QTTestUtils.createQS_PartnerPortalUrlsTestDataForLive();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        Test.startTest();

        RefreshSandboxData__mdt refeshData = UpdateCustomSettings.getProdCustomSettingSandboxData();

        Test.stopTest();

        Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        if (!IsSandbox) {

            System.assertEquals(qtSetting.ULC_Base_URL__c, refeshData.ULC_Base_URL__c);
            System.assertEquals(qtSetting.BoomiBaseURL__c, refeshData.BoomiBaseURL__c);

            System.assertEquals(sbSetting.BoomiBaseURL__c, refeshData.BoomiBaseURL__c);
            System.assertEquals(sbSetting.BoomiToken__c, refeshData.BoomiToken__c);
            System.assertEquals(sbSetting.CMInstanceUrl__c, refeshData.CMInstanceUrl__c);
            System.assertEquals(sbSetting.SpringAccountPrefix__c, refeshData.SpringAccountPrefix__c);

            System.assertEquals(qvmSetting.QlikMarket_Website__c, refeshData.QlikMarket_Website__c);

            //System.assertEquals(qsSetting.Attunity_Portal_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM + '/' + UpdateCustomSettings.URL_PART_ATTUNITYCUTOMERPORTAL + '/s' : refeshData.Attunity_Portal_Url__c);
            //System.assertEquals(qsSetting.Support_Portal_Login_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : qsSetting.Support_Portal_Login_Url__c, refeshData.Support_Portal_Login_Url__c);
            System.assertEquals(qsSetting.Support_Portal_CSS_Base__c, refeshData.Support_Portal_CSS_Base__c);
            System.assertEquals(qsSetting.Support_Portal_Live_Agent_API_Endpoint__c, refeshData.Support_Portal_Live_Agent_API_Endpoint__c);
            System.assertEquals(qsSetting.Support_Portal_Login_Page_Url__c, refeshData.Support_Portal_Login_Page_Url__c);
            //System.assertEquals(qsSetting.Support_Portal_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0] + '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM + '/' : qsSetting.Support_Portal_Url__c, refeshData.Support_Portal_Url__c);
            System.assertEquals(qsSetting.Support_Portal_Logout_Page_Url__c, refeshData.Support_Portal_Logout_Page_Url__c);
            System.assertEquals(qsSetting.Support_Portal_Url_Base__c, refeshData.Support_Portal_Url_Base__c);

            System.assertEquals(qtSettingHier.Close_Chat_Survey_Id__c, refeshData.Close_Chat_Survey_Id__c);
            System.assertEquals(qtSettingHier.Close_Chat_Survey_Id_QA__c, refeshData.Close_Chat_Survey_Id_QA__c);
            System.assertEquals(qtSettingHier.Survey_Link_For_Live_Chat__c, refeshData.Survey_Link_For_Live_Chat__c);
            System.assertEquals(qtSettingHier.Survey_Link_For_Live_Chat_QA__c, refeshData.Survey_Link_For_Live_Chat_QA__c);
            System.assertEquals(qtSettingHier.Survey_Link__c, refeshData.Survey_Link__c);
            System.assertEquals(qtSettingHier.Survey_Link_QA__c, refeshData.Survey_Link_QA__c);
        }
    }

    // To avoid duplication of code, the checks of asserts is made in a separate method but with a large number of parameters,
    // which is not good practice but help to avoid duplication.
    static void assertsForCustomSettings(QTCustomSettings__c qtSetting, QTCustomSettingsHier__c qtSettingHier,
            Steelbrick_Settings__c sbSetting, QVM_Settings__c qvmSetting, QS_Partner_Portal_Urls__c qsSetting,
            List<String> urlsInstance, RefreshSandboxData__mdt refeshData) {
        System.assertEquals(qtSetting.ULC_Base_URL__c, refeshData.ULC_Base_URL__c);
        System.assertEquals(qtSetting.BoomiBaseURL__c, refeshData.BoomiBaseURL__c);

        System.assertEquals(sbSetting.BoomiBaseURL__c, refeshData.BoomiBaseURL__c);
        System.assertEquals(sbSetting.BoomiToken__c, refeshData.BoomiToken__c);
        System.assertEquals(sbSetting.CMInstanceUrl__c, refeshData.CMInstanceUrl__c);
        System.assertEquals(sbSetting.SpringAccountPrefix__c, refeshData.SpringAccountPrefix__c);

        System.assertEquals(qvmSetting.QlikMarket_Website__c, refeshData.QlikMarket_Website__c);

        System.assertEquals(qsSetting.Attunity_Portal_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM + '/' + UpdateCustomSettings.URL_PART_ATTUNITYCUTOMERPORTAL + '/s' : refeshData.Attunity_Portal_Url__c);
        System.assertEquals(qsSetting.Support_Portal_Login_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : refeshData.Support_Portal_Login_Url__c);
        System.assertEquals(qsSetting.Support_Portal_CSS_Base__c, refeshData.Support_Portal_CSS_Base__c);
        System.assertEquals(qsSetting.Support_Portal_index_allow_options__c, refeshData.Support_Portal_index_allow_options__c);
        System.assertEquals(qsSetting.Support_Portal_Live_Agent_API_Endpoint__c, refeshData.Support_Portal_Live_Agent_API_Endpoint__c);
        System.assertEquals(qsSetting.Support_Portal_Login_Page_Url__c, refeshData.Support_Portal_Login_Page_Url__c);
        System.assertEquals(qsSetting.Support_Portal_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0] + '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM + '/' : refeshData.Support_Portal_Url__c);
        System.assertEquals(qsSetting.Support_Portal_Logout_Page_Url__c, refeshData.Support_Portal_Logout_Page_Url__c);
        System.assertEquals(qsSetting.Support_Portal_Url_Base__c, refeshData.Support_Portal_Url_Base__c);

        System.assertEquals(qtSettingHier.Base_URL__c, UpdateCustomSettings.SALESFORCE_BASE_URL);
        System.assertEquals(qtSettingHier.Base_Content_URL__c, UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.SALESFORCE_BASE_URL_PARTS[0] + '--c.' + UpdateCustomSettings.SALESFORCE_BASE_URL_PARTS[1] + '.content.' + UpdateCustomSettings.URL_PART_FORCE_COM);
        System.assertEquals(qtSettingHier.Close_Chat_Survey_Id__c, refeshData.Close_Chat_Survey_Id__c);
        System.assertEquals(qtSettingHier.Close_Chat_Survey_Id_QA__c, refeshData.Close_Chat_Survey_Id_QA__c);
        System.assertEquals(qtSettingHier.Survey_Link_For_Live_Chat__c, refeshData.Survey_Link_For_Live_Chat__c);
        System.assertEquals(qtSettingHier.Survey_Link_For_Live_Chat_QA__c, refeshData.Survey_Link_For_Live_Chat_QA__c);
        System.assertEquals(qtSettingHier.Survey_Link__c, refeshData.Survey_Link__c);
        System.assertEquals(qtSettingHier.Survey_Link_QA__c, refeshData.Survey_Link_QA__c);
    }
}