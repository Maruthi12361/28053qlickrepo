//*********************************************************/
// Author: Mark Cane&
// Creation date: 19/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efClassWrapper{
	private List<efProductWrapper> classList = new List<efProductWrapper>();
	private String groupName='';
	private Datetime gDate;
	
	public String getGroupName(){
		return this.groupName;
	}
	
	public  void setGroupName(String name){
		this.groupName = name;
	}
	
	public Datetime getGroupDate(){
		return this.gDate;
	}
	
	public  void setGroupDate(Datetime dt){
		this.gDate = dt;
	}
	 
	public List<efProductWrapper> getGroupClasses(){
		return this.classList;
	}
	
	public void addClass(efProductWrapper p){
		this.classList.add(p);
	}
	
	public void setClassList(List<efProductWrapper> cList){
		this.classList = cList;
	}
	// End accessor methods.	
}