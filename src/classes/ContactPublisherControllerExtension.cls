/*************************************************************
*Author: CCE
* 
* 2015-03-18 CCE CR# 16347 - Add Custom Publisher Action for Lead Generation on Contacts
* 2018-04-18  ext_bad CHG0033720   Change org wide email, select it from custom settings.
*
**************************************************************/

public class ContactPublisherControllerExtension 
{
    public ContactPublisherControllerExtension(ApexPages.StandardController stdController) 
    {
        caseController = stdController;
        if(!Test.isRunningTest()) {
            List<String> fieldNames = new List<String>{'Name', 'Account.Responsible_Partner__r.Name', 'Account_Type__c', 'Phone', 'Email', 'Account.Name', 'Account.Owner.Email', 'DefaultContact__c'};
            caseController.Addfields(fieldNames);
        }
        
        ContactObj = (Contact)stdController.getRecord();
        Description = '';            
        Email = ContactObj.Email; 
        PhoneNumber = ContactObj.Phone;
        AccountName = String.valueOf(ContactObj.Account.Name);
        ConsentCheckBox = false;
        NotInSFCheckBox = false;
        bNext = false;
        bSubmit = true;
        ContactDetails = new Contact();        
        ContactObj.DefaultContact__c = ContactObj.Id;
        System.debug('ContactPublisherControllerExtension: ContactPublisherControllerExtension: Email = ' + Email);
    }
           
    public PageReference NextStep()
    { 
        System.debug('ContactPublisherControllerExtension: NextStep: ContactObj.Id = ' + ContactObj.Id);
        if(ContactObj.Id != null)
        {
            bNext=true;
            contactDetails.id = ContactObj.Id;
            System.debug('ContactPublisherControllerExtension: NextStep: contactDetails.Name = ' + contactDetails.Name);
        }
        return null;                     
    }            
    /*******************************************************
    * Submit button Action
    * Create Campaigns member
    * Notify by E-mail
    * Notify by Chatter message
    *
    *******************************************************/
    public void SubmitCampaign()
    {                               
        System.Debug('ContactPublisherControllerExtension: SubmitCampaign: TypeOfLead  = ' + TypeOfLead);
        System.Debug('ContactPublisherControllerExtension: SubmitCampaign: Description  = ' + Description);
        System.Debug('ContactPublisherControllerExtension: SubmitCampaign: contactDetails = ' + contactDetails);
        
        Comments = String.Format('{0}\n Origin: Contact Id {1}, UserName: {2}, User Email: {3}', new String[]{Description, ContactObj.Id, UserInfo.getName(),UserInfo.getUserEmail()});
        
        if (TypeOfLead == System.Label.Software_or_License) {            
                CampaignName = System.Label.Global_Service_Campaign;
        }
        else {  // “Training/Consulting/Support”
                CampaignName = System.Label.Global_Service_Other_Campaign;
        }
        System.Debug('ContactPublisherControllerExtension: SubmitCampaign: CampaignName = ' + CampaignName);   
        System.Debug('ContactPublisherControllerExtension: SubmitCampaign: NotInSFCheckBox = ' + NotInSFCheckBox);   
        if(!NotInSFCheckBox)
        {
            List<Campaign> campaignIds = new List<Campaign>();
            CampaignMember cMember = new CampaignMember ();                                
            
            campaignIds  = [select Name, Id from Campaign where Name =: CampaignName ];
            if (campaignIds.Size() >0)
            {
                cMember.campaignId = campaignIds[0].Id;
                cMember.ContactId = ContactDetails.Id;
                cMember.Comments__c = Comments;                    
                cMember.Status = 'Target';                    
                System.Debug('ContactPublisherControllerExtension: SubmitCampaign: cMember = ' + cMember);
                try
                {
                    insert cMember;
                }
                catch(System.DMLException ex)
                {
                    ApexPages.addMessages(ex);
                    return;                                           
                }
                cMemberId = cMember.Id;
                System.Debug('ContactPublisherControllerExtension: SubmitCampaign: Comments__c  = ' + cMember.Comments__c);                      
            }
            else
            {
                System.Debug('ContactPublisherControllerExtension: Campaign does not exist');
                ApexPages.Message errMessage = new ApexPages.Message(ApexPages.Severity.Warning, 'Campaign does not exist');
                ApexPages.addMessage(errMessage);
                return;
            }            
        }
        SendEmail();
                        
        CreateChatter();
    }
    /*******************************************************
    *  Notify servicesales@qlikview.com and Account owner about the lead generation 
    *  by E-mail to Account owner and servicesales@qlikview.com 
    *
    *******************************************************/        
    private void SendEmail()
    {               
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String messageBody = '';
        String[] toAddresses = new String[] {System.Label.Qlik_service_email_address, ContactDetails.Account.Owner.Email};//{'servicesales@qlik.com', ContactDetails.Account.Owner.Email};                
        mail.setToAddresses(toAddresses); 
        
        mail.setSaveAsActivity(false);
        
        //set the FROM address by setting the org-wide address to use
        string Qlik_noreply_email_address = '';
        if(QTCustomSettings__c.getInstance('Default') != null) {
            Qlik_noreply_email_address = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
        }
        OrgwideEmailAddress[] orgwideaddress = [select id from orgwideEmailAddress where displayname = :Qlik_noreply_email_address Limit 1]; 
        //OrgwideEmailAddress[] orgwideaddress = [select id from orgwideEmailAddress where displayname = :System.Label.Qlik_noreply_email_address]; 
        mail.setOrgWideEmailAddressId(orgwideaddress[0].id); 

        mail.setSubject('New Lead for ' + CampaignName); 
        mail.setUseSignature(false);            
        if(!NotInSFCheckBox)
        {
            messageBody = String.Format('{0} <a href={1}/{2}> {3}</a> <br>', new String[] {System.Label.Create_Campaign_Member, URL.getSalesforceBaseUrl().toExternalForm(),cMemberId,System.Label.Click_here});
        }
        else
        {
            messageBody = System.Label.Lead_Not_Matched + '\n';
        }
        messageBody += CampaignDetails.replaceAll('\n', '<br>');
        
        System.Debug('ContactPublisherControllerExtension: SendEmail: MessageBody' + messageBody);
        mail.setHtmlBody(messageBody);
        
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
        catch (system.EmailException ex) {
            // If the sending of the email fails, due to non existing
            // email address, do nothing
        }
    }
    /*******************************************************
    *  Notify the lead generation by chatter
    *   
    *
    *******************************************************/  
    private void CreateChatter()
    {
        FeedItem post = new FeedItem();
        post.ParentId = ContactObj.Id;
        String chatterBody = '';
        System.Debug('ContactPublisherControllerExtension: CreateChatter: ContactObj.Id: ' + ContactObj.Id);
        if(!NotInSFCheckBox)
        {
            chatterBody = System.Label.Lead_Submitted;
        }
        else
        {
            chatterBody= System.Label.Lead_Not_Matched;
        }
        post.Body = chatterBody +  '\n' + CampaignDetails;
        insert post;
    }
    /*******************************************************
    *  Lead generation details
    *   
    *
    *******************************************************/
    private String CampaignDetails
    {
        get
        {
            if (NotInSFCheckBox) {
                return String.Format('Contact: {0}\nCompany (Account): {1}\nPhone: {2}\nEmail: {3}\nType of Lead: {4}\nComment: {5}',
                                                  new String[] {ContactName, AccountNameNotInSF, PhoneNumberNotInSF, EmailNotInSF, TypeOfLead, Comments});
            } else {
                return String.Format('Contact: {0}\nCompany (Account): {1}\nPhone: {2}\nEmail: {3}\nType of Lead: {4}\nComment: {5}',
                                                  new String[] {contactDetails.Name, AccountName, PhoneNumber, Email, TypeOfLead, Comments});
            }
        }
    }
    
    public String getMessage()
    {
            return System.Label.Lead_Warning_Message;
            
    }
    public Boolean IsPartner 
    {
        get
        {               
            return (String.isNotBlank(ContactObj.Account_Type__c) && ContactObj.Account_Type__c.contains('Partner')) || 
                       String.isNotBlank(ContactObj.Account.Responsible_Partner__r.Name);
        }
        set;
    }
    public Contact ContactDetails
    {
        get
        {
            System.Debug('ContactPublisherControllerExtension: ContactDetails: ContactObj.DefaultContact__c: ' + ContactObj.DefaultContact__c);
            if(ContactObj.DefaultContact__c != null)
            {
                ContactDetails = [SELECT Id, Name, EMail, Phone, Account.Name, Account.Owner.Email FROM Contact where Id=:ContactObj.DefaultContact__c];
                EMail = ContactDetails.Email;
                PhoneNumber = ContactDetails.Phone;
                AccountName = ContactDetails.Account.Name;
                System.Debug('ContactPublisherControllerExtension: ContactDetails: ContactDetails = ' + ContactDetails);
                return ContactDetails;
            }
            return null;                  
        }
        set;
    }
    
    public Boolean EnableNextButton
    {
        get
        {               
            return (ConsentCheckBox &&
                ((String.isNotBlank(ContactName) && NotInSFCheckBox) || (String.isNotBlank(ContactObj.Id) && !NotInSFCheckBox)) && 
                ((String.isNotBlank(AccountNameNotInSF) && NotInSFCheckBox) || (String.isNotBlank(AccountName) && !NotInSFCheckBox)) &&
                ((String.isNotBlank(PhoneNumberNotInSF) && NotInSFCheckBox) || (String.isNotBlank(PhoneNumber) && !NotInSFCheckBox)) &&
                ((String.isNotBlank(EMailNotInSF) && NotInSFCheckBox) || (String.isNotBlank(Email) && !NotInSFCheckBox))
            );
        }                          
    }
    
    public Boolean RenderNext
    {
        get{ return (!IsPartner) && (!bNext) ;}
    }
    public String Description
    { 
      get; 
      set
      {
          Description = value; 
          if ( String.IsBlank(value))
          {
              bSubmit = true;
          }
          else
              bSubmit = false;
          
      }
    }
    
    
    private Contact ContactObj;
    public Id cMemberId;
    private String Comments;
    public String CampaignName;
    private ApexPages.StandardController  caseController;
    public String ContactName { get; set; }       
    public String AccountName { get; set; }
    public String AccountNameNotInSF { get; set; }
    public String PhoneNumber { get; set; }        
    public String PhoneNumberNotInSF { get; set; }        
    public String EMail { get; set; }
    public String EMailNotInSF { get; set; }
    public String TypeOfLead {get; set; }
    public Boolean NotInSFCheckBox {get; set;}
    public Boolean ConsentCheckBox {get; set;}
    public Boolean bNext{get; set;}
    public Boolean bSubmit{get; set;}
}