/*
//  CR# 9885
//  test class to test OpportunityPDFCreator, OpportunityPDFTemplateCtrl and visualforce page: Opportunity_PDF_Template
//  Change log:
//  October 14, 2013 - Initial Implementation - Madhav Kakani - Fluido Oy
    20140624    Madhav Kakani modify test class to create mock data 
//  07.02.2017   RVA :   changing methods
//16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors	
//24.03.2017 Rodion Vakulovskyi    
*/
@isTest
private class OpportunityPDFCreatorTest {  
    static testMethod void testOpportunityPDFCreator() {
          Profile p = [select id from profile where name='System Administrator']; 
          User u = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');


         System.runAs(u) {
		 /*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c qtc = new QlikTech_Company__c();
        qtc.name = 'SWE';
        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
        qtc.Country_Name__c = 'Sweden';
        qtc.CurrencyIsoCode = 'SEK';
		qtc.Subsidiary__c = testSubs1.id;
        insert qtc;
		*/
		/*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
                 Name = 'USA',
                 Country_Name__c = 'USA',
                 QlikTech_Company_Name__c = 'QlikTech Inc',      
				 Subsidiary__c = testSubs1.id 
        );
        Insert QTComp;
		*/
		QlikTech_Company__c qtc = QTTestUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'Sweden');
			qtc.Country_Name__c = 'Sweden';
			qtc.CurrencyIsoCode = 'SEK';
		update qtc;
			
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc', 'USA', 'United States');
			   QTComp.Country_Name__c = 'USA';
			   QTComp.CurrencyIsoCode = 'USD'; 
		update QTComp;

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');  
            
        
            Account act = new Account(name='Test Account');
            act.OwnerId = u.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';           
            act.Account_Creation_Reason__c = 'Test';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
            
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
                
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            system.assert(PB != null);
            
            //Create test Opportunity
            Opportunity opp = New Opportunity (
                Short_Description__c = 'TestOpp',
                Name = 'TestOpp',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = rt.Id,
                CurrencyIsoCode = 'GBP',
                AccountId = ct.AccountId,
                Signature_Type__c = 'Digital Signature',
                ForecastCategoryName = 'Omitted'
            );
            Test.startTest();
            insert opp;
            Test.stopTest();
            system.assert(opp.Id != null);
            
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = opp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;         
            insert LineItem;    
            system.assert(LineItem.Id != null);
            
            Non_Licence_Related_ProductsCO_Non_Li__c nlp = new Non_Licence_Related_ProductsCO_Non_Li__c();
            nlp.Opportunity__c = opp.Id;
            nlp.Description__c = 'test';
            nlp.Product_Family__c = 'Training';
            nlp.Application__c = 'Test';
            nlp.Qty__c = 1;
            nlp.List_Price__c = 10;
            nlp.Discount_perc__c = 0;
            insert nlp;
            system.assert(nlp.Id != null);
            
            String sResp = OpportunityPDFCreator.CreatePDF(opp.Id, opp.Name);
            system.assert(sResp == 'Success');

            // Test the OpportunityPDFTemplateCtrl
            PageReference pdf = Page.Opportunity_PDF_Template;        
            pdf.getParameters().put('id', opp.id);
            Test.setCurrentPageReference(pdf);   

            ApexPages.StandardController sc = new ApexPages.standardController(opp);
            OpportunityPDFTemplateCtrl pdf_x = new OpportunityPDFTemplateCtrl(sc);
            system.assert(pdf_x.showItems1 == true);
            system.assert(pdf_x.showItems2 == true);
                             
               
        }
    }
     // testOpportunityPDFCreator

     static testMethod void testCreateOldOppToPDF() {
          Profile p = [select id from profile where name='System Administrator']; 
          User u = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');


         System.runAs(u) {
		 /*
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c qtc = new QlikTech_Company__c();
        qtc.name = 'SWE';
        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
        qtc.Country_Name__c = 'Sweden';
        qtc.CurrencyIsoCode = 'SEK';
        qtc.Subsidiary__c = testSubs1.id;
        insert qtc;

        QlikTech_Company__c QTComp = new QlikTech_Company__c(
                 Name = 'USA',
                 Country_Name__c = 'USA',
                 QlikTech_Company_Name__c = 'QlikTech Inc',      
                 Subsidiary__c = testSubs1.id 
        );
        Insert QTComp;
		*/
		QlikTech_Company__c qtc = QTTestUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'Sweden');
			qtc.Country_Name__c = 'Sweden';
			qtc.CurrencyIsoCode = 'SEK';
		update qtc;
			
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc', 'USA', 'United States');
			   QTComp.Country_Name__c = 'USA';
			   QTComp.CurrencyIsoCode = 'USD'; 
		update QTComp;
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');  
            
        
            Account act = new Account(name='Test Account');
            act.OwnerId = u.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';           
            act.Account_Creation_Reason__c = 'Test';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
            
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
            
            Id standardPriceBookId = Test.getStandardPricebookId();

            Pricebook2 tpb = new Pricebook2();
            tpb.Name = 'Test Pricebook';
            insert tpb;

            // create the product
            Product2 p1 = new Product2(
                name='Test Product 1',
                IsActive=true,
                Description='My Product',
                CurrencyIsoCode = 'GBP',
                ProductCode='12345',
                Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
                Income_Account__c = '26000 Deferred Revenue'
            );
            insert p1;    

            // create the pricebookentry for standard price book;
            PricebookEntry pbe = new PricebookEntry(
                Pricebook2Id= standardPriceBookId,
                Product2Id= p1.id,
                UnitPrice= 850.00,
                IsActive=true,
                CurrencyIsoCode = 'GBP'
            ); 
            insert pbe;
            System.assertNotEquals(null, pbe.Id);

            // create the pricebookentry for custom price book;
            PricebookEntry pbe1 = new PricebookEntry(
                Pricebook2Id= tpb.Id,
                Product2Id= p1.id,
                UnitPrice= 900.00,
                IsActive=true,
                UseStandardPrice=false,
                CurrencyIsoCode = 'GBP'
            ); 
            insert pbe1;
            System.assertNotEquals(null, pbe1.Id);

            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            system.assert(PB != null);
            
            //Create test Opportunity
            Opportunity opp = New Opportunity (
                Short_Description__c = 'TestOpp',
                Name = 'TestOpp',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = rt.Id,
                CurrencyIsoCode = 'GBP',
                AccountId = ct.AccountId,
                Signature_Type__c = 'Digital Signature',
                ForecastCategoryName = 'Omitted'
            );
            Test.startTest();
            insert opp;
            
            opp = [Select Id, Pricebook2Id From Opportunity Where Id = :opp.Id];
            opp.Pricebook2Id = tpb.Id;
            update opp;

            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = opp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = pbe1.Id;         
            insert LineItem;    
                 
                      
            String sResp = OpportunityPDFCreator.CreateOldOpportunityToPDF(opp.Id, 'test', 'Goal Identified');
           // system.assert(sResp == 'Success');

            // Test the OpportunityPDFTemplateCtrl
            PageReference pdf = Page.Opportunity_PDF_Template_Q2CW;        
            pdf.getParameters().put('id', opp.id);
            Test.setCurrentPageReference(pdf);   

            ApexPages.StandardController sc = new ApexPages.standardController(opp);
            OpportunityPDFTemplateQ2CWCtrl pdf_x = new OpportunityPDFTemplateQ2CWCtrl(sc);
            system.assert(pdf_x.showItems1 == true);

            OpportunityPDFCreator.CreateSoEPDF(opp.Id, 'test', 'Goal Identified');
            OpportunityPDFCreator.CreateAuditPDF(opp.Id, 'test', 'Goal Identified');
            OpportunityPDFCreator.CreateSoIPDF(opp.Id, 'test', 'Goal Identified');
            Test.stopTest();              
               
        }
    }
     // testOpportunityPDFCreator
    
} // class OpportunityPDFCreatorTest