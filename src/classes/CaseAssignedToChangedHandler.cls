/**************************************************
*
* File Name: CaseAssignedToChangedHandler
*
* 2018-04-10 ext_bad     CHG0032643      Add email to previous AssignedTo user.
* 2018-04-18 ext_bad     CHG0033720      Change org wide email, select it from custom settings.
* 2018-04-20 ext_bad     CHG0033720      Change email 'from' to current user.
* 2018-05-22 ext_bad     CHG0034008      Email notification to previously Assigned to User even if the new value = Blank.
*
*/
public with sharing class CaseAssignedToChangedHandler {


    public static void emailPreviousAssignedUser(Map<Id, Case> newCases, Map<Id, Case> oldCases) {
        List<String> userIds = new List<String>();
        List<String> caseIds = new List<String>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

        for (String caseId : oldCases.keySet()) {
            if (newCases.get(caseId).Assigned_To__c != oldCases.get(caseId).Assigned_To__c) {
                userIds.add(oldCases.get(caseId).Assigned_To__c);
                userIds.add(newCases.get(caseId).Assigned_To__c);
                caseIds.add(caseId);
            }
        }

        if (userIds.size() > 0) {
            Map<String, User> users = new Map<String, User>([
                    SELECT Id, Email, FirstName, LastName
                    FROM User
                    WHERE Id IN :userIds
            ]);
            EmailTemplate et = [
                    SELECT id, Body
                    FROM EmailTemplate
                    WHERE DeveloperName = :'Case_Assigned_To_Ownership_Change_Notification'
            ];

            for (String caseId : caseIds) {
                Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();

                User oldUser = users.get(oldCases.get(caseId).Assigned_To__c);
                User newUser = users.get(newCases.get(caseId).Assigned_To__c);
                if (oldUser != null) {

                    String plainText = et.Body.replace('{!Case.CaseNumber}', newCases.get(caseId).CaseNumber);
                    String newUserName = newUser != null ? (newUser.FirstName + ' ' + newUser.LastName) : ' ';
                    plainText = plainText.replace('{!Case.Assigned_To__c}', newUserName);
                    plainText = plainText.replace('{!Case.Subject}', newCases.get(caseId).Subject);
                    plainText = plainText.replace('{!Case.Link}', URL.getSalesforceBaseUrl().toExternalForm() + '/' + caseId);
                    emailMessage.setPlainTextBody(plainText);
                    emailMessage.setSubject('Assigned-To User Has Been Changed');
                    emailMessage.setToAddresses(new String[]{
                            oldUser.Email
                    });
                    emailMessage.setTemplateId(et.ID);
                    emailMessage.setSaveAsActivity(false);
                    messages.add(emailMessage);
                }
            }

            if (!messages.isEmpty()) {
                try {
                    List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(messages);
                    for (Messaging.SendEmailResult res : sendres) {
                        system.debug(LoggingLevel.DEBUG, '[emailPreviousAssignedUser] Result: ' + res);
                    }
                } catch (System.EmailException ex) {
                    system.debug(LoggingLevel.DEBUG, '[emailPreviousAssignedUser] Error when sending mail: ' + ex.getMessage());
                }
            }
        }

    }
}