/************************************************************************************************************
        CaseAttachmentPurge
        Description: This is the schedulable class to call the 
                     class where purge case attachments that are closed and older than 90 days

*************************************************************************************************************/
global class CaseAttachmentPurge_RunBatch implements Schedulable{

    global void execute(SchedulableContext sc){
        Database.executeBatch(new CaseAttachmentPurge());
    }

    // to run job daily
    global void execute(){
        // Tests for scheduled jobs can fail if the same job is already executing in Salesforce with the same name
        String jobName = Test.isRunningTest() ? 'CaseAttachmentPurgeTest' : 'CaseAttachmentPurge';
        system.schedule(jobName, '0 0 0 * * ?', new CaseAttachmentPurge_RunBatch());
    }
}