/****************************************************************
*
*  ContactAssignAttPermissionHandler
*  29.06.2020 : UIN : DGM-380: THis class is used to assign permissions to user to enable them to login in to attunitysupportportal.This is done
when the value DI Support Portal is assigned to them in SFDCAccessGrantedPicklist
The permission is removed if picklist value is remvoed.
*****************************************************************/
public class ContactAssignAttPermissionHandler{
    private static final String SettingName = 'SteelbrickSettingsDetails';
    public class myException extends Exception {}
    public ContactAssignAttPermissionHandler() {
        
    }

    public static void handleAfterUpdate(List<Contact> triggerNew, List<Contact> triggerOld) {
        List<Contact> cts = new List<Contact>();
        List<Id> contactIds= new List<Id>();
        List<Id> delcontactIds= new List<Id>();
        for (Integer i = 0; i < triggerNew.size(); i++) {
            Contact newContact  = triggerNew[i];
            Contact oldContact = triggerOld[i];
            if(String.isNotBlank(newContact.SFDCAccessGranted__c) && (newContact.SFDCAccessGranted__c != oldContact.SFDCAccessGranted__c)
                 && newContact.SFDCAccessGranted__c.contains('DI Support Portal')){
                cts.add(newContact);
                contactIds.add(newContact.Id);
            }
        }
        
        for (Integer i = 0; i < triggerNew.size(); i++) {
            Contact newContact  = triggerNew[i];
            Contact oldContact = triggerOld[i];
            if((newContact.SFDCAccessGranted__c != oldContact.SFDCAccessGranted__c)
                 && !newContact.SFDCAccessGranted__c.contains('DI Support Portal') && oldContact.SFDCAccessGranted__c.contains('DI Support Portal')){
                cts.add(newContact);
                delcontactIds.add(newContact.Id);
            }
        }
        
        system.debug('XVXVXVXV'+ contactIds);
        if(!delcontactIds.isEmpty()){
            removePermissions(delcontactIds);
        }
        
        if(!contactIds.isEmpty()){
            assignPermissions(contactIds);
        }
    }
    
    //Method to assign the permission set to the users
    @future
    static void assignPermissions(List<Id> contactIds){
            Map<String, User> mCtIdUsr = new Map<String, User>();
            Map<String, ULC_Details__c> mCtIdULCDetails = new Map<String, ULC_Details__c>();
            PermissionSet ps = [select id,name from permissionset where name = 'Access_to_Attunity_Customer_Portal' limit 1];
            for(ULC_Details__c u: [select id, ContactId__c, ULCStatus__c, ULCName__c,ULC_email__c from ULC_Details__c where ContactId__c in :contactIds and ULCStatus__c ='Active']){
                if(!mCtIdULCDetails.containsKey(u.ContactId__c))
                    mCtIdULCDetails.put(u.ContactId__c, u) ;        
            }
            system.debug('XVXVXVXV4'+ mCtIdULCDetails);
            List<String> userids = new List<String>();
            for(User u: [select id, FederationIdentifier, Contactid,email from User where Contactid in :contactIds and IsActive = true]){
                if(!mCtIdUsr.containsKey(u.Contactid)){
                    mCtIdUsr.put(u.Contactid , u)  ;  
                    userids.add(u.Id); 
                }    
            }
            Map<String, String> mAssigneeIdPSId = new Map<String, String>();
            for(PermissionSetAssignment psa: [select id, AssigneeId, PermissionSetId from PermissionSetAssignment where AssigneeId in :userids and PermissionSetId =:ps.Id]){
                if(!mAssigneeIdPSId.containsKey(psa.AssigneeId))
                    mAssigneeIdPSId.put(psa.AssigneeId , psa.Id)  ;   
                    
            }
            
            system.debug('XVXVXVXV3'+ mCtIdUsr);
            List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();
            
            
            for(String cid: contactIds){
                system.debug('XVXVXVXV1'+ cid);
                if(mCtIdULCDetails.containsKey(cid) && mCtIdUsr.containsKey(cid) && (mAssigneeIdPSId.isEmpty() || !(mAssigneeIdPSId.keySet().contains(mCtIdUsr.get(cid).Id)))){
                    if((mCtIdULCDetails.get(cid).ULCName__c.equalsIgnoreCase(mCtIdUsr.get(cid).FederationIdentifier)) && (mCtIdULCDetails.get(cid).ULC_email__c.equalsIgnoreCase(mCtIdUsr.get(cid).email))){
                        system.debug('XVXVXVXV2'+ cid);
                        if(String.isNotBlank(ps.Id)){
                            PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = mCtIdUsr.get(cid).Id);
                            permissionSetList.add(psa);
                        }
                    }
                }
            
            }
            
            if(!permissionSetList.isEmpty())
                upsert permissionSetList;
    }
    
    //Method to delete the permission set assignments
    @future
    static void removePermissions(List<Id> contactIds){
        Map<String, User> mUserIdUsr = new Map<String, User>();
        List<PermissionSetAssignment> permissionSetAssignList = new List<PermissionSetAssignment>();
        PermissionSet ps = [select id,name from permissionset where name = 'Access_to_Attunity_Customer_Portal' limit 1];
        system.debug('XVXVXVXV99'+ contactIds);
        for(User u: [select id, FederationIdentifier, Contactid,email from User where Contactid in :contactIds and IsActive = true]){
                if(!mUserIdUsr.containsKey(u.Id))
                    mUserIdUsr.put(u.Id, u)  ;       
        }
        system.debug('XVXVXVXV98'+ mUserIdUsr);
        if(!mUserIdUsr.isEmpty()){
            permissionSetAssignList = [select id from PermissionSetAssignment where AssigneeId in :mUserIdUsr.keyset() and PermissionSetId =:ps.Id];
            
            system.debug('XVXVXVXV97'+ permissionSetAssignList );
            if(!permissionSetAssignList .isEmpty())
                delete permissionSetAssignList ;
        }
    }
}