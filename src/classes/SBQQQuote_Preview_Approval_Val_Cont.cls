/*************************************************
*
* 2017-06-21 AIN Controller for SBQQQuote_Preview_Approval_Validation_Controller.page
*
*************************************************/
public without sharing class SBQQQuote_Preview_Approval_Val_Cont {

    public List<ApproverStatus> approverStatuses {get;set;}
    public boolean ValidationSucceeded {get;set;}
    public id QuoteId {get;set;}
    public SBQQ__Quote__c Quote {get;set;}

    public SBQQQuote_Preview_Approval_Val_Cont() {
        approverStatuses = new List<ApproverStatus>();
        ValidationSucceeded = true;

        QuoteId = (ApexPages.CurrentPage().getparameters().get('id') != null && ApexPages.CurrentPage().getparameters().get('id') != '' ) ? (Id)ApexPages.CurrentPage().getparameters().get('id') : null;
        if(QuoteId == null)
            return;

        List<SBQQ__Quote__c> quotes = [select id, 
            Name,
            Sales_Approver_1__c, Sales_Approver_1_NS_Id__c,
            Sales_Approver_2__c,
            Sales_Approver_3__c,
            Sales_Approver_4__c,
            Sales_Approver_5__c,
            Consulting_Approver_1__c,
            Consulting_Approver_2__c,
            Consulting_Approver_3__c,
            Education_Approver_1__c,
            Education_Approver_2__c,
            Education_Approver_3__c
            from SBQQ__Quote__c where id = :quoteId];
        if(quotes.size() == 0)
            return;
        Quote = quotes[0];

        List<Id> userIds = new List<Id>();
        List<String> userIdsString = new List<String>();
        userIdsString.add(Quote.Sales_Approver_1__c);
        userIdsString.add(Quote.Sales_Approver_2__c);
        userIdsString.add(Quote.Sales_Approver_3__c);
        userIdsString.add(Quote.Sales_Approver_4__c);
        userIdsString.add(Quote.Sales_Approver_5__c);
        userIdsString.add(Quote.Consulting_Approver_1__c);
        userIdsString.add(Quote.Consulting_Approver_2__c);
        userIdsString.add(Quote.Consulting_Approver_3__c);
        userIdsString.add(Quote.Education_Approver_1__c);
        userIdsString.add(Quote.Education_Approver_2__c);
        userIdsString.add(Quote.Education_Approver_3__c);


        for(String uId : userIdsString) {
            if(uId != null && uId != '' && (uId.Length() == 15 || uId.Length() == 18))
                userIds.add(uId);
        }

        Map<Id, User> users;
        if(userIds.Size() > 0)
            users = new Map<Id, User>([select id, Name, IsActive, Approval_Manager__c, Quote_Approval_Role__c from User where id in :userIds]);
        else
            users = new Map<Id, User>();

        boolean approvalHasHitCEO = false;
        integer j = 0;
        for(integer i = 0; i < userIdsString.Size(); i++){
            string approverType = (i<=4?'Sales':i<=7?'Consulting':'Education');
            if(i == 5 || i == 8){
                approvalHasHitCEO = false;
                j=0;
            }

            if(userIdsString[i] != null && userIdsString[i] != '' && (userIdsString[i].length()==15 || userIdsString[i].length()==18) && users.containsKey(userIdsString[i])){
                User sas = users.get(userIdsString[i]);
                approverStatuses.add(new ApproverStatus(approverType, j+1, approvalHasHitCEO, sas.Id, sas.IsActive, sas.Name, sas.Quote_Approval_Role__c, sas.Approval_Manager__c));    
                if(sas.Quote_Approval_Role__c != null && sas.Quote_Approval_Role__c == 'CEO')
                    approvalHasHitCEO = true;
            }
            else {
                approverStatuses.add(new ApproverStatus(approverType, j+1, approvalHasHitCEO, null, null, null, null, null));
            }
            j++;
        }

        for(ApproverStatus sas : ApproverStatuses){
            if(sas.status != ''){
                ValidationSucceeded = false;
                break;
            }
        }
        
    }
    public PageReference redirectToApproval() {
        if(!ValidationSucceeded)
            return null;
        PageReference pageRef = Page.SBAA__PreviewApprovals;
        pageref.getParameters().put('id', QuoteId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public PageReference redirectToQuote() {
        
        PageReference pageRef = Page.SBAA__PreviewApprovals;
        pageref.getParameters().put('id', QuoteId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public class ApproverStatus {
        public string ApproverType {get;set;}
        public Integer ApproverNumber {get;set;}
        public Boolean PreviousApprovalHasHitCEO {get;set;}
        public Id ApproverId {get;set;}
        public boolean ApproverIsActive {get;set;}
        public String ApproverName {get;set;}
        public String QuoteApprovalRole {get;set;}
        public Id ApprovalManager {get;set;}
        public String Status {
            get{
                //Please note that if these error messages are changed the test class needs to be changed as well.
                if(PreviousApprovalHasHitCEO)
                    return '';
                else if(ApproverId == null){
                    return GetStringFromLabel(Label.SBQQQuoteApprovalValidationPage_Field_Not_Populated_Exception);
                    //return ApproverType + ' Approver ' + ApproverNumber + ' field is not populated!';
                }
                else if(!ApproverIsActive){
                    return GetStringFromLabel(Label.SBQQQuoteApprovalValidationPage_User_Not_Active_Exception);
                    //return ApproverType + ' Approver ' + ApproverNumber + ' field is not populated!';
                }
                else if(QuoteApprovalRole == null || QuoteApprovalRole == ''){
                    return GetStringFromLabel(Label.SBQQQuoteApprovalValidationPage_Quote_Approver_Not_Populated_Exception);
                    //return ApproverType + ' Approver ' + ApproverNumber + ': ' +ApproverName + ' (ID: '+ApproverId+') Quote Approval Role is not populated!';
                }
                else if(ApprovalManager == null && QuoteApprovalRole != 'CEO'){
                    return GetStringFromLabel(Label.SBQQQuoteApprovalValidationPage_Approval_Manager_Not_Populated_Exception);
                    //return ApproverType + ' Approver ' + ApproverNumber + ': ' +ApproverName + ' (ID: '+ApproverId+') Approval Manager is not populated!';
                }
                return '';
            }
        }
        public ApproverStatus(string ApproverType, Integer ApproverNumber, boolean PreviousApprovalHasHitCEO, Id ApproverId, boolean ApproverIsActive, String ApproverName, 
            String QuoteApprovalRole, Id ApprovalManager) {
            this.ApproverType = ApproverType;
            this.ApproverNumber = ApproverNumber;
            this.PreviousApprovalHasHitCEO = PreviousApprovalHasHitCEO;
            this.ApproverId = ApproverId;
            this.ApproverIsActive = ApproverIsActive;
            this.ApproverName = ApproverName;
            this.QuoteApprovalRole = QuoteApprovalRole;
            this.ApprovalManager = ApprovalManager;
        }
        public string GetStringFromLabel(string label)
        {
            string message;
            message = label.Replace('{ApproverType}', ApproverType);
            message = message.Replace('{ApproverNumber}', String.valueOf(ApproverNumber));
            message = message.Replace('{PreviousApprovalHasHitCEO}', String.valueOf(PreviousApprovalHasHitCEO));
            if(ApproverId != null)
                message = message.Replace('{ApproverId}', ApproverId);
            if(ApproverName != null)
                message = message.Replace('{ApproverName}', ApproverName);
            if(QuoteApprovalRole != null)
                message = message.Replace('{QuoteApprovalRole}', QuoteApprovalRole);
            if(ApprovalManager != null)
                message = message.Replace('{ApprovalManager}', ApprovalManager);
            return message;

        }

    }

}