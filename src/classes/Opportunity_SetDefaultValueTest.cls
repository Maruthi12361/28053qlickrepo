@isTest
private class Opportunity_SetDefaultValueTest {
/******************************************************

        Opportunity_SetDefaultValueTest

        This method makes unit test of the Opportunity_SetDefaultValue
        trigger.

        Changelog:
            2011-10-26  MHG     Created method
            2012-02-14  CCE     Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
            2014-09-08  TJG     Moved these 3 test methods from Test_Triggers_toAdd.cls here.
            2015-04-20  SLH     Added function and solution area to handle OP006 validation rule
            2016-06-17  roman@4front  test_Opportunity_SetDefaultValue_DefaultRecordType method created
			06.02.2017   RVA :   changing QT methods
			2017-05-27 MTM Set QCW-1633 OEM subscription
			2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring
			2017-09-03 Srinivasan PR - fix for test class error QCW 2934
            09-10-2017	Oleksandr@4Front commented out TestUpdateOEMSubscriptionOnQuote;QCW-4073
			2020-07-09 Leonardo Grauer, Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
				"Deployment Preference" is required in Goal Confirmed stage 
    ******************************************************/
    static testMethod void test_Opportunity_SetDefaultValue()
    {
        System.debug('test_Opportunity_SetDefaultValue: Starting');
        /*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc'
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account Parent = new Account(Name = 'TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert Parent;

        test.startTest();

        QTCustomSettings__c MySetting = new QTCustomSettings__c(
            Name = 'Default',
            OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW',
            Opp_Default_License_Amount_For_Direct__c = 25000.0
        );
        Insert MySetting;
        QuoteTestHelper.createCustomSettings();
        decimal Val  = MySetting.Opp_Default_License_Amount_For_Direct__c;

        Opportunity Opp = new Opportunity();
        Opp.AccountId = Parent.Id;
        Opp.Short_Description__c = 'My Test Description';
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'USD';
        Opp.CloseDate = Date.today().addDays(30);
		Opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        Opp.StageName = 'Goal Confirmed';
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        Opp.Revenue_Type__c = 'Direct';
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Signature_Type__c = 'Digital Signature';
        Opp.Included_Products__c = 'Qlik Sense';
        insert Opp;

        Account TestAccount = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Sell Qlik Test Ltd.',
                Navision_Status__c = 'Partner',
                QlikTech_Company__c = 'QlikTech Inc'
            );
        insert TestAccount;

        Contact TestContact = new Contact (
                AccountId = TestAccount.Id,
                OwnerId = UserInfo.getUserId(),
                LastName = 'Wilson-Hemingway',
                FirstName = 'Daryl',
                Email = 'dwy@qlikview.com'
            );
        insert TestContact;

        Contact cont = [select Id, Name from Contact LIMIT 1];
        Opp = [select Id, CurrencyIsoCode, Amount from Opportunity where Id = :Opp.Id];
        OpportunitySetDefaultValueHandler tempObj = new OpportunitySetDefaultValueHandler();
        /*Sphere_of_Influence__c sph = new Sphere_of_Influence__c(
            Opportunity__c = Opp.Id,
            Contact__c = cont.Id,
            Role__c = 'Decision Maker;User');
        insert sph;*/

        System.assertEquals(0.00, Opp.Amount);

        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'SEK';
        update Opp;

        Opp = [select Id, CurrencyIsoCode, Amount from Opportunity where Id = :Opp.Id];
      //  System.assert(Opp.Amount > Val);

        test.stopTest();
        System.debug('test_Opportunity_SetDefaultValue: Finishing');

    }

    /******************************************************

        test_Opportunity_SetDefaultValue_RS

        This method makes unit test of the Opportunity_SetDefaultValue
        trigger for Reseller Sense revenue type.

        Changelog:
            2014-09-01  TJG     Added Direct Sense and Reseller Sense revenue type tests for CR 16062
                                https://eu1.salesforce.com/a0CD000000kLgD9
    ******************************************************/
    static testMethod void test_Opportunity_SetDefaultValue_RS()
    {
        System.debug('test_Opportunity_SetDefaultValue_RS: Starting');
        /*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc'
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account Parent = new Account(Name = 'TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert Parent;

        test.startTest();

        QTCustomSettings__c MySetting = new QTCustomSettings__c(
            Name = 'Default',
            OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW',
            Opp_Default_License_Amount_For_Reseller__c = 8000.0
        );
        Insert MySetting;

        // 2014-09-01 CR 16062
        QuoteTestHelper.createCustomSettings();
        decimal ValRe = MySetting.Opp_Default_License_Amount_For_Reseller__c;
        Account TestAccount = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Sell Qlik Test Ltd.',
                Navision_Status__c = 'Partner',
                QlikTech_Company__c = 'QlikTech Inc'
            );
        insert TestAccount;

        Contact TestContact = new Contact (
                AccountId = TestAccount.Id,
                OwnerId = UserInfo.getUserId(),
                LastName = 'Wilson-Hemingway',
                FirstName = 'Daryl',
                Email = 'dwy@qlikview.com'
            );
        insert TestContact;

        Opportunity Opp2 = new Opportunity(
            AccountId = Parent.Id,
            Short_Description__c = 'Reseller Sense Test',
            Name = 'Reseller Sense Test1',
            Amount = 0,
            CurrencyIsoCode = 'USD',
            CloseDate = Date.today().addDays(30),
			Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            StageName = 'Goal Confirmed',
            ForecastCategoryName = 'Omitted',
            Partner_Contact__c = TestContact.Id,
            Revenue_Type__c = 'Reseller Sense',
            Function__c = 'Other',
            Signature_Type__c = 'Digital Signature',
            Solution_Area__c = 'Other',
            Included_Products__c = 'Qlik Sense'
        );

        insert Opp2;
        Opp2 = [select Id, Amount from Opportunity where Id = :Opp2.Id];
        System.assertEquals(0.00, Opp2.Amount);

        test.stopTest();
        System.debug('test_Opportunity_SetDefaultValue_RS: Finishing');

    }

    /******************************************************

        test_Opportunity_SetDefaultValue_DS

        This method makes unit test of the Opportunity_SetDefaultValue
        trigger for Direct Sense revenue type.

        Changelog:
            2014-09-01  TJG     Added Direct Sense and Reseller Sense revenue type tests for CR 16062
                                https://eu1.salesforce.com/a0CD000000kLgD9
    ******************************************************/
    static testMethod void test_Opportunity_SetDefaultValue_DS()
    {
        System.debug('test_Opportunity_SetDefaultValue_DS: Starting');
        /*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc'
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account Parent = new Account(Name = 'TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert Parent;

        test.startTest();

        QTCustomSettings__c MySetting = new QTCustomSettings__c(
            Name = 'Default',
            OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW',
            Opp_Default_License_Amount_For_Direct__c = 25000.0
        );
        Insert MySetting;
        decimal Val  = MySetting.Opp_Default_License_Amount_For_Direct__c;
        QuoteTestHelper.createCustomSettings();
        // 2014-09-01 CR 16062
        Opportunity Opp1 = new Opportunity(
            AccountId = Parent.Id,
            Short_Description__c = 'Direct Sense Test',
            Name = 'Direct Sense Test1',
            Amount = 0,
            CurrencyIsoCode = 'USD',
            CloseDate = Date.today().addDays(30),
			Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            StageName = 'Goal Confirmed',
            ForecastCategoryName = 'Omitted',
            Revenue_Type__c = 'Direct Sense',
            Function__c = 'Other',
            Signature_Type__c = 'Digital Signature',
            Solution_Area__c = 'Other',
            Included_Products__c = 'Qlik Sense'
        );

        insert Opp1;
        Opp1 = [select Id, Amount from Opportunity where Id = :Opp1.Id];
        System.assertEquals(0.00, Opp1.Amount);

        test.stopTest();
        System.debug('test_Opportunity_SetDefaultValue_DS: Finishing');

    }

    /******************************************************

        test_Opportunity_SetDefaultValue_DefaultRecordType

        This method makes unit test of the Opportunity_SetDefaultValue
        trigger for Default Record Type.

        Changelog:
            2016-06-17  roman@4front    test_Opportunity_SetDefaultValue_DefaultRecordType method created
    ******************************************************/
    static testMethod void test_Opportunity_SetDefaultValue_DefaultRecordType()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('test_Opportunity_SetDefaultValue_DefaultRecordType: Starting');

        //QlikTech_Company__c QTComp = new QlikTech_Company__c(
        //    Name = 'USA',
        //    QlikTech_Company_Name__c = 'QlikTech Inc'
        //);
        //insert QTComp;
        QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account Parent = new Account(Name = 'TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert Parent;

        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert settings;

        Id s = Test.getStandardPricebookId();
        System.assertNotEquals(null, s);
        // create the product
        Product2 p1 = new Product2(
            name='Test Product 1',
            IsActive=true,
            Description='My Product',
            CurrencyIsoCode = 'USD',
            ProductCode='12345',
            Deferred_Revenue_Account__c = '26000 Deferred Revenue',
            Income_Account__c = '26000 Deferred Revenue'
        );
        insert p1;

        // create the pricebookentry
        PricebookEntry pbe1 = new PricebookEntry(
            Pricebook2Id= s,
            Product2Id= p1.id,
            UnitPrice= 900.00,
            IsActive=true,
            UseStandardPrice=false,
            CurrencyIsoCode = 'USD'
        );
        insert pbe1;
        System.assertNotEquals(null, pbe1.Id);

        test.startTest();

        // 2014-09-01 CR 16062
        Opportunity Opp1 = new Opportunity(
            AccountId = Parent.Id,
            Short_Description__c = 'Direct Sense Test',
            Name = 'Direct Sense Test1',
            Amount = 0,
            CurrencyIsoCode = 'USD',
            CloseDate = Date.today().addDays(30),
			Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            StageName = 'Goal Confirmed',
            ForecastCategoryName = 'Omitted',
            Revenue_Type__c = 'Direct Sense',
            Function__c = 'Other',
            Signature_Type__c = 'Digital Signature',
            Solution_Area__c = 'Other',
            Included_Products__c = 'Qlik Sense'
        );
        Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
        //Semaphores.OpportunityMarketType_HasRun = true;
        insert Opp1;

        OpportunityLineItem oli = new OpportunityLineItem();
        oli.Quantity = 1;
        oli.UnitPrice = 1000;
        oli.PricebookEntryId = pbe1.id;
        oli.OpportunityId = Opp1.id;
        oli.Partner_Margin__c = 40;
        oli.Discounted_Price__c = 900;
        insert oli;
        System.assertNotEquals(null, oli.Id);

        delete oli;

        Opp1 = [select Id, Amount from Opportunity where Id = :Opp1.Id];
        //System.assertEquals(Val, Opp1.Amount);
    }

	// Test SBQQQuoteSetFields.PopulateProductFilterOnQuotes
    /* Oleksandr@4Front QCW-4073
	static testMethod void TestUpdateOEMSubscriptionOnQuote() {

    	Profile p = [select id from profile where name='System Administrator'];
    	User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com',
			emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          	localesidkey='en_US', profileid = p.Id,
          	timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.RunAs(mockSysAdmin)
        {
		    QTTestUtils.GlobalSetUp();
        	//QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');

            Account  testAccount = QTTestUtils.createMockAccount('TestCompany', mockSysAdmin, true);
	        testAccount.RecordtypeId = '012D0000000KBYxIAO';  //OEM End User Account
			testAccount.Name = 'Test Val Rule 1';
	        update testAccount;

            Contact ct = new Contact(AccountId=testAccount.Id,lastname='Contact1',firstname='Test', Email = 'junk@gmail.com');
            insert ct;
			Test.startTest();
            Opportunity origOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'OEM',
                CloseDate = Date.today(),
				Solution_Type__c = 'Hosted Subscription Items Only',
				Subscription_Start_Date__c = Date.today(),
				Subscription_Unit__c = 'Months',
				No_of_Subscription_Units__c = '3',
                StageName = 'Alignment Meeting',
                RecordTypeId = '01220000000DXq5AAG',   // OEM - Order
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = testAccount.Id,
				End_User_Account_del__c = testAccount.Id
            );
			insert origOpp;

			RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote'];
			insert QuoteTestHelper.createPCS(testAccount);//2934
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, ct.id, testAccount, '', 'OEM', 'Open', 'Quote', true, origOpp.Id);
            insert quoteForTest;
			//Opportunity newOpp = origOpp.Clone(true, true, false, true);
			origOpp.No_of_Subscription_Units__c = '10';
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            Semaphores.OpportunityTriggerBeforeUpdate2 = false;
			OpportunitySetDefaultValueHandler.IsBeforeUpdateProcessing = false;
			update origOpp;
			System.Debug('Units =' + origOpp.No_of_Subscription_Units__c);
			System.Debug('SBQQ__PrimaryQuote__c =' + origOpp.SBQQ__PrimaryQuote__c);
			System.Debug('Subscription_Start_Date__c =' + origOpp.Subscription_Start_Date__c);
			//map<Id, Opportunity> oldMap = new map<Id, Opportunity>();
			//oldMap.put(origOpp.Id, origOpp);
			//OpportunitySetDefaultValueHandler.UpdateOEMSubscriptionOnQuote(new List<Opportunity>{newOpp}, oldMap);
			SBQQ__Quote__c quoteForTest1 = [Select Id, SBQQ__SubscriptionTerm__c From SBQQ__Quote__c  where Id =: quoteForTest.Id];

            System.assertNotEquals(null, quoteForTest1.Id);
			System.assertEquals(10, quoteForTest1.SBQQ__SubscriptionTerm__c);

            Test.stopTest();
        }
	}
    */
}