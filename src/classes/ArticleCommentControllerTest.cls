/**************************************************
* Author: Alberto de Diego from Fluido Oy
*
* Change Log:
* 2013-01-15 Madhav Kakani: Replaced Article_Comment__c with Case object
* 2012-11-15 Alberto de Diego: CR# 4623
*        Initial Development
* 2015-10-26 RDZ: Winter 16 Issues- Adding the use of QTTestUtils.createMockKnowledgeArticle for error on test data.
*                 Incrementing from 23 to 31 api version.
* 2019-05-13 extbad Retrieve ArticleFeedback Record Type for Case object
**************************************************/
@isTest
private class ArticleCommentControllerTest {
    
    private static KnowledgeArticle article;
    private static Case comment;
    
    static testmethod void testArticleCommentController() {
        
        Setup();
        Test.startTest();                       
                
        ArticleCommentController controller = new ArticleCommentController(new ApexPages.StandardController(comment));
        PageReference page = controller.changeOwner();
        Case cmt = [SELECT Status FROM Case WHERE Id = :comment.Id];
                
        System.assertEquals('In Process', cmt.Status);
               
        Test.stopTest();
    }
    /*
    	Test Case closeAndNotify method without closing comments
    */
    static testmethod void testCaseCloseWithoutComments() {
    	
    	Setup();
    	Test.startTest();
    	        
        ArticleCommentController controller = new ArticleCommentController(new ApexPages.StandardController(comment));
        
   		controller.closeAndNotify();
   		
   		for(Apexpages.Message msg : Apexpages.getMessages()){
   			System.AssertEquals('Closing Comments cannot be empty.', msg.getSummary());
   		}
        
        Test.stopTest();
    }
    
    /*
    	Test case closeAndNotify method
    */
    static testmethod void testCloseAndNotify() {
    	
    	Setup();
    	Test.startTest();
    	
        comment.Closing_Comments__c = 'Test closing comment';
        update comment;
        
        ArticleCommentController controller = new ArticleCommentController(new ApexPages.StandardController(comment));
        
   		controller.closeAndNotify();
        Case cmt = [SELECT Status FROM Case WHERE Id = :comment.Id];
        
        System.assertEquals('Closed', cmt.Status);
        
        Test.stopTest();
    }
    /*
    Initialise Knowledge article, FeedItem and Case
    */
    private static void Setup()
    {
    	//This util class insert at least one knowledge article so we can query on them.
        QTTestUtils.createMockKnowledgeArticle();
        article = [SELECT Id FROM KnowledgeArticle Limit 1].get(0);
        System.assert(article.Id != null, 'Mock Article insert did not work.');
    	    	
        FeedItem feedItem = new FeedItem(ParentId = article.Id, Body = 'Test body'); 
        insert feedItem;

        //Winter 16 Issue - Adding a comment as with trigger dissable does not happen in an automated way.
        //If trigger is active do not create case. Otherwise for the test to work we need to create the test.
        //[Select ID, Name, Status from ApexTrigger Where Name='FeedItemArticleComment']
        ApexTrigger t = [Select ID, Name, Status from ApexTrigger Where Name='FeedItemArticleComment'];
        //No longer using the trigger FeedItemArticleComment as we can't use connectapi methods without seeAllData.

        String articleFeedbackId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Article Feedback')
                .getRecordTypeId();
        comment = new Case(Description=feedItem.Body,
                                    RecordTypeId=articleFeedbackId,
                                    Title__c='Title',
                                    Subject='Subject',
                                    Knowledge_article__c=article.Id,
                                    Origin='Internal',
                                    Feed_Item__c=feedItem.Id);
        insert comment;
        //comment = [SELECT Id, Feed_Item__c, CreatedById FROM Case WHERE Knowledge_article__c = :article.Id];
        /*System.assert(t.Status == 'Inactive', 'Due to issue with Support portal and Winter 16 we expect this trigger to be deactivated');
        if(t.Status == 'Inactive')
        {
            System.assert(comment.size()==0, '---Comment = 0 ->It was deactivated temporarily due to issues on Support Portal Project 20151026'); 
            //To avoid classes to fail. TODO when trigger is activated this code shoul not be run.  
            Case c = new Case(Knowledge_article__c=article.Id);
            insert c;             
            comment = [SELECT Id, Feed_Item__c, CreatedById FROM Case WHERE Knowledge_article__c = :article.Id];
        }
        else
        {
            System.assert(comment.size()>0 , '---Comments>0 ->If no comments then trigger is deactivated or not working as expected (FeedItemArticleComment).');
        }
        */
        //System.assert(comment.size()>0 , '---Comments>0 ->If no comments then trigger is deactivated or not working as expected (FeedItemArticleComment).');
        
    } 
}