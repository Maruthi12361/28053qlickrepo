/********************************************************
* CLASS: ScheduleWebActivityRetry
* DESCRIPTION: Schedulable class for WebActivityRetry batch
* CHANGELOG:    
*	2020-01-13 - CRW - Added Initial logic
*********************************************************/
global class ScheduleWebActivityRetry implements Schedulable{
    String executionType;
    
    global ScheduleWebActivityRetry(String Type){
        executionType = Type;
    }
    
    global void execute(SchedulableContext sc){
		Database.executeBatch(new WebActivityRetry(executionType), 100);
    }
}