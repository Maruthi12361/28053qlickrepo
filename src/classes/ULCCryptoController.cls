public with sharing class ULCCryptoController {

    public string ULCId { get; set; }

    public string getBaseURL() {
        QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');
        return (Settings != null ? Settings.ULC_Base_URL__c : '');
    }

    public string getGeneratedCrypto() {

        QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');
        if (Settings == null) {
            return '';
        }

        List<ULC_Details__c> uldetails = [SELECT LeadID__r.Email, ContactId__r.Account_Type__c, ContactId__c, LeadID__r.LastName, ContactId__r.AccountId, ContactId__r.Email, ULCName__c, ContactId__r.FirstName, ContactId__r.LastName, LeadID__r.FirstName, ContactId__r.Account.Adopted_Navision_Status__c, ContactId__r.Account.Parent.Adopted_Navision_Status__c, ContactId__r.Account.Parent.Navision_Status__c, ContactId__r.Account.Partner_Type__c, ContactId__r.Account.Parent.ParentId FROM ULC_Details__c WHERE ID = :ULCId];

        if (uldetails.size() == 0) {
            return '';
        }

        ULC_Details__c u = uldetails.get(0);
        string token = 'USERNAME=' + EncodingUtil.urlEncode(u.ULCName__c, 'UTF-8');
        token += '&FIRSTNAME=' + EncodingUtil.urlEncode((u.ContactId__c != null ? u.ContactId__r.FirstName : u.LeadId__r.FirstName), 'UTF-8');
        token += '&LASTNAME=' + EncodingUtil.urlEncode((u.ContactId__c != null ? u.ContactId__r.LastName : u.LeadId__r.LastName), 'UTF-8');
        token += '&EMAIL=' + EncodingUtil.urlEncode((u.ContactId__c != null ? u.ContactId__r.Email : u.LeadId__r.Email), 'UTF-8');
        if (u.ContactId__c != null) {
            token += '&CONTACTID=' + EncodingUtil.urlEncode(u.ContactId__c, 'UTF-8');
            token += '&ACCOUNTID=' + EncodingUtil.urlEncode(u.ContactId__r.AccountId, 'UTF-8');
        } else {
            token += '&CONTACTID=&ACCOUNTID=';
        }

        string ULCLevels = '';
        if (u.ContactId__c != null) {
            string NavisionStatus = '';
            string act = u.ContactId__r.Account_Type__c;
            if (act == 'Customer' || u.ContactId__r.Account.Adopted_Navision_Status__c == 'Customer') {
                NavisionStatus = 'Customer';
            } else if (act == 'Partner' || u.ContactId__r.Account.Adopted_Navision_Status__c == 'Partner') {
                NavisionStatus = 'Partner';
                if (u.ContactId__r.Account.Partner_Type__c == 'MasterReseller') {
                    ULCLevels += 'MasterReseller';
                }
            } else if (act == 'Pending Partner' || act == 'Pending Customer') {
                NavisionStatus = 'Download';
            } else if (act == 'Employee') {
                NavisionStatus = 'Employee';
            } else {
                if (u.ContactId__r.Account.ParentId != null) {
                    string AdditionalULCLevel = GetParentNavisionStatus(u.ContactId__r.Account.ParentId, 0);
                    if (AdditionalULCLevel != null) {
                        ULCLevels += AdditionalULCLevel;
                    }
                }
                if (HasActiveLicense(u.ContactId__r.AccountId)) {
                    if (ULCLevels != '') {
                        ULCLevels += ',';
                    }
                    ULCLevels += 'Download';
                }
            }

            if (NavisionStatus != '') {
                if (ULCLevels != '') {
                    ULCLevels += ',';
                }
                ULCLevels += NavisionStatus;
            }
            string additionalLevels = LoadAdditionalLevels(u.ContactId__c);
            if (additionalLevels != '') {
                if (ULCLevels != '') {
                    ULCLevels += ',';
                }
                ULCLevels += additionalLevels;
            }
        }

        if (ULCLevels != '') {
            ULCLevels += ',';
        }
        ULCLevels += 'Base';

        token += '&ULCLEVELS=' + EncodingUtil.urlEncode(ULCLevels, 'UTF-8');
        token += '&TIMELIMIT=' + DateTime.now().getTime();
        token += '&ORGID=' + UserInfo.getOrganizationId();
        token += '&SECRET=' + EncodingUtil.urlEncode((Settings.ULC_Crypto_Secret__c != null ? Settings.ULC_Crypto_Secret__c : 'secret'), 'UTF-8');   

        Blob key = Blob.valueOf((Settings.ULC_Crypto_Key__c != null ? Settings.ULC_Crypto_Key__c : '12345678901234567890123456789012'));
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256', key, Blob.valueOf(token));
        //Blob encryptedData = Crypto.encrypt('AES256', key, Blob.valueOf('60iP0h6vJoEaJs7N') , Blob.valueOf(token));
        return EncodingUtil.urlEncode(EncodingUtil.convertToHex(encryptedData), 'UTF-8');

    }

    @TestVisible
    private string GetParentNavisionStatus(string ParentId, Integer RecursionLevel) {
        List<Account> accounts = [SELECT Adopted_Navision_Status__c, Navision_Status__c, ParentId FROM Account WHERE Id = :ParentId];
        if (accounts.size() == 0) {
            return null;
        }
        Account a = accounts.get(0);
        if (a.Navision_Status__c == 'Customer' || a.Adopted_Navision_Status__c == 'Customer') {
            return 'Customer';
        } else if (a.Navision_Status__c == 'Partner' || a.Adopted_Navision_Status__c == 'Partner') {
            return 'Partner';
        } else if (a.ParentId != null) {
            if (RecursionLevel == 3) {
                return null;
            }
            return GetParentNavisionStatus(a.ParentId, RecursionLevel++);
        }
        return null;
    }

    @TestVisible
    private Boolean HasActiveLicense(string AccountId) {
        List<AggregateResult> res = [SELECT count(Time_Limit__c) FROM Account_License__c WHERE Time_Limit__c >= TODAY AND Account__c = :AccountId];
        if (res.size() == 0) {
            return false;
        }
        AggregateResult a = res.get(0);
        if (a.get('expr0') != 0) {
            return true;
        }
        return false;
    }

    @TestVisible
    private string LoadAdditionalLevels(string ContactId) {
        List<Assigned_ULC_Level__c> levels = [SELECT ULCLevelId__r.Name FROM Assigned_ULC_Level__c WHERE ContactId__c = :ContactId AND Status__c = 'Approved'];
        string ULCLevels = '';
        for (Assigned_ULC_Level__c a : levels) {
            if (ULCLevels != '') {
                ULCLevels += ',';
            }
            ULCLevels += a.ULCLevelId__r.Name;
        }
        return ULCLevels;
    }

}