/****************************************************************
*
*  QuoteLineTableController_TEST 
*
*  06.02.2017  RVA :   changing CreateAcounts methods
* 16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors
* 02.09.2017 : Srinivasan PR- fix for query error
* 18.04.2019 : Anjuna Baby - Adjusted to increase code coverage
*****************************************************************/
@isTest
private class QuoteLineTableController_TEST {
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    @isTest 
    public static void test_method_one() {
     QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        /*
            Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
        */
        Account  testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [Select id From Subsidiary__c];
         QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        /*
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount;
        */
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
                                where DeveloperName = 'Partner_Account' and sobjecttype='Account' ];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            productForTest.ProductCode = '8070';
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        Test.startTest();
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           //quoteForTest.SBQQ__Primary__c = true;
           insert quoteForTest;
            
        
            update quoteForTest;
         SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        insert testQuoteLine;
        
        //CAL for testQuoteLine (Server)
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c(
        SBQQ__Quantity__c = 2,
        License_Code__c = 'license-Code',
        SBQQ__Quote__c = quoteForTest.id,
        SBQQ__Product__c = productForTest.id,
        Product_Group_Text__c = 'group Text',
        Family__c = 'family',
        CurrencyIsoCode = 'USD',
        SBQQ__CustomerPrice__c = 6,
        SBQQ__DefaultSubscriptionTerm__c = 12,
        SBQQ__ListPrice__c = 1,
        SBQQ__StartDate__c = Date.today(),
        SBQQ__NetPrice__c = 1,
        SBQQ__Number__c = 4,
        SBQQ__PartnerPrice__c = 1,
        SBQQ__PricingMethod__c = 'List',
        SBQQ__SubscriptionPercent__c = 2,
        SBQQ__SubscriptionPricing__c = 'Percent Of Total',
        SBQQ__SubscriptionScope__c = 'Quote scope',
        Disable_AJAX__c = false,
        LicenseProductID__c = 'licenseId_new',
        LEFDetailID__c = 'lefDetail_1',
        SBQQ__RequiredBy__c = testQuoteLine.id
        );
        
        insert quoteLine ;
        Test.stopTest();
        
        SBQQ__QuoteLine__c serverQuoteLine= QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceIdserver', 'lefDetailserver');
        insert serverQuoteLine;
        
        SBQQ__QuoteLine__c calQuoteLine= QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceIdcal', 'lefDetailCAL');
        calQuoteLine.SBQQ__RequiredBy__c = serverQuoteLine.Id;
        insert calQuoteLine;
        
        Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(testQuoteLine.Id, '13124', 'USD', '1','32232', '12345', '23423', null);
            qlsl.LicenseProductJWT__c = 'abcd1234';
            qlsl.License_Number__c = '2424';
            update qlsl;
        Quote_Line_Sub_Line__c qlslForCAL = TestQuoteUtil.createMockQuoteLineSubLine(calQuoteLine.Id, '34543', 'USD', '1','3434', '23456', '54544', null);
        
        QuoteLineTableController quteLineTC = new QuoteLineTableController();
            quteLineTC.quoteId = quoteForTest.id;
            quteLineTC.userId = testUser.id;
        System.assertEquals(testUser.id, quteLineTC.userId);
        System.assert(quteLineTC.getQuoteLineWrapperList().size() > 0);
        quteLineTC.getTableWrapperList();
        }
    }   
    
    
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr, 
                                            Country_Name__c = countryName, 
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
    

    
}