@isTest
public class LeadUpdateULCTrigger_Test {

    /******************************************************

        TEST_LeadUpdateULCTrigger
    
        This method makes unit test of the LeadUpdateULCTrigger
    
        Changelog:
            2015-11-26  SAN     Created method
			2018-03-06  CRW     Modified ulc username to avoid new validation error.BMW-508
    ******************************************************/     

    static testMethod void TEST_LeadUpdateULCTrigger() {
        
        ID stdLeadRecordType = [select Id from RecordType where Name = 'Standard lead' LIMIT 1].Id;      
        
        List<Lead> leads = new List<Lead>();            
        Lead lead = new Lead(   FirstName = 'Test LeadCountryISOUpdate',
                                Country = 'Sweden', 
                                Email = 'asd@asd.com',
                                LastName = 'Lastname', 
                                City = 'Stockholm', 
                                State = 'lund',
                                Street = 'Street 12-',
                                QCloudID__c = 'Enterprise',
                                PartnerSourceNo__c = '12345',
                                Phone = '5555-5555',
                                Company = 'company',
                                PostalCode = 'po2 '                                 
                                );
       
        lead.RecordTypeId = stdLeadRecordType;       
        leads.add(lead);
        
        test.startTest();
        
        // Do insert tests
        insert(leads); 

        List<ULC_Details__c> ULCDetails = new List<ULC_Details__c>();
        ULC_Details__c ulc = new ULC_Details__c();
        ulc.ULCName__c = 'theleaduser';
        ulc.ULCStatus__c = 'Active';
        ulc.LeadId__c = leads[0].Id;
        ULCDetails.add(ulc);

        insert (ULCDetails);   

        ulc = [select Id, ULC_Email__c, ULC_QCloudID__c, ULC_PartnerSourceNo__c, Toggle_dirty__c from ULC_Details__c where ULCName__c = 'theleaduser'];
        System.assertEquals('Enterprise', ulc.ULC_QCloudID__c);
        System.assertEquals('asd@asd.com', ulc.ULC_Email__c);
        System.assertEquals('12345', ulc.ULC_PartnerSourceNo__c);
        System.assertEquals(false, ulc.Toggle_dirty__c);

        lead = [select Id, FirstName, LastName, QCloudID__c, Email, PartnerSourceNo__c from Lead where FirstName = 'Test LeadCountryISOUpdate'];
        
        lead.QCloudID__c = 'not enterprise';
        lead.Email = 'notasd@asd.com';
        lead.PartnerSourceNo__c = '54321';
        
        // Do update tests

        Semaphores.LeadTriggerHandlerAfterUpdate = false;
        update(lead);      
        ulc = [select Id, ULC_Email__c, ULC_QCloudID__c, ULC_PartnerSourceNo__c, Toggle_dirty__c  from ULC_Details__c where ULCName__c = 'theleaduser'];
        System.assertEquals('not enterprise', ulc.ULC_QCloudID__c);
        System.assertEquals('notasd@asd.com', ulc.ULC_Email__c);
        System.assertEquals('54321', ulc.ULC_PartnerSourceNo__c);
        System.assertEquals(true, ulc.Toggle_dirty__c);


        test.stopTest();
        
    }

}