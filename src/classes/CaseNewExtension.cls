/******************************************************

	CaseNewExtension
        
    Extends the CaseNew vf page
    Prepopulates various fields on the CaseNew vf page      
        
    Changelog:
    	2012-04-24  CCE         Initial development for CR# 4357 - Changes to CaseNew page
                                                      
******************************************************/
public with sharing class CaseNewExtension  {
        
	ApexPages.standardController m_sc = null;
                
    public CaseNewExtension(ApexPages.standardController sc)
    {
        m_sc = sc;
    }

    public PageReference doSaveAndEdit()
    {
    	SObject so = m_sc.getRecord();
        upsert so;
    
        string s = '/' + ('' + so.get('Id')) + '/e?retURL=/' + ('' + so.get('Id'));
        System.debug('CaseNewExtension ' + s);
                            
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info, s));
        return new Pagereference(s);
    }
    

	public static testMethod void TEST_doSaveAndEdit()
	{
		PageReference pageRef = new PageReference('/apex/CaseNew');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
		CaseNewExtension controller = new CaseNewExtension(sc);
		System.assertNotEquals(null, controller.m_sc);
		Pagereference pr = controller.doSaveAndEdit();
		string s = pr.getUrl();
		System.assert(s.contains('/e'), 'Assertion failed as /e was not added to the Pagereference');
	}
                        
}