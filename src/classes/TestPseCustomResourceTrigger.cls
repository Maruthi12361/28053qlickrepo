@isTest

/***************************************************************************************************************************************

	Changelog:
		2012-02-14  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
		2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
****************************************************************************************************************************************/

private class TestPseCustomResourceTrigger {

    static testMethod void testInsertSuperUser() {
        QTTestUtils.GlobalSetUp();
        // data preperation
       Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert testAccount;

        List<Contact> accountResources = new List<Contact>();

        for (Integer i = 0; i < 20; i++) {
            Contact c = new Contact(LastName = 'Test Resource ' + i, pse__Is_Resource__c = true, 
                AccountId = testAccount.Id);
            accountResources.add(c);
        }

        // sample user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert u;

        Contact superUser = new Contact(LastName = 'Test Resource Super User', pse__Is_Resource__c = true,
            AccountId = testAccount.Id, Account_Super_User__c = true, pse__Salesforce_User__c = u.Id);
        Contact regularUser = new Contact(LastNAme = 'Test Resource Reg', pse__Is_Resource__c = true,
            AccountId = testAccount.Id);

        // insert records for test
        insert accountResources;
        
        // test insert super user
        test.startTest();
        Semaphores.ContactTriggerHandlerAfterInsert = false;
        // insert super user record to start trigger
        insert superUser;
        // insert regular user after super user
        Semaphores.ContactTriggerHandlerAfterInsert = false;
        insert regularUser;

        test.stopTest();

        // Assert # PCs = 21
        List<pse__Permission_Control__c> pcs = [SELECT Name, pse__Resource__c, pse__Resource__r.LastName FROM
            pse__Permission_Control__c WHERE pse__User__c = :u.Id ];
        System.assert(pcs.size() == 21);
        for(pse__Permission_Control__c pc : pcs) {
            System.assert(pc.pse__Resource__c != null && 
                pc.pse__Resource__r.LastName.startsWith('Test Resource'));
        }

    }

    static testMethod void testUpdateSuperUser() {
        QTTestUtils.GlobalSetUp();
        // data preperation
       Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert testAccount;

        List<Contact> accountResources = new List<Contact>();

        for (Integer i = 0; i < 20; i++) {
            Contact c = new Contact(LastName = 'Test Resource ' + i, pse__Is_Resource__c = true, 
                AccountId = testAccount.Id);
            accountResources.add(c);
        }

        // sample user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert u;

        Contact superUser = new Contact(LastName = 'Test Resource Super User', pse__Is_Resource__c = true,
            AccountId = testAccount.Id, Account_Super_User__c = true, pse__Salesforce_User__c = u.Id);

        // insert records for test
        insert accountResources;
        insert superUser;

        // Start Test for trigger
        test.startTest();
        Semaphores.ContactTriggerHandlerBeforeUpdate = false;
        // Change super user to false
        superUser.Account_Super_User__c = false;
        update superUser;

        // Assert # PCs = 0
        List<pse__Permission_Control__c> noPcs = [SELECT Name, pse__Resource__c, pse__Resource__r.LastName FROM
            pse__Permission_Control__c WHERE pse__User__c = :u.Id ];
        System.assert(noPcs.size() == 0);
        Semaphores.ContactTriggerHandlerBeforeUpdate = false;
        // Change super user back to true
        superUser.Account_Super_User__c = true;
        update superUser;
        test.stopTest();

        // Assert # PCs = 20
        List<pse__Permission_Control__c> yesPcs = [SELECT Name, pse__Resource__c, pse__Resource__r.LastName FROM
            pse__Permission_Control__c WHERE pse__User__c = :u.Id ];
        System.assert(yesPcs.size() == 20);
    }
    
}