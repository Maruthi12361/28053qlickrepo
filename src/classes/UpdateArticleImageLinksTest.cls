/**
* UpdateArticleImageLinks
*
* Added: 		28-12-2018 - ext_bad - CHG0035127
*
* Change log:
* 28-12-2018 - ext_bad - CHG0035127 - update image links that lead to EU1 in articles.
*
*/
@isTest
public with sharing class UpdateArticleImageLinksTest {
    private static final String EU1_LINK = 'https://c.eu1.content.force.com';
    private static final String QLIK_EU1_LINK = 'https://qlik--c.eu1.content.force.com';
    private static final String SERVLET_LINK_PART = '/servlet/servlet.ImageServer?id=';
    private static final String NEW_DOMAIN_LINK = 'https://qlik.my.salesforce.com';
    private static final String FILE_DOWNLOAD_LINK = '/servlet/servlet.FileDownload?file=';
    private static final String IMG_SRC = 'src=&quot;';
    private static final String IMG_SERVER_OID = '&amp;oid=';



    @testSetup static void setup() {
        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.QlikNoReplyEmailAddress__c = 'no-reply@test.com';
        insert settings;
    }

    static testMethod void updateLinksForBasicArticles() {
        Id kaId = createBasicArticle(false);

        Basic__kav basicToTest = [SELECT Id, ArticleNumber, Cause__c, Description__c, Resolution__c FROM Basic__kav
                        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(!basicToTest.Cause__c.contains(EU1_LINK));
        System.assert(!basicToTest.Resolution__c.contains(EU1_LINK));
        System.assert(!basicToTest.Cause__c.contains(NEW_DOMAIN_LINK));
        System.assert(!basicToTest.Resolution__c.contains(NEW_DOMAIN_LINK));

        Test.startTest();
        Database.executeBatch(new UpdateArticleImageLinksBatch('\'' + basicToTest.ArticleNumber + '\'', 'Basic__kav', false));
        Test.stopTest();

        basicToTest = [SELECT Id, Cause__c, Description__c, Resolution__c FROM Basic__kav
                        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];

        System.assert(basicToTest.Cause__c.contains(NEW_DOMAIN_LINK + SERVLET_LINK_PART));
        System.assert(basicToTest.Resolution__c.contains(NEW_DOMAIN_LINK + SERVLET_LINK_PART));
    }

    static testMethod void updateLinksForDiagnosticArticles() {
        Id kaId = createDiagnosticArticle(false);
        Diagnostic__kav diagnosticToTest = [SELECT Id, ArticleNumber, Cause__c, Description__c, Resolution__c FROM Diagnostic__kav
                                    WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(!diagnosticToTest.Description__c.contains(EU1_LINK));
        System.assert(!diagnosticToTest.Resolution__c.contains(EU1_LINK));
        System.assert(!diagnosticToTest.Description__c.contains(NEW_DOMAIN_LINK));
        System.assert(!diagnosticToTest.Resolution__c.contains(NEW_DOMAIN_LINK));

        Test.startTest();
        Database.executeBatch(new UpdateArticleImageLinksBatch('\'' + diagnosticToTest.ArticleNumber + '\'', 'Diagnostic__kav', false));
        Test.stopTest();

        diagnosticToTest = [SELECT Id, Cause__c, Description__c, Resolution__c FROM Diagnostic__kav
                    WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(diagnosticToTest.Description__c.contains(NEW_DOMAIN_LINK + SERVLET_LINK_PART));
        System.assert(diagnosticToTest.Resolution__c.contains(NEW_DOMAIN_LINK + SERVLET_LINK_PART));
    }

    static testMethod void updateDomainLinksForBasicArticles() {
        Id kaId = createBasicArticle(true);

        Basic__kav basicToTest = [SELECT Id, ArticleNumber, Cause__c, Description__c, Resolution__c FROM Basic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(basicToTest.Cause__c.contains(EU1_LINK));
        System.assert(basicToTest.Resolution__c.contains(EU1_LINK));

        Test.startTest();
        Database.executeBatch(new UpdateArticleImageLinksBatch('\'' + basicToTest.ArticleNumber + '\'', 'Basic__kav'));
        Test.stopTest();

        basicToTest = [SELECT Id, Cause__c, Description__c, Resolution__c FROM Basic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(!basicToTest.Cause__c.contains(EU1_LINK));
        System.assert(!basicToTest.Resolution__c.contains(EU1_LINK));
        System.assert(basicToTest.Cause__c.contains(NEW_DOMAIN_LINK));
        System.assert(basicToTest.Resolution__c.contains(NEW_DOMAIN_LINK));
    }

    static testMethod void updateDomainLinksForDiagnosticArticles() {
        Id kaId = createDiagnosticArticle(true);
        Diagnostic__kav diagnosticToTest = [SELECT Id, ArticleNumber, Cause__c, Description__c, Resolution__c FROM Diagnostic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(diagnosticToTest.Description__c.contains(EU1_LINK));
        System.assert(diagnosticToTest.Resolution__c.contains(EU1_LINK));

        Test.startTest();
        Database.executeBatch(new UpdateArticleImageLinksBatch('\'' + diagnosticToTest.ArticleNumber + '\'', 'Diagnostic__kav'));
        Test.stopTest();

        diagnosticToTest = [SELECT Id, Cause__c, Description__c, Resolution__c FROM Diagnostic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(!diagnosticToTest.Description__c.contains(EU1_LINK));
        System.assert(!diagnosticToTest.Resolution__c.contains(EU1_LINK));
        System.assert(diagnosticToTest.Description__c.contains(NEW_DOMAIN_LINK));
        System.assert(diagnosticToTest.Resolution__c.contains(NEW_DOMAIN_LINK));
    }

    static testMethod void updateFileDownloadForBasicArticles() {
        Id kaId = createBasicFileDownloadArticle();

        Basic__kav basicToTest = [SELECT Id, ArticleNumber, Cause__c, Description__c, Resolution__c FROM Basic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(basicToTest.Cause__c.contains(EU1_LINK));
        System.assert(basicToTest.Resolution__c.contains(EU1_LINK));
        System.assert(basicToTest.Cause__c.contains(FILE_DOWNLOAD_LINK));
        System.assert(basicToTest.Resolution__c.contains(FILE_DOWNLOAD_LINK));

        Test.startTest();
        Database.executeBatch(new UpdateArticleImageLinksBatch('\'' + basicToTest.ArticleNumber + '\'', 'Basic__kav'));
        Test.stopTest();

        basicToTest = [SELECT Id, Cause__c, Description__c, Resolution__c FROM Basic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(!basicToTest.Cause__c.contains(EU1_LINK));
        System.assert(!basicToTest.Resolution__c.contains(EU1_LINK));
        System.assert(basicToTest.Cause__c.contains(NEW_DOMAIN_LINK + SERVLET_LINK_PART + '123456789123456' + IMG_SERVER_OID));
        System.assert(basicToTest.Resolution__c.contains(NEW_DOMAIN_LINK + SERVLET_LINK_PART + '123456789123456' + IMG_SERVER_OID));
        System.assert(!basicToTest.Cause__c.contains(FILE_DOWNLOAD_LINK));
        System.assert(!basicToTest.Resolution__c.contains(FILE_DOWNLOAD_LINK));
    }

    static testMethod void updateQlikEu1ForDiagnosticArticles() {
        Id kaId = createDiagnosticQlikEu1Article();
        Diagnostic__kav diagnosticToTest = [SELECT Id, ArticleNumber, Cause__c, Description__c, Resolution__c FROM Diagnostic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(diagnosticToTest.Description__c.contains(QLIK_EU1_LINK));
        System.assert(diagnosticToTest.Resolution__c.contains(QLIK_EU1_LINK));

        Test.startTest();
        Database.executeBatch(new UpdateArticleImageLinksBatch('\'' + diagnosticToTest.ArticleNumber + '\'', 'Diagnostic__kav'));
        Test.stopTest();

        diagnosticToTest = [SELECT Id, Cause__c, Description__c, Resolution__c FROM Diagnostic__kav
        WHERE PublishStatus = 'Online' AND KnowledgeArticleId = :kaId];
        System.assert(!diagnosticToTest.Description__c.contains(QLIK_EU1_LINK));
        System.assert(!diagnosticToTest.Resolution__c.contains(QLIK_EU1_LINK));
        System.assert(diagnosticToTest.Description__c.contains(NEW_DOMAIN_LINK));
        System.assert(diagnosticToTest.Resolution__c.contains(NEW_DOMAIN_LINK));
    }

    static testMethod void generateReport() {
        UpdateArticleImageLinksBatch updateLinks = new UpdateArticleImageLinksBatch();
        Set<String> errors = new Set<String>{'123456', '987654'};

        updateLinks.TEST_EMAIL_SENDING = false;
        Test.startTest();
        updateLinks.generateErrorReport(errors);
        Test.stopTest();

        System.assert(updateLinks.TEST_EMAIL_SENDING);
    }

    private static ID createBasicArticle(boolean domainLink) {
        Basic__kav basic = new Basic__kav(Title = 'test Basic article', UrlName = 'testURL' + System.now().millisecond(),
                Summary = 'test summary', Cause__c = IMG_SRC + (domainLink ? EU1_LINK : '') + SERVLET_LINK_PART + '123456789123456',
                Resolution__c = IMG_SRC + (domainLink ? EU1_LINK : '') + SERVLET_LINK_PART + '123456789123456', Language = 'en_US', IsVisibleInCsp = true);
        // need to simulate old created article
        Semaphores.BasicKnowledgeArticlesTriggerBeforeInsert = true;
        insert basic;
        basic = [SELECT Id, KnowledgeArticleId FROM Basic__kav WHERE Id = :basic.Id];

        KbManagement.PublishingService.publishArticle(basic.KnowledgeArticleId, true);
        return basic.KnowledgeArticleId;
    }

    private static ID createDiagnosticArticle(boolean domainLink) {
        Diagnostic__kav article = new Diagnostic__kav(Title = 'test Diagnostic article', UrlName = 'testURL' + System.now().millisecond(),
                Description__c = IMG_SRC + (domainLink ? EU1_LINK : '') + SERVLET_LINK_PART + '123456789123456',
                Resolution__c = IMG_SRC + (domainLink ? EU1_LINK : '') + SERVLET_LINK_PART + '123456789123456',
                Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);
        // need to simulate old created article
        Semaphores.DiagnosticKnowledgeArticlesTriggerBeforeInsert = true;
        insert article;
        article = [SELECT Id, KnowledgeArticleId FROM Diagnostic__kav WHERE Id = :article.Id];

        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        return article.KnowledgeArticleId;
    }

    private static ID createBasicFileDownloadArticle() {
        Basic__kav basic = new Basic__kav(Title = 'test Basic article', UrlName = 'testURL' + System.now().millisecond(),
                Summary = 'test summary', Cause__c = IMG_SRC + EU1_LINK + FILE_DOWNLOAD_LINK + '123456789123456',
                Resolution__c = IMG_SRC + EU1_LINK + FILE_DOWNLOAD_LINK + '123456789123456', Language = 'en_US', IsVisibleInCsp = true);
        // need to simulate old created article
        Semaphores.BasicKnowledgeArticlesTriggerBeforeInsert = true;
        insert basic;
        basic = [SELECT Id, KnowledgeArticleId FROM Basic__kav WHERE Id = :basic.Id];

        KbManagement.PublishingService.publishArticle(basic.KnowledgeArticleId, true);
        return basic.KnowledgeArticleId;
    }

    private static ID createDiagnosticQlikEu1Article() {
        Diagnostic__kav article = new Diagnostic__kav(Title = 'test Basic article', UrlName = 'testURL' + System.now().millisecond(),
                Cause__c = 'test summary', Description__c = IMG_SRC + QLIK_EU1_LINK + SERVLET_LINK_PART + '123456789123456',
                Resolution__c = IMG_SRC + QLIK_EU1_LINK + SERVLET_LINK_PART + '123456789123456', Language = 'en_US', IsVisibleInCsp = true);
        // need to simulate old created article
        Semaphores.BasicKnowledgeArticlesTriggerBeforeInsert = true;
        insert article;
        article = [SELECT Id, KnowledgeArticleId FROM Diagnostic__kav WHERE Id = :article.Id];

        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        return article.KnowledgeArticleId;
    }
}