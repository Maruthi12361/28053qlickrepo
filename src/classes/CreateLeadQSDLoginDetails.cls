/********************************************************
* ScheduleCreateQSDLoginDetails
* Description: This is the class use to where the activity and campaign association is done for 
leads and contacts whenever Boomi updates the total login count.
*
* Change Log: 22-06-2017 v1 UIN Added Initial logic
* Change Log: 23-06-2017 v2 UIN Fix for bug 1699
* Change Log: 23-06-2017 v3 UIN CHG0031909 Fix for already associated campaigns. Changed error mail messages.
*********************************************************/
global class CreateLeadQSDLoginDetails implements Database.Batchable<sObject>, Database.Stateful{
	
	String query;
	String sObjectName;
	global List<String> exception_List;
	
	global CreateLeadQSDLoginDetails(String objectName) {
		if(exception_List == null)
            exception_List = new List<String>();
        sObjectName = objectName;
        if(objectName == 'Contact')
        	query = 'select id, name, Updated_Total_Login_Count__c from ' + sObjectName + ' where Updated_Total_Login_Count__c = true';
    	else if(objectName == 'Lead')
    		query = 'select id, name, Updated_Total_Login_Count__c from ' + sObjectName + ' where isconverted = false';
	}
	


	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Task> tasks = new List<Task>();
        List<CampaignMember> CampaignMembers = new List<CampaignMember>();
		List<Id> Lids = new List<Id>();
		Lead l;
		Contact ct;
		
		Map<Id, Set<ID>> mCampIdMemberIds = new Map<Id, Set<Id>>();



		if(Lead.Sobjecttype == scope[0].Id.getSObjectType())
			this.sObjectName = 'Lead';
		else if(Contact.Sobjecttype == scope[0].Id.getSObjectType())
			this.sObjectName = 'Contact';
		for(sobject s: scope){
			if('Contact' == this.sObjectName)
				lIds.add(s.Id);
			else if('Lead' == this.sObjectName){
				l = (Lead)s;
				if(l.Updated_Total_Login_Count__c)
					lIds.add(s.Id);
			}
		}
		
		for(CampaignMember cm: [select id,leadid,contactId from campaignmember where campaignid = :Label.QSD_Campaign_ID and (leadid in:lIds or contactid in :lIds)]){
			if(mCampIdMemberIds.containskey(Label.QSD_Campaign_ID)){
				if(String.isNotblank(cm.LeadId))
					mCampIdMemberIds.get(Label.QSD_Campaign_ID).add(cm.LeadId);
				if(String.isNotblank(cm.contactId))
					mCampIdMemberIds.get(Label.QSD_Campaign_ID).add(cm.contactId);
			}
			else{
				if(String.isNotblank(cm.LeadId))
					mCampIdMemberIds.put(Label.QSD_Campaign_ID, new Set<Id>{cm.LeadId});
				else if(String.isNotblank(cm.contactId))
					mCampIdMemberIds.put(Label.QSD_Campaign_ID, new Set<Id>{cm.contactId});
			}

		}


		String activityQuery = 'select id, (select subject from Tasks where subject = \'QlikSense Desktop Login\') from '+ this.sobjectname +' where id in : lIds';
		List<sObject> lsobj = Database.Query(activityQuery);
		Boolean crAct = false;
		Boolean assCampaign = false;
		for(Sobject s: lsobj){
			if('Lead'.equalsIgnoreCase(this.sObjectName)){
				l = (Lead)s;
				if(l.tasks.size() ==  0)
					crAct = true;
				if(mCampIdMemberIds.isEmpty() || !mCampIdMemberIds.get(Label.QSD_Campaign_ID).contains(l.Id))
					assCampaign = true;
			}
			else if('Contact'.equalsIgnoreCase(this.sObjectName)){
				ct = (Contact)s;
				if(ct.tasks.size() ==  0)
					crAct = true;
				if(mCampIdMemberIds.isEmpty() || !mCampIdMemberIds.get(Label.QSD_Campaign_ID).contains(ct.Id))
					assCampaign = true;
			}

			if(crAct){
				Task task = new Task();
	            task.WhoId = s.Id;
	            task.Subject = 'QlikSense Desktop Login';
	            task.ActivityDate = Date.today();
	            task.Incentive_Name__c = 'QSD Login';
	            task.Source_ID2__c = 'QlikSense Desktop';
	            task.Status = 'Completed';
	            task.Priority = 'Normal';
	            task.Type = 'TK - Web Activity'; // This is mandatory in QA/Prod
	            task.Description = 'QSD Login ' + string.valueOf(DateTime.now());
	            tasks.add(task);
	        }
	        crAct = false;
	        if(assCampaign){    
                CampaignMember cm;
                cm = new CampaignMember();
                //cm.CampaignId = '70126000000LVF8'; // QA - 70126000000LVF8, Prod - 701D0000001PKdf
                cm.CampaignId = Label.QSD_Campaign_ID;
                cm.Status = 'Form Fill Out';
                cm.Number_Of_Form_Submissions__c = 1;
                if('Lead'.equalsIgnoreCase(this.sObjectName))
                	cm.LeadId = s.Id;
            	else if('Contact'.equalsIgnoreCase(this.sObjectName))
        			cm.ContactId = s.Id;
                CampaignMembers.add(cm);
			}
			assCampaign = false;
		}

		if(tasks.size()>0){
			List<Database.SaveResult> sResults_act = Database.insert(tasks, false);
			for(integer i =0; i<sResults_act.size();i++){
                String msg='';
                If(!sResults_act[i].isSuccess()){
                    msg += 'Activity creation failed for id'+ tasks.get(i).WhoId + 'failed due to the Error: "';        
                    for(Database.Error err: sResults_act[i].getErrors()){  
                         msg += err.getmessage()+'<br/>';
                    } 
                }
            if(msg!='' && msg.contains('Error:'))
                exception_List.add(msg);
            } 
            if(System.Test.isRunningTest()){
				exception_List.add('efgh');
				sendErrorMails(exception_List);
			}
            if(exception_List.size() > 50)
                sendErrorMails(exception_List);
        }
        if(CampaignMembers.size()>0){
			List<Database.SaveResult> sResults_cm = Database.insert(CampaignMembers, false);
			for(integer i =0; i<sResults_cm.size();i++){
                String msg='';
                If(!sResults_cm[i].isSuccess()){
                    msg += 'Campaign member creation failed due to the Error for record with id'+CampaignMembers.get(i).LeadId+CampaignMembers.get(i).ContactId +': "';        
                    for(Database.Error err: sResults_cm[i].getErrors()){  
                         msg += err.getmessage()+'<br/>';
                    } 
                }
            if(msg!='' && msg.contains('Error:'))
                exception_List.add(msg);
            } 
            
            if(exception_List.size() > 50)
                sendErrorMails(exception_List);
        }	
	}
	
	global void finish(Database.BatchableContext BC) {
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
				            TotalJobItems, CreatedBy.Email
				            FROM AsyncApexJob WHERE Id =
				            :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[] {a.CreatedBy.Email};
        if(System.Test.isRunningTest()){
        	mail.settargetobjectId(a.CreatedBy.ID);
        }
    	else{
			mail.setToAddresses(((String)Label.QSD_Mailing_List).split(';'));
    	}
        mail.setSaveAsActivity(false);
        mail.setSubject('QSD Login Update Batch Job Status ' + a.Status);
        mail.setPlainTextBody('The batch job has finished.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		
	}

	private void sendErrorMails(List<String> exceptions){
        String errMsg = 'The batch Apex job errors for Records processed till now are' +'<br/>';
        String temp='';
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[] {((String)Label.QSD_Mailing_List).split(';')};
        if(System.Test.isRunningTest()){
        	String[] toAddresses = new String[] {'ullalramakrishna.kini@qlik.com'};
        	mail.setToAddresses(toAddresses);
        }
        else{
 			mail.setToAddresses(((String)Label.QSD_Mailing_List).split(';'));
        }
        mail.setSubject('QSD Login Update Batch Status In-Progress');
        if(!exceptions.isEmpty()){
            for(String str:exceptions)
                temp = temp + str;
            mail.setHTMLBody(errMsg + temp);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            exception_List = new List<String>();
        }  
    }
	
}