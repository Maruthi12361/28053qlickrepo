@isTest

/***************************************************************************************************************************************

	Changelog:
		2012-02-15  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
							to TestPseMarkProjectBillableTrigger() and testUpdateBudgets()
        2017-11-09  ext_vos Add test for new logic: Populates the Project PO number when budget was approved.                    
****************************************************************************************************************************************/
// ext_vos     2017-11-09   was moved to PseBudgetTriggerTest.cls
// to be deleted
private class TestPseMarkProjectBillableTrigger {

    /*static testMethod void testNewBudgets() {
        // setup account
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert testAccount;

        // setup Project
        List<pse__Proj__c> projects = new List<pse__Proj__c>();
        for (Integer i = 0; i < 20; i++) {
            pse__Proj__c project = new pse__Proj__c(
                Name = 'Test Project ' + i, 
                pse__Is_Active__c = true,
                pse__Allow_Timecards_Without_Assignment__c = true,
                pse__Is_Billable__c = false, 
                pse__Account__c = testAccount.Id, 
                pse__Stage__c = 'In Progress',
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                Invoicing_Type__c = 'Deferred');
            projects.add(project);
        }
        insert projects;

        // Setup budgets
        List<pse__Budget__c> budgets = new List<pse__Budget__c>();
        Integer i = 0;
        for (pse__Proj__c project : projects) {
            pse__Budget__c budget = new pse__Budget__c(Name = 'Test Budget ' + i++, pse__Project__c = project.Id,
                pse__Status__c = 'Approved', pse__Approved__c = true, CurrencyIsoCode = 'USD');
            budgets.add(budget);
        }
        insert budgets;

        List<pse__Proj__c> updatedProjects = [SELECT Name FROM pse__Proj__c WHERE Id in :projects];
        for (pse__Proj__c updatedProject : updatedProjects) {
            System.assert(updatedProject.pse__Is_Billable__c = true);
        }

    }

    static testMethod void testUpdateBudgets() {
        // setup account
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert testAccount;

        // setup Project
        List<pse__Proj__c> projects = new List<pse__Proj__c>();
        for (Integer i = 0; i < 20; i++) {
            pse__Proj__c project = new pse__Proj__c(
                Name = 'Test Project ' + i, 
                pse__Is_Active__c = true, 
                pse__Allow_Timecards_Without_Assignment__c = true,
                pse__Is_Billable__c = false, 
                pse__Account__c = testAccount.Id,
                pse__Stage__c = 'In Progress',
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                Invoicing_Type__c = 'Deferred');
            projects.add(project);
        }
        insert projects;

        // Setup budgets
        List<pse__Budget__c> budgets = new List<pse__Budget__c>();
        Integer i = 0;
        for (pse__Proj__c project : projects) {
            pse__Budget__c budget = new pse__Budget__c(Name = 'Test Budget ' + i++, pse__Project__c = project.Id, CurrencyIsoCode = 'USD');
            budgets.add(budget);
        }
        insert budgets;

        for (pse__Budget__c budget : budgets) {
            budget.pse__Status__c = 'Approved';
            budget.pse__Approved__c = true;
        }
        update budgets;

        List<pse__Proj__c> updatedProjects = [SELECT Name FROM pse__Proj__c WHERE Id in :projects];
        for (pse__Proj__c updatedProject : updatedProjects) {
            System.assert(updatedProject.pse__Is_Billable__c = true);
        }
    }

    static testMethod void testPopulatingPONumberFromBudgets() {
        // setup test data
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary(); 
        QlikTech_Company__c comp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
            Country_Name__c = 'United Kingdom',
            Subsidiary__c = sub.id                        
        );
        insert comp;
        
        Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = comp.QlikTech_Company_Name__c, Billing_Country_Code__c = comp.Id);
        insert testAccount;

        pse__Proj__c project = new pse__Proj__c(
                                    Name = 'Test Project ', 
                                    pse__Is_Active__c = true,
                                    pse__Allow_Timecards_Without_Assignment__c = true,
                                    pse__Is_Billable__c = false, 
                                    pse__Account__c = testAccount.Id, 
                                    pse__Stage__c = 'In Progress',
                                    pse__Start_Date__c = Date.today().addDays(-10),
                                    pse__End_Date__c = Date.today().addDays(30),
                                    Key_Engagement_Features__c = 'QlikView',
                                    Purpose_of_Engagement__c = 'Test',
                                    Sales_Classification__c = 'Internal',
                                    Customer_Critial_Success_Factors__c = 'Test',
                                    Invoicing_Type__c = 'Deferred');
        insert project;
        
        pse__Proj__c toTest = [select PO_Number__c from pse__Proj__c where Id = :project.Id];
        System.assertEquals('na', toTest.PO_Number__c);

        List<pse__Budget__c> budgets = new List<pse__Budget__c>();
        pse__Budget__c budgetOpen = new pse__Budget__c(Name = 'Test Budget ONE', 
                                        pse__Project__c = project.Id,
                                        pse__Status__c = 'Open',
                                        PO_Number__c = '11111111',
                                        CurrencyIsoCode = 'USD');
        budgets.add(budgetOpen);
        pse__Budget__c budgetApproved = new pse__Budget__c(Name = 'Test Budget TWO', 
                                        pse__Project__c = project.Id,
                                        pse__Status__c = 'Approved',
                                        pse__Approved__c = true,
                                        PO_Number__c = '222222222',
                                        CurrencyIsoCode = 'USD');
        budgets.add(budgetApproved);
        insert budgets;

        toTest = [select PO_Number__c from pse__Proj__c where Id = :project.Id];
        System.assertNotEquals(budgetOpen.PO_Number__c, toTest.PO_Number__c);
        System.assertEquals(budgetApproved.PO_Number__c, toTest.PO_Number__c);

        budgetOpen.pse__Status__c = 'Approved';
        update budgetOpen;

        toTest = [select PO_Number__c from pse__Proj__c where Id = :project.Id];
        System.assertEquals(budgetOpen.PO_Number__c, toTest.PO_Number__c);
    }*/
}