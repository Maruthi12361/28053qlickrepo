/**
* Change log:
*
*   2019-10-07 ext_bad	IT-2173: create the test for EditAtRiskEscalationController
*/
@IsTest
private class EditAtRiskEscalationTest {

    static testMethod void testRejected() {
        Account acc = new Account();
        acc.Name = 'testRejected';
        insert acc;
        At_risk_escalation__c esc = new At_risk_escalation__c();
        esc.Account__c = acc.Id;
        insert esc;

        ApexPages.StandardController ctrl = new ApexPages.StandardController(esc);
        EditAtRiskEscalationController contr = new EditAtRiskEscalationController(ctrl);
        System.assertEquals(esc.Id, contr.escalation.Id);

        contr.escalation.Rejected_Reason__c = 'test reason';

        Test.startTest();
        PageReference ref = contr.saveReason();
        Test.stopTest();

        System.assert(!ref.getUrl().contains('AtRiskEscalationsList'));
        System.assert(ref.getUrl().contains(acc.Id));
        esc = [SELECT Id, Rejected__c FROM At_risk_escalation__c WHERE Id = :esc.Id];
        System.assert(esc.Rejected__c);
    }

    static testMethod void testCreate() {
        Account acc = new Account();
        acc.Name = 'testCreate';
        insert acc;
        At_risk_escalation__c esc = new At_risk_escalation__c();
        esc.Account__c = acc.Id;

        ApexPages.StandardController ctrl = new ApexPages.StandardController(esc);
        EditAtRiskEscalationController contr = new EditAtRiskEscalationController(ctrl);
        System.assertEquals(null, contr.escalation.Id);

        Test.startTest();
        PageReference ref = contr.save();
        Test.stopTest();

        System.assert(!ref.getUrl().contains('AtRiskEscalationsList'));
        System.assert(ref.getUrl().contains(acc.Id));
    }

    static testMethod void testSaveFromAccount() {
        Account acc = new Account();
        acc.Name = 'testSaveFromAccount';
        insert acc;
        At_risk_escalation__c esc = new At_risk_escalation__c();
        esc.Account__c = acc.Id;
        insert esc;

        AddAtRiskEscalationByAccountController accContr = new AddAtRiskEscalationByAccountController();
        accContr.escId = esc.Id;
        accContr.accId = acc.Id;

        EditAtRiskEscalationController contr = new EditAtRiskEscalationController();
        contr.escFromAccountController = accContr;
        System.assertEquals(esc.Id, contr.escalation.Id);

        Test.startTest();
        contr.escalation.Completion_date__c = null;
        PageReference ref = contr.save();
        Test.stopTest();

        System.assert(ref.getUrl().contains('AtRiskEscalationsList'));

        acc = [SELECT Id, Escalation_is_in_progress__c FROM Account WHERE Id = :acc.Id];
        System.assert(acc.Escalation_is_in_progress__c);
        contr.escalation.Completion_date__c = Date.today();
        contr.save();
        acc = [SELECT Id, Escalation_is_in_progress__c FROM Account WHERE Id = :acc.Id];
        System.assert(!acc.Escalation_is_in_progress__c);
    }
}