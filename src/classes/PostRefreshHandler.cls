/*
* File PostRefreshHandler
    * @description : Class for Post deployemnt activity for updating post deployment activity
                     It will be executed after QA refresh in prod.
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       09.01.2018   Pramod Kumar V      Created Class
    2       16.01.2018   Pramod Kumar V      Added logic from SandboxRefresh_Controller class
                                             Automated Jira Tickets: SFDC: Sandbox Refresh App,SFDC: Contact email updates,SFDC: Lead Email Updates
    3       28.04.2018   Pramod Kumar v      ITRM-60   Fixed Mixed DML exception and added logic to create subsidary
    4       18.07.2018   ext_bjd             OAR-64 Added necessary logic for inActive users for Activate them after refresh
    5       25.07.2018   ext_bjd             QAR-64 Added runUpdate method for asynchronous invoke other methods to fix error MIXED_DML_OPERATION
    6       25.09.2018   ext_bjd             ITRM-225 Added logic for creation Appirio records
    7       24.01.2019   ext_bjd             ITRM-228 Excluded creation Campaign, this logic contains in test class TaskAddLeadContactToCampaignTest
    8       25.01.2019   ext_bjd             ITRM-301 Added activation of users with license limit exceeded
    9       03.02.2020   ext_bjd             ITRM-380 code refactored
    10      01.07.2020   ext_bjd             ITRM-487 Initial Development
*/
public class PostRefreshHandler {
    public static void postRefreshHelper(String sandboxName) {
        Boolean isSandbox = isSandboxFinder();

        // Define Sandbox name
        if (sandboxName == null) {
            SandboxNameFinder snf = new SandboxNameFinder();
            sandboxName = snf.SandboxNameHelper();
        }

        if (isSandbox || (!isSandbox && Test.isRunningTest())) {
            // Create QlikTech Company
            RefreshApp.Create_QlikComp();
            // Create Appirio records
            InitCustomMetadataType.runInsertCustomMetadata();
            // Update Users
            ModifyInActiveUsers.runUserUpdatesWithLicenses(sandboxName);
            // Update custom settings. Refer the link to see what custom setting are updated
            // https://confluence.qliktech.com/pages/viewpage.action?spaceKey=ISD&title=Salesforce+-+Refresh+Guide#Salesforce-RefreshGuide-HowtogenerateaSecurityToken
            UpdateCustomSettings.runUpdate(sandboxName);
        }
    }

    public static Boolean isSandboxFinder() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}