/********************************************************
* CLASS: WebActivityRecordDownload
* DESCRIPTION: Handles record download of leads and contacts
*
* CHANGELOG:    
*   2018-01-31 - CRW - Added Initial logic
*********************************************************/
public class WebActivityRecordDownload {
    public static List<WebActivityUtils.WebActivityContent> RecordDownload(List<WebActivityUtils.WebActivityContent> lstWAContent){
        List<Lead> leads = new List<Lead>();
        List<Contact> contacts = new List<Contact>();
        List<Id> lstContacts = new List<Id>();
        List<String> lstLeads = new List<String>();
        Map<Id, WebActivityUtils.AdditionalParams> mapContactToAddParams = new Map<Id, WebActivityUtils.AdditionalParams>();
        Map<Id, WebActivityUtils.AdditionalParams> mapLeadToAddParams = new Map<Id, WebActivityUtils.AdditionalParams>();
        Map <String, String> LeadZiftPartnerIdMap = new Map<String, String>();
        Map <String, String> LeadZiftPartnerNameMap = new Map<String, String>();
        Map <String, String> ContactZiftPartnerIdMap = new Map<String, String>();
        Map <String, String> ContactZiftPartnerNameMap = new Map<String, String>();
        map<string,string> mapLeadSaveResult = new map<string,string>();
        map<string,string> mapContactSaveResult = new map<string,string>();
        map<string,string> mapLeadTasksSaveResult = new map<string,string>();
        List<Task> LstTasksToCreate = new List<Task>();
        //Load Zift Parameters to oUser
        lstWAContent = PopulateZiftParamsToList (lstWAContent);
        //Load Lead/Contact ID to Ouser
        lstWAContent = GetLeadOrContact(lstWAContent);
        
        
        For (WebActivityUtils.WebActivityContent cl : lstWAContent){
            if (String.isNotBlank(cl.oUser.LeadID)){
                lstLeads.add(cl.oUser.LeadID);
                LeadZiftPartnerIdMap.put(cl.oUser.LeadID, cl.oUser.ziftpartnerID);
                LeadZiftPartnerNameMap.put(cl.oUser.LeadID, cl.oUser.ziftpartnername);
                mapLeadToAddParams.put(cl.oUser.LeadID,cl.additParams);
            }
            else if (String.isNotBlank(cl.oUser.ContactID)) {
                lstContacts.add(cl.oUser.ContactID);
                ContactZiftPartnerIdMap.put(cl.oUser.ContactID, cl.oUser.ziftpartnerID);
                ContactZiftPartnerNameMap.put(cl.oUser.ContactID, cl.oUser.ziftpartnername);
                mapContactToAddParams.put(cl.oUser.ContactID,cl.additParams);
            }
        }
        System.debug ('leads>>' + lstLeads);
        System.debug ('contcts>>' + lstContacts);
        if (lstLeads.size() > 0) {
			leads = [SELECT Id, Zift_Partner_Id__c, Zift_Partner_Name__c FROM Lead WHERE Id in :lstLeads];
        } 
        if (lstContacts.size() > 0){
        	contacts = [SELECT Id, Zift_Lead_Partner_Id__c, Zift_Partner_Name__c FROM Contact WHERE Id in :lstContacts];
        }
        
        System.debug ('lead map with additparams' + mapContactToAddParams);

        if(!leads.isEmpty() && leads.size() >0){
            
            for(Lead ll : leads){
                WebActivityUtils.AdditionalParams additParams = new WebActivityUtils.AdditionalParams();
                if (mapLeadToAddParams.containsKey(ll.id)){
                    additParams = mapLeadToAddParams.get(ll.id);
                    System.debug ('additparams value in loop' + additParams);
                    ll.Lead_Source_Detail_Mirror__c = String.isNotBlank(additParams.LeadSourceDetailMirror) ? additParams.LeadSourceDetailMirror : '';
                    if (String.isNotBlank(additParams.TcAccepted) && ('TRUE'.equalsignorecase(additParams.TcAccepted))){
                        ll.Accepted_T_and_C__c = true;
                    }
                    if (LeadZiftPartnerIdMap.containsKey(ll.id)){
                        String ZPartnerId = LeadZiftPartnerIdMap.get(ll.id);
                        if(!ZPartnerId.contains('{')){
                            ll.Zift_Partner_Id__c = String.isNotBlank(ZPartnerId) ? ZPartnerId : '';
                        }
                    }
                    if (LeadZiftPartnerNameMap.containsKey(ll.id)){
                        String ZPartnerName = LeadZiftPartnerNameMap.get(ll.id);
                        if(!ZPartnerName.contains('{')){
                        ll.Zift_Partner_Name__c = String.isNotBlank(ZPartnerName) ? ZPartnerName : '';
                        }
                    }
                    ll.Marketing_Suspended__c = true; 
                    
                    LstTasksToCreate.add(CreateTask(additParams,ll.Id));
                }
            }
          System.debug ('leads after update>>' + leads); 
        }
        if (!contacts.isEmpty() && contacts.size() >0){
            for (Contact CL:contacts){
                if (ContactZiftPartnerIdMap.containsKey(CL.id)){
                        String ZPartnerId = ContactZiftPartnerIdMap.get(CL.id);
                        CL.Zift_Lead_Partner_Id__c = String.isNotBlank(ZPartnerId) ? ZPartnerId : '';   
                    }
                    if (ContactZiftPartnerNameMap.containsKey(CL.id)){
                        String ZPartnerName = ContactZiftPartnerNameMap.get(CL.id);
                        CL.Zift_Partner_Name__c = String.isNotBlank(ZPartnerName) ? ZPartnerName : '';
                    }
                WebActivityUtils.AdditionalParams additParams = new WebActivityUtils.AdditionalParams();
                additParams = mapContactToAddParams.get(CL.id);
                LstTasksToCreate.add(CreateTask(additParams,CL.Id));
            }
            System.debug ('leads after update>>' + contacts);
        }
        //Task Creation
		System.debug ('tasks with leads>>' + lstTasksToCreate);
       
        try {
            List<Database.SaveResult> saveResults = Database.insert(lstTasksToCreate, false);
            for(integer i = 0; i < saveResults.size(); i++){
                String msg = 'Error (Leads Update): ';
                if(saveResults[i].isSuccess()){
                    mapLeadTasksSaveResult.put(lstTasksToCreate.get(i).WhoId, lstTasksToCreate.get(i).WhoId);
                }
                else
                {
                    for(Database.Error err: saveResults[i].getErrors()){  
                        msg += err.getmessage() + '\n';
                    }
                    mapLeadTasksSaveResult.put(lstTasksToCreate.get(i).WhoId, msg);
                }
            }
            System.debug ('tasks with leads results>>' + mapLeadTasksSaveResult);
		    //Update list of WA with LeadID or Error
            for(WebActivityUtils.WebActivityContent CreatedTaskResult : lstWAContent) {
                String Results;
                if (String.isNotBlank(CreatedTaskResult.oUser.LeadId)){
                    Results = mapLeadTasksSaveResult.get(CreatedTaskResult.oUser.LeadId);
                }else {Results = mapLeadTasksSaveResult.get(CreatedTaskResult.oUser.ContactId);}
                
                if (String.isNotBlank(Results) && Results.startsWith('00Q')){
                    CreatedTaskResult.oUser.LeadId = Results;
                }else if (String.isNotBlank(Results) && Results.startsWith('003')){
                    CreatedTaskResult.oUser.ContactId = Results;
                }
                else if (String.isNotBlank(Results) && Results.contains('Error')){
                    CreatedTaskResult.Status = 'Failed';
                    CreatedTaskResult.AuditTrail = CreatedTaskResult.AuditTrail + '\n' + Results;
                }
            }
        }catch (System.Exception ex) {
            System.debug('Task creation for Lead/Contact Failed- ' + ex.getMessage());
        }
        
        //Campaign Member update or creation
        lstWAContent = CreateOrUpdateCampaignMember(lstWAContent,leads,contacts,mapLeadToAddParams,mapContactToAddParams);
        
		//Saving updated leads list
        try {
            List<Database.SaveResult> saveResults = Database.update(leads, false);
        }catch (System.Exception ex) {
            System.debug('Lead update Failed- ' + ex.getMessage());
        }
        //Saving updated contacts list
        try {
            List<Database.SaveResult> saveResults = Database.update(contacts, false);
        }catch (System.Exception ex) {
            System.debug('Contact update Failed- ' + ex.getMessage());
        }
        return lstWAContent;
    }
    @TestVisible
    private static Task CreateTask(WebActivityUtils.AdditionalParams additParams, Id leadORCOntactId){
        Task task = new Task();
        System.debug('params in task method ' + additParams);
        if (leadORCOntactId != null){
            if(additParams.Incentive != null){
                if (String.isNotBlank(additParams.Incentive)){task.Subject = additParams.Incentive;
                                                             task.Incentive_Name__c = additParams.Incentive;}
                if (String.isNotBlank(additParams.SourceId2)){task.Source_ID2__c = additParams.SourceId2; 
                                                              task.Subject = task.Subject + '-' + additParams.SourceId2;}
                task.Description = task.Subject + string.valueOf(DateTime.now());
                task.ActivityDate = Date.today();
                task.Status = 'Completed';
                task.Priority = 'Normal';
                task.Type = 'TK - Web Activity';
                task.WhoId = leadORCOntactId;
                return task;
            } 
        }
        return null;
    }
    
    @TestVisible
    private static List<WebActivityUtils.WebActivityContent> CreateOrUpdateCampaignMember(List<WebActivityUtils.WebActivityContent> lstWAContent,List<Lead> leads, List<Contact> contacts, Map <Id, WebActivityUtils.AdditionalParams> mapLeadToAddParams,Map <Id, WebActivityUtils.AdditionalParams> mapContactToAddParams){
        List<CampaignMember> FoundCampaignMembers = new List<CampaignMember>();
        List<CampaignMember> FoundCampaignMembersToUpdate = new List<CampaignMember>();
        
        List<CampaignMember> ListOfCampaignMembersToCreate = new List<CampaignMember>();
        List<String> LeadsContactsWithoutCampaignMembers = new List<String>();
        Map <Id, Id> MapLeadToCampaignId = new map<Id,Id>();
        Map <Id, Id> MapContactToCampaignId = new map<Id,Id>();
        map<string,string> mapCMUpdateSaveResult = new map<string,string>();
        List <Id> LeadIds = new List<Id>();
        List <Id> ContactIds = new List<Id>();
        Map<Id,List<CampaignMember>> PMapLIdToCamaignMember = new map<Id,List<CampaignMember>>();
        Map<Id,List<CampaignMember>> PMapCIdToCamaignMember = new map<Id,List<CampaignMember>>();
        Map <Id, Exception> MapIdToException = new map<Id,Exception>();
        system.debug('leads for CM >>' + leads);
        WebActivityUtils.AdditionalParams additParams = new WebActivityUtils.AdditionalParams();
        for (Lead CMForLeads: leads){
            additParams = mapLeadToAddParams.get(CMForLeads.Id);
            system.debug('addparams of lead' + additParams);
            LeadIds.add(CMForLeads.Id);
            MapLeadToCampaignId.put(CMForLeads.Id, additParams.CampaignId);
        }
        for (Contact CMForContact: contacts){
            additParams = mapContactToAddParams.get(CMForContact.Id);
            ContactIds.add(CMForContact.Id);
            MapContactToCampaignId.put(CMForContact.Id, additParams.CampaignId);
        }
        system.debug('MapContactToCampaignId and contactid' + MapContactToCampaignId + ',' + ContactIds);
        if (LeadIds.size() > 0){
            for (CampaignMember CM : [SELECT Id, Number_Of_Form_Submissions__c,CampaignId,LeadId,ContactId FROM CampaignMember WHERE LeadId In :LeadIds])
            {
                if (PMapLIdToCamaignMember.containskey(CM.LeadId)){
                    PMapLIdToCamaignMember.get(CM.LeadId).add(CM);
                }else {PMapLIdToCamaignMember.put(CM.LeadId,new list<CampaignMember>{CM});}
                
            }system.debug('parent Map' + PMapLIdToCamaignMember);
            for (Id LID: LeadIds){
                try {
                    if (PMapLIdToCamaignMember.containsKey(LID)){
                        List<CampaignMember> MatchedCMRecord = PMapLIdToCamaignMember.get(LID);
                        String MatchedCMIdFromAddParams = String.isNotBlank(MapLeadToCampaignId.get(LID)) ? MapLeadToCampaignId.get(LID) : '';
                        system.debug('CM list' + MatchedCMRecord);
                        Boolean CMCheckFlag = false;
                        for (CampaignMember cml:MatchedCMRecord){
                            if (cml.CampaignId == MatchedCMIdFromAddParams){
                                system.debug('CM matching' + cml.CampaignId + ',' + MatchedCMIdFromAddParams);
                                FoundCampaignMembers.add(cml);
                                CMCheckFlag = true;
                            }
                            system.debug('flag check' + CMCheckFlag);
                        }
                        if (CMCheckFlag == false){LeadsContactsWithoutCampaignMembers.add(LID);}
                    }else {LeadsContactsWithoutCampaignMembers.add(LId);}
                }
                catch (System.Exception ex){
                    MapIdToException.put(LID,ex);
                    system.debug('Campaign member check error -' + ex);
                } 
            }
        }
        if (ContactIds.size() > 0){
            for (CampaignMember CM : [SELECT Id, Number_Of_Form_Submissions__c,CampaignId,ContactId,LeadId FROM CampaignMember WHERE ContactId In :ContactIds])
            {
                if (PMapCIdToCamaignMember.containskey(CM.ContactId)){
                    PMapCIdToCamaignMember.get(CM.ContactId).add(CM);
                }else {PMapCIdToCamaignMember.put(CM.ContactId,new list<CampaignMember>{CM});}
            }
            for (Id CID: ContactIds){
                try{
                    if (PMapCIdToCamaignMember.containsKey(CID)){
                        List<CampaignMember> MatchedCMRecord = PMapCIdToCamaignMember.get(CID);
                        String MatchedCMIdFromAddParams = String.isNotBlank(MapContactToCampaignId.get(CID)) ? MapContactToCampaignId.get(CID) : '';
                        system.debug('CM list' + MatchedCMRecord);
                        Boolean CMCheckFlag = false;
                        for (CampaignMember cml:MatchedCMRecord){
                            if (cml.CampaignId == MatchedCMIdFromAddParams){
                                system.debug('CM matching' + cml.CampaignId + ',' + MatchedCMIdFromAddParams);
                                FoundCampaignMembers.add(cml);
                                CMCheckFlag = true;
                            }
                            system.debug('flag check' + CMCheckFlag);
                        }
                        if (CMCheckFlag == false){LeadsContactsWithoutCampaignMembers.add(CID);}
                    }else {LeadsContactsWithoutCampaignMembers.add(CID);}
                }
                catch (System.Exception ex){
                    MapIdToException.put(CID,ex);
                    system.debug('Campaign member check error -' + ex);
                }
            }
        }
        system.debug('ids for creation of CM' + LeadsContactsWithoutCampaignMembers);
        system.debug('CM for updating' + FoundCampaignMembers);
        
        for(WebActivityUtils.WebActivityContent WAC:lstWAContent){
            if (String.isNotBlank(WAC.oUser.LeadId) && MapIdToException.containsKey(WAC.oUser.LeadId)){
                WAC.status = 'Failed';
                WAC.AuditTrail = WAC.AuditTrail + MapIdToException.get(WAC.oUser.LeadId);
            }
            if(String.isNotBlank(WAC.oUser.ContactId) && MapIdToException.containsKey(WAC.oUser.ContactId)){
                WAC.status = 'Failed';
                WAC.AuditTrail = WAC.AuditTrail + MapIdToException.get(WAC.oUser.ContactId);
            }
        }

        if (!FoundCampaignMembers.isEmpty() && FoundCampaignMembers.size() > 0){
            for (CampaignMember FCM: FoundCampaignMembers){
                if (FCM.Number_Of_Form_Submissions__c == null){
                    FCM.Number_Of_Form_Submissions__c = 1;
                }
                FCM.Number_Of_Form_Submissions__c = FCM.Number_Of_Form_Submissions__c + 1;
                FoundCampaignMembersToUpdate.add(FCM);
            }
        }
        if(!LeadsContactsWithoutCampaignMembers.isEmpty() && LeadsContactsWithoutCampaignMembers.size() > 0)
        {
            for (String Lid: LeadsContactsWithoutCampaignMembers){ 
            CampaignMember CampaignMembersToCreate = new CampaignMember();
            CampaignMembersToCreate.Status = 'Form Fill Out';
            CampaignMembersToCreate.Number_Of_Form_Submissions__c = 1;
                if (Lid.startsWith('00Q')){
                    CampaignMembersToCreate.LeadId = Lid;
                    if(String.isNotBlank(MapLeadToCampaignId.get(Lid))){CampaignMembersToCreate.CampaignId = MapLeadToCampaignId.get(Lid);}
                    else {CampaignMembersToCreate.CampaignId = Label.QSD_Campaign_ID;}
                }
                else if (Lid.startsWith('003')){
                    CampaignMembersToCreate.ContactId = Lid;
                    if(String.isNotBlank(MapContactToCampaignId.get(Lid))){CampaignMembersToCreate.CampaignId = MapContactToCampaignId.get(Lid);}
                    else {CampaignMembersToCreate.CampaignId = Label.QSD_Campaign_ID;}
                }
            ListOfCampaignMembersToCreate.add(CampaignMembersToCreate);
            }
        }
        system.debug('ids for creation ' + ListOfCampaignMembersToCreate);
        //Saving existing campaign members
        try {
            List<Database.SaveResult> saveResults = Database.update(FoundCampaignMembersToUpdate, false);
            for(integer i = 0; i < saveResults.size(); i++){
                String msg = 'Error (Campaign Member Update): ';
                if(saveResults[i].isSuccess()){
                    if (String.isNotBlank(FoundCampaignMembersToUpdate.get(i).LeadId)){
                        mapCMUpdateSaveResult.put(FoundCampaignMembersToUpdate.get(i).LeadId,saveResults[i].getId());
                    } else {
                        mapCMUpdateSaveResult.put(FoundCampaignMembersToUpdate.get(i).ContactId,saveResults[i].getId());
                    }
                }
                else
                {
                    for(Database.Error err: saveResults[i].getErrors()){  
                        msg += err.getmessage() + '\n';
                    }
                    if (String.isNotBlank(FoundCampaignMembersToUpdate.get(i).LeadId)){
                        mapCMUpdateSaveResult.put(FoundCampaignMembersToUpdate.get(i).LeadId, msg);
                    }
                    else {
                        mapCMUpdateSaveResult.put(FoundCampaignMembersToUpdate.get(i).ContactId, msg);
                    }
                }
            }
            system.debug('result map' + mapCMUpdateSaveResult);
            for (WebActivityUtils.WebActivityContent UpdatedCMResult : lstWAContent){
                //String LeadCMResults;
                if (String.isNotBlank(UpdatedCMResult.oUser.LeadId)){
                    String LeadCMResults = mapCMUpdateSaveResult.get(UpdatedCMResult.oUser.LeadId);
                    if (String.isNotBlank(LeadCMResults) && LeadCMResults.startsWith('00v')){
                        if (UpdatedCMResult.status != 'Failed'){
                            UpdatedCMResult.status = 'Completed';
                        }
                    }else if (String.isNotBlank(LeadCMResults) && LeadCMResults.startsWith('Error')){
                        UpdatedCMResult.status = 'Failed';
                        UpdatedCMResult.AuditTrail = UpdatedCMResult.AuditTrail + LeadCMResults;
                    }  
                }else {String ContactCMResults = mapCMUpdateSaveResult.get(UpdatedCMResult.oUser.ContactId);
                      if (String.isNotBlank(ContactCMResults) && ContactCMResults.startsWith('00v')){
                    if (UpdatedCMResult.status != 'Failed'){
                        UpdatedCMResult.status = 'Completed';
                    	}
                      }else if (String.isNotBlank(ContactCMResults) && ContactCMResults.startsWith('Error')){
                          UpdatedCMResult.status = 'Failed';
                          UpdatedCMResult.AuditTrail = UpdatedCMResult.AuditTrail + ContactCMResults;
                      }
                }
            }
        }
        catch (System.Exception ex){
            system.debug('Campaign members failed to update -');
        }
        
        //Inserting new Campaign members
        try {
            List<Database.SaveResult> saveResults = Database.insert(ListOfCampaignMembersToCreate, false);
            for(integer i = 0; i < saveResults.size(); i++){
            	String msg = 'Error (Campaign Member Not created): ';
                if(saveResults[i].isSuccess()){
                    if (String.isNotBlank(ListOfCampaignMembersToCreate.get(i).LeadId)){
                        mapCMUpdateSaveResult.put(ListOfCampaignMembersToCreate.get(i).LeadId,saveResults[i].getId());
                    } else {
                        mapCMUpdateSaveResult.put(ListOfCampaignMembersToCreate.get(i).ContactId,saveResults[i].getId());
                    }
                }
                else
                {
                    for(Database.Error err: saveResults[i].getErrors()){  
                        msg += err.getmessage() + '\n';
                    }
                    if (String.isNotBlank(ListOfCampaignMembersToCreate.get(i).LeadId)){
                        mapCMUpdateSaveResult.put(ListOfCampaignMembersToCreate.get(i).LeadId, msg);
                    }
                    else {
                        mapCMUpdateSaveResult.put(ListOfCampaignMembersToCreate.get(i).ContactId, msg);
                    }
                }
            }
            system.debug('result map' + mapCMUpdateSaveResult);
            for (WebActivityUtils.WebActivityContent CreatedCMResult : lstWAContent){
                String LeadCMResults = mapCMUpdateSaveResult.get(CreatedCMResult.oUser.LeadId);
                if (String.isNotBlank(LeadCMResults) && LeadCMResults.startsWith('00v')){
                    if (CreatedCMResult.status != 'Failed'){
                        CreatedCMResult.status = 'Completed';
                    }
                }else if (String.isNotBlank(LeadCMResults) && LeadCMResults.startsWith('Error')){
                    CreatedCMResult.status = 'Failed';
                    CreatedCMResult.AuditTrail = CreatedCMResult.AuditTrail + LeadCMResults;
                }   
            }
            for (WebActivityUtils.WebActivityContent CreatedCMResult : lstWAContent){
                String ContactCMResults = mapCMUpdateSaveResult.get(CreatedCMResult.oUser.ContactId);
                if (String.isNotBlank(ContactCMResults) && ContactCMResults.startsWith('00v')){
                    if (CreatedCMResult.status != 'Failed'){
                        CreatedCMResult.status = 'Completed';
                    }
                }else if (String.isNotBlank(ContactCMResults) && ContactCMResults.startsWith('Error')){
                    CreatedCMResult.status = 'Failed';
                    CreatedCMResult.AuditTrail = CreatedCMResult.AuditTrail + ContactCMResults;
                }
            }
        }
        catch (System.Exception ex){
            system.debug('Campaign members creation failed -');
        }
        system.debug('return list' + lstWAContent);
        return lstWAContent;
    }

    @TestVisible
    private static List<WebActivityUtils.WebActivityContent> PopulateZiftParamsToList(List<WebActivityUtils.WebActivityContent> lstWAContent) {

        for(WebActivityUtils.WebActivityContent wa : lstWAContent)
        {
             wa.oUser.ziftpartnerID = '';
             wa.oUser.ziftpartnername = '';                

            if(String.isNotBlank(wa.additParams.ZiftData))
            {
                String DataToParse = 0 == wa.additParams.ZiftData.indexOf('~') ? wa.additParams.ZiftData.substring(1) : wa.additParams.ZiftData;
                List<string> MyValues = DataToParse.split('~');

                if (math.mod(MyValues.size(), 2) == 1) 
                {                                      
                    DataToParse += '~';
                    MyValues = DataToParse.Split('~');
                }

                for (Integer i = 0; i < MyValues.size(); i += 2)
                {
                    string Key = MyValues[i];
                    string Val = MyValues[i + 1];

                    switch on Key.toUpperCase() 
                    {
                        when 'ZIFTPARTNERID'    {wa.oUser.ziftpartnerID = Val;}
                        when 'ZIFTPARTNERNAME'  {wa.oUser.ziftpartnername = Val;}
                    }
                }   
            }
        }
        return lstWAContent;
    }
    @TestVisible
    private static List<WebActivityUtils.WebActivityContent> GetLeadOrContact(List<WebActivityUtils.WebActivityContent> lstWAContent) {
        List<String> lstUsernames = new List<String>();
        Map<String, Integer> mapUsernameToWAIndex = new Map<String, Integer>();
        Integer i = 0;

        for(WebActivityUtils.WebActivityContent wa : lstWAContent)
        {
            lstUsernames.add(wa.oUser.UserName);
            mapUsernameToWAIndex.put(wa.oUser.UserName, i);
            i++;
        }
		system.debug('ulc map' + mapUsernameToWAIndex);
        system.debug('ulc map' + lstUsernames);
        for (ULC_Details__c ulc : [select Id, ULCName__c, ContactId__c, LeadId__c from ULC_details__c where ULCName__c in :lstUsernames]) 
        {
            i = mapUsernameToWAIndex.get(ulc.ULCName__c);
            if (String.isNotBlank(ulc.LeadId__c)){lstWAContent[i].oUser.LeadID = ulc.LeadId__c;}
            else {lstWAContent[i].oUser.ContactID = ulc.ContactId__c;}
        }


        return lstWAContent;
    }
}