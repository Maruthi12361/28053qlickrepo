/* 
Purpose: EP_QlikExpertiseProgram Test
Created Date: 8 Jan 2016
Initial delvelpment MTM 8 Jan 2016
Change log:
 **/
@isTest
public class EP_QlikExpertiseProgramTest {

    @testSetup static void testsetup() {
        QuoteTestHelper.createCustomSettings();
    }

Static Account account { get; set; }
    Static Partner_Expertise_Area__c ExpertiseArea {get; set;}
    Static Partner_Expertise_Application_Lookup__c ExpertLookup {get; set;} 
    Static User testUser {get; set;}
    Static Contact testContact {get; set;}

    static testmethod void test_PEVisibility() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_QlikExpertiseProgram controller = new EP_QlikExpertiseProgram();            
            System.assertEquals(false, controller.PEVisibility);    
        }
        Test.stopTest();
    }
    
    static testmethod void test_InitProfileSettings() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_QlikExpertiseProgram controller = new EP_QlikExpertiseProgram();            
            System.assertEquals(false, controller.isPRMBase);
            System.assertEquals(false, controller.ExpertiseAreaExists());
        }
        Test.stopTest();
    }
    
    public Static void Setup()
    {                    
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        
        insert qtc;
        
        //Create Account
        account = TestDataFactory.createAccount('Test AccountName', qtc);
        insert account;
         
		//Create Contacts
		testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', account.Id);
        insert testContact;
        
        // Retrieve Customer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'PRM - Sales Dependent Territory + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

		// Create User
		testUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert testUser;
    }
}