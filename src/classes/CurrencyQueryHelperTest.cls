/**
* CurrencyQueryHelperTest
* 06.02.2017   RVA :   changing QT methods
* 29-03-2017   Roman Dovbush : removing hardcoded rec type IDs.
 */
@isTest
private class CurrencyQueryHelperTest {
	static Id AccRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id; //'01220000000DOFu';  //End User Account
	@isTest static void test_getCurrenciesByISOCodes() {
		QTTestUtils.GlobalSetUp();
        //QTTestUtils.SetupNSPriceBook('GBP');
        List<String> currencyList = new List<String>();
        List<CurrencyType> returnCurencyList = new List<CurrencyType>();
		User objUser = QTTestUtils.createMockOperationsAdministrator();
        Id oppSalesQCCS_recType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id; // in QA '012260000000YTi'
        /*
		Profile p = [select id from profile where name='Standard User'];
        User objUser = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert objUser;
		*/
        System.assertNotEquals(null, objUser.Id);
        
        System.RunAs(QTTestUtils.createMockOperationsAdministrator()) {  
		//CCE  Adding an account with BillingCountryCode and Qliktech Company
		/*
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
	            Name = 'GBR',
	            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
	        );
	        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
	        insert QTComp;
	        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
	            
	        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId='01220000000DOFzAAO', Pending_Validation__c = false);
	        insert acc;
	     */      
		 User testUser = [Select id From User where id =: UserInfo.getUserId()];
		Account  testAcnt = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAcnt.RecordtypeId = AccRecordTypeId;
	        update testAcnt; 
	        // create the opportunity
	        Opportunity opp1 = new Opportunity(
	            name='Test Opp 1',
	            recordtypeid=oppSalesQCCS_recType,
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'GBP',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = testAcnt.Id,
	            Finance_Approved__c = false        
	        );
	        insert opp1;
	        System.assertNotEquals(null, opp1.Id);

			// create the opportunity
	        Opportunity opp2 = new Opportunity(
	            name='Test Opp 2',
	            recordtypeid=oppSalesQCCS_recType,
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'USD',
	            CloseDate = System.today(),
	            Signature_Type__c = 'Digital Signature',
	            AccountId = testAcnt.Id,
	            Finance_Approved__c = false    
	        );
	        insert opp2;
	        System.assertNotEquals(null, opp1.Id);

            Test.startTest();
            currencyList.add(opp1.CurrencyIsoCode); 
            currencyList.add(opp2.CurrencyIsoCode);  

            returnCurencyList = CurrencyQueryHelper.getCurrenciesByISOCodes(currencyList);
            System.assertEquals(2, returnCurencyList.size());
       		System.assertEquals('GBP', returnCurencyList[0].IsoCode);
       		System.assertEquals('USD', returnCurencyList[1].IsoCode);
       		returnCurencyList.clear();
       		returnCurencyList = CurrencyQueryHelper.getCurrenciesByISOCodes(currencyList, true);
       		System.assertEquals('GBP', returnCurencyList[0].IsoCode);
       		System.assertEquals('USD', returnCurencyList[1].IsoCode);

            Test.stopTest();
        }//RunAs
	}
	
	@isTest static void test_getCurrencyByISOCode() {
		QTTestUtils.GlobalSetUp();
        //QTTestUtils.SetupNSPriceBook('GBP');
		User objUser = QTTestUtils.createMockPRMIndTerritoryPartnerUser();
        Id oppSalesQCCS_recType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id; // in QA '012260000000YTi'
		/*
        Profile p = [select id from profile where name='PRM - Sales Dependent Territory + QlikBuy'];
        User objUser = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert objUser;
		*/
        System.assertNotEquals(null, objUser.Id);
        
        //System.RunAs(objUser) {  
        //CCE  Adding an account with BillingCountryCode and Qliktech Company
		/*
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
	            Name = 'GBR',
	            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
	        );
	        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
	        insert QTComp;
	        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
	            
	        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId='01220000000DOFzAAO', Pending_Validation__c = false);
	        insert acc;
	        */
			User testUser = [Select id From User where id =: UserInfo.getUserId()];
			Account  testAcnt = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAcnt.RecordtypeId = AccRecordTypeId;
	        update testAcnt;    
	        // create the opportunity
	        Opportunity opp1 = new Opportunity(
	            name='Test Opp 1',
	            recordtypeid=oppSalesQCCS_recType,
				Revenue_Type__c = 'Direct',
	            StageName = 'Goal Identified',
	            CurrencyIsoCode = 'GBP',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = testAcnt.Id,
	            Finance_Approved__c = false        
	        );
	        insert opp1;
	        System.assertNotEquals(null, opp1.Id);

			// create the opportunity
	        Opportunity opp2 = new Opportunity(
	            name='Test Opp 2',
	            recordtypeid=oppSalesQCCS_recType,
				Revenue_Type__c = 'Direct',
	            StageName = 'Goal Identified',
	            CurrencyIsoCode = 'USD',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = testAcnt.Id,
	            Finance_Approved__c = false      
	        );
	        System.debug('--opp1' + opp1);
            Test.startTest();

            CurrencyType returnCurency = CurrencyQueryHelper.getCurrencyByISOCode(opp1.CurrencyIsoCode);
       		System.assertEquals('GBP', returnCurency.IsoCode);
       	
       		//delete opp1;
       		//System.assertEquals(null, opp1.Id);
       		CurrencyType returnCurency1 = CurrencyQueryHelper.getCurrencyByISOCode('abcde');
       		System.assertEquals(null, returnCurency1);

            Test.stopTest();
        //}//RunAs
	}
	
}