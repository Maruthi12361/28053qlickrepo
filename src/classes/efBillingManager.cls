//*********************************************************/
// Author: Mark Cane&
// Creation date: 18/08/2010
// Intent:  Provides billing calculation/
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efBillingManager{	
	private List<efProductWrapper> registrationProducts;
    private List<efProductWrapper> trainingProducts;
    private List<efProductWrapper> promotionProducts;
    private List<efProductWrapper> addOnProducts;
    private List<BillItem> paymentItems;
       
    
    // Intialise methods.
    
    public efBillingManager(List<efProductWrapper> registrationProducts,
                          List<efProductWrapper> trainingProducts,
                          List<efProductWrapper> promotionProducts, 
                          List<efProductWrapper> addOnProducts){
    	reset(registrationProducts,
    	      trainingProducts,
    	      promotionProducts,
    	      addOnProducts);
    }
    
    public void reset(List<efProductWrapper> registrationProducts,
                      List<efProductWrapper> trainingProducts,
                      List<efProductWrapper> promotionProducts,
                      List<efProductWrapper> addOnProducts){
        this.registrationProducts = registrationProducts;
        this.trainingProducts = trainingProducts;
        this.promotionProducts = promotionProducts;
        this.addOnProducts = addOnProducts;
    }
    // End Intialise methods.

    
    // Payment calculation methods.
    
    public Double getTotalRegistrationAmount(){     	
    	Double totalRegistrationAmount = 0;
        
        if(registrationProducts!=null && registrationProducts.size()>0){
            PricebookEntry[] pb = registrationProducts[0].getProduct().PricebookEntries;
            
            if(pb!=null && pb.size()>0 && pb[0].UnitPrice!=null){
                totalRegistrationAmount=pb[0].UnitPrice;    
            }
        }
        return totalRegistrationAmount;
    }
	
	public Double getTotalClassesAmount(){
		Double totalClassesAmount = 0;
							
		if(trainingProducts!=null && trainingProducts.size()>0){
			for(efProductWrapper p:trainingProducts){
				if(p.getIsSelected() && !p.getIsClassFull()){
					PriceBookEntry[] pb = p.getProduct().PriceBookEntries;
					if(pb!=null && pb.size()>0) 
						totalClassesAmount+=pb[0].UnitPrice; 
				}					
			}
		}
		return totalClassesAmount;		 
	}
	
	public Double getTotalDiscountAmount(){
		Double totalDiscountAmount = 0;
		
		for(efProductWrapper p:promotionProducts){
			totalDiscountAmount+=double.valueOf(p.getCalculatedDiscountAmount());
		}
		return totalDiscountAmount;
	}
	
    public Double getTotalAddOnAmount(){
        Double total = 0;

		if(addOnProducts!=null && addOnProducts.size()>0){
			for(efProductWrapper p:addOnProducts){
            	PricebookEntry[] pbe = p.getProduct().PricebookEntries;
	            if(pbe!=null && pbe.size()>0){
	                total += pbe[0].UnitPrice;
	            }
			}
        }
        return total;
    }
	
	public Double getTotalPaymentAmount(){ return getSubTotalPaymentAmount()+getTotalDiscountAmount(); }
	
	public Double getSubTotalPaymentAmount(){ return getTotalClassesAmount()+getTotalRegistrationAmount()+getTotalAddOnAmount(); }
	
    // End payment calculation methods.
    
    
    // Billing methods.
    
    private Double aggregatePrices(List<efProductWrapper> products, Boolean excludeFull){
    	Double totalPrice = 0;
    	
    	for (efProductWrapper pi:products){
    		if (!excludeFull || !pi.getIsClassFull()){
    			totalPrice += getPrice(pi);
    		}
    	}    	
    	return totalPrice;
    }
    
    private Double aggregatePrices(List<efProductWrapper> products){
    	return aggregatePrices(products, false);
    }
    
    private Double getPrice(efProductWrapper pi){
        PricebookEntry[] pbe = pi.getProduct().PricebookEntries;

        if(pbe!=null && pbe.size()>0){
            return pbe[0].UnitPrice;
        }
        else{
        	return 0;
        }
    }
    
    private String formatAmount(Double amount){
    	String formatted = efUtility.formatDecimals(String.valueOf(amount));
    	
    	if (amount != null && amount < 0){
    		formatted = '(' + formatted + ')';
    	}    	
    	return formatted;
    }
    
    public void generateBill(){
        List<BillItem> resultItems = new List<BillItem>();
        List<BillItem> productItems = new List<BillItem>();
        List<BillItem> discountItems = new List<BillItem>();
        List<BillItem> addOnItems = new List<BillItem>();
        
        if (registrationProducts.size() > 0){
	        productItems.add(new BillItem(registrationProducts[0].getName(),
	                                      formatAmount(aggregatePrices(registrationProducts)),
	                                      null,
	                                      registrationProducts[0].getProduct().Id,
	                                      registrationProducts[0].getProduct().ProductCode,
	                                      false));
        }
        
        if (aggregatePrices(trainingProducts) > 0){
        	productItems.add(new BillItem('Trainings',
                                          formatAmount(aggregatePrices(trainingProducts, true)),
                                          null,
                                          null,
                                          null,
                                          false));
        }
        
        for (efProductWrapper pi:promotionProducts){
            discountItems.add(new BillItem(pi.getName(),
                                           pi.getFormatedDiscountAmount(),
                                           null,
                                           pi.getProduct().Id,
                                           pi.getProduct().ProductCode,
                                           true));
    	}               
    	
    	for (efProductWrapper pi:addOnProducts){
            addOnItems.add(new BillItem(pi.getName(),
                                        formatAmount(getPrice(pi)),
                                        null,
                                        pi.getProduct().Id,
                                        pi.getProduct().ProductCode,
                                        false));
        }    	    	
        resultItems.addAll(productItems);
        resultItems.addAll(discountItems);
        resultItems.addAll(addOnItems);
        paymentItems = resultItems;
    }
    
    public List<BillItem> getPaymentItems(){
        return paymentItems;
    }    
    // End billing methods.

	
	// Inner classes.

    public class BillItem{
        private String amount;
        private String name;
        private List<BillItem> subItems;
        private Boolean isExpanded = false;
        private Id productId;
        private Boolean isPromoCode;
        private String productCode;
        
        public BillItem(String name,
                        String amount,
                        List<BillItem> subItems,
                        Id productId,
                        String productCode,
                        Boolean isPromoCode){
            this.amount = amount;
            this.name = name;
            this.subItems = subItems;
            this.productId = productId;
            this.isPromoCode = isPromoCode;
            this.productCode = productCode;
        }
        
        public String getProductCode(){
        	return productCode;
        }
        
        public void setProductCode(String productCode){
        	this.productCode = productCode;
        }
        
        public Boolean getIsPromoCode(){
        	return isPromoCode;
        }
        
        public void setIsPromoCode(Boolean isPromoCode){
        	this.isPromoCode = isPromoCode;
        }
        
        public Id getProductId(){
        	return productId;
        }
        
        public void setProductId(Id productId){
        	this.productId = productId;
        }
        
        public String getAmount(){
            return amount;
        }
        
        public String getName(){
            return name;
        }
        
        public List<BillItem> getSubItems(){
            return subItems;
        }
        
        public Boolean getHasSubItems(){
            return subItems != null && subItems.size() > 0;
        }
        
        public Boolean getIsExpanded(){
            return isExpanded;
        }
        
        public void expand(){
            isExpanded = true;
        }
        
        public void collapse(){
        	isExpanded = false;
        }        
	}
	// End Inner classes.
}