/**     
 * File Name : Z_RenewalOpportunityJobTest
 * Description : This is a test class to test the Z_RenewalOpportunityJob class with following test methods:
 *               - renewalOpportunityJobSchedulerTest
 * @author : Inki Hong
 * Modification Log ====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

@IsTest
private class Z_RenewalOpportunityJobTest {

    private static Account testAcc;

    private static Contact testContact;

    private static Zuora__CustomerAccount__c testCustomerAcc;

    private static Opportunity testOppty;

    private static Zuora__Subscription__c testSubscription;

    private static zqu__Quote__c testQuote;

    private static QlikTech_Company__c testqtComp;

    private static Q2CWSettings__c testQ2cwSettings;

    private static Steelbrick_Settings__c testSteelbrickSettings;

    private static Renewal_Automation_Settings__c testRASettings;

    private static Apex_Debug_Log__c testApexDebugLog;

    static void setup() {
        testQ2cwSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();
        testRASettings = Z_RenewalTestFactory.settings;
        testApexDebugLog = Z_RenewalTestFactory.makeApexDebugLogForRenewalOpp();

        testqtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');

        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOppty = Z_RenewalTestFactory.makeRenewalOpportunity(testAcc);
        testSubscription = Z_RenewalTestFactory.makeSubscription();

        testAcc.QlikTech_Company__c = testqtComp.name;
        testAcc.Billing_Country_Code__c  = testqtComp.id;

        List<sObject> tests = new List<sObject>{
            testQ2cwSettings, testSteelbrickSettings, testRASettings, testApexDebugLog
        };

        insert tests;

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOppty, testSubscription
        };

        insert testObjects;

        List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();

        for (Integer i = 0; i < 5; i++) {
            ocrList.add(Z_RenewalTestFactory.makeOCR(testContact, testOppty));
        }

        insert ocrList;

        testQuote = Z_RenewalTestFactory.makeQuote(testAcc, testContact, testOppty, testCustomerAcc);

        System.debug('IH: quote id = ' + testQuote.Id);

        Organization_Info__c orgInfo = new Organization_Info__c(isSandbox__c=true);
        insert orgInfo;

        orgInfo.CreatedInOrgId__c = orgInfo.Id;
        update orgInfo;
    }

    static void negativeSetup() {
        testQ2cwSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();
        testRASettings = Z_RenewalTestFactory.settings;

        testqtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');

        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOppty = Z_RenewalTestFactory.makeRenewalOpportunity(testAcc);
        testSubscription = Z_RenewalTestFactory.makeSubscription();

        testAcc.QlikTech_Company__c = testqtComp.name;
        testAcc.Billing_Country_Code__c  = testqtComp.id;

        List<sObject> tests = new List<sObject>{
            testQ2cwSettings, testSteelbrickSettings, testRASettings
        };

        insert tests;

        testOppty.Next_Renewal_Opportunity__c = 'non existing oppty';

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOppty, testSubscription
        };

        insert testObjects;

        List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();

        for (Integer i = 0; i < 5; i++) {
            ocrList.add(Z_RenewalTestFactory.makeOCR(testContact, testOppty));
        }

        insert ocrList;

        testQuote = Z_RenewalTestFactory.makeQuote(testAcc, testContact, testOppty, testCustomerAcc);

        System.debug('IH: quote id = ' + testQuote.Id);

        Organization_Info__c orgInfo = new Organization_Info__c(isSandbox__c=true);
        insert orgInfo;

        orgInfo.CreatedInOrgId__c = orgInfo.Id;
        update orgInfo;
    }

    @IsTest
    static void renewalOpportunityJobSchedulerTest() {
        Semaphores.SetAllSemaphoresToTrue();
        
        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;

        setup();

        Test.startTest();

        Z_RenewalOpportunityJob job = new Z_RenewalOpportunityJob();
        job.run();
        
        Test.stopTest();
    }
}