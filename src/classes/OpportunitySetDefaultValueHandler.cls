/************************************************************************
*
*   Opportunity_SetDefaultValue
*   
*   This trigger will make sure Opportuinitys with empty Value will
*   be updated with the Average Deal Size. Setting set in Custom 
*   Setting.    
*       
*   Changelog:
*   2016-05-19  roman@4front     Migrated from Opportunity_SetDefaultValue.trigger
*   2016-05-20  TJG     Moved here CR# 17441 https://eu1.salesforce.com/a0CD000000lViDU
*                       Make sure the Forecast Category does not include any other 
*                       value than Omitted for OEM Prepay opportunities.
*   2016-06-23  roman@4front     Query for CurrencyType is replaced with CurrencyQueryHelper class with the query inside it.
*   2017-05-17  MTM Set QCW-1633 OEM End user account for OEM rec deal
*   2017-09-13  QCW-3804 Nikhil Jain - Populating the subscription start date based on CloseDate for MSP
*	2017-10-08	Oleksandr@4Front commenting out UpdateOEMSubscriptionOnQuote method; QCW-4073
*	2017-11-1	QCW-2948 Sergii Grushai - fix populating the Type (New/Existing Customer) based on Account assets, added condition "Eval__c = false"
*   2019-1-9    Shubham Gupta BSL-1261 check New customer flag when opportunity is created on prospect account
*   2019-2-22    Anjuna BSL-1615 When the account status is blank and the domestic ultimate duns is blank, the opportunity New Customer flag should be checked off (Removed the code that is setting New Customer flag in given condition)
*   2018-02-20  BSL-1481 Dhivya nagarajan - including subscriptions for identifying existing customers changes in SetOpportunityType method

*************************************************************************/

public class OpportunitySetDefaultValueHandler {
    private static final String CSNAME = 'SteelbrickSettingsDetails';
    public Static boolean IsBeforeUpdateProcessing = false;
    public OpportunitySetDefaultValueHandler() {        
    }

    public static void BeforeUpdate(List<Opportunity> newList, Map<ID, Opportunity> oldMap)
    {
        system.debug('#####Debug 4######');
        if(!IsBeforeUpdateProcessing)
        {
            IsBeforeUpdateProcessing = true;
            //UpdateOEMSubscriptionOnQuote(newList, oldMap);
        }
    }

    public static void handle(List<Opportunity> triggerNew) {
        Set<Account> resultedAcc = new Set<Account>();
        List<Account> acctoupdate = new List<Account>();
        Set<id> acctsid = new Set<id>();
        Steelbrick_Settings__c settingQuote = Steelbrick_Settings__c.getInstance(CSNAME);
        String settingPriceBook = settingQuote.Standard_PriceBook__c;
        for (Opportunity opp : triggerNew)
        {     
            opp.Pricebook2Id = settingPriceBook;
            opp.SBQQ__QuotePricebookId__c = settingPriceBook;
            acctsid.add(opp.AccountId);
			//QCW-3804 This will be called on BeforeInsert event
            //moved to SBQQQUoteSetFields; QCW-4073
            /*            
            if(opp.Revenue_Type__c != NULL && opp.Revenue_Type__c != '' && opp.Revenue_Type__c.containsIgnoreCase('MSP')){
                opp.Subscription_Start_Date__c = opp.CloseDate.toStartOfMonth();
            }
			*/
        }
        //BSL-1261 changes start
        Map<id,Account> idtoAccount = new Map<id,Account>([select id,AccountStatus__c,Domestic_Ultimate_DUNS__c from Account where id in :acctsid AND Domestic_Ultimate_DUNS__c != '000000000']);
        if(!idtoAccount.isEmpty()){
        for(Opportunity oppty:triggerNew){
            if(idtoAccount.containsKey(oppty.AccountID)){
                if(idtoAccount.get(oppty.AccountID).AccountStatus__c == null && !String.isEmpty(idtoAccount.get(oppty.AccountID).Domestic_Ultimate_DUNS__c)){
                    oppty.New_Customer__c = true;
                    idtoAccount.get(oppty.AccountID).AccountStatus__c = 'Prospect';
                    resultedAcc.add(idtoAccount.get(oppty.AccountID));
                }
                if(idtoAccount.get(oppty.AccountID).AccountStatus__c == 'Prospect'){
                oppty.New_Customer__c = true;
                }
            }
        }
        acctoupdate.addAll(resultedAcc);
        if(!acctoupdate.isEmpty())
        Update acctoupdate;
        //changes end
    }
    } 
    public static void SetOEMEndUserAccount(List<Opportunity> triggerNew)
    {
        for (Opportunity opp : triggerNew)
        {     
            System.debug('Setting end user acct mmmm' + opp.AccountId);
            if(opp.Revenue_Type__c == 'OEM Recruitment')
                opp.End_User_Account_del__c = opp.AccountId;
            System.debug('Set end user acct mmmm' + opp.End_User_Account_del__c);
        }
    }
	/*    
    public static void UpdateOEMSubscriptionOnQuote(List<Opportunity> newList, Map<ID, Opportunity> oldMap)
    {
        //map<Id,SBQQ__Quote__c> quotToUpdate =new map<Id,SBQQ__Quote__c>();
        system.debug('#####Debug 5######');
        List <SBQQ__Quote__c> quotToUpdate = new List <SBQQ__Quote__c>();
        for (Opportunity newRec: newList)
        {
            Opportunity oldRec = oldMap.get(newRec.Id);
            if(newRec.SBQQ__PrimaryQuote__c != null && (newRec.Revenue_Type__c.containsIgnoreCase('OEM') || newRec.Revenue_Type__c.containsIgnoreCase('MSP')) &&
                (
                    oldRec.No_of_Subscription_Units__c != newRec.No_of_Subscription_Units__c ||
                    oldRec.Subscription_Start_Date__c != newRec.Subscription_Start_Date__c ||
                    oldRec.SBQQ__PrimaryQuote__c != newRec.SBQQ__PrimaryQuote__c
                )
            )
            {
                newRec.Subscription_Changed__c = true;
                SBQQ__Quote__c quote =new SBQQ__Quote__c(Id= newRec.SBQQ__PrimaryQuote__c);
                if(newRec.Subscription_Start_Date__c != null)
                    quote.SBQQ__StartDate__c = newRec.Subscription_Start_Date__c.toStartOfMonth();
                System.debug('quote.SBQQ__StartDate__c =' + quote.SBQQ__StartDate__c);
                if(newRec.No_of_Subscription_Units__c != null)
                    quote.SBQQ__SubscriptionTerm__c = Integer.valueOf(newRec.No_of_Subscription_Units__c);
                quotToUpdate.Add(quote);
            }
            
            //QCW-3804- This will be called on Before Update event
            //system.debug('#####Old Date######'+oldRec.CloseDate);
            system.debug('#####New Date######'+newRec.CloseDate);
            if(newRec.Revenue_Type__c != NULL && newRec.Revenue_Type__c != '' && newRec.Revenue_Type__c.containsIgnoreCase('MSP') && oldRec.CloseDate != newRec.CloseDate){
                newRec.Subscription_Start_Date__c = newRec.CloseDate.toStartOfMonth();
                system.debug('#####Debug 6######');
            }
            system.debug('#####Debug 7######');
        }
        if(quotToUpdate.Size() > 0)
        {
            System.debug('1111');
            update quotToUpdate;
        }
    }
	*/
    
                /*
                SBQQ__Quote__c quote;
                List<SBQQ__Quote__c> quotes = [select Id, SBQQ__StartDate__c,SBQQ__SubscriptionTerm__c from SBQQ__Quote__c where Id = : newRec.SBQQ__PrimaryQuote__c];
                if(quotes.size() > 0)
                {
                    quote = quotes[0];
                }
                if(newRec.Subscription_Start_Date__c != null)
                    quote.SBQQ__StartDate__c = newRec.Subscription_Start_Date__c.toStartOfMonth();
                System.debug('quote.SBQQ__StartDate__c =' + quote.SBQQ__StartDate__c);
                if(newRec.No_of_Subscription_Units__c != null)
                    quote.SBQQ__SubscriptionTerm__c = Integer.valueOf(newRec.No_of_Subscription_Units__c);
                if(!quotToUpdate.ContainsKey(quote.Id))
                    quotToUpdate.put(quote.id,quote);

            }
        }
        if(quotToUpdate.Size() > 0)
        {
            System.debug('1111');
            update quotToUpdate.Values();
            }
            */
    
/*
        decimal Val;
        Map<string, decimal> IsoToConvert = new Map<string, decimal>();
        List<String> isoCodes = new List<String>();
        List<CurrencyType> currencyList = new List<CurrencyType>();
     
        System.Debug('Starting Opportunity_SetDefaultValue');
        ID ID_CCSDealSplitChild = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('CCS Deal Split Child').getRecordTypeId();
        System.debug('ID_CCSDealSplitChild is: ' + ID_CCSDealSplitChild);
        ID ID_CCSDealSplitParent = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('CCS Deal Split Parent').getRecordTypeId();
        System.debug('ID_CCSDealSplitParent is: ' + ID_CCSDealSplitParent);
        ID ID_DealSplitChild = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Deal Split Child').getRecordTypeId();
        System.debug('ID_DealSplitChild is: ' + ID_DealSplitChild);
        ID ID_DealSplitParent = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Deal Split Parent').getRecordTypeId();
        System.debug('ID_DealSplitParent is: ' + ID_DealSplitParent);
        
        //CCS Deal Split Child = 01220000000I4jf
        //CCS Deal Split Parent = 01220000000I4kJ
        //Deal Split Child = 01220000000DoEj
        //Deal Split Parent = 01220000000Dmf5
        
        // EDIT 19.09.2012 : Exclude Academic Program Opportunity records from this trigger, Id from Custom Setting
        Id academidProgramOpportunityId;
        if(Academic_Program_Settings__c.getInstance('AcademicProgramSetting') != null) {
            academidProgramOpportunityId = Academic_Program_Settings__c.getInstance('AcademicProgramSetting').OpportunityRecordTypeId__c;
        }
        
         CR# 17441 
        QTCustomSettings__c mySetting = QTCustomSettings__c.getValues('Default');
        String recordTypeIds = null == mySetting ? null : mySetting.RecordTypesForecastOmitted__c;
        if (null == recordTypeIds)
        {
            recordTypeIds = '01220000000DNwZAAW';
        }
        for (Opportunity opp : triggerNew)
        {   
            if (null != opp.RecordTypeId && recordTypeIds.contains(opp.RecordTypeId))
            {
                opp.ForecastCategoryName = 'Omitted';
            }
        }    
         End of CR# 17441 

        for(Opportunity opp : triggerNew) {
            isoCodes.add(opp.CurrencyIsoCode);
        }
        //2016-06-23  roman@4front   Query for CurrencyType is replaced with CurrencyQueryHelper class with the query inside it.
        if(!isoCodes.isEmpty()) {
            currencyList = CurrencyQueryHelper.getCurrenciesByISOCodes(isoCodes);
        }

        for (Opportunity Opp : triggerNew)
        {   
            if ((Opp.RecordTypeId == ID_CCSDealSplitChild) || (Opp.RecordTypeId == ID_CCSDealSplitParent) || (Opp.RecordTypeId == ID_DealSplitChild) || (Opp.RecordTypeId == ID_DealSplitParent))
            {
                System.Debug('Split deal found so skip');
                continue;
            }
            if(academidProgramOpportunityId != null && Opp.RecordTypeId == academidProgramOpportunityId){
                System.Debug('Academic Program Opportunity found so skip');
                continue;
            }
            
            //if its a new/update opp and Amount is null set it to the avg of the region
            //TODO: If is an update and does not have prod and have non lic related prod set amount to 0
            //Num_Prod = [select Id ProductsLineItem]
            //If Only vouchers on quote
            System.Debug('-------------------');
            System.Debug('Opp.Count_of_Opportunity_Products__c:' + Opp.Count_of_Opportunity_Products__c);
            System.Debug('Opp.Count_of_NLRP:' + Opp.Count_of_Non_License_Related_Products__c);
            System.Debug('Opp.Amount:' + Opp.Amount);        
            System.Debug('-------------------');
            if(Opp.Count_of_Opportunity_Products__c + Opp.Count_of_Non_License_Related_Products__c == 0)
            {
                if (Opp.Amount != 0)
                {
                    System.Debug('Opp.Amount is not 0 so skip');
                    continue;               
                }
                
                //QTCustomSettings__c MySetting = QTCustomSettings__c.getValues('Default');
                if (MySetting == null)
                {
                    System.Debug('No QTCustomSettings__c found');
                    continue;               
                }   
                if (opp.Revenue_Type__c == 'Direct')
                {
                    System.Debug('opp.Revenue_Type__c =  Direct');
                    Val  = MySetting.Opp_Default_License_Amount_For_Direct__c;
                }
                else if (opp.Revenue_Type__c == 'Reseller')
                {
                    System.Debug('opp.Revenue_Type__c = Reseller');
                    Val = MySetting.Opp_Default_License_Amount_For_Reseller__c;
                }
                else if (opp.Revenue_Type__c == 'Direct Sense')
                {
                    System.Debug('opp.Revenue_Type__c =  Direct Sense');
                    // 2014-08-19 we are still sharing Clikview default amount
                    Val  = MySetting.Opp_Default_License_Amount_For_Direct__c;
                }
                else if (opp.Revenue_Type__c == 'Reseller Sense')
                {
                    System.Debug('opp.Revenue_Type__c = Reseller Sense');
                    // 2014-08-19 we are still sharing Clikview default amount
                    Val = MySetting.Opp_Default_License_Amount_For_Reseller__c;
                }            
                else
                {
                    // Dont populate the value field if not Direct or Reseller
                    System.Debug('Not Direct or Reseller');
                    continue;
                }
                
                if (Opp.CurrencyIsoCode != 'USD')
                {
                    if (IsoToConvert.isEmpty())
                    {
                        //2016-06-23  roman@4front   Query for CurrencyType is replaced with CurrencyQueryHelper class with the query inside it.
                        for (CurrencyType c : currencyList) {
                            //[Select IsoCode, ConversionRate From CurrencyType])
                            IsoToConvert.put(c.IsoCode, c.ConversionRate);
                        }
                    }   
                    
                    if (IsoToConvert.containsKey(Opp.CurrencyIsoCode))
                    {
                        Opp.Amount = Val * IsoToConvert.get(Opp.CurrencyIsoCode);
                    }
                }
                else
                {
                    Opp.Amount = Val;
                }
                System.Debug('Calculated Opp.Amount =' + Opp.Amount);
                       
            }
            else if (Opp.Count_of_Opportunity_Products__c > 0.01)
            {
                Opp.Amount = 0;
            }
            
        }
        System.Debug('Finishing Opportunity_SetDefaultValue');
    }
    */
	
	
	//Start -- QCW-2948
    public static void SetOpportunityType(List<Opportunity> triggerNew){
        List<String> lstAccountIds = new List<String>();
        Map<String,Integer> mapAccountVsAssets = new Map<String,Integer>();
        Map<String,Integer> mapAccountVsSubscriptions = new Map<String,Integer>();

        for (Opportunity opp : triggerNew){     
            lstAccountIds.add(opp.AccountId);
        }
        
        for(AggregateResult aggr: [SELECT End_User__c, count(Id) FROM Asset WHERE End_User__c in :lstAccountIds AND Eval__c = false group by End_User__c]){
            mapAccountVsAssets.put(string.valueOf(aggr.get('End_User__c')),integer.valueOf(aggr.get('expr0')));
        }

        for(AggregateResult aggr: [SELECT Zuora__Account__c, count(Id) FROM Zuora__Subscription__c WHERE Zuora__Account__c in :lstAccountIds group by Zuora__Account__c]){
            mapAccountVsSubscriptions.put(string.valueOf(aggr.get('Zuora__Account__c')),integer.valueOf(aggr.get('expr0')));
        }
        
        for (Opportunity opp : triggerNew){
            if((mapAccountVsAssets.get(opp.AccountId) != NULL && mapAccountVsAssets.get(opp.AccountId) > 0) || (mapAccountVsSubscriptions.get(opp.AccountId) != NULL && mapAccountVsSubscriptions.get(opp.AccountId) > 0) && opp.Type != 'New Customer'){
                opp.Type = 'Existing Customer';
            }else{
                opp.Type = 'New Customer';
            }
        }
    }
    //End -- QCW-2948
		
    
    public static void handleCloseDateForQuote(List<Opportunity> triggerNew, Map<ID,Opportunity> newMap, Map<ID,Opportunity> oldMap){
		Map<Id, Id> mapQuoteToOpp = new Map<Id, Id>();
        List<Opportunity> listOpp = new List<Opportunity>();
        for(Opportunity op: triggerNew){
            if(op.CloseDate != oldMap.get(op.Id).CloseDate && op.Revenue_Type__c == 'MSP'){
                listOpp.add(op);
            }
        }
        for(Opportunity opp: listOpp){
            if(opp.SBQQ__PrimaryQuote__c != null){
            	mapQuoteToOpp.put(opp.SBQQ__PrimaryQuote__c,opp.Id);
        	}
        }
        List<SBQQ__Quote__c> listQuoteForUpd = new List<SBQQ__Quote__c>();
        List<SBQQ__Quote__c> listQt = ([SELECT Id, Subscription_Start_Date_Editable__c, SBQQ__Type__c
                              			FROM SBQQ__Quote__c
                              			WHERE Id IN: mapQuoteToOpp.keySet()]);
        for(SBQQ__Quote__c qt: listQt){
            if(qt.SBQQ__Type__c != 'Amendment'){
				Id oppId = mapQuoteToOpp.get(qt.Id);
				Opportunity oppo = newMap.get(oppId);
				qt.Subscription_Start_Date_Editable__c = oppo.CloseDate.toStartOfMonth();
            	listQuoteForUpd.add(qt);
            }
		}
        if(!listQuoteForUpd.isEmpty()){
            update listQuoteForUpd;
        }
    }    
}