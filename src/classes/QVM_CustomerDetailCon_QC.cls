/*
@author: Malay Desai, Slalom LLC
@date: 01/24/2019
@description: Controller for the QVM customers detail page in Qlik Commerce Community
*/
public without sharing class QVM_CustomerDetailCon_QC {
    
    private QVM_Settings__c settings {get;set;}
    public QVM_Customer_Products__c customer {get;set;}
    public Id cId {get;set;} //customer ID
    
    public QVM_CustomerDetailCon_QC() {
        
        cId = ApexPages.currentPage().getParameters().get('id');
        
        customer = (cId == null) ? new QVM_Customer_Products__c() : 
            [select Id, Lead_Contact__c, First_Name__c, Last_Name__c, QVM_Product_Name__c, Type__c, Company_Name__c,
            Responsible_Partner__c, Navision_Status_Check_Field__c, QlikTech_Company__c, Shipping_Country_Code__c, 
            Phone__c, Email__c, CreatedDate, Lead__c, Lead__r.FirstName, Lead__r.LastName from QVM_Customer_Products__c where Id = :cId];
        
        settings = QVM_Settings__c.getOrgDefaults();
        
    }
    
    public void convertToLead() {
            
        try {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            
            Lead newLead = new Lead();
            newLead.RecordTypeId = QVM.getRecordType('Lead', settings.Default_Lead_Record_Type__c);
            newLead.setOptions(dmo);
            newlead.Qlikmarket_Originated__c = true;
            newLead.Status = settings.Default_Lead_Status__c;
            newLead.QVM_Customer__c = customer.Id;
            newLead.FirstName = customer.First_Name__c;
            newLead.LastName = customer.Last_Name__c;
            newLead.Phone = customer.Phone__c;
            newLead.Email = customer.Email__c;
            newLead.Company = customer.Company_Name__c;
            newLead.LeadSource = settings.Default_Lead_Source__c;
            newLead.QlikMarket_Originated_Date__c = system.now();
            insert newLead;
            
            customer.Lead__c = newLead.Id;
            update customer;
        }
        catch(Exception e) { ApexPages.addMessages(e); }
    }
    
    public PageReference viewAllCustomers() {
        PageReference p = Page.QVM_Customers_QC;
        return p;
    }
    
    public PageReference viewQlikMarket() {
        PageReference p = Page.QVM_ControlPanelMain_QC;
        return p;
    }
    
}