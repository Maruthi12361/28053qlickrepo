/**
 * 2014-03-14	TJG CR# 9027 https://eu1.salesforce.com/a0CD000000ZxGyw
 *				Verify that validation rules still work after the modifications
 *				17.03.2017 : Rodion Vakulvsokyi 
 * 11-1-2018 Shubham Added bypass rule true to pass validation error 
 * 2020-04-20 Leonardo Grauer - OTQ 399 - Record types deactivation
 */	
@isTest
private class TestOpportunityValidationRulesApi {
    static final String ApiOnlyUserProfileId = '00e20000000zFfpAAE';	//Custom: Api Only User
    static final String OperationAdminProfId = '00e20000001ODnHAAW';	//Operations Administrator
    static final String StandardUserProfieId = '00e20000000yyV1AAI';	// Standard User
    //static final String OppRecordTypeId = '01220000000J1KR'; //Qlikbuy CCS Standard  //Record type Qlikbuy CCS Standard has been deactivated - OTQ-399
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';

    static testMethod void competitorInfoTestAsApiUser(){
    
    	QTCustomSettings__c setting = new QTCustomSettings__c(
    		Name = 'Default', 
    		eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO',
    		OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW'
    	);
	    Insert setting;    	
	    QuoteTestHelper.createCustomSettings();
	   	User apiUser = [select Id from user where ProfileId = :ApiOnlyUserProfileId and alias <> 'qwebs' and IsActive=true and UserRoleId <> null limit 1];		
    	User opeUser = [select Id from user where ProfileId = :OperationAdminProfId and IsActive=true limit 1];
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'United Kingdom',
            CurrencyIsoCode = 'GBP',
            Language__c = 'English',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;        

        Account TestResellerAccount = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Valrules Test Ltd.',
                Sub_Reseller__c = true,
                Navision_Status__c = 'Partner',
                QlikTech_Company__c = 'QlikTech Inc'                
        );

        Account acc = new Account(
            Name = 'Test Val Rule 1',
            RecordTypeId = AccRecordTypeId_EndUserAccount,
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Legal_Approval_Notes__c = 'Some legal stuff',
            Billing_Country_Code__c = QTComp.Id
        );
        List<Account> Accs = new List<Account>();
        Accs.add(acc);
        Accs.add(TestResellerAccount);
        insert Accs;
 
		Contact TestContact = new Contact (
		    AccountId = TestResellerAccount.Id,
		    OwnerId = UserInfo.getUserId(),
		    LastName = 'Wilson-Hemingway',
		    FirstName = 'Daryl',
		    Email = 'dwy@qlikview.com'
		);
        //----------------------------------------------------------
		//Updated on 09-04-20 Record type Qlikbuy CCS Standard has been deactivated - OTQ-399
        
		/*insert TestContact;             

        Test.startTest();
                
        Opportunity Opp = new Opportunity(
        	RecordTypeId = OppRecordTypeId,
        	Name = 'Test Val Rule Opp',
        	CurrencyIsoCode = 'USD',
        	Short_Description__c = 'Partner Reseller',
        	//pse__Is_Services_Opportunity__c = false,
        	//Competitors__c = 0,
        	StageName = 'Champion',
        	CloseDate = Date.today().addDays(7),
        	Type = 'New Customer',
        	AccountId = acc.Id,
        	Sell_Through_Partner__c = TestResellerAccount.Id
        );

		System.RunAs(apiUser){
			Opp.Sell_Through_Partner__c = TestResellerAccount.Id;
			Opp.Revenue_Type__c = 'Reseller';
			Opp.Electronic_Acceptance__c = 'Yes';
	
			
			// 'Competitor Information' must be filled out in Stage 'Champion' or above	
			Opp.StageName = 'Champion';
			Opp.RecordTypeId = [Select id From Recordtype where DeveloperName = 'Sales_QCCS'].id;
			//Opp.Effective_Amount__c = 10000.0;
			//Opp.License_Forecast_Amount__c = 10000.0;
			Opp.Included_Products__c = 'Qlik Sense';
			Opp.ForecastCategoryName = 'Omitted';
			//Opp.Competitors__c = 0;
			
			try {		     
        		insert Opp;
			} catch(DmlException ex) {
				System.assert(false, ex.getMessage());
			}*/
			
			//Stage must be at Deal split Create, Deal Split Established, Closed Won or Closed Lost when using this record type
			//----------------------------------------------------------
			//Updated on 09-04-20 by Leonardo Grauer - Starting test here since record type Qlikbuy CCS Standard has been deactivated
			
		Test.startTest();
		System.RunAs(apiUser){
			Opportunity Opp = new Opportunity(
			//RecordTypeId = '01220000000Dmf5',	// Deal Split Parent  ---> This record type has also been deactivated - OTQ-399
			RecordTypeId = '01220000000DoEj', // Deal Split Child
			Name = 'Test Val Rule Opp',
			CurrencyIsoCode = 'USD',
        	Short_Description__c = 'Partner Reseller',
			StageName = 'Champion',
        	CloseDate = Date.today().addDays(7),
        	Type = 'New Customer',
        	AccountId = acc.Id,
        	Sell_Through_Partner__c = TestResellerAccount.Id,
			Revenue_Type__c = 'Reseller',
			Electronic_Acceptance__c = 'Yes',
			Included_Products__c = 'Qlik Sense',
			ForecastCategoryName = 'Pipeline');
			
			try {		     
        		//update Opp;
				insert Opp;
			} catch(DmlException ex) {
				System.assert(false, ex.getMessage());
			}

			//You must choose a Reason Lost if stage = Closed Lost or Goal Rejected
			Opp.StageName = 'Closed Lost';
			Opp.Primary_reason_lost__c = null;
			try {		     
        		update Opp;
			} catch(DmlException ex) {
				System.assert(false, ex.getMessage());
			}

			//Must have a Billing and Shipping address on the account before setting Stage on an Opportunity to this level (SiB on Std Sales/Extension Sales; Present Contracts on Order Only; Quote on Exclusively Reseller; Prove Value on Customer Centric Selling)
			Opp.StageName = 'SiB';
			Opp.Bypass_Rules__c = true;
			try {		     
        		update Opp;
			} catch(DmlException ex) {
				System.assert(false, ex.getMessage());
			}
			
			// The opportunity must have been approved before you can set to Closed Won. Set to a lower stage and then submit Opportunity for Approval.
			/*			
			Opp.pse__Is_Services_Opportunity__c = false;
			Opp.Discount_from_List__c = 0.10;
			Opp.RecordTypeId = '01220000000DZil';
			Opp.Opportunity_Approved__c = false;
			Opp.StageName = 'Closed Won';
			try {		     
        		update Opp;
			} catch(DmlException ex) {
				System.assert(false, ex.getMessage());
			}
			*/
			/*
			// quote status must be at order placed (set within QlikBuy) in order to set the opportunity to closed won
			Opp.RecordTypeId = '01220000000J1KR'; // '01220000000J1KQ';
			Opp.StageName = 'Closed Won';
			Opp.Electronic_Acceptance__c = 'Yes';
			//Opp.Quote_Status__c <> 'Order Placed'
			try {		     
        		update Opp;
			} catch(DmlException ex) {
				System.assert(false, ex.getMessage());
			}
			*/
			//You need to add a Sell Through Partner and the Partner Contact to be able to set the Revenue Type = Reseller
			Opp.Sell_Through_Partner__c = null;
			Opp.Electronic_Acceptance__c = 'Yes';
			try {		     
        		update Opp;
			} catch(DmlException ex) {
				System.assert(false, ex.getMessage());
			}			
		}
		Test.stopTest();    	
    }
}