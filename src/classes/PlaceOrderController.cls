/**
    Change log
*  20.05.2017 : Muneer  QCW-1633
   2018-06-04 MTM BSL-490 New visual compliance implementations
   2018-06-29 Anjuna BSL-612 New visual compliance implementations 
   2020-04-12 UIN DGM-169 changes for oem
**/
public class PlaceOrderController {
    public Boolean status
    {
        get
        {
            if(ECustomsHelper.NewVisualComplianceEnabled) return VCstatus;
            else
            {
                if((quote.Select_Billing_Address__r.Legal_Approval_Status__c == 'Auto Approved' ||
                    quote.Select_Billing_Address__r.Legal_Approval_Status__c == 'Granted') &&
                    (quote.Shipping_Address__r.Legal_Approval_Status__c == 'Auto Approved' ||
                    quote.Shipping_Address__r.Legal_Approval_Status__c == 'Granted') &&
                    PartnerAddressesApproved())
                    {
                        return true;
                    }
                    else return false;
                }
        }
        set;
    }

    public Boolean VCstatus
    {
        get
        {
            Boolean vcApproved = false;
            vcApproved = ECustomsHelper.VCStatus(quote.Revenue_Type__c, quote.SBQQ__Account__r.ECUSTOMS__RPS_Status__c,
                                          quote.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c, quote.SBQQ__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
                                          quote.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c);
            if(quote.Revenue_Type__c.contains('OEM') && vcApproved){
                List<Opportunity> opps = new List<Opportunity>();
                List<String> oppIds = new List<String>();
                List<SBQQ__Quote__c> quotes = new List<SBQQ__Quote__c>();
                quotes = [select id, SBQQ__Opportunity2__c,SBQQ__Opportunity2__r.End_User_Account_del__c from SBQQ__Quote__c where id= :quote.Id];
                for( SBQQ__Quote__c q : quotes){
                    if(String.isNotBlank(q.SBQQ__Opportunity2__r.End_User_Account_del__c))
                        oppIds.add(q.SBQQ__Opportunity2__c);
                }
                if(!oppIds.isEmpty()){
                    opps = [select id, End_User_Account_del__r.ECUSTOMS__RPS_Status__c,End_User_Account_del__r.ECUSTOMS__RPS_RiskCountry_Status__c from opportunity where id in:oppIds];
                    if('No Matches'.equalsIgnoreCase(opps[0].End_User_Account_del__r.ECUSTOMS__RPS_Status__c) &&
                         !('Alert'.equalsIgnoreCase(opps[0].End_User_Account_del__r.ECUSTOMS__RPS_RiskCountry_Status__c ))){
                        if(vcApproved)
                            vcApproved = true;
                    }
                        else vcApproved = false;
                    }
                }
            return vcApproved;
        }
        set;
    }
    
    public Boolean academicValidationStatus  { get {
        if (quote.Revenue_Type__c.equals('Academic Program')) {
            for (SBQQ__QuoteLine__c line : [SELECT Id, Product_Group__c, Family__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id]) {
                if (line.Family__c.equals('Licenses')) {
                    return false;
                }
            }
        }
        return true;
    } private set;}

    public SBQQ__Quote__c quote {get; set;}

    public ApexPages.StandardController controller;
    public PlaceOrderController (ApexPages.StandardController stdController) {
        quote = (SBQQ__Quote__c)stdController.getRecord();
        controller =stdController;
        //status = quote .Select_Billing_Address__r.Legal_Approval_Status__c;
    }
    
    public Boolean PartnerAddressesApproved()
    {
        HardcodedValuesQ2CW__c hSetting = HardcodedValuesQ2CW__c.getOrgDefaults();
        String partnerRevTypes = hSetting.Partner_Revenue_Types__c;
        Set<String> revTypes = new Set<String>();
        if(partnerRevTypes != null)
        {
            System.debug('PlaceOrderController:partnerRevTypes = ' + partnerRevTypes);
            revTypes.addAll(partnerRevTypes.split(','));
        }
        Boolean shippingLegalStatus = false;
        Boolean billingLegalStatus = false;
        if(revTypes.Contains(quote.Revenue_Type__c) && String.isNotBlank(quote.SBQQ__Account__c))
        {
            System.debug('PlaceOrderController:quote.Revenue_Type__c = ' + quote.Revenue_Type__c);
            List<Address__c> addresses = [SELECT id,
                                          Address_Type__c,
                                          Legal_Approval_Status__c
                                          FROM Address__c
                                          WHERE Account__c =: quote.SBQQ__Account__c
                                          AND Default_Address__c = true];
            if(addresses.size() == 2)
            {
                for(Address__c add: addresses)
                {
                    if(add.Address_Type__c == 'Shipping'
                       && (add.Legal_Approval_Status__c == 'Auto Approved' || add.Legal_Approval_Status__c == 'Granted'))
                        shippingLegalStatus = true;
                    if(add.Address_Type__c == 'Billing'
                       && (add.Legal_Approval_Status__c == 'Auto Approved' || add.Legal_Approval_Status__c == 'Granted'))
                        billingLegalStatus = true;
                }
                System.debug('PlaceOrderController:shippingLegalStatus = ' + shippingLegalStatus);
                System.debug('PlaceOrderController:billingLegalStatus = ' + billingLegalStatus);
                return shippingLegalStatus && billingLegalStatus; //End user legal approval passed
            }
            else //If there are no default billing and shipping addresses on account, we pass the status to avoid blocking quote process
            return true; //Missing addresses errors should not be captured here.
        }
        // This is not partner deal. Pass legal approval status
        System.debug('PlaceOrderController:PartnerAddressesApproved END');
        return true;

    }
public Boolean IsNewVisualComplianceEnabled
    { 
        get {
            return ECustomsHelper.NewVisualComplianceEnabled;
        }
        set;
    }

}