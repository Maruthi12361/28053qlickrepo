/*************************************************************************

 Class: LeadOnInsertTrigger2Handler

 Changelog:

 20121026	TJG Quick CR 6484 - Currently when new Partner Opp Reg Leads are created, 
 			no value is being recorded in the Lead Source field. 
 			This value should be 'PART - Partner' for every new Lead created
 			
 20120905	TJG Trigger fires only when Web Activity is Qonnect Markting Services and Record type is 'Parter Opp Reg'
			and Lead source is 'Parter - Partner'

 20120823	TJG PO179 https://projects.qliktech.com/sites/projects/MarketingasaServiceforPartners/default.aspx
 			Copy partner OwnerId from QMS_Original_Partner_OwnerId__c to OwnerId

 017-10-25 AYS BMW-402 : Migrated from LeadOnInsertTrigger2.trigger.
**************************************************************************/

public class LeadOnInsertTrigger2Handler {
	static final String WA_SOURCE = 'Qonnect Marketing Services';
	static final String LEADSOURCE = 'PART - Partner';
	static final String RT_PARTEROPPREG = '012D0000000JsWAIA0';

	public LeadOnInsertTrigger2Handler() {
		
	}

	public static void handle(List<Lead> triggerNew) {
		for(Lead lead : triggerNew)
		{
			if (lead.QMS_Original_Partner_OwnerId__c != null
				&& lead.Web_Activity_Source__c == WA_SOURCE
				&& lead.LeadSource == LEADSOURCE
				&& lead.RecordTypeId ==  RT_PARTEROPPREG
				&& lead.QMS_Original_Partner_OwnerId__c != '')
			{
				lead.OwnerId = lead.QMS_Original_Partner_OwnerId__c;
			}			
			// Quick CR 6484
			if ((lead.LeadSource == null || lead.LeadSource == '')
				&& lead.RecordTypeId ==  RT_PARTEROPPREG)
			{
				lead.LeadSource = LEADSOURCE;
			}
		}
	}
}