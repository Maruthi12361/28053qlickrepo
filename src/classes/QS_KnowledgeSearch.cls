/**
* Change log:
* 
* 2018-03-28    ext_vos     CHG0032295: add constructor with 4 parameters (for QS_Controller: 'Trending STT Webinars').
* 2019-05-05    AIN         IT-1597 Updated for Support Portal Redesign
*/
public with sharing class QS_KnowledgeSearch extends QS_Search {

    public Map<string, string> searchCategories;
    public string orderBy;

    public QS_KnowledgeSearch(string searchTerms) {

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        pageSize = (integer)searchSettings[0].Page_Size__c;
        startIndex = 0;
        this.searchTerms = searchTerms;
       
        doSearch();
        totalSize = getCount();
        totalPages = getTotalPages();
    }
    public QS_KnowledgeSearch(string searchTerms, Map<string, string> v_searchCategories) {
        system.debug('QS_KnowledgeSearch constructor 2 param start');
        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        pageSize = (integer)searchSettings[0].Page_Size__c;
        startIndex = 0;
        this.searchTerms = searchTerms;

        searchCategories = v_searchCategories;
       
        doSearch();
        totalSize = getCount();
        totalPages = getTotalPages();
        system.debug('QS_KnowledgeSearch constructor 2 param end');
    }
    public QS_KnowledgeSearch(string searchTerms, Map<string, string> v_searchCategories, string v_orderBy) {
        system.debug('QS_KnowledgeSearch constructor 2 param start');
        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        pageSize = (integer)searchSettings[0].Page_Size__c;
        startIndex = 0;
        this.searchTerms = searchTerms;

        searchCategories = v_searchCategories;
        orderBy = v_orderBy;
       
        doSearch();
        totalSize = getCount();
        totalPages = getTotalPages();
        system.debug('QS_KnowledgeSearch constructor 2 param end');
    }

    public QS_KnowledgeSearch(string searchTerms, Map<string, string> v_searchCategories, string v_orderBy, integer v_pageSize) {
        system.debug('QS_KnowledgeSearch constructor 4 param start');
        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        pageSize = v_pageSize;
        startIndex = 0;
        this.searchTerms = searchTerms;

        searchCategories = v_searchCategories;
        orderBy = v_orderBy;
       
        doSearch();
        totalSize = getCount();
        totalPages = getTotalPages();
        system.debug('QS_KnowledgeSearch constructor 4 param end');
    }

    public override void doSearch() {
        system.debug('QS_KnowledgeSearch doSearch start');
 
    
        if (searchTerms.length() > 0) {
            string orderByQ;
            
            if(orderBy == 'LastPublishedDate' || orderBy == 'FirstPublishedDate')
                orderByQ = orderBy;
            else
                orderByQ = 'LastPublishedDate';
            
            searchTerms = string.escapesinglequotes(searchTerms);
            List<String> arguments = new String[] {searchTerms, orderByQ, pageSize.format(), startIndex.format()};
            string queryString = string.format('FIND  \'\'{0}\'\' In Name Fields RETURNING KnowledgeArticleVersion (Title, Summary, id, LastPublishedDate, isVisibleinPkb, KnowledgeArticleId where PublishStatus=\'\'Online\'\' and Language = \'\'en_US\'\' and IsLatestVersion = true order by {1} desc limit {2} offset {3})', arguments);
            if(searchCategories != null && searchCategories.size() > 0)
                queryString += buildCategorySosl();
            system.debug('queryString: ' + queryString);
            List<List<SObject>> searchList = Search.query(queryString);
            KnowledgeArticleVersion[] articles = ((List<KnowledgeArticleVersion>)searchList[0]);
                   
            system.debug('articles.Size(): ' + articles.Size());
            List<Id> knowledgeArticleIds = new List<Id>();
            for (KnowledgeArticleVersion a : articles) {
                knowledgeArticleIds.add(a.KnowledgeArticleId);
            }

            Map<id, Double> scoreMap = new map<Id, Double>();
            List<KnowledgeArticleVoteStat> kbVotes = [select Parentid, NormalizedScore from KnowledgeArticleVoteStat where ParentId in :knowledgeArticleIds and Channel = 'AllChannels'];
            for (KnowledgeArticleVoteStat vote : kbVotes)
                scoreMap.put(vote.ParentId, vote.NormalizedScore);

            Map<id, Double> viewMap = new Map<Id, Double>();
            List<KnowledgeArticleViewStat> kbViews = [select Parentid, NormalizedScore, ViewCount from KnowledgeArticleViewStat where ParentId in :knowledgeArticleIds and Channel = 'AllChannels'];
            for (KnowledgeArticleViewStat view : kbViews)
                viewMap.put(view.ParentId, view.ViewCount);

            searchResults = new List<QS_SearchWrapper>();
            for (KnowledgeArticleVersion a : articles) {
            //for (Basic__kav a : basicKavs) {
               QS_SearchWrapper w = new QS_SearchWrapper();
                w.title = a.Title;
                w.description = a.Summary;
                w.url = 'QS_CaseWizardKnowledgeArticle?id=' + a.Id;
                w.dateAdded = a.LastPublishedDate.format('dd MMMM yyyy');
                //AIN 2015-05-27
                w.dateAddedDT = a.LastPublishedDate;
                w.isVisibleInPkb = a.IsVisibleInPkb;
                w.articleId = a.Id;
                w.rating = scoreMap.get(a.KnowledgeArticleId);
                w.views = viewMap.get(a.KnowledgeArticleId);
                searchResults.add(w);
                system.debug('Adding article with title: ' + a.Title +' and summary: ' + a.summary);
            }
            /*system.debug('List before sort');
            for(QS_SearchWrapper wrap : searchResults) {
                system.debug('wrap.title: ' + wrap.title);
                system.debug('wrap.views: ' + wrap.views);
                system.debug('wrap.rating: ' + wrap.rating);
            }
            if(orderBy == 'Views' || orderBy == 'Rating')
                SortList(searchResults, orderBy);
            
            system.debug('List after sort');
            for(QS_SearchWrapper wrap : searchResults) {
                system.debug('wrap.title: ' + wrap.title);
                system.debug('wrap.views: ' + wrap.views);
                system.debug('wrap.rating: ' + wrap.rating);
            }*/
        }
    }
    private void SortList(List<QS_SearchWrapper> sList, string order) {
        QS_SearchWrapper temp;
        for(Integer i=0;i<sList.Size();i++){
            for(Integer j=i+1;j<sList.Size();j++){
                if((order == 'Views' && sList[i].views < sList[j].views) || (order == 'Rating' && sList[i].rating < sList[j].rating)){
                    system.debug('Exchanging ' + (order == 'Views'?sList[i].views:sList[i].rating) + ' with ' + (order == 'Views'?sList[j].views:sList[j].rating));
                    temp = sList[i];
                    sList[i] = sList[j];
                    sList[j] = temp;
                }
            }
        }
    }

    private integer getCount() {

         if (searchTerms.Length() > 0) {
             List<String> arguments = new String[] {searchTerms};
             string queryString = string.format('FIND  \'\'{0}\'\'  RETURNING KnowledgeArticleVersion (id where PublishStatus=\'\'Online\'\' and Language = \'\'en_US\'\' and IsLatestVersion = true order by LastPublishedDate limit 2000)', arguments);
             if(searchCategories != null && searchCategories.size() > 0)
                queryString += buildCategorySosl();
             List<List<SObject>> searchList = Search.query(queryString);
             KnowledgeArticleVersion[] articles = ((List<KnowledgeArticleVersion>)searchList[0]);
             return articles.size();
         }
         else {
            return 0;
         }
    }

    public String buildCategorySosl() {
        string categorySosl = ' WITH DATA CATEGORY ';
        List<string> categoryGroups = new List<string>();
        for(string key : searchCategories.keySet()) {
            string value = searchCategories.get(key);
            string searchString = '';
            list<string> valueList = value.split(',');
            for (integer i=0;i<valueList.size();i++)
                valueList[i] = valueList[i]+'__c';
            value = join(valueList, ',');
            categoryGroups.add(string.format('{0} BELOW ({1})',new String[]{key, value}));
        }
        return categorySosl + (join(categoryGroups, ' AND '));
    }
    // joins elements in the passed list of strings by the passed string c
      // c can be a single character or a longer string -- useful for joinging segments of a dynamic sosl or soql query
    public static String join(List<String> strings, String c) {
        if (strings == null || strings.isEmpty()) return null;

        String sep = (isBlank(c) ? ',' : c);
        Integer i, size;
        String s = strings[0];

        size = strings.size();
        for (i = 1; i < size; i++) {
        s += (sep + strings[i]);
        }
        return s;
    }

    // version of join method that takes a set of strings
    public static String join(Set<String> strings, String c) {
        return join(new List<String>(strings), c);
    }
     // returns true if string is null or empty
    public static Boolean isBlank(String s) {
        return ( s == null || s == '');
    }
}