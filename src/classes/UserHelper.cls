/**
 * User helper class that ensures we only go to SOQL once if we need additional information about the currently running user or their profile.
 * Only one user soql query should ever execute to the currenlty running user record and profile in a transaction 
 * 2019-11-27 : FEH Created the class
 */ 
public class UserHelper {

    private static User extendedUser; //holds the extend information we might need about a user, the user record should only ever be queried once per transaction 
    private static Schema.FieldSet ExtendedUserFields = Schema.SObjectType.User.fieldSets.ExtendedUser;
    
    public static User getExtendedUser() {
   
        if(extendedUser == null) {
            Id userId = UserInfo.getUserId(); 
            String[] queryFields = new List<String>(); 

            for(Schema.FieldSetMember f: ExtendedUserFields.getFields()) {
                queryFields.add(f.getFieldPath());
            }
           
            List<SObject> users = Database.query('SELECT '+String.join(queryFields, ', ')+' FROM User WHERE Id=:userId');
            if(users.size() > 0) {
                extendedUser = (User) users[0];
            }
        }

		return extendedUser;
    }
    
    
    
}