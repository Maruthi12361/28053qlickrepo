/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_LithiumSearch extends QS_Search {
    
    public static string JSON_LIST = 'items';
    public static string JSON_SUBJECT = 'subject';
    //public static string JSON_RESOURCES = 'resources';
    //public static string JSON_RESOURCES_HTML = 'conversation';
    //public static string JSON_RESOURCES_HTML_URL = 'view_href';
    public static string JSON_TOPIC = 'topic';
	public static string JSON_TOPIC_HTML_URL = 'view_href';
    public static string JSON_PUBLISHED = 'post_time';
    public static string JSON_CONTENT = 'search_snippet';
    public static string JSON_CONTENT_TEXT = 'text';
    public static final integer DESCRIPTION_CHARS = 200;
    //public static final string JSON_HEADER = 'throw \'allowIllegalResourceCall is false.\';';
    public static final string JSON_HEADER = '';
   
    private string serviceEndpoint;
    private integer resultSize = 0;
    private boolean personasBased = false;
    private string lithiumForum;
    private boolean responseError;
    private boolean isActive = true;
    
    public QS_LithiumSearch(string searchTerms, integer NumberItems, boolean personasBased) {


        QS_SearchSettings__c searchSettings = QS_SearchSettings__c.getInstance('SearchSettings');

        if(searchSettings != null) {
            if(searchSettings.Community_Posts_List_Active__c != null){
                isActive = searchSettings.Community_Posts_List_Active__c;
            }
        }

        responseError = false;
        
        if (personasBased) {

            serviceEndPoint =  searchSettings.Lithium_EndPoint_Personas__c;

            boolean unauthenticated = false;
            User userRec;
            //todo factor into common class
            if(userinfo.getUserId() != null)
                userRec = [Select Id, ContactId, AccountId, Contact.Persona__c 
                           From User where Id = :UserInfo.getUserId()];
            
            if(userRec == null || userRec.ContactId == null || userRec.AccountId == null)
                unauthenticated = true;
            else
                unauthenticated = false;
            
            if(!unauthenticated && userRec != null && userRec.ContactId != null && userRec.Contact.Persona__c != null) {
                string[] personas = userRec.Contact.Persona__c.split(';');
                string persona = '';

                if(personas.size() > 1) {
                    Double rnd =  Math.random();
                    integer index = (integer)(rnd * personas.size());
                    persona = personas[index];
                }
                else
                    persona = personas[0];
                //QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(userRec.Contact.Persona__c);
                system.debug('Persona in LithiumSearch: ' + persona);
                QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(persona);
                lithiumForum = categoryRecord != null ? categoryRecord.Lithium_Forum__c : '';
            } else {
                lithiumForum = searchSettings.Lithium_Default_Forum__c;
            }
            if(lithiumForum != null)
            	serviceEndPoint = serviceEndPoint.replace('placeTerms', lithiumForum);
        }
        else {
           serviceEndpoint = searchSettings.Lithium_EndPoint__c;  
        }
        Integer daysBackToSearch = 30;
        if(searchSettings.Lithium_Popular_Posts_Days_Back__c != 0)
        	daysBackToSearch = (Integer)searchSettings.Lithium_Popular_Posts_Days_Back__c;
        Datetime dtDaysBackToSearch = DateTime.Now();
        dtDaysBackToSearch = dtDaysBackToSearch.AddDays(-daysBackToSearch);
      	
      	

       /*if (searchTerms != null && searchTerms.length() > 1 ) {
                searchTerms = searchTerms.replace(' ', '+');
                serviceEndpoint = serviceEndpoint.replace('searchTerms', searchTerms);
        }*/
        //set page size
        if (numberItems == 0) {
            pageSize = (Integer)searchSettings.Page_Size__c;
        }
        else {
            pageSize = numberItems;
        } 
		
		serviceEndpoint = serviceEndpoint.replace('{dtFrom}', dtDaysBackToSearch.format('yyyy-MM-dd') + 'T' + dtDaysBackToSearch.format('HH:mm:ss.FFF'));
        serviceEndpoint = serviceEndpoint.replace('{limit}', pageSize.format());
        
        //set default size and index
        startIndex = 0;
        totalSize = 0; 
        doSearch();
        totalPages = getTotalPages();
    }
    
    public override void doSearch() {
                   
            //string nextSearchUrl = serviceEndpoint.replace('=index', '=' + startIndex.format());
            string nextSearchUrl = serviceEndpoint;
            string response;
            String responseBody = callLithiumWebService(nextSearchUrl);
            if(!responseError)
                response = responseBody.replace(JSON_HEADER, '');
            else
                response = responseBody;

            if(!responseError) {
                //de-serialise response          
                Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response);
                
                //get list of search result elements
                Map<String, Object> data = (Map<String, Object>)m.get('data');
                List<Object> resultList = (List<Object>)data.get(JSON_LIST);
                resultSize = (resultList != null && !resultList.isEmpty()) ? resultList.size() : 0;
                
                //populate into list of search wrappers           
                searchResults = new List<QS_SearchWrapper>();
                totalSize = resultSize;
                system.debug('~~~~~~~~~~~~~ Inside Lithium Search - resultSize='+resultSize );

                for(integer i = 0; i < resultSize; i++) {
                    
                    QS_SearchWrapper w = new QS_SearchWrapper();
                    
                    //get top level elements
                    Map<String, Object> r1 = (Map<String, Object>)resultList[i];
                    w.title = (string)r1.get(JSON_SUBJECT);
                    system.debug('~~~~~~~~~~~~~ Inside Lithium Search - w.title='+w.title );
                    if (r1.get(JSON_PUBLISHED) != null) {
                        
                        string jiveDate = ((string)r1.get(JSON_PUBLISHED)).replace('T', ' ').substring(0,19);
                        w.dateAdded = Datetime.valueOf(jiveDate).format('dd MMMM yyyy');  
                        w.dateAddedDT = Datetime.valueOf(jiveDate);
                        system.debug('~~~~~~~~~~~~~ Inside Lithium Search - inside published - w.dateAddedDT= ' + w.dateAddedDT);
                    }
                    
                    //get post description
                    //Map<String, Object> jiveConentMap = (Map<String, Object>)r1.get(JSON_CONTENT);
                    String lithiumContent = (string)r1.get(JSON_CONTENT);
                    if (lithiumContent != null) {
                        
                        w.description = lithiumContent.stripHtmlTags().normalizeSpace();
                        system.debug('~~~~~~~~~~~~~ Inside Lithium Search - w.description='+w.description );
                        if (w.description.length() > DESCRIPTION_CHARS) {
                            w.description = w.description.substring(0, DESCRIPTION_CHARS);
                        }
                    }
                    
                    //get post url
                    Map<String, Object> lithiumTopic = (Map<String, Object>)r1.get(JSON_TOPIC);
                    //Map<String, Object> lithiumHtml = (Map<String, Object>)lithiumTopic.get(JSON_TOPIC_HTML_URL);
                    w.url = (string)lithiumTopic.get(JSON_TOPIC_HTML_URL);
                    system.debug('~~~~~~~~~~~~~ Inside Lithium Search - w.url='+w.url );

                    searchResults.add(w);
                }
            } else {
                searchResults = null;
            }
    }
    
    public string callLithiumWebService(string searchUrl) {

        if(isActive)
        {
    		//searchUrl = 'http://qlik:fYN~Rp6hY@migration29.stage.lithium.com/api/2.0/search?q=SELECT+*+FROM+messages+where+conversation.style=+\'forum\'+and+depth=0+and+conversation.last_post_time>=2018-08-13T00:00:00.000+order+by+kudos.sum(weight)+desc+limit+2';
            //make call to Lithium
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(searchUrl);
            req.setMethod('GET');
            system.debug('~~~~~~~~~~~~~ Inside Lithium Search - searchURL: ' + searchURL);
            HttpResponse res = http.send(req);
            
            // setting the return message if the web service call is not successful
            String returnMsg = '';
            responseError = false;
            system.debug('~~~~~~~~~~~~~ Inside Lithium Search - callLithiumWebService -- if' );
            system.debug('~~~~~~~~~~~~~ Inside Lithium Search - response.Status: ' + res.getStatus());
            if(res.getStatusCode() >= 300 && res.getStatusCode() != 306 && res.getStatusCode() != 308) {
                system.debug('~~~~~~~~~~~~~ Inside Lithium Search - callLithiumWebService -- if - inside error' );
                responseError = true;
                returnMsg = System.Label.QS_Jive_Search_Inactive_Message;
                return returnMsg;
            }
            system.debug('Return message: ' + res.getBody());
            return res.getBody();
        }
        else {
            system.debug('~~~~~~~~~~~~~ Inside Lithium Search - callLithiumWebService -- else' );
            // setting the return message if the web service is not invoked
            responseError = true;
            String returnMsg = System.Label.QS_Jive_Search_Inactive_Message;
            return returnMsg;
        }
    }
    
    public override Integer getTotalPages() {
        try{
            system.debug('~~~~~~~ totalSize:' +totalSize + '  pageSize:' +  pageSize);
                if (math.mod(totalSize, pageSize) > 0) 
                {
                    return totalSize/pageSize + 1;
                } 
                else 
                {
                    return (totalSize/pageSize);
                }
            }
        catch(Exception e){system.debug('~~~~~~~ error' );return 1; }
    }
    
    public override Integer getPageNumber() {
        return 1;
    }
    
    public override Boolean getDisableNext() { 
        if (resultSize < pageSize) return true; else return false;
    }
}