public class PseBillingEventsCustomExtension {

    /**
     * Creates an extension from a standard set controller.  
     * Expects a set of pse__Billing_Event__c records.
     */
    public PseBillingEventsCustomExtension(ApexPages.StandardSetController stdSetController) {
        this.billingEvent = (pse__Billing_Event__c)stdSetController.getRecord();
    }

    /**
     * Exports the list of Billing Events.
     */
    public PageReference export() {
        updateSelectList();
        // TODO wrap with try/catch

        for (pse__Billing_Event__c event : selectedEvents) {
            event.Export_Date__c = Date.today();

            if (exportEvent.Export_Label__c != null) {
                event.Export_Label__c = exportEvent.Export_Label__c;
            } else {
                event.Export_Label__c = 'Export ' + event.Export_Date__c;
            }
            //event.Exported__c = true;
        }

        // Update
        update selectedEvents;

        // Export to CSV
        return Page.Pse_BillingEventsExportCsv;
    }

    /**
     * Preview items to be exported.
     */
    public PageReference preview() {
        updateSelectList();
        return null;
    }

    /** 
     * Update the list of selected records based on the checkboxes. 
     */
    private void updateSelectList() {
        selectedEventItems = null;
        List<pse__Billing_Event__c> selectedList = new List<pse__Billing_Event__c>();
        for (WrapperBillingEvent wrappedEvent : eventList) {
            if (wrappedEvent.selected) {
                selectedList.add(wrappedEvent.event);
            }
        }
        selectedEvents = selectedList; 
    }

    /**
     * List of selectable events.
     */
    public List<WrapperBillingEvent> eventList {
        get {
            if (eventList == null) {
                eventList = new List<WrapperBillingEvent>();
                for (pse__Billing_Event__c event : billingEvents) {
                    eventList.add(new WrapperBillingEvent(event));
                }
            }
            return eventList;
        }
        private set;
    }

    /**
     * Custom set controller.
     */
    public ApexPages.StandardSetController eventRecords {
        get {
            if (eventRecords == null) {
                eventRecords = new ApexPages.StandardSetController(Database.getQueryLocator(
                        [SELECT Id, Name, 
                         pse__Is_Released__c, Exported__c, pse__Invoiced__c, 
                         pse__Billing_Event_Batch__c, 
                         pse__Billing_Event_Batch__r.pse__Account__c, 
                         pse__Billing_Event_Batch__r.pse__Account__r.OwnerId, 
                         pse__Project__c, pse__Date__c, pse__Summary_Amount__c
                         FROM pse__Billing_Event__c
                         WHERE pse__Is_Released__c = true AND Exported__c = false AND pse__Invoiced__c = false
                         ORDER BY CreatedDate asc]));
                eventRecords.setPageSize(1000);
            }
            return eventRecords;
        }
        private set;
    }

    /** Template billing event to store export info. */
    public pse__Billing_Event__c exportEvent {
        get {
            if (exportEvent == null) {
                exportEvent = new pse__Billing_Event__c();
            }
            return exportEvent;
        }
        private set;
    }

    /** List of selected events. */
    public List<pse__Billing_Event__c> selectedEvents {
        get { return (List<pse__Billing_Event__c>) eventRecords.getSelected(); }
        private set { eventRecords.setSelected(value); }
    }

    public List<pse__Billing_Event_Item__c> selectedEventItems {
        get {
            if (selectedEventItems == null) {
                selectedEventItems = [SELECT
                            pse__Billing_Event__r.pse__Billing_Event_Batch__c,
                            pse__Billing_Event__r.pse__Billing_Event_Batch__r.pse__Account__c,
                            pse__Billing_Event__r.pse__Billing_Event_Batch__r.pse__Account__r.Name,
                            pse__Billing_Event__r.pse__Billing_Event_Batch__r.pse__Account__r.OwnerId,
                            pse__Billing_Event__r.pse__Billing_Event_Batch__r.pse__Account__r.Owner.Name,
                            pse__Billing_Event__r.pse__Project__c,
                            pse__Billing_Event__r.pse__Project__r.Name,
                            pse__Billing_Event__r.pse__Project__r.pse__Project_Type__c,
                            pse__Billing_Event__r.pse__Project__r.Invoice_Notes__c,
                            pse__Billing_Event__r.pse__Project__r.PO_Number__c,
                            pse__Billing_Event__r.Name,
                            Id, Name, pse__Description__c, pse__Object_Id__c, pse__Date__c, 
                            pse__Quantity__c, pse__Unit_Price__c, pse__Amount__c, pse__Category__c,
                            (SELECT pse__Resource__c, pse__Resource__r.Name, pse__Start_Date__c, pse__End_Date__c
                             FROM pse__Timecard_Splits__r),
                            (SELECT Id FROM pse__Milestones__r),
                            (SELECT Id FROM pse__Miscellaneous_Adjustments__r),
                            (SELECT Id FROM pse__Budgets__r),
                            (SELECT Id FROM pse__Expenses__r)
                         FROM pse__Billing_Event_Item__c
                         WHERE pse__Billing_Event__c in :selectedEvents
                         ORDER BY pse__Billing_Event__r.CreatedDate asc, pse__Date__c asc];
            }
            return selectedEventItems;
        }
        private set;
    }

    /** Controller billing event record. */
    private final pse__Billing_Event__c billingEvent;

    /**
     * List of underlying billing events.
     */
    private List<pse__Billing_Event__c> billingEvents {
        get { return (List<pse__Billing_Event__c>) eventRecords.getRecords(); }
    }



    /** 
     * Wrapper class for pse__Billing_Event__c to keep track of selection. 
     */
    public class WrapperBillingEvent {
        public WrapperBillingEvent(pse__Billing_Event__c event) {
            this(event, false);
        }

        public WrapperBillingEvent(pse__Billing_Event__c event, boolean selected) {
            this.event = event;
            this.selected = selected;
        }

        public final pse__Billing_Event__c event {get; private set;}
        public boolean selected {get; set;}
    }

}