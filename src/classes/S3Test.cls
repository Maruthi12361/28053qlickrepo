@isTest
private class S3Test {
	/* Below are all the test methods required to deploy this apex code and meet the Test Method Code Coverage requirements of the Force.com platform
   */ 
  
 
   public static testmethod void test1() {
        QTTestUtils.GlobalSetUp();

        s3.ListBucketResponse_element lbre = new s3.ListBucketResponse_element();
        s3.ListBucketResult lbr = new s3.ListBucketResult();
        lbr.Metadata  = null;
        lbr.Name = 'test';
        s3.ListAllMyBucketsList lambl = new s3.ListAllMyBucketsList();
        lambl.Bucket = null;
        s3.metadataEntry me = new s3.metadataEntry();
        me.name = 'Test';
        s3.ListAllMyBucketsEntry lambe = new s3.ListAllMyBucketsEntry();   
        lambe.name = 'test';
        s3.ListAllMyBucketsResult lambr = new s3.ListAllMyBucketsResult();
        s3.CreateBucketResult cbr = new s3.CreateBucketResult();
        s3.CanonicalUser co = new s3.CanonicalUser();
        s3.listEntry le = new s3.listEntry();
        le.Key = 'test';
        le.LastModified = datetime.now();
        le.Etag = 'test';
        le.size=20.0;
        le.StorageClass = 'test';
        le.owner = co;
        s3.DeleteObject_element  doe = new s3.DeleteObject_element();
        s3.DeleteBucketResponse_element dbr = new s3.DeleteBucketResponse_element();
        s3.PrefixEntry pe = new s3.PrefixEntry();
        s3.status status = new s3.status();
        status.Code = 10;
        status.description = 'test';
        s3.ListAllMyBucketsResponse_element labre = new s3.ListAllMyBucketsResponse_element();
        s3.CreateBucketResponse_element cbre = new s3.CreateBucketResponse_element();
        S3.PutObjectResult por = new S3.PutObjectResult();

        S3.AmazonS3 sss = new S3.AmazonS3(); 
        
        test.startTest();
        sss.DeleteObject( '','','',system.now(),'','');
        test.stopTest();
   
   }
   
    public static testmethod void test2() {
        QTTestUtils.GlobalSetUp();
        S3.AmazonS3 sss = new S3.AmazonS3();
        sss.secret = '42';
        system.assert( sss.signature('encode',system.now() ) != null);
        test.startTest();
        sss.ListBucket( '','','', 3,'','key',system.now(),'sig','cred');    
        test.stopTest();
    }
 
    public static testmethod void test3() {
    	  QTTestUtils.GlobalSetUp();
        S3.AmazonS3 S3 = new S3.AmazonS3();
        S3.secret = '42';
        system.assert( S3.signature('encode',system.now() ) != null);
        test.startTest();
        S3.ListAllMyBuckets('key',system.now(),'sig');
        test.stopTest();
    }  
   
    public static testmethod void test4() {
        QTTestUtils.GlobalSetUp();
        S3.AmazonS3 S3 = new S3.AmazonS3();
        S3.secret = '42';
        system.assert( S3.signature('encode',system.now() ) != null);
        s3.AccessControlList acl = new s3.AccessControlList();
        test.startTest();
        S3.CreateBucket('bucket',acl,'key',system.now(),'sig');
        test.stopTest();
    }  
   
    public static testmethod void test5() {
    	  QTTestUtils.GlobalSetUp();

        S3.AmazonS3 S3 = new S3.AmazonS3();
        S3.secret = '42';
        system.assert( S3.signature('encode',system.now() ) != null);
        test.startTest();
        S3.DeleteBucket('bucket','key',system.now(),'sig','cred');
        test.stopTest();
    }
    
    public static testmethod void test6(){
        QTTestUtils.GlobalSetUp();

        S3.AmazonS3 S3 = new S3.AmazonS3();
        S3.PutObjectInline_element putObjectElem = new S3.PutObjectInline_element();
        putObjectElem.Bucket='test';
        putObjectElem.Key = 'testKey';
        putObjectElem.Metadata = null;
        putObjectElem.Data= 'a';
        putObjectElem.ContentLength = 1;
        putObjectElem.AccessControlList = null;
        putObjectElem.StorageClass='classname';
        putObjectElem.AWSAccessKeyId='asdf';
        putObjectElem.Timestamp = Datetime.now();
        putObjectElem.Signature='test';
        putObjectElem.Credential='test'; 
    }
  
    public static testmethod void test7(){
    
      	S3.AmazonS3 S3 = new S3.AmazonS3();
      	S3.secret = '42';
      	system.assert( S3.signature('encode',system.now() ) != null);
         S3.MetadataEntry[] mdes = new S3.MetadataEntry[2];
        S3.MetadataEntry mde = new S3.MetadataEntry();
        mde.name = 'TEst';
        mde.value = 'test';
        mdes[0] = mde;
        mde = new S3.MetadataEntry();
        mde.name = 'TEst';
        mde.value = 'test';
        mdes[1] = mde;
        test.startTest();
      	try{
      		S3.PutObjectInline_ACL('bucket','key',mdes,'datatesting',6,'public-read','keyid',System.now(),'sig','cred', 'ownerid') ;
      	}
      	catch(Exception ex){
      	}
        test.stopTest();
      
    }
    public static testmethod void test8(){
    
        S3.AmazonS3 S3 = new S3.AmazonS3();
        S3.secret = '42';
        system.assert( S3.signature('encode',system.now() ) != null);
         S3.MetadataEntry[] mdes = new S3.MetadataEntry[2];
        S3.MetadataEntry mde = new S3.MetadataEntry();
        mde.name = 'TEst';
        mde.value = 'test';
        mdes[0] = mde;
        mde = new S3.MetadataEntry();
        mde.name = 'TEst';
        mde.value = 'test';
        mdes[1] = mde;
        test.startTest();
        try{
          S3.PutObjectInline_ACL('bucket','key',mdes,'datatesting',6,'public-write','keyid',System.now(),'sig','cred', 'ownerid') ;
        }
        catch(Exception ex){
        }
        test.stopTest();
      
    }
    public static testmethod void test9(){
      QTTestUtils.GlobalSetUp();

      S3.AmazonS3 as3 = new S3.AmazonS3('key', 'secret');
      string bucketName = 'qlikview-market-dev';
      String folder = 'logos/';
      String permission = 'public-read';
      string base64Body = null;
      //attachmentBody.BodyLength
      double attachmentBodyLength = 0;

      s3.AccessControlList accessControlList = null;
      string StorageClass = 'test';
      String AWSAccessKeyId = 'test';
      DateTime Timestamp = Datetime.Now();
      String Signature = 'test';
      String Credential = 'test';
      test.startTest();
      as3.PutObjectInline(bucketName,folder + 'hello.gif', null,base64Body,  attachmentBodyLength,accessControlList,StorageClass,AWSAccessKeyId, Timestamp, Signature, Credential);
      test.stopTest();
    }
    public static testmethod void test10() {

        QTTestUtils.GlobalSetUp();

        S3HttpMock mock = S3HttpMock.CreateHTTPMock(15); 
        
        S3.AmazonS3 sss = new S3.AmazonS3(); 
        test.startTest();
        S3.MetadataEntry[] mdes = new S3.MetadataEntry[2];
        S3.MetadataEntry mde = new S3.MetadataEntry();
        mde.name = 'TEst';
        mde.value = 'test';
        mdes[0] = mde;
        mde = new S3.MetadataEntry();
        mde.name = 'TEst';
        mde.value = 'test';
        mdes[1] = mde;
        Test.setMock(HttpCalloutMock.class, mock);
        sss.SetObjectAccessControlPolicy('Bucket', 'Key', 'accessType', 'AWSAccessKeyId', Datetime.Now(), 'Signature', 'Credential', 'canonicalUserId');
        sss.CopyObject_ACL('SourceBucket', 'SourceKey', 'DestinationBucket', 'DestinationKey', 'MetadataDirective', mdes, 'public-read', 'canonicalUserId', 'StorageClass', 'AWSAccessKeyId', Datetime.Now(), 'Signature', 'Credential');
        sss.CopyObject_ACL('SourceBucket', 'SourceKey', 'DestinationBucket', 'DestinationKey', 'MetadataDirective', mdes, 'public-write', 'canonicalUserId', 'StorageClass', 'AWSAccessKeyId', Datetime.Now(), 'Signature', 'Credential');
        test.stopTest();
   }
}