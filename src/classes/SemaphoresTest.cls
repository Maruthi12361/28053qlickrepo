/*************************************************************************

    SemaphoresTest
    2017-03-23 AIN Initial development


*************************************************************************/
@isTest
private class SemaphoresTest {
    static testMethod void Test1() {
        Semaphores.SetAllSemaphoresToTrue();
        Semaphores.TriggerHasRun('Test');
        Semaphores.SetAllSemaphoresToFalse();
        Semaphores.TriggerHasRun('Test');
        Semaphores.TriggerHasRun('Test', 1);
        Semaphores.TriggerHasRun(1);
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Test';
        insert c;
    }
}