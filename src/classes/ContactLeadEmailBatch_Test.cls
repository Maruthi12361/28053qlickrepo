/**
    16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors
**/
@isTest
private class ContactLeadEmailBatch_Test {
    
     //Test Data Setup Method
    @testSetup static void  createTestData() { 
        List<String> emailList = new List<String>{'aaaa@aaa.com', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb@aaa.com'};

        insert createNewLead(emailList);
        insert createNewContact(emailList); 

    }
    
    @isTest
    static void callLeadBatch() {
        // Implement test code
        UpdateLeadEmailBatch updleadBatch = new UpdateLeadEmailBatch();
        Database.executeBatch(updleadBatch, 100);
        User u = new User(

    ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com.qa',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US'
);
insert u;
system.runAs(u)
{
        PostRefreshSandboxEmailUpdate p=new PostRefreshSandboxEmailUpdate();
        p.PostRefreshEmailUpdate();
        }
        }
    @isTest
    static void callContactBatch() {
        UpdateContactEmailBatch updContactBatch = new UpdateContactEmailBatch();
        Database.executeBatch(updContactBatch, 100);
    }

    private static List<Lead> createNewLead(List<String> emailList) {
        List<Lead> lLeads = new List<Lead>();
        for(String email: emailList){
            Lead lead = new Lead();
            lead.LastName = 'last';
            lead.Company = 'Company';   
            lead.IsUnreadByOwner = True;
            lead.Country='Sweden';
            lead.Email=email;
            lead.Phone='3333';
            lLeads.add(lead);
        }
        return lLeads;        
    }
    

    private static List<Contact> createNewContact(List<String> emailList) {
        List<Contact> lContacts = new List<Contact>();
        for(String email: EmailList){
            Contact ct = new Contact(   FirstName = 'Test LeadCountryISOUpdate',
                                Email = email,
                                LastName = 'Lastname', 
                                QCloudID__c = 'Enterprise',
                                PartnerSourceNo__c = '12345'
                                );
            lContacts.add(ct);
        }
        return lContacts;     
    }
}