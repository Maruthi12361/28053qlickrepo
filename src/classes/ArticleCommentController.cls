/**************************************************
* Author: Alberto de Diego from Fluido Oy
*
* Change Log:
* 2012-11-15 Alberto de Diego: CR# 4623
*        Initial Development
* 2013-01-08 Madhav Kakani: Replaced Article_Comment__c with Case object
* 2013-08-22	CCE:	CR# 9173 Fixes to "Article Feedback" Cases https://eu1.salesforce.com/a0CD000000Zz1x0
*						if the original Chatter message is deleted and we try to reply to it we get an exception. 
*						Modified to not show the error message for this exception.
*
**************************************************/
public with sharing class ArticleCommentController {

    public Case comment {get; private set;}

    public ArticleCommentController(ApexPages.StandardController stdController) {
        this.comment = (Case)stdController.getRecord();
        this.comment = [SELECT Id, Closing_Comments__c, Knowledge_article__c, Feed_Item__c 
                        FROM Case WHERE Id = :comment.Id];
    }

    public PageReference changeOwner() {
        comment.OwnerId = UserInfo.getUserId();
        comment.Status = 'In Process';
        update comment;
        return new PageReference('/'+comment.Id);
    }
    
    public PageReference closeAndNotify() {
        String closingComments = comment.Closing_Comments__c; 
        if (closingComments  == null || closingComments == '') {            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Closing Comments cannot be empty.'));
            return null;            
        }
        
        comment.Status = 'Closed';
        update comment;
        
        try {
            insert new FeedComment(CommentBody = closingComments, FeedItemId = comment.Feed_Item__c);
            //insert new FeedItem(Body = closingComments, ParentId = comment.Knowledge_article__c);
        } catch(Exception e) {
            //CR 9173 Added by CCE Don't show the error message
            //if the original Chatter message is deleted and we try to reply to it we get the following exception message:
            //"Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY, invalid cross reference id: []"
            //if we get this message we will respond by returning without displaying the error message.
            if (e.getMessage().containsIgnoreCase('INVALID_CROSS_REFERENCE_KEY, invalid cross reference id:')) {
            	return new PageReference('/'+comment.Id);	//CR 9173 Added by CCE Don't show the error message
            }
            
            Apexpages.getMessages().clear();
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            //CR 9173 CCE this line no longer valid as we are filtering this error out.
            //Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'FeedComment could not be created, invalid Feed Item id on record.'));
            return null;
        }
        return new PageReference('/'+comment.Id);
    }
    
}