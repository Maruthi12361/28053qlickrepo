@isTest

/***************************************************************************************************************************************

	Changelog:
		2012-02-15  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
		2012-09-12  SAN		Handle multiple cases with various record type
****************************************************************************************************************************************/

private class TestPickUpCaseFromDetailPage 
{
	static testMethod void testPickUpCaseFromDetailPage()
	{
		QTTestUtils.GlobalSetUp();

		Customizations__c cc = new Customizations__c();
		cc.Pickup_Case_Status__c = 'In Process';
		insert cc;

    	final String IN_PROCESS = 'In Process';
    	final String STATUS_NEW = 'New';
    

		//Create test data
		//Create test User 
		//User testUser = new User(FirstName = 'Test1', 
                                 //LastName= 'User1',
                                 //Alias = 'tUser1', 
                                 //Email = 'testUser1@testorg.com', 
                                 //UserName = 'testUser1@testorg.com', 
                                 //EmailEncodingKey='UTF-8', 
                                 //LanguageLocaleKey='en_US', 
                                 //LocaleSidKey='en_US', 
                                 //TimeZoneSidKey='America/Los_Angeles',
                                 //ProfileId = testProfile.Id,
                                 //UserRoleId = testUserRole.Id);
		//insert testUser;
		//Create test data
        //Create test User using SFDC Recomendation to avoid duplicate username
      
		
		Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
		User thisUserForRunAs = [SELECT Id, Country FROM User WHERE Id =: UserInfo.getUserId()];
				System.runAs(thisUserForRunAs) 
		{


		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c qtc = new QlikTech_Company__c();
        qtc.name = 'SWE';
        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
        qtc.Country_Name__c = 'Sweden';
        qtc.CurrencyIsoCode = 'SEK';
		qtc.Subsidiary__c = testSubs1.id;
        insert qtc;

        QlikTech_Company__c QTComp = new QlikTech_Company__c(
                 Name = 'USA',
                 Country_Name__c = 'USA',
                 QlikTech_Company_Name__c = 'QlikTech Inc',      
				 Subsidiary__c = testSubs1.id 
        );
        Insert QTComp;

        //QTTestUtils.GlobalSetUp(); // duplicated
        QTTestUtils.SetupNSPriceBook('GBP'); 


			 User testUser = Util.createUser('Jon', 'Snow', testProfile.Id, testUserRole.Id);
			        insert testUser;
			//QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
			Account testAcnt = new Account(Name='Test1 Account1', OwnerId = testUser.Id,
			 QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, 
			 Billing_Country_Code__c = QTComp.Id);
			Test.startTest();
			insert testAcnt;
			
			//Create test Contact
			Contact testContact = new Contact(FirstName='Test1', LastName='Contact1', AccountId=testAcnt.Id, OwnerId=testUser.Id);
			insert testContact;
			
			//Create test Case
	        List<Case> testCases = new List<Case>();
	        
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000Ddyi'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DdyY'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DdyT'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000IE3k'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000Ddp2'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DZgI'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DZqG'));

	        insert testCases;			
			
	        //Positive testing data  
	        Set<Id> validTestCaseIds = new Set<Id>();
	        for(Case c:testCases)
	        {
	        	validTestCaseIds.add(c.Id);
	        }   			
//			Case testCase = new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000Ddp2');
//			insert testCase;
			
			
			
			//Call method
			String updateStatus = PickUpCaseFromDetailPage.pickupCase(testCases.get(0).Id);
			
			//Assert return status string
			System.assertEquals('success', updateStatus);			
			
			//Requery the Case
			//testCase = [SELECT Id, Status, OwnerId FROM Case WHERE Id =: testCase.Id];
			testCases = [SELECT Id, Status, OwnerId FROM Case WHERE Id IN : validTestCaseIds];
			
			//Assert that Case Status is 'In Process' 
			//System.assertEquals('In Process', testCase.Status);
			//rdz changes due to CR# 5396 ServiceDesk Modifcations Summer 2012
			System.assertEquals(STATUS_NEW, testCases.get(0).Status);
			System.assertEquals(STATUS_NEW, testCases.get(1).Status);
			System.assertEquals(STATUS_NEW, testCases.get(2).Status);
			System.assertEquals(STATUS_NEW, testCases.get(3).Status);
			System.assertEquals(STATUS_NEW, testCases.get(4).Status);
			System.assertEquals(STATUS_NEW, testCases.get(5).Status);
			System.assertEquals(STATUS_NEW, testCases.get(6).Status);
			
			//Assert that Case Owner is now logged in User
			System.assertEquals(UserInfo.getUserId(), testCases.get(0).OwnerId);
						
			Test.stopTest();
		}                                 
	} 
	
}