/************************************************************************

    sfUtils
    
    Wrapper class for the sfUtils webservice    
        
    Changelog:
        2011-02-14  MHG     CR# 1447 - Extract the Last Udpated Opp code
                            https://eu1.salesforce.com/a0CD000000CAteX
        2011-05-16  MHG     CR# 229 - Navision Salesperson to equal SFDC Opportunity Owner
                            https://eu1.salesforce.com/a0C2000000AO4rq                              
        2013-12-05  TJG     CR# 10211
                            https://eu1.salesforce.com/a0CD000000detnE
        2014-02-02  SLH     Massive read timeout issues in live - mainly sfutil
                            Reducing to 20 seconds to see if this helps the issue                   
                                                
*************************************************************************/ 
public class sfUtils 
{

    @future (callout=true)
    public static void LastModifiedOpportunityTrigger(List<string> Opportunities)
    {
        if (Opportunities.size() == 0)
        {
            System.debug('sfUtils.LastModifiedOpportunityTrigger - Nothing to do, returning');
            return;
        }
        
        sfUtilsQlikviewCom.ServiceSoap service = new sfUtilsQlikviewCom.ServiceSoap();
        sfUtilsQlikviewCom.ArrayOfString Strings = new sfUtilsQlikviewCom.ArrayOfString();
        Strings.string_x = Opportunities;
        // CR# 10211 Increase timeout limit to maximum 120 seconds 
        // SLH reduced to 20 seconds to see if it helps the massive sfutils timeout issue in live
        service.timeout_x = 20000;      
        service.LastModifiedOpportunityTrigger(Strings); 
    } 
    
    @future (callout=true)
    public static void UpdateIntegrationOwner(List<string> Opportunities)
    {
        if (Opportunities.size() == 0)
        {
            System.debug('sfUtils.UpdateIntegrationOwner - Nothing to do, returning');
            return;
        }
        
        sfUtilsQlikviewCom.ServiceSoap service = new sfUtilsQlikviewCom.ServiceSoap();
        sfUtilsQlikviewCom.ArrayOfString Strings = new sfUtilsQlikviewCom.ArrayOfString();
        Strings.string_x = Opportunities;
        // CR# 10211 Increase timeout limit to maximum 120 seconds 
        // SLH reduced to 20 seconds to see if it helps the massive sfutils timeout issue in live
        service.timeout_x = 20000;      
        service.UpdateIntegrationOwner(Strings);    
    }   

}