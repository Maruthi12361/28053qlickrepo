/**     * File Name: PartnerFocusAreaHandler
        * Description : This handler class is used to update the account Indutries fields on insert,update or delete of Industry field on Partner Focus Area records
        * @author : Pramod Kumar V
        * Jira Id : PAEX - 220
        * Modification Log :
        
        Ver     Date         Author            
        1      31/07/2017  Pramod Kumar V   
     
*/
public class PartnerFocusAreaHandler{

    /*  @Description :This common method is used to update account field after insert of PartnerFocusArea record

        @parameter inputList: Trigger.new Account list
        @parameter newMap   : Map PartnerFocusArea id and records.  
    */
    public static void onAfterInsert(List<Partner_Focus_Area__c> inputList, Map<Id, Partner_Focus_Area__c> newMap) {
     
      List<Partner_Focus_Area__c> listToProcess = new List<Partner_Focus_Area__c> ();
      for(Partner_Focus_Area__c item : inputList) {
        if(String.isNotBlank(item.Industry__c))
          listToProcess.add(item);
    }
        if(listToProcess.size() > 0) {
            processPartnerFocus(listToProcess);
        }
        
    }

    /*  @Description :This common method is used to update account field after insert of PartnerFocusArea record

        @parameter inputList: Trigger.new Account list
        @parameter newMap   : Map PartnerFocusArea id and records.
        @parameter oldMap   : Old version Map PartnerFocusArea id and records.  
    */
    public static void onAfterUpdate(List<Partner_Focus_Area__c> inputList, Map<Id, Partner_Focus_Area__c> newMap, Map<Id, Partner_Focus_Area__c> oldMap) {
       List<Partner_Focus_Area__c> listToProcess = new List<Partner_Focus_Area__c> ();
        for(Partner_Focus_Area__c itemSpec : inputList) {
            if(itemSpec.Industry__c != oldMap.get(itemSpec.Id).Industry__c) {
                listToProcess.add(itemSpec);
            }
            if(String.isNotBlank(itemSpec.Industry__c)){
            listToProcess.add(itemSpec);
            }
        }
        if(listToProcess.size() > 0) {
            processPartnerFocus(listToProcess);
        }
    }


    /*  @Description :This common method is used to update account field after insert of PartnerFocusArea record

        @parameter inputList: Trigger.new Account list
        @parameter newMap   : Map PartnerFocusArea id and records.  
    */
    public static void onAfterDelete(List<Partner_Focus_Area__c> inputList, Map<Id, Partner_Focus_Area__c> newMap) {
        processPartnerFocus(inputList);
    }
    
    private static void processPartnerFocus(List<Partner_Focus_Area__c> inputList) {
        Set<Id> setOfAccs = new Set<Id>();
        for(Partner_Focus_Area__c focItem: inputList){
          if(focItem.Partner_Account__c != null)
              setOfAccs.add(focItem.Partner_Account__c);
        }
        if(!setOfAccs.isEmpty()){
          Map<Id, Account> mapOfAccs = new Map<Id, Account>([SELECT id, RecordType.Name,Industries__c, (SELECT Id, Partner_Account__c,Industry__c FROM Partner_Focus_Area__r)  FROM Account WHERE Id in :setOfAccs]);
          Map<Id, Set<String>> mapOfAccountSpecs = new Map<Id, Set<String>>();
          for(Id accountItem : mapOfAccs.keySet()) {
          //if(mapOfAccs.get(accountItem).Recordtype.Name=='Partner Account'||mapOfAccs.get(accountItem).Recordtype.Name=='OEM Partner Account')
              mapOfAccs.get(accountItem).Industries__c= '';
              for(Partner_Focus_Area__c itemSpec : mapOfAccs.get(accountItem).Partner_Focus_Area__r) {                
                      if(!mapOfAccountSpecs.containsKey(accountItem)) {
                          mapOfAccountSpecs.put(accountItem, new Set<String>{itemSpec.Industry__c});
                      }else
                        mapOfAccountSpecs.get(accountItem).add(itemSpec.Industry__c);                
              }
          }
          for(Id accountItem : mapOfAccountSpecs.keySet()) {
              List<String> listOfSpecs = new List<String>();
              listOfSpecs.addAll(mapOfAccountSpecs.get(accountItem));
              for(String itemSpecString : listOfSpecs) {
                  mapOfAccs.get(accountItem).Industries__c += itemSpecString + ';';
              }
          }
          if(mapOfAccs.values().size() > 0){
              update mapOfAccs.values();
          }
        }
    } 
}