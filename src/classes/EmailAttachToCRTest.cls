/*******************************************************************
* Class EmailAttachToCR
*
* Class controlling the creation of a note when an email is sent to cr@qlikview.com when the subject line contains CR#
* 
* Change Log:
* 2012-10-15   Fluido (Alberto de Diego)        Initial Development
* 2012-11-19   RDZ                              CR# 6685 Add sender and receiver info on "Email to CR" notes
*                                               https://eu1.salesforce.com/a0CD000000U6yt1
*
* 2013-08-13   CCE  Change to test class as Simon McAllister is on holiday (a validation rule has had a name
*                   change from Simon to Josefine so we need to use her ID) 
* 2013-09-12   SAN  CR#9570 to undo previous CCE CR#9280 Change to test class as Simon McAllister is on holiday (a validation rule has had a name
*                   change from Simon to Josefine so we need to use her ID) 
*
**********************************************************************/


@isTest
private class EmailAttachToCRTest {
    static testmethod void test() {
        // Create a new email and envelope object
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        SLX__Change_Control__c change = new SLX__Change_Control__c(
        //Business_Owner__c = '00520000001Ce2K',  //use Josefine Elmqvist as
        Business_Owner__c = '005D0000001qrhb',    //Simon McAllister is on holiday
        Business_Area__c = 'Sales');
        insert change;
        change = [SELECT CR_Number2__c, Name, Id FROM SLX__Change_Control__c WHERE Id = :change.Id];  
        
        // Create the email body with an Id
        email.toAddresses = new String[] {'testemail <testemail@test.com>'};
        email.plainTextBody = 'some text';
        email.subject = change.CR_Number2__c;
        email.fromAddress ='test@test.com';
        String contactEmail = 'jsmith@test.com';
        email.ccAddresses = new String[] {'Jon Smith <' + contactEmail + '>'};        
        
        EmailAttachToCR emailAttachToCR = new EmailAttachToCR();
        
        Messaging.InboundEmailResult result = emailAttachToCR.handleInboundEmail(email, env);
        
        List<Note> notes = [SELECT Id, Body, Title FROM Note WHERE ParentId = :change.Id];
        String body = notes[0].Body;
        System.assert(body.contains(email.plainTextBody));
        System.debug('----------------------------');
        System.debug('Body: ' + body);
        System.debug('ToAddress: ' + email.fromAddress);
        System.debug('----------------------------');
        
        
        //System.assert(body.contains(env.toAddress));
        
        System.assert(body.contains(email.fromAddress));
        for (string ad : email.ccAddresses)
        {
            System.assert(body.contains(ad));
        }
        
        for (string ad : email.toAddresses)
        {
            System.assert(body.contains(ad));
        }
        System.assertEquals(notes[0].Title, email.subject);
        
    }
}