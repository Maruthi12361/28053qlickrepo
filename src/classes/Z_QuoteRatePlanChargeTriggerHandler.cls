public class Z_QuoteRatePlanChargeTriggerHandler {

    public static void updateCharges(List<zqu__QuoteRatePlanCharge__c> summaries){
        Map<String, Zuora__SubscriptionProductCharge__c> chargeMapping = new Map<String, Zuora__SubscriptionProductCharge__c>();
        List<String> chargeIds = new List<String>();
        for (zqu__QuoteRatePlanCharge__c summary : summaries){
            chargeIds.add(summary.zqu__SubscriptionRatePlanChargeZuoraId__c);
        }
        
        List<Zuora__SubscriptionProductCharge__c> charges = [SELECT Id, ControlNumber__c, LicenseKey__c, Zuora__External_Id__c FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__External_Id__c in :chargeIds];

        if (!charges.isEmpty()){
            for (Zuora__SubscriptionProductCharge__c charge : charges){
                chargeMapping.put(charge.Zuora__External_Id__c, charge);
            }
            
            for (zqu__QuoteRatePlanCharge__c summary : summaries){
                Zuora__SubscriptionProductCharge__c currentCharge = chargeMapping.get(summary.zqu__SubscriptionRatePlanChargeZuoraId__c);
                if (currentCharge != null){
                    summary.ControlNumber__c = currentCharge.ControlNumber__c;
                    summary.LicenseKey__c = currentCharge.LicenseKey__c;
                }
            }
        }
    }
    
}