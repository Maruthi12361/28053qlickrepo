@isTest
private class ForecastReviewCreationTest {

    @isTest (seeAllData = true)
    private static void testHandler() {
        
        //seeAllData, won't work in dev boxes
        List<pse__Forecast_Enhanced_Summary_Category__c> sumCats = [select pse__Forecast_Enhanced_Summary__r.pse__Forecast_Enhanced_Calculation__c from pse__Forecast_Enhanced_Summary_Category__c where pse__Forecast_Enhanced_Summary__r.pse__Forecast_Enhanced_Calculation__c!= null]; 
        if(sumCats.Size() > 0){
            
            pse__Forecast_Enhanced_Summary_Category__c sumCat = sumCats[0];
            pse__Forecast_Enhanced_Calculation__c calc = [select id from pse__Forecast_Enhanced_Calculation__c where id = :sumCat.pse__Forecast_Enhanced_Summary__r.pse__Forecast_Enhanced_Calculation__c];
            
            
            test.StartTest();
        
            PageReference pageRef = Page.ForecastReviewCreation;
            pageRef.getParameters().put('id', String.valueOf(calc.Id));
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(calc);
            ForecastReviewCreationController controller = new ForecastReviewCreationController(sc);
            PageReference pageRef2 = controller.generateForecastReviews();
            
            test.StopTest();
        }
    } 
}