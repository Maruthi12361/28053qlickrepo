/*
* File RejectDuplicateULCDetailsOnContactTest
    * @description : Unit test for RejectDuplicateULCDetailsOnContactLead
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       21.12.2017   Pramod Kumar V         Created Class

*/
@isTest(seeAlldata=false)
private class RejectDuplicateULCDetailsOnContactTest{
    
   
    static testMethod void test_RejectDuplicateULCDetailsOnContact() {
     Id rtId2= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
           Subsidiary__c subs = QuoteTestHelper.createSubsidiary();    
           QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
           Account testAccount = new Account(QlikTech_Company__c ='QTcomp', name='Test AccountName',recordtypeid=rtId2,BillingCountry='India',Payment_Terms__c = '45 days',Navision_Status__c = 'Partner');
           insert testAccount;
           
           Contact conPartner = QuoteTestHelper.createContact(testAccount.id);
         // conPartner.TriggerPortalControl__c = true;
          insert conPartner;
          
            Lead lead = new Lead (
            FirstName = 'Lead',
            LastName = 'Test1001',
            Company = 'Test',
            Country = 'Sweden',
            Email = 'asd@ad.com',
            Zift_Lead_Partner__c = testAccount.Id,
        Zift_Distribution_Status__c = 'Send to Zift',
        Zift_Rejection_Reason__c = 'No Capacity',
        Zift_Unqualified_Invalid_Reason__c = 'No Budget or Interest',
        Zift_Sales_Rep_Email__c = 'partner@test.com',
        Zift_Sales_Rep_Name__c = 'Partner',
        Zift_Registered_Lead__c = true,
        Zift_SSO_CompanyID__c = testAccount.Id,
        Zift_Partner_Id__c = '123456',
        Zift_Partner_Name__c = 'Partner Name'
        );  
        insert lead;  
                     
          ULC_Details__c Detail =new ULC_Details__c (ULCName__c='test', ULC_Password__c ='test',ContactId__c=conPartner.id,LeadId__c=lead.id );
           insert Detail ;
           
           
           Contact conPartner1 = QuoteTestHelper.createContact(testAccount.id);
         // conPartner.TriggerPortalControl__c = true;
          insert conPartner1;
          
            Lead lead1 = new Lead (
            FirstName = 'Lead',
            LastName = 'Test1001',
            Company = 'Test',
            Country = 'Sweden',
            Email = 'asd@ad.com',
            Zift_Lead_Partner__c = testAccount.Id,
        Zift_Distribution_Status__c = 'Send to Zift',
        Zift_Rejection_Reason__c = 'No Capacity',
        Zift_Unqualified_Invalid_Reason__c = 'No Budget or Interest',
        Zift_Sales_Rep_Email__c = 'partner@test.com',
        Zift_Sales_Rep_Name__c = 'Partner',
        Zift_Registered_Lead__c = true,
        Zift_SSO_CompanyID__c = testAccount.Id,
        Zift_Partner_Id__c = '123456',
        Zift_Partner_Name__c = 'Partner Name'
        );  
        insert lead1;
        
        ULC_Details__c Detail1 =new ULC_Details__c (ULCName__c='test1', ULC_Password__c ='test1',LeadId__c=lead1.id,ContactId__c=conPartner1.id );
        insert Detail1 ;
          
    
    }
    
    }