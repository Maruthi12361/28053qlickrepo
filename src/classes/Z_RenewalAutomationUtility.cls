/**
 * File Name : Z_RenewalAutomationUtility
 * Description : This is a utility class shared by Z_RenewalOpportunityJob and Z_RenewalQuoteInitJob classes.
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

public with sharing class Z_RenewalAutomationUtility {

    private static final String CLASS_NAME = 'Z_RenewalAutomationUtility',
                                DEBUG_PREFIX = '> $ ' + CLASS_NAME;
    
    public static Map<String, Map<String, Object>> getSubscriptionFieldsMap() {
        Renewal_Automation_Settings__c settings = Renewal_Automation_Settings__c.getOrgDefaults();
        Map<String, Map<String, Object>> subscriptionFieldsMap = new Map<String, Map<String, Object>>();

        for (Zuora__Subscription__c s : [SELECT Id, Zuora__External_Id__c, Zuora__SubscriptionNumber__c, ACV__c, Zuora__SubscriptionEndDate__c,
                                                Zuora__TermSettingType__c, Zuora__SubscriptionStartDate__c, Zuora__NextRenewalDate__c, 
                                                Zuora__CustomerAccount__r.Zuora__AccountNumber__c, Zuora__Version__c, Zuora__InitialTerm__c,
                                                Zuora__InvoiceOwner__r.Zuora__Zuora_Id__c, Zuora__InvoiceOwner__r.Name
                                         FROM Zuora__Subscription__c 
                                         WHERE Zuora__Status__c = 'Active' 
                                         AND Digital__c = 'No'
                                         AND Zuora__TermEndDate__c >= :settings.SubscriptionEndDateBound__c]) {
            
            String subscriptionNumber = s.Zuora__SubscriptionNumber__c;
            Decimal acv = (null == s.ACV__c) ? 0 : Decimal.valueOf(s.ACV__c);
            Decimal initialTerm = (null == s.Zuora__InitialTerm__c) ? 0 : Decimal.valueOf(s.Zuora__InitialTerm__c.replaceAll('[^0-9]', '').trim());
            Map<String, Object> subscriptionFields = new Map<String, Object> {
                'Id' => s.Id,
                'Zuora__External_Id__c' => s.Zuora__External_Id__c,
                'Zuora__SubscriptionNumber__c' => subscriptionNumber,
                'ACV__c' => acv,
                'Zuora__SubscriptionEndDate__c' => s.Zuora__SubscriptionEndDate__c,
                'Zuora__TermSettingType__c' => s.Zuora__TermSettingType__c,
                'Zuora__SubscriptionStartDate__c' => s.Zuora__SubscriptionStartDate__c,
                'Zuora__NextRenewalDate__c' => s.Zuora__NextRenewalDate__c, 
                'Zuora__CustomerAccount__r.Zuora__AccountNumber__c' => s.Zuora__CustomerAccount__r.Zuora__AccountNumber__c,
                'Zuora__InvoiceOwner__r.Zuora__Zuora_Id__c' => s.Zuora__InvoiceOwner__r.Zuora__Zuora_Id__c,
                'Zuora__InvoiceOwner__r.Name' => s.Zuora__InvoiceOwner__r.Name,
                'Zuora__Version__c' => s.Zuora__Version__c,
                'Zuora__InitialTerm__c' => initialTerm
            };

            subscriptionFieldsMap.put(subscriptionNumber, subscriptionFields);

            System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' ' + subscriptionFields);
        }

        return subscriptionFieldsMap;
    }

    public static List<String> getFieldApiNames(DescribeSObjectResult describeResult, String fieldSetApiName) {
        Schema.FieldSet fieldSetObject = describeResult.FieldSets.getMap().get(fieldSetApiName);
        List<String> fieldApiNames = new List<String>();

        for (Schema.FieldSetMember member : fieldSetObject.getFields()) {
            fieldApiNames.add(member.getFieldPath());
        }

        return fieldApiNames;
    }
}