public with sharing class InfluencereditpageController {
  
    private final Influencer__c infl;
    
    public InfluencereditpageController(ApexPages.StandardController stdController) {
        this.infl =(Influencer__c)stdController.getRecord();
    }
   
    
    
    public PageReference save() {   
       Contact c = [SELECT name FROM Contact WHERE Id = :Infl.Contact__c];
        infl.name=c.name;
        update Infl;
        PageReference newPage = New PageReference('/' + infl.id);
        newPage.setRedirect(true);
        return newPage;
    }
    
 
}