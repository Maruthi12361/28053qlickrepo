public with sharing class ForecastReviewCreationController {

    private final pse__Forecast_Enhanced_Calculation__c fec;

    public ForecastReviewCreationController(ApexPages.StandardController stdCont) {
        this.fec = (pse__Forecast_Enhanced_Calculation__c)stdCont.getRecord();
    }

    public PageReference generateForecastReviews() {
        System.debug('this worked: ' + fec);
        List<Id> fecIds = new List<Id>();
        fecIds.add(fec.Id);
        ForecastReviewHandler.processForecastReviewCreation(fecIds);
        return new PageReference('/' + fec.Id);
    }


}
