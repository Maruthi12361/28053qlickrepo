/*******************************************************************
* Class ChangeControlChangeManagerUpdate
*
* Test class for trigger ChangeControlChangeManagerUpdate
* 
* Change Log:
* 2014-06-25   AIN (Andreas Nilsson)	 	Initial Development
*											CR# 10110 Add Release Management to CR Process
**********************************************************************/
@isTest
private class ChangeControlChangeManagerUpdateTest {

	//Success test
	private static testmethod void Test1() {
		SLX__Change_Control__c change;
		List<SLX__Change_Control__c> changes = new List<SLX__Change_Control__c>();
		Id recordTypeITSystems;

		InitializeCustomSettings();

		recordTypeITSystems = [Select ID from RecordType where id = '01220000000IMuV' limit 1].Id;

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Infrastructure';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Integration';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Systems - Knowledge';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Systems - Relationship';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Systems - Corporate Services';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Systems - Revenue';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Installation';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Systems - Accounting';
		changes.add(change);

		
		
		DoTest(changes, true);
	}

	private static testmethod void Test2() {

		SLX__Change_Control__c change;
		List<SLX__Change_Control__c> changes = new List<SLX__Change_Control__c>();
		Id recordTypeITSystems;
		Id recordTypeProject;

		InitializeCustomSettings();
		

		recordTypeITSystems = [Select ID from RecordType where id = '01220000000IMuV' limit 1].Id;
		recordTypeProject = [Select ID from RecordType where id = '01220000000I5iz' limit 1].Id;

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeProject;
		change.Type__c = 'Infrastructure';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeProject;
		change.Type__c = 'Integration';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeProject;
		change.Type__c = 'Systems - Knowledge';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Qonnect Portal';
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = null;
		changes.add(change);

		change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeITSystems;
		change.Type__c = 'Random';
		changes.add(change);
	
		DoTest(changes, false);
		
	}
	private static void DoTest(List<SLX__Change_Control__c> changes, boolean expected)
	{
		insert changes;

		List<string> ids = new List<string>();

		for(SLX__Change_Control__c c : changes)
		{
			ids.add(c.id);
		}

		changes = [select Id, Name, Type__c, RecordTypeId, Change_Manager__c from SLX__Change_Control__c where id in :changes];

		

		for(SLX__Change_Control__c c : changes)
		{
			system.debug(c.Type__c);
			system.debug(c.Change_Manager__c);
			if(c.Type__c == 'Infrastructure')
				system.assert((c.Change_Manager__c == '00520000001LMCT') == expected);
			else if(c.Type__c == 'Installation')
				system.assert((c.Change_Manager__c == '00520000000yk70') == expected);
			else if(c.Type__c == 'Integration')
				system.assert((c.Change_Manager__c == '00520000001PSbP') == expected);
			else if(c.Type__c == 'Systems - Corporate Services')
				system.assert((c.Change_Manager__c == '00520000000yunr') == expected);
			else if(c.Type__c == 'Systems - Knowledge')
				system.assert((c.Change_Manager__c == '005D0000002RaJJ') == expected);
			else if(c.Type__c == 'Systems - Relationship')
				system.assert((c.Change_Manager__c == '00520000001Oxq5') == expected);
			else if(c.Type__c == 'Systems - Revenue')
				system.assert((c.Change_Manager__c == '005D0000001sYjG') == expected);
			else if(c.Type__c == 'Systems - Accounting')
				system.assert((c.Change_Manager__c == null) == expected);
			else
				system.assert(!expected);
		}
	}
	private static void InitializeCustomSettings()
	{
		List<ChangeControlChangeManager__c> customSettings = new List<ChangeControlChangeManager__c>();

		ChangeControlChangeManager__c customSetting = new ChangeControlChangeManager__c();
		customSetting.Name = 'Infrastructure';
		customSetting.RecordType__c = '01220000000IMuV;IT/Systems';
		customSetting.Type__c = 'Infrastructure';
		customSetting.ChangeManager__c = '00520000001LMCT;Niclas Rävås';
		customSettings.add(customSetting);

		customSetting = new ChangeControlChangeManager__c();
		customSetting.Name = 'Installation';
		customSetting.RecordType__c = '01220000000IMuV;IT/Systems';
		customSetting.Type__c = 'Installation';
		customSetting.ChangeManager__c = '00520000000yk70;Mark Child';
		customSettings.add(customSetting);

		customSetting = new ChangeControlChangeManager__c();
		customSetting.Name = 'Integration';
		customSetting.RecordType__c = '01220000000IMuV;IT/Systems';
		customSetting.Type__c = 'Integration';
		customSetting.ChangeManager__c = '00520000001PSbP;Ted Sager';
		customSettings.add(customSetting);

		customSetting = new ChangeControlChangeManager__c();
		customSetting.Name = 'Systems - Corporate Services';
		customSetting.RecordType__c = '01220000000IMuV;IT/Systems';
		customSetting.Type__c = 'Systems - Corporate Services';
		customSetting.ChangeManager__c = '00520000000yunr;Daryl Wilson-Hemingway';
		customSettings.add(customSetting);

		customSetting = new ChangeControlChangeManager__c();
		customSetting.Name = 'Systems - Knowledge';
		customSetting.RecordType__c = '01220000000IMuV;IT/Systems';
		customSetting.Type__c = 'Systems - Knowledge';
		customSetting.ChangeManager__c = '005D0000002RaJJ;Wiveka Göransson';
		customSettings.add(customSetting);

		customSetting = new ChangeControlChangeManager__c();
		customSetting.Name = 'Systems - Relationship';
		customSetting.RecordType__c = '01220000000IMuV;IT/Systems';
		customSetting.Type__c = 'Systems - Relationship';
		customSetting.ChangeManager__c = '00520000001Oxq5;Patrick Merstetter';
		customSettings.add(customSetting);

		customSetting = new ChangeControlChangeManager__c();
		customSetting.Name = 'Systems - Revenue';
		customSetting.RecordType__c = '01220000000IMuV;IT/Systems';
		customSetting.Type__c = 'Systems - Revenue';
		customSetting.ChangeManager__c = '005D0000001sYjG;Stuart Sleight';
		customSettings.add(customSetting);

		insert customSettings;
	}
	
}