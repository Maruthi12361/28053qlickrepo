/****************************************************************
*
*  Class: LeadPRMReferralUpdateContactFieldHandler
*
*  Trigger to update the hidden field PRM_Referring_Contact_new__c on the PRM Referral Opp Reg
*  record type from the Partner Contact. This is being done to get round the issue of SFDC not 
*  letting us do two Lookups on the Partner Contact field.
*
*  2012-05-31 CCE : Trigger created - Test method is in Test_Triggers_PRM_Referral
*  2017-10-25 AYS BMW-402 : Migrated from Lead_PRMReferral_UpdatePRMReferringContactField.trigger.
* 
*****************************************************************/

public class LeadPRMReferralUpdateContactFieldHandler {
	public LeadPRMReferralUpdateContactFieldHandler() {
		
	}

	public static void handle(List<Lead> triggerNew) {
		system.debug('Lead_PRMReferral_UpdatePRMReferringContactField: Starting');

		for (Lead o : triggerNew)
		{
			if ((o.Partner_Contact__c !=null) && (o.RecordTypeId == '012D0000000K781IAC'))	//PRM Referral Opp Reg record type qttest=012L00000004JyJ live=012D0000000K781IAC
			{
				o.PRM_Referring_Contact_new__c = o.Partner_Contact__c;
			}
		}
		system.debug('Lead_PRMReferral_UpdatePRMReferringContactField: Finishing');
	}
}