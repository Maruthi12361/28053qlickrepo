/**********************************************Account

    Class: AccountLicensesRelatedList
    
    Changelog:
        2016-02-02  IRN     Created file
        2016-03-14  IRN     CR33068 implemented interfaces batchable and scheduable to be able to go through all existing data in the system and update 
                            one item on the contract with account license info for the unique comination of account licenses and contract 
        2016-04-04  IRN     Fix for populating the ns support contract item that has a support end date if it exist, if not populate an item with no support end date
        2017-02-07  IRN     CR# 104657 Added null check if(items[i].Contract_Item_Account_License__c != null) 
                            
******************************************************/
global class AccountLicensesRelatedList implements Database.Batchable<sObject>,schedulable{

  public static boolean isUpdated = false;
  public static Set<String> itemAccountLicenseCombination = New Set<String>();

  global void execute (SchedulableContext sc)
  {
    System.Debug('--excecute--');
        ID batchId = Database.executeBatch(this); 
  }

  global Database.QueryLocator start(Database.BatchableContext bc){
        System.Debug('Starting: batchable account licenses');
        return Database.getQueryLocator([Select Id, Contract_Item_Account_License__c, Account_License_for_RL__c, NS_Support_Contract__c, Contract_Item_End_Date__c from NS_Support_Contract_Item__c where Contract_Item_Account_License__c != null order by Contract_Item_Account_License__c, Contract_Item_End_Date__c DESC NULLS LAST]);
  }

  global void execute(Database.BatchableContext BC, List<sObject> scope){
    List<NS_Support_Contract_Item__c> nsContractItems = (List<NS_Support_Contract_Item__c>)scope;
    List<NS_Support_Contract_Item__c> tobeupdate = new List<NS_Support_Contract_Item__c>();
    Map<String, Id> accountLicenseMap = new Map<String, Id>();
    try{
        for(Integer i = 0; i<nsContractItems.size(); i++){
        System.debug('AccountLicensesRelatedList  populateList' + nsContractItems[i].Contract_Item_Account_License__c);
        String key = nsContractItems[i].Contract_Item_Account_License__c + ' ' +nsContractItems[i].NS_Support_Contract__c;
        System.debug('accountLicenseMap.containsKey(key) ' + accountLicenseMap.containsKey(key));
        System.debug('Account_License_for_RL__c ' + nsContractItems[i].Account_License_for_RL__c);
        if(accountLicenseMap.containsKey(key) || itemAccountLicenseCombination.contains(key)){
          nsContractItems[i].Account_License_for_RL__c = null;
          tobeupdate.add(nsContractItems[i]);
        }else{
          nsContractItems[i].Account_License_for_RL__c = nsContractItems[i].Contract_Item_Account_License__c;
          accountLicenseMap.put(key, nsContractItems[i].Id);
          itemAccountLicenseCombination.add(key);
          tobeupdate.add(nsContractItems[i]);
        } 
      }
      if(tobeupdate != null && tobeupdate.size()>0){
        System.debug('tobeupdate ' + tobeupdate);
        Integer BATCHSIZE = 200;
        
        for (Integer i = 0; i < (tobeupdate.Size() / BATCHSIZE) + 1; i++)
        {
            List<NS_Support_Contract_Item__c> tempList;
            if (tobeupdate.size() > ((i + 1) * BATCHSIZE))
            {
                tempList = getRange(tobeupdate, i * BATCHSIZE, BATCHSIZE);
            }
            else
            {
                tempList = getRange(tobeupdate, i * BATCHSIZE, tobeupdate.size() - (i * BATCHSIZE));
            }
            System.debug('NS_Support_Contract_Item__c to update ' + tempList.size());
            update tempList;
        }
      } 

    }catch(Exception e){
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
      String[] toAddresses = new String[] {a.CreatedBy.Email};
      sendEmail(e, nsContractItems, toAddresses);
    }
  }

  public void sendEmail(Exception e, List<NS_Support_Contract_Item__c> nsContractItems,String[] toAddresses){
     //Email what has gone wrong
      System.debug('ERROR ' + e.getMessage());
      Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
      mail.setToAddresses(toAddresses);
      mail.setSenderDisplayName('Apex error message AccountLicenseRelatedList');
      mail.setSubject('Apex error message AccountLicenseRelatedList: ' + UserInfo.getOrganizationName());
      String body = ' id:';
      for(Integer i = 0; i<nsContractItems.size(); i++){
        body = body +  ' ' + nsContractItems.get(i).Id + ', ';
      }
      mail.setPlainTextBody('Error: ' + e.getMessage() + '. NS support contract items ids=:' + body);
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
  }

  global void finish(Database.BatchableContext bc){
  }

  public List<NS_Support_Contract_Item__c> getRange(List<NS_Support_Contract_Item__c> fromList, Integer fromIndex, Integer amount)
  {
      List<NS_Support_Contract_Item__c> returnList = new List<NS_Support_Contract_Item__c>();
      for(Integer i = fromIndex; i < fromIndex + amount; i++){
          returnList.add(fromList.get(i));
      }
      return returnList;
  }

  public void populateList(List<NS_Support_Contract_Item__c> items){
    isUpdated = true;
    Set<Id> accountLicenseIds = new Set<Id>();
    for(Integer i = 0; i<items.size(); i++){
      if(items[i].Contract_Item_Account_License__c != null){
        accountLicenseIds.add(items[i].Contract_Item_Account_License__c);
      }
    }
    System.debug('accountLicenseIds ' + accountLicenseIds + ' items ' + items);
    List<NS_Support_Contract_Item__c> nsContractItems  = [Select Id, Contract_Item_Account_License__c, 
    Account_License_for_RL__c, NS_Support_Contract__c, Contract_Item_End_Date__c from NS_Support_Contract_Item__c where Contract_Item_Account_License__c in : accountLicenseIds order by Contract_Item_Account_License__c, Contract_Item_End_Date__c DESC NULLS LAST];
    
    List<NS_Support_Contract_Item__c> tobeupdate = new List<NS_Support_Contract_Item__c>();
    Map<String, Id> accountLicenseMap = new Map<String, Id>();
    for(Integer i = 0; i<nsContractItems.size(); i++){
      System.debug('AccountLicensesRelatedList  populateList' + nsContractItems[i].Contract_Item_Account_License__c);
      if(nsContractItems[i].Contract_Item_Account_License__c != null){
        String key = nsContractItems[i].Contract_Item_Account_License__c + ' ' +nsContractItems[i].NS_Support_Contract__c;
        System.debug('accountLicenseMap.containsKey(key) ' + accountLicenseMap.containsKey(key));
        System.debug('Account_License_for_RL__c ' + nsContractItems[i].Account_License_for_RL__c);
        if(accountLicenseMap.containsKey(key)){
          nsContractItems[i].Account_License_for_RL__c = null;
          tobeupdate.add(nsContractItems[i]);
        }else{

          nsContractItems[i].Account_License_for_RL__c = nsContractItems[i].Contract_Item_Account_License__c;
          accountLicenseMap.put(key, nsContractItems[i].Id);
          tobeupdate.add(nsContractItems[i]);
        }
      }else{
        nsContractItems[i].Account_License_for_RL__c = null;
        tobeupdate.add(nsContractItems[i]);
      }
      
    }
    if(tobeupdate != null && tobeupdate.size()>0){
      System.debug('tobeupdate ' + tobeupdate);
      update tobeupdate;  
    }
  }
}

/*
  To update all existing account licenses with existing NS_Support_Contract_Item__c
  run 
  AccountLicensesRelatedList a = new AccountLicensesRelatedList();
  String sch = '0 0 11 * * ?';
  String jobID = system.schedule('AccountLicensesRelatedList Job', sch, a);
*/