/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_License_DownloadLEF_Cntrl {

    public String letterBody {get; set;}
    public String resultsLicenses {get; set;}
    private Integer Chk;
    public QS_License_DownloadLEF_Cntrl() {

        resultsLicenses = ApexPages.CurrentPage().getparameters().get('licenseNames');
        letterBody = '';
        if(resultsLicenses != null && resultsLicenses != '') {
            if(resultsLicenses.contains('-regex-')) {
                for(String licName : resultsLicenses.split('-regex-')) {
                    letterBody += generateLEF(licName) + '\r\n<br></br>';
                }   
            } else {
                letterBody = generateLEF(resultsLicenses);
            }
        } else {
            letterBody = 'No_License_selected';
        }
        System.debug('%%%%letterBody: '+letterBody);
        getLetterLines();
    }

    public String generateLEF(String myLicenseName) {
        String responseBody = '';
        system.debug('generateLEF start');
        system.debug('myLicenseName: ' +myLicenseName );
        try {
            if(myLicenseName != null && myLicenseName != '') {
                Chk = 4711;
                string cause = '203';
                String licName = myLicenseName;
                licName = licName.substring (0, 4) + ' ' +licName.substring (4, 8) + ' ' + licName.substring (8, 12) + ' ' + licName.substring(12, 16);
                System.debug('%%%%licName: '+licName);

                simpleCheck16_impl (licName);
                simpleCheck16_impl (cause);
                String licNameUrlSafe = licName.replace(' ', '%20');

                Http http = new Http();
                HttpRequest req = new HttpRequest();
                String urlFinal = System.label.QS_LEF_Endpoint + licNameUrlSafe + '&ver=&user=&org=&os=&cause=' + cause + '&chk=' + String.valueof(Chk);
                req.setEndpoint(urlFinal);req.setMethod('GET');
                req.setTimeout(6000);
                
                HttpResponse res;
                if(!Test.isRunningTest())
                    res = http.send(req);
                if(res != null)
                    responseBody = res.getBody();
                else
                    responseBody = null;
                
                System.debug('%%%%responseBody: '+responseBody);
                return responseBody;
            }
            
        }

        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
        return responseBody;
    }

    public List<String> getLetterLines() {
        if (letterBody == null) {
            return new List<String>();
        }
        return letterBody.split('\\s+');
    }

    @TestVisible
    private void simpleCheck16_impl (String Buf) {
        for (integer i = 0; i < Buf.length(); i ++) {
            Chk *= 2;
            if (Chk >= 65536) {
                Chk -= 65535;
            }
            Chk ^= Buf.codePointAt(i);
        }
    }
}