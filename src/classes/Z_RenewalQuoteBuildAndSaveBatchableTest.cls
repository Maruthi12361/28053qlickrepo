/**     
 * File Name : Z_RenewalQuoteBuildAndSaveBatchableTest
 * Description : This is a test class to test the Z_RenewalQuoteBuildAndSaveBatchable class with following test methods:
 *               - renewalQuoteBuildAndSaveJobTest
 * @author : Inki Hong
 * Modification Log ======================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

@IsTest
private class Z_RenewalQuoteBuildAndSaveBatchableTest {
    private static Account testAcc;

    private static Contact testContact;

    private static Zuora__CustomerAccount__c testCustomerAcc;

    private static Opportunity testOppty;

    private static Zuora__Subscription__c testSubscription;

    private static zqu__Quote__c testQuote;

    private static QlikTech_Company__c testqtComp;

    private static Q2CWSettings__c testQ2cwSettings;

    private static Steelbrick_Settings__c testSteelbrickSettings;

    static void setup() {
        testQ2cwSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();

        testqtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOppty = Z_RenewalTestFactory.makeRenewalOpportunity(testAcc);
        testOppty.CloseDate = Date.today();
        testSubscription = Z_RenewalTestFactory.makeSubscription();

        testAcc.QlikTech_Company__c = testqtComp.name;
        testAcc.Billing_Country_Code__c  = testqtComp.id;

        List<sObject> tests = new List<sObject>{
            testQ2cwSettings, testSteelbrickSettings
        };

        insert tests;

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOppty, testSubscription
        };

        insert testObjects;

        testQuote = Z_RenewalTestFactory.makeQuote3(testAcc, testContact, testOppty, testCustomerAcc);

        System.debug('IH: quote id = ' + testQuote.Id);

        zqu__QuoteChargeDetail__c qcd1 = Z_RenewalTestFactory.makeQCD(testQuote);

        System.debug('IH: quote id of qcd1 = ' + qcd1.zqu__Quote__c);

        insert qcd1;
    }

    @IsTest
    static void renewalQuoteBuildAndSaveJobTest() {
        Semaphores.SetAllSemaphoresToTrue();
        
        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;

        setup();

        Test.startTest();
        
        List<Id> quoteIds = new List<Id> {
            testQuote.Id
        };

        Z_RenewalQuoteBuildAndSaveBatchable job = new Z_RenewalQuoteBuildAndSaveBatchable(quoteIds);

        Database.executeBatch(job);

        Test.stopTest();
    }
}