/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_License_DownloadLEF_CntrlTest {
	
	@isTest static void testSingleLicense() {

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

		Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '3106480607789576');
        insert productLicense;

		PageReference pageRef = Page.QS_License_DownloadLEF;
        pageRef.getParameters().put('licenseNames', '3106480607789576');
        Test.setCurrentPageReference(pageRef);

		QS_License_DownloadLEF_Cntrl controller = new QS_License_DownloadLEF_Cntrl();

		//licenseNames=3106480607789576-regex-3107360903027807
	}

	@isTest static void testMultipleLicenses() {

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

		Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '3106480607789576');
        Entitlement productLicense2 = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '3107360903027807');
        List<Entitlement> productLicenses = new List<Entitlement>();
        productLicenses.Add(productLicense);
        productLicenses.Add(productLicense2);

        insert productLicenses;

		PageReference pageRef = Page.QS_License_DownloadLEF;
        pageRef.getParameters().put('licenseNames', '3106480607789576-regex-3107360903027807');
        Test.setCurrentPageReference(pageRef);

		QS_License_DownloadLEF_Cntrl controller = new QS_License_DownloadLEF_Cntrl();

		//licenseNames=3106480607789576-regex-3107360903027807
	}

	@isTest static void testNoLicense() {

		PageReference pageRef = Page.QS_License_DownloadLEF;
        pageRef.getParameters().put('licenseNames', '');
        Test.setCurrentPageReference(pageRef);

		QS_License_DownloadLEF_Cntrl controller = new QS_License_DownloadLEF_Cntrl();

	}
	
	
}