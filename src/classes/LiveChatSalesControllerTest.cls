/**
* Test for LiveChatSalesController.
* 
* Change log:
* 2018-11-28	ext_vos	CHG0035115: add test for controller.
*/
@isTest
public class LiveChatSalesControllerTest {
	
	@TestSetup
	static void createTestData() {
		// init data
        Account a1 = new Account(Name = 'LCSCtest One');
        insert a1;
        Account a2 = new Account(Name = 'LCSCtest Two');
        insert a2;

        Profile profile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		List<User> users = [select Id from User where Name = 'Qliktech Webservice' limit 1];
		if (users.isEmpty()) {
			User user = new User(Username = 'LCSCtest@test.com', FirstName = 'QlikTech', LastName = 'Webservice', Email = 'LCSCtest@test.com', 
    						 Alias = 'tula', TimeZoneSidKey = 'Europe/Paris', LocaleSidKey = 'es_ES', 
    						 EmailEncodingKey = 'ISO-8859-1', ProfileId = profile.Id, LanguageLocaleKey = 'en_US'); 
			insert user;
		}
        QS_Qoncierge_Country_License_Support__c settings1 = new QS_Qoncierge_Country_License_Support__c();
        settings1.Name = 'TestOne';
        settings1.Temp_Account__c = a1.Id;
        insert settings1;
        QS_Qoncierge_Country_License_Support__c settings2 = new QS_Qoncierge_Country_License_Support__c();
        settings2.Name = 'TestTwo';
        settings2.Temp_Account__c = a2.Id;
        settings2.Is_country_popular__c = true;
        insert settings2;
    }
    
    static testMethod void testController() {
		LiveChatSalesController controller = new LiveChatSalesController();
		RecordType rt = [select Id from RecordType where SobjectType = 'Contact' and Name = 'Business Contact'];
		System.assertEquals(rt.Id, controller.recordTypeId);

		rt = [select Id from RecordType where SobjectType = 'Case' and Name = 'Live Chat Support Record Type'];
		System.assertEquals(rt.Id, controller.caseRecordTypeId);

		System.assertEquals('SUP - Support Request', controller.leadSource);

		Account a = [select Id from Account where Name = 'LCSCtest One'];
		System.assert(controller.countryOptions.contains('TestTwo'));
		System.assert(controller.countryOptions.contains(a.Id));

		User u = [select Id from User where Name = 'Qliktech Webservice' limit 1];
		System.assertEquals(u.Id, controller.ownerId);
    }
}