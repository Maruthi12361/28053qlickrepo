/********************************************************
* CLASS: WebActivityProduct
* DESCRIPTION: Handles Product relation to InvitedBy
*
*
* CHANGELOG:
*   2019-11-27 - BAD - BMW-1849 - Added Initial logic
*********************************************************/
public  class WebActivityProduct {

    public static List<WebActivityUtils.WebActivityContent> GetProductsForInvitedBy(List<WebActivityUtils.WebActivityContent> lstWAContent, List<String> lstInvitedBy) {
        system.debug('Function: GetProductsForInvitedBy');
        Map<String, product_user__c> mapInvitedByToProductUser = new Map<String, product_user__c>();
        Map<String, String> mapSubscriptionToProduct = new Map<String, String>();
        Set<Id> subscriptionIDs = new Set<Id>();        

        if (lstInvitedBy.size() > 0)
        {
            //Get Subscriptions by InvitedBySubject (qlikID on Contact)
            for (Product_user__c pu : [Select Contact__r.qlikid__c, Subscription__c, Zuora_Subscription_ID__c
                                        From product_user__c
                                        Where Contact__r.qlikid__c in : lstInvitedBy])
            {
                if(pu.Subscription__c != null){
                    mapInvitedByToProductUser.put(pu.Contact__r.qlikid__c, pu);
                    subscriptionIDs.add(pu.Subscription__c);
                }
            }

            System.debug('mapInvitedByToProductUser ' + mapInvitedByToProductUser);
            System.debug('subscriptionIDs ' + subscriptionIDs);

            //Find out which Products are related to Subscription
            for (Zuora__Subscription__c sub : [Select id, name, (select Zuora__ProductName_Product__c from Zuora__Subscription_Product_Charges__r)
                                                From Zuora__Subscription__c
                                                Where id in : subscriptionIDs])
            {
                string product = '';
                for(Zuora__SubscriptionProductCharge__c subProd : sub.Zuora__Subscription_Product_Charges__r)
                {
                    system.debug(subProd.Zuora__ProductName_Product__c);
        
                    if(subProd.Zuora__ProductName_Product__c != null)
                    {
                        if(subProd.Zuora__ProductName_Product__c.contains('Qlik Sense Enterprise'))
                        {
                            //prioritize QSE, we can stop looking
                            product = 'Qlik Sense Enterprise';
                            break;
                        }
                        else if(subProd.Zuora__ProductName_Product__c.contains('Qlik Sense Business'))
                        {
                            product = 'Qlik Sense Business';
                        }                    
                    }
                }
                if(string.isNotBlank(product) && !mapSubscriptionToProduct.containsKey(sub.id))
                {
                    mapSubscriptionToProduct.put(sub.id, product);
                }
            }        

            system.debug('mapSubscriptionToProduct ' + mapSubscriptionToProduct);

            //Loop Web Activity to set Product and Subscription fields
            for(WebActivityUtils.WebActivityContent wa : lstWAContent)
            {
                WebActivityUtils.AdditionalParams additParams = new WebActivityUtils.AdditionalParams();
            	additParams = wa.AdditParams;
                if(String.isNotBlank(wa.WebActivityType) && wa.WebActivityType.contains('invited'))
                {
                    wa.OUser.Product = 'Qlik Sense Business'; //Set Default Product to QSB    
                    if(string.isNotBlank(wa.oUser.ulcqlikId) && mapInvitedByToProductUser.containsKey(wa.OUser.ulcqlikId))
                    {
                        //string sub = mapInvitedByToProductUser.get(wa.OUser.ulcqlikId);
                        Product_User__c pu = mapInvitedByToProductUser.get(wa.OUser.ulcqlikId);
                        wa.OUser.Subscription = pu.subscription__c;
                        wa.OUser.SubscriptionName = pu.Zuora_Subscription_ID__c;
                        wa.OUser.Product = mapSubscriptionToProduct.get(pu.subscription__c);
                        additParams.taskSubject = 'Invited User Registration - ' + wa.OUser.Product;
                    }
                }
            }
        }
        return lstWAContent;
    }

}