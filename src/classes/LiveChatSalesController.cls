/**
* Class: LiveChatSalesController. Controller for Live_Agent_Pre_chat_Sales.page
* 
* Change log:
* 2018-11-26	ext_vos	CHG0035115: Add Case creation, update created Contact using Account by Country.
*/
public class LiveChatSalesController {

	private final static String CONTACT_RECORD_TYPE = 'Business Contact';
	private final static String CONTACT_LEAD_SOURCE = 'SUP - Support Request';
    private final static String CONTACT_OWNER = 'Qliktech Webservice';
    private final static String CASE_RECORD_TYPE = 'Live Chat Support Record Type';
    private final static String CASE_TYPE = 'Service Request';
    private final static String CASE_ORIGIN = 'Live Chat';

    private final static String TAG_START = '<option value="';
    private final static String TAG_CLOSE = '">';
    private final static String TAG_END = '</option>';
    private final static String EMPTY_VALUE = TAG_START + ' ' + TAG_CLOSE + '-- None --' + TAG_END;
    private final static String DISABLED_VALUE = TAG_START + '" disabled="" ' + TAG_CLOSE + '--------------' + TAG_END;

	public String recordTypeId {get;set;}
	public String ownerId {get;set;}
	public String leadSource {get;set;}
	public String caseType {get;set;}
	public String caseOrigin {get;set;}
	public String caseRecordTypeId {get;set;}
	public String countryOptions {get; set;}

	public LiveChatSalesController() {
		caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CASE_RECORD_TYPE).getRecordTypeId();
		recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CONTACT_RECORD_TYPE).getRecordTypeId();
		leadSource = CONTACT_LEAD_SOURCE;
		caseType = CASE_TYPE;
		caseOrigin = CASE_ORIGIN;
		List<User> users = [SELECT Id, Name FROM User WHERE Name = :CONTACT_OWNER LIMIT 1];
		if (!users.isEmpty()) {
			ownerId = users[0].Id;
		}
		countryOptions = findCountryOptions();
	}

	private String findCountryOptions() {	
		String options = EMPTY_VALUE;	
		
		// get sustom setting records	
		List<QS_Qoncierge_Country_License_Support__c> customSettings = [select Name, Temp_Account__c, Is_country_popular__c 
																			from QS_Qoncierge_Country_License_Support__c];
		if (!customSettings.isEmpty()) {
			// sort elements by Name
			Map<String,String> noSortedMap = new Map<String,String>();
			Map<String,String> popularNoSortedMap = new Map<String,String>();
			for (QS_Qoncierge_Country_License_Support__c cs : customSettings) {
	            if (cs.Is_country_popular__c) {
					popularNoSortedMap.put(cs.Name, cs.Temp_Account__c);
	            } else {
	            	noSortedMap.put(cs.Name, cs.Temp_Account__c);
	            }
	        }
			// add options with popular countries
	        List<String> sortedList = new List<String>();
	        sortedList.addAll(popularNoSortedMap.keySet());
	        sortedList.sort();
	        for (String pName : sortedList) {
				options += TAG_START + popularNoSortedMap.get(pName) + TAG_CLOSE + pName + TAG_END;          
	        }
	        options += DISABLED_VALUE;
	        
	        // add options with other countries
			sortedList = new List<String>();
	        sortedList.addAll(noSortedMap.keySet());
	        sortedList.sort();
	        
	        for (String name : sortedList) {
	        	options += TAG_START + noSortedMap.get(name) + TAG_CLOSE + name + TAG_END;
	        }
	        // shield the single quote in Country names
	        options = options.replaceAll('\'', '&#39;');
	    }
        return options;
	}
}