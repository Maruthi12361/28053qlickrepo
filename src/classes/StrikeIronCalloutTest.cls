/**
    Rodion Vakulovskyi : 28.03.2017 : Test class for QtStrikeironCom class
    * 02.09.2017 : Srinivasan PR- fix for query error
**/
@isTest
public class StrikeIronCalloutTest {
    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    static final String OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;

    public static testmethod void processStrikeIronVetify() {
     User testUser = [Select id From User where id =: UserInfo.getUserId()];

        QuoteTestHelper.createCustomSettings();

        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAccount.BillingCountry = 'France';
          update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
                                 where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        RecordType rTypeOpp = [Select id From RecordType where Id=: OppRecordTypeId];
        Test.setMock(WebServiceMock.class, new StrikeIronMockService());

        //to invoke callout test , we should uncomment in class AddressTriggerHelper=>VerifyAddress=> strikeIronservice.AdvancedVerify(addressIds);
        Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
        billAddress.Valid_Address__c = true;
        billAddress.Legal_Approval_Status__c = 'Granted';
        billAddress.Country__c = 'United Kingdom';
        insert billAddress;

        Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            shippAddress.Valid_Address__c = true;
            shippAddress.Legal_Approval_Status__c  = 'Granted';
        insert shippAddress;
        //Manual callout
        Test.startTest();
        makeDummyCallout(new List<Id>{billAddress.id});

        Test.stopTest();
    }

    public static testmethod void processTestAdvancedVerify() {
     User testUser = [Select id From User where id =: UserInfo.getUserId()];

        QuoteTestHelper.createCustomSettings();

        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAccount.BillingCountry = 'France';
          update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
                                   where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        RecordType rTypeOpp = [Select id From RecordType where Id=: OppRecordTypeId];
        Test.setMock(WebServiceMock.class, new StrikeIronTestMockService());

        //to invoke callout test , we should uncomment in class AddressTriggerHelper=>VerifyAddress=> strikeIronservice.AdvancedVerify(addressIds);
        Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
        billAddress.Valid_Address__c = true;
        billAddress.Legal_Approval_Status__c = 'Granted';
        billAddress.Country__c = 'United Kingdom';
        insert billAddress;

        Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            shippAddress.Valid_Address__c = true;
            shippAddress.Legal_Approval_Status__c  = 'Granted';
        insert shippAddress;
        //Manual callout
        Test.startTest();
        makeTestDummyCallout(billAddress.id);

        Test.stopTest();
    }

    public static testmethod void processTestAdvancedVerifyWithAddress() {
     User testUser = [Select id From User where id =: UserInfo.getUserId()];

        QuoteTestHelper.createCustomSettings();

        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAccount.BillingCountry = 'France';
        testAccount.Territory_Country__c='France';
          update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
                                  where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        RecordType rTypeOpp = [Select id From RecordType where Id=: OppRecordTypeId];
        Test.setMock(WebServiceMock.class, new AdvancedVerifyWithAddressCallout());

        //to invoke callout test , we should uncomment in class AddressTriggerHelper=>VerifyAddress=> strikeIronservice.AdvancedVerify(addressIds);
        Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
            billAddress.Valid_Address__c = true;
            billAddress.Legal_Approval_Status__c = 'Granted';
            insert billAddress;

        Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            shippAddress.Valid_Address__c = true;
            shippAddress.Legal_Approval_Status__c  = 'Granted';
        insert shippAddress;
        //Manual callout
        Test.startTest();
        makeTestDummyWithAddressCallout('France','France','France','France','France');

        Test.stopTest();
    }



    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr,
                                            Country_Name__c = countryName,
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }

    public static void makeDummyCallout(List<String> listOfAddressId) {
            QtStrikeironCom.ArrayOfString addressIds = new QtStrikeironCom.ArrayOfString();
            addressIds.string_x = listOfAddressId;
            QtStrikeironCom.AddressValidationServiceSoap strikeIronservice = new QtStrikeironCom.AddressValidationServiceSoap();
            strikeIronservice.timeout_x = 90000;
            QTWebserviceUtil.SetWebServiceEndpoint(strikeIronservice);
            strikeIronservice.AdvancedVerify(addressIds);
    }

    public static void makeTestDummyCallout(String stringId) {
            QtStrikeironCom.AddressValidationServiceSoap strikeIronservice = new QtStrikeironCom.AddressValidationServiceSoap();
            strikeIronservice.timeout_x = 90000;
            QTWebserviceUtil.SetWebServiceEndpoint(strikeIronservice);
            strikeIronservice.TestAdvancedVerify(stringId);
    }

    public static void makeTestDummyWithAddressCallout(String Country,String Street,String City,String State,String PostCode) {
            QtStrikeironCom.AddressValidationServiceSoap strikeIronservice = new QtStrikeironCom.AddressValidationServiceSoap();
            strikeIronservice.timeout_x = 90000;
            QTWebserviceUtil.SetWebServiceEndpoint(strikeIronservice);
            strikeIronservice.AdvancedVerifyWithAddress(Country, Street, City, State, PostCode);
    }

    // AsyncQtStrikeironCom testing
    public static testMethod void test_method_TestAdvancedVerify() {
        String Country = 'France';
        AsyncQtStrikeironCom.AsyncAddressValidationServiceSoap strikeIronService = new AsyncQtStrikeironCom.AsyncAddressValidationServiceSoap();
        strikeIronService.endpoint_x = 'http://svctestweb-t-01.qliktech.com/QTStrikeIron/AddressValidationService.asmx';
        Continuation continuation = new Continuation(60);
        strikeIronService.beginTestAdvancedVerify(continuation, Country);
    }

    public static testMethod void test_method_AdvancedVerify() {
        QtStrikeironCom.ArrayOfString addressIds = new QtStrikeironCom.ArrayOfString();
        AsyncQtStrikeironCom.AsyncAddressValidationServiceSoap strikeIronService = new AsyncQtStrikeironCom.AsyncAddressValidationServiceSoap();
        strikeIronService.endpoint_x = 'http://svctestweb-t-01.qliktech.com/QTStrikeIron/AddressValidationService.asmx';
        Continuation continuation = new Continuation(60);
        strikeIronService.beginAdvancedVerify(continuation, addressIds);
    }

    public static testMethod void test_method_AdvancedVerifyAddress() {
        String Country = 'France';
        String Street = 'Street';
        String City = 'Paris';
        String State = 'West';
        String PostCode = '1234';
        AsyncQtStrikeironCom.AsyncAddressValidationServiceSoap strikeIronService = new AsyncQtStrikeironCom.AsyncAddressValidationServiceSoap();
        strikeIronService.endpoint_x = 'http://svctestweb-t-01.qliktech.com/QTStrikeIron/AddressValidationService.asmx';
        Continuation continuation = new Continuation(60);
        strikeIronService.beginAdvancedVerifyWithAddress(continuation, Country, Street, City, State, PostCode);
    }
}