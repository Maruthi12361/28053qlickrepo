@IsTest
public class Z_RenewalOpportunityJobBulkTest {

    private static Account testAcc;
    private static Contact testContact;
    private static Zuora__CustomerAccount__c testCustomerAcc;
    private static QlikTech_Company__c testqtComp;
    private static Q2CWSettings__c testQ2cwSettings;
    private static Steelbrick_Settings__c testSteelbrickSettings;
    
    private static Test_Class_Data__mdt testClassVarData = [SELECT Id, MasterLabel, Numeric_Variable_Value__c FROM Test_Class_Data__mdt WHERE MasterLabel = 'Z_RenewalOpportunityJobBulkTestOppVar' LIMIT 1 ];
    
    private static Integer numberOfOpps = testClassVarData.Numeric_Variable_Value__c.intValue();
    private static Integer count = 0;
    
    private static List<Opportunity> testOpportunities;
    private static List<zqu__Quote__c> testQuotes;
    private static List<Zuora__Subscription__c> testSubscriptions;

    static void setup() {
        testQ2cwSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();

        testqtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');

        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        
        testAcc.QlikTech_Company__c = testqtComp.name;
        testAcc.Billing_Country_Code__c  = testqtComp.id;

        List<sObject> tests = new List<sObject>{
            testQ2cwSettings, testSteelbrickSettings
        };

        insert tests;

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc
        };

        insert testObjects;
        
        testOpportunities = Z_RenewalTestFactory.makeOpportunitiesBulkQCCS(testAcc, numberOfOpps);
        testQuotes = new List<zqu__Quote__c>();
        testSubscriptions = new List<Zuora__Subscription__c>();
        
        for(Opportunity testOpp : testOpportunities){
            count++;
            Zuora__Subscription__c testSub = Z_RenewalTestFactory.makeSubscription();
            testSub.Zuora__External_Id__c= 'Sub' + count;
            testSub.Zuora__SubscriptionNumber__c= 'Sub' + count;
            testSubscriptions.add(testSub);
            
            zqu__Quote__c testQuote = Z_RenewalTestFactory.makeQuotesBulk(testAcc, testContact, testCustomerAcc);
            testQuote.zqu__Opportunity__c = testOpp.id;
            testQuote.zqu__ZuoraSubscriptionId__c = 'Sub' + count;
            testQuote.zqu__Hidden_Subscription_Name__c = 'Sub' + count;
            testQuotes.add(testQuote);
        }
        
        insert testQuotes;
        insert testSubscriptions;
        

        Organization_Info__c orgInfo = new Organization_Info__c(isSandbox__c=true);
        insert orgInfo;

        orgInfo.CreatedInOrgId__c = orgInfo.Id;
        update orgInfo;
        // if(orgInfo == null || orgInfo.isSandbox__c == null || orgInfo.Id == null || orgInfo.Id != orgInfo.CreatedInOrgId__c
    }

    @IsTest
    static void renewalOpportunityJobTest() {

        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;

        setup();
        
        System.debug('$$ >> SOQL Queries Executed for Setup: '+Limits.getQueries());
        System.debug('$$ >> CPU Time for Setup: '+Limits.getCpuTime());
        
        
        //validate subscription and quote setup 
        
        Zuora__Subscription__c[] subscriptions = [SELECT Id, Zuora__SubscriptionNumber__c FROM Zuora__Subscription__c WHERE Zuora__Status__c = 'Active' 
                                         AND Digital__c = 'No'
                                         AND Zuora__TermEndDate__c >=: Date.newInstance(2019, 01, 01)];
        
        System.assertEquals(numberOfOpps, subscriptions.size()); 
        List<String> subNumbers = new List<String>();
        for(Zuora__Subscription__c sub: subscriptions) {
            subNumbers.add(sub.Zuora__SubscriptionNumber__c); 
        }
                                        
                                         
                                         
        zqu__Quote__c[] quotes = [SELECT Id FROM zqu__Quote__c WHERE
                                    zqu__SubscriptionType__c IN ('New Subscription', 'Renew Subscription')
                                AND Quote_Status__c IN ('Accepted by Customer', 'Order Placed')
                                AND zqu__Subscription_Name__c IN :subNumbers
                                AND zqu__Status__c = 'Sent to Z-Billing'
                                AND zqu__Opportunity__c != NULL
                                AND zqu__Opportunity__r.Next_Renewal_Opportunity__c = NULL
                                AND zqu__Opportunity__r.RecordType.Name IN ('Sales QCCS Zuora Subscription','Subscription Renewal')
                                ORDER BY zqu__SubscriptionTermEndDate__c];
        
        

        System.assertEquals(numberOfOpps, quotes.size()); 
        
        
        
        Semaphores.SetAllSemaphoresToFalse(); 
        Test.startTest();
        
        System.debug('$$ >> SOQL Queries Start of Test: '+Limits.getQueries());
        System.debug('$$ >> CPU Time Start of Test: '+Limits.getCpuTime());
        
        Z_RenewalOpportunityJob job = new Z_RenewalOpportunityJob();
        job.run();

        System.debug('$$ >> SOQL Queries End of Test: '+Limits.getQueries());
        System.debug('$$ >> CPU Time End of Test: '+Limits.getCpuTime());

        Test.stopTest();
        
        System.debug('$$ >> SOQL Queries Post Test: '+Limits.getQueries());
        System.debug('$$ >> CPU Time Post Test: '+Limits.getCpuTime());
    }  
    
}