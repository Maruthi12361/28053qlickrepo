// CR# 10215
// test class to test OpportunityChildClone trigger
// Change log:
// September 05, 2014 - Initial Implementation - Madhav Kakani - Fluido Denmark
// 20140925     RDZ     Changing it to Qlikbuy II record type "Qlikbuy CCS Standard II"
// 20151027     AIN     Trigger doesn't seem to exist
// 24.03.2017 Rodion Vakulovskyi       
@isTest
private class OpportunityChildCloneTest {
    static testMethod void testOpportunityClone() {
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');

        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {                      
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');

            Opportunity opp = QTTestUtils.createMockOpportunity('Test Opp', 'Test Opp', 'New Customer', 'Direct', 'Prospecting', rt.Id, 'GBP', mockSysAdmin, true);
            system.assert(opp.Id != null);  

            Sphere_Of_Influence__c soi = QTTestUtils.createMockSOI(opp, true);
            system.assert(soi.Id != null);
            
            Test.StartTest();

            Opportunity oppClone = opp.clone(false, true);
            system.assert(oppClone != null);
            insert oppClone;

            oppClone.To_be_Cloned__c = true;
            oppClone.Original_Opportunity__c = opp.Id;
            update oppClone; // covers OpportuntyChildClone trigger

            Test.StopTest();                               
        }
    } // testOpportunityClone  
} // class OpportunityChildCloneTest