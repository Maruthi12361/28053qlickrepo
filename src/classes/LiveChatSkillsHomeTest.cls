/**
* Test for LiveChatSkillsHomeController.
* 
* Change log:
* 26-12-2018	ext_vos	CHG0035234: add test for controller.
*/
@isTest
public class LiveChatSkillsHomeTest {
	@TestSetup
	static void createTestData() {
		QuoteTestHelper.createCustomSettings();
		// init data
		Profile profile = [select Id from Profile where Name = 'Custom: Support Std User'];
		List<User> newUsers = new List<User>();
		User userAgent = new User(Username = 'LCSkillSet1@test.com', FirstName = 'QlikTech', LastName = 'HomeComponent', Email = 'LCSkillSet1@test.com', 
    						 Alias = 'agentTC', TimeZoneSidKey = 'Europe/Paris', LocaleSidKey = 'es_ES', UserPermissionsLiveAgentUser = true,
    						 EmailEncodingKey = 'ISO-8859-1', ProfileId = profile.Id, LanguageLocaleKey = 'en_US'); 
		newUsers.add(userAgent);
		User user = new User(Username = 'noLCSkillSet1@test.com', FirstName = 'QlikTech', LastName = 'NoHomeComponent', Email = 'noLCSkillSet1@test.com', 
    						 Alias = 'noagTC', TimeZoneSidKey = 'Europe/Paris', LocaleSidKey = 'es_ES', 
    						 EmailEncodingKey = 'ISO-8859-1', ProfileId = profile.Id, LanguageLocaleKey = 'en_US'); 
		newUsers.add(user);
		insert newUsers;
		
        QTCustomSettingsHier__c ins = [select id, Base_URL__c from QTCustomSettingsHier__c limit 1];
        ins.Base_URL__c = URL.getOrgDomainUrl().toExternalForm();
        update ins;
    }
    
    static testMethod void testController() {
		PageReference page = Page.LiveChatSkillsHomeComponent;
		Test.setCurrentPage(page);	

    	User us = [select Id from User where Alias = 'agentTC'];
    	System.runAs(us) {
			LiveChatSkillsHomeController contr = new LiveChatSkillsHomeController();
			System.assert(contr.isSectionAvailable);
			System.assert(String.isNotEmpty(contr.info));
			System.assertEquals('Add Sales skill', contr.buttonName);

			List<SkillUser> relations = [select Id from SkillUser where SkillId =: contr.salesSkillId and UserId =: UserInfo.getUserId()];	
			System.assertEquals(0, relations.size());
			
			Test.startTest();
			// SFDC provides creation SkillUser object from code via HttpRequest only 
			// that is why, we cannot test the controller behavior, just use some mock class (MockBoomiUtilsResponse) for success response. 
			Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());
			contr.updateSkills();
			// call to cover code
			LiveChatSkillsHomeController.deleteSkill(contr.salesSkillId);
			Test.stopTest();
		}
		// unavailable for "no-live-agent" users
		us = [select Id from User where Alias = 'noagTC'];
    	System.runAs(us) {
			LiveChatSkillsHomeController contr = new LiveChatSkillsHomeController();
			System.assert(!contr.isSectionAvailable);
		}
    }
}