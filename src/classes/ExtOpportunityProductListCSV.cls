/********
* NAME : ExtOpportunityProductListCSV.cls
* Description: QCW-1832 - Export opportunities primary quote to CSV file.
*
*Change Log:
    6/2/17  Viktor Yaremenko @4front.se QCW-1832 created this class.
    2018-21-06 ext_vad  remove Account.BillingAddress links.
******/

public with sharing class ExtOpportunityProductListCSV {

    private Opportunity opp { get; set; }

    public QuoteWrapper pQuote { get; set; }
    public Boolean isCsvFile { get; set; }
    public String content { get; set; }
    public String errorMessage { get; set; }

    public ExtOpportunityProductListCSV(ApexPages.StandardController sc) {
        opp = (Opportunity)sc.getRecord();
        List<Opportunity> lo = [SELECT SBQQ__PrimaryQuote__c, Revenue_Type__c FROM Opportunity WHERE Id =: opp.Id LIMIT 1];
		if ( lo.size() == 1 && lo[0].Revenue_Type__c == 'Distributor' && String.isNotBlank(lo[0].SBQQ__PrimaryQuote__c) ) {
            SBQQ__Quote__c pq = [SELECT Id, Name, SBQQ__ExpirationDate__c, 
                    Sell_Through_Partner__r.Name, Sell_Through_Partner__r.BillingStreet, Sell_Through_Partner__r.BillingCity,
                    Sell_Through_Partner__r.BillingState, Sell_Through_Partner__r.BillingPostalCode,
                    Sell_Through_Partner__r.BillingCountry, Currency_Text__c, SBQQ__Account__r.Name,
                    SBQQ__Account__r.BillingStreet, SBQQ__Account__r.BillingCity, SBQQ__Account__r.BillingState,
                    SBQQ__Account__r.BillingPostalCode, SBQQ__Account__r.BillingCountry,
                    Quote_Recipient__r.Name, Quote_Recipient__r.Email, Maintenance_Level__c,
                    Second_Partner__r.Name, Second_Partner__r.BillingStreet, Second_Partner__r.BillingCity,
                    Second_Partner__r.BillingState, Second_Partner__r.BillingPostalCode, Second_Partner__r.BillingCountry,
                    Second_Partner_Contact__r.Name,
                    Second_Partner_Contact__r.Email, Term__c, Maintenance_Net_Amount__c,
                    SBQQ__Opportunity2__r.Opportunity_Source__c, License_Net_Amount__c, SBQQ__NetAmount__c
                                  FROM SBQQ__Quote__c
                                  WHERE ID =: lo[0].SBQQ__PrimaryQuote__c LIMIT 1];
                pQuote = new QuoteWrapper(pq);
                isCsvFile = true;
                content = 'text/csv#' + pq.Name + '.csv';
        } else {
            isCsvFile = false;
            content = 'text/html';
            errorMessage = 'Action is not allowed.';
        }
    }

    public List<QuoteLineWrapper> getQuoteLines() {
        List<QuoteLineWrapper> res = new List<QuoteLineWrapper>();
    	for ( SBQQ__QuoteLine__c ql : [SELECT Id, Name,  SBQQ__ProductCode__c, SBQQ__Quantity__c, 
                                       		SBQQ__Description__c, SBQQ__ListPrice__c, Additional_Discount_Perc__c,
                                            Extended_CUP_RO__c, Extended_CUP_AD_RO__c , SBQQ__NetTotal__c,
                                       		SBQQ__PartnerDiscount__c, Maintenance_Level__c, SBQQ__NetPrice__c,
                                            Distributor_Discount_on_Quote__c, Maintenance_Item__c
                                       FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: pQuote.getId()]) {
        
			res.add(new QuoteLineWrapper(ql));
        }
        return res;
    }
    
    public class QuoteWrapper {
        private SBQQ__Quote__c quote { get; set; }
        
        QuoteWrapper(SBQQ__Quote__c q) {
            this.quote = q;
        }
        
        public Id getId() {
        	return quote.Id;   
        }
        
        public String getQuoteNumber() {
            return escapeCsv(quote.Name);
        }
        
        public String getQuoteExpirationDate() {
            if ( quote.SBQQ__ExpirationDate__c != null ) {
            	return escapeCsv(quote.SBQQ__ExpirationDate__c.format());
            }
            return '';
        }
    
        public String getDistributorName() { // Distributor Name -> Sell Through Partner
           	return escapeCsvRel(quote.Sell_Through_Partner__r, 'Name');
        }
		
        public String getDistributorAddress() { // Distributor address Sell Through Partner Account's Address
            if ( quote.Sell_Through_Partner__r != null ) {
            	return escapeCsv(parseAddress(quote.Sell_Through_Partner__r.BillingStreet,
                        quote.Sell_Through_Partner__r.BillingCity,
                        quote.Sell_Through_Partner__r.BillingState,
                        quote.Sell_Through_Partner__r.BillingPostalCode,
                        quote.Sell_Through_Partner__r.BillingCountry));
            }
            return '';
        }
        
        public String getCurrency() {
            return escapeCsv(quote.Currency_Text__c);
        }
        
        public String getEndUserName() { // End User Name -> Account Name
           	return escapeCsvRel(quote.SBQQ__Account__r, 'Name');
        }
		
        public String getEndUserAddress() { // End User Address -> Account Name Address
            if ( quote.SBQQ__Account__r != null ) {
            	return escapeCsv(parseAddress(quote.SBQQ__Account__r.BillingStreet,
                        quote.SBQQ__Account__r.BillingCity,
                        quote.SBQQ__Account__r.BillingState,
                        quote.SBQQ__Account__r.BillingPostalCode,
                        quote.SBQQ__Account__r.BillingCountry));
            }
            return '';
        }
		
        public String getEUContactName() { // EU Contact name -> Quote Recipient
            return escapeCsvRel(quote.Quote_Recipient__r, 'Name');
        }
		
        public String getEUEmail() { // EU email -> Quote Recipient Email address
            return escapeCsvRel(quote.Quote_Recipient__r, 'Email');
        }
		
        public String getResellerName() { // Reseller name -> Second Partner
            return escapeCsvRel(quote.Second_Partner__r, 'Name');
        }
		
        public String getResellerAddress() { // Reseller address -> Second partner Account Address
            if ( quote.Second_Partner__r != null ) {
            	return escapeCsv(parseAddress(quote.Second_Partner__r.BillingStreet,
                        quote.Second_Partner__r.BillingCity,
                        quote.Second_Partner__r.BillingState,
                        quote.Second_Partner__r.BillingPostalCode,
                        quote.Second_Partner__r.BillingCountry));
            }
            return '';
        }
        
		public String getResellerContactname() { // Reseller Contact name Second Partner Contact
            return escapeCsvRel(quote.Second_Partner_Contact__r, 'Name');
        }
        
		public String getResellerEmail() { // Reseller email -> Second Partner Contact email address
            return escapeCsvRel(quote.Second_Partner_Contact__r, 'Email');
        }
        
        public String getPartnerRegisteredOpp() { // yes/no
            
            if ( quote.SBQQ__Opportunity2__r != null 
                && quote.SBQQ__Opportunity2__r.Opportunity_Source__c == 'Partner Registration'               
               ) {
                   return 'yes';
               }
            return 'no';
        }

        public String getSupportDuration() { //Support Duration: months of coverage -> This is the maintenance months
            return escapeCsv(String.valueOf(quote.Term__c));
        }

        public String getMaintenanceLevel() { //Maintenance Level   Basic or Premium
            return escapeCsv(quote.Maintenance_Level__c);
        }


        public String getLicenseAmount() {
            return escapeCsv(formatDecimal(quote.License_Net_Amount__c));
        }

        public String getMaintenanceAmount() {
            return escapeCsv(formatDecimal(quote.Maintenance_Net_Amount__c));
        }
        public String getQuoteTotal() {
            return escapeCsv(formatDecimal(quote.SBQQ__NetAmount__c));
        }

    }
    
    public class QuoteLineWrapper {
        private SBQQ__QuoteLine__c ql {get; set;}

        QuoteLineWrapper(SBQQ__QuoteLine__c ql) {
            this.ql = ql;
        }
        
        public String getNumber() { // Quote line number
            return escapeCsv(ql.Name);
        }
        
        public String getItem() { // Item -> Product Code
            return escapeCsv(ql.SBQQ__ProductCode__c);
        }
            
        public String getQty() { // Qty
            return escapeCsv(String.valueOf(ql.SBQQ__Quantity__c));
        }
            
        public String getItemDescription() { // Item Description -> Product Description
            return escapeCsv(ql.SBQQ__Description__c);
        }
            
        public String getUnitListPrice() { // Unit List Price
            return escapeCsv(formatDecimal(ql.SBQQ__ListPrice__c));
        }

        public String getExtUnitListPrice() { // Extended Unit List Price Extended_CUP_RO__c
            return escapeCsv(formatDecimal(ql.Extended_CUP_RO__c));
        }

        public String getQlikUnitDiscount() {
            return escapeCsv(formatDecimal(ql.Additional_Discount_Perc__c)) + '%';
        }

        private Decimal unitNetPrice() {// Ext CUP - AD (Extended_CUP_AD_RO__c)
            return nullValue(ql.Extended_CUP_AD_RO__c, 0.00);
        }

        public String getUnitNetPrice() {
            return escapeCsv(formatDecimal(unitNetPrice()));
        }

        //Calculated method
        private Decimal discountPercent() { //Discount percent -> Partner Discount
            return nullValue(ql.SBQQ__PartnerDiscount__c, 0.00);
        }

        public String getPartnerDiscount() {
            return escapeCsv(formatDecimal(discountPercent())) + '%';
        }


        private Decimal unitNetBeforeDD() { //Unit Distributor cost:   Unit Net Price  - discount percent = Disti Cost
            Decimal d = this.unitNetPrice() * ( 100.00 - this.discountPercent()) / 100.00;
            return d;
        }

        public String getUnitNetBeforeDD() { // Ext CUP - AD minus ( Ext CUP - AD times Partner Discount)
            return escapeCsv(formatDecimal(unitNetBeforeDD()));
        }

        public String getDistributorDiscount() { // Distributor total discount %
            return escapeCsv(formatDecimal(ql.Distributor_Discount_on_Quote__c)) + '%';
        }

        public String getUnitDistributorCost() { //Net Total
            return escapeCsv(formatDecimal(ql.SBQQ__NetTotal__c));
        }

    }

    private static String escapeCsv(String str) {
        if ( str != null ) {
            return str.escapeCsv();
        }
        return '';
    }

    private static String escapeCsvRel(sObject obj, String str) {
        if ( obj != null && String.isNotBlank(str) && String.isNotBlank(((String)obj.get(str)))) {
            return ((String)obj.get(str)).escapeCsv();
        }
        return '';
    }

    private static String parseAddress(String street, String city, String state, String postalcode, String country) {
        return
                    (String.isNotBlank(street) ? street + '; ' : '')
                            + (String.isNotBlank(city) ? city + '; ' : '')
                            + (String.isNotBlank(state) ? state + '; ' : '')
                            + (String.isNotBlank(postalcode) ? postalcode + '; ' : '')
                            + (String.isNotBlank(country) ? country : '');
    }
    
    private static Decimal nullValue(Decimal inD, Decimal outD) {
        if ( inD != null ) return inD;
        if ( outD != null ) return outD;
        return 0.00;
    }

    private static String formatDecimal(Decimal inValue) {
        if ( inValue != null ) {
            String s = inValue.setScale(2).format();
            return s.contains('.') ? s : s + '.00';
        }
        return '0.00';
    }
}