/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *  2013_07_23  CCE CR# 9083 https://eu1.salesforce.com/a0CD000000Zy1GV
 *  2014-02-14  SLH CR# 11131 reveresed CR# 9083 Mr Moe is back!
 */
@isTest
private class Test_CasetoCR_CRtoCase {

    //public Id businessOwnerID = '005D0000001pH7F'; //Ulrika Sandén CCE CR# 9083
     
    public static TestMethod void testCRtoCase()
    {
        RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'Sales Ops Internal Apps' limit 1];
        Id pId = [Select Id From Profile Where Name Like 'System Administrator' Limit 1].Id; 
        User johnBrown = new User();
        johnBrown = [select Id from User where profileId=:pId and isActive=true limit 1];
                        
        List<Case> testCases = new List<Case>();                
        for(Integer i=0; i< 2; i++)
        {
            Integer no = 50 +i;
            testCases.add(new Case(RecordTypeId = caseRT.Id, Subject = 'Test Case N'+no, Status = 'Not Opened', Description = 'Desc: Test Case N'+no,    
            Category__c = 'Applications', 
             Requester_Name__c = johnBrown.Id, Priority = 'Medium', SuppliedEmail = 'abc@abc.com'));                    
        }
        test.startTest();
        insert testCases;
        
        
        // For Support Business Area --> Business Owner Michael Moe 

        User businessOwner = new User();
        businessOwner = [select Id from User where Name like 'Michael Moe%' and isActive=true limit 1];   //CR# 9083
                
        List<SLX__Change_Control__c> testChangeControls = new List<SLX__Change_Control__c>();
        
        for(Integer i=0; i< 2; i++)
        {
            Integer no = 50 +i;
            testChangeControls.add(new SLX__Change_Control__c(Business_Owner__c = businessOwner.Id, Business_Area__c = 'Support', Name = 'test change control' + no, SLX__Requestor__c = johnBrown.Id, Change_Description__c = testCases[i].Description,
                                                  Business_Need__c = testCases[i].Description + ' Business Need', ITIL_level__c = 'Normal - Needs Approval', Type__c = 'Installation', Affected_systems__c = 'Others', Problem_Case_Nr__c = testCases[i].Id));    //CR# 9083
            //testChangeControls.add(new SLX__Change_Control__c(Business_Owner__c = businessOwnerID, Business_Area__c = 'Support', Name = 'test change control' + no, SLX__Requestor__c = johnBrown.Id, Change_Description__c = testCases[i].Description,
            //                                        Business_Need__c = testCases[i].Description + ' Business Need', ITIL_level__c = 'Normal - Needs Approval', Type__c = 'Installation', Affected_systems__c = 'Others', Problem_Case_Nr__c = testCases[i].Id));
                                
        }
        
        insert testChangeControls;      
        
        List<SLX__Change_Control__c> tCControls = [Select Id, Problem_Case_Nr__c from SLX__Change_Control__c where Name like 'test change control%' order by Name];     
        List<Case> tCases = [Select Id, Linked_to_CR__c from Case where Subject like 'Test Case N%' order by Subject];
                
        for(Integer i=0; i<2; i++)
        {
            System.assertEquals(tCases[i].id, tCControls[i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[i].Linked_to_CR__c);
        }       
        
        test.stopTest();
    }
    
    
    public static TestMethod void testCRDeltoCase()
    {
        RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'Sales Ops Internal Apps' limit 1];
        Id pId = [Select Id From Profile Where Name Like 'System Administrator' Limit 1].Id; 
        User johnBrown = new User();
        johnBrown = [select Id from User where profileId=:pId and isActive=true limit 1];
                        
        List<Case> testCases = new List<Case>();                
        for(Integer i=0; i< 2; i++)
        {
            Integer no = 50 +i;
            testCases.add(new Case(RecordTypeId = caseRT.Id, Subject = 'Test Case N'+no, Status = 'Not Opened', Description = 'Desc: Test Case N'+no,    
            Category__c = 'Applications', 
             Requester_Name__c = johnBrown.Id, Priority = 'Medium', SuppliedEmail = 'abc@abc.com'));                    
        }
        //test.startTest();
        insert testCases;
                
        User businessOwner = new User();
        businessOwner = [select Id from User where Name like 'Michael Moe%' and isActive=true limit 1];   //CR# 9083
                
        List<SLX__Change_Control__c> testChangeControls = new List<SLX__Change_Control__c>();
        
        for(Integer i=0; i< 2; i++)
        {
            Integer no = 50 +i;
            testChangeControls.add(new SLX__Change_Control__c(Business_Owner__c = businessOwner.Id, Business_Area__c = 'Support', Name = 'test change control' + no, SLX__Requestor__c = johnBrown.Id, Change_Description__c = testCases[i].Description,
                                                  Business_Need__c = testCases[i].Description + ' Business Need', ITIL_level__c = 'Normal - Needs Approval', Type__c = 'Installation', Affected_systems__c = 'Others', Problem_Case_Nr__c = testCases[i].Id));    //CR# 9083
            //testChangeControls.add(new SLX__Change_Control__c(Business_Owner__c = businessOwnerID, Business_Area__c = 'Support', Name = 'test change control' + no, SLX__Requestor__c = johnBrown.Id, Change_Description__c = testCases[i].Description,
            //                                        Business_Need__c = testCases[i].Description + ' Business Need', ITIL_level__c = 'Normal - Needs Approval', Type__c = 'Installation', Affected_systems__c = 'Others', Problem_Case_Nr__c = testCases[i].Id));
                    
        }
        test.startTest();
        insert testChangeControls;      
        
        List<SLX__Change_Control__c> tCControls = [Select Id, Problem_Case_Nr__c from SLX__Change_Control__c where Name like 'test change control%' order by Name];     
        List<Case> tCases = [Select Id, Linked_to_CR__c from Case where Subject like 'Test Case N%' order by Subject];
                
        for(Integer i=0; i<2; i++)
        {
            System.assertEquals(tCases[i].id, tCControls[i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[i].Linked_to_CR__c);
        }       
        
        delete tCControls;
        
        List<Case> tCases2 = [Select Id, Linked_to_CR__c from Case where Subject like 'Test Case N%' order by Subject];
        
        for(Integer i=0; i<2; i++)
        {
            System.assertEquals(tCases2[i].Linked_to_CR__c, null);          
        }   
        
        test.stopTest();
    }
    

    public static TestMethod void testCaseToCR()
    {
        RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'Sales Ops Internal Apps' limit 1];
        Id pId = [Select Id From Profile Where Name Like 'System Administrator' Limit 1].Id; 
        User johnBrown = new User();
        johnBrown = [select Id from User where profileId=:pId and isActive=true limit 1];
                        
        List<Case> testCases = new List<Case>();                
        for(Integer i=0; i< 2; i++)
        {
            Integer no = 50 +i;
            testCases.add(new Case(RecordTypeId = caseRT.Id, Subject = 'Test Case N'+no, Status = 'Not Opened', Description = 'Desc: Test Case N'+no,    
            Category__c = 'Applications', 
             Requester_Name__c = johnBrown.Id, Priority = 'Medium', SuppliedEmail = 'abc@abc.com'));        
        }
        
        //test.startTest();
        
        insert testCases;
        
        User businessOwner = new User();
        businessOwner = [select Id from User where Name like 'Michael Moe%' and isActive=true limit 1];   //CR# 9083
                
        List<SLX__Change_Control__c> testChangeControls = new List<SLX__Change_Control__c>();
        
        for(Integer i=0; i< 2; i++)
        {
            Integer no = 50 +i;
            testChangeControls.add(new SLX__Change_Control__c(Business_Owner__c = businessOwner.Id, Business_Area__c = 'Support', Name = 'test change control' + no, SLX__Requestor__c = johnBrown.Id, Change_Description__c = testCases[i].Description,
                                                  Business_Need__c = testCases[i].Description + ' Business Need', ITIL_level__c = 'Normal - Needs Approval', Type__c = 'Installation', Affected_systems__c = 'Others'));  //CR# 9083
            //testChangeControls.add(new SLX__Change_Control__c(Business_Owner__c = businessOwnerID, Business_Area__c = 'Support', Name = 'test change control' + no, SLX__Requestor__c = johnBrown.Id, Change_Description__c = testCases[i].Description,
            //                                        Business_Need__c = testCases[i].Description + ' Business Need', ITIL_level__c = 'Normal - Needs Approval', Type__c = 'Installation', Affected_systems__c = 'Others')); 
                                                    //Problem_Case_Nr__c = testCases[i].Id)); 
        }
        
        test.startTest();       
        insert testChangeControls;  
        
        List<SLX__Change_Control__c> tCControls = [Select Id, Problem_Case_Nr__c from SLX__Change_Control__c where Name like 'test change control%' order by Name];
        List<Case> tCases = [Select Id, Linked_to_CR__c from Case where Subject like 'Test Case N%' order by Subject];
        
        for(Integer i=0; i<2; i++)
        {
            tCases[i].Linked_to_CR__c = tCControls[i].Id;
        }
        
        update tCases;
        
        tCControls = [Select Id, Problem_Case_Nr__c from SLX__Change_Control__c where Name like 'test change control%' order by Name];
        tCases = [Select Id, Linked_to_CR__c from Case where Subject like 'Test Case N%' order by Subject];
        
        for(Integer i=0; i<2; i++)
        {
            System.assertEquals(tCases[i].id, tCControls[i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[i].Linked_to_CR__c);
        }
            
            
        // Changing CR value (Linked_to_CR__c)  of Case 
        
        for(Integer i=0; i<2; i++)
        {
            tCases[i].Linked_to_CR__c = tCControls[1-i].Id;
        }
        
        update tCases;
        
        tCControls = [Select Id, Problem_Case_Nr__c from SLX__Change_Control__c where Name like 'test change control%' order by Name];
        tCases = [Select Id, Linked_to_CR__c from Case where Subject like 'Test Case N%' order by Subject];
        
        for(Integer i=0; i<2; i++)
        {
            System.assertEquals(tCases[i].id, tCControls[1-i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[1-i].Linked_to_CR__c);
        }
        
        test.stopTest();
    }

}