/*
|-----------------------------------------------------------------------------
|  Class: EmailQonciergeServiceRequestToCase_Test
|
|  Author: Peter Friberg, Fluido Sweden AB
|
|  Description:
|    Class performing tests on the email inbound service class called
|    EmailQonciergeServiceRequestToCase
|
| Change Log:
| 2013-06-22  PetFri  Initial Development
| 2013-07-02  PetFri  Added test cases for single and multiple contacts 
| 2013-08-21 MTM			  CR# 9273 Qoncierge Service Request to Case - fix for reported issues
|-----------------------------------------------------------------------------
*/

@isTest
public class EmailQonciergeServiceRequestToCase_Test {

    // Static email body
    static String EmailBody =
        'A Qoncierge support request form has been submitted.\n'+
        'Please be cautious with any attachment from the website. '+
        'Make sure it is scanned for viruses before opening the file.\n'+
        '\n'+
        'Properties:\n'+
        '\n'+
        'First Name\n'+
        '\n'+
        'Lassi\n'+
        '\n'+
        'Last Name\n'+
        '\n'+
        'Doginen\n'+
        '\n'+
        'Company\n'+
        '\n'+
        'Mate and Co\n'+
        '\n'+
        '*Country*\n'+
        '\n'+
        'Finland\n'+
        '\n'+
        '*Work E-Mail*\n'+  // First using < >
        '\n'+
        '<mailto:peter.friberg@fluidogroup.com>\n'+
        '\n'+
        'Work E-Mail\n'+  // Second with * *
        '\n'+
        '*peter.friberg@fluidogroup.com*\n'+
        '\n'+
        'Phone Number incl. Country Code\n'+
        '\n'+
        '0708-332211\n'+
        '\n'+
        'QlikView Account User Name\n'+
        '\n'+
        'Lasdog33\n'+
        '\n'+
        'License Number\n'+
        '\n'+
        '987234628399999\n'+
        '\n'+
        'Topic\n'+
        '\n'+
        'Account Administration\n'+
        '\n'+
        'Description\n'+
        '\n'+
        'Good Day, Can you please assist I keep trying to create an online QLikView account,\n'+
        'but it keep crashing. I have attached a screenshot.\n'+
        'Thanks, Lassi!\n'+
        '\n'+
        'Attachment\n'+
        '\n'+
        'C:\\qlik1.txt\n'+
        'C:\\qlik2.txt\n';

    /*
    |----------------------------------------------------------
    |  Method: createTempAccount
    |
    |  Description:
    |      Create temporary account
    |
    |  Input parameters:
    |      None
    |
    |  Returns:
    |      Account created
    |----------------------------------------------------------
    */
    
    static Account createTempAccount() {

        // Create temp account
        Account tempAcc =
            new Account(
                Name = 'Temp Qoncierge');
        insert tempAcc;

        return tempAcc;
    }

    /*
    |----------------------------------------------------------
    |  Method: createTempContact
    |
    |  Description:
    |      Create single temporary contact
    |
    |  Input parameters:
    |      acc    Account - Account to replate the contatct to
    |
    |  Returns:
    |      Contact created
    |----------------------------------------------------------
    */

    static Contact createTempContact(Account acc) {

        // Get record type Id
        RecordType rt = 
            [SELECT Id FROM RecordType 
             WHERE (sObjectType ='Contact') AND 
                   (Name = 'Business Contact')];

        // Create temp contact
        Contact tempContact =
            new Contact(
                FirstName = 'Kalle',
                LastName = 'Larsson',
                AccountId = acc.id,
                LeadSource = 'Qlikmarket',
                Email = 'peter.friberg@fluidogroup.com',
                RecordTypeId = rt.id);
        insert tempContact;

        return tempContact;
    }

    /*
    |----------------------------------------------------------
    |  Method: createTempMultipleContacts
    |
    |  Description:
    |      Create specified number of contacts
    |
    |  Input parameters:
    |      count    Integer - Number of contacts to create
    |      acc      Account - The account to relate the contacts to 
    |
    |  Returns:
    |      List of Contacts created
    |----------------------------------------------------------
    */

    static List<Contact> createTempMultipleContacts(Integer count, Account acc) {

        // List of contacts to return
        List<Contact> contacts = new List<Contact>();

        // Get record type Id
        RecordType rt = 
            [SELECT Id FROM RecordType 
             WHERE (sObjectType ='Contact') AND 
                   (Name = 'Business Contact')];

        // Create temp contacts
        for (Integer idx = 1; idx <= count; idx++) {
            contacts.add(
                new Contact(
                    FirstName = 'Kalle_' + String.valueOf(idx),
                    LastName = 'Larsson',
                    AccountId = acc.id,
                    LeadSource = 'Qlikmarket',
                    Email = 'peter.friberg@fluidogroup.com',
                    RecordTypeId = rt.id));
        }
        insert contacts;

        return contacts;
    }

    /*
    |----------------------------------------------------------
    |  Method: Test_NonexistingContact
    |
    |  Description:
    |      Thest when a contact is non existent, new contact
    |      shall be created.
    |
    |  Input parameters:
    |      None
    |
    |  Returns:
    |      Nothing
    |----------------------------------------------------------
    */ 
    static testMethod void Test_NonexistingContact() {

        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.BinaryAttachment inAttB = new Messaging.InboundEmail.BinaryAttachment();
        Messaging.InboundEmail.TextAttachment inAttT = new Messaging.InboundEmail.TextAttachment();

        // Set email header
        email.subject = '';
        env.fromAddress = 'user@acme.com';

        // Set email body
        email.htmlBody = EmailBody;
        email.plainTextBody = EmailBody;

        // set the body of the binary attachment
        String binaryBody = 'This is a binary body!';
        inAttB.body = blob.valueOf(binaryBody);
        inAttB.fileName = 'qlik1.txt';
        inAttB.mimeTypeSubType = 'plain/txt';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { inAttB }; 

        // Set the body of the text attachment
        String textBody = 'This is a text body!';
        inAttT.body = textBody;
        inAttT.fileName = 'qlik2.txt';
        inAttT.mimeTypeSubType = 'plain/txt';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { inAttT }; 

        // ###
        // ### Start the test!
        // ###

        //Test.setMock(WebServiceMock.class, new DummyWebServiceMockImpl());

        test.startTest();

        createTempAccount();

        // call the class and test it with the data in the testMethod
        EmailQonciergeServiceRequestToCase emailServiceObj = new EmailQonciergeServiceRequestToCase();
        emailServiceObj.handleInboundEmail(email, env);                      

        // ###
        // ### End the test!
        // ###
        
        test.stopTest();

        // Validation
        List<Contact> conts =
            [SELECT Id, Name FROM Contact];
        system.assertEquals(1, conts.size(), 'There shall be only one new contact');
        system.assertEquals('Lassi Doginen', conts[0].name, 'The name of the contact shall be Lassi Doginen');
    }

    /*
    |----------------------------------------------------------
    |  Method: Test_SingleContact
    |
    |  Description:
    |      Thest when a single contact exists, present contact
    |      shall be used.
    |
    |  Input parameters:
    |      None
    |
    |  Returns:
    |      Nothing
    |----------------------------------------------------------
    */

    static testMethod void Test_SingleContact() {

        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // Set email header
        email.subject = '';
        env.fromAddress = 'user@acme.com';

        // Set email body
        email.htmlBody = EmailBody;
        email.plainTextBody = EmailBody;

        // ###
        // ### Start the test!
        // ###

        Account acc  = createTempAccount();
        Contact cont = createTempContact(acc);

        test.startTest();

        // call the class and test it with the data in the testMethod
        EmailQonciergeServiceRequestToCase emailServiceObj = new EmailQonciergeServiceRequestToCase();
        emailServiceObj.handleInboundEmail(email, env);                      

        // ###
        // ### End the test!
        // ###
        
        test.stopTest();
    }

	static testMethod void Test_SingleContactWithHTMLBody() {

        // Create a new email, envelope object and Attachment
        String htmlEmailBody = '<div><p><strong>First Name</strong></p><p>Taurus</p></div><div><p><strong>Last Name</strong></p><p>Phil</p></div><div><p><strong>Company</strong></p><p>Chelsea</p></div><div><p><strong>Country</strong></p><p>United Kingdom</p></div><div><p><strong>Work E-Mail</strong></p><p>muneeram@hotmail.com</p></div><div><p><strong>Phone Number incl. Country Code</strong></p><p></p></div><div><p><strong>QlikView Account User Name</strong></p><p></p></div><div><p><strong>License Number</strong></p><p></p></div><div><p><strong>Topic</strong></p><p>Training</p></div><div><p><strong>Description</strong></p><p>Taurus and & \r\n\'single quotes\'\r\n "double quotes" \r\ntest message delete !!!</p></div><div><p><strong>Attachment</strong></p><p>(no file submitted)</p></div>';
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // Set email header
        email.subject = '';
        env.fromAddress = 'user@acme.com';

        // Set email body
        email.htmlBody = htmlEmailBody;
        

        // ###
        // ### Start the test!
        // ###

        Account acc  = createTempAccount();
        Contact cont = createTempContact(acc);

        test.startTest();

        // call the class and test it with the data in the testMethod
        EmailQonciergeServiceRequestToCase emailServiceObj = new EmailQonciergeServiceRequestToCase();
        emailServiceObj.handleInboundEmail(email, env);                      

        // ###
        // ### End the test!
        // ###
        
        test.stopTest();
    }
    
    /*
    |----------------------------------------------------------
    |  Method: Test_MultipleContacts
    |
    |  Description:
    |      Thest when multiple contacts exists, new contact
    |      shall be created.
    |
    |  Input parameters:
    |      None
    |
    |  Returns:
    |      Nothing
    |----------------------------------------------------------
    */
    static testMethod void Test_MultipleContacts() {

        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // Set emnail header
        email.subject = '';
        env.fromAddress = 'user@acme.com';

        // Set email body
        email.htmlBody = EmailBody;
        email.plainTextBody = EmailBody;

        // ###
        // ### Start the test!
        // ###

        Account acc = createTempAccount();
        List<Contact> contacts = createTempMultipleContacts(2, acc);

        test.startTest();

        // call the class and test it with the data in the testMethod
        EmailQonciergeServiceRequestToCase emailServiceObj = new EmailQonciergeServiceRequestToCase();
        emailServiceObj.handleInboundEmail(email, env);                      

        // ###
        // ### End the test!
        // ###
        
        test.stopTest();

        // Check that 
    }
}