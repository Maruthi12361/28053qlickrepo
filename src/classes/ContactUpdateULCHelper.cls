/***************************************************
Trigger: ContactUpdateULCTrigger
Object: Lead
Description: When a Contact is changed/updated, especially the fields
             that was used by ULC, we need to make the ulc record dirty so heroku
             connect will synch data to heroku postgres db
Change Log:
201510026   SAN 
20160324    CCE CR# 80053 - Adding semaphore to reduce SOQL queries
20170504    BAD CR# 97619 - Added Portal_User_Active__c
2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
******************************************************/

public with sharing class ContactUpdateULCHelper {

	public static void updateULC(List<Contact> triggernew, List<Contact> triggerold){
		System.debug('ContactUpdateULCTrigger: Running');
            Set<Id> ContactsId = new Set<Id>();
            List<ULC_Details__c> ULCsToBeUpdated = new List<ULC_Details__c>();

            for(Integer i = 0; i<triggernew.size();i++){
                if( triggernew[i].Email != triggerold[i].Email ||
                    triggernew[i].FirstName != triggerold[i].FirstName ||
                    triggernew[i].LastName != triggerold[i].LastName ||
                    triggernew[i].AccountId != triggerold[i].AccountId ||
                    triggernew[i].Account_Type__c != triggerold[i].Account_Type__c ||
                    triggernew[i].Account.Adopted_Navision_Status__c != triggerold[i].Account.Adopted_Navision_Status__c ||
                    triggernew[i].Account.Partner_Type__c != triggerold[i].Account.Partner_Type__c ||
                    triggernew[i].Account.ParentId != triggerold[i].Account.ParentId ||
                    triggernew[i].MailingCity != triggerold[i].MailingCity ||
                    triggernew[i].MailingCountry != triggerold[i].MailingCountry ||
                    triggernew[i].MailingPostalCode != triggerold[i].MailingPostalCode ||
                    triggernew[i].MailingStreet != triggerold[i].MailingStreet ||
                    triggernew[i].MailingState != triggerold[i].MailingState ||
                    triggernew[i].ULClevel_Calculated__c != triggerold[i].ULClevel_Calculated__c ||
                    triggernew[i].QCloudID__c != triggerold[i].QCloudID__c ||
                    triggernew[i].PartnerSourceNo__c != triggerold[i].PartnerSourceNo__c ||
                    triggernew[i].Country_Code__r.Name != triggerold[i].Country_Code__r.Name ||
                    triggernew[i].Phone != triggerold[i].Phone ||
                    triggernew[i].Portal_User_Active__c != triggerold[i].Portal_User_Active__c) 
                {
                        ContactsId.add(triggernew[i].Id);
                }         
            }
                
            for ( ULC_Details__c ulcList : [select Id, Toggle_dirty__c from ULC_Details__c where ULCStatus__c = 'Active' AND ContactId__c in : ContactsId])
            {
                ulcList.Toggle_dirty__c = !ulcList.Toggle_dirty__c;
                ULCsToBeUpdated.add(ulcList);
            }
            if (ULCsToBeUpdated.size() > 0)
            {
                update ULCsToBeUpdated;     
            }
            System.debug('ContactUpdateULCTrigger: Finishing');
        }

    }