/***
*  23.03.2017 : Rodion Vakulvsokyi
*  06.04.2017 : Rodion Vakulvsokyi QCW-1967
*  22.03.2018 : Linus Löfberg QCW-108
****/
global with sharing class LicenseDeliveryActivityManager {
    public static final String TYPECONST = 'Other';
    global class LicenseDeliveryActivityManagerRequest {
        @InvocableVariable
        public Id quoteId;
        @InvocableVariable
        public Id templateId;
        @InvocableVariable
        public String toAddress;
        @InvocableVariable
        public String ccRecipients;
    }
    global class LicenseDeliveryActivityManagerResult {
        @InvocableVariable
        public String result;
    }
    @InvocableMethod(label='Activity Creation')
    global static List<LicenseDeliveryActivityManagerResult> templateDecider(List<LicenseDeliveryActivityManagerRequest> requests) {
        List<LicenseDeliveryActivityManagerResult> results = new List<LicenseDeliveryActivityManagerResult>();
        for (LicenseDeliveryActivityManagerRequest request : requests) {
            results.add(processActivityAndEmail(request));
        }
        return results;
    }

    private static LicenseDeliveryActivityManagerResult processActivityAndEmail(LicenseDeliveryActivityManagerRequest request) {
        Messaging.SingleEmailMessage emailMessage = sendTemplatedEmail(request.ccRecipients, request.templateId, request.toAddress, request.quoteId);
        Task newTask = creatAnActivity(request, emailMessage);
        createAttachment(newTask, emailMessage);
        return null;
    }

    public static Messaging.SingleEmailMessage sendTemplatedEmail(String ccRecId, String templateId, ID targetObjId, Id inputQuoteId) {
        System.debug('!!!templateId ' + templateId);
        System.debug('!!!targetObjId ' + targetObjId);
        OrgWideEmailAddress orgWideDefAddr = [select Id from OrgWideEmailAddress where displayname = 'noreply@qlik.com' limit 1];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<User> emailCcRecipient = new List<User>([Select Id, Email From User Where id = :ccRecId]);
        if (!emailCcRecipient.isEmpty()) {
            email.setCcAddresses(new List<String>{
                    emailCcRecipient[0].email,'license.delivery@qlik.com'
            });
        }
        email.setTargetObjectId(targetObjId);
        if(orgWideDefAddr != null){
        	email.setOrgWideEmailAddressId(orgWideDefAddr.id);
        }
        email.setSaveAsActivity(false);
        email.setWhatId(inputQuoteId);
        email.setTemplateId(templateId);

        List<Messaging.SendEmailResult> previewResult = new List<Messaging.SendEmailResult>();

        try{
            previewResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            SBQQ__Quote__c quoteForSelection = [SELECT Id, Name, SBQQ__SalesRep__c FROM SBQQ__Quote__c where id =:inputQuoteId];
            if(quoteForSelection != null) {
                email = createSimpleEmail(quoteForSelection, ex.getMessage());
            }
        }
        for(Messaging.SendEmailResult rslt:previewResult){
        if(rslt.isSuccess()){
            System.debug('email sent successfully and setting License_Delivery_Date__c field');
                Datetime dt = Datetime.now();
                SBQQ__Quote__c quoteforDeliveryDate = [Select id, License_Delivery_Date__c From SBQQ__Quote__c where id =:inputQuoteId];
                quoteforDeliveryDate.License_Delivery_Date__c = dt;
                update quoteforDeliveryDate;
            }
        }
        return email;
    }

    private static Task creatAnActivity(LicenseDeliveryActivityManagerRequest request, Messaging.SingleEmailMessage emailMessage) {
        Task taskToCreate = new Task(
                Type = TYPECONST,
                WhoId = emailMessage.targetObjectId,
                Subject = emailMessage.Subject,
                CC_Address__c = String.join(emailMessage.ccAddresses, ','),
                WhatId = request.quoteId,
                Status = 'Completed'
        );
        if(Test.isRunningTest()) {
           taskToCreate.WhoId = null;
        }
        insert taskToCreate;
        return taskToCreate;
    }

    private static void createAttachment(Task inputTask, Messaging.SingleEmailMessage email) {
        Attachment attachment = new Attachment(Body = Blob.valueOf(email.getHtmlBody()),
                Name = email.Subject + '.html',
                ParentId = inputTask.id
        );
        insert attachment;
    }

    public static Messaging.SingleEmailMessage createSimpleEmail(SBQQ__Quote__c quote, String details) {
        String body = 'Your customers licenses were created, but not delivered to the customer/partner. Please contact helpdesk to help with this situation.';
        body += '</br></br>\r\n\r\n';
        body += 'Quote Id: ' + quote.Id;
        body += '</br>\r\n';
        body += 'Quote Number: ' + quote.Name;
        body += '</br>\r\n';
        body += 'License delivery failure details: ';
        body += '</br>\r\n';
        body += details;

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(quote.SBQQ__SalesRep__c);
        mail.setReplyTo('DL-IT-Sales&LegalBCS@qlik.com');
        mail.setSenderDisplayName('License Delivery Message');
        mail.setSubject('License Delivery Message');
        mail.setBccSender(false);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setPlainTextBody(body.remove('</br>'));
        mail.setHtmlBody(body);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        return mail;
    }

}