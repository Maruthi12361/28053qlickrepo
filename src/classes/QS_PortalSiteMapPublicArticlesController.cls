/* 
* Custom controller class for the QS_PortalSiteMapPublicArticles visualforce page
*
* Change log:
* 04-02-2019    ext_vos     CHG0035417: created. 
* 2019-05-05    AIN IT-1597 Updated for Support Portal Redesign
*/
public class QS_PortalSiteMapPublicArticlesController {
    public static final String ARTICLE_URL = '/articles/';

    public Map<String,String> articles {get; set;}
    public  String externalUrl {get;set;}
    
    public QS_PortalSiteMapPublicArticlesController() {
        externalUrl = [select Support_Portal_Login_Url__c from QS_Partner_Portal_Urls__c limit 1].Support_Portal_Login_Url__c;

        articles = new Map<String,String>();
        for (Basic__kav basic : [SELECT ArticleNumber FROM Basic__kav where PublishStatus = 'Online' and IsVisibleInPkb = true]) {
            articles.put(basic.ArticleNumber, externalUrl + ARTICLE_URL + basic.ArticleNumber);
        }
        for (Diagnostic__kav diagnostic : [SELECT ArticleNumber FROM Diagnostic__kav where PublishStatus = 'Online' and IsVisibleInPkb = true]) {
            articles.put(diagnostic.ArticleNumber, externalUrl + ARTICLE_URL + diagnostic.ArticleNumber);
        }
    }

}