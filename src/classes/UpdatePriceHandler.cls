/******************************************************

    Class: UpdatePriceHandler 
    
    Changelog:
        2016-05-24  Roman@4front      Migrated from UpdatePrice.trigger. 
                            
******************************************************/


public with sharing class UpdatePriceHandler {
	public UpdatePriceHandler() {
		
	}

	public static void handle(List<OpportunityLineItem> triggerNew, Boolean isInsert) {
		Map<Id,PricebookEntry> pbMap;   
		List<Id> pbeIds = new List<Id>();

		QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');	
		List<string> PriceBooksToIgnore = Settings.Exclude_From_OppUpdatePrice_Trigger__c != null ? Settings.Exclude_From_OppUpdatePrice_Trigger__c.split(',', -1) : new List<string>();

	    // get a list of pricebookentry Ids to produce the map
	    for (OpportunityLineItem oli : triggerNew)
	    	pbeIds.add(oli.PricebookEntryId);
	    
		// retrieve a map of the PricebookEntries that correspond to each Opp Product
		// can use this map each time through the loop instead of querying each time
	    pbMap = new Map<Id,PricebookEntry>(
	        [select Id, UnitPrice, Pricebook2Id 
	         from PricebookEntry
	         where Id in :pbeIds]
	    );
	    
	    // will store list price from oli or from pricebook
	    Decimal listPrice = 0.00;
	    
	    // will store the two discount percentages to be applied
	    Decimal partnerMargin = 0.0000;
	    Decimal discountPercent = 0.0000;

	    // prices that reflect discounts
	    Decimal priceAfterPartnerMargin = 0.00;
	    Decimal priceAfterDiscount = 0.00;
	    priceAfterPartnerMargin.setScale(2, System.RoundingMode.HALF_UP);
	    priceAfterDiscount.setScale(2, System.RoundingMode.HALF_UP);

	    for (OpportunityLineItem oli : triggerNew) {
	        
	        Boolean IgnoreItem = false;
	        string pbi = null;
	    	if (pbMap.containsKey(oli.PriceBookEntryId))
	    	{
	        	pbi = (string) pbMap.get(oli.PriceBookEntryId).Pricebook2Id;
		        for (string PriceBookEntryId : PriceBooksToIgnore)
		        {
			        	if (pbi.startsWith(PriceBookEntryId)) 
			        	{
			        		IgnoreItem = true;
			        	}	        	
		        }
	    	}
	    	system.debug('PBEId: ' + oli.PriceBookEntryId);
	        system.debug('PriceBookToIgnore: ' + PriceBooksToIgnore);
	        system.debug('pbi: ' + pbi);
	        
	        if (IgnoreItem) continue;
	        
	        // get the list price from the pricebook if the trigger is an insert (since the oli will not have ListPrice on a before insert)
	        // otherwise get the list price from the oli's list price
	        // modified by Steve - outside if statment added, original line was the one in the else
	        if(oli.CPQ_List_Price__c !=null) {            
	            if(oli.CPQ_List_Price__c < 0) {
	                listPrice = oli.CPQ_List_Price__c * -1;
	            } else {    
	                listPrice = oli.CPQ_List_Price__c;
	            }
	        } else {    
	            listPrice = (isInsert && pbMap.containsKey(oli.PricebookEntryId) ? pbMap.get(oli.PricebookEntryId).UnitPrice : oli.ListPrice);
	        }
	        
	        Double tempPartner = Double.valueOf(oli.Partner_Margin__c);
	        // get the partner_margin if it is not null, otherwise set to 0
	        partnerMargin = (oli.Partner_Margin__c != null ? Decimal.valueOf(tempPartner).divide(100, 8, System.RoundingMode.HALF_UP) : 0.00);

	        Double tempDiscount = Double.valueOf(oli.Discount__c);
	        // get the discount percent if it is not null, otherwise set to 0
	        discountPercent = (oli.Discount__c != null ? Decimal.valueOf(tempDiscount).divide(100, 8, System.RoundingMode.HALF_UP) : 0.00);

	        // Don't apply partner margin to registration related OLI.
	        if (oli.Registration__c != null) {
	            partnerMargin = 0.00;
	            discountPercent = 0.00;
	        }

	        
	        // calculate price based on Partner Margin percentage
	        // this will essentially multiply the price by 1 if Partner_Margin__c is null
	        priceAfterPartnerMargin = (1.00 - partnerMargin) * listPrice;

	        // price after discount is the Discount applied to the partnerListPrice
	        // (recall that if either percentages are null this is essentially multiplying by 1)
	        priceAfterDiscount = (1.00 - discountPercent) * priceAfterPartnerMargin;
	            

	        if (oli.Discounted_Price__c != null) {
	            // calculate the discount percentage by comparing the Discounted Price to the 
	            // price after the partner margin is taken
	            if (priceAfterPartnerMargin != 0.00) {
	                oli.Discount__c = 100*(1.000 - (oli.Discounted_Price__c.divide(priceAfterPartnerMargin, 8, System.RoundingMode.HALF_UP)));
	            }
	            oli.UnitPrice = oli.Discounted_Price__c;
	        } else {
	            // set the Sales Price (UnitPrice) of the oli to the calculated discounted price
	            // (note that if Discount__c is null than this will simply be the 
	            //  list price with the partner margin discount taken)
	            oli.UnitPrice = priceAfterDiscount;
	        }
	    }
	}
}