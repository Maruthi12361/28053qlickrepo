/**
* Change log:
*
*   2019-07-24 ext_ciz	CHG0036474: create testAddAtRiskEscalationByAccount test
*   2019-10-07 ext_bad	IT-2173: move functionality to the component
*/
@IsTest
private class AddAtRiskEscalationControllerTest {

    static testMethod void testAddAtRiskEscalationByAccount() {
        AddAtRiskEscalationByAccountController contr = new AddAtRiskEscalationByAccountController();

        Test.startTest();
        PageReference ref = contr.back();
        Test.stopTest();

        System.assert(ref.getUrl().contains('AtRiskEscalationsList'));
    }
}