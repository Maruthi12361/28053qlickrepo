/**
    16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors
    02.09.2017 : Srinivasan PR : fix for query errors
    2017-09-15 : UIN : Changes due to the opplineitem afterupdate change.
    25-10-2017  Oleksandr@4Front QCW-4238 fixed test errors 
	03-09-2018 : Added new testmethod processTestmethod5 to increase code coverage for OpportunityForecastProductsHandler (BSL-692) 
**/
@isTest
public class OppForecastProductHndlrTest {
    private static final String DEALCHILD = 'Deal_Split_Child';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    public static testmethod void processTestmethod() {
        QuoteTestHelper.createCustomSettings();
        
        Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
        
        RecordType recTypeAccEndUser = [Select id, DeveloperName From Recordtype Where DeveloperName = 'End_User_Account' and SobjectType = 'Account'];
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, recTypeAccEndUser, 'EndUserAcc');
        testAccount.Territory_Country__c = 'France';
        insert testAccount;               
       
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account'
                                  and sobjecttype='Account']; //fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        Test.startTest();
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', true, '');
        //quoteForTest.SBQQ__Primary__c = true;
        //insert quoteForTest;
        quoteForTest.Short_Description__c = 'quoteDesc';
        quoteForTest.Bypass_Rules__c = true;
        insert quoteForTest;
        Test.stopTest();
        quoteForTest.No_of_Subscription_Units__c = String.valueOf(2);
        update quoteForTest;            
        
        SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        insert testQuoteLine;
        //}
    }

     public static testmethod void processTestmethod2() {
        User testUser = [Select id From User where id =: userInfo.getUserId()];
        RecordType rType = [select id, developerName From RecordType where developerName =: DEALCHILD];
        QuoteTestHelper.createCustomSettings();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        update testAccount;

        Opportunity oppCreatedForUpdate1 = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        insert oppCreatedForUpdate1;

        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Deal_Split_Child']);
        oppCreatedForUpdate.License_Forecast_Amount__c = 25;
        oppCreatedForUpdate.Deal_Split_Parent_Opportunity__c = oppCreatedForUpdate1.Id;
        oppCreatedForUpdate.Deal_Split_Percentage__c = 10;

        insert oppCreatedForUpdate;
        
        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            pbEntryTest.CurrencyIsoCode = 'USD';
            insert pbEntryTest;
        
        OpportunityLineItem createdLineItem = QuoteTestHelper.createLineItem(oppCreatedForUpdate.id, pbEntryTest);
        insert createdLineItem;
        
        OpportunityForecastProductsHandler.afterInsert(new List<Opportunity>{oppCreatedForUpdate});
            
        System.assertEquals(true,[select id From OpportunityLineItem].size() > 0);
        
    }

     public static testmethod void processTestmethod3() {
        User testUser = [Select id From User where id =: userInfo.getUserId()];
        RecordType rType = [select id, developerName From RecordType where developerName =: DEALCHILD];
        QuoteTestHelper.createCustomSettings();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        update testAccount;
        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Deal_Split_Child']);
            oppCreatedForUpdate.License_Forecast_Amount__c = 25;
        insert oppCreatedForUpdate;
        
        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            pbEntryTest.CurrencyIsoCode = 'USD';
            insert pbEntryTest;
        
        //OpportunityLineItem createdLineItem = QuoteTestHelper.createLineItem(oppCreatedForUpdate.id, pbEntryTest);
        //insert createdLineItem;
        Opportunity oldOpp = oppCreatedForUpdate;
        oldOpp.License_Forecast_Amount__c = 0;

        Map<Id, Opportunity> mapOfOpportunity = new Map<Id, Opportunity>();
        mapOfOpportunity.put(oldOpp.id, oldOpp);
        System.debug(mapOfOpportunity);

        OpportunityForecastProductsHandler.afterUpdate(new List<Opportunity>{oppCreatedForUpdate}, mapOfOpportunity);
            
        System.assertEquals(false,[select id From OpportunityLineItem].size() > 0);
        
                OpportunityLineItem oli = new OpportunityLineItem();
                oli.Quantity = 1;
                oli.UnitPrice = 1000;
                oli.PricebookEntryId = pbEntryTest.id;
                oli.OpportunityId = oldOpp.id; 
                oli.Partner_Margin__c = 40;
                oli.Discounted_Price__c = 900;
                insert oli;
                
        OppProductRollupMaker.onAfterInsert(new List<OpportunityLineItem>{oli});            
        OppProductRollupMaker.onBeforeInsert(new List<OpportunityLineItem>{oli});       
        OppProductRollupMaker.onBeforeUpdate(new List<OpportunityLineItem>{oli});       
        OppProductRollupMaker.onAfterDelete(new List<OpportunityLineItem>{oli}); 
         
        Set<String> setOfOppsid = new Set<String>();
        for(OpportunityLineItem oppLineItem : new List<OpportunityLineItem>{oli}) {
            setOfOppsid.add(oppLineItem.OpportunityId);
        }
         
        Map<String, SBQQ__Quote__c> mapQuoteFromOpp = OpportunityScheduleCreationHandler.queryQuoteFromOpp(setOfOppsid);
        OpportunityScheduleCreationHandler.onBeforeInsert(new List<OpportunityLineItem>{oli});
        OpportunityScheduleCreationHandler.onBeforeUpdate(new List<OpportunityLineItem>{oli});
        
        //QCW-4737
        //OpportunityScheduleCreationHandler.processScheduleSub(new List<OpportunityLineItem>{oli}, mapQuoteFromOpp);
        
        Map<Id, OpportunityLineItem> mapOfOpportunityLI = new Map<Id, OpportunityLineItem>();
        mapOfOpportunityLI.put(oli.id, oli);
        Map<Id, OpportunityLineItem> oldMapOfOpportunityLI = new Map<Id, OpportunityLineItem>();// UIN Test Class Fix
        oldMapOfOpportunityLI.put(oli.id, oli);// UIN Test Class Fix
        
        OpportunityScheduleCreationHandler.onAfterInsert(new List<OpportunityLineItem>{oli}, mapOfOpportunityLI);
        //OpportunityScheduleCreationHandler.onAfterUpdate(new List<OpportunityLineItem>{oli}, mapOfOpportunityLI);
        OpportunityScheduleCreationHandler.onAfterUpdate(new List<OpportunityLineItem>{oli}, mapOfOpportunityLI, oldMapOfOpportunityLI);// UIN Test Class Fix
        OpportunityScheduleCreationHandler.deleteOldSchedules(new List<OpportunityLineItem>{oli},mapQuoteFromOpp);
    }

     public static testmethod void processTestmethod4() {
        User testUser = [Select id From User where id =: userInfo.getUserId()];
        RecordType rType = [select id, developerName From RecordType where developerName =: DEALCHILD];
        QuoteTestHelper.createCustomSettings();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        update testAccount;
        
        Opportunity oppCreatedForUpdate1 = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        insert oppCreatedForUpdate1;

        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Deal_Split_Child']);
        oppCreatedForUpdate.License_Forecast_Amount__c = 25;
        oppCreatedForUpdate.Deal_Split_Parent_Opportunity__c = oppCreatedForUpdate1.Id;
        oppCreatedForUpdate.Deal_Split_Percentage__c = 10;
                
            oppCreatedForUpdate.Consultancy_Forecast_Amount__c = 0;
            oppCreatedForUpdate.Education_Forecast_Amount__c  = 25;
            oppCreatedForUpdate.Support_Forecast_Amount__c  = 25;
        insert oppCreatedForUpdate;
        
        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;
    
        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            pbEntryTest.CurrencyIsoCode = 'USD';
            insert pbEntryTest;
            
        //QCW-4737
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = oppCreatedForUpdate.Id , PriceBookEntryId = pbEntryTest.Id , UnitPrice = 1000, Quantity = 1);
        insert oli;
        
        //OpportunityLineItem createdLineItem = QuoteTestHelper.createLineItem(oppCreatedForUpdate.id, pbEntryTest);
        //insert createdLineItem;
        Opportunity oldOpp = oppCreatedForUpdate;
        oldOpp.License_Forecast_Amount__c = 5;
        oldOpp.Consultancy_Forecast_Amount__c = 5; 
        oldOpp.Education_Forecast_Amount__c  = 5;
        oldOpp.Support_Forecast_Amount__c  = 5;
        Map<Id, Opportunity> mapOfOpportunity = new Map<Id, Opportunity>();
        mapOfOpportunity.put(oldOpp.id, oldOpp);
        System.debug(mapOfOpportunity);
    
        OpportunityForecastProductsHandler.afterUpdate(new List<Opportunity>{oppCreatedForUpdate}, mapOfOpportunity);
        
        //QCW-4737
        oli.Quantity = 2;
        update oli;
        delete oli;
        //System.assertEquals(false,[select id From OpportunityLineItem].size() > 0);
        
    }
    
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr, 
                                            Country_Name__c = countryName, 
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
	
	//Added by Anjuna for BSL-692
    public static testmethod void processTestmethod5() {
        User testUser = [Select id From User where id =: userInfo.getUserId()];
        QuoteTestHelper.createCustomSettings();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        update testAccount;
        
        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        insert oppCreatedForUpdate;
        
        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            pbEntryTest.CurrencyIsoCode = 'USD';
            insert pbEntryTest;
        
        OpportunityLineItem createdLineItem = QuoteTestHelper.createLineItem(oppCreatedForUpdate.id, pbEntryTest);
        insert createdLineItem;
        
        OpportunityForecastProductsHandler.afterInsert(new List<Opportunity>{oppCreatedForUpdate});
        
        System.assertEquals(true,[select id From Opportunity].size() > 0);
        
        oppCreatedForUpdate.License_Forecast_Amount__c = 0;
        
        Map<Id, Opportunity> mapOfOpportunity = new Map<Id, Opportunity>();
        mapOfOpportunity.put(oppCreatedForUpdate.id, oppCreatedForUpdate);
        System.debug(mapOfOpportunity);

        OpportunityForecastProductsHandler.afterUpdate(new List<Opportunity>{oppCreatedForUpdate}, mapOfOpportunity);

    }
}