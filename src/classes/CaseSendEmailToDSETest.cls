/****************************************************************************************************

    CaseSendEmailToDSE
    
    Test class for CaseSendEamilToDSE trigger
            
    Changelog:

        2016-04-06  AIN Initial implementation
        2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.

****************************************************************************************************/
@isTest
private class CaseSendEmailToDSETest {
    static testMethod void test1() {
        QTTestUtils.GlobalSetUp();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //query profile for dse
        Profile profileRec = [select id from profile where Name = 'System Administrator' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        //Create DSE user
        User dseUser = TestDataFactory.createUser(profileId, 'testSandboxdse@qlikTech.com', 'tSboxdse', null);
        insert dseUser;

    //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.QT_Designated_Support_Contact__c = dseUser.Id;
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.DSE_Access__c = true;
        insert testContact;

        // Retrieve Customer Profile and get the profile id
        profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Semaphores.CustomCaseTriggerAfterInsert = false;
        System.runAs(communityUser) {
          Case caseObj = new Case();
          caseObj.AccountId = testAccount.Id;
          caseObj.ContactId = testContact.Id;
          caseObj.Status = 'New';
          caseObj.Designated_Support_Engineer__c = dseUser.id;
          insert caseObj;
        }

        system.assertEquals(CaseSendEmailToDSE.EmailWasSent, true);
    }

    static testMethod void test2() {
        CaseSendEmailToDSE.EmailWasSent = false;
        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //query profile for dse
        Profile profileRec = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        //Create DSE user
        User dseUser = TestDataFactory.createUser(profileId, 'testSandboxdse@qlikTech.com', 'tSboxdse', null);
        insert dseUser;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.QT_Designated_Support_Contact__c = dseUser.Id;
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName',
                'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.DSE_Access__c = false;
        insert testContact;

        // Retrieve Customer Profile and get the profile id
        profileRec = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Base Access + QlikBuy' LIMIT 1];
        profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        Semaphores.SetAllSemaphoresToFalse();
        System.runAs(communityUser) {
            Case caseObj = new Case();
            caseObj.AccountId = testAccount.Id;
            caseObj.ContactId = testContact.Id;
            caseObj.Status = 'New';
            insert caseObj;
        }

        system.assertEquals(false, CaseSendEmailToDSE.EmailWasSent);
    }
}