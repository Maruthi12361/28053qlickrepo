/**
* Change log:
*
* 2018-03-05    ext_vos     CHG0031154: add test for 'To queue' logic.
* 2018-05-17   ext_vos     CHG0034020: add test for status updating for "To Queue" functionality.
* 2019-10-16	ext_bad		IT-2185: Added Attunity queue.
*/
@isTest
private class SCC_CaseButtonHolderExtensionTest {
    public static testmethod void TestPickup() {

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        Case testCase = new Case();
        testCase.Contact = testContact;
        testCase.Account = testAccount;
        insert testCase;

        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        SCC_CaseButtonHolderExtension testExtension = new SCC_CaseButtonHolderExtension(sc);

        PageReference pageRef = Page.SCC_CaseButtonHolder;
        pageRef.getParameters().put('id', String.valueOf(testCase.Id));
        Test.setCurrentPage(pageRef);

        System.assertEquals(testCase.Id, testExtension.getCaseObj().Id);
        String error = testExtension.getErrorMsg();

        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(mockSysAdmin) {
            testExtension.CasePickup();
        }

        System.assertEquals(mockSysAdmin.Id, testExtension.getCaseObj().OwnerId);
    }

    public static testmethod void TestBackToQueue() {

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testBackToQueue@qlikTech.com', '+44-8787878787', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        Case testCase = new Case();
        testCase.Contact = testContact;
        testCase.Account = testAccount;
        testCase.Type = 'Incident';
        testCase.Status = 'New';
        insert testCase;

        Case toTest = [select Id, Status, Type from Case where Id =: testCase.Id];
        System.assertEquals('Incident', toTest.Type);
        System.assertEquals('New', toTest.Status);

        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        SCC_CaseButtonHolderExtension testExtension = new SCC_CaseButtonHolderExtension(sc);

        PageReference pageRef = Page.SCC_CaseButtonHolder;
        pageRef.getParameters().put('id', String.valueOf(testCase.Id));
        Test.setCurrentPage(pageRef);
        System.assertEquals(testCase.Id, testExtension.getCaseObj().Id);
     
        // Incident Type
        testExtension.CasePickup();      
        Group queueForTest = [select Id from Group where DeveloperName = 'Technical_Product_Support'];
        System.assertEquals(queueForTest.Id, testExtension.getCaseObj().OwnerId);
        System.assertEquals(toTest.Status, testExtension.getCaseObj().Status);
        System.assert(testExtension.getIsCanPickup());

        // Service Request Type
        testCase.Type = 'Service Request';
        testCase.OwnerId = UserInfo.getUserId();
        update testCase;
        toTest = [select Id, Type from Case where Id =: testCase.Id];
        System.assertEquals('Service Request', toTest.Type);

        testExtension.CasePickup();
        queueForTest = [select Id from Group where DeveloperName = 'Qoncierge_Service'];
        System.assertEquals(queueForTest.Id, testExtension.getCaseObj().OwnerId);
        System.assert(testExtension.getIsCanPickup());

        // Designated Support Engineer is assigned
        testCase.Designated_Support_Engineer__c = UserInfo.getUserId();
        testCase.OwnerId = UserInfo.getUserId();
        update testCase;

        testExtension.CasePickup();
        queueForTest = [select Id from Group where DeveloperName = 'Designated_Support_Engineer' limit 1];
        System.assertEquals(queueForTest.Id, testExtension.getCaseObj().OwnerId);
        System.assert(testExtension.getIsCanPickup());

        // Proactive Type -> case can be pickuped and cannot be returned to Queue
        testCase.Type = 'Proactive';
        testCase.Designated_Support_Engineer__c = null;
        update testCase;
        toTest = [select Id, Type from Case where Id =: testCase.Id];
        System.assertEquals(queueForTest.Id, testExtension.getCaseObj().OwnerId);
        System.assertEquals('Proactive', toTest.Type);

        testExtension.CasePickup();
        System.assertEquals(UserInfo.getUserId(), testExtension.getCaseObj().OwnerId);
        System.assert(!testExtension.getIsCanPickup());

        testCase.Product__c = 'Attunity Visibility';
        update testCase;
        testExtension.CasePickup();
        toTest = [select Id, Attunity_Product__c from Case where Id =: testCase.Id];
        queueForTest = [select Id from Group where DeveloperName = 'Attunity_Case_Queue' limit 1];
        System.assert(toTest.Attunity_Product__c);
        System.assertEquals(queueForTest.Id, testExtension.getCaseObj().OwnerId);
    }
}