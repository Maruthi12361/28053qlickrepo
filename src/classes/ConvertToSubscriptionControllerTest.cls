/***************************************************
*
* 2018-11-26 AIN Test class created for class ConvertToSubscriptionController
*
***************************************************/
@isTest
private class ConvertToSubscriptionControllerTest {
	
	@isTest static void test_method_one() {
		QTTestUtils.GlobalSetUp();
		Semaphores.SetAllSemaphoresToTrue();
		Account acc = Z_TestFactory.makeAccount('Sweden', 'Test Account', 'Skåne', '21364');
		acc.ECUSTOMS__RPS_Status__c= 'No Matches';
        acc.ECUSTOMS__RPS_Date__c= Date.Today();
        acc.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        acc.BillingStreet='hhhh';
        acc.BillingCity='iiiii';
        insert acc;
        acc.Pending_Validation__c =FALSE;
        update acc;

		Contact con = Z_TestFactory.makeContact(acc);
		insert con;

		//Set another record type and forecast amount than what the function we are testing
		Id SalesQCCSRecordtypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS').getRecordTypeId();   
		

		Opportunity opp = Z_TestFactory.makeOpportunity(acc, con);
		opp.License_Forecast_Amount__c = 100;
		opp.RecordTypeId = SalesQCCSRecordtypeId;
		insert opp;

		zqu__Quote__c quote = Z_TestFactory.makeQuote(opp, acc);
		quote.zqu__BillToContact__c = con.Id;
		quote.zqu__SoldToContact__c = con.Id;
		quote.Billing_Frequency__c = 'Prepaid';
		insert quote;

		Semaphores.SetAllSemaphoresToFalse();
		ApexPages.StandardController stc = new ApexPages.StandardController(opp);
		ConvertToSubscriptionController controller = new ConvertToSubscriptionController(stc);

		
		opp = [select id, License_Forecast_Amount__c, recordtypeid from Opportunity where Id = :opp.id];
		system.assertequals(opp.recordtypeid, SalesQCCSRecordtypeId, 'Record type ID mismatch before function');
		system.assertequals(opp.License_Forecast_Amount__c, 100, 'Forecast amount mismatch before function');

		controller.updateOpp();

		Id SalesQCCSZouraRecordtypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS Zuora Subscription').getRecordTypeId();   
		opp = [select id, License_Forecast_Amount__c, recordtypeid from Opportunity where Id = :opp.id];
		system.assertequals(opp.recordtypeid, SalesQCCSZouraRecordtypeId, 'Record type ID mismatch after function');
		system.assertequals(opp.License_Forecast_Amount__c, 0, 'Forecast amount mismatch after function');
	}
}