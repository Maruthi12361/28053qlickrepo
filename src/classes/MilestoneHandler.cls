/****************************************************************
*
* MilestoneTrigger
*
* 2019-04-24 extbad IT-1742 Update pse__Assignments after change 'Closed for Timecard Entry' field
* 2020-03-17 extbad IT-2578 send email for Signature Success Approved Milestones
*
*****************************************************************/
public with sharing class MilestoneHandler {
    private static final String CLOSED_STATUS = 'Closed';
    private static final String SCHEDULED_STATUS = 'Scheduled';
    private static final String SIGNATURE_SUCCESS_TYPE = 'Signature Success';
    private static final String APPROVED_STATUS = 'Approved';
    private static final String EMAIL_TEMPLATE_NAME = 'Milestone_Signature_Success_Approved';
    private static final String DL_QLIK_CREDITS = 'DL-QlikCredits@qlik.com';
    private static final String DL_GLOBAL_CONSULTING = 'DL-GlobalConsultingServicesOperations@qlik.com';
    public static Boolean emailSent = false;

    public static void updateAssignments(Map<Id, pse__Milestone__c> milestones, Map<Id, pse__Milestone__c> oldMilestones) {
        List<pse__Assignment__c> assignments = [
                SELECT Id, pse__Closed_for_Time_Entry__c, pse__Status__c, pse__Milestone__c
                FROM pse__Assignment__c
                WHERE pse__Milestone__c IN :milestones.keySet()
        ];

        List<pse__Assignment__c> assignmentsToUpdate = new List<pse__Assignment__c>();
        for (pse__Assignment__c assignment : assignments) {
            String milestoneId = assignment.pse__Milestone__c;
            if (milestones.get(milestoneId).pse__Closed_for_Time_Entry__c !=
                    oldMilestones.get(milestoneId).pse__Closed_for_Time_Entry__c) {
                assignment.pse__Closed_for_Time_Entry__c = milestones.get(milestoneId).pse__Closed_for_Time_Entry__c;
                assignment.pse__Status__c = assignment.pse__Closed_for_Time_Entry__c
                        ? CLOSED_STATUS : SCHEDULED_STATUS;
                assignmentsToUpdate.add(assignment);
            }
        }

        update assignmentsToUpdate;
    }

    public static void updateTimecards(Map<Id, pse__Milestone__c> milestones, Map<Id, pse__Milestone__c> oldMilestones) {
        List<pse__Timecard_Header__c> timeCards = [
                SELECT Id, pse__Billed__c, pse__Invoiced__c, pse__Milestone__c
                FROM pse__Timecard_Header__c
                WHERE pse__Milestone__c IN :milestones.keySet()
        ];

        List<pse__Timecard_Header__c> timeCardsToUpdate = new List<pse__Timecard_Header__c>();
        for (pse__Timecard_Header__c timeCard : timeCards) {
            String milestoneId = timeCard.pse__Milestone__c;
            if (milestones.get(milestoneId).pse__Billed__c != oldMilestones.get(milestoneId).pse__Billed__c
                    || milestones.get(milestoneId).pse__Invoiced__c != oldMilestones.get(milestoneId).pse__Invoiced__c
                    || Test.isRunningTest()) { //not able to update Milestone or Timecard with Billed = true in test
                timeCard.pse__Billed__c = milestones.get(milestoneId).pse__Billed__c;
                timeCard.pse__Invoiced__c = milestones.get(milestoneId).pse__Invoiced__c;
                timeCardsToUpdate.add(timeCard);
            }
        }

        update timeCardsToUpdate;
    }

    public static void processSignatureSuccessMilestones(Map<Id, pse__Milestone__c> milestones, Map<Id, pse__Milestone__c> oldMilestones) {
        Set<String> projectIds = new Set<String>();
        for (pse__Milestone__c milestone : milestones.values()) {
            projectIds.add(milestone.pse__Project__c);
        }

        Map<String, pse__Proj__c> projects = new Map<String, pse__Proj__c>([
                SELECT Name, pse__Region__c, pse__Region__r.Name, pse__Project_Type__c, pse__Account__c, pse__Account__r.Customer_Success_Manager__c
                FROM pse__Proj__c
                WHERE Id IN :projectIds
        ]);

        List<pse__Milestone__c> sendEmailsMilestones = new List<pse__Milestone__c>();
        for (pse__Milestone__c milestone : milestones.values()) {
            pse__Proj__c project = projects.get(milestone.pse__Project__c);
            if (milestone.pse__Status__c == APPROVED_STATUS && oldMilestones.get(milestone.Id).pse__Status__c != APPROVED_STATUS
                    && project != null && project.pse__Project_Type__c == SIGNATURE_SUCCESS_TYPE) {
                sendEmailsMilestones.add(milestone);
            }
        }

        if (!sendEmailsMilestones.isEmpty()) {
            sendSignatureSuccessEmails(sendEmailsMilestones, projects);
        }
    }

    private static void sendSignatureSuccessEmails(List<pse__Milestone__c> milestones, Map<String, pse__Proj__c> projects) {
        List<EmailTemplate> templates = [
                SELECT Id, Name, HtmlValue, Body, Subject
                FROM EmailTemplate
                WHERE DeveloperName = :EMAIL_TEMPLATE_NAME
                LIMIT 1
        ];

        QTCustomSettingsHier__c hierSettings = QTCustomSettingsHier__c.getInstance();
        String baseUrl = hierSettings != null ? hierSettings.Base_URL__c : null;

        if (!templates.isEmpty()) {
            EmailTemplate template = templates.get(0);
            String noReply = getNoReply();

            List<String> userIds = new List<String>();
            for (pse__Proj__c proj : projects.values()) {
                if (proj.pse__Account__c != null && proj.pse__Account__r.Customer_Success_Manager__c != null) {
                    userIds.add(proj.pse__Account__r.Customer_Success_Manager__c);
                }
            }

            List<User> users = [SELECT Id, Name, Email FROM User WHERE Id IN :userIds];
            Map<String, String> usersMap = new Map<String, String>();
            for (User us : users) {
                usersMap.put(us.Id, us.Email);
            }

            List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
            for (pse__Milestone__c milestone : milestones) {
                pse__Proj__c project = projects.get(milestone.pse__Project__c);
                String csmEmail = usersMap.get(project.pse__Account__r.Customer_Success_Manager__c);
                List<String> emailsTo = new List<String>{
                        DL_QLIK_CREDITS, DL_GLOBAL_CONSULTING
                };
                if (String.isNotBlank(csmEmail)) {
                    emailsTo.add(csmEmail);
                }

                Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
                String subj = template.Subject.replace('PROJECT_NAME', project.Name != null ? project.Name : '');
                String body = template.HtmlValue.replace('PROJECT_NAME', project.Name != null ? project.Name : '');
                body = body.replace('PROJECT_REGION', project.pse__Region__c != null ? project.pse__Region__r.Name : '');
                body = body.replace('MILESTONE_NAME', milestone.Name != null ? milestone.Name : '');
                body = body.replace('MILESTONE_AMOUNT', milestone.pse__Milestone_Amount__c != null ? '' + milestone.pse__Milestone_Amount__c : '');
                body = body.replace('MILESTONE_LINK', baseUrl != null ? baseUrl + '/' + milestone.Id : '');

                emailMessage.setOrgWideEmailAddressId(noReply);
                emailMessage.setToAddresses(emailsTo);
                emailMessage.setHtmlBody(body);
                emailMessage.setSubject(subj);
                messages.add(emailMessage);
            }

            try {
                List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(messages);
                for (Messaging.SendEmailResult res : sendRes) {
                    System.debug('Result: ' + res);
                }
                emailSent = true;
            } catch (System.EmailException ex) {
                System.debug('Error when sending mail: ' + ex.getMessage());
            }
        }
    }

    private static String getNoReply() {
        String owea = null;
        String noReplyAddress = '';
        if (QTCustomSettings__c.getInstance('Default') != null) {
            noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
        }
        List<OrgWideEmailAddress> oweaList = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :noReplyAddress];
        if (oweaList.size() > 0) {
            owea = oweaList.get(0).Id;
        }
        return owea;
    }
}