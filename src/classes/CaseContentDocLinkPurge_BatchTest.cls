/****************************************************************************************************
  IT-79
    CaseAttachment ContentDocumentLinkPurge
    Description: Test class for Scheduler CaseContentDocumentLinkPurge_RunBatch
    IT-467 :Custom settings records added to maintain code coverage for CaseAttachmentPurge CRW
    IT-606 : Run scheduled CaseContentDocumentLinkPurge_RunBatch - ext_bad - 2018-05-16
****************************************************************************************************/    
@isTest 
private class CaseContentDocLinkPurge_BatchTest  {

    static testMethod void CaseContentDocumnetsLinksPurge_RunSceduleBatch() {
        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.CaseAttachmentPurgeEmail__c = 'test@test.com';
        settings.QlikNoReplyEmailAddress__c = 'no-reply@test.com';
        insert settings;

        CaseContentDocumentLinkPurge_RunBatch csh1 = new CaseContentDocumentLinkPurge_RunBatch();

        Test.StartTest();
        csh1.execute();
        Test.stopTest();
    }
}