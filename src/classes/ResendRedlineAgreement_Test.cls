/********
* NAME : ResendRedlineAgreement_Test
* Description: Tests for Helper class to send a redline agreement to Netsuite through Boomi
* 
*
*Change Log:
	MTM		 2014-12-05   CR# 19375 Created test class
    IRN      2015-10-29   Added testmethods testCanSendAgreement to get a better code coverage 
    Roman Dovbush (4front) : 2017-24-03 : Record type 'QlikBuy II' for Oppty was deleted,
    so old rec type replaced with rec type 'Sales QCCS' in Setup().
	29.03.2017 Rodion Vakulvoskyi fix test failures commenting additional  Qttestutils.GlobalSetUp(); 
******/

@isTest
private class ResendRedlineAgreement_Test {

    private static Opportunity opp;
    private static ResendRedlineAgreement batch;
    private static void Setup()
    {
        //QTTestUtils.GlobalSetUp();

        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
        Semaphores.Opportunity_ManageForecastProductsIsInsert = true; 
        /*NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
        settings.UserRecordTypeIdsToExclude__c = '005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';

        insert settings;

        QTCustomSettings__c settings2 = new QTCustomSettings__c();
        settings2.Name = 'Default';
        settings2.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        settings2.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        settings2.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
   
        insert settings2;*/


        User aUser = QTTestUtils.createMockOperationsAdministrator();
        Account anAcc = QTTestUtils.createMockAccount('Redline Test Test Account', aUser);
        Id QBuy2RecordType;
        	if(NS_Settings_Detail__c.getInstance('NSSettingsDetail') != null) {
            	QBuy2RecordType = NS_Settings_Detail__c.getInstance('NSSettingsDetail').Qlikbuy_II_Opp_Record_Type__c;
            }
        Id oppSalesRecordType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id;

        batch = new ResendRedlineAgreement();
        opp = QTTestUtils.createOpportunity('RedlineTest', 'Redline test Opp', 'New Customer', 'Direct', 'agreement send', oppSalesRecordType, 'GBP', aUser, anAcc);		
    }
    static testMethod void testSendNullAgreement()
    { 
        QTTestUtils.GlobalSetUp();
    	Setup();
        test.startTest();
        // test send null as agreement Id
        Boolean status = batch.SendToNS(null, null);        
        System.assertEquals(false, status);
        test.stopTest();
    }

    static testMethod void testSendToNS()
    { 
        QTTestUtils.GlobalSetUp();
        Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());
        Setup();
        NS_Quote__c quote = QTTestUtils.CreateQuote(opp, 'Waiting for Finance Approval');   
        Redline_Manuel_Agreement__c agreement = CreateAgreement(opp, quote,'ERP Transfer in progress');     
        test.startTest();
        // test send null as agreement Id
        Boolean status = batch.SendToNS(agreement, quote);        
        System.assertEquals(true, status);

        status = batch.SendToNS(agreement, null);        
        System.assertEquals(false, status);

        test.stopTest();
    }
    
    static testMethod void testSendAgreementByBoomi()
    {
        QTTestUtils.GlobalSetUp();
        Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());
        Setup();
        test.startTest();        
        NS_Quote__c quote = QTTestUtils.CreateQuote(opp, 'Waiting for Finance Approval');
        Redline_Manuel_Agreement__c agreement = CreateAgreement(opp, quote,'ERP Transfer in progress');
        AddAttachment(agreement.Id);
        boolean Status = batch.SendAgreementByBoomi(agreement, quote);        
        System.assertEquals(true, status);  
        Status = batch.SendAgreementByBoomi(null, null);        
        System.assertEquals(false, status);              
        test.stopTest();
    }
    
    static testMethod void testSendAgreement() 
    {	
		QTTestUtils.GlobalSetUp();
        Setup();
        test.StartTest();
        NS_Quote__c quote = QTTestUtils.CreateQuote(opp, 'Waiting for Finance Approval');
        Redline_Manuel_Agreement__c agreement = CreateAgreement(opp, quote,'ERP Transfer in progress');            
        ID batchId = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    static testMethod void TestRedlineBatch()
    {
        test.StartTest();
        ResendRedlineAgreement schedule = new ResendRedlineAgreement();
        test.stopTest();
    }
    
    static testMethod void testCanSendAgreement()
    {	
		QTTestUtils.GlobalSetUp();
        Setup();
        test.startTest();
        NS_Quote__c quote = QTTestUtils.CreateQuote(opp, 'Waiting for Finance Approval');   

        Redline_Manuel_Agreement__c agreement = CreateAgreement(opp, quote,'ERP Transfer in progress');     
        Boolean canSend = batch.CanSendAgreement(agreement, null);
        System.assert(!canSend);

        canSend = batch.canSendAgreement(agreement, quote);
        System.assert(canSend);

        NS_Quote__c quote2 = QTTestUtils.CreateQuote(opp, 'Waiting for Finance Approval');
        canSend = batch.canSendAgreement(agreement, quote2);  
        System.assert(!canSend);

        test.stopTest();
        
    }

    static testMethod void testGetPrimaryQuote()
    {	
		QTTestUtils.GlobalSetUp();
        Setup();
        test.startTest();
        NS_Quote__c quote = QTTestUtils.CreateQuote(opp, 'Waiting for Finance Approval');     
        Redline_Manuel_Agreement__c agreement = CreateAgreement(opp, quote,'ERP Transfer in progress');     
        Map<Id, Opportunity> opps = new Map<Id, Opportunity>();
        opps.put(opp.Id, opp);
        NS_Quote__c primaryQuote = batch.GetPrimaryQuote(opp.Id, opps);
        System.assertEquals(primaryQuote, null);

        opp.StageName = 'Closed';
        update opp;
        primaryQuote = batch.GetPrimaryQuote(opp.Id, opps);
        System.assertEquals(primaryQuote, null);
        test.stopTest();
        
    }

    public static Redline_Manuel_Agreement__c CreateAgreement(Opportunity o, NS_Quote__c quote, String Status)
    {
        Redline_Manuel_Agreement__c agreement = new Redline_Manuel_Agreement__c(Message__c = 'Redline Manual Agreement test', Opportunity__c = o.Id, Related_Quote__c = quote.Id, CurrencyIsoCode = 'GBP', Status__c= status);
        insert agreement;
        return agreement;
        
    }


    
    public static void AddAttachment(Id ParentId)
    {
        String encodedContentsString = 'fileContents';
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf(encodedContentsString);
        attachment.Name = String.valueOf('test.txt');
        attachment.ContentType = 'txt';
        attachment.ParentId = ParentId;
        insert attachment;
	}
}