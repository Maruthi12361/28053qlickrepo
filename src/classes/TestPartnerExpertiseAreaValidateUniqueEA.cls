/*******************************************************************
* Class TestPartnerExpertiseAreaValidateUniqueEA
* Test class for PartnerExpertiseAreaValidateUniqueEA Trigger
* 
* Log History:
* 20130221	RDZ		CR# 6729	https://eu1.salesforce.com/a0CD000000U7AFt
*								To validate PEA are created with unique EA per ACC.
*
********************************************************************/
@isTest
private class TestPartnerExpertiseAreaValidateUniqueEA {
	
	static testMethod void TestOnInsert(){
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
	    Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
								
		Contact TestDEContact = new Contact(
			FirstName = 'Mr Partner',
			LastName = 'PP',
			AccountId = TestPartnerAccount.Id
		);
		insert TestDEContact;
		
		Partner_Expertise_Area__c pea;
		Partner_Expertise_Area__c pea02;
		
		List<Partner_Expertise_Area__c> pealist  = new List<Partner_Expertise_Area__c>();
		
		List<String> EATypes = new String[]{'Manufacturing & High Tech', 'Retail & Services','Energy & Utilities','Communications','Life Sciences','Healthcare','Financial Services','QlikView Mobile'};
		
		for (integer i=0; i<EATypes.size();i++)
		{
			pea = new Partner_Expertise_Area__c(Expertise_Area__c = EATypes[i],
												Dedicated_Expertise_Contact__c = TestDeContact.Id,
												Partner_Account_Name__c = TestPartnerAccount.Id);	
			pealist.add(pea);
		
		}
		
		
		Test.startTest();	
		
		insert pealist;
		
		try{
			insert pea;	
		}
		catch(Exception ex)
		{
			//we expect this to happen
			System.assert(true);
		}
		Test.stopTest();
	}

	static testMethod void TestOnUpdate(){
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
	    Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
								
		Contact TestDEContact = new Contact(
			FirstName = 'Mr Partner',
			LastName = 'PP',
			AccountId = TestPartnerAccount.Id
		);
		insert TestDEContact;
		
		Partner_Expertise_Area__c pea;
		Partner_Expertise_Area__c pea02;
		
		List<Partner_Expertise_Area__c> pealist  = new List<Partner_Expertise_Area__c>();
		
		List<String> EATypes = new String[]{'Manufacturing & High Tech', 'Retail & Services','Energy & Utilities','Communications','Life Sciences','Healthcare','Financial Services','QlikView Mobile'};
		
		for (integer i=0; i<EATypes.size();i++)
		{
			pea = new Partner_Expertise_Area__c(Expertise_Area__c = EATypes[i],
												Dedicated_Expertise_Contact__c = TestDeContact.Id,
												Partner_Account_Name__c = TestPartnerAccount.Id);	
			pealist.add(pea);
		
		}
		
		
		Test.startTest();	
		
		insert pealist;
		
		try{
			insert pea;	
		}
		catch(Exception ex)
		{
			//we expect this to happen
			System.assert(true);
		}
	
		pealist = [Select Id, Expertise_Area__c from Partner_Expertise_Area__c where Partner_Account_Name__c = :TestPartnerAccount.Id   limit 2];
		
		if (pealist.size()!=2)
		{
			System.assert(false); //We expect 2 PEA 
		}
		else{
			//Do update to swap expertise area from communications to another existing one
		
			pea = pealist[0];
			pea02 = pealist[1];
			pea.Expertise_Area__c = pea02.Expertise_Area__c; 
			
			try{
				update pea;
			}
			catch(Exception ex)
			{
				//We expect this to happen
				System.assert(true);
			}
		}
		Test.stopTest();
	}
}