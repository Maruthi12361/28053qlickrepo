/******************************************************

    OpportunityUpdateOppOwnerIntIdHandler
    
    Populates the INT_NetSuite_OppOwner_InternalID field on Opportunity. 
    
    This is done using a trigger instead of a formula field or Workflow 
    since to manu references are allready created on the Opportunity.     
    
    Changelog:
        2016-05-19  Roman@4front     Migrated from Opportunity_UpdateOppOwnerIntId.trigger.
		2017-06-22 MTM  QCW-2711 remove sharing option
******************************************************/

public  class OpportunityUpdateOppOwnerIntIdHandler {
	public OpportunityUpdateOppOwnerIntIdHandler() {
		
	}

	public static void handleInsert(List<Opportunity> triggerNew) {
		System.debug('Started: OpportunityUpdateOppOwnerIntIdHandler.handleInsert() method');
		Set<ID> OwnerIDs = new Set<ID>();

		for (Opportunity Opp : triggerNew)
		{
			//if (Trigger.isInsert) {
			OwnerIDs.add(Opp.OwnerId);
			//} 
			//else if (triggerOldMap.get(Opp.Id).OwnerId != Opp.OwnerId 
			//	|| triggerOldMap.get(Opp.Id).RecordTypeId != Opp.RecordTypeId 
			//	|| (Opp.OwnerId != null && Opp.INT_NetSuite_OppOwner_InternalD__c == null))
			//{
			//	OwnerIDs.add(Opp.OwnerId);
			//}		
		}
		
		if (OwnerIDs.size() == 0) return;

		handleUserMap(OwnerIDs, triggerNew);
		System.debug('Finished: OpportunityUpdateOppOwnerIntIdHandler.handleInsert() method');
	}

	public static void handleUpdate(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap) {
		System.debug('Started: OpportunityUpdateOppOwnerIntIdHandler.handleUpdate() method');
		Set<ID> OwnerIDs = new Set<ID>();

		for (Opportunity Opp : triggerNew)
		{
			//if (Trigger.isInsert) {
			//	OwnerIDs.add(Opp.OwnerId);
			//} 
			if (triggerOldMap.get(Opp.Id).OwnerId != Opp.OwnerId 
				|| triggerOldMap.get(Opp.Id).RecordTypeId != Opp.RecordTypeId 
				|| (Opp.OwnerId != null && Opp.INT_NetSuite_OppOwner_InternalD__c == null))
			{
				OwnerIDs.add(Opp.OwnerId);
			}
		}
		
		if (OwnerIDs.size() == 0) return;
		
		handleUserMap(OwnerIDs, triggerNew);
		System.debug('Finished: OpportunityUpdateOppOwnerIntIdHandler.handleUpdate() method');
	}

	private static void handleUserMap(Set<Id> ownerIDs, List<Opportunity> triggerNew) {
		System.debug('Started: OpportunityUpdateOppOwnerIntIdHandler.handleUserMap() method');
		Map<Id, User> UserMap = new Map<Id, User>([select Id, INT_NetSuite_InternalId__c from User where Id in :ownerIDs]);

		for (Opportunity Opp : triggerNew)
		{
			if (UserMap.containsKey(Opp.OwnerId))
			{
				Opp.INT_NetSuite_OppOwner_InternalD__c = UserMap.get(Opp.OwnerId).INT_NetSuite_InternalId__c;
			}
		}
		System.debug('Finished: OpportunityUpdateOppOwnerIntIdHandler.handleUserMap() method');
	}
}