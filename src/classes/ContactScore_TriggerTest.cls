/**************************************************
* CR# 20060 – Add Predictive Lead Score Grade and Overall Priority
* Change Log:
* 2015-03-12 Madhav Kakani: Test case for ContactScore_Trigger
* 2015-06-11 CCE Added test for clearing of follow-up flags (testContactScoreTrigger_ClearFU_RequiredFlags)
* 2015-07-13 CCE/Madhav Kakani CR# 30746 - Create New Predictive Lead Score Field and Revise Grade changes
* 2016-04-21 CCE CR# 82611 - Replacing Lead Status value "Follow-Up Accepted" with new value "Follow-Up Attempt 1"
* 2016-10-20 CCE CR# 95899 - Removing testmethod testContactScoreTrigger as no longer required as code it was testing has been removed from the trigger
* 2018-03-29 CCE CHG0033445 - Add test for "Set Conversica options field to Stop (case sensitive)"
* 2018-10-12 CCE CHG0034825 BMW-1054 Update test due to code changes for this CHG
* 2018-10-12 CCE CHG0034697 BMW-1015 Update test due to code changes for this CHG
* 2020-03-11 CCE FUR Revamp 2020 All scoring has been removed and now only used for Contact disposition.
* 2020-04-01 CCE CHG0038108 DGM-172 (FUR Revamp 2020)  Due to SalesLoft implementation remove "Follow-Up Disqualified" automation
**************************************************/
@isTest
private class ContactScore_TriggerTest {

    static testmethod void testContactScoreTrigger_ClearFU_RequiredFlags(){
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) { 
            Account acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
            system.assert(acct.Id != null);
            Contact ct = QTTestUtils.createMockContact(acct.Id);
            system.assert(ct.Id != null);
            //Set Contact to required start state
            ct.Contact_Status__c = 'Follow-Up Attempt 5';
            ct.New_Responder_Follow_Up_Required__c = true;
            ct.Trial_Follow_Up_Required__c = true;
            ct.Overall_Follow_up_Required__c = true;
            ct.AVA__AVAAI_action_required__c = true;
            ct.Number_of_Follow_Up_Voicemails__c = 5;
            ct.Number_of_Follow_Up_Emails__c = 1;
            update ct;

            Test.startTest();
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            //ct.Number_of_Follow_Up_Voicemails__c = 6;
            ct.Contact_Status__c = 'Contacted-Additional Work Required';
            ct.Follow_Up_Disqualified_Reason__c = 'No Response';
            update ct;
            
            Contact ctTmp = [SELECT New_Responder_Follow_Up_Required__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c, AVA__AVAAI_options__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.New_Responder_Follow_Up_Required__c == false);
            system.assert(ctTmp.Trial_Follow_Up_Required__c == false);
            system.assert(ctTmp.Overall_Follow_up_Required__c == false);
            system.assert(ctTmp.AVA__AVAAI_options__c == 'Stop');
            
            Test.stopTest();
        }
    }

}