//*********************************************************/
// Author: Mark Cane&
// Creation date: 19/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efOpportunityManager{	
    private efRegistrationManager regMan; 
    private String regOpportunityId;   
    private String eduOpportunityId;
    
    // Intialise methods.
    
    public efOpportunityManager(efRegistrationManager regMan){
		this.regMan = regMan;
    }
    // end intialise methods.
    
    
    // Opportunity methods.
    
    public void cancelOldOpportunityAndLineItems(){
        List<Opportunity> OpptyList = new List<Opportunity>();
        List<OpportunityLineItem> OpptyProductList = new List<OpportunityLineItem>();
        Registration__c Reg = [Select r.Id, 
                                  (Select Id,
                                   Status__c 
                                   From R00N70000001wy3TEAQ__r
                                   Where Status__c!=:efConstants.REG_STATUS_CANCELLED),
                                   	(Select Id,
                                    	StageName 
                                    	From Opportunities__r
                                    	Where StageName!=:efConstants.OPP_STATUS_CANCELLED) 
                                    From Registration__c r where r.Id =:regMan.getRegistrationWrapper().getRegistration().Id];
        if(Reg.R00N70000001wy3TEAQ__r.size()>0){
            for(integer i=0 ; i<Reg.R00N70000001wy3TEAQ__r.size(); i++){
                Reg.R00N70000001wy3TEAQ__r[i].Status__c = efConstants.REG_STATUS_CANCELLED;
                OpptyProductList.add(Reg.R00N70000001wy3TEAQ__r[i]);                    
            }
        }        
        if(Reg.Opportunities__r.size() > 0){
            for(integer i=0 ; i<Reg.Opportunities__r.size(); i++ ){
                Reg.Opportunities__r[i].StageName = efConstants.OPP_STATUS_CANCELLED;
                OpptyList.add(Reg.Opportunities__r[i]);                    
            }
        }        
        if(OpptyProductList.size() > 0){ update OpptyProductList; }        
        if(OpptyList.size() > 0){ update OpptyList; }
    }
    
    public Id createRegistrationOpportunity(){
        if(!efUtility.isNull(regOpportunityId)){ return regOpportunityId; }

        Opportunity regOpp = new Opportunity();
        regOpp.PriceBook2Id = regMan.getEventWrapper().getPriceBookId();
        regOpp.accountId = regMan.getContactWrapper().getContact().accountId;
        regOpp.RecordTypeId = regMan.getOppRecordType();
        regOpp.ownerId = regMan.getOppOwner();
        regOpp.StageName = efConstants.OPP_STATUS_PENDING;
        regOpp.CloseDate = System.today();
        regOpp.CurrencyIsoCode = 'USD';
        
        regOpp.Name = regMan.getEventWrapper().getEvent().Name + 
        					regMan.getRegistrationWrapper().getRegType()+' - '+
        					regMan.getContactWrapper().getContact().FirstName+' '+ 
        					regMan.getContactWrapper().getContact().LastName;
        regOpp.Registration__c = regMan.getRegistrationWrapper().getRegistration().Id;
        
        Database.SaveResult resultOpportunity = Database.Insert(regOpp);
        createOpportunityContactRole(resultOpportunity.getId());
        
        regOpportunityId = resultOpportunity.getId();
        return resultOpportunity.getId();
    }

    public void createRegOLI(efProductWrapper[] regProduct, List<efProductWrapper> promoCodeList, Boolean isPending){
        Set<String> products = new Set<String>(); 
        Map<String,String> promoPrice = new Map<String,String>();
        
        if(promoCodeList!=null && promoCodeList.size()>0){
            for(efProductWrapper p:promoCodeList){   
                if (p.getProduct().Promo_Code_Type__c.equals(efConstants.PROMO_TYPE_REGISTRATION)){
                    products.add(p.getProduct().Id);
                    promoPrice.put(p.getProduct().Id, p.getCalculatedDiscountAmount());
                }   
            }               
        }
        
        if (regProduct!=null){   
            for(efProductWrapper p:regProduct){
                products.add(p.getProduct().Id);
            }
        }    

        if (products!=null){
            PricebookEntry[] peRecords = [Select Id,
                                                 IsActive,
                                                 Name,
                                                 Pricebook2Id,
                                                 Product2Id,
                                                 Product2.family,
                                                 UnitPrice
                                          From PricebookEntry
                                          Where IsActive=true And
                                          		Pricebook2Id=:regMan.getEventWrapper().getPriceBookId() And
                                          		Product2Id IN :products];
            if(peRecords!=null && peRecords.size()>0){
                String oppId = createRegistrationOpportunity();
                Opportunity o = new Opportunity(Id=oppId);
                if (!isPending) o.StageName = efConstants.OPP_STATUS_CLOSED_PAID;
                update o;
                
                if(!efUtility.isNull(oppId)){
                    List<OpportunityLineItem> oliRecords = new List<OpportunityLineItem>();
                    
                    for(Integer i=0;i<peRecords.size();i++){
                        PricebookEntry pe = peRecords[i]; 
                        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = oppId,
                                                                          pricebookentryId=pe.Id,
                                                                          Quantity=1,
                                                                          Registration__c=regMan.getRegistrationWrapper().getRegistration().Id);
                        String family = pe.Product2.family;
                        
                        if(family.equals(efConstants.PROD_FAMILY_PROMO))
                            oli.unitprice=double.valueOf(promoPrice.get(pe.Product2Id));
                        else
                            oli.unitprice=pe.unitprice;
                            
                        oliRecords.add(oli);         
                    }
                    
                    Database.SaveResult[] resultOli = Database.Insert(oliRecords);
                    if(!resultOli[0].isSuccess()){
                        efUtility.updateDebugLog('Error in creation of Opportunity Line Item ::  '+resultOli[0].getErrors(),'E1014','Error');
                    }
                }   
            }
        }
    }
    	   
    private void createOpportunityContactRole(String oppId){
        if (!efUtility.isNull(regMan.getContactWrapper().getContact().Id) &&
            !efUtility.isNull(oppId)){
            OpportunityContactRole ocr = new OpportunityContactRole(ContactId=regMan.getContactWrapper().getContact().Id,
                                                                    OpportunityId=oppId,
                                                                    IsPrimary=true,
                                                                    Role=efConstants.OPP_CONTACT_ROLE_REGISTRANT);
           // insert ocr;
        }
    }

    public Id createEducationOpportunity(){
        if(!efUtility.isNull(eduOpportunityId)){
            return eduOpportunityId;
        }
        
        Opportunity eduOpp=new Opportunity();
        eduOpp.PriceBook2Id=regMan.getEventWrapper().getPriceBookId();
        eduOpp.accountId=regMan.getContactWrapper().getContact().accountId;
        eduOpp.RecordTypeId = regMan.getOppRecordType();
        eduOpp.ownerId = regMan.getOppOwner();
        eduOpp.StageName = efConstants.OPP_STATUS_PENDING;
        eduOpp.CloseDate=System.today();
        eduOpp.CurrencyIsoCode = 'USD';
        eduOpp.Name = regMan.getEventWrapper().getEvent().Name + ' Education ' +
        					regMan.getRegistrationWrapper().getRegType()+' - '+
        					regMan.getContactWrapper().getContact().FirstName+' '+ 
        					regMan.getContactWrapper().getContact().LastName;
        eduOpp.Registration__c = regMan.getRegistrationWrapper().getRegistration().Id;
        
        Database.SaveResult resultOpportunity = Database.Insert(eduOpp);

        if(!resultOpportunity.isSuccess()){
            efUtility.updateDebugLog('Error in creation of Education Oppty :: '+resultOpportunity.getErrors(),'E1016','Error');
        }

        createOpportunityContactRole(resultOpportunity.getId());
        
        eduOpportunityId = resultOpportunity.getId();
        return resultOpportunity.getId();
    }

    public void createEduOLI(List<efProductWrapper> selectedTraining, efPromoCodeUtil pcodeUtil, Boolean isPending){        
       	// CR& : all OLI on a single Opp.
        //String oppId = createEducationOpportunity();
        String oppId = createRegistrationOpportunity();
        
        List<OpportunityLineItem> oliRecords = new List<OpportunityLineItem>();
        List<Product2> classUsageRecords = new List<Product2>();
        
        try{
            if(selectedTraining.size()>0){
                if(!efUtility.isNull(oppId)){
                    for(efProductWrapper p:selectedTraining){
                        if(p.getIsSelected() && !p.getIsClassFull()){
                            PricebookEntry pe = p.getProduct().PriceBookEntries; 

                            OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = oppId,
                                                                              pricebookentryId=pe.Id,
                                                                              Quantity=1,
                                                                              Registration__c=regMan.getRegistrationWrapper().getRegistration().Id);
                            oli.unitprice = pe.unitprice;
                            oliRecords.add(oli);
                            
                            Double classUsage = 0;
                            
                            if(p.getProduct().Used__c!=null)
                                classUsage=p.getProduct().Used__c;
                            
                            classUsage++;
                            Product2 productUsage  = new Product2(Id=p.getProduct().Id, Used__c=classUsage);
                            classUsageRecords.add(productUsage);
                        }
                    }
                }
            }
            
            oliRecords = pcodeUtil.addEducationPromoToOLI(oliRecords,oppId);
            
            if(oliRecords!=null && oliRecords.size()>0){
                Opportunity o = new Opportunity(Id=oppId);
                if (!isPending) o.StageName = efConstants.OPP_STATUS_CLOSED_PAID;
                update o;
                
                Database.SaveResult[] resultOli = Database.Insert(oliRecords);
                
                if(classUsageRecords!=null && classUsageRecords.size()>0)
                    update classUsageRecords; 
                } else{
                Opportunity o = new Opportunity(Id=oppId);
                o.StageName = efConstants.OPP_STATUS_CANCELLED;
                update o;
            }
        } catch(Exception e){
            efUtility.updateDebugLog('Error in creation of Education OLI :: '+e.getMessage(),'E1017','Error');    
        }
    }
    
    // End Opportunity methods.
}