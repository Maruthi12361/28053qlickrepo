/*******************************************************************
* Class ChangeControlChangeManagerUpdateTest
*
* Test class for trigger ChangeControlChangeManagerUpdate
* Author: Saúl Santos

* Change Log:
* 2015-10-30 Created. 
*               
* 2016-01-25    TJG fix the error of FIELD_CUSTOM_VALIDATION_EXCEPTION, Account can be owned only by Master Reseller Partners.                         
**********************************************************************/
@isTest
private class ChangeControlCaseSearchControllerTest {

    static testmethod void changeControlCaseSearchControllerTest()
    {
        User u = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');
        Account Partner = QTTestUtils.createMockAccount('QGate Software Ltd', u, true);
        Account Customer = QTTestUtils.createMockAccount('Dako Denmark A/S', u, true);
        Case TheCase = new Case(Subject = 'Test Account', AccountId = Partner.Id);
        SLX__Change_Control__c ChangeControl = new SLX__Change_Control__c (        
            //Business_Owner__c = '00520000001Ce2K',  //use Josefine Elmqvist as
            Business_Owner__c = '005D0000001qrhb',    //Simon McAllister is on holiday
            Business_Area__c = 'Sales'
        );
        ChangeControl.Name = 'Test CR MHG 100921';              
        insert ChangeControl;
        insert TheCase;
        
        Apexpages.currentPage().getParameters().put('CRNumber', '');
        Apexpages.currentPage().getParameters().put('Name', 'Test CR MHG 100921');
         
        
        System.currentPageReference().getParameters().put('CaseId', TheCase.Id);
        
        test.startTest();
        
        ChangeControlCaseSearchController ThisTest = new ChangeControlCaseSearchController();   
        ThisTest.ThisCase = TheCase;
        ThisTest.runSearch();
        ThisTest.Change.Related_to_Change_Request_Project__c = ChangeControl.Id;
        ThisTest.relateChange();
        ThisTest.relateChange();
        ThisTest.backToParentCase();            
        ThisTest.toggleSort();
        System.assert(ThisTest.debugSoql != null); 
        System.assert(ThisTest.getCaseId() != null);
        test.stopTest();
        
    }
}