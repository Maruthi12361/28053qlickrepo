/*
	SBQQSubscriptionSharingTest
	
	Author:
		Tao Jiang, tjg@qlikview.com
		
	Changes:
        2017-04-02 TJG Fixing System.DmlException: Update failed. First exception on row 0 with id 80026000002dmhdAAA; 
                        first error: FAILED_ACTIVATION, Choose a valid contract status and save your changes. 
                        For a new or cloned contract, choose Draft. An Activated contract's status can't be edited.: [Status]
        2017-03-16 TJG Fixing test failures
		2017-01-20 TJG	Created.

	Description:
		Test class for SBQQSubscriptionSharing.
		Rodion Vakulvsokyi 14.03.2017
 */
@isTest
public class SBQQSubscriptionSharingTest {
	static final integer NBR_OF_ACCOUNTS = 2; 
    //    SELECT Id, Name FROM profile WHERE name = 'System Administrator' LIMIT 1
    static final Id AdminProfId = '00e20000000yyUzAAI';
    //    Select Id From UserRole Where PortalType = 'None' Limit 1
    static final Id PortalRoleId = '00E20000000vrJSEAY';
    //    SELECT Id, name FROM Profile WHERE Name LIKE 'PRM - Sales Dependent %y'
    static final Id ProfileId = '00e20000001OyLwAAK';
    // Partner Account Record Type ID
    static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';

    static testMethod void dummyTest()
    {
    	ApexSharingRules.TestingParnershare = true;
    	Account acc = new Account();
    	SBQQ__Subscription__c subsc = createAnSubscription(acc,null);
    	Insert subsc;
    	List<Id> subscIds = new List<Id>();
    	SBQQSubscriptionSharing.UpdateSBQQSubscriptionSharing(subscIds);
    }

   static testMethod void SellThroughPartnerTest() 
    {
        ApexSharingRules.TestingParnershare = true;
        Test.startTest();
        QTCustomSettings__c setting = new QTCustomSettings__c(
        Name = 'Default', 
        eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO',
        RecordTypesForecastOmitted__c = '01220000000DNwZAAW',
        OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
        Insert setting;
        NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
        settings.UserRecordTypeIdsToExclude__c = UserInfo.getUserId(); //'005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
        insert settings;
        QuoteTestHelper.createCustomSettings();
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User ownerUser = createSysAdminUser(AdminProfId, PortalRoleId, me);

        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');

        List<Contact> ctList = new List<Contact>();
        List<Account> partnerAccs = createPartnerAccounts(ctList, ownerUser, qtComp);

        //PRM - Sales Dependent Territory + QlikBuy
        List<User> ppUsers = createUserFromProfileIdContact(ProfileId, ctList);
        
        // sell through partner account Ids
        Set<Id>  uniqueAccIds = new Set<Id>();
        List<Contract> contracts = new List<Contract>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) 
        {
        	Contract ct = createTestContract(partnerAccs[i].Id, partnerAccs[NBR_OF_ACCOUNTS + i].Id, partnerAccs[NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + i].Id, Date.today(), null);
        	contracts.add(ct);
        }
        contracts[1].Sell_through_Partner__c = null;
        Insert contracts;

        List<SBQQ__Subscription__c> testSubs = new List<SBQQ__Subscription__c>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
            // create SBQQ__Subscription__c with partnerAccs[1] as SellThrough partner
            Id stpId = partnerAccs[NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + i].Id;
            SBQQ__Subscription__c anSubsc = createAnSubscription(
                partnerAccs[i],
                contracts[i].Id
            );
            testSubs.add(anSubsc);
            uniqueAccIds.add(stpId);
        }

        Insert testSubs;

        List<Id> subscIds = new List<Id>();
        Integer iLen = testSubs.size();
        for (Integer i = 0; i < iLen; i++)
        {
            subscIds.add(testSubs[i].Id); 
            Integer j = 0 == i ? iLen : i;
            j--;
            testSubs[i].SBQQ__Account__c = partnerAccs[j].Id;
            testSubs[i].SBQQ__Contract__c = contracts[j].Id;
        }
        Update testSubs;

        List<SBQQ__Subscription__Share> subscShares = [Select Id from SBQQ__Subscription__Share Where RowCause = 'Manual' AND ParentId in :subscIds];

        System.assertNotEquals(0, subscShares.size());

        for (Integer i = 0; i < iLen; i++)
        {
            testSubs[i].SBQQ__Account__c = partnerAccs[i].Id;
            testSubs[i].SBQQ__Contract__c = contracts[i].Id;
        }
        
        Update testSubs;

        Test.stopTest();        
    }    
    
    static testMethod void EndUserTest() 
    {
        ApexSharingRules.TestingParnershare = true;
        Test.startTest();
        QTCustomSettings__c setting = new QTCustomSettings__c(
        Name = 'Default', 
        eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO',
        RecordTypesForecastOmitted__c = '01220000000DNwZAAW',
        OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
        Insert setting;
        NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
        settings.UserRecordTypeIdsToExclude__c = UserInfo.getUserId(); //'005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
        insert settings;
        QuoteTestHelper.createCustomSettings();
        /////RecordType rt = [SELECT Id FROM RecordType WHERE Name='Qlikbuy CCS Standard II' limit 1];
        //Profile prof = [Select Id from Profile where name = 'System Administrator' limit 1];

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User ownerUser = createSysAdminUser(AdminProfId, PortalRoleId, me);

        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');

        List<Contact> ctList = new List<Contact>();
        List<Account> partnerAccs = createPartnerAccounts(ctList, ownerUser, qtComp);


        //PRM - Sales Dependent Territory + QlikBuy
        /////Profile prof = [SELECT Id, name FROM Profile WHERE Name LIKE 'PRM - Sales Dependent %y']; 
        List<User> ppUsers = createUserFromProfileIdContact(ProfileId, ctList);
        List<Contract> contracts = new List<Contract>();
       
        
        	Contract ct = createTestContract(partnerAccs[0].Id, partnerAccs[NBR_OF_ACCOUNTS + 0].Id, partnerAccs[NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + 0].Id, Date.today(), null);
        	contracts.add(ct);
        

        Insert contracts;

        List<SBQQ__Subscription__c> testSubscs = new List<SBQQ__Subscription__c>();
       // for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
            // create SBQQ__Subscription__c with partnerAccs[1] as SellThrough partner
            SBQQ__Subscription__c anSubsc = createAnSubscription(
                partnerAccs[0],
                contracts[0].Id
            );
            testSubscs.add(anSubsc);
       // }

        Insert testSubscs;

        List<Id> subscIds = new List<Id>();
        //Integer iLen = testSubscs.size();
        //for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++)
       // {
            subscIds.add(testSubscs[0].Id);
            testSubscs[0].SBQQ__Account__c = partnerAccs[0].Id;
      //  }

        List<SBQQ__Subscription__Share> subscShares = [Select Id from SBQQ__Subscription__Share Where RowCause = 'Manual' AND ParentId in :subscIds];

        //--System.assertNotEquals(0, subscShares.size());

        /*
        Set<SBQQ__Subscription__c> oSet = new Set<SBQQ__Subscription__c>();
        oSet.addAll(testSubscs);

        SBQQ__Subscription__Share oppShare = new SBQQ__Subscription__Share(
            ParentId = testSubscs[0].Id,
            AccessLevel = 'Edit',
            UserOrGroupId = ppUsers[1].Id);
        Insert oppShare;

        ApexSharingRules.UpdateSBQQ__Subscription__cSharing(oSet);
        // force to update again
        ApexSharingRules.UpdateSBQQ__Subscription__cSharing(oSet);
        */

        List<Id> accountIds = new List<Id>();
        for (Account acc : partnerAccs)
        {
            accountIds.add(acc.Id);
        }

        //List <UserRole> uRoles = [Select Id, PortalAccountId, ParentRoleId, PortalType, PortalRole from UserRole where PortalAccountId in :accountIds];


        //System.debug('uRoles.size() = ' + uRoles.size() + ', uRoles=' + uRoles);

        //System.assertNotEquals(uRoles, null);
        //System.assertNotEquals(uRoles.size(), 0); 

        System.assertNotEquals(0, subscShares.size());

        Update testSubscs;

        Test.stopTest();        
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me ) 
    {
        User u 
            = new User(
               alias = 'adminUsr', email='adminUser@tjtest.com.test',
                Emailencodingkey='UTF-8', lastname='adminTest', 
                Languagelocalekey='en_US', localesidkey='en_US',
                Profileid = profId,
                Timezonesidkey='America/Los_Angeles', 
                //ContactId = cont.Id,
                UserRoleId = roleId,
                Username= 'usr' + System.now().millisecond() + '@jttest.com.test'
            );

        System.runAs ( me ) 
        {
            insert u;
        }

        return u;
    }   

    public static List<User> createUserFromProfileIdContact(Id profId, List<Contact> cont) {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<User> usrs = new List<User>();
        System.runAs ( thisUser ) {
            //UserRole ur = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            
            for (integer ndx = 0; ndx < cont.size(); ndx++)
            {
                User u = new User(alias = 'newUser', email='puser'+ ndx + '@tjtest.com.test',
                    Emailencodingkey='UTF-8', lastname='Testing', 
                    Languagelocalekey='en_US', localesidkey='en_US',
                    Profileid = profId,
                    Timezonesidkey='America/Los_Angeles',
                    ContactId = cont[ndx].Id,
                    //UserRoleId = ur.Id,
                    Username= 'opuser' + ndx + '@tjtest.com.test');
                usrs.add(u);
            }
            insert usrs;
            for (integer i = 0; i < usrs.size(); i++)
            {
                usrs[i].IsPortalEnabled = true;                        
            }
            update usrs;
        }
        return usrs;
    } 

    private static List<Account> createAccounts(User user)
    {
        List<Account> nuAccounts = new List<Account>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
            Account acc = QTTestUtils.createMockAccount('OppSharing Test Inc [' + (i+1) + ']', user, false);
            nuAccounts.add(acc);
        }
        Insert nuAccounts;
        return nuAccounts;
    }

    public static Account createPartnerAccount(String sName, User user, QlikTech_Company__c qtComp, Boolean doInsert)
    {
        Account acc = new Account(  Name=sName, 
            OwnerId = user.Id,
            Billing_Country_Code__c = qtComp.Id,
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
            Navision_Customer_Number__c = '12345', 
            Navision_Status__c = 'Partner',
            RecordTypeId = PartnerAccRecTypeId,
            Legal_Approval_Status__c = 'Legal Approval Granted', 
            BillingStreet = '31 Hight Street',
            BillingState ='Berks',
            BillingPostalCode = 'RG2 5ST',
            BillingCountry = 'United Kingdom',
            BillingCity = 'Reading',           
            Shipping_Country_Code__c = QTComp.Id,
            ShippingStreet = '31 Hight Street',
            ShippingState ='Berks',
            ShippingPostalCode = 'RG2 5ST',
            ShippingCountry = 'United Kingdom',
            ShippingCity = 'Reading',
            From_Lead_Conversion__c = false
        );      
        if (doInsert)
        {
            insert acc;
        }
        return acc;
    }

    public static List<Account> createPartnerAccounts(List<Contact> ctList, User user, QlikTech_Company__c qtComp)
    {
        List<Account> nuAccounts = new List<Account>();
        for (Integer i = 0; i < (NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS) ; i++)
        {
            Account acc = new Account(  
                Name = 'SBQQ__Subscription__cTest' + i, 
                OwnerId = user.Id,
                Billing_Country_Code__c = qtComp.Id,
                QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                Navision_Customer_Number__c =  'N' + (10000+i), 
                Navision_Status__c = 'Partner',
                RecordTypeId = PartnerAccRecTypeId,  // Partner Account
                Legal_Approval_Status__c = 'Legal Approval Granted', 
                BillingStreet = '3' + i + ' Hight Street',
                BillingState ='Berks',
                BillingPostalCode = 'RG2 5ST',
                BillingCountry = 'United Kingdom',
                BillingCity = 'Reading',           
                Shipping_Country_Code__c = QTComp.Id,
                ShippingStreet = '3' + i + ' Hight Street',
                ShippingState ='Berks',
                ShippingPostalCode = 'RG2 5ST',
                ShippingCountry = 'United Kingdom',
                ShippingCity = 'Reading',
                From_Lead_Conversion__c = false
            );
            nuAccounts.add(acc);
        }
        Insert nuAccounts;

        for(Integer i = 0; i < (NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS); i++)
        {
            Contact contact = new Contact( 
                FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = nuAccounts[i].Id,
                pse__Is_Resource__c = true,
                pse__Is_Resource_Active__c = true,
                Email = 'subsc' + i + '@test.com');
            ctList.add(contact);  
        }
        insert ctList;
        return nuAccounts;
    }

    public static Contract createTestContract(Id accountId, Id resellerId, Id supportId, Date startDate, Id calId) {
    	Contract c = new Contract(
                Name = 'test',
                StartDate = startDate,
                Status = 'Draft', 
                AccountId = accountId,  
                ContractTerm = 4,
                Support_Level__c = 'Basic',
                Support_Percentage__c = 15,
                Sell_Through_Partner__c = resellerId,
                Support_Provided_By__c = supportId,
                Support_Provided_By_Type__c = 'Qlik',
                Sales_Channel__c= calId
                );
    	//insert c;  Callers will do this!
  		return c;
  	}

    private static SBQQ__Subscription__c createAnSubscription(Account acc, Id contractId)
    {
        SBQQ__Subscription__c subsc = New SBQQ__Subscription__c (
            SBQQ__Account__c = acc.Id,
            SBQQ__Quantity__c = 10,
            SBQQ__Contract__c = contractId
        ); 
   
        return subsc;
    }
}