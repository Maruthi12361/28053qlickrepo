/************************************************************************
 * @author  UIN
 * 2017-03-10 Partner Program Project
 *
 * Added logic to set opportunity source on lead conversion only.
 *
 * 2017-10-25 AYS BMW-402 : Migrated from oppSourceUpdonLeadConv.trigger.
 *************************************************************************/
public class LeadOppSourceUpdonLeadConvHandler {
	public LeadOppSourceUpdonLeadConvHandler() {
		
	}
	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {

		Set<Id> convOppoIds = new Set<Id>();
	    Map<Id, Id> oppToContMap = new Map<Id, Id>(); // <Opportunity.Id, Contact.Id>
	    List<Opportunity> convOppos = new List<Opportunity>();
		List<Opportunity> convOpposToUpdate = new List<Opportunity>();
	    Map<Id, Lead> oppIdLeadMap = new Map<Id, Lead>(); // <Opportunity.Id, Contact.Id>
	    List<String> lLeadRecTypes = new List<String>{'Partner_Opp_Reg', 'Partner_Referral_Opp_Reg'};

	    for(Integer i = 0; i<triggerNew.size();i++){
	        if(triggerNew[i].IsConverted == true && triggerOld[i].isConverted == false &&
	        	String.isNotBlank(triggerNew[i].ConvertedOpportunityId)){
	            convOppoIds.add(triggerNew[i].ConvertedOpportunityId);
	            oppToContMap.put(triggerNew[i].ConvertedOpportunityId, triggerNew[i].ConvertedContactId);
	            oppIdLeadMap.put(triggerNew[i].ConvertedOpportunityId, triggerNew[i]);
	        }
	    }
	    if(!convOppoIds.isEmpty())
	        convOppos = [SELECT Id, RecordTypeId, Academic_Contact_Name__c,Opportunity_Source__c
	            FROM Opportunity WHERE Id IN :convOppoIds];

	    if(!convOppos.isEmpty()){
	        Map<String, Id> mRecTypeIdName = new Map<String, Id>();
	        for(Recordtype rec: [select id, name,developername from RecordType where developername in :lLeadRecTypes]){
	            if(!mRecTypeIdName.containsKey(rec.developername))
	                mRecTypeIdName.put(rec.developername, rec.Id);
	        }

	        for(Opportunity opp:convOppos){
	        	if(oppIdLeadMap.containsKey(opp.Id)){
	        		if (oppIdLeadMap.get(opp.Id).RecordTypeId == mRecTypeIdName.get(lLeadRecTypes[0]) && !opp.Opportunity_Source__c.equals('Partner Registration')) {
	        			opp.Opportunity_Source__c = 'Partner Registration';
	    				convOpposToUpdate.add(opp);
	    			} else if(oppIdLeadMap.get(opp.Id).RecordTypeId == mRecTypeIdName.get(lLeadRecTypes[1]) && !opp.Opportunity_Source__c.equals('Partner Referral')) {
	        			opp.Opportunity_Source__c = 'Partner Referral';
	    				convOpposToUpdate.add(opp);
	    			} else if (!opp.Opportunity_Source__c.equals('Qlik Lead') && oppIdLeadMap.get(opp.Id).RecordTypeId != mRecTypeIdName.get(lLeadRecTypes[0])
	                                && oppIdLeadMap.get(opp.Id).RecordTypeId != mRecTypeIdName.get(lLeadRecTypes[1])) {
	    				opp.Opportunity_Source__c = 'Qlik Lead';
	    				convOpposToUpdate.add(opp);
	    			}
	        	}
	        }

	        try {
	            if (convOpposToUpdate.size() > 0) {
	                update convOpposToUpdate;
	    		}
	        } catch (Exception e) {
	            System.debug(e);
	        }
	    }
	}
}