/**************************************************
* Author: Madhav Kakani Fluido Denmark
* Test class for trigger CaseFeedTopicAssignment_Trigger
* Change Log:
* 2015-07-07 Madhav Kakani: CR# 20197 - add topic to the related parent
*        Initial Development
* 2015-10-26 AIN Using org data so we don't insert anything and cause triggers to fire.
             This trigger doesn't actually test the trigger as the trigger is only active on insert. We instead use if for code coverage
             * 2018-01-30 ext_vos CHG0032996: add test method for Portal user. Updates for runTest(): add delete logic.
**************************************************/

@IsTest(seeAllData=true)
private class CaseFeedTopicAssignment_TriggerTest {
    static testmethod void runTest() {
        Test.startTest();

        RecordType caseRt = [select Id from RecordType where Name = 'QlikTech Master Support Record Type'];
        Case c = new Case();
        c.First_Name__c = 'TEST_runTest';
        c.RecordTypeId = caseRt.Id;
        insert c;

        // Create a feed item and attach it to the Case
        FeedItem feedItem = new FeedItem(ParentId = c.Id, Body = '#TEST_runTest Feed Item');
        insert feedItem;
        System.assert(feedItem.Id != null);

        // Check that a topic is created and is also associated with the related Case
        ConnectApi.TopicPage tpage = ConnectApi.Topics.getTopics(null, c.Id);
        ConnectApi.Topic topic = tpage.topics[0];
        System.assertEquals(1, tpage.topics.size());
        System.assertEquals('TEST_runTest', topic.Name);

        TopicAssignment ta = [select Id from TopicAssignment where EntityId =: feedItem.Id and TopicId =: topic.Id];
        delete ta;
        tpage = ConnectApi.Topics.getTopics(null, c.Id);
        System.assertEquals(0, tpage.topics.size());

        Test.stopTest();
    }

    static testmethod void runTestAsPortalUser() {
        RecordType caseRt = [select Id from RecordType where Name = 'QlikTech Master Support Record Type'];

        List<User> communityUserList = [select Id, ContactId, Contact.AccountId, ProfileId,
                                            (select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1)
                                        from User
                                        where UserType != 'Standard' And UserType != 'CsnOnly'
                                            AND ProfileId != null AND IsPortalEnabled = true
                                            AND IsActive = true AND ContactId != null
                                            AND AccountId != null AND Id IN (select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];

        System.assert(communityUserList.size() > 0, 'No community users found.');
        User communityUser = communityUserList[0];
        Test.startTest();

        System.runAs(communityUser) {
            Case c = new Case();
            c.First_Name__c = 'TEST_runTestAsPortalUser';
            c.AccountId = communityUser.Contact.AccountId;
            c.ContactId = communityUser.ContactId;
            c.RecordTypeId = caseRt.Id;
            insert c;

            FeedItem feedItem = new FeedItem(ParentId = c.Id, Body = '#Test Feed Item');
            insert feedItem;
        }
        Case testCase = [select Id from Case where First_Name__c = 'TEST_runTestAsPortalUser'];
        ConnectApi.TopicPage tpage = ConnectApi.Topics.getTopics(null, testCase.Id);
        System.assertEquals(0, tpage.topics.size());

        Test.stopTest();
    }
}