/****************************************************************
*
*  CalculateQuoteLineLimits_Test 
*
*  11.25.2017  UIN :   Initial DEvelopment
*  02.05.2017  Reshma Ravi : Code adjustment for test class failure
*  29.12.2017  Pramod Kumar V : Test class coverage 
*****************************************************************/
@isTest
private class CalculateQuoteLineLimits_Test {
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    @isTest static void test_method_one() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];     
        //Added by Reshma
        /*
        Q2CWSettings__c hSettings1 = new Q2CWSettings__c();
        hSettings1.Max_Units_Consumed_Limit__c = 7000;
        hSettings1.Max_Units_Hit_Error_Messag__c = 'aaaaa';
        insert hSettings1;
        */
        System.runAs(testUser) {
        /*
            Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
        */
        Account  testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [Select id From Subsidiary__c];
         QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        /*
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount;
        */
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType 
                               where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        Product2 productForTest2 = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server 2', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest2;

        PricebookEntry pbEntryTest2 = QuoteTestHelper.createPriceBookEntry(productForTest2.id, '01s20000000E0PWAA0');
            insert pbEntryTest2;
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        Test.startTest();
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           //quoteForTest.SBQQ__Primary__c = true;
           insert quoteForTest;
       List<SBQQ__QuoteLine__c> lquoteLines = new List<SBQQ__QuoteLine__c>();
        SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        SBQQ__QuoteLine__c testQuoteLine3 = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest2.id, 'smth2', 6, 'licenseCode2', 'Text2', 'family2', 'USD', 6, 1, 'LiceId2', 'lefDetail2');
        lquoteLines.add(testQuoteLine);
        lquoteLines.add(testQuoteLine3);
        insert lquoteLines;
        
        //Added By Pramod
        try{   
            testQuoteLine.SBQQ__Quantity__c=5000;
            update  testQuoteLine;
        }catch(Exception e){
            String expectedExceptionThrown =  e.getMessage();
            System.debug(expectedExceptionThrown);
        }

        //Product2 productForTest2 = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Education', 1, 'QlikView', 'QlikView');
         //   insert productForTest2;
        try{   
        SBQQ__QuoteLine__c testQuoteLine4 = QuoteTestHelper.createQuoteLine(quoteForTest.id,productForTest.id, 'smth', 6000, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        insert testQuoteLine4;
        }catch(Exception e){
            String expectedExceptionThrown =  e.getMessage();
            System.debug(expectedExceptionThrown);
        }
        Test.stopTest();

       }
    }

    @isTest static void test_method_three() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        Q2CWSettings__c hSettings1 = new Q2CWSettings__c();
        hSettings1.Max_Units_Consumed_Limit__c = 70001;
        hSettings1.Max_Units_Hit_Error_Messag__c = 'aaaaa1';
        //insert hSettings1;
        System.runAs(testUser) {

        Account  testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [Select id From Subsidiary__c];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];

        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType 
                               where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        Test.startTest();
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           //quoteForTest.SBQQ__Primary__c = true;
           insert quoteForTest;

                try{   
            SBQQ__QuoteLine__c testQuoteLine4 = QuoteTestHelper.createQuoteLine(quoteForTest.id,productForTest.id, 'smth', 6000, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
            insert testQuoteLine4;
            }catch(Exception e){
                String expectedExceptionThrown =  e.getMessage();
                System.debug(expectedExceptionThrown);
            }
            Test.stopTest();

       }
    }
    
    @isTest static void test_method_two() {
        // Implement test code
        CalculateQuoteLineLimits calc= new CalculateQuoteLineLimits();

        SBQQQuoteLineTriggerHandler sbbqquote = new SBQQQuoteLineTriggerHandler(true,1);
        SBQQQuoteLineTriggerHandler.OnAfterInsertAsync(new Set<id>{});
        Boolean a1 = sbbqquote.IsTriggerContext;
        Boolean a2 = sbbqquote.IsVisualforcePageContext;
        Boolean a3 = sbbqquote.IsWebServiceContext;
        Boolean a4 = sbbqquote.IsExecuteAnonymousContext;
        sbbqquote.OnUndelete(new List<SBQQ__QuoteLine__c>{});
    }

    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr, 
                                            Country_Name__c = countryName, 
                                            Subsidiary__c = subsId);
        return qlikTechIns;

    }
    
}