/*
* 2018-03-15    AYS     Class Created
* 2019-07-04    CCE     Added sg and usg to improver coverage
*/
@isTest
private class LeadUpdateULCDetailsOnConvertHandlerTest {
	
	@isTest static void test_method_1() {

		Test.startTest();

		Lead lead = new Lead( LastName='Test2', 
            FirstName='FollowUpRequiredType', 
            Country='UK',
            Email='lds@test.com',             
            LeadSource = 'PART - Partner',
            Company ='FUTesting',
            Overall_Follow_up_Required__c = false,
            Status = 'Follow-Up Disqualified',
            Follow_Up_Disqualified_Reason__c = 'No Response');
		insert lead;

        ULC_Details__c ulc = new ULC_Details__c(LeadId__c = lead.id, ULCName__c = 'somename');
        insert ulc;

        Sense_Group__c sg = new Sense_Group__c();
        insert sg;

        User_To_Sense_Group__c usg = new User_To_Sense_Group__c(Member_Lead__c = lead.id, ULC_Details_Id__c = ulc.id, Sense_Group_Id__c = sg.id);
        insert usg;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setleadId(lead.Id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Lead - Converted');  //CCE CR# 6546
        //lc.setConvertedStatus('Qualified - B');
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadTriggerHandlerAfterUpdate = false;
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        Test.stopTest();
        System.assert(lcr.isSuccess());

        ulc = [SELECT id, ContactId__c FROM ULC_Details__c WHERE Id =: ulc.id];
        System.assert(ulc.ContactId__c != null);
	}

}