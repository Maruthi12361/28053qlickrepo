/******************************************************

  Class: ApexSharingRules
  
  Changelog:
    2009-08-25  MHG   Created file
    2010-08-18  MHG   Added sharing of Cases: 
              AddCaseSharingOnAccountOrigin
    2010-09-01  MHG   Ported AccountSharingRulesOnPartnerPortalAccounts
              functionallity to Web Service   
    2013-12-05  TJG   CR# 10211
              https://eu1.salesforce.com/a0CD000000detnE
    2014-11-06  TJG   Recreated CR# 7097
              https://eu1.salesforce.com/a0CD000000U8WRR
              Partner Lead sharing after submssion
    2016-01-11    IRN     CR42998 - partnershare project - added getAccessTypeValue, newAccountSharing, updateAccountSharing, getPartnerRoles, GetListOfUserRolesByPortalAccountIds
                            getListOfGroupsByRelatedId,  getRange. 
                            removed AccountSharingRulesOnPartnerPortalAccounts
    2016-03-04  CCE     CR# 78524 https://eu1.salesforce.com/a0CD000000xSfTV Create Partner Sharing on Marketing Campaigns

    2017-01-25  TJG add entries for Asset and SBQQSubscription
    2019-02-20  BAD - Qlik Commerce - removing sharing
                  
******************************************************/
public class ApexSharingRules {
  public static Boolean TestingParnershare = false;
  public static Boolean TestingAccountShare = false;
  public static Boolean TestingContactShare = false;
  public static Boolean TestingOpportunityShare = false;
  public static Boolean TestingCampaignShare = false;
         
  /**********************************************************
  
    AddPRMAccountSharingRulesOnResponsiblePartner
    
    -> newAccounts - List of Accounts
    
    Method iterates true the list of accounts and create
    sharing rules for all the Partner groups associated
    with the account responsible partner field.
  
  **********************************************************/ 
  /*
  @future (callout=true)
  public static void AddPRMOpportunitySharingRulesOnSellThroughPartner(string[] newOpportunities) 
  {
    sharinghandlerQlikviewCom.ArrayOfString AoS = new sharinghandlerQlikviewCom.ArrayOfString();
    AoS.string_x = newOpportunities;
    sharinghandlerQlikviewCom.ServiceSoap service = new sharinghandlerQlikviewCom.ServiceSoap();  
    // CR# 10211 Increase timeout limit to 60 seconds stored in custom settings
    service.timeout_x = 60000;     
    if(!Test.isRunningTest())service.OpportunitySellThroughPartner(AoS);
    }   
    */   
   /**********************************************************
  
    AddCaseSharingOnAccountOrigin
    
    -> ModifiedCases - List of Cases
    
    Method iterates true the list of cases and create
    sharing rules for all the customer groups associated
    with the AccountOrigin field.
  
  **********************************************************/ 
  /*
  @future (callout=true)
  public static void AddCaseSharingOnAccountOrigin(string[] CaseIDs) 
  {   
    sharinghandlerQlikviewCom.ArrayOfString Cases = new sharinghandlerQlikviewCom.ArrayOfString();    
    Cases.string_x = CaseIDs;
    sharinghandlerQlikviewCom.ServiceSoap service = new sharinghandlerQlikviewCom.ServiceSoap();  
    // CR# 10211 Increase timeout limit to 60 seconds stored in custom settings
    service.timeout_x = 60000;     
    service.CaseSharingOnAccountOrigin(Cases);
    }
  */
  /**********************************************************

    PartnerLeadShareWithCreaterAfterSubmission

    submittedLeads

    CR 7097

  **********************************************************/

  @future (callout=true)
  public static void PartnerLeadShareWithCreaterAfterSubmission(string[] submittedLeads) 
  {
    sharinghandlerQlikviewCom.ArrayOfString AoS = new sharinghandlerQlikviewCom.ArrayOfString();
    AoS.string_x = submittedLeads;
    sharinghandlerQlikviewCom.ServiceSoap service = new sharinghandlerQlikviewCom.ServiceSoap(); 
    service.timeout_x = 60000;
    service.PartnerLeadShareWithCreaterAfterSubmission(AoS);
  }

/*
//removed when Parter Community implemented

  @future
  public static void UpdateContactSharing(List<Id> contactIds)
  {
    UpdateContactSharingForTest(contactIds);
  }

  public static void UpdateContactSharingForTest(List<Id> contactIds)
  {
    System.debug('UpdateContactSharingForTest: contactIds size = ' + contactIds.size() + ', TestingContactShare = ' + TestingContactShare);
    if ((contactIds.size() > 0) && (!Test.isRunningTest() || TestingContactShare))
    {
      ContactSharing.UpdateContactSharing(contactIds);
    }
  }
*/
  @future
  public static void UpdateSBQQSubscriptionSharing(List<Id> subsIds)
  {
    UpdateSBQQSubscriptionSharingNow(subsIds);
  }

  public static void UpdateSBQQSubscriptionSharingNow(List<Id> subsIds)
  {
    SBQQSubscriptionSharing.UpdateSBQQSubscriptionSharing(subsIds);
  }

  /*@future
  public static void UpdateAssetSharing(List<Id> assetIds)
  {
    UpdateAssetSharingNow(assetIds);
  }

  public static void UpdateAssetSharingNow(List<Id> assetIds)
  {
    AssetSharing.UpdateAssetSharing(assetIds);
  }*/


  @future
  public static void UpdateCaseSharing(List<Id> cases)
  {
    UpdateCaseSharingForTest(cases);
  }

  public static void UpdateCaseSharingForTest(List<Id> cases)
  {
    CaseSharing.UpdateCaseSharing(cases);
  }

/*
//removed when Parter Community implemented

  @future
  public static void UpdateOpportunitySharing(List<Id> opps)
  {
    UpdateOpportunitySharingForTest(opps);
  }

  public static void UpdateOpportunitySharingForTest(List<Id> opps)
  {
    OpportunitySharing.UpdateOpportunitySharing(opps);
  }
*/
  @future
  public static void UpdateAssignmentSharing(List<Id> assignments)
  {
    UpdateAssignmentSharingForTest(assignments);
  }

  public static void UpdateAssignmentSharingForTest(List<Id> assignments)
  {
    AssignmentSharing.UpdateAssignmentSharing(assignments);
  }

  private static Map<String, Integer> accessType = new Map<String, Integer>
    {
      'private' => 0,
      'read' => 1,
      'edit' => 2

    };

    public static Integer getAccessTypeValue(String s)
    {
      return accessType.get(s);
    }
  

   @future 
   public static void updateAccountSharing(List<Id> accounts){
      System.debug('---updateAccountSharing---' + accounts.size());
      AccountSharing.updateAccountSharing(accounts);
   }

   public static void updateAccountSharingForTest(List<Id> accounts){
      AccountSharing.updateAccountSharing(accounts);
   }

/*
//removed when Parter Community implemented
   @future 
   public static void updateAccountOpportunitySharing(List<Id> accounts, List<Id> opps)
  {
      System.debug('---updateAccountOpportunitySharing---' + accounts.size() + ', ' + opps.size());
      updateAccountOpportunitySharingForTest(accounts, opps);
  }

  public static void updateAccountOpportunitySharingForTest(List<Id> accounts, List<Id> opps)
  {
    if ((accounts.size() > 0) && (!Test.isRunningTest() || TestingAccountShare))
    {
        AccountSharing.updateAccountSharing(accounts);
      }
      if ((opps.size() > 0) && (!Test.isRunningTest() || TestingOpportunityShare))
      {
        OpportunitySharing.UpdateOpportunitySharing(opps);
      }
  }
*/

   @future 
   public static void updateNSSupportContractSharing(List<Id> endUsers, List<Id> contractIds){
      System.debug('---updateNSSupportContractSharing---');
      updateNSSupportContractSharingForTest(endUsers, contractIds);
   }


   public static void updateNSSupportContractSharingForTest(List<Id> endUsers, List<Id> contractIds){
      ResponsiblePartners rp  = new ResponsiblePartners();
      rp.populateResponsiblePartners(endUsers);
      List<NS_Support_Contract__c> contracts = [Select Id, Reseller__c, Responsible_Partner__c, End_User__c from NS_Support_Contract__c where Id in :contractIds];
      NSSupportContractSharing nsSharing = new NSSupportContractSharing();
      Map<Id, List<Id>> contractPartnerIds = getUniquePartnersToShareWith(contracts);
      nsSharing.shareContractsToPartner(contractPartnerIds);
      ProductLicensesSharing productlicensesSharing = new ProductLicensesSharing();
    productlicensesSharing.populateRelatedPartners(contracts);
   }

   @future
   public static void updateAccountLicensesDueToNSSupportContractSharing(List<id> contractIds){
      System.debug('---updateAccountLicensesDueToNSSupportContractSharing---');
      ProductLicensesSharing ps = new ProductLicensesSharing();
      //remove support provided by and sell through partner on account licenses
      ps.removeRelatedPartnersWhenNSSupportContractIsRemoves(contractIds);
   }

   public static void updateAccountLicensesDueToNSSupportContractSharingForTest(List<id> contractIds){
      System.debug('---updateAccountLicensesDueToNSSupportContractSharing---');
      ProductLicensesSharing ps = new ProductLicensesSharing();
      ps.removeRelatedPartnersWhenNSSupportContractIsRemoves(contractIds);
   }

    public static Map<Id, List<Id>> getUniquePartnersToShareWith(List<NS_Support_Contract__C> contracts){
    Map<Id, List<Id>> uniqueId = new Map<Id, List<Id>>();
    for(Integer i=0; i<contracts.size(); i++){
      Set<Id> temp = new Set<Id>();
      if(contracts[i].End_User__c != null && !temp.contains(contracts[i].End_User__c)){
        temp.add(contracts[i].End_User__c);
      }
      if(contracts[i].Reseller__c!= null && !temp.contains(contracts[i].Reseller__c)){
        temp.add(contracts[i].Reseller__c);
      }
      if(contracts[i].Responsible_Partner__c!= null && !temp.contains(contracts[i].Responsible_Partner__c)){
        temp.add(contracts[i].Responsible_Partner__c);
      }
      List<Id> l = new List<Id>();
      l.addAll(temp);
      uniqueId.put(contracts[i].Id, l);
    }
    return uniqueId; 
  }


    public static List<UserRole> getPartnerRoles(List<Id> acc, String portalType, String portalRole){
      System.debug('beginning getPartnerRoles ' + acc + ', portalType ' + portalType + ', portalRole ' + portalRole);

      List<UserRole> partnerRoles = new List<UserRole>();
      if(acc == null || acc.size() == 0)
      {
          return partnerRoles;
      }

      if (portalRole == null || portalRole=='')
      {
          partnerRoles = [select Id, PortalAccountId, ParentRoleId from UserRole where PortalAccountId in :acc and PortalType =: portalType];
      }
      else
      {
          partnerRoles = [select Id, PortalAccountId, ParentRoleId from UserRole where PortalAccountId in :acc and PortalType =: portalType and PortalRole =: portalRole];
      }

      System.debug('end getPartnerRoles ' + partnerRoles);
      return partnerRoles;
   }

    // param examples, portalType = 'Partner', portalRole = 'Executive'
    public static List<UserRole> GetListOfUserRolesByPortalAccountIds(Set<Id>paIds, String portalType, String portalRole) {
      List<UserRole> ptnerRoles = (portalRole == null || portalRole=='') ?
      [select Id, PortalAccountId, ParentRoleId from UserRole 
                where PortalAccountId in :paIds and PortalType = :portalType] :
      [select Id, PortalAccountId, ParentRoleId from UserRole 
                where PortalAccountId in :paIds and PortalType = :portalType and PortalRole = :portalRole];
      return ptnerRoles;
    }
    
    // param examples, portalType = ['Partner'], portalRole = 'Executive'
    public static List<UserRole> GetListOfUserRolesByPortalAccountIds(Set<Id>paIds, List<String> portalTypes, String portalRole) {
      List<UserRole> ptnerRoles = (portalRole == null || portalRole=='') ?
      [select Id, PortalAccountId, ParentRoleId from UserRole 
                where PortalAccountId in :paIds and PortalType in :portalTypes] :
      [select Id, PortalAccountId, ParentRoleId from UserRole 
                where PortalAccountId in :paIds and PortalType in :portalTypes and PortalRole = :portalRole];
      return ptnerRoles;
    }
    
    // param example type = 'RoleAndSubordinates'
    public static List<Group> getListOfGroupsByRelatedId(List<Id> relatedIds, String type)
    {
      List<Group> result = new List<Group>();
      if (relatedIds == null || relatedIds.size() == 0)
      {
          return result;
      }
      result = [select Id, RelatedId from Group where Type =: type and RelatedId in: relatedIds];
      return result;
    }

    
   public static List<Id> getRange(List<Id> fromList, Integer fromIndex, Integer amount){
      List<Id> returnList = new List<Id>();
      for(Integer i = fromIndex; i < fromIndex + amount; i++){
        returnList.add(fromList.get(i));
      }
      return returnList;
   }

    //2016-03-04  CCE Added Campaign sharing for CR# 78524
    @future
    public static void UpdateCampaignSharing(List<Id> campaignIds)
    {
        UpdateCampaignSharingForTest(campaignIds);
    }

    public static void UpdateCampaignSharingForTest(List<Id> campaignIds)
    {
        System.debug('UpdateCampaignSharingForTest: campaignIds size = ' + campaignIds.size() + ', TestingCampaignShare = ' + TestingCampaignShare);
        if ((campaignIds.size() > 0) && (!Test.isRunningTest() || TestingCampaignShare))
        {
            CampaignSharing.UpdateCampaignSharing(campaignIds);
        }
    }
   
    @future (callout=true)
    public static void AddQuoteSharingRulesOnSellThroughPartner(List<String> newQuotes) {
      if(!newQuotes.isEmpty()) {
       // if(!Test.isRunningTest()) {
          SBQQQuoteSharing.UpdateQuotesSharing(newQuotes);
       // }
      }   
    }
}