/*
@author: Malay Desai, Slalom LLC.
@date: 01/18/2019
@description: Controller for the QVM "view all products" page for Qlik Commerce community
*/
public without sharing class QVM_Products_QC {
	
	public QVM_Table products { get; private set; }
	public String showArchived { get; private set; }
	
	private String filter {get; private set;}
	
	public list<QVM_Product_Data__c> productList { get { return products.rows; }}
	
	public QVM_Products_QC() {
        QVM_Partner_Data__c partnerData = null;

        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) return;
        
        showArchived = QVM.getPageParameter('showArchived');
		filter = (showArchived == 'true') ? ' and Status__c = \'Archived\'' : ' and Status__c != \'Archived\'';
		
		products = new QVM_Table(new List<QVM_Table.Column> {
            new QVM_Table.Column(QVM_Product_Data__c.Product_Name__c.getDescribe().getLabel(), 'Product_Name__c', true),
            new QVM_Table.Column(QVM_Product_Data__c.Status__c.getDescribe().getLabel(), 'Status__c', true),
            new QVM_Table.Column(QVM_Product_Data__c.Magento_Status__c.getDescribe().getLabel(), 'Magento_Status__c', true),
            new QVM_Table.Column(QVM_Product_Data__c.Version__c.getDescribe().getLabel(), 'Version__c', true),
            new QVM_Table.Column(Label.QVM_Created_By, 'CreatedBy.Name', true),
            new QVM_Table.Column(QVM_Product_Data__c.LastModifiedDate.getDescribe().getLabel(), 'LastModifiedDate', true),
            new QVM_Table.Column(QVM_Product_Data__c.Magento_Last_Updated__c.getDescribe().getLabel(), 'Magento_Last_Updated__c', true)
        },'LastModifiedDate');
        
        QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
        products.customRowsToDisplay = settings.Pagination_After_Row__c.intValue();
        this.getTableData(filter);
		
	}
	
	private void getTableData(String filter) {
        String queryString = 'select Id, Product_Name__c, Status__c, Magento_Status__c, ' +
        'Version__c, CreatedBy.Name, LastModifiedDate, Magento_Last_Updated__c from QVM_Product_Data__c ' + 
        'where QVM_Partner__r.Partner_Account__c = \'' + QVM.getUserAccountId() + '\' ' + filter;
        
        products.populate(queryString);
    }
    
    public void sort() {
        products.sort();
        this.getTableData(filter);
    }
    
    public PageReference goToQVMControlPanel() {
        PageReference p;
        p = Page.QVM_ControlPanel_QC;
        return p;
    }
    
    // CCE  |  CR# 40489 Qlik Market - add notice to register to additional pages
    public PageReference checkRegistration() {
        PageReference p = null;
        QVM_Partner_Data__c partnerData = null;
                
        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) p = Page.QVM_ControlPanelMain_QC;
        return p;
    }

}