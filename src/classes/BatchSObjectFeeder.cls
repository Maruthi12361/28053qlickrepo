/********
* NAME: BatchSObjectFeeder
* Description: Iterator for BatchProcessor
*
*Change Log:
    extcqb  2020-02-28  IT-2488 Mass update of Cases
******/
public class BatchSObjectFeeder implements Iterator<SObject>, Iterable<SObject>{
    SObject[] source;

    public Iterator<SObject> iterator() {
        return this;
    }

    public BatchSObjectFeeder(SObject[] source) {
        this.source = source;
    }

    public SObject next() {
        return source.remove(0);
    }

    public Boolean hasNext() {
        return source != null && !source.isEmpty();
    }
}