//*********************************************************/
// Author: Mark Cane&
// Creation date: 24/08/2010
// Intent:  
//          
// Change History
// --------------
// 2012-04-05   RDZ     Changing getIsConflictSchedule to use getTime instead of format('kmm')
//                      CR# 4645: https://eu1.salesforce.com/a0CD000000J5NXA 
// 2014-02-13   SAN     CR#11207 to put datetime in session as the same as eu1 sfdc server      
//*********************************************************/
public class efSessionWrapper{
    Session__c s;
    Integer counter=0;  
    Boolean isSelected= false ;
    Boolean isExpanded = false;
    private Boolean isInBucket = false;
    private List<SelectOption> sessionTopics;
    private String selectedTopic;
            
    Boolean recordCreated = false;
    Boolean isSelectedforPage= false ;
    List<efSessionAttendanceWrapper> sessionAttendanceList = new List<efSessionAttendanceWrapper>();
    String specialRequirements ='';
    boolean addTableDivider = false;
    
    public efSessionWrapper(Session__c s){  
        this.s = s;
    }

    public efSessionWrapper(Session__c s,boolean b){ 
        this.s = s;

        if(s.Session__r!=null && s.Session__r.size()>0){
            for(Session_Attendance__c sa:s.Session__r){
                sessionAttendanceList.add(new efSessionAttendanceWrapper(sa));
                selectedTopic = sa.Session_Topic__c;
            }
        }
    }
    
    /******************* GENERIC INFO **********************/    
    public String getSessionId(){
        return s.Id;
    }       
    
    public Session__c getSessions(){ 
        return s; 
    } 
    
    public String getIndex(){
        return getCounter();        
    }
    
    public String getCounter(){
        return String.valueOf(counter);
    }

    public void setCounter(Integer i){
        this.counter = i;
    }

    public boolean getAttendeeStatus(){
        List<Session_Attendance__c> att = s.Session__r;

        if(att!=null && att.size()>0){
            return true;
        } else
            return false;
    }
    
    public String getSessionAttendeeId(){
        List<Session_Attendance__c> att = s.Session__r;
        
        if(att!=null && att.size()>0){
            return att[0].Id;
        } else
            return '';
    }

    public Boolean getRecordCreated(){
        return recordCreated; 
    }
    
    public void setRecordCreated(Boolean recordCreated){  
        this.recordCreated = recordCreated; 
    }   
       
    public List<Speaker__c> getSpeakerList(){
        List<Speaker__c> sSpeakerlst = new List<Speaker__c>() ;
 
        if (s.Speakers__r != null){
            sSpeakerlst = s.Speakers__r ;
        }
        return sSpeakerlst ;
    }
    
    public String getSpeakerNames(){
        String retval = '' ;
      
        if (s.Speakers__r != null){
            for(Speaker__c s:s.Speakers__r){
                if (retval != ''){
                    retval += ', ';
                }
                retval += efUtility.giveValue(s.Clean_Contact_Name__c);
                retval += ' (' + efUtility.giveValue(s.Clean_Account_Name__c) + ')';
            }
        }      
        if (retval == ''){
            retval = 'To Be Announced' ;
        }        
        return retval;
    }
    
    public String getSessionName(){
        return efUtility.giveValue(s.Name).trim() ;
    }

    public String getEscapedName(){
        if (s.Name != null){
            return s.Name.replace('\'','\\\'');
        } else {
            return '';
        }
    }
    
    public String getSid(){
        return s.Id;        
    }

    public String getSdescription(){
        return efUtility.giveValue(s.Session_Abstract__c) ;
    }

    public String getTrackName(){
        if (s != null){
            if (s.Track__r.Name != null)   
                return efUtility.giveValue(s.Track__r.Name) ;
            else 
                return ''; 
        } else
         return '';
    }
    
    public String getDescription(){
        return s.Session_Description__c;
    }
    
    public String getCTSpeakerName(){
        return s.Customer_Theater_Speaker__c;
    }
    
    public String getVenue(){
        return s.Venue__c;
    }
    
    public String getLevel(){
        return s.Level__c;
    }
            
    public String getTheme(){
        return s.Session_Theme__c;
    }
    
    public String getCategory(){
        return s.Session_Type__c;
    }
    
    public String getRoom(){
        return efUtility.giveValue(s.Room__r.Name);
    }

    public String getSpecialRequirement(){           
        return this.specialRequirements;
    }
    
    public void setSpecialRequirement(String specialRequirement){           
        this.specialRequirements = specialRequirement ; 
    }
            
    /******************* ATTENDANCE AND CAPACITY **********************/    
    public boolean getIsAddedToSchedule(){
        if(sessionAttendanceList!=null && sessionAttendanceList.size()>0){
            for(efSessionAttendanceWrapper satt:sessionAttendanceList){
                if(!satt.getSessionAttendance().Status__c.equals(efConstants.SESSION_ATTENDANCE_CANCELLED)){
                    return true;
                }
            }
        }
        return false;
    }
    
    public Boolean getIsConflictSchedule(List<efSessionAttendanceWrapper> allSessionAttendanceList){
        if(allSessionAttendanceList!=null && allSessionAttendanceList.size()>0){
            System.debug('-------------Start------------' );
                
            for(efSessionAttendanceWrapper satt:allSessionAttendanceList){
                
                
                System.debug('satt.Id:' + satt.getSession().Id);
                System.debug('satt.Session_Start_Time__c:' + satt.getSession().Session_Start_Time__c);
                System.debug('s.Session_Start_Time__c:' + s.Session_Start_Time__c);
                
                DateTime sessionStartDateTime = satt.getSession().Session_Start_Time__c;
                DateTime sattStartDateTime = s.Session_Start_Time__c;
                if(sessionStartDateTime.isSameDay(sattStartDateTime)){
                    //Integer sattStartTime = Integer.valueOf(satt.getSession().Session_Start_Time__c.format('kmm'));
                    //Integer sattEndTime = Integer.valueOf(satt.getSession().Session_End_Time__c.format('kmm'));
                    //Integer sessStartTime = Integer.valueOf(s.Session_Start_Time__c.format('kmm'));
                    //Integer sessEndTime = Integer.valueOf(s.Session_End_Time__c.format('kmm'));    
                    
                    Long sattStartTime = satt.getSession().Session_Start_Time__c.getTime();
                    Long sattEndTime = satt.getSession().Session_End_Time__c.getTime();
                    Long sessStartTime = s.Session_Start_Time__c.getTime();
                    Long sessEndTime = s.Session_End_Time__c.getTime();    
                    
                    
                    System.debug('sattStartTime:' + sattStartTime);
                    System.debug('sattEndTime:' + sattEndTime);
                    System.debug('sessStartTime' + sessStartTime);
                    System.debug('sessEndTime' + sessEndTime);
                    
                    if(sessStartTime<=sattStartTime && sessEndTime>=sattEndTime)
                    {
                        System.debug('sessStartTime<=sattStartTime && sessEndTime>=sattEndTime:' + (sessStartTime<=sattStartTime && sessEndTime>=sattEndTime));
                        return true;
                    }
                    if((sessStartTime>sattStartTime && sessStartTime<sattEndTime) || 
                         (sessEndTime>sattStartTime && sessEndTime< sattEndTime))
                         {
                            System.debug('(sessStartTime>sattStartTime && sessStartTime<sattEndTime) || (sessEndTime>sattStartTime && sessEndTime< sattEndTime):' + ((sessStartTime>sattStartTime && sessStartTime<sattEndTime) + ' or ' + (sessEndTime>sattStartTime && sessEndTime< sattEndTime)));
                        
                            return true;
                         }
                }
            }
            System.debug('-------------Finish------------' );
                
        }
        return false;
    }    
        
    public void addSessionAttendance(Session_Attendance__c sa){
        sessionAttendanceList.add(new efSessionAttendanceWrapper(sa));
    }

    public List<efSessionAttendanceWrapper> getSessionAttendanceList(){
        return sessionAttendanceList;
    }

    public void setSessionAttendanceList(List<efSessionAttendanceWrapper> l){
        this.sessionAttendanceList = l;
    }

    public String getBalance(){
        return String.valueOf(s.Balance__c);       
    }    
    
    public boolean getIsSessionFull(){
        return s.balance__c <= 0;
    }

    public boolean hasAttendance(String saId){
        if (!efUtility.isNull(saId) &&
            this.sessionAttendanceList != null &&
            this.sessionAttendanceList.size() > 0){
            for(efSessionAttendanceWrapper satt:sessionAttendanceList){
                if (String.valueOf(satt.getSessionAttendance().Id).equals(saId)){
                    return true;
                }
            }                
        }            
        return false;
    }
    
    public Boolean getHasTopics(){
        return !efUtility.isNull(s.Session_Topic_Category__c);
    }
    
    /**************** DATE AND TIME METHODS *********************/
    //SAN CR#11207 to put datetime in session as the same as eu1 sfdc server
    public String getSessionDate(){
        if (s.Session_Start_Time__c != null){
            DateTime dt = efUtility.convertToGMT(s.Session_Start_Time__c);
            return dt.format('E, MMM d') + ' '+dt.format('h:mm a');
        } else
            return '';        
    }
    
    public String getSessionEndDate(){
        if (s.Session_End_Time__c != null){
            DateTime dt =  efUtility.convertToGMT(s.Session_End_Time__c);
            return dt.format('E, MMM d') + ' '+dt.format('h:mm a');
        } else
            return '';
    }
    
    public String getSDaynDate(){
        if (s.Session_Start_Time__c != null){
            DateTime dt =  s.Session_Start_Time__c ;
            return dt.format('EEEE, MMM d');
        } else
            return '';
    }

    public String getTimeRange(){
        String retval ='';
        
        if (s.Session_Start_Time__c != null){
            DateTime dt =  s.Session_Start_Time__c ;
            retval =  dt.format('h:mm a');
        } else
            return '';
        
        if (s.Session_End_Time__c != null){
            DateTime dte =  s.Session_End_Time__c ;
            retval = retval + '-' +  dte.format('h:mm a') ;
        } else
            return '';              
        
        return retval;
    }
    
    public Datetime getSDate(){
        return this.s.Session_Start_Time__c;
    }
    
    public Datetime getEDate(){
        return this.s.Session_End_Time__c;
    }
    
    public String getSessionLongTime(){
        return s.Session_Start_Time__c != null ? efUtility.getFormatedTimeWithAM(s.Session_Start_Time__c) : '';
    }
    
    public String getSessionTime(){
        if (s.Session_Start_Time__c != null){
            DateTime dt =  s.Session_Start_Time__c ;
            return dt.format('ha');
        } else
            return '';
    }

    public String getFormatedDate(){
        return efActivityUtil.getFormatedDate(s.Session_Start_Time__c, s.Session_End_Time__c);
    }
    
    public String getFormatedTime(){        
        return efActivityUtil.getFormatedTime(s.Session_Start_Time__c, s.Session_End_Time__c);
    }
    
    public String getLongStartDay(){
        return s.Session_Start_Time__c != null ? s.Session_Start_Time__c.format('EEEE') : '';
    }
    
    public String getShortStartDay(){
        return s.Session_Start_Time__c != null ? s.Session_Start_Time__c.format('EEE') : '';
    }
    
    public String getLongStartHour(){
        return s.Session_Start_Time__c != null ? s.Session_Start_Time__c.format('h:mm a') : '';
    }
    
    /******************* VIEW RELATED METHODS ************************/
    public Boolean getIsSelected(){
        return isSelected; 
    }
    
    public void setIsSelected(Boolean isSelected){  
        this.isSelected = isSelected; 
    }
    
    public Boolean getIsInBucket(){
        return isInBucket;
    }
    
    public void setIsInBucket(Boolean isInBucket){
          this.isInBucket = isInBucket;
    }

    public Boolean getIsExpanded(){
        return isExpanded;
    }
    
    public void setIsExpanded(Boolean isExpanded){
        this.isExpanded = isExpanded;
    }

    public Boolean getIsSelectedforPage(){
        return isSelectedforPage; 
    }
    
    public void setIsSelectedforPage(Boolean isSel){  
        this.isSelectedforPage = isSel; 
    }

    public boolean getAddTableDivider(){
      return addTableDivider;   
    }
    
    public void setAddTableDivider(boolean val){
      this.addTableDivider = val;   
    }
    
    public boolean getDefaultSelected(){
        return s.Default_Selected__c;
    }
    
    public void setDefaultSelected(Boolean defaultSelected){
        s.Default_Selected__c = defaultSelected;
    }
    
    public Boolean getIsMandatory(){
        return s.Mandatory__c;
    }
    
    public void setIsMandatory(Boolean isMandatory){
        s.Mandatory__c = isMandatory;
    }

    public List<SelectOption> getTopicList(){
        if (sessionTopics == null){
            sessionTopics = new List<SelectOption>();
            sessionTopics.add(new SelectOption('','-- None --'));
            if (getHasTopics()){
                String topicsString = efCustomSettings.getValues(efCustomSettings.SESSION_MAP).get('topics');
                
                for (String topic:efUtility.splitStringToList(topicsString, '~')){
                    if (topic.startsWith(s.Session_Topic_Category__c + ':')){
                        sessionTopics.add(
                            new SelectOption(topic,
                                             topic.replaceFirst(s.Session_Topic_Category__c + ':', '')));
                    }
                }
            }
        }        
        return sessionTopics;
    }
    
    public String getSelectedTopic(){
        return selectedTopic;
    }
    
    public void setSelectedTopic(String selectedTopic){
        this.selectedTopic = selectedTopic;
    }
    
    public String getFormattedSelectedTopic(){
        if (getHasTopics() && !efUtility.isNull(selectedTopic)){
            return selectedTopic.replaceFirst(s.Session_Topic_Category__c + ':','');
        }       
        return '';
    }
}