/**     
 * File Name : Z_RenewalSPCTriggerHandlerTest
 * Description : This is a test class to test the Z_RenewalSPCTriggerHandlerTest class with following test methods:
 *               - renewalSubscriptionTriggerHandlerAfterInsertTest
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

@IsTest
private class Z_RenewalSPCTriggerHandlerTest {

    private static Account testAcc;

    private static Contact testContact;

    private static Zuora__CustomerAccount__c testCustomerAcc;

    private static Opportunity testOppty;

    private static Zuora__Subscription__c testSubscription;

    private static zqu__Quote__c testQuote;

    private static QlikTech_Company__c testqtComp;

    private static Q2CWSettings__c testQ2cwSettings;

    private static Steelbrick_Settings__c testSteelbrickSettings;

    private static Zuora__SubscriptionRatePlan__c testSrp;

    private static Zuora__SubscriptionProductCharge__c testSpc;

    private static zqu__QuoteAmendment__c testQuoteAmendment;

    private static zqu__QuoteRatePlan__c testQrp;

    private static zqu__QuoteRatePlanCharge__c testQrpc;

    private static zqu__QuoteChargeSummary__c testQcs;

    static void setup() {
        testQ2cwSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();

        testqtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');

        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOppty = Z_RenewalTestFactory.makeRenewalOpportunity(testAcc);
        testSubscription = Z_RenewalTestFactory.makeSubscription();

        testAcc.QlikTech_Company__c = testqtComp.name;
        testAcc.Billing_Country_Code__c  = testqtComp.id;

        List<sObject> tests = new List<sObject>{
            testQ2cwSettings, testSteelbrickSettings
        };

        insert tests;

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOppty, testSubscription
        };

        insert testObjects;

        testQuote = Z_RenewalTestFactory.makeQuote3(testAcc, testContact, testOppty, testCustomerAcc);
        testQuote.zqu__ExistSubscriptionID__c = 'different id';

        update testQuote;

        testSrp = Z_RenewalTestFactory.makeSRP(testSubscription, testAcc, '123456782290');
        insert testSrp;

        testSpc = Z_RenewalTestFactory.makeSPC(testSubscription, testAcc, testSrp, '123456682901');
        insert testSpc;

        testQuoteAmendment = Z_RenewalTestFactory.makeQuoteAmendment(testQuote);
        insert testQuoteAmendment;

        testQrp = Z_RenewalTestFactory.makeQRP(testQuote, testQuoteAmendment);
        insert testQrp;
        
        testQrpc = Z_RenewalTestFactory.makeQRPC(testQuote, testQrp, 'test qrpc');
        insert testQrpc;

        testQcs = Z_RenewalTestFactory.makeQCS(testQuote, testQrp, testQrpc, 'test qcs');
        insert testQcs;

        testSpc.Zuora__RatePlanName__c = 'random rate plan name';
        update testSpc;

        System.debug('IH: quote id = ' + testQuote.Id);
    }

    @IsTest
    static void renewalSPCTriggerHandlerAfterInsertTest() {
        Semaphores.SetAllSemaphoresToTrue();
        
        setup();

        Test.startTest();

        Map<Id, Zuora__SubscriptionProductCharge__c> newSPCMap = new Map<Id, Zuora__SubscriptionProductCharge__c> { testSpc.Id => testSpc };

        Z_RenewalSPCTriggerHandler.handleAfterInsert(newSPCMap);

        Test.stopTest();
    }

    @IsTest
    static void renewalSPCTriggerHandlerAfterUpdateTest() {
        Semaphores.SetAllSemaphoresToTrue();
        
        setup();

        Test.startTest();

        Map<Id, Zuora__SubscriptionProductCharge__c> newSPCMap = new Map<Id, Zuora__SubscriptionProductCharge__c> { testSpc.Id => testSpc };

        Z_RenewalSPCTriggerHandler.handleAfterUpdate(newSPCMap);

        Test.stopTest();
    }
}