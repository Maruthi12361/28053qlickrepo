/**
    * File PartnerCateStatusDeleteControllerTest 
    * @description : Unit test for PartnerCategoryStatusDeleteController
    * @purpose : QCW-3792
    * @author : Reshma Ravi
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       20.09.2017   Reshma Ravi         Created Class
    **/
@isTest
public class PartnerCateStatusDeleteControllerTest {
    static testMethod void myUnitTest() {
        Id rtId2= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
        Id rtId = Schema.SObjectType.Partner_Category_Status__c.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();

        Account testAccount = new Account(name='Test AccountName',recordtypeid=rtId2,BillingCountry='India');
        insert testAccount;
        Partner_Category_Status__c testPartner = new Partner_Category_Status__c(Partner_Account__c=testAccount.Id , recordtypeid=rtId ,Partner_Level__c= 'Elite' , Partner_Category__c='Resell' , Program_Version__c = 'QPP');
        insert testPartner;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testPartner);
        PartnerCategoryStatusDeleteController c = new PartnerCategoryStatusDeleteController(sc);
        
        System.assertEquals(c.init().getUrl(), '/'+testAccount.Id);
    }
}