/*******************************************************************
* File RefreshApp
  * @description : Performs Sandbox Refresh activity.Logic moved from Sandbox Update Tab
                   It will be executed after QA refresh in prod.
  * @author : Pramod Kumar V
  * Modification Log ===============================================
  Ver     Date         Author              Modification
  1       20.01.2018   Pramod Kumar V      Created Class
  2       28.04.2018   Pramod Kumar v      ITRM-60
  3       25.07.2018   ext_bjd             QAR-64 Added runUpdate method for asynchronous invoke other methods
  4       06.03.2019   ext_bjd             ITRM-320 Added method getUrlForInstance to determine instance url and update
                                           custom settings Support_Portal_Login_Url__c and Support_Portal_Url__c dynamically
  5       03.02.2020   ext_bjd             ITRM-380 Added new custom settings, code refactored
********************************************************************/

public class UpdateCustomSettings {
    @TestVisible
    private static final String URL_PART_HTTPS = 'https://';
    @TestVisible
    private static final String URL_PART_QLIKSUPPORT = 'qliksupport';
    @TestVisible
    private static final String URL_PART_QLIKID = 'qlikid';
    @TestVisible
    private static final String URL_PART_QLIK_COM = 'qlik.com/portal/support?relaystate=';
    @TestVisible
    private static final String URL_PART_FORCE_COM = 'force.com';
    @TestVisible
    private static final String URL_PART_ATTUNITYCUTOMERPORTAL = 'attunitycustomerportal';

    public static final String SALESFORCE_BASE_URL = System.Url.getSalesforceBaseURL().toExternalForm();
    public static final List<String> SALESFORCE_BASE_URL_PARTS = System.Url.getSalesforceBaseUrl().getHost().split('\\.');

    @Future
    public static void runUpdate(String sandboxName) {
        UpdateCustomSettings customSettings = new UpdateCustomSettings();
        RefreshSandboxData__mdt refreshSandboxData = customSettings.getRefreshSandboxDataBySandboxName(sandboxName);
        updateCustomSettingsFromMdt(refreshSandboxData);
    }

    public RefreshSandboxData__mdt getRefreshSandboxDataBySandboxName(String sandboxName) {
        String defaultKeyValue = 'DEFAULTVALUES';
        Boolean isSandbox = PostRefreshHandler.isSandboxFinder();
        RefreshSandboxData__mdt refreshSandboxData = null;
        Map<String, RefreshSandboxData__mdt> refreshSandboxItem = new Map<String, RefreshSandboxData__mdt>();
        for (RefreshSandboxData__mdt item : [
                SELECT Id, Attunity_Portal_Url__c, BoomiBaseURL__c, BoomiToken__c, Close_Chat_Survey_Id__c,
                        Close_Chat_Survey_Id_QA__c, CMInstanceUrl__c, MasterLabel, NSQuoteApprovals__c,
                        QlikMarket_Website__c, Support_Portal_Live_Agent_API_Endpoint__c,
                        Support_Portal_Login_Page_Url__c, Support_Portal_Login_Url__c, Support_Portal_Url__c,
                        Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c, Support_Portal_CSS_Base__c,
                        Support_Portal_index_allow_options__c, Survey_Link_For_Live_Chat__c, Survey_Link_For_Live_Chat_QA__c,
                        Survey_Link__c, Survey_Link_QA__c, SpringAccountPrefix__c, ULC_Base_URL__c
                FROM RefreshSandboxData__mdt
        ]) {
            refreshSandboxItem.put(item.MasterLabel.toUppercase(), item);
        }
        // 1) Metadata for Production
        if (!isSandbox && Test.isRunningTest()) {
            refreshSandboxData = getProdCustomSettingSandboxData();
        // 2) Metadata for QA or DEVQ sandboxes
        } else if (refreshSandboxItem.containsKey(sandboxName.toUppercase())) {
            refreshSandboxData = refreshSandboxItem.get(sandboxName.toUppercase());
        // 3) Metadata for standard developer sandbox
        } else if (refreshSandboxItem.containsKey(defaultKeyValue)) {
            refreshSandboxData = refreshSandboxItem.get(defaultKeyValue);
        }
        return refreshSandboxData;
    }

    public static void updateCustomSettingsFromMdt(RefreshSandboxData__mdt refreshSandboxData) {
        QS_Partner_Portal_Urls__c qsSettings = QS_Partner_Portal_Urls__c.getOrgDefaults();
        QTCustomSettings__c qtSettings = QTCustomSettings__c.getOrgDefaults();
        QTCustomSettingsHier__c qtSettingsHier = QTCustomSettingsHier__c.getOrgDefaults();
        QVM_Settings__c qvmSettings = QVM_Settings__c.getOrgDefaults();
        Steelbrick_Settings__c sbSettings = Steelbrick_Settings__c.getOrgDefaults();
        List<String> urlsInstance = getUrlForInstance();

        if (refreshSandboxData != null) {
            qsSettings.Support_Portal_CSS_Base__c = refreshSandboxData.Support_Portal_CSS_Base__c;
            qsSettings.Support_Portal_index_allow_options__c = refreshSandboxData.Support_Portal_index_allow_options__c;
            qsSettings.Support_Portal_Live_Agent_API_Endpoint__c = refreshSandboxData.Support_Portal_Live_Agent_API_Endpoint__c;
            qsSettings.Support_Portal_Login_Page_Url__c = refreshSandboxData.Support_Portal_Login_Page_Url__c;
            qsSettings.Support_Portal_Login_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM : refreshSandboxData.Support_Portal_Login_Url__c;
            qsSettings.Support_Portal_Logout_Page_Url__c = refreshSandboxData.Support_Portal_Logout_Page_Url__c;
            qsSettings.Support_Portal_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + URL_PART_QLIKID + '-' + urlsInstance[0] + '.' + URL_PART_QLIK_COM + URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM + '/': refreshSandboxData.Support_Portal_Url__c;
            qsSettings.Support_Portal_Url_Base__c = refreshSandboxData.Support_Portal_Url_Base__c;
            qsSettings.Attunity_Portal_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM + '/' + URL_PART_ATTUNITYCUTOMERPORTAL + '/s': refreshSandboxData.Attunity_Portal_Url__c;

            qtSettings.ULC_Base_URL__c = refreshSandboxData.ULC_Base_URL__c;
            qtSettings.BoomiBaseURL__c = refreshSandboxData.BoomiBaseURL__c;
            qtSettings.BoomiToken__c = refreshSandboxData.BoomiToken__c;

            qtSettingsHier.Base_URL__c = SALESFORCE_BASE_URL;
            qtSettingsHier.Base_Content_URL__c = URL_PART_HTTPS + SALESFORCE_BASE_URL_PARTS[0] + '--c.' + SALESFORCE_BASE_URL_PARTS[1] + '.content.' + URL_PART_FORCE_COM;
            qtSettingsHier.Close_Chat_Survey_Id__c = refreshSandboxData.Close_Chat_Survey_Id__c;
            qtSettingsHier.Close_Chat_Survey_Id_QA__c = refreshSandboxData.Close_Chat_Survey_Id_QA__c;
            qtSettingsHier.Survey_Link_For_Live_Chat__c = refreshSandboxData.Survey_Link_For_Live_Chat__c;
            qtSettingsHier.Survey_Link_For_Live_Chat_QA__c = refreshSandboxData.Survey_Link_For_Live_Chat_QA__c;
            qtSettingsHier.Survey_Link__c = refreshSandboxData.Survey_Link__c;
            qtSettingsHier.Survey_Link_QA__c = refreshSandboxData.Survey_Link_QA__c;

            qvmSettings.QlikMarket_Website__c = refreshSandboxData.QlikMarket_Website__c;

            sbSettings.BoomiBaseURL__c = refreshSandboxData.BoomiBaseURL__c;
            sbSettings.BoomiToken__c = refreshSandboxData.BoomiToken__c;
            sbSettings.SpringAccountPrefix__c = refreshSandboxData.SpringAccountPrefix__c;
            sbSettings.CMInstanceUrl__c = refreshSandboxData.CMInstanceUrl__c;

            update qsSettings;
            update qtSettings;
            update qtSettingsHier;
            update qvmSettings;
            update sbSettings;
        }
    }

    @TestVisible
    private static List<String> getUrlForInstance() {
        // Logic with possible options:
        // 1) cs80.salesforce.com               -- 3 parts, Instance is 1st part, name not determinable
        // 2) qlik--qa.cs107.my.salesforce.com  -- 5 parts, Instance is 2nd part, name is 1nd part
        // 3) qlik.my.salesforce.com            -- 4 parts, Instance and name is not determinable
        List<String> instances = new List<String>();
        List<String> parts = System.Url.getSalesforceBaseUrl().getHost().replace('-api','').replace('qlik--','').split('\\.');

        if (parts.size() == 3) {
            instances.add(null);
        } else if (parts.size() == 5) {
            instances.add(parts[0]);
            instances.add(parts[1]);
        } else {
            instances.add(null);
        }
        return instances;
    }

    @TestVisible
    private static RefreshSandboxData__mdt getProdCustomSettingSandboxData(){
        QS_Partner_Portal_Urls__c qsSettings = QS_Partner_Portal_Urls__c.getOrgDefaults();
        QTCustomSettings__c qtSettings = QTCustomSettings__c.getOrgDefaults();
        QTCustomSettingsHier__c qtSettingsHier = QTCustomSettingsHier__c.getOrgDefaults();
        QVM_Settings__c qvmSettings = QVM_Settings__c.getOrgDefaults();
        Steelbrick_Settings__c sbSettings = Steelbrick_Settings__c.getOrgDefaults();
        List<String> urlsInstance = getUrlForInstance();

        RefreshSandboxData__mdt metaData = new RefreshSandboxData__mdt();

        metaData.Support_Portal_CSS_Base__c = qsSettings.Support_Portal_CSS_Base__c;
        metaData.Support_Portal_Live_Agent_API_Endpoint__c = qsSettings.Support_Portal_Live_Agent_API_Endpoint__c;
        metaData.Support_Portal_Login_Page_Url__c = qsSettings.Support_Portal_Login_Page_Url__c;
        metaData.Support_Portal_Login_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM : metaData.Support_Portal_Login_Url__c;
        metaData.Support_Portal_Logout_Page_Url__c = qsSettings.Support_Portal_Logout_Page_Url__c;
        metaData.Support_Portal_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + URL_PART_QLIKID + '-' + urlsInstance[0] + '.' + URL_PART_QLIK_COM + URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM + '/': metaData.Support_Portal_Url__c;
        metaData.Support_Portal_Url_Base__c = qsSettings.Support_Portal_Url_Base__c;
        metaData.Attunity_Portal_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM + '/' + URL_PART_ATTUNITYCUTOMERPORTAL + '/s': metaData.Attunity_Portal_Url__c;

        metaData.ULC_Base_URL__c = qtSettings.ULC_Base_URL__c;
        metaData.BoomiBaseURL__c = qtSettings.BoomiBaseURL__c;
        metaData.Close_Chat_Survey_Id__c = qtSettingsHier.Close_Chat_Survey_Id__c;
        metaData.Close_Chat_Survey_Id_QA__c = qtSettingsHier.Close_Chat_Survey_Id_QA__c;
        metaData.Survey_Link_For_Live_Chat__c = qtSettingsHier.Survey_Link_For_Live_Chat__c;
        metaData.Survey_Link_For_Live_Chat_QA__c = qtSettingsHier.Survey_Link_For_Live_Chat_QA__c;
        metaData.Survey_Link__c = qtSettingsHier.Survey_Link__c;
        metaData.Survey_Link_QA__c = qtSettingsHier.Survey_Link_QA__c;

        metaData.QlikMarket_Website__c = qvmSettings.QlikMarket_Website__c;

        metaData.BoomiBaseURL__c = sbSettings.BoomiBaseURL__c;
        metaData.BoomiToken__c = sbSettings.BoomiToken__c;
        metaData.SpringAccountPrefix__c = sbSettings.SpringAccountPrefix__c;
        metaData.CMInstanceUrl__c = sbSettings.CMInstanceUrl__c;
        return metaData;
    }
}