/**
* LiveChatSkillsHomeController controller for LiveChatSkillsHomeComponent.
* 
* Change log: 
* 23-12-2018    ext_vos     CHG0035234: add/remove skillset for agent.
*/
public without sharing class LiveChatSkillsHomeController {

    private static final String NAME_BUTTON_ADD = 'Add Sales skill';
    private static final String NAME_BUTTON_REMOVE = 'Remove Sales skill';
    private static final String ANAVAILABLE_MESSAGE = 'This functionality is available for LiveAgent users only';
    private static final String INFO_MESSAGE = 'Change my ability to work with Sales Live chats:';
    private static final String SKILL_NAME_SALE = 'Skill_set_all';
    private static final String SKILLUSER_HTTP_ENDPOINT = '/services/data/v41.0/sobjects/SkillUser';

    public Boolean isSectionAvailable {get;set;}
    public String buttonName {get;set;}
    public String info {get;set;}
    
    public final String salesSkillId = [select Id, DeveloperName from Skill where DeveloperName =: SKILL_NAME_SALE limit 1].Id;
        
    public LiveChatSkillsHomeController() {
        isSectionAvailable = [select Id, UserPermissionsLiveAgentUser from User where Id =: UserInfo.getUserId()].UserPermissionsLiveAgentUser;
        if (!isSectionAvailable) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ANAVAILABLE_MESSAGE));
        } else {            
            info = INFO_MESSAGE;
            // check skills: if user has no access to Sales chats -> show "add" button; show "remove" button vice versa
            List<SkillUser> relations = [select Id from SkillUser where SkillId =: salesSkillId and UserId =: UserInfo.getUserId()];
            buttonName = relations.isEmpty() ? NAME_BUTTON_ADD : NAME_BUTTON_REMOVE;
        }       
    }
    
    public PageReference updateSkills() {       
        List<SkillUser> relations = [select Id from SkillUser where SkillId =: salesSkillId and UserId =: UserInfo.getUserId()];
        if (relations.isEmpty()) {
            SkillUser newOne = new SkillUser(SkillId = salesSkillId, UserId = UserInfo.getUserId());
            addSkill(newOne);
        } else {
            deleteSkill(relations[0].Id);
        }
        return new PageReference(ApexPages.currentPage().getUrl()).setRedirect(true);
    }

    private static String getBaseURL() {
        return QTCustomSettingsHier__c.getInstance().Base_URL__c;
    }

    public static void addSkill(SkillUser newRecord) {
        String endpoint = getBaseURL() + SKILLUSER_HTTP_ENDPOINT;

        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        request.setBody(JSON.serialize(newRecord));
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        new Http().send(request); 
    }

    public static void deleteSkill(String skillId) {
        String endpoint = getBaseURL() + SKILLUSER_HTTP_ENDPOINT + '/' + skillId;

        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        request.setEndpoint(endpoint);
        request.setMethod('DELETE');
        new Http().send(request);
    }
}