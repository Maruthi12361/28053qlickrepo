/**     
 * File Name : Z_RenewalTestFactory
 * Description : This is a factory class used for creating mock/test data to supply for the test classes.
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

@IsTest
public class Z_RenewalTestFactory { 

    private static final String ZSUB_ID = '12345';

    public static Renewal_Automation_Settings__c settings = new Renewal_Automation_Settings__c(
                                                                AttunityRepresentativeUser__c='005D0000008C3rKIAS',
                                                                ErrorEmailDL__c='test@email.com',
                                                                OpportunityBatchSize__c=25,
                                                                OpportunityRecordsLimit__c=25,
                                                                QuoteClosureRecordsLimit__c=25,
                                                                QuoteCreationBatchSize__c=25,
                                                                QuoteCreationRecordsLimit__c=25,
                                                                SubscriptionEndDateBound__c=Date.today()
                                                            );
    
    public static Account makeAccount() {
        Account testAcc = new Account(
            Name='[Renewal] Test Account',
            Partner_Margin__c=12,
            Navision_Status__c='Partner',
            Territory_Country__c='United States',
            QlikTech_Company__c='qlik tech company',
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId()
        );
        
        insert testAcc;
        return testAcc;
    }

    public static Q2CWSettings__c makeQ2CWSettings() {
        return new Q2CWSettings__c(
            Disable_Address_trigger__c=false,
            Disable_Contract_trigger__c=false,
            Disable_User_trigger__c=false,
            Max_Units_Consumed_Limit__c=7000,
            Max_Units_Hit_Error_Messag__c='error message',
            Qlik_Id_Download__c='http://downloadurl',
            Qlik_Id_Login__c='http://registerurl',
            SalesQCCSZuoraSubscription_OppRecordType__c='recordtypeid',
            Zuora_SSD_Users__c='a,b,c,d,e'
        );
    }

    public static Steelbrick_Settings__c makeSteelbrickSettings() {
        return new Steelbrick_Settings__c(
            Name='SteelbrickSettingsDetails',
            Academic_program_OppRecordType__c = '012D0000000KB2N',
            Account__c = '00ND00000068LYg',
            Billing_Address__c = '00ND00000068Suq',
            BoomiBaseURL__c = 'https://q2cwsbx.qliktech.com:9093/ws/simple/',
            BoomiToken__c = 'BASIC ',
            CMInstanceUrl__c = 'na21',
            Deal_split_child_OppRecordType__c = '01220000000DoEjAAK',
            eCustoms_QlikBuy_RecordTypes__c = '',
            ECustomsManualApproval__c = null,
            TemplateId__c = null,
            ExcludedCountries__c = 'Russian Federation',
            ExcludedCountries1__c = 'Turkmenistan,United Arab ',
            Industry__c = '00ND00000068St8',
            Invoice_Address__c = '00ND00000068StI',
            NewVisualComplianceEnabled__c = true,
            OEM_Order_Deal_Split_Child_OppRecordType__c = '',
            Oem_order_OppRecordType__c = '01220000000DXq5AAG',
            Oem_prepay_OppRecordType__c = '01220000000DNwZAAW',
            OEM_Recruitment_Deal_Split_Child_Id__c = '012D0000000KIkxIAG',
            Oem_recruitment_OppRecordType__c = '012D0000000JpIxIAK',
            OEM_Record_Types__c = '01220000000DXq5,012D0000000KIkn,01220000',
            Opportunity__c = '00ND00000068LZB',
            Ops_Admin_Profile_Id__c = '00e20000001ODnH,00eD0000001P4DC',
            Partner_Not_For_Resale_OppRecordType__c = '012D0000000VCl0IAG',
            Partner_Contact__c = '00ND00000068Su0',
            Primary__c = '00ND00000068LZJ',
            Qlik_online_purchase_OppRecordType__c = '012D0000000KJRSIA4',
            QuoteDealTypeFalse__c = null,
            QuoteDealTypeTrue__c = null,
            Quote_Recipient__c = '00ND00000068SuQ',
            RedCountry__c = 'Iran (Islamic Republic of), Iran,Syrian Arab ',
            Revenue_Type__c = '00ND00000068Sua',
            SBPricebookId__c = '01s20000000E0PW',
            Second_Partner__c = '00ND00000068Sun',
            Second_Partner_Contact__c = '00ND00000068jCz',
            Sector__c = '00ND00000068Suo',
            Sell_Through_Partner__c = '00ND00000068Sur',
            Shipping_Address__c = '00ND00000068Sut',
            Short_Description__c = '00ND00000068Suv',
            SpringAccountPrefix__c = '17990',
            Standard_PriceBook__c = '01s20000000E0PW',
            Steelbrick_OppRecordType__c = '012D0000000khWZ',
            Subscription_Date__c = '00ND00000068LZU',
            Subscription_Terms__c = '00ND00000068LZW',
            Sys_Admin_ProfileId__c = '00e20000000yyUzAAI',
            Type__c = '00ND00000068SvQ',
            YellowCountry__c = 'Iraq,South Sudan,Russian ');
    }

    public static Contact makeContact(Account testAcc) {
        return new Contact(
            FirstName = 'Renewal',
            LastName='Test',
            Email='test@email.com',
            MailingCountry='United States',
            MailingState='CA',
            MailingPostalCode='94404',
            MailingStreet='123 Main Street',
            MailingCity='Foster City',
            Phone='123-456-7890',
            AccountId=testAcc.Id
        );
    }

    public static Zuora__CustomerAccount__c makeBillingAccount(Account testAcc) {

        Zuora__CustomerAccount__c billAcc = new Zuora__CustomerAccount__c();
        billAcc.Name = 'Test Billing Account';
        billAcc.Zuora__Account__c = testAcc.Id;
        billAcc.Zuora__AccountNumber__c = 'A-001';
        billAcc.Zuora__AutoPay__c = false;
        billAcc.Zuora__Batch__c = 'Batch1';
        billAcc.Zuora__BillCycleDay__c = '1';
        billAcc.Zuora__Balance__c = 0;
        billAcc.Zuora__BillToCity__c = 'Foster City';
        billAcc.Zuora__BillToCountry__c = 'United States';
        billAcc.Zuora__BillToAddress1__c = '123 Main Street';
        billAcc.Zuora__BillToState__c = 'California';
        billAcc.Zuora__BillToPostalCode__c = '94401';
        billAcc.Zuora__BillToId__c = '1234';
        billAcc.Zuora__BillToName__c = 'Test Name';
        billAcc.Zuora__BillToWorkEmail__c = 'test@test.com';
        billAcc.Zuora__MRR__c = 100;
        billAcc.Zuora__Credit_Balance__c = 100;
        billAcc.Zuora__Currency__c = 'USD';
        billAcc.Zuora__External_Id__c = '1234';
        billAcc.Zuora__PaymentTerm__c = 'Due Upon Receipt';
        billAcc.Zuora__Status__c = 'Active';
        billAcc.Zuora__Zuora_Id__c = '1234';

        return billAcc;
    }

    public static Opportunity makeOpportunity(Account testAcc) {
        return new Opportunity(
            Name='Test Opportunity',
            ForecastCategoryName='Omitted',
            Short_Description__c='Test Opportunity',
            Revenue_Type__c='Direct',
            CurrencyIsoCode='USD',
            CloseDate=System.today().addMonths(1),
            StageName='Goal Identified',
            Type='New Business',
            Next_Renewal_Opportunity__c=null,
            AccountId=testAcc.Id,
            RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription Renewal').getRecordTypeId(),
            Is_Manual_Process__c=false,
            Send_to_DL_QT_Region__c='test@gmail.com'
        );
    }

    public static Opportunity makeOpportunity2(Account testAcc) {
        return new Opportunity(
            Name='Test Opportunity',
            ForecastCategoryName='Omitted',
            Short_Description__c='Test Opportunity',
            Revenue_Type__c='Direct',
            CurrencyIsoCode='USD',
            CloseDate=System.today().addMonths(1),
            StageName='Goal Identified',
            Type='New Business',
            Next_Renewal_Opportunity__c='1234',
            AccountId=testAcc.Id,
            RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS Zuora Subscription').getRecordTypeId(),
            Is_Manual_Process__c=false,
            Primary_Zuora_Quote_OF_Status__c='None',
            Subscription_Forecast_ACV_Amount__c=123,
            Send_to_DL_QT_Region__c='test@gmail.com'
        );
    }

    public static Opportunity makeRenewalOpportunity(Account testAcc) {
        return new Opportunity(
            Name='Test Opportunity',
            ForecastCategoryName='Omitted',
            Short_Description__c='Test Opportunity',
            Revenue_Type__c='Direct',
            CurrencyIsoCode='USD',
            CloseDate=System.today().addMonths(1),
            StageName='Goal Identified',
            Type='New Business',
            Next_Renewal_Opportunity__c=null,
            AccountId=testAcc.Id,
            RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription Renewal').getRecordTypeId(),
            Is_Manual_Process__c=false,
            Send_to_DL_QT_Region__c='test@gmail.com'
        );
    }

    public static OpportunityContactRole makeOCR(Contact c, Opportunity opp) {
        return new OpportunityContactRole(
            ContactId=c.Id,
            OpportunityId=opp.Id
        );
    }

    public static Opportunity makeNewSubscriptionOpportunity(Account testAcc) {
        return new Opportunity(
            Name='Test Opportunity',
            ForecastCategoryName='Omitted',
            Short_Description__c='Test Opportunity',
            Revenue_Type__c='Direct',
            CurrencyIsoCode='USD',
            CloseDate=System.today().addMonths(-1),
            StageName='Goal Identified',
            Type='New Business',
            AccountId=testAcc.Id,
            RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS Zuora Subscription').getRecordTypeId(),
            Is_Manual_Process__c=false,
            Primary_Zuora_Quote_OF_Status__c='None',
            Subscription_Forecast_ACV_Amount__c=123,
            Send_to_DL_QT_Region__c='test@gmail.com'
        );
    }

    public static Zuora__Subscription__c makeSubscription() {
        return new Zuora__Subscription__c(
            Zuora__External_Id__c=ZSUB_ID,
            Zuora__SubscriptionNumber__c=ZSUB_ID,
            ACV__c=ZSUB_ID,
            Zuora__SubscriptionEndDate__c=Date.today(),
            Zuora__Status__c='Active',
            Digital__c='No',
            Zuora__RenewalTerm__c='12 months',
            Zuora__TermEndDate__c=Date.today().addMonths(12),
            Zuora__SubscriptionStartDate__c=Date.today(),
            Zuora__TermSettingType__c='TERMED'
        );
    }

    public static Zuora__SubscriptionRatePlan__c makeSRP(Zuora__Subscription__c sub, Account acc, String zId) {
        Zuora__SubscriptionRatePlan__c srp = new Zuora__SubscriptionRatePlan__c();
        srp.Zuora__Account__c = acc.Id;
        srp.Zuora__External_Id__c = zId;
        srp.Name = 'Test SRP';
        return srp;
    }

    public static Zuora__SubscriptionProductCharge__c makeSPC(Zuora__Subscription__c sub, Account acc, Zuora__SubscriptionRatePlan__c srp, String zId){
        Zuora__SubscriptionProductCharge__c spc = new Zuora__SubscriptionProductCharge__c();
        spc.Zuora__Account__c = acc.Id;
        spc.Zuora__External_Id__c = zId;
        spc.Zuora__Zuora_Id__c = zId;
        spc.Name = 'Test SPC';
        spc.Zuora__SubscriptionRatePlan__c = srp.Id;
        spc.Zuora__SubscriptionChargeNumber__c = 'C-130';
        spc.Zuora__ChargeNumber__c = 'C-130';
        return spc;
    }

    public static zqu__QuoteAmendment__c makeQuoteAmendment(zqu__Quote__c quote){
        zqu__QuoteAmendment__c qa = new zqu__QuoteAmendment__c();
        qa.Name = 'Test Quote Amendment';
        qa.zqu__Quote__c = quote.Id;
        qa.zqu__Type__c = 'NewProduct';
        return qa;
    }

    public static zqu__QuoteRatePlan__c makeQRP(zqu__Quote__c quote, zqu__QuoteAmendment__c qa) {
        zqu__QuoteRatePlan__c qrp = new zqu__QuoteRatePlan__c();
        qrp.Name = 'Test Quote Rate Plan';
        qrp.zqu__QuoteAmendment__c = qa.Id;
        qrp.zqu__Quote__c = quote.Id;
        qrp.zqu__SubscriptionRatePlanZuoraId__c = '1234567890';
        return qrp;
    }
    
    public static zqu__QuoteRatePlanCharge__c makeQRPC(zqu__Quote__c quote, zqu__QuoteRatePlan__c qrp, String name){
        zqu__QuoteRatePlanCharge__c qrpc = new zqu__QuoteRatePlanCharge__c();
        qrpc.Name = name;
        qrpc.zqu__QuoteRatePlan__c = qrp.id;
        qrpc.zqu__DeltaTCV__c = null;
        qrpc.zqu__SubscriptionRatePlanChargeZuoraId__c = '12345668901';
        return qrpc;
    }

    public static zqu__QuoteChargeSummary__c makeQCS(zqu__Quote__c quote, zqu__QuoteRatePlan__c qrp, zqu__QuoteRatePlanCharge__c qrpc, String name) {
        zqu__QuoteChargeSummary__c qcs = new zqu__QuoteChargeSummary__c();
        qcs.Name = name;
        qcs.zqu__QuoteRatePlanCharge__c = qrpc.id;
        qcs.zqu__QuoteRatePlan__c = qrp.id;
        qcs.zqu__DeltaTCV__c = null;
        qcs.zqu__ChargeNumber__c = 'C-130';
        return qcs;
    }

    public static zqu__Quote__c makeQuote(Account testAcc, Contact testContact, Opportunity testOppty, Zuora__CustomerAccount__c testCustomerAcc) {
        zqu__Quote__c testQuote = new zqu__Quote__c(
            zqu__ZuoraSubscriptionId__c=ZSUB_ID,
            zqu__Hidden_Subscription_Name__c=ZSUB_ID,
            zqu__BillToContact__c=testContact.Id,
            zqu__SoldToContact__c=testContact.Id,
            Billing_Frequency__c='Prepaid',
            Name='my test quote',
            Quote_Status__c='Accepted by Customer',
            zqu__Opportunity__c=testOppty.Id,
            zqu__Account__c=testAcc.id,
            zqu__Currency__c='USD',
            zqu__Status__c='Sent to Z-Billing',
            zqu__SubscriptionTermStartDate__c=Date.today(),
            zqu__startdate__c=Date.today(),
            zqu__InitialTerm__c=12,
            zqu__SubscriptionTermEndDate__c=Date.today().addMonths(12),
            zqu__SubscriptionType__c='New Subscription',
            zqu__RenewalTerm__c=12,
            zqu__Primary__c=true,
            zqu__InvoiceOwnerId__c=testCustomerAcc.Zuora__External_Id__c,
            zqu__InvoiceOwnerName__c=testCustomerAcc.Name
        );

        insert testQuote;
        return testQuote;
    }

    public static zqu__Quote__c makeQuote3(Account testAcc, Contact testContact, Opportunity testOppty, Zuora__CustomerAccount__c testCustomerAcc) {
        zqu__Quote__c testQuote = new zqu__Quote__c(zqu__ZuoraSubscriptionId__c=ZSUB_ID,
                                                    zqu__Hidden_Subscription_Name__c=ZSUB_ID,
                                                    zqu__BillToContact__c=testContact.Id,
                                                    zqu__SoldToContact__c=testContact.Id,
                                                    Billing_Frequency__c='Prepaid',
                                                    Name='my test quote',
                                                    zqu__ZuoraEntityId__c=testCustomerAcc.Zuora__EntityID__c,
                                                    // zqu__BillingEntity__c=previousQuote.zqu__BillingEntity__c,
                                                    zqu__Amendment_Name__c='inki test amendment name',
                                                    zqu__Status__c='New',
                                                    Quote_Status__c='Open',
                                                    zqu__Opportunity__c=testOppty.Id,
                                                    zqu__SubscriptionType__c='Renew Subscription',
                                                    zqu__Currency__c='USD',
                                                    zqu__InvoiceOwnerId__c=testCustomerAcc.Zuora__External_Id__c,
                                                    zqu__InvoiceOwnerName__c=testCustomerAcc.Name,
                                                    zqu__Zuora_Account_Number__c=testCustomerAcc.Name,
                                                    zqu__ZuoraAccountID__c=testCustomerAcc.Zuora__External_Id__c,
                                                    zqu__ExistSubscriptionID__c=ZSUB_ID,
                                                    zqu__Primary__c=true,
                                                    RecordTypeId=Schema.SObjectType.zqu__Quote__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId(),
                                                    zqu__ValidUntil__c=Date.today().addMonths(1),
                                                    zqu__QuoteBusinessType__c='Renewal',
                                                    zqu__SubscriptionTermEndDate__c=Date.today().addMonths(12),
                                                    zqu__Subscription_Term_Type__c='termed',
                                                    zqu__StartDate__c=Date.today(),
                                                    zqu__RenewalTerm__c=12,
                                                    zqu__PaymentTerm__c='Net 120 Days',
                                                    zqu__AutoRenew__c=true);

        insert testQuote;
        return testQuote;
    }

    public static zqu__Quote__c makeQuotesBulk(Account testAcc, Contact testContact, Zuora__CustomerAccount__c testCustomerAcc) {
        return new zqu__Quote__c(
            zqu__BillToContact__c=testContact.Id,
            zqu__SoldToContact__c=testContact.Id,
            Billing_Frequency__c='Prepaid',
            Name='my test quote',
            Quote_Status__c='Accepted by Customer',
            zqu__Account__c=testAcc.id,
            zqu__Currency__c='USD',
            zqu__Status__c='Sent to Z-Billing',
            zqu__SubscriptionTermStartDate__c=Date.today(),
            zqu__startdate__c=Date.today(),
            zqu__InitialTerm__c=12,
            zqu__SubscriptionTermEndDate__c=Date.today().addMonths(12),
            zqu__SubscriptionType__c='New Subscription',
            zqu__RenewalTerm__c=12,
            zqu__Primary__c=true,
            zqu__InvoiceOwnerId__c=testCustomerAcc.Zuora__External_Id__c,
            zqu__InvoiceOwnerName__c=testCustomerAcc.Name
        );
    }

    public static List<Opportunity> makeOpportunitiesBulkQCCS(Account testAcc, Integer oppNumber){
    
        List<Opportunity> oppsQCCS = new List<Opportunity>(); 
        
        for(Integer int2=0;int2<oppNumber;int2++){
            oppsQCCS.add(new Opportunity(
                Name='Test Opportunity' + int2, 
                ForecastCategoryName='Omitted',
                Short_Description__c='Test Opportunity',
                Revenue_Type__c='Direct',
                CurrencyIsoCode='USD',
                CloseDate=System.today().addMonths(1),
                StageName='Goal Identified',
                Type='New Business',
                AccountId=testAcc.Id,
                RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS Zuora Subscription').getRecordTypeId(),
                Is_Manual_Process__c=false,
                Primary_Zuora_Quote_OF_Status__c='None',
                Subscription_Forecast_ACV_Amount__c=123,
                Send_to_DL_QT_Region__c='test@gmail.com'
            ));
        }
            
        insert oppsQCCS;
        return oppsQCCS; 
    }

    public static zqu__Quote__c makeQuote2(Account testAcc, Contact testContact, Opportunity testOppty, Zuora__CustomerAccount__c testCustomerAcc) {
        zqu__Quote__c testQuote = new zqu__Quote__c(
            zqu__ZuoraSubscriptionId__c=ZSUB_ID,
            zqu__BillToContact__c=testContact.Id,
            zqu__SoldToContact__c=testContact.Id,
            Billing_Frequency__c='Prepaid',
            Name='my test quote',
            zqu__Opportunity__c=testOppty.Id,
            zqu__Account__c=testAcc.id,
            zqu__Currency__c='USD',
            zqu__Status__c='New',
            zqu__SubscriptionTermStartDate__c=Date.today(),
            zqu__startdate__c=Date.today(),
            zqu__InitialTerm__c=12,
            zqu__SubscriptionTermEndDate__c=Date.today().addMonths(12),
            zqu__SubscriptionType__c='New Subscription',
            zqu__RenewalTerm__c=12,
            zqu__Primary__c=true,
            zqu__InvoiceOwnerId__c=testCustomerAcc.Zuora__External_Id__c,
            zqu__InvoiceOwnerName__c=testCustomerAcc.Name
        );

        insert testQuote;
        return testQuote;
    }

    public static zqu__QuoteChargeDetail__c makeQCD(zqu__Quote__c quote){
        return new zqu__QuoteChargeDetail__c(
            Name='Test quote charge detail',
            zqu__Quote__c=quote.id,
            zqu__DeltaTCV__c=50
        );
    }

    public static Apex_Debug_Log__c makeApexDebugLogForRenewalOpp() {
        return new Apex_Debug_Log__c(
            Apex_Class__c='Z_RenewalOpportunityJob',
            Message__c='Processing',
            Method__c='createRenewalOpportunities',
            Stack_Trace__c=null,
            Type__c='Information'
        );
    }

    public static Apex_Debug_Log__c makeApexDebugLogForRenewalQuote() {
        return new Apex_Debug_Log__c(
            Apex_Class__c='Z_RenewalQuoteInitJob',
            Message__c='Processing',
            Method__c='createRenewalQuotes',
            Stack_Trace__c=null,
            Type__c='Information'
        );
    }
}