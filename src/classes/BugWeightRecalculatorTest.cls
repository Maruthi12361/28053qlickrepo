/********************************************************
* 2020-05-05    ain         IT-2767, test class for BugWeightRecalculator and CaseBugWeightRecalculator
*********************************************************/
@isTest
private class BugWeightRecalculatorTest {
    public static List<Decimal> bugWeightsInEscalations;
    public static Support_Bug_Weight_Calculations__mdt bugWeightMdt = null;
    
    static testMethod void testUpdateBugWeightInsert() {
        
        bugWeightsInEscalations = new List<Decimal>();
    
        List<Support_Bug_Weight_Calculations__mdt> bugWeightMdts = [select 
            Case_Escalation_Proactive_Red__c, Case_Escalation_Proactive_Yellow__c, 
            Case_Escalation_Reactive_Red__c, Case_Escalation_Reactive_Yellow__c,
            Case_Base_Weight__c, Entitlement_Signature_Support_SLA__c, 
            Product_Saas_1__c, Product_Saas_Value__c, Severity_1__c, Priority_Urgent__c, 
            Case_Weight_Per_Associated_Case__c
            from Support_Bug_Weight_Calculations__mdt where MasterLabel = 'Master'];
        system.assertEquals(bugWeightMdts.Size(), 1);
        
        bugWeightMdt = bugWeightMdts[0];
        
        List<Account> accounts = new List<Account>();
        Account acc = new Account(Name = 'Test Account');
        Account acc2 = new Account(Name = 'Test Account');
        accounts.add(acc);
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        Contact c = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title');
        contacts.add(c);
        insert contacts;
        
        Bugs__c bug1 = new Bugs__c();
        insert bug1;

        Bugs__c bug2 = new Bugs__c();
        insert bug2;
        
        string saasProduct = bugWeightMdt.Product_Saas_1__c;
        List<string> saasProductSplit = saasProduct.split(',');
        if(saasProductSplit.size() > 1)
            saasProduct = saasProductSplit[1];
        
        SlaProcess signatureEntitlementProcess = [select Id,name from SlaProcess where name = 'Signature Support SLA' limit 1];
        
        Entitlement signatureEntitlement = new Entitlement(Name = 'Signature Support SLA', AccountId = acc.Id, SlaProcessId = signatureEntitlementProcess.Id, SLA_Process_Name__c = 'Signature Support SLA');
        insert signatureEntitlement;
        
        Case cs = new Case();
        cs.Account_Origin__c = acc.Id;
        cs.ContactId = cs.Id;
        cs.Status = 'New';
        cs.Subject = 'New';
        cs.Severity__c = '1';
        cs.OwnerId = UserInfo.getUserId();
        cs.Bug__c = bug1.Id;
        cs.Severity__c = '1'; //100
        cs.Priority = 'Urgent'; //50
        cs.Product__c = saasProduct; //70
        cs.EntitlementId = signatureEntitlement.Id; //60
        insert cs; //Baseline 230
        
        Qlikview_Support_Escalation__c esc = new Qlikview_Support_Escalation__c();
        esc.Case__c = cs.Id;
        esc.ContactName__c = c.Id;
        esc.Description__c = 'Reactive Red';
        esc.Business_Case__c = 'test';
        esc.Distribution_List__c = 'test';
        //Value 1000
        esc.Rapid_Response_Escalation__c = false; 
        esc.Temperature__c = 'Red';

        Qlikview_Support_Escalation__c esc2 = new Qlikview_Support_Escalation__c();
        esc2.Case__c = cs.Id;
        esc2.ContactName__c = c.Id;
        esc2.Description__c = 'Reactive Yellow';
        esc2.Business_Case__c = 'test';
        esc2.Distribution_List__c = 'test';
        //Value 600
        esc2.Rapid_Response_Escalation__c = false; 
        esc2.Temperature__c = 'Yellow';
        
        Test.startTest();
        
        //600
        TestCaseEscalation(esc2, 'insert');

        cs = [select id, Product__c, Bug_Weight_Case__c, Bug_Weight_Case_Escalation__c, Bug_Weight_Priority__c, 
            Bug_Weight_Saas__c, Bug_Weight_Severity__c, Bug_Weight_Signature__c, Entitlement.Name from Case where id = :cs.Id];
        
        Decimal expectedBugWeightCase = bugWeightMdt.Case_Base_Weight__c + 
            bugWeightMdt.Entitlement_Signature_Support_SLA__c + 
            bugWeightMdt.Product_Saas_Value__c + 
            bugWeightMdt.Severity_1__c + 
            bugWeightMdt.Priority_Urgent__c + 
            bugWeightMdt.Case_Escalation_Reactive_Yellow__c;
        
        system.debug('');
        system.debug('Case: ' + cs.Id);
        
        
        system.debug('SaasProduct. Case: ' + cs.Product__c + ', Data: ' + saasProduct);   
        system.debug('Entitlement. Case: ' + cs.Entitlement.Name+ ', Data: ' + signatureEntitlement.Name);
        system.debug('Bug weight Base. Metadata: ' + bugWeightMdt.Case_Base_Weight__c);
        system.debug('Bug weight Priority. Case: ' + cs.Bug_Weight_Priority__c + ', Metadata: ' + bugWeightMdt.Priority_Urgent__c);
        system.debug('Bug weight Severity. Case: ' + cs.Bug_Weight_Severity__c + ', Metadata: ' + bugWeightMdt.Severity_1__c);
        system.debug('Bug weight Saas. Case: ' + cs.Bug_Weight_Saas__c + ', Metadata: ' + bugWeightMdt.Product_Saas_Value__c);
        system.debug('Bug weight Case Escalation. Case: ' + cs.Bug_Weight_Case_Escalation__c + ', Metadata: ' + bugWeightMdt.Case_Escalation_Reactive_Yellow__c);
        system.debug('Bug weight Signature. Case: ' + cs.Bug_Weight_Signature__c + ', Metadata: ' + bugWeightMdt.Entitlement_Signature_Support_SLA__c);
        
        
        system.debug('Bug Weight On Case: ' +  cs.Bug_Weight_Case__c);
        system.debug('Expected Bug Weight: ' + expectedBugWeightCase);


        //Expected bug weight on the bug
        decimal expectedBugWeight = bugWeightMdt.Case_Weight_Per_Associated_Case__c * 1 + expectedBugWeightCase;



        bug1 = [select id, Bug_Weight_Case__c, Bug_Weight_Formula__c from Bugs__c where id = :bug1.Id];

        system.debug('Bug Weight from case on bug: ' +  bug1.Bug_Weight_Case__c);
        system.debug('Bug Weight On bug: ' +  bug1.Bug_Weight_Formula__c);
        system.debug('Expected Bug Weight: ' + expectedBugWeight);

        cs.Severity__c = '2';

        Semaphores.CustomCaseTriggerBeforeInsert = false;
        Semaphores.CustomCaseTriggerAfterInsert = false;
        Semaphores.CustomCaseTriggerBeforeUpdate = false;
        Semaphores.CustomCaseTriggerAfterUpdate = false;
        CaseBugWeightRecalculator.bugsToUpdate = new Map<Id, Bugs__c>();

        update cs;

        cs.Bug__c = bug2.Id;

        Semaphores.CustomCaseTriggerBeforeInsert = false;
        Semaphores.CustomCaseTriggerAfterInsert = false;
        Semaphores.CustomCaseTriggerBeforeUpdate = false;
        Semaphores.CustomCaseTriggerAfterUpdate = false;
        CaseBugWeightRecalculator.bugsToUpdate = new Map<Id, Bugs__c>();

        update cs;

        TestCaseEscalation(esc, 'insert');

        cs.Bug__c = null;

        Semaphores.CustomCaseTriggerBeforeInsert = false;
        Semaphores.CustomCaseTriggerAfterInsert = false;
        Semaphores.CustomCaseTriggerBeforeUpdate = false;
        Semaphores.CustomCaseTriggerAfterUpdate = false;
        CaseBugWeightRecalculator.bugsToUpdate = new Map<Id, Bugs__c>();

        update cs;

        

        Test.stopTest();
    }
    static testMethod void testUpdateBugWeightDelete() {
        
        bugWeightsInEscalations = new List<Decimal>();
    
        List<Support_Bug_Weight_Calculations__mdt> bugWeightMdts = [select 
            Case_Escalation_Proactive_Red__c, Case_Escalation_Proactive_Yellow__c, 
            Case_Escalation_Reactive_Red__c, Case_Escalation_Reactive_Yellow__c,
            Case_Base_Weight__c, Entitlement_Signature_Support_SLA__c, 
            Product_Saas_1__c, Product_Saas_Value__c, Severity_1__c, Priority_Urgent__c 
            from Support_Bug_Weight_Calculations__mdt where MasterLabel = 'Master'];
        system.assertEquals(bugWeightMdts.Size(), 1);
        
        bugWeightMdt = bugWeightMdts[0];
        
        List<Account> accounts = new List<Account>();
        Account acc = new Account(Name = 'Test Account');
        Account acc2 = new Account(Name = 'Test Account');
        accounts.add(acc);
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        Contact c = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title',
                Signature_Support_Access__c = true//60
                );
        contacts.add(c);
        insert contacts;
        
        Bugs__c bug1 = new Bugs__c();
        insert bug1;

        Bugs__c bug2 = new Bugs__c();
        insert bug2;
        
        string saasProduct = bugWeightMdt.Product_Saas_1__c;
        List<string> saasProductSplit = saasProduct.split(',');
        if(saasProductSplit.size() > 1)
            saasProduct = saasProductSplit[1];
        
        SlaProcess signatureEntitlementProcess = [select Id,name from SlaProcess where name = 'Signature Support SLA' limit 1];
        
        Entitlement signatureEntitlement = new Entitlement(Name = 'Signature Support SLA', AccountId = acc.Id, SlaProcessId = signatureEntitlementProcess.Id, SLA_Process_Name__c = 'Signature Support SLA');
        insert signatureEntitlement;
        
        Case cs = new Case();
        cs.Account_Origin__c = acc.Id;
        cs.ContactId = cs.Id;
        cs.Status = 'New';
        cs.Subject = 'New';
        cs.Severity__c = '1';
        cs.OwnerId = UserInfo.getUserId();
        cs.Bug__c = bug1.Id;
        cs.Severity__c = '1'; //100
        cs.Priority = 'Urgent'; //50
        cs.Product__c = saasProduct; //70
        cs.EntitlementId = signatureEntitlement.Id; 
        insert cs; //Baseline 230
        
        Qlikview_Support_Escalation__c esc = new Qlikview_Support_Escalation__c();
        esc.Case__c = cs.Id;
        esc.ContactName__c = c.Id;
        esc.Description__c = 'Reactive Red';
        esc.Business_Case__c = 'test';
        esc.Distribution_List__c = 'test';
        //Value 1000
        esc.Rapid_Response_Escalation__c = false; 
        esc.Temperature__c = 'Red';
        
        Test.startTest();
        
        //1000
        TestCaseEscalation(esc, 'insert');

        cs = [select id, Product__c, Bug_Weight_Case__c, Bug_Weight_Case_Escalation__c, Bug_Weight_Priority__c, 
            Bug_Weight_Saas__c, Bug_Weight_Severity__c, Bug_Weight_Signature__c, Entitlement.Name from Case where id = :cs.Id];
        
        Decimal expectedBugWeight = bugWeightMdt.Case_Base_Weight__c + 
            bugWeightMdt.Entitlement_Signature_Support_SLA__c + 
            bugWeightMdt.Product_Saas_Value__c + 
            bugWeightMdt.Severity_1__c + 
            bugWeightMdt.Priority_Urgent__c + 
            bugWeightMdt.Case_Escalation_Reactive_Red__c;
        
        system.debug('');
        system.debug('Case: ' + cs.Id);
        
        
        system.debug('SaasProduct. Case: ' + cs.Product__c + ', Data: ' + saasProduct);   
        system.debug('Entitlement. Case: ' + cs.Entitlement.Name+ ', Data: ' + signatureEntitlement.Name);
        system.debug('Bug weight Base. Metadata: ' + bugWeightMdt.Case_Base_Weight__c);
        system.debug('Bug weight Priority. Case: ' + cs.Bug_Weight_Priority__c + ', Metadata: ' + bugWeightMdt.Priority_Urgent__c);
        system.debug('Bug weight Severity. Case: ' + cs.Bug_Weight_Severity__c + ', Metadata: ' + bugWeightMdt.Severity_1__c);
        system.debug('Bug weight Saas. Case: ' + cs.Bug_Weight_Saas__c + ', Metadata: ' + bugWeightMdt.Product_Saas_Value__c);
        system.debug('Bug weight Case Escalation. Case: ' + cs.Bug_Weight_Case_Escalation__c + ', Metadata: ' + bugWeightMdt.Case_Escalation_Reactive_Red__c);
        system.debug('Bug weight Signature. Case: ' + cs.Bug_Weight_Signature__c + ', Metadata: ' + bugWeightMdt.Entitlement_Signature_Support_SLA__c);
        
        
        system.debug('Bug Weight On Case: ' +  cs.Bug_Weight_Case__c);
        system.debug('Expected Bug Weight: ' + expectedBugWeight);

        Semaphores.CustomCaseTriggerBeforeInsert = false;
        Semaphores.CustomCaseTriggerAfterInsert = false;
        Semaphores.CustomCaseTriggerBeforeUpdate = false;
        Semaphores.CustomCaseTriggerAfterUpdate = false;
        CaseBugWeightRecalculator.bugsToUpdate = new Map<Id, Bugs__c>();

        delete cs;

        Test.stopTest();
    }
    public static Decimal getBugWeight(Qlikview_Support_Escalation__c escalation){
        if(escalation.Rapid_Response_Escalation__c == true && escalation.Temperature__c == 'Yellow')
            return bugWeightMdt.Case_Escalation_Proactive_Yellow__c;
        else if(escalation.Rapid_Response_Escalation__c == true && escalation.Temperature__c == 'Red')
            return bugWeightMdt.Case_Escalation_Proactive_Red__c;
        else if(escalation.Rapid_Response_Escalation__c == false && escalation.Temperature__c == 'Yellow')
            return bugWeightMdt.Case_Escalation_Reactive_Yellow__c;
        else if(escalation.Rapid_Response_Escalation__c == false && escalation.Temperature__c == 'Red')
            return bugWeightMdt.Case_Escalation_Reactive_Red__c;
        else 
            return 0;
            
    }
    public static void TestCaseEscalation(Qlikview_Support_Escalation__c escalation, string operation){
        
        CaseBugWeightRecalculator.bugsToUpdate = new Map<Id, Bugs__c>();       
        Semaphores.CustomCaseTriggerBeforeInsert = false;
        Semaphores.CustomCaseTriggerAfterInsert = false;
        Semaphores.CustomCaseTriggerBeforeUpdate = false;
        Semaphores.CustomCaseTriggerAfterUpdate = false;
        
        if(operation == 'insert') {
            insert escalation;
            bugWeightsInEscalations.add(getBugWeight(escalation));
        }
        if(operation == 'update') {
            update escalation;
            bugWeightsInEscalations.add(getBugWeight(escalation));
        }
        else if(operation == 'delete'){
            bugWeightsInEscalations.remove(bugWeightsInEscalations.indexOf(getBugWeight(escalation)));
            delete escalation;
        }
        
        Case cs = [select id, Bug_Weight_Case_Escalation__c from Case where id = :escalation.Case__c];
        system.debug('');
        system.debug(escalation.Description__c);
        system.debug('Operation: ' + operation);
        system.debug('Bug_Weight_Case_Escalation__c: ' + cs.Bug_Weight_Case_Escalation__c);
        system.debug('getHighestBugWeight: ' + getHighestBugWeight());
    }
    public static Decimal getHighestBugWeight(){
        Decimal highestValue = 0;
        for(Decimal i : bugWeightsInEscalations){
            if(highestValue < i)
                highestValue = i;
        }
        return highestValue;
    }
}