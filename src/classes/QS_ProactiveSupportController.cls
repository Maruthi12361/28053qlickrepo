/************************************************************************
 * @author  AIN
 * 2016-04-11 AIN Initial development
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 *  
 * Controller for QS_ProactiveSupportController.page, used by Proactive Support as an anchor to their integrated solution
 *
 *************************************************************************/
public with sharing class QS_ProactiveSupportController {
    public User userRec {get; set;}
    public string SessionId {get; set;}
    public string AuthSessionId {get; set;}
	public boolean IsSandbox {get;set;}
	public string serverUrl {get;set;}

    public QS_ProactiveSupportController() {
		IsSandbox = [select Id, IsSandbox from Organization limit 1].IsSandbox;
        if(userinfo.getUserId() != null)
            userRec = [Select Id, Name, FirstName, LastName, Email, ContactId, Contact.AccountId, 
                            Contact.Persona__c, Contact.Name, Contact.Account.Name, Contact.Account.IsPartner, 
                            Contact.Account.IsCustomerPortal, Contact.Account.Partner_Support_Contact__c, 
                            Contact.Account.Responsible_Partner_count__c
                            From User 
                            where Id = :userinfo.getUserId() 
                            LIMIT 1];
        SessionId = UserInfo.getSessionId();
        AuthSessionId = '';
        if(userRec != null) {
            List<AuthSession> auth = [select Id from AuthSession where usersid = :userRec.Id order by CreatedDate desc limit 1];
            if(auth.Size() > 0)
                AuthSessionId = auth[0].Id;
        }
		serverUrl = (isSandbox?'https://tps-service1.qliktech.com/app/mimirApp/loader.js':'https://proactiveportal.qliktech.com/app/mimirApp/loader.js');
    }
}