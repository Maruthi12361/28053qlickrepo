/*********************************
* TaskSetCaseFeedHandlerTest
*    
* Change log:
*
* 2017-11-24	ext_vos	CR#CHG0030519	Initial Development for testing TaskSetCaseFeedHandler.
* 2017-12-13	ext_vos CR#CHG0032770	Updated for new handler logic: "is custom" Task creation.
**********************************/
@isTest
private class TaskSetCaseFeedHandlerTest {
	
	public static testMethod void testCaseFeed() {
        Account a = new Account(Name='TestAccount');
        insert a;
        Contact con = new Contact(LastName = 'testcontact', AccountId = a.Id);
		insert con;
		Test.startTest();
		Case c1 = new Case(Type = 'Incident', AccountId = a.id, ContactId = con.Id);
		insert c1;
		Case c2 = new Case(Type = 'Incident', AccountId = a.id, ContactId = con.Id);
		insert c2;
		Test.stopTest();

		Id idRTSupportTasks = Task.SObjectType.getDescribe().getRecordTypeInfosByName().get('PRM Tasks').getRecordTypeId(); 

		List<Task> tasks = new List<Task>();
		Task task1 = new Task(RecordTypeId = idRTSupportTasks, Subject = 'Call', CallType = 'Inbound', WhatId = c1.Id, Description = 'test comment for 1 case');
		tasks.add(task1);
		Task task2 = new Task(RecordTypeId = idRTSupportTasks, Subject = 'Call', CallType = 'Outbound', Description = 'test comment for 2 case');
		tasks.add(task2);
		insert tasks;	
		
		List<Task> items1 = [select Id, Description from Task where WhatId = :c1.Id and Is_Custom_Log_Call__c = true];
		System.assertEquals(1, items1.size());
		System.assert(items1[0].Description.contains('Inbound'));
		System.assert(items1[0].Description.contains('for 1 case'));

		Semaphores.TaskTriggerAfterUpdate = false;
		task1.Description = 'new for 1 case';
		update task1;

		items1 = [select Id, Description from Task where WhatId = :c1.Id and Is_Custom_Log_Call__c = true];
		System.assertEquals(2, items1.size());

		List<Task> items2 = [select Id from Task where WhatId = :c2.Id and Is_Custom_Log_Call__c = true];
		System.assertEquals(0, items2.size());

		Semaphores.TaskTriggerAfterUpdate = false;
		task2.WhatId = c2.Id;
		update task2;

		items2 = [select Id, Description from Task where WhatId = :c2.Id and Is_Custom_Log_Call__c = true];
		System.assertEquals(1, items2.size());
		System.assert(items2[0].Description.contains('Outbound'));
		System.assert(items2[0].Description.contains('for 2 case'));

		Semaphores.TaskTriggerAfterUpdate = false;
		task2.WhatId = c1.Id;
		update task2;
		
		items1 = [select Id, Description from Task where WhatId = :c1.Id and Is_Custom_Log_Call__c = true];
		System.assertEquals(3, items1.size());		

	}
}