/****************************************************************
*
*   UpdateClosedOppQuotesHandler
*   2017-09-11 AXW QCW-3635
*       If the parent opportuity is Closed Won and Quote status in not equal to 'Accepted by Customer' or 'Order Placed',
*            1) Update the status of the quote as 'Locked - Oppty Closed
*            2) If quote status is 'Waiting for approval', remove all approval records so that they don't sit in users queue for approval
*
*   2017-12-07 Anjuna Baby QCW-4486 Update forecastcatagory as omitted for DS opportunites on before update
*   2018-06-04 Linus Löfberg BSL-10 Adding setting of Bypass_Rules__c before closing Quotes.
*****************************************************************/


public class UpdateClosedOppQuotesHandler{

    public static void OnBeforeUpdate(List<Opportunity> triggerNew) {
        //fix for QCW-4486
        for(Opportunity opp : triggerNew) {
            if(opp.Split_Opportunity__c == true) {
                opp.ForecastCategoryName= 'Omitted';
            }
        }
    }

    // to be called on event after Update only
    public static Boolean OpportunityQuoteUpdate = false;

    public static void OnAfterUpdate(List<Opportunity> triggerNew) {
        if(OpportunityQuoteUpdate)
        return;

        List<Id> closedWonOpps = new List<Id>();
        List<SBQQ__Quote__c > quotesToUpdate = new List<SBQQ__Quote__c >();
        List<sbaa__Approval__c> approvalsToRevoke = new List<sbaa__Approval__c>();

        for(Opportunity opp : triggerNew) {
            if(opp.StageName.toLowerCase().contains('closed won')) {
                closedWonOpps.add(opp.Id);
                OpportunityQuoteUpdate = true;
            }
        }

        if(closedWonOpps.size() > 0) {
            for(SBQQ__Quote__c quoteRec : [SELECT Id, Bypass_Rules__c, SBQQ__Status__c, ApprovalStatus__c,
            (SELECT Id, sbaa__Status__c FROM Approvals__r)
            FROM SBQQ__Quote__c
            WHERE SBQQ__Status__c != 'Accepted by Customer'
            AND SBQQ__Status__c != 'Order Placed'
            AND SBQQ__Status__c != 'Locked - Oppty Closed'
            AND SBQQ__Opportunity2__c IN: closedWonOpps]) {

                if(quoteRec.SBQQ__Status__c == 'Waiting for Approval') {
                    quoteRec.ApprovalStatus__c = 'Revoked';
                    for(sbaa__Approval__c approvals : quoteRec.Approvals__r) {
                        if(approvals.sbaa__Status__c != 'Approved') {
                            approvals.sbaa__Status__c = 'Revoked';
                            approvalsToRevoke.add(approvals);
                        }
                    }
                }
                quoteRec.SBQQ__Status__c = 'Locked - Oppty Closed';
                quoteRec.Bypass_Rules__c = true;
                quotesToUpdate.add(quoteRec);
            }
        }

        if(approvalsToRevoke.size() > 0) {
            try{
                update approvalsToRevoke;
            }catch(Exception e) {
                system.debug('Update Approval Exception '+e);
            }
        }
        if(quotesToUpdate.size() > 0) {
            try{
                update quotesToUpdate;
            }catch(Exception e) {
                system.debug('Update Quote Exception '+e);
            }
        }
    }
}