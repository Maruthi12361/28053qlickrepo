/*
	This is a small extension controller used to inject a meta header
	forcing IE9/10 to not use compatible mode. Force.com is on a MS exclusion
	list which is forcing issues with HTML5. 
 */
public with sharing class IE10compatModeFixer {

    public IE10compatModeFixer(ApexPages.StandardController stdController) 
    {
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
    }
    
   static testMethod void Test_IE10compatModeFixer() 
   {
   		IE10compatModeFixer controller = new IE10compatModeFixer(new ApexPages.StandardController(new Account(name = 'Test')));
   } 

}