/**
 * Created by Viktor S. Yaremenko on 1/19/2018.
 *29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta
 */

@IsTest
private class AccountSetSupportOfficeHandlerTest {

    static testMethod void checkAccountSetSupportOfficeHandler() {

        Semaphores.setAllSemaphoresToTrue();

        //Populate the Custom setting that holds the list of users email addresses
        QTCustomSettings__c cs = new QTCustomSettings__c(
                Name = 'Default',
                null_Support_Office_Email__c = 'tester1@qlik.com.sandbox,tester2@qlik.com.sandbox'
        );
        insert cs;

        //Create some Support Offices
        List<Support_Office__c> soList = new List<Support_Office__c>{
                new Support_Office__c(Name = 'Sydney'),
                new Support_Office__c(Name = 'Lund'),
                new Support_Office__c(Name = 'Raleigh'),
                new Support_Office__c(Name = 'Tokyo'),
                new Support_Office__c(Name = 'Bangalore')
        };
        insert soList;
        Map<Id,Support_Office__c> testSOMap = new Map<Id,Support_Office__c>(soList);

        Subsidiary__c subs = new Subsidiary__c(
                Legal_Entity_Name__c = 'Legal_Entity_Name__c',
                Netsuite_Id__c = 'Netsuite_Id__c',
                Legal_Country__c = 'Sweden'
        );
        insert subs;
        //Create QlikTech Companies
        List<QlikTech_Company__c> qtComps = new List<QlikTech_Company__c>{
                new QlikTech_Company__c(
                        QlikTech_Company_Name__c = 'QlikTech Australia',
                        QlikTech_Geographic_Segment__c = 'APAC',
                        Name = 'AUS',
                        Country_Name__c = 'Australia',
                        QlikTech_Operating_Region__c = 'APAC',
                        Subsidiary__c = subs.Id
                ),
                new QlikTech_Company__c(
                        QlikTech_Company_Name__c = 'QlikTech France',
                        QlikTech_Geographic_Segment__c = 'EMEA',
                        Name = 'FRA',
                        Country_Name__c = 'FRANCE',
                        QlikTech_Operating_Region__c = 'EMEA',
                        Subsidiary__c = subs.Id
                ),
                new QlikTech_Company__c(
                        QlikTech_Company_Name__c = 'QlikTech Canada',
                        QlikTech_Geographic_Segment__c = 'AMERICAS',
                        Name = 'CAN',
                        Country_Name__c = 'Canada',
                        QlikTech_Operating_Region__c = 'AMERICAS',
                        Subsidiary__c = subs.Id
                ),
                new QlikTech_Company__c(
                        QlikTech_Company_Name__c = 'QlikTech JAPAN',
                        QlikTech_Geographic_Segment__c = 'APAC',
                        Name = 'JPN',
                        Country_Name__c = 'JAPAN',
                        QlikTech_Operating_Region__c = 'APAC',
                        Subsidiary__c = subs.Id
                ),
                new QlikTech_Company__c(
                        QlikTech_Company_Name__c = 'QlikTech India',
                        QlikTech_Geographic_Segment__c = 'APAC',
                        Name = 'IND',
                        Country_Name__c = 'INDIA',
                        QlikTech_Operating_Region__c = 'India',
                        Subsidiary__c = subs.Id
                )
        };
        insert qtComps;

        //Create and insert the Accounts
        List<Account> accList = new List<Account>{
                new Account(
                        Id = '001000000000001',
                        Name = 'Test Account for Sydney',
                        QlikTech_Company__c = 'QlikTech_Company__c',
                        Navision_Status__c = 'Customer',
                        Billing_Country_Code__c = qtComps[0].Id,
                        Support_Office__c = null
                ),
                new Account(
                        Id = '001000000000002',
                        Name = 'Test Account for Lund',
                        QlikTech_Company__c = 'QlikTech_Company__c',
                        Navision_Status__c = 'Customer',
                        Billing_Country_Code__c = qtComps[1].Id,
                        Support_Office__c = null
                ),
                new Account(
                        Id = '001000000000003',
                        Name = 'Test Account for Raleigh',
                        QlikTech_Company__c = 'QlikTech_Company__c',
                        Navision_Status__c = 'Customer',
                        Billing_Country_Code__c = qtComps[2].Id,
                        Support_Office__c = null
                ),
                new Account(
                        Id = '001000000000004',
                        Name = 'Test Account for Tokyo',
                        QlikTech_Company__c = 'QlikTech_Company__c',
                        Navision_Status__c = 'Customer',
                        Billing_Country_Code__c = qtComps[3].Id,
                        Support_Office__c = null
                ),
                new Account(
                        Id = '001000000000005',
                        Name = 'Test Account for Bangalore',
                        QlikTech_Company__c = 'QlikTech_Company__c',
                        Navision_Status__c = 'Customer',
                        Billing_Country_Code__c = qtComps[4].Id,
                        Support_Office__c = null
                )
        };

        AccountSetSupportOfficeHandler.onBeforeInsert(accList);

        system.assertEquals('Sydney', testSOMap.get(accList[0].Support_Office__c).Name); // Australia-Sydney
        system.assertEquals('Lund', testSOMap.get(accList[1].Support_Office__c).Name); // France-Lund
        system.assertEquals('Raleigh', testSOMap.get(accList[2].Support_Office__c).Name); // Canada - Raleigh
        system.assertEquals('Tokyo', testSOMap.get(accList[3].Support_Office__c).Name); // Japan - Tokyo
        system.assertEquals('Bangalore', testSOMap.get(accList[4].Support_Office__c).Name); // India - Bangalore

        Map<Id, Account> oldAccs = new Map<Id, Account>{
                '001000000000001' => accList[0]
        };
        List<Account> newAccList = new List<Account>{
                new Account(
                        Id = '001000000000001',
                        Name = 'Test Account for Sydney',
                        QlikTech_Company__c = 'QlikTech_Company__c',
                        Navision_Status__c = 'Customer',
                        Billing_Country_Code__c = qtComps[1].Id,
                        Support_Office__c = null
                )
        };

        AccountSetSupportOfficeHandler.onBeforeUpdate(newAccList, null, oldAccs);
        system.assertEquals('Lund', testSOMap.get(newAccList[0].Support_Office__c).Name); // France-Lund

        //We shouldn't test send messsages here. The code below added for code coverage only.
        AccountSetSupportOfficeHandler.sendErrorMails();
    }
}