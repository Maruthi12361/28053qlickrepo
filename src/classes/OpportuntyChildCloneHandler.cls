/****************************************************************
*
*  OpportuntyChildCloneHandler
*
*  2016-05-19 TJG : Migrated from the following trigger
					OpportuntyChildClone 
*  2017-06-22 MTM  QCW-2711 remove sharing option
*****************************************************************/
public class OpportuntyChildCloneHandler {
	// to be called on event after Update only
	public static void handle(List<Opportunity> triggerNew) {
	    if (
	    	!Semaphores.OpportuntyChildCloneHasRun && 
	    	!system.isBatch() && 
	    	!system.isFuture()
	    ) 
	    {
	        Semaphores.OpportuntyChildCloneHasRun = true;
	        Map<Id, Id> mapOppIds = new Map<Id, Id>();
	    
	        for(Opportunity opp : triggerNew)
	        {
	            if (
	            	opp.To_be_Cloned__c == false || 
	            	opp.Original_Opportunity__c == null
	            ) continue;
	            mapOppIds.put(opp.Id, opp.Original_Opportunity__c);
	        }

	        if (mapOppIds.isEmpty()) 
	        {
	            Semaphores.OpportuntyChildCloneHasRun = false;
	            return;
	        }
	                
	        try 
	        {
	            OpportunityCloneUtils.cloneChildren(mapOppIds);                   
	        } catch(Exception e) 
	        {
	            for (Opportunity opp: triggerNew) 
	            {
	                opp.addError(e);
	            }
	        }
	        Semaphores.OpportuntyChildCloneHasRun = false;
	    }		
	}
}