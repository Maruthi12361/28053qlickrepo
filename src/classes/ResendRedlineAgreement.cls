/*
    Initial development: MTM
    2014-12-05  MTM CR# 19375  ResendRedlineAgreement.
	2014-12-18  MTM CR# 20637  Do not sent any old agreements or agreements with closed opportunity ones
*/
global class ResendRedlineAgreement implements Database.Batchable<Redline_Manuel_Agreement__c>, Schedulable, Database.AllowsCallouts
{
    global ResendRedlineAgreement (){System.Debug('Starting: ResendRedlineAgreement');}
    
    Static String message = '';
    
    global Iterable<Redline_Manuel_Agreement__c> start(Database.BatchableContext BC) {        
        // Get all RedlineAgreement in progress
        System.Debug('Starting: ResendRedlineAgreement :Start collecting RedLine Agreements');
        List<Redline_Manuel_Agreement__c> agreements = new List<Redline_Manuel_Agreement__c>();
        
        datetime runTime = DateTime.Now();
        agreements = [SELECT Id,
                     	 Opportunity__c, 
                         Related_Quote__c, Status__c,                         
                         (SELECT Id FROM Attachments) 
                     FROM Redline_Manuel_Agreement__c 
                     WHERE Status__c = 'ERP Transfer in progress'
                     AND LastModifiedDate < :runtime.addMinutes(-30) 
                     AND LastModifiedDate > :runtime.addHours(-6)];
        System.Debug('RedLine Agreements count = ' + agreements.size());
        return agreements;                  
    }
    
    global void execute(Database.BatchableContext BC, List<Redline_Manuel_Agreement__c> scope) {
        
        System.Debug('Starting: execute Resend Redline Agreement ');
        List<Id> opptyId = new List<Id> ();
        for (Redline_Manuel_Agreement__c a: scope)
        {
            opptyId.add(a.Opportunity__c);
        }
        Map<Id, Opportunity> opps = new Map<Id, Opportunity>(
            						[SELECT Id, 
                                     		StageName, 
                                     		INT_NetSuite_InternalID__c, 
                                         	(SELECT Id, Quote_Id__c, INT_NetSuite_InternalID__c, Primary__c FROM NS_Quotes__r) 
                                    FROM Opportunity WHERE Id IN :opptyId]);
        
        System.Debug('opps count = ' + opps.size());
        
        for(Redline_Manuel_Agreement__c agreement: scope)
        {
            NS_Quote__c primaryQuote = GetPrimaryQuote(agreement.Opportunity__c, opps);
            SendToNS(agreement, primaryQuote);
        }
        
        System.Debug('Finishing: execute Resend Redline Agreement');    
    }        
    
    global Boolean SendToNS(Redline_Manuel_Agreement__c agreement, NS_Quote__c primaryQuote)
    {        
        try
        {   
            System.Debug('Starting SendToNS ' );
            if(agreement == null)
            {
                return false;
            }
            
            if(CanSendAgreement(agreement, primaryQuote))
            {                    
                SendAgreementByBoomi(agreement, primaryQuote);
                return true;            
            }
        }   
        catch(DmlException e){
            system.Debug('Error in RedlineQuoteHandler: ' + e);             
        }
        return false;        
    }  

    global Boolean SendAgreementByBoomi(Redline_Manuel_Agreement__c agreement, NS_Quote__c primaryQuote){
        /*'<Record type="Estimate" environment="' + orgId + '"> <ID>' + ns_quoteId + '</ID> 
        <secondaryID type="NS_Quote__c">' + quote_name + '</secondaryID> </Record>';*/

        if(agreement != null && primaryQuote != null)    {

        	String boomiMessage = '<?xml version="1.0"?><Record environment="' + UserInfo.getOrganizationId() + 
        	'" type="Estimate"><ID>' + primaryQuote.INT_NetSuite_InternalID__c + '</ID><secondaryID type="NS_Quote__c">' + primaryQuote.Quote_Id__c + 
        	'</secondaryID><secondaryID type="Redline_Manuel_Agreement__c">' + agreement.Id + '</secondaryID></Record>';

            System.debug('SendAgreementByBoomi agreement with message:' + boomiMessage);

            callBoomi(primaryQuote.Quote_Id__c, 'updateNSQuoteStatus', boomiMessage);

            System.debug('Boomi call finished');
            
            message = 'Sending the agreement to ERP... \n It can take few minutes to send this information.';
            return true;
        }
        message = 'The primaryQuote is null or the agreement is null.';
        return false;
    }
    //Helper functions
    global Boolean CanSendAgreement(Redline_Manuel_Agreement__c agreement, NS_Quote__c primaryQuote){    	
        if (primaryQuote == null)
        {
            message = 'There is no primary quote attached to this agreement '+ agreement.Id;
            return false;
        }    	
       
    	if(agreement.Related_Quote__c  != primaryQuote.Id){
    		message = 'Primary quote on the Opportunity is ' + primaryQuote.Id + ' now. Please create a new Redline agreement.';
    		return false;
    	}
               
    	if (agreement.Attachments == null) {
            message = 'No Documents are attached with this agreement to send! ' + + agreement.Id;
            return false;
        }
    	return true;
    }
    
    global NS_Quote__c GetPrimaryQuote(Id OppId, Map<Id, Opportunity> opps){
        System.Debug('opps count = ' + opps.size());
        Opportunity opp = opps.get(OppId);
        if(opp != null)
        {
            if(opp.StageName.contains('Closed'))
            {
                return null;
            }
            for (NS_Quote__c q : opp.NS_Quotes__r )
            {
                if (q.Primary__c == true)
                {
                    return q;
                }
            }
        }
        return null;            
    }
    
    public static void callBoomi(String objectId, String boomiMethodToCall, String boomiMessage) {
            QTCustomSettings__c custSettings = BoomiUtil.getCustomSettings();
            System.debug('custom settings ' + custSettings);
            if (custSettings == null || custSettings.BoomiBaseURL__c == null) {
                system.debug('Cannot call boomi, settings are missing');
                return;
            }
        
            String urlToCall = custSettings.BoomiBaseURL__c + boomiMethodToCall;

            system.Debug('Calling BOOMI  ' + urlToCall + ' at '+ Datetime.now() +' with Message :: ' + boomiMessage);
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(urlToCall);
            // HTTP headers, Content-Type and Digest Authorization.
            req.setHeader('Content-Type','text/xml');
            req.setHeader('Authorization',custSettings.BoomiToken__c); 
            //Set the boomiMessage as HTTPRequest body   
            req.setBody(boomiMessage);
            
            Http http = new Http();    
            try {
                //Execute web service call here     
                HTTPResponse res = http.send(req);  
 
                //Helpful debug messages
                System.debug(res.toString());
                System.debug('STATUS:' + res.getStatus());                  
                System.debug('STATUS_CODE:' + res.getStatusCode());
                
            } catch(System.CalloutException e) {
                    //Exception handling goes here....
                    System.debug('Error Calling BOOMI : ' + e);
            }
    }
    
    global void execute(SchedulableContext sc)
	{ 
		ID batchId = Database.executeBatch(new ResendRedlineAgreement()); 
	}
    
	global void finish(Database.BatchableContext BC) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email, 'mtm@qlikview.com'};
        
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex ResendRedlineAgreement status ' + a.Status);
        mail.setPlainTextBody
        ('The batch Apex job  ResendRedlineAgreement processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        try
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch (System.EmailException ex)
        {

        }

    } 
}
/*
 * To Schedule the job use
ResendRedlineAgreement m = new ResendRedlineAgreement();
String sch = '0 0 * * * ?';
String jobID = system.schedule('ResendRedlineAgreement Job', sch, m);

*/