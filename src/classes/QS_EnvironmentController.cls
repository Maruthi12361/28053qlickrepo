/*-----------------------------
*Author: MTM Initial development
* 2014-09-29 customer portal project
* Change log
* 2018-01-08  ext_vos CHG0032031: add the customm settings for "How to locate version" link.
* 2018-01-16  ext_vos CHG0031626: add condition for Licenses query for Partner users.
* 2018-01-18  ext_vos CHG0032673: update the calling of QS_Utility.HasULCLevel(..) method, namely to use the ContactId.
* 2019-03-18 AIN IT-1676 Fix for partner community project to not show all environments
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
* 2019-11-21 AIN IT-2320 Added filter expression for Coveo
 -----------------------------*/


global with sharing class QS_EnvironmentController {
    public Environment__c env {get; set;}
    public Boolean Flag = true; 
    public Boolean fromCaseDetail {get; set;}
    public Boolean fromEnvironmentListPage {get; set;}
    public boolean selectedAccountNameHasChanged {get; set;}
    private string v_selectedAccountName;
    public String AccountFilterText {get; set;}
    public String LicenseFilterText {get; set;}
    public static String selectedLicense {get; set;}
    public String errorMessageEnvInsert {get; set;}
    public List<String> errorMessageEnvInsertList {get; set;}
    // all Environments 
    private List<Environment__c> resultsAllEnvironment;
    //All test environtment
    List<Environment__c> TestEnvironment{ get; set; }
    //All development environtment
    List<Environment__c> DevEnvironment{ get; set; } 
    //All Production environtment
    List<Environment__c> ProdEnvironment{ get; set; } 
    public boolean showSelectionOption { get; set; }
    public boolean RenderExpanded {get; set;}
    public boolean RenderCreateBotton {get {return (RenderCreateTop && RenderExpanded) ;} set;} 
    public boolean RenderCreateTop {get; set;}
    public Id selectedEnvironment { get; set; }
    public List<EnvironmentProduct__c> environmentProductList;
    public QS_CaseWizard_Controller caseWizardRef {get; set;}
    public QS_CaseController caseDetailRef {get; set;}
    public List<SelectOption> accountOptionListFiltered {get; set;}
    public List<SelectOption> licenseOptionListFiltered {get; set;}
    public static List<Entitlement> licenseList;
    private transient List<SelectOption> v_licenseOptionList;
    private transient List<SelectOption> v_accountOptionList;
    public static String versionArticleLink {get; set;}
    public string CoveoFilterExpression { get; set; }

    public String selectedAccountName {
        get {
            if(v_selectedAccountName == null)
                v_selectedAccountName = '';
            return v_selectedAccountName;
        }
        set {
            selectedAccountNameHasChanged = true;
            v_selectedAccountName = value;
        }
    }

    private transient boolean v_TempCPVIEW;
    public boolean TempCPVIEW {
        get {
            if(v_TempCPVIEW == null)
                v_TempCPVIEW = QS_Utility.HasULCLevel(objUser.ContactId, 'CPVIEW');
            return v_TempCPVIEW;
        }
        set {
            v_TempCPVIEW = value;
        }
    }
    private transient boolean v_TempISCPLOG;
    public boolean TempISCPLOG {
        get {
            if(v_TempISCPLOG == null)
                v_TempISCPLOG = QS_Utility.HasULCLevel(objUser.ContactId, 'CPLOG');
            return v_TempISCPLOG;
        }
        set {
            v_TempISCPLOG = value;
        }
    }

    private transient User v_objUser;
    public User objUser {
        get {
            if(v_objUser == null)
                v_objUser = [Select id, name, accountid, ContactId, Contact.Account.ispartner From User  where Id = :userinfo.getUserId()  LIMIT 1];
            return v_objUser;
        }
        set {
            v_objUser = value;
        }
    }

    private transient boolean v_isIndirectCustomer;
    public boolean isIndirectCustomer {
        get {
            if(v_isIndirectCustomer == null)
                v_isIndirectCustomer = (!IsPartner && TempCPVIEW == true  && TempISCPLOG == false );
            return v_isIndirectCustomer;
        }
        set {
            v_isIndirectCustomer = value;
        }
    }
    private transient boolean v_isPartner;
    public boolean IsPartner {
        get {
            if(v_isPartner == null)
                v_isPartner = (objUser.Contact.Account.ispartner);
            return v_isPartner;
        }
        set {
            v_isPartner = value;
        }
    }
    public integer accountListSize {
        get {
            return accountList.Size();
        }
        set {
        }
    }
    public integer licenseListSize {
        get {
            if(licenseOptionList != null)
                return licenseOptionList.Size();
            else
                return -1;
        }
        set {
        }
    }

    private transient List<Account> v_accountList;
    public List<Account> accountList {
        get {
            if(v_accountList == null) {
                if(!IsPartner) {
                    Id accountId = objUser.AccountId;
                    v_accountList = [Select Id, Name, RecordtypeId, RecordType.Name  From Account 
                                    Where Id = :accountId LIMIT 1];
                }
                else {
                    Id accountId = objUser.AccountId;
                    List<Account> endUserAccounts = [select Id, Name, RecordtypeId, RecordType.Name from account where id in (select AccountId from entitlement where account_license__r.Support_Provided_By__c = :accountId and AccountId != :accountId) Order By Name, RecordType.Name];
                    List<Account> partnerAccount = [Select Id, Name, RecordtypeId, RecordType.Name
                                    From Account Where Id = :accountId LIMIT 1];
                    v_accountList = partnerAccount;
                    v_accountList.addAll(endUserAccounts);
                }
            }
            return v_accountList;
        }
        set {
            v_accountList = value;
        }
    }
    
    public List<SelectOption> accountOptionList {   
        get {
            if(v_accountOptionList == null){
                v_accountOptionList = new list<SelectOption>();
                for(Account acc : accountList) {
                    v_accountOptionList.add(new SelectOption(acc.Id, acc.Name));
                }
            }
            
            return v_accountOptionList;
        }
        set {
            v_accountOptionList = value;
        }
    }

    public List<SelectOption> licenseOptionList {
        get {
            system.debug('licenseOptionList get start');
            system.debug('selectedAccountName: ' + selectedAccountName);

            if(selectedAccountNameHasChanged || v_licenseOptionList == null) {
                list<selectOption> licenseListTemp = new list<selectOption>();
                if (selectedAccountName != null && selectedAccountName != ''){
                    //Entitlement_Check__c = TRUE and 
                    List<Entitlement> licenses = [select id, name, Legacy_License_Key__c, License_Reference__c, 
                        Full_Version__c, Account_License__r.product__c
                        from Entitlement 
                        where Account.Id = :selectedAccountName and 
                        (Account_License__r.Support_Provided_By__c =:MyAccountId or Account.Id =:MyAccountId) and
                        (EndDate >= TODAY or Entitlement_Check__c = true) and
                        RecordType.Name != 'Obsolete' 
                        Order by Name, Account.Name, Status];
            
                    if (licenses != null && licenses.size() > 0){
                        //licenseListTemp.add (  new SelectOption ( 'Please Select', '-- Please Select --' ) );
                        for ( Entitlement accLic : licenses ){
                            licenseListTemp.add ( new SelectOption ( accLic.id, accLic.name  ) );
                        }
                    } else {
                        //licenseListTemp.add (  new SelectOption ( 'None to Select', '-- None --' ) );
                    }
                } else {
                    //licenseListTemp.add (  new SelectOption ( 'None to Select', '-- None --' ) );
                }
                v_licenseOptionList = licenseListTemp;
                selectedAccountNameHasChanged = false;
            }
            system.debug('licenseOptionList get end');
            return v_licenseOptionList;
        }
         
        set {
            v_licenseOptionList = value;
        }
    }

    public QS_EnvironmentController() {
        errorMessageEnvInsertList = new List<string>();
        System.debug('Limits.getQueries(): ' + Limits.getQueries());
        selectedAccountNameHasChanged = false;
        
        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        CoveoFilterExpression  = partnerPortalURLs.Coveo_Filter_Expression__c;  

        system.debug('QS_EnvironmentController start');
        NewEnvironment();
        fromCaseDetail = false;
        fromEnvironmentListPage = false;
        errorMessageEnvInsert = null;

        if(ApexPages.currentPage().getURL() != null && ApexPages.currentPage().getURL().contains('QS_EnvironmentListPage'))
            fromEnvironmentListPage = true;
        else if(ApexPages.currentPage().getURL() != null && ApexPages.currentPage().getURL().contains('QS_CaseDetails'))
            fromCaseDetail = true;
        
        showSelectionOption = !fromEnvironmentListPage;
        refreshAccountList();
        system.debug('QS_EnvironmentController end');
        FilterLicenses();
    }

    public PageReference Create() {
        system.debug('Create start');
        try {  
            errorMessageEnvInsert = '';
            System.debug('%%%%selectedLicense: '+selectedLicense);
            System.debug('%%%%env: '+env);
            System.debug('%%%%environment: '+environment);
            System.debug('%%%%selectedAccountName: '+selectedAccountName);
            
            
            if(selectedLicense != null && selectedLicense != '' && !selectedLicense.contains('Select')) {
                env.Product_License__c = selectedLicense;
            } else {
                env.Product_License__c = null;
            }
            
            if(selectedAccountName != null && selectedAccountName != '' && !selectedAccountName.contains('Select')) {
                    env.Account__c = selectedAccountName;
            } else {
                env.Account__c = null;
            }
            
            System.debug('%%%%After selectedLicense: '+selectedLicense);

            if(env == null || env.Account__c == null || env.Product_License__c == null || env.Name == null || env.Name == '' || env.Product__c == null || env.Product__c == '' || env.Type__c == null || env.Type__c == '') {
                errorMessageEnvInsert = 'Please enter the following mandatory fields: <br />';
                errorMessageEnvInsert += (env == null || env.Name == null || env.Name == '') ? '<b> Name </b> <br />' : '';
                errorMessageEnvInsert += (env == null || env.Account__c == null) ? '<b> Account </b> <br />' : '';
                errorMessageEnvInsert += (env == null || env.Product_License__c == null) ? '<b> License </b> <br />' : '';
                errorMessageEnvInsert += (env == null || env.Product__c == null || env.Product__c == '') ? '<b> Product </b> <br />' : '';
                errorMessageEnvInsert += (env == null || env.Type__c == null || env.Type__c == '') ? '<b> Type </b> <br />' : '';

                errorMessageEnvInsertList = new List<string>();
                if(env == null || env.Name == null || env.Name == '')
                    errorMessageEnvInsertList.add('Name');
                if(env == null || env.Account__c == null)
                    errorMessageEnvInsertList.add('Account');
                if(env == null || env.Product_License__c == null)
                    errorMessageEnvInsertList.add('License');
                if(env == null || env.Product__c == null || env.Product__c == '')
                    errorMessageEnvInsertList.add('Product');
                if(env == null || env.Type__c == null || env.Type__c == '')
                    errorMessageEnvInsertList.add('Type');
                
                System.debug('%%%%errorMessageEnvInsert: '+errorMessageEnvInsert);
                
                return null;
            } else {
                insert env;
                errorMessageEnvInsert = '';
            }
            // if we insert a new environment from the Environment Page then we need to redirect to the enviroment detail page
            selectedEnvironment = env.Id;
            setEnviron();
            NewEnvironment();
            system.debug('Create end');
            return null;
        }
        catch(System.DMLException ex) {
            ApexPages.addMessages(ex);
            system.debug('Create end exception');
            return null;                                           
        }
    }
   
    private void NewEnvironment()
    {
        system.debug('NewEnvironment start');
        env = new Environment__c();
        environment = new Environment__c();
        RenderCreateTop = false;
        RenderExpanded = false;
        AccountFilterText = '';
        LicenseFilterText = '';
        system.debug('NewEnvironment end');
    }
    
    public Environment__c Environment {
        get { return env; }
        set;
    }

    public String MyAccName {
        get { 
            if(MyAccountId != null)
                return [Select Name from Account where Id =: MyAccountId].Name;
            else return '';
        }
        set;
    }
    private Id myAccountIdVar;
    public Id MyAccountId {
        get { 
            if(myAccountIdVar != null)
                return myAccountIdVar;

            Id contactId, accId;
            User usr = [Select contactid, id from User where id =: Userinfo.getUserid()];
            if(usr !=null) {
                contactId = usr.contactId;
                if(contactId != null)
                    accId  = [Select AccountId from Contact where id =: contactId].AccountId;
            }
            myAccountIdVar = accId;
            return accId;
        }
    }
    
    public List<Environment__c> GetEnvironments() {
        system.debug('~~~~~inside getEnvironments 1');
        
        List<Environment__c> environments = new List<Environment__c>();
        String queryEnvironment = '';
        system.debug('~~~~~inside getEnvironments myAccountId-' + myAccountId);
        
        queryEnvironment = 'SELECT e.Account__c, e.Virtual__c, e.CreatedDate, e.Version__c, e.Type__c, e.Product__c, e.Product_License__c, e.Product_License__r.Name, e.Operating_System__c, e.Name,' 
                                   + ' e.Description__c, e.Clustered__c, e.Architecture__c,  e.Account__r.Name, e.Last_Used_Date__c , e.Product_License__r.Legacy_License_Key__c '
                                   + ' From Environment__c e';
        //queryEnvironment += ' WHERE e.Product_License__r.Entitlement_Check__c = TRUE';
        queryEnvironment += ' WHERE (e.Product_License__r.EndDate >= TODAY or e.Product_License__r.Entitlement_Check__c = TRUE) ';

        if(!IsPartner)
            queryEnvironment += ' AND Account__c =: myAccountId';
        else
            queryEnvironment += ' AND (Product_License__r.account_license__r.Support_Provided_By__c =: myAccountId or Account__c =: myAccountId)';
        
        if(caseWizardRef != null || caseDetailRef != null)
            queryEnvironment += ' ORDER BY Last_Used_Date__c DESC NULLS Last';
        
        environments = (List<Environment__c>) Database.query(queryEnvironment);
        system.debug('~~~~~inside getEnvironments environments.size-' + environments.size());
        
        //save in global var to be used in Test & Production func
        //resultsAllEnvironment = (List<Environment__c>) Database.query(queryEnvironment); removed linesh
        resultsAllEnvironment = environments;
        
        //All Production environment
        ProdEnvironment= new List<Environment__c>();
        
        for (Environment__c env : resultsAllEnvironment ) {
            if(env.Type__c=='Production') {
                ProdEnvironment.add(env);
                system.debug('env.name: ' + env.name);
            }
        }
        system.debug('~~~~~inside getEnvironments ProdEnvironment-' + ProdEnvironment.size());
        return ProdEnvironment;
    }

    public List<Environment__c> getresultsTestEnvironment() {
        System.debug('Test Environment populated');
        TestEnvironment= new List<Environment__c>();
        for (Environment__c env : resultsAllEnvironment )
            if(env.Type__c=='Test')
                TestEnvironment.add(env);
        return TestEnvironment;
    }
    
    public List<Environment__c> getresultsDevEnvironment() {
        System.debug('Dev Environment populated');
        DevEnvironment= new List<Environment__c>();
        for (Environment__c env : resultsAllEnvironment )
            if(env.Type__c=='Development')
                DevEnvironment.add(env);
        return DevEnvironment;
    }
    
    public void setEnviron() {   
        system.debug('setEnviron start');

        if(caseWizardRef != null) {
            caseWizardRef.environmentControllerRef = this;
            caseWizardRef.selectedEnvironement = selectedEnvironment;
            caseWizardRef.selectedEnvironementRetrieve();
            if(selectedEnvironment != null)
                getEnvironmentProductList();
        }
        if(caseDetailRef != null) {
            caseDetailRef.environmentControllerRef = this;
            caseDetailRef.selectedEnvironement = selectedEnvironment;
            caseDetailRef.selectedEnvironementRetrieve();
            if(selectedEnvironment != null)
                getEnvironmentProductList();
        }
        system.debug('setEnviron start');
    }
    
    public List<EnvironmentProduct__c> getEnvironmentProductList() {
        environmentProductList = [Select Id, Name, License__c, License__r.Name, Environment__c From EnvironmentProduct__c where Environment__c = :selectedEnvironment ORDER BY Name];
        return environmentProductList;
    }
    
    public String EnvironmentName {
        get { return env.Name; } 
        set;          
    }
    
    public String Accounts {       
        get;
        set{Environment.Account__c = value;}
    }
    
    public String Entitlements {
        get;
        set { Environment.Product_License__c = value; }
    }
   
    public void DeleteEnvironment() {
        if(selectedEnvironment !=null) {
            try {
                Environment__c selectedEnv = [SELECT Id from Environment__c where Id =:selectedEnvironment limit 1];
                System.debug('Env to delete = ' + selectedEnv.Id);
                
                ApexPages.Message infoMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Environment selected' + selectedEnv.Id);
                ApexPages.addMessage(infoMessage);
                delete selectedEnv;
            }
            catch(DmlException e) {
                ApexPages.addMessages(e);
            }
        }
        else {
            ApexPages.Message infoMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Environment not selected for deletion');
            ApexPages.addMessage(infoMessage);
        }
    }
    
    
    public void CreateNewClicked() {
        RenderCreateTop ^= true;
        //accountList = [Select Id, Name, RecordtypeId, RecordType.Name From Account Order By Name, RecordType.Name];
        //licenseList = [Select Id, Name, Account.Name, Account.Id, Status 
                            //From Entitlement  
                            //where Entitlement_Check__c = TRUE 
                                    //and RecordType.Name != 'Obsolete'
                            //Order By Name, Account.Name, Status LIMIT 999];
        errorMessageEnvInsert = null;
        //set version help link
        QS_Help_Links__c link = QS_Help_Links__c.getInstance('Version Help Article');
        versionArticleLink = (link != null && link.Link__c != null) ? link.Link__c : '';
    }

    @RemoteAction
    global static List<Entitlement> getLicenseList() {
        //Entitlement_Check__c = TRUE 
        if(licenseList == null || licenseList.isEmpty())
            licenseList = [Select Id, Name, Account.Name, Account.Id, Status 
                                From Entitlement  
                                where EndDate >= TODAY 
                                    and RecordType.Name != 'Obsolete'
                                Order By Name, Account.Name, Status];
        return licenseList;
    }

    public void refreshAccountList() {
        system.debug('refreshAccountList start');
        FilterAccounts();
        system.debug('refreshAccountList end');
        
    }
    
    public void FilterAccounts() {
        system.debug('FilterAccounts start');
        
        Id contactId, accId = null, Acc;
        string Accounttype ='';
        User usr = [Select contactid from User where id =: Userinfo.getUserid()];
        if(usr !=null) {
            contactId = usr.contactId;
            List<Contact> contacts = new List<Contact>();
            contacts = [Select AccountId, Account_Type__c from Contact where id =: contactId];
            if(contactId != null && contacts.Size() > 0) {
                Contact contacttype = contacts[0];
                accId = contacttype.AccountId;
                if(contacttype.Account_Type__c != null)
                    Accounttype = contacttype.Account_Type__c ;     
            }
        }
                
        if(Flag) {
            Flag = false;

            if(Accounttype.contains('Partner')) {
                Acc = ApexPages.currentPage().getParameters().get('accountId');
                if(Acc != null) {
                    List<Account> AccsList = [select id, name, IsPartner, IsCustomerPortal from Account where Id = :Acc limit 1];
                    Account Acclist = null;
                    if(AccsList.size() > 0){
                        Acclist= [select id, name, IsPartner, IsCustomerPortal from Account where Id = :Acc limit 1];
                        AccountFilterText = Acclist.name;
                        selectedAccountName = Acclist.Id;
                    }
                    
                }  
            } else {
               if(Accounttype.contains('Customer')) {
                    List<Account> AccsList = [select id, name from Account where Id = :accId limit 1];
                    Account Acclist = null;
                    if(AccsList.size() > 0){
                        Acclist= [select id, name from Account where Id = :accId limit 1];
                        system.debug('URLID Acclist.name ===='+Acclist.name);
                        AccountFilterText = Acclist.name;
                        selectedAccountName = Acclist.Id;
                    }
                }
            }
        }
        List<SelectOption> newFilteredList = new List<SelectOption>();
        string filterText = AccountFilterText.toLowerCase();

        if(accountOptionList != null)
            for(SelectOption so : accountOptionList)
                if(so.getLabel().toLowerCase().contains(filterText))
                    newFilteredList.Add(so);

        accountOptionListFiltered = new List<SelectOption>();
        
        
        system.debug('newFilteredList.Size(): ' + newFilteredList.Size());
        if(newFilteredList.size() > 100) {
            accountOptionListFiltered.add(new SelectOption('','Too many accounts, please use the filter above ('+newFilteredList.Size()+')'));        
        }
        else if(newFilteredList.size() > 0) {
            if(newFilteredList.size() > 1)
                accountOptionListFiltered.add(new SelectOption('','Please select a value ('+newFilteredList.Size()+')'));
            accountOptionListFiltered.addAll(newFilteredList);
        }
        else
            accountOptionListFiltered.add(new SelectOption('','No accounts found, please use the filter to find an account'));        

        if(selectedAccountName == '' && accountOptionListFiltered.size() == 1)
            selectedAccountName = accountOptionListFiltered[0].getValue();
     
        FilterLicenses();
        system.debug('FilterAccounts end');
    }

    public void FilterLicenses() {
        system.debug('FilterLicenses start');
        //if(acc.Name.contains(AccountFilterText))
        //newFilteredList.add(new SelectOption(acc.Id, acc.Name));
        system.debug('licenseOptionList.Size(): ' + licenseOptionList.Size());
        List<SelectOption> newFilteredList = new List<SelectOption>();
        string filterText = LicenseFilterText.toLowerCase();
        
        for(SelectOption so : licenseOptionList)
            if(so.getLabel().Contains(filterText))
                newFilteredList.Add(so);
        system.debug('newFilteredList.Size(): ' + newFilteredList.Size());
        system.debug('LicenseFilterText: ' + LicenseFilterText);
        licenseOptionListFiltered = new List<SelectOption>();
        
        if(newFilteredList.size() > 100)
            licenseOptionListFiltered.add(new SelectOption('','Too many licenses, please use the filter above ('+newFilteredList.Size()+')'));        
        else if(newFilteredList.size() > 0) {
            if(newFilteredList.size() > 1)
                licenseOptionListFiltered.add(new SelectOption('','Please select a value ('+newFilteredList.Size()+')'));
            licenseOptionListFiltered.addAll(newFilteredList);
        }
        else if(selectedAccountName == '') {
            licenseOptionListFiltered.add(new SelectOption('','Please select an account above'));
        }
        else
            licenseOptionListFiltered.add(new SelectOption('','No licenses found, please use the filter to find an account'));
        system.debug('FilterLicenses end');
    }

        
    public void ExpandClicked() {
        RenderExpanded ^= true;
    }

    public void  refreshLicenseList(){
        system.debug('refreshLicenseList start');
        FilterLicenses();
        system.debug('refreshLicenseList end');
    }
    
    public PageReference cancelNewEnvironment() {
        system.debug('cancelNewEnvironment start');
        RenderExpanded = false;
        RenderCreateTop = false;
        //accountList = null; 

        licenseList = null;
        selectedAccountName = null;
        selectedLicense = null;
        errorMessageEnvInsert = null;
        NewEnvironment();
        system.debug('cancelNewEnvironment end');
        return null;
    }
    
}