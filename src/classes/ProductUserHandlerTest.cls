/********************************************************
* CLASS: ProductUserHandlerTest
* DESCRIPTION: Test class ProductUserHandler

* CHANGELOG:    
*   2019-11-28 - BAD - Added Initial logic
*********************************************************/
@isTest
public class ProductUserHandlerTest {

    @testSetup
    static void testSetup() {}

    @isTest
    static void test_connect_Subscription_pos() {
        ProductUserHandler tempObj = new ProductUserHandler();

        Zuora__Subscription__c testSubsctiption = new Zuora__Subscription__c();
        testSubsctiption.name = 'A-TestSub1';
        testSubsctiption.Zuora__External_Id__c = 'A-TestSub1';
        insert testSubsctiption;
        System.assertNotEquals(null, testSubsctiption.Id);

        Product_User__c testPU = new Product_User__c();
        testPU.Zuora_Subscription_ID__c = 'A-TestSub1';
        testPU.User_Role__c = 'Service Account Owner';
        testPU.Status__c = 'Active';
        insert testPU;
        System.assertNotEquals(null, testPu.Id);

        Test.startTest();
        testPU = [SELECT Id, Name, Zuora_Subscription_ID__c, Subscription__c FROM Product_User__c WHERE Id = : testPU.Id];
        System.assertEquals(testSubsctiption.Id, testPU.Subscription__c);
        Test.stopTest();
    }

}