/*******************************************************************************
*  Custom controller class for the Live_Agent_Sales visualforce page
*  BSL-349 2018-05-09 - MTM - Sales post chat survey
*  IT-1499(CHG0035405) - 31-01-2019 - ext_vos - Add "Save Chat" button.
*  IT-1897 - 29.05.2019 - extbad  Update 'Close Chat Survey Id'
*  IT-2124 - 03.09.2019 extciz update survey Link
*******************************************************************************/
public class LiveAgentSalesCtrl {
    public static final String CHAT_NUMBER_LABEL = 'If you need to reference the chat in the future, your chat number is: ';
    public static final String CHAT_TEXT_SEPARATOR = '\r\n\r\n';

    public Id TranscriptId { 
        get { 
            return ChatTranscript != null?  ChatTranscript.Id :  null; 
        } 
    }
    public String sUserId {get; set;}
    public LiveChatTranscript ChatTranscript { get; set; }
    public boolean isSandbox {get;set;}
    public string caseNumber {get;set;}
    public string surveyId {get;set;}
    public string caseId {get;set;}
    public string surveyLink{get;set;}

    public LiveAgentSalesCtrl() {
        isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        QTCustomSettingsHier__c qsCSettingsHier = QTCustomSettingsHier__c.getInstance();
        surveyId = isSandbox ? qsCSettingsHier.Close_Chat_Survey_Id_QA__c : qsCSettingsHier.Close_Chat_Survey_Id__c;
        surveyLink = isSandbox ? qsCSettingsHier.Survey_Link_For_Live_Chat_QA__c : qsCSettingsHier.Survey_Link_For_Live_Chat__c;

        String chatkey = ApexPages.currentPage().getParameters().get('chatkey');       
        if (String.isNotEmpty(chatkey)) {
            List<LiveChatTranscript> transcripts = [SELECT Id, Name FROM LiveChatTranscript WHERE ChatKey =: chatkey];
            if (transcripts != null && transcripts.size() > 0) {
                ChatTranscript = transcripts[0];
            } else {
                ChatTranscript = null;
            }
        }       
        String cd = ApexPages.currentPage().getParameters().get('chatDetails');
        if (cd != null) {
            JSONParser parser = JSON.createParser(cd);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'userid')) {
                    parser.nextToken();
                    sUserId = parser.getText();
                    break;
                }
            }
        }
        caseId = '';
        String attachedRecords = ApexPages.currentPage().getParameters().get('attachedRecords');
        if (attachedRecords != null) {
            JSONParser parser = JSON.createParser(attachedRecords);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'CaseId')) {
                    parser.nextToken();
                    caseId = parser.getText();
                    break;
                }
            }
            if (caseId != '') {
                List<Case> cases = [select id, CaseNumber from Case where id = :caseId limit 1];
                if (cases.size() > 0) {
                    caseNumber = cases[0].CaseNumber;
                }
            }
        }        
    }

    public PageReference saveTranscript() {
        String t = ApexPages.currentPage().getParameters().get('transcript');
        String ck = ApexPages.currentPage().getParameters().get('chatkey');
        if (t != null) {
            Live_Agent_Transcript__c latNew = new Live_Agent_Transcript__c();
            latNew.Name = ck;
            latNew.Transcript__c = CHAT_NUMBER_LABEL + caseNumber + CHAT_TEXT_SEPARATOR + t;
            insert latNew;
        } 
        return null;
    }

}