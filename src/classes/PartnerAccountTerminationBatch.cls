/**     * File Name: PartnerAccountTerminationBatch
        * Description :
        * @author : Linus Löfberg
        * Modification Log ===============================================================
        Ver     Date         Author         Modification
        1.0     2018-01-31   ext_lfb        Class initiated.
*/
global class PartnerAccountTerminationBatch implements Database.Batchable<sObject> {

    Set<Id> partnerAccountIdSet;

    global PartnerAccountTerminationBatch(Set<Id> inputSet) {
        partnerAccountIdSet = inputSet;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, IsPartner FROM Account WHERE Id IN :partnerAccountIdSet]);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {
        for (Account acc : scope) {
            acc.IsPartner = false;
        }

        Database.SaveResult[] results = Database.update(scope, false);

        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                // Success!!
                System.debug('Successfully de-activated Partner Account. Account ID: ' + sr.getId());
            } else {
                // Operation failed.
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC) {

    }

}