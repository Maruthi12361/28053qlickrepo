public class CasePickupSetContExt
{
	//Variables	
	//Standard Set Controller
    private final ApexPages.StandardSetController ssc = null;    
    //List of Cases which are supposed to be picked up and then updated
    public List<Case> pickupCases = null;
    
    public CasePickupSetContExt(ApexPages.StandardSetController ssc)
    {
        this.ssc = ssc;
        //Show error message if no Cases have not been selected
        if(this.ssc.getSelected().size() == 0)
        {
        	areCasesSelected = false;
        	System.debug('***** No Cases selected *****' );
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No cases have been selected. Please click "Back" and select at least one case using the checkboxes in the leftmost column.'));
        }    
        else
        {
        	areCasesSelected = true;
        	getSelected();
        }	
    }
    
    //Properties
    public Boolean areCasesSelected {get; set;}
    
    //Method which processes selected Cases and changes the Status to 'In Process' and assigns the logged in User as the Owner for the Case
    private void getSelected()
    {
    	Id caseOwnerId = UserInfo.getUserId();
    	
    	//Hierarchical custom setting which houses the default Case status as 'In Process'
		Customizations__c custs = Customizations__c.getOrgDefaults();
		
		//Null Check for the Org Wide Custom Setting value
		if(custs == null)
		{
			String errorMsg = 'Error retrieving Customization Config custom settings. Please ensure Default Organization Level Values have been configured correctly.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
            return;
		}
		
		//Loop over the selected Cases and change their Status and the reassign current logged in User as the Owner of the Case
    	for(Integer i=0;i<this.ssc.getSelected().size();i++)
    	{
    		Case c = (Case) this.ssc.getSelected()[i];
    		System.debug('***** Current Selected Case: ' + c + '*****');
    		c.Status = custs.Pickup_Case_Status__c;
    		c.OwnerId = caseOwnerId;
    		System.debug('***** Current Selected Case after field changes: ' + c + '*****');
    		if(pickupCases == null)
    		{
    			pickupCases = new List<Case>();
    		}
    		pickupCases.add(c);  
    		System.debug('***** Pickedup Case: ' + pickupCases.get(i) + '*****');  		
    	}
    	System.debug('***** Pickedup Cases: ' + pickupCases + '*****');
    }
    
    //Method called from the VF Page action attribute. This updates the selected Case through a DML statement
    public PageReference updateCasesForPickup()
    {	
    	System.debug('***** Updating Pickedup Cases: ' + pickupCases + '*****');
    	if(pickupCases != null)
    	{
    		try
    		{
    			update pickupCases;
    		}
    		catch(DmlException dmle)
    		{
				for(Integer i=0; i < dmle.getNumDml(); i++)
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, dmle.getDmlMessage(i)));
                return null;
    		}		    		
    	}   
    	return this.ssc.cancel(); 		
    }
}