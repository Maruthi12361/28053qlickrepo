/********
* NAME :UpdateExistingAccountLicensesTest
* Description: Tests for UpdateExistingAccountLicensesclass
* 
*
*Change Log:
    IRN      2016-01-22   CR# 63693 Created test class
    Pramod Kumar V 12-12-2017 Increase Coverage
    
******/
@isTest
private class UpdateExistingAccountLicensesTest {

    private static Account_License__c accountItemLicense;

    private static NS_Support_Contract__c createMockNSSupportContract(List<Account> acc){
        NS_Support_Contract__c ns = new NS_Support_Contract__c(Name='test', End_User__c = acc[0].Id, Reseller__c = acc[1].Id);
        insert ns;
        
        accountItemLicense = new Account_License__c( Account__c = acc[1].Id);
        insert accountItemLicense;

        NS_Support_Contract_Item__c item = new NS_Support_Contract_Item__c(NS_Support_Contract__c =ns.Id, Contract_Item_Account_License__c = accountItemLicense.Id);
        insert item;

        return ns;
    }

    private static testMethod void TestUpdatingExistingAccountLicenses(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        List<NS_Support_Contract__c> contracts = new List<NS_Support_Contract__c>();
        List<Account> acc = new List<Account>();
        for(Integer i = 0; i<2; i++){
            Account pAccount = new Account(Name = 'partneracc' + i);
            acc.add(pAccount);  
        }
        Insert acc;
        NS_Support_Contract__c c = createMockNSSupportContract(acc);
        contracts.add(c);
        
        UpdateExistingAccountLicenses u = new UpdateExistingAccountLicenses();
        List<NS_Support_Contract_Item__c> items = [Select Contract_Item_Account_License__c, NS_Support_Contract__c from NS_Support_Contract_Item__c where Contract_Item_Account_License__c!= null and NS_Support_Contract__c != null];
        u.execute(null, items);
        //pramod
       
        DataBase.executeBatch(u);


        List<Account_License__c> licenses = [Select id, Support_Provided_By__c, Selling_Partner__c from Account_License__c where Id = :accountItemLicense.Id];
        System.assertEquals(licenses[0].Selling_Partner__c, acc[1].Id);
        test.stopTest();
    }

}