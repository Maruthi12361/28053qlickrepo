/**
*
* extbad    2019-09-17 IT-2037 Send an email to the 'Submitted_by'
* extcqb    2020-03-06 IT-2537 Select article version by article id
*/
global with sharing class ArticleFeedbackHelper {
    public static final String EMAIL_TEMPLATE_NAME = 'Support_Article_Feedback_Response';
    public static final String RESULT_SUCCESS = 'Feedback to submitter has now been sent via email.';
    public static final String RESULT_ARTICLE_NOT_PUBLISHED = 'The referenced Article is not published.';

    webService static String sendFeedbackEmail(String caseId) {
        Case cs = [
                SELECT Id, Subject, Feedback_to_submitter_Actions_taken__c, Submitted_by__c, Submitted_by__r.Email,
                        Knowledge_Article__c
                FROM Case
                WHERE Id = :caseId
        ];

        List<EmailTemplate> templates = [
                SELECT Id, Name, HtmlValue, Body, Subject
                FROM EmailTemplate
                WHERE DeveloperName = :EMAIL_TEMPLATE_NAME
                LIMIT 1
        ];

        if (!templates.isEmpty()) {
            EmailTemplate template = templates.get(0);

            String articleNumber;
            String articleTitle;
            try {
                KnowledgeArticleVersion knowledgeArticleVersion = [
                        SELECT Id, ArticleNumber, Title
                        FROM KnowledgeArticleVersion
                        WHERE PublishStatus = 'Online'
                        AND IsLatestVersion = TRUE
                        AND KnowledgeArticleId = :cs.Knowledge_Article__c
                ];
                articleNumber = knowledgeArticleVersion.ArticleNumber;
                articleTitle = knowledgeArticleVersion.Title;
            } catch (QueryException ex) {
                return RESULT_ARTICLE_NOT_PUBLISHED;
            }

            // Here build the single test email message
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {'invalid@emailaddr.test'});
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setTemplateId(template.Id);

            //The email template will be merged with the brandtemplate (letterhead) after sending. But because of the rollback it will not be sent
            Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            Database.rollback(sp);

            // Use htmlbody merged with letterhead to replace placeholders
            String body = mail.getHtmlBody().replace('CASE_ACTIONS_TAKEN',
                    cs.Feedback_to_submitter_Actions_taken__c != null ? cs.Feedback_to_submitter_Actions_taken__c : '');
            body = body.replaceAll('ARTICLE_TITLE', articleTitle);
            QS_Partner_Portal_Urls__c partnerUrls = QS_Partner_Portal_Urls__c.getInstance();
            String portalUrl = partnerUrls.Support_Portal_Login_Url__c;
            body = body.replaceAll('ARTICLE_LINK', portalUrl != null ? portalUrl + '/articles/' + articleNumber : '');
            body = body.replaceAll('DID_YOU_KNOW', '<b><font size="3" style="color: rgb(0, 152,69);">Did you know?</font></b>');

            String subj = template.Subject.replace('ARTICLE_TITLE', articleTitle);

            Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();

            String noReplyAddress = '';
            if (QTCustomSettings__c.getInstance('Default') != null) {
                noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
            }
            List<OrgWideEmailAddress> oweaList = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :noReplyAddress];
            if (oweaList.size() > 0) {
                emailMessage.setOrgWideEmailAddressId(oweaList.get(0).Id);
            }

            emailMessage.setToAddresses(new String[] {cs.Submitted_by__r.Email});
            emailMessage.setHtmlBody(body);
            emailMessage.setSubject(subj);
            emailMessage.setWhatId(caseId);
            try {
                List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailMessage});
                for (Messaging.SendEmailResult res : sendres) {
                    System.debug('Result: ' + res);
                }
            } catch (System.EmailException ex) {
                return 'Error when sending mail: ' + ex.getMessage();
            }
        }
        return RESULT_SUCCESS;
    }
}