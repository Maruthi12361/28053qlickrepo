/**
 *	Description		:	This class is helper for Trigger_OpportunityTeamMember.
 *
 *	Created By		:	ext_nup
 *
 *	Created Date	:	12/13/2017
 *
 *	Version			:	V_1.0
 *
 *	Revision Log	:	V_1.0 - Created
**/ 
public without sharing class OpportunityTeamMemberTriggerHelper {
	
	//Constants
	static final String OPPORTUNITY_TEAM_MEMBER_ROLE_EXECUTIVE_SPONSER = 'Executive Sponsor';
	
	/** This method is used to populate Execute Sponser on Opportunity if a Team Member is added with the Role = 'Executive Sponser' 
	 * 	In case of multiple executive sponser , they will be concatenate by comma (,) **/
	 
    public static void populateExecutiveSponsor(List<OpportunityTeamMember> newOppTeamMembers , Map<Id, OpportunityTeamMember> oldOppTeamMembers) {
	
		//Map of Opportunuty to be updated 
		Map<Id , Opportunity> mapOppToBeUpdated = new Map<Id , Opportunity>();
		
		//Set of opportunity ids 
		Set<Id> setOppIds = new Set<Id>();
		    	
    	//Loop over Trigger.New [insert /update]
    	if(newOppTeamMembers != null) {
	    	for(OpportunityTeamMember oTeamMember : newOppTeamMembers) {
	    		
	    		//Checking for conditions
	    		if(String.isNotBlank(oTeamMember.TeamMemberRole ) && oTeamMember.TeamMemberRole == OPPORTUNITY_TEAM_MEMBER_ROLE_EXECUTIVE_SPONSER &&
	    			(oldOppTeamMembers == null  || (oldOppTeamMembers.get(oTeamMember.Id).TeamMemberRole != oTeamMember.TeamMemberRole
	    											|| oldOppTeamMembers.get(oTeamMember.Id).UserId != oTeamMember.UserId))) {
	    			//Populating 		    			
	    			setOppIds.add(oTeamMember.OpportunityId);
	    		}else if(oldOppTeamMembers == null  || (oldOppTeamMembers.get(oTeamMember.Id).TeamMemberRole == OPPORTUNITY_TEAM_MEMBER_ROLE_EXECUTIVE_SPONSER)) {
	    			//Populating 		    			
	    			setOppIds.add(oTeamMember.OpportunityId);
	    		}
	    	}
    	}else { //Delete case 
    		
    		//Loop over old ones 
    		for(OpportunityTeamMember oldTeamMember : oldOppTeamMembers.values()) {
    			
    			//Checking for conditions
    			if(String.isNotBlank(oldTeamMember.TeamMemberRole ) && oldTeamMember.TeamMemberRole == OPPORTUNITY_TEAM_MEMBER_ROLE_EXECUTIVE_SPONSER) {
    				
    				//Populating 		    			
    				setOppIds.add(oldTeamMember.OpportunityId);
    			}
    		}    		
    	}	
    	
    	//checking for set size
    	if(setOppIds.size() > 0) {
    		
    		List<OpportunityTeamMember> opTemMembers = [select Id, Name, OpportunityId , UserId, TeamMemberRole FROM OpportunityTeamMember where OpportunityId IN : setOppIds
    													AND  TeamMemberRole =: OPPORTUNITY_TEAM_MEMBER_ROLE_EXECUTIVE_SPONSER];
    													
    		for(OpportunityTeamMember oTeamMember : opTemMembers) {
    			
    			//Populating map
    			if(mapOppToBeUpdated.containsKey(oTeamMember.OpportunityId))
    				mapOppToBeUpdated.get(oTeamMember.OpportunityId).Executive_Sponsor__c += ', ' + oTeamMember.Name;
    			else
    				mapOppToBeUpdated.put(oTeamMember.OpportunityId , new Opportunity(Id = oTeamMember.OpportunityId , Executive_Sponsor__c = oTeamMember.Name));
    		}
    		
    		/** If no any executive sponser remaining then make that fie;d blank **/
			//Loop over set
			for(Id strOppId : setOppIds) {
				
				//checking for containsKey
				if(!mapOppToBeUpdated.containsKey(strOppId)) {
					
					mapOppToBeUpdated.put(strOppId , new Opportunity(Id = strOppId , Executive_Sponsor__c = ''));
				}
			}
    	}    	
    	
    	
    	//Checking for map size and update opportunities
    	if(mapOppToBeUpdated.size() > 0)
    		update mapOppToBeUpdated.values();
    }
}