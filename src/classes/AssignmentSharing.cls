/******************************************************

    Class: AssignmentSharing
    Assignment Sharing
	Assignment record will be shared with a partner (subcontractor) when partner(subcontractor) 
	is the “Resourse” assigned to the Assignment

    Changelog:
        2015-12-01  TJG     Created file
                            
******************************************************/

public with sharing class AssignmentSharing {
    public static Boolean UpdateAssignmentSharing(List<Id> asmIds)
    {
        if (asmIds.size() == 0)
        {
            return true;
        }

    	List<Id> resIds = new List<Id>();
    	List<pse__Assignment__c> assignments = [SELECT Id, pse__Resource__c FROM pse__Assignment__c WHERE pse__Resource__c != null AND Id IN :asmIds];
    	Map<Id, Id> asm2Contact = new Map<Id, Id>();
    	for (pse__Assignment__c assignm : assignments)
    	{
    		asmIds.add(assignm.Id);
    		if (assignm.pse__Resource__c != null)
    		{
    			resIds.add(assignm.pse__Resource__c);
    			asm2Contact.put(assignm.Id, assignm.pse__Resource__c);
    		}
    	}

    	List<Contact> resList = [select Id, AccountId from contact where Id in :resIds];

    	Set<Id> uniqueAccIds = new Set<Id>();
    	Map<Id, Id> ct2Account = new Map<Id, Id>();
    	for (Contact ct : resList)
    	{
    		if (ct.AccountId != null && !uniqueAccIds.contains(ct.AccountId))
    		{
    			uniqueAccIds.add(ct.AccountId);
    			ct2Account.put(ct.Id, ct.AccountId);
    		}
    	}

    	Boolean noNewShares = false;
        List<pse__Assignment__Share> newAssignmentShares = new List<pse__Assignment__Share>();

        // If no UniqueAccounts found then no new shares needed
        if (uniqueAccIds.size() == 0)
        {
            System.debug('UpdateAssignmentSharing: No unique accounts found.');            
            noNewShares = true;
        }
        else
        {
	        // Fetch all the PortalRoles
	        Map<Id, Id> accountToRole = new Map<Id,Id>();
	        Set<Id> uniquePortalRoles = new Set<Id>();
	        List<Id> portalRoleIds = new List<Id>();

	        List<UserRole> userRoles = ApexSharingRules.GetListOfUserRolesByPortalAccountIds(uniqueAccIds, 'Partner', 'Executive');
	        
	        for (UserRole usrRole :userRoles)
	        {
	            if (!uniquePortalRoles.contains(usrRole.Id))
	            {
	                uniquePortalRoles.add(usrRole.Id);
	                portalRoleIds.add(usrRole.Id);
	            }
	            accountToRole.put(usrRole.PortalAccountId, usrRole.Id);
	        }

	        System.debug('UpdateAssignmentSharing: uniqueAccIds=' + uniqueAccIds + ', uniquePortalRoles=' + uniquePortalRoles + '###### userRoles=' + userRoles);

	        if (uniquePortalRoles.size() == 0)
	        {            
	            System.debug('UpdateAssignmentSharing: No unique PortalRoles found.');
	            noNewShares = true;
	        }

	        Map<Id,Id> roleToGroup = new Map<Id,Id>();

	        List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(portalRoleIds, 'RoleAndSubordinates');
	        for (Group grp : groups)
	        {
	            roleToGroup.put(grp.RelatedId, grp.Id);
	        }

	        if (roleToGroup.size() == 0)
	        {
	            System.debug('UpdateAssignmentSharing: No unique Groups found.');            
	            noNewShares = true;
	        }

	        if (!noNewShares)
	        {
		        Map<Id, Id> assignmentToResPartner = new Map<Id, Id>();
		        for (Id key : asm2Contact.keySet())
		        {
		        	Id key2 = asm2Contact.get(key);
		        	if (ct2Account.containsKey(key2))
		        	{
		        		assignmentToResPartner.put(key, ct2Account.get(key2));
		        	}	
		        }

		        for (pse__Assignment__c assignm : assignments)
		        {
		            if (assignmentToResPartner.containsKey(assignm.Id))
		            {
		                Id stPartner = assignmentToResPartner.get(assignm.Id);
		                
		                System.debug('stPartner=' + stPartner);
		                System.debug('accountToRole=' + accountToRole);

		                if (accountToRole.containsKey(stPartner))
		                {
		                    Id stPartnerRole = accountToRole.get(stPartner);
		                    if (roleToGroup.containsKey(stPartnerRole))
		                    {
		                        newAssignmentShares.add(createNewAssignmentShare(assignm.Id, 'Edit', roleToGroup.get(stPartnerRole)));
		                    }
		                }
		            }
		        }
		    }
	    }

        // retrieving existing Manual Opportunity Sharing
        List<pse__Assignment__Share> asmShares = [select Id,ParentId, UserOrGroupId, AccessLevel from pse__Assignment__Share 
                    where ParentId in :asmIds and RowCause = 'Manual'];

        // filter out those need to be deleted
        List<pse__Assignment__Share> toDel = new List<pse__Assignment__Share>();
        for (pse__Assignment__Share oldShare : asmShares)
        {
            Boolean matched = false;
            for (integer i=0; i<newAssignmentShares.size(); i++)
            {
                if (oldShare.AccessLevel == newAssignmentShares[i].AccessLevel &&
                    oldShare.ParentId == newAssignmentShares[i].ParentId &&
                    oldShare.UserOrGroupId == newAssignmentShares[i].UserOrGroupId )
                {
                    matched = true;
                    // already in place, no need to insert again
                    newAssignmentShares.remove(i);
                }
            }
            if (!matched)
            {
                toDel.add(oldShare);
            }
        } 
        // delete those sharing that are no longer valid
        if (toDel.size() > 0)
        {
            Delete toDel;
        }

        // Insert sharing if there is the need
        if (newAssignmentShares.size() > 0)
        {
            Insert newAssignmentShares;
        }   	

    	return true;
    }

    public static pse__Assignment__Share createNewAssignmentShare(Id asmId, string accessLevel, string usrOrGroupId)
    {
        pse__Assignment__Share asmShare = new pse__Assignment__Share(
        		ParentId = asmId,
        		AccessLevel = accessLevel,
        		UserOrGroupId = usrOrGroupId
        	);

        return asmShare;
    }    	
}