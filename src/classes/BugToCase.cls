/*********************************************
Change Log:

2020-05-05 AIN IT-2776 moved from trigger to apex class when creating trigger framework on Bug
********************************************/
public with sharing class BugToCase{


    public static void BugToCase(List<Bugs__c> bugs) {
        Map<Id, Id> mapBugCase = new Map<Id, Id>();
        List<Case> updateCases = new List<Case>();
        List<Bugs__c> updateBugs = new List<Bugs__c>();


        for(Bugs__c newBugRec : bugs) {
            //If there is a case on the bug record, add it to the map for 2 reasons
            //To use the case id in the key and update the case with the bug in the value
            //Use the values to update the bugs and set the field LookupForPublisherActionToCase__c to null 
            //so the trigger doesn't update the same record twice.
            if(newBugRec.LookupForPublisherActionToCase__c != null) {
                // Adding to Map Case id, Bug Id
                mapBugCase.put(newBugRec.LookupForPublisherActionToCase__c, newBugRec.Id);
            }
        }

        if(mapBugCase.KeySet() != null && (!mapBugCase.KeySet().isEmpty())) {
            //Query the cases that have the IDs found on the bug records
            for(Case caseObj : [Select Bug__c, Id from Case where (Id in : mapBugCase.KeySet())]) {
                //Make sure the id is in the keyset
                if(mapBugCase.keySet().contains(caseObj.Id)) {
                    //Check if the field needs to be updated
                    Id bugId = mapBugCase.get(caseObj.Id);
                    if(bugId != caseObj.Bug__c) {
                        caseObj.Bug__c = bugId;
                        updateCases.add(caseObj);
                    }
                    else
                        system.debug('Bug is already assigned to the correct case');
                }
            }
            //Only update if there are changed records
            if(updateCases != null && (!updateCases.isEmpty())) {
                try {
                    // Update case Records
                    update updateCases;
                }
                catch(System.Exception excep) {
                    System.debug('Case Update Error'+ excep.getMessage());
                    trigger.new[0].addError('Case Update Error');
                }
            }
        }
    }
}