/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 * LOG Changes:
 *
 * xxxx-xx-xx   XXX: Initial code
 * 2012-02-15   RDZ: Adding to the test that Sphere of Influence is a mandatory value before creating a quote or adding products
 * 2012-06-14   CCE: Added the test Account so that I can set Legal_Approval_Status__c = 'Legal Approval Granted'.
 *                   This is because this test class was failing when I tried to deploy Must_be_Closed_Won_to_be_Finance_Approve
 *                   for eCustoms.
 * 2013-07-10   SLH: Set From_Lead_Conversion__c to false in the test Opportunity to avoid OP010_Cant_Create_4_EndUserAc_PendingVal validation rule
 * 2013-09-11   MHG: Changed user selection to be dynamic
 * 2013-06-09   TJG: Fix test error: System.DmlException: Update failed. First exception on row 0 with id 006f0000005hV0FAAU; 
 *                   first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Closed Won can only be set by system.
 * 2015-10-27   IRN  Winter 16 Issue - called globalsetup and setupNsPriceBook - created a mock contact instead of reading from the db
 */
@isTest
private class OppLockingTestClass {
/*
    static testMethod void myUnitTest() {
		
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');

        //User user = [select id from User where alias='tpy'];
        User userApi = [select id from User where IsActive = true and Profile__c = 'Custom: Api Only User' LIMIT 1];           // 2014-06-09
        User userStd = [select id from User where IsActive = true and Profile__c = 'Custom: QlikBuy Sales Std User' LIMIT 1];      // 2014-06-09
        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];  


        ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
        

            //Create test Account
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc'           
            );
            QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
            insert QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account();
            testAcnt.name='Test1 Account1';
            testAcnt.OwnerId = userStd.Id;
            testAcnt.Navision_Customer_Number__c = '12345';
            testAcnt.Legal_Approval_Status__c = 'Legal Approval Granted';           
            
            testAcnt.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;         
            testAcnt.Billing_Country_Code__c = QTComp.Id;
            testAcnt.BillingStreet = '417';
            testAcnt.BillingState ='CA';
            testAcnt.BillingPostalCode = '94104';
            testAcnt.BillingCountry = 'United States';
            testAcnt.BillingCity = 'SanFrancisco';
            
            testAcnt.Shipping_Country_Code__c = QTComp.Id;
            testAcnt.ShippingStreet = '417';
            testAcnt.ShippingState ='CA';
            testAcnt.ShippingPostalCode = '94104';
            testAcnt.ShippingCountry = 'United States';
            testAcnt.ShippingCity = 'SanFrancisco';
            testAcnt.From_Lead_Conversion__c = false;
            insert testAcnt;
            
            //Create test Contact
            Contact testCon = new Contact(FirstName='Test1', LastName='Contact1', AccountId=testAcnt.Id, OwnerId=userStd.Id);
            insert testCon;
            test.startTest();
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delte me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                ForecastCategoryName = 'Omitted',   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
                RecordTypeId = '01220000000DNwY',
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                AccountId = testCon.AccountId
            );                               
            
            insert testNewOpp;
            test.stopTest();
            ////RDZ In order to add quoutes or products to opp they need to have a Sphere of Influence. Validation Rule
            ////select Contact__c, Opportunity__c from Sphere_of_Influence__c
            ////6 a0v20000000Gq4bAAC  Champion    false   SOI0000832  0032000000IvcyxAAB  006200000085uECAAY
            //Sphere_of_Influence__c SoI = new Sphere_of_Influence__c(
            //    Opportunity__c = testNewOpp.Id,
            //    Contact__c = '0032000000IvcyxAAB', 
            //    Role__c = 'Champion'
            //);
            //
            //insert SoI;
            
            OpportunityLineItem LineItem = new OpportunityLineItem();
            
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;
            
            insert LineItem;        
        System.RunAs(userApi)
        {                    
            testNewOpp.Short_Description__c = 'TestOpp - Del';
            testNewOpp.StageName = 'Closed Won';
            testNewOpp.Finance_Approved__c = true;        
            
            
            
            update testNewOpp;
        }

        System.RunAs(userStd)
        {    
            boolean GotException = false;

            System.debug('Running as QlikBuy Sales Std User');
            try 
            {
                update testNewOpp;
            }
            catch(Exception ex)
            {
                System.debug('Got Exception on update of delete testNewOpp - ' + ex.getMessage());
                GotException = true;
            }
            System.assertEquals(false, GotException);
        }
		

        
        //boolean GotException = false;
        //try 
        //{
        //    delete testNewOpp;
        //}
        //catch(Exception ex)
        //{
        //    System.debug('Got Exception on update of Delete');
        //    GotException = true;
        //}
        //System.assertEquals(true, GotException);
        
        
    }
    
    
    static testMethod void myUnitTest2() {  
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        
                
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        Account acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
        Contact testCon = QTTestUtils.createMockContact(acct.Id);
        test.startTest();
        Opportunity testNewOpp = New Opportunity (
            Short_Description__c = 'TestOpp Again',
            Name = 'TestOpp Again',
            Type = 'New Customer',
            Revenue_Type__c = 'Direct',
            CloseDate = Date.today(),
            StageName = 'Alignment Meeting',
            ForecastCategoryName = 'Omitted',   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
            RecordTypeId = '01220000000DNwY',
            AccountId = testCon.AccountId,
            Signature_Type__c = 'Digital Signature',
            CurrencyIsoCode = 'GBP'
        );

        insert testNewOpp;
         test.stopTest();        
        
        ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
        
        
       // System.assertEquals(1, Opportunities.size());
       // testNewOpp = Opportunities.get(0);
        
        OpportunityLineItem LineItem = new OpportunityLineItem();
        LineItem.OpportunityId = testNewOpp.Id;
        LineItem.UnitPrice = 10;
        LineItem.Quantity = 2;
        LineItem.PricebookEntryId = PB;

        System.debug(LineItem);
        
        insert LineItem;
                
        
    }
*/
}