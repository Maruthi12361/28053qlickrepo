/**
 * 2014-03-04	TJG CR# 11205 https://eu1.salesforce.com/a0CD000000fbNNU
 *				If Sell Through Partner auto-added check Partner Bonus if applicable
 *              when opportunity is created or updated the sell through partner field will be
 *              populated according to Partner Contact
 * 2014-06-04   TJG Fix test error on QTNS
 *              System.DmlException: Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY,
 *              Record Type ID: this ID value isn't valid for the user: 01220000000J1KRAA0: [RecordTypeId]
 */
@isTest
private class TestSellThroughPartnerValue {

    static testMethod void CheckSellThroughPartnerExists() {
        //Id OppRecordTypeId = '01220000000J1KR'; //Qlikbuy CCS Standard Commented out on 2014-06-04 on QTNS
        Id OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id;
		Id AccRecordTypeId_EndUserAccount = '01220000000DOFu';
		Test.startTest();
        QTCustomSettings__c setting = new QTCustomSettings__c(
            Name = 'Default',
            eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012f0000000Cugl',
            OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
        Insert setting;
        QuoteTestHelper.createCustomSettings();
        Account TestAccount = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Sell Qlik Test Ltd.',
                Navision_Status__c = 'Partner',
                QlikTech_Company__c = 'QlikTech Inc'
            );
        insert TestAccount;

        Contact TestContact = new Contact (
                AccountId = TestAccount.Id,
                OwnerId = UserInfo.getUserId(),
                LastName = 'Wilson-Hemingway',
                FirstName = 'Daryl',
                Email = 'dwy@qlikview.com'
            );

        insert TestContact;

       Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id
		);
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account acc = new Account(
            Name = 'Test Sell Through 1',
            RecordTypeId = AccRecordTypeId_EndUserAccount,
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Legal_Approval_Notes__c = 'Some legal stuff',
            Billing_Country_Code__c = QTComp.Id);
        insert acc;

        Opportunity Opp = new Opportunity();
        Opp.RecordTypeId = OppRecordTypeId;
        Opp.Name = 'Test sell through partner';
        Opp.CurrencyIsoCode = 'USD';
        Opp.Short_Description__c = 'Partner Bonus Test';
        Opp.StageName = 'Goal Identified';
        Opp.CloseDate = Date.today();
        Opp.Type = 'New Customer';
        Opp.Revenue_Type__c = 'Reseller';
        Opp.ForecastCategoryName = 'Omitted';
        Opp.AccountId = acc.Id; // '0012000000I7SMR';
        Opp.Partner_Contact__c = TestContact.Id; // '0032000000IvzWqAAJ';
        insert Opp;

        Opp = [Select Id, Sell_Through_Partner__c from Opportunity where Id = :Opp.Id];
        System.debug('TestSellThroughPartnerValue: Opp=' + Opp);
        System.assertNotEquals(null, Opp.Sell_Through_Partner__c);
        Test.stopTest();
    }
}