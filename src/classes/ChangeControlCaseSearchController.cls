/*
ChangeLog
    2013-08-13  CCE Change to test class as Simon McAllister is on holiday (a validation rule has had a name
                    change from Simon to Josefine so we need to use her ID) 
    2013-09-12  SAN CR#9570 to undo previous CCE CR#9280 Change to test class as Simon McAllister is on holiday (a validation rule has had a name
                    change from Simon to Josefine so we need to use her ID) 
*/
public with sharing class ChangeControlCaseSearchController 
{

    public string CaseId = '';  
    private Case PrivateCase = null;
    public SLX__Change_Control__c Change { get; set; } 
    

    private String soql
    {
        get;
        set;
    }

    public List<SLX__Change_Control__c> Changes {
        get;
        set;
    }

    public ChangeControlCaseSearchController()
    {
        if (System.currentPagereference().getParameters().containsKey('CaseId'))
        {
            CaseId = System.currentPagereference().getParameters().get('CaseId');           
        }
        Change = new SLX__Change_Control__c();
    }
     
    public String sortDir {
        get  
        { 
            if (sortDir == null) 
            {  
                sortDir = 'asc'; 
            } 
            return sortDir;  
        }
        set;
    }
 
    public String sortField {
        get
        {
            if (sortField == null) 
            {
                sortField = 'CR_Number2__c'; 
            } 
            return sortField;  
        }
        set;
    }   
    
    public void toggleSort() 
    {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';    
        runQuery();
    }   
    
  
    public String debugSoql 
    {
        get 
        { 
            return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; 
        }
        set;
    }   
    
    public Case ThisCase
    {
        get 
        {
            if (PrivateCase == null && CaseId != null)
            {
                PrivateCase = [select Id, Subject, Bug_ID__c, Problem_Case2__c, CaseNumber from Case where Id = :CaseId Limit 1];
                PrivateCase.Problem_Case2__c = null;
            }
        
            return PrivateCase;
        }
        
        set
        {
            PrivateCase = ThisCase;
        }
    }
    
    public string getCaseId()
    {
        Case C = ThisCase;
        
        if (C != null)
        {
            return C.Id;
        }
        
        return null;
    }
    
    public void runQuery()
    {
        try 
        {
            Changes = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
        } 
        catch (Exception e) 
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Search failed: ' + e.getMessage()));
        }
    }   
        
    public PageReference runSearch() {
     
        String CRNumber = Apexpages.currentPage().getParameters().get('CRNumber');
        String Name = Apexpages.currentPage().getParameters().get('Name');
        //String BugID = Apexpages.currentPage().getParameters().get('BugID');
             
        soql = 'select Id, CR_Number2__c, Name from SLX__Change_Control__c where Id != null';
        if (!CRNumber.equals(''))
            soql += ' and CR_Number2__c LIKE \'%'+String.escapeSingleQuotes(CRNumber)+'%\'';
        if (!Name.equals(''))
            soql += ' and Name LIKE \'%'+String.escapeSingleQuotes(Name)+'%\'';
 
        runQuery();
     
        return null;
    }   
    
    public PageReference backToParentCase() {
        return new ApexPages.StandardController(ThisCase).view();
    }
     
    public PageReference relateChange() {
        
        if (Change.Related_to_Change_Request_Project__c == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No change to relate'));
            return null;    
        }
        
        try
        {
            // Fetch the related object
            //Case RelatedCase = [select Id, Problem_Case2__c from Case where Id = :ThisCase.Problem_Case2__c LIMIT 1];
            SLX__Change_Control__c RelatedChange = [select Id, Problem_Case_Nr__c from SLX__Change_Control__c where Id = :Change.Related_to_Change_Request_Project__c LIMIT 1];
            if (RelatedChange == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No change to relate'));
                return null;                
            }
            
            RelatedChange.Problem_Case_Nr__c = ThisCase.Id;
            update RelatedChange;
            
            Change.Related_to_Change_Request_Project__c = null;
        }
        catch (Exception Ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while relating change: ' + Ex.getMessage()));
            return null;            
        }
        return null;    
    }   


}