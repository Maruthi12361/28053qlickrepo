/**
* Change log:
*
*   2019-05-22 ext_vos	CHG0036145: Update Recent_Timecard_End_Date__c field for active parent Project.
*   2019-07-11 ext-ciz CHG0036145: Changed update Recent_Timecard_End_Date__c field for active parent Project.
*   
*/
public class PseTimecardHeaderTriggerHandler {
    private static final String APPROVED_STATUS = 'Approved';

    public static void onAfterInsert(Map<Id, pse__Timecard_Header__c> newItems) {
        reCalculateRecentEndDate(newItems, null);
    }

    public static void onAfterUpdate(Map<Id, pse__Timecard_Header__c> newItems) {
        reCalculateRecentEndDate(newItems, null);
    }

    public static void onAfterDelete(Map<Id, pse__Timecard_Header__c> oldItems) {
        reCalculateRecentEndDate(null, oldItems);
    }

    // CHG0036145
    private static void reCalculateRecentEndDate(Map<Id, pse__Timecard_Header__c> newCards, Map<Id, pse__Timecard_Header__c> oldCards) {
        List<Id> projectIds = new List<Id>();
        if (newCards != null) {
            for (Id cardId : newCards.keySet()) {
                pse__Timecard_Header__c newCard = newCards.get(cardId);
                projectIds.add(newCard.pse__Project__c);
            }
        }
        if (oldCards != null) {
            for (Id cardId : oldCards.keySet()) {
                pse__Timecard_Header__c oldCard = oldCards.get(cardId);
                projectIds.add(oldCard.pse__Project__c);
            }
        }
        // need recalculate Recent_Timecard_End_Date__c field for active Projects
        updateRecentTimecardEndDateForProject(projectIds);
    }

    // is used in Batch job too
    public static void updateRecentTimecardEndDateForProject(List<Id> projectIds) {
        List<pse__Proj__c> toUpdate = new List<pse__Proj__c>();
        List<pse__Timecard_Header__c> childCards = new List<pse__Timecard_Header__c>();

        if (!projectIds.isEmpty()) {
            List<pse__Proj__c> projects = [
                    select Id, Recent_Timecard_End_Date__c, (
                            select Id, pse__End_Date__c, pse__Start_Date__c, pse__Total_Hours__c, pse__Sunday_Hours__c, pse__Saturday_Hours__c,
                                    pse__Friday_Hours__c, pse__Thursday_Hours__c,pse__Wednesday_Hours__c, pse__Tuesday_Hours__c, pse__Monday_Hours__c
                            from pse__Timecards__r
                            where pse__Status__c = :APPROVED_STATUS
                            order by pse__End_Date__c DESC
                    )
                    from pse__Proj__c
                    where Id in:projectIds
            ];
            for (pse__Proj__c pr : projects) {
                if (!pr.pse__Timecards__r.isEmpty()) {
                    childCards = pr.pse__Timecards__r;
                    for (pse__Timecard_Header__c timeCard : childCards) {
                        if (!childCards.isEmpty() && timeCard.pse__Start_Date__c != null && timeCard.pse__Total_Hours__c != 0) {
                            Map<Integer, Decimal> daysOfWeek = new Map<Integer, Decimal>();
                            daysOfWeek.put(6, timeCard.pse__Saturday_Hours__c);
                            daysOfWeek.put(5, timeCard.pse__Friday_Hours__c);
                            daysOfWeek.put(4, timeCard.pse__Thursday_Hours__c);
                            daysOfWeek.put(3, timeCard.pse__Wednesday_Hours__c);
                            daysOfWeek.put(2, timeCard.pse__Tuesday_Hours__c);
                            daysOfWeek.put(1, timeCard.pse__Monday_Hours__c);
                            daysOfWeek.put(0, timeCard.pse__Sunday_Hours__c);
                            for (Integer i = 6; i >= 0; i--) {
                                if (daysOfWeek.get(i) != null && daysOfWeek.get(i) > 0) {
                                    pr.Recent_Timecard_End_Date__c = timeCard.pse__Start_Date__c.addDays(i);
                                    break;
                                }
                            }
                            toUpdate.add(pr);
                            break;
                        }
                    }
                } else {
                    pr.Recent_Timecard_End_Date__c = null;
                    toUpdate.add(pr);
                }
            }
            if(toUpdate.size() > 0){
                Database.SaveResult [] updateResult = Database.update(toUpdate, false);
            }
        }
    }
}