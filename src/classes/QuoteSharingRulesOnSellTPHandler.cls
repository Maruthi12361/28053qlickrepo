public class QuoteSharingRulesOnSellTPHandler{
	public Boolean flagForUpdate = false;
    public static void onAfterUpdate(List<SBQQ__Quote__c> listOfRecords, Map<Id, SBQQ__Quote__c> oldMap) {
      processUpdate(listOfRecords, oldMap);
    }
    public static void onAfterInsert(List<SBQQ__Quote__c> listOfRecords) {
      processInsert(listOfRecords);
    }
    
    private static void processInsert(List<SBQQ__Quote__c> listOfRecords) {
      
    List<Id> quoteIds = new List<Id>();
      for(SBQQ__Quote__c itemQuote : listOfRecords) {
        quoteIds.add(itemQuote.id);
      }
    
      if(!quoteIds.isEmpty()) {
        ApexSharingRules.AddQuoteSharingRulesOnSellThroughPartner(quoteIds);
      }
    }    
    private static void processUpdate(List<SBQQ__Quote__c> listOfRecords, Map<Id, SBQQ__Quote__c> oldMap) {
      List<Id> quoteIds = new List<Id>();
      for (SBQQ__Quote__c itemQuote : listOfRecords) {
      //Boolean quoteAdded = false;
      // Recalculate sharing when Sell_Through_Partner or Referring_Contact
      if (!String.isBlank(itemQuote.Sell_Through_Partner__c) && itemQuote.Sell_Through_Partner__c != oldMap.get(itemQuote.id).Sell_Through_Partner__c){
          quoteIds.add(itemQuote.Id);      
      }
    }

    if(!quoteIds.isEmpty()) {
        ApexSharingRules.AddQuoteSharingRulesOnSellThroughPartner(quoteIds);
      }
    }

}