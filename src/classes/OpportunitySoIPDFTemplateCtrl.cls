// CR# 29825
// controller class behind the visualforce page: Opportunity_SOI_PDF_Template
// Change log:
// April 8, 2015 - Initial Implementation - Stuart Sleight
public with sharing class OpportunitySoIPDFTemplateCtrl {
    private Opportunity opp;
    public List<Sphere_of_Influence__c> lSoI {get;set;}
    public Boolean showSoI {get;set;}    
    
    public OpportunitySoIPDFTemplateCtrl(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();

        showSoI = false;
        lSoI = [SELECT INT_Contact_Name__c, Title__c, Goal__c, Formula__c, Improvement__c,
                        Value__c, Owner_Full_Name__c
                    FROM Sphere_of_Influence__c WHERE Opportunity__c = :opp.Id];
       if(lSoI.size() > 0) showSoI = true;
    }
}