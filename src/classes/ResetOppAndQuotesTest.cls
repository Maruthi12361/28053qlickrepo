/**     * File Name:AccountIndustrySectorFunctionHandlerTest
        * Description : Test class for ResetOppAndQuotes
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 March 23rd 2017 RamakrishnaKini     Initial version.
        1.1 March 31st 2017 Andreas Nilsson	    Fixed test class failure
		1.2 August 3rd 2017 Aslam Kamal         QCW-2934 Test method correction PCS record																			
*/
@isTest
private class ResetOppAndQuotesTest {
	
	@testSetup static void testDataSetup() {
		QTTestUtils.GlobalSetUp();
		QTTestUtils.SetupNSPriceBook('USD');
		Profile p = [select id from profile where name='System Administrator'];
		User u = new User(alias = 'standt', email='standarduser@testorg.com', 
							emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
							localesidkey='en_US', profileid = p.Id, 
							timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com', 
							INT_NetSuite_InternalID__c = '5307');

		Semaphores.SetAllSemaphoresToTrue();

        System.runAs(u) {
        	Id accrecTypeId = [select id from recordtype where developername = 'End_User_Account' limit 1].Id;

			Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, true);
	        testAccount.RecordtypeId = accrecTypeId;
			testAccount.Name = 'Test Val Rule 1';
	        
	        update testAccount;
	        
	        List<QlikTech_Company__c> companyQT = [Select Id, QlikTech_Company_Name__c, Country_Name__c From QlikTech_Company__c Where Country_Name__c = 'France'];
			System.assertEquals('France', companyQT[0].Country_Name__c);

			Contact testContact = QTTestUtils.createMockContact(); // partner contact, lastname = 'RDZTestLastName'
	       	Contact testContact1 = new Contact(); // typical account contact;
	       	testContact1.FirstName = 'Roman';
	       	testContact1.Lastname = 'Dovbush';
	       	testContact1.Email = 'test@test.com';
	       	testContact1.HasOptedOutOfEmail = false;
	       	testContact1.AccountId = testAccount.Id;
	       	insert testContact1;

			RecordType opprecTYpe= [select id from Recordtype where DeveloperName='Sales_QCCS' limit 1]; 
	        system.debug('qqqq'+opprecTYpe);
	        Account testacc2 = [select id from account where name ='Test Val Rule 1' limit 1];
	        system.debug('qqqq'+testacc2);
			Opportunity opp = new Opportunity(CurrencyIsoCode= 'USD', name = 'asasa', Short_Description__c ='frff',
	        							StageName = 'Goal Identified', Revenue_Type__c= 'Direct', CloseDate = Date.Today() +10,
	        							AccountId = testacc2.ID, RecordtypeId= opprecTYpe.Id);
	        insert opp;
			
			/******* changes for qcw-2934 start *********/
            insert QuoteTestHelper.createPCS(testAccount);
            /******* changes for qcw-2934 end*********/	
			
	        System.assertNotEquals(null, opp.Id);
			RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote'];
			List<SBQQ__Quote__c> quotesForInsert = new List<SBQQ__Quote__c>();
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact1.id, testAccount, null, 'Direct', 'Open', 'Quote', true, opp.Id);
			quotesForInsert.add(quoteForTest);
			SBQQ__Quote__c quoteForTest1 = QuoteTestHelper.createQuote(rTypeQuote, testContact1.id, testAccount, null, 'Direct', 'Open', 'Quote', false, opp.Id);
			quotesForInsert.add(quoteForTest1);
			insert quotesForInsert;
            System.assertNotEquals(null, quoteForTest.Id);
    	}
    	Semaphores.SetAllSemaphoresToFalse();
	}
	
	@isTest static void testControllerInitAndQuoteDelete() {
		// Implement test code
		//Id oppId = [select id from Opportunity limit 1].Id;
		//System.assertNotEquals(null, oppId);
		Opportunity opp = [select id from Opportunity limit 1];
		System.assertNotEquals(null, opp);
		Id oppId=opp.Id;
		System.assertNotEquals(null, oppId);
		Test.startTest();
			PageReference pageRef = Page.ResetQuotes;
         	Test.setCurrentPage(pageRef);
         	pageRef.getParameters().put('Id', String.valueOf(oppId));
         	ApexPages.StandardController sc = new ApexPages.StandardController(opp);
     		ResetOppAndQuotes resetOppQuotes = new ResetOppAndQuotes(sc);
     		ResetOppAndQuotes.resetQuotesDeleteProducts(oppId);
		Test.stopTest();
		List<SBQQ__Quote__c> quotes = [select id,SBQQ__Status__c from SBQQ__Quote__c];
		System.assertEquals(0, quotes.size());
	}

	@isTest static void testContractingQuotePresetNegScenario() {
		// Implement test code
		//Id oppId = [select id from Opportunity limit 1].Id;
		
		Opportunity opp = [select id from Opportunity limit 1];
		System.assertNotEquals(null, opp);
		Id oppId=opp.Id;
		System.assertNotEquals(null, oppId);
		SBQQ__Quote__c quoteForTest = [select id,SBQQ__Status__c from SBQQ__Quote__c where SBQQ__Primary__c = true limit 1];
		System.assertNotEquals(null, quoteForTest);
		Test.startTest();
			PageReference pageRef = Page.ResetQuotes;
         	quoteForTest.SBQQ__Status__c = 'Contracting';
         	update quoteForTest;
     		ResetOppAndQuotes.resetQuotesDeleteProducts(oppId);
		Test.stopTest();
		List<SBQQ__Quote__c> quotes = [select id,SBQQ__Status__c from SBQQ__Quote__c];
		System.assertEquals(2, quotes.size());
	}

	@isTest static void testOrderPlacedQuotePresetNegScenario() {
		// Implement test code
		//Id oppId = [select id from Opportunity limit 1].Id;
		//System.assertNotEquals(null, oppId);
		Opportunity opp = [select id from Opportunity limit 1];
		System.assertNotEquals(null, opp);
		Id oppId=opp.Id;
		System.assertNotEquals(null, oppId);
		SBQQ__Quote__c quoteForTest = [select id,SBQQ__Status__c from SBQQ__Quote__c where SBQQ__Primary__c = true limit 1];
		System.assertNotEquals(null, quoteForTest);
		Test.startTest();
			PageReference pageRef = Page.ResetQuotes;
         	quoteForTest.SBQQ__Status__c = 'Order Placed';
         	update quoteForTest;
     		ResetOppAndQuotes.resetQuotesDeleteProducts(oppId);
		Test.stopTest();
		List<SBQQ__Quote__c> quotes = [select id,SBQQ__Status__c from SBQQ__Quote__c];
		System.assertEquals(2, quotes.size());
	}
	
}