/************************************************************
* 2020-02-05 extcqb IT-2335 controller for 'Plan Of Action' quick action
************************************************************/
public with sharing class PlanOfActionController {

    public Case theCase {get; private set;}
    public String problemHypothesis {get; set;}
    public String troubleshootingDone {get; set;}
    public String nextActionItems {get; set;}
    public String nextActionOwners {get; set;}
    public String followupCommitment {get; set;}
    public String businessCase {get; set;}

    public PlanOfActionController(ApexPages.StandardController stdController) {
        this.theCase = (Case) stdController.getRecord();
    }

    public void postPlanOfAction() {
        if (isFormValid()) {
            try {
                insert new FeedItem(
                        ParentId = theCase.Id,
                        Body = getPostBody(),
                        Visibility = 'InternalUsers',
                        IsRichText = true
                );
                clearForm();
                updateCase();
            } catch (Exception ex) {
                ApexPages.addMessages(ex);
            }
        } else {
            ApexPages.addMessages(new DmlException('Please fill all required fields'));
        }
    }

    private String getPostBody() {
        return '#PoA<p/><u><b>Plan of Action (PoA):</b></u><p>'
                + '</p><b>Problem hypothesis:</b><p>' + problemHypothesis
                + '</p><b>Troubleshooting Done:</b><p>' + troubleshootingDone
                + '</p><b>Next Action Items:</b><p>' + nextActionItems
                + '</p><b>Next Action Owners:</b><p>' + nextActionOwners
                + '</p><b>Follow-up Commitment:</b><p>' + followupCommitment
                + (businessCase != '' ? ('</p><b>Business Case:</b><p>' + businessCase) : '')
                + '</p>';
    }

    private Boolean isFormValid() {
        return problemHypothesis != '' && troubleshootingDone != '' && nextActionItems != '' && nextActionOwners != '' && followupCommitment != '';
    }

    private void clearForm() {
        problemHypothesis = '';
        troubleshootingDone = '';
        nextActionItems = '';
        nextActionOwners = '';
        followupCommitment = '';
        businessCase = '';
    }

    private void updateCase() {
        update new Case(
                Id = theCase.Id,
                PoA_Created__c = System.now()
        );
    }
}