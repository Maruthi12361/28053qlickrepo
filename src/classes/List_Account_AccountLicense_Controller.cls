/******************************************************

	Class: APEXReport_Controller
	
	Changelog:
		2009-10-23	MHG		Created file
		2009-10-29  MHG		Added test functions
		2009-10-30  MHG		Added methods for single view
		
******************************************************/
public class List_Account_AccountLicense_Controller {

    private ApexPages.StandardController controller;
    private Account_License__c license;
    User me;

	/* ******************************************************
		List_Account_AccountLicense_Controller
		
		Constructors fetches the current record
		2009-10-23	MHG		Created constructor
		2009-10-30	MHG		Created constructor with controller argument
	****************************************************** */
	public List_Account_AccountLicense_Controller() {
		init();
	}
	
	public List_Account_AccountLicense_Controller(ApexPages.StandardController stdController) {
		controller = stdController;		
		
		Account_License__c tmpLicense = (Account_License__c) stdController.getRecord();
		license = [SELECT Id, Account__c FROM Account_License__c WHERE Id = :tmpLicense.Id LIMIT 1];
		
		init();
	}
	
	
	
	/* ******************************************************
		init
		
		Common init functions for the constructors
		
		2009-10-30	MHG		Created method
	****************************************************** */	
	private void init() {
		string myId = System.UserInfo.getUserId();
		me = [SELECT Contact.Account.Id, Profile.Name FROM User WHERE Id = :myId LIMIT 1];	
	}
	
	
	
	/* ******************************************************
		getMyAccountLicenses
		
		Fetches all the Account_Licenses__c that is related
		to the contacts account. 
		
		2009-10-23	MHG		Created method
	****************************************************** */	
	public List<Account_License__c> getMyAccountLicenses()
	{
		List<Account_License__c> myLicenses = new List<Account_License__c>();

		myLicenses = [SELECT ID, Name, Application__c, Support_From__c, Support_To__c, Version__c FROM Account_License__c WHERE Account__c = :me.Contact.Account.Id LIMIT 400];		
 
		return myLicenses;
	}



	/* ******************************************************
		getURL
		
		Method used for fetching the current URL.
		
		2009-10-23	MHG		Created method
	****************************************************** */		
	public string getURL() {
		return Site.getCurrentSiteUrl();	
	}



	/* ******************************************************
		getAccess
		
		Method determines if the current user should see 
		this record. 
		
		2009-10-30	MHG		Created method
		2010-06-28  MHG		Added profile check so that the 
							ViewPage can be used as the view
							button. 
	****************************************************** */		
	public boolean getAccess() {
		
		if (me.Profile.Name != 'Customer Portal Case Viewing Access' && me.Profile.Name != 'Customer Portal Case Logging Access')
		{
			return true;
		}
		
		return (me.Contact.Account.Id == license.Account__c);
	}
	
	
	
	
	
	/* ******************************************************	
	
		TEST METHODS
	
	******************************************************* */
	
	public static  TestMethod void testList_Account_AccountLicense_Controller()
	{
		List_Account_AccountLicense_Controller controllerTest = new List_Account_AccountLicense_Controller();

		string myId = System.UserInfo.getUserId();
		User me = [SELECT Contact.Account.Id FROM User WHERE Id = :myId];
		
		Account_License__c accLic = new Account_License__c();
		accLic.Name = 'Test Account';
		accLic.Application__c = 'Test application';
		accLic.Support_From__c = date.valueOf('2009-01-01');
		accLic.Support_To__c = date.valueOf('2009-12-31');
		accLic.Account__c = me.Contact.Account.Id;
		
		insert accLic;
		
		List<Account_License__c> myList = controllerTest.getMyAccountLicenses();
		Boolean foundLicense = false;
		for(Account_License__c acc : myList) {
			if ('Test Account' == acc.Name) {
				foundLicense = true;
				break;				
			}	
		}
		// This assertion can't be done on live since the SSO Hold Account
		// keeps all the non matched licenses (25k+)		
		//System.assert(foundLicense); 
		
		System.assertEquals(Site.getCurrentSiteUrl(), controllerTest.getURL());
		
		
		ApexPages.standardController controller = new ApexPages.standardController(accLic);
		controllerTest = new List_Account_AccountLicense_Controller(controller);
		
		System.assert(controllerTest.getAccess());
	}

}