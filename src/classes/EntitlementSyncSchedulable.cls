/*
Name:  EntitlementSyncSchedulable.cls
Copyright © 2011  salesforce.com consulting
======================================================
======================================================
Purpose:
-------
======================================================
======================================================
History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     Mark Cane&  2011-10-07  Initial development.
*/
global class EntitlementSyncSchedulable implements Schedulable {
	public EntitlementSyncSchedulable(){}
    
    global void execute(SchedulableContext sc){
        System.abortJob(sc.getTriggerId());
        //aaron added 1 line below + line 25 reference
		Double rnd =  Math.random();
        //& if > 4 batch processes queued or processing then reschedule this class and abort instance.
        if ([select count() from AsyncApexJob where Status='Queued' or Status='Processing']>4){                
            String s = '05 '+ (System.Now().Minute()==59 ? '0' : (System.Now().Minute()+1).format()) + ' * * * ?';
            String j = System.schedule('Entitlement Sync - Auto Schedule ' + rnd, s, new EntitlementSyncSchedulable());
        	return;
        }        	
		//& invoke Batch Apex class.
		EntitlementSyncBatchable b = new EntitlementSyncBatchable();
		Id pId = Database.executeBatch(b); //& default scope of 200.
    }
}