/*
    2017-10-03 AIN Test class created.
*/
@isTest
private class OppProductPopulateProductGrpHandlerTest {
    @isTest static void testNoTrigger() {

        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('USD');
        ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'USD' LIMIT 1].Id;
        //QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, 
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com', 
        INT_NetSuite_InternalID__c = '5307');

        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGrpHandlerTest 1');


            SIC_Code__c code = new SIC_Code__c(Name ='8732:Commercial Economic, Sociological, and Educational Research', Industry__c='Services', QlikTech_Sector__c='Retail & Services');
            insert code;

            Id accrecTypeId = [select id from recordtype where developername = 'End_User_Account' limit 1].Id;

            u = [select id from User where id =: UserInfo.getUserId()];
            Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, true);
            
            testAccount.RecordtypeId = accrecTypeId;
            testAccount.Name = 'Test Val Rule 1';
            testAccount.Territory_Country__c = 'France';
            update testAccount;
            
            List<QlikTech_Company__c> companyQT = [Select Id, QlikTech_Company_Name__c, Country_Name__c From QlikTech_Company__c Where Country_Name__c = 'France'];
            System.assertEquals('France', companyQT[0].Country_Name__c);

            Contact testContact1 = new Contact(); // typical account contact;
            testContact1.FirstName = 'Roman';
            testContact1.Lastname = 'Dovbush';
            testContact1.Email = 'test@test.com';
            testContact1.HasOptedOutOfEmail = false;
            testContact1.AccountId = testAccount.Id;
            insert testContact1;
            List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(testAccount.Id, testContact1.Id, 'Billing'),
                QuoteTestHelper.createAddress(testAccount.Id, testContact1.Id, 'Shipping')                
            };
            insert addrs;
            RecordType opprecTYpe= [select id from Recordtype where DeveloperName='Sales_QCCS' limit 1]; 
            Account testacc2 = [select id from account where name ='Test Val Rule 1' limit 1];
            Opportunity opp = new Opportunity(CurrencyIsoCode= 'USD', name = 'asasa', Short_Description__c ='frff',
                                        StageName = 'Goal Identified', Revenue_Type__c= 'Direct', CloseDate = Date.Today() +10,
                                        AccountId = testacc2.ID, RecordtypeId= opprecTYpe.Id);
            insert opp;

            system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGrpHandlerTest 2');

            test.StartTest();

            RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote'];
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact1.id, testAccount, null, 'Direct', 'Open', 'Quote', true, opp.Id);
            insert quoteForTest;

            system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGrpHandlerTest 4');

            Product2 prod1 = QuoteTestHelper.createProduct('Prod1', 'Family1', 0, 'ProdGroup1', '');
            Product2 prod2 = QuoteTestHelper.createProduct('Prod2', 'Family1', 0, 'ProdGroup1', '');
            Product2 prod3 = QuoteTestHelper.createProduct('Prod3', 'Family1', 0, 'ProdGroup2', '');
            List<Product2> products = new List<Product2>();
            products.add(prod1);
            products.add(prod2);
            products.add(prod3);
            insert products;

            SBQQ__QuoteLine__c quoteLine1 = QuoteTestHelper.createQuoteLine(quoteForTest.Id, prod1.Id, 'serverSiteSetup', 10, '00', 'GroupText', 'Family', 'USD', 1000, 10, 'licenseId', '');
            SBQQ__QuoteLine__c quoteLine2 = QuoteTestHelper.createQuoteLine(quoteForTest.Id, prod2.Id, 'serverSiteSetup', 10, '00', 'GroupText', 'Family', 'USD', 1000, 10, 'licenseId', '');
            SBQQ__QuoteLine__c quoteLine3 = QuoteTestHelper.createQuoteLine(quoteForTest.Id, prod3.Id, 'serverSiteSetup', 10, '00', 'GroupText', 'Family', 'USD', 1000, 10, 'licenseId', '');
            List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
            quoteLines.add(quoteLine1);
            quoteLines.add(quoteLine2);
            quoteLines.add(quoteLine3);
            insert quoteLines;

            OpportunityLineItem opplineitem1 = new OpportunityLineItem();
            opplineitem1.SBQQ__QuoteLine__c = quoteline1.Id;
            opplineitem1.OpportunityId = opp.Id;
            opplineitem1.Quantity = 5;
            opplineitem1.TotalPrice = 50;
            opplineitem1.PricebookEntryId = PB;

            OpportunityLineItem opplineitem2 = new OpportunityLineItem();
            opplineitem2.SBQQ__QuoteLine__c = quoteline2.Id;
            opplineitem2.OpportunityId = opp.Id;
            opplineitem2.Quantity = 5;
            opplineitem2.TotalPrice = 50;
            opplineitem2.PricebookEntryId = PB;

            OpportunityLineItem opplineitem3 = new OpportunityLineItem();
            opplineitem3.SBQQ__QuoteLine__c = quoteline3.Id;
            opplineitem3.OpportunityId = opp.Id;
            opplineitem3.Quantity = 5;
            opplineitem3.TotalPrice = 50;
            opplineitem3.PricebookEntryId = PB;

            List<OpportunityLineItem> opplineitems = new List<OpportunityLineItem>();
            opplineitems.add(opplineitem1);
            opplineitems.add(opplineitem2);
            opplineitems.add(opplineitem3);
            insert opplineitems;

            List<Id> opplineids = new List<Id>();
            for(OpportunityLineItem opplineitem : opplineitems)
                opplineids.add(opplineitem.Id);

            opplineitems = [select id, OpportunityId, Product_Group_From_Quoteline__c, SBQQ__QuoteLine__c, SBQQ__QuoteLine__r.Name, SBQQ__QuoteLine__r.Product_Group__c from OpportunityLineItem where id in :opplineids];

            OppProductPopulateProductGrpHandler.PopulateProductGroup(opplineitems);

            opp = [select id, Product_Groups__c from Opportunity where id = :opp.Id];

            system.debug(LoggingLevel.WARN, 'opp.Product_Groups__c: ' + opp.Product_Groups__c);

            system.assertEquals('ProdGroup1;ProdGroup2', opp.Product_Groups__c);

            delete opplineitems;

            test.stopTest();
        
    }
    
    @isTest static void test_method_two() {
        // Implement test code
    }
}