/*
@author: Malay Desai, Slalom LLC.
@date: 01/18/2019
@description: Controler for the QVM partner profile page for Qlik Commerce community
*/
public without sharing class QVM_PartnerProfile_QC {
	
	public QVM_Partner_Data__c partner {get;set;}
	public Id pId {get;set;}
	
	public QVM_PartnerProfile_QC() {
        QVM_Partner_Data__c partnerData = null;

        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) return;
        
		pId = QVM.getPageParameter('Id');
		
		partner = (null == pId) ? new QVM_Partner_Data__c() : [select Id, Partner_Display_Name__c, 
		Partner_Website__c, Partner_Profile__c, Phone__c, Address__c, Contact_Email__c, 
		Support_Email__c, Support_Phone__c, Support_URL__c, Profile_Status__c, Last_Partner_Update__c 
		from QVM_Partner_Data__c where Id = :pId];
	}
	
	public PageReference savePartner() {
		try {
			Boolean passValidation = true;
			if(!partner.Partner_Website__c.contains('http://') && !partner.Partner_Website__c.contains('https://')) {
				partner.Partner_Website__c.addError(Label.QVM_Profile_Partner_Website_Validation);
				passValidation = false;
			}
			if(!partner.Support_URL__c.contains('http://') && !partner.Support_URL__c.contains('https://')) {
                partner.Support_URL__c.addError(Label.QVM_Profile_Partner_Support_Website_Validation);
                passValidation = false;
            }
            if(passValidation){
            	partner.Last_Partner_Update__c = system.now();
	            partner.Profile_Status__c = 'Active';
	            update partner;
	            PageReference next;
	            next = Page.QVM_ControlPanelMain_QC;
	            return next;
            } else {
            	return null;
            }
			
		} catch(Exception e) {
			ApexPages.addMessages(e);
			return null;
		}
	}
	
	public PageReference cancel() {
		PageReference next;
		next = Page.QVM_ControlPanelMain_QC;
		return next;
	}
    
    // CCE  |  CR# 40489 Qlik Market - add notice to register to additional pages
    public PageReference checkRegistration() {
        PageReference p = null;
        QVM_Partner_Data__c partnerData = null;
                
        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) p = Page.QVM_ControlPanelMain_QC;
        return p;
    }
}