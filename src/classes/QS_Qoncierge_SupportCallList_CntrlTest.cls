/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_Qoncierge_SupportCallList_CntrlTest {
	
	// Unit test for QS_Qoncierge_SupportCallList_Cntrl Apex class and covers the functionality of QS_Qoncierge_SupportCallList Visualforce Component which is being used on Community Home Page
    static testMethod void testCaseController() {
		List<QS_Support_Contact_Info__c> supportContactList = new List<QS_Support_Contact_Info__c>();
		supportContactList.add(TestDataFactory.supportContactInfo('Test Country 1', '1800 137 636 (Toll free) | 02 6111 2045', 'testSupportQlik1@email.com', 'Monday - Friday between 8:00 am - 5:00 pm Sydney EST/EDT'));
		supportContactList.add(TestDataFactory.supportContactInfo('Test Country 2', '1800 137 637 (Toll free) | 02 6111 2046', 'testSupportQlik2@email.com', 'Monday - Friday between 9:00 am - 6:00 pm Sydney EST/EDT'));
		supportContactList.add(TestDataFactory.supportContactInfo('Test Country 3', '1800 137 638 (Toll free) | 02 6111 2047', 'testSupportQlik3@email.com', 'Monday - Friday between 10:00 am - 7:00 pm Sydney EST/EDT'));
		supportContactList.add(TestDataFactory.supportContactInfo('Test Country 4', '1800 137 639 (Toll free) | 02 6111 2048', 'testSupportQlik4@email.com', 'Monday - Friday between 11:00 am - 8:00 pm Sydney EST/EDT'));
		
		insert supportContactList;
		
		Test.startTest();
			QS_Qoncierge_SupportCallList_Cntrl supportCallList = new QS_Qoncierge_SupportCallList_Cntrl();
			System.assert(supportCallList.countryPhoneList != null && !supportCallList.countryPhoneList.isEmpty());
			
			System.assertEquals(supportCallList.countryPhoneList.size(), 4);
			
			supportCallList.selectedCountry = null;
			supportCallList.populateSupportDetailInfo();
			System.assertEquals(supportCallList.supportDetailInfo, null);
			
			supportCallList.selectedCountry = supportContactList[2].Name;
			supportCallList.populateSupportDetailInfo();
			System.assert(supportCallList.supportDetailInfo != null);
			
			supportCallList.selectedCountry = supportContactList[1].Name;
			supportCallList.populateSupportDetailInfo();
			System.assert(supportCallList.supportDetailInfo != null);
			
			
		Test.stopTest();
	} 
}