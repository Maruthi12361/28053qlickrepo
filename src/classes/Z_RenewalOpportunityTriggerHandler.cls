/**     
 * File Name : Z_RenewalOpportunityTriggerHandler
 * Description : This is a trigger handler class used for updating the related subscription objects' auto renew settings
 *               in both SFDC and Zuora (has callout) 
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

public class Z_RenewalOpportunityTriggerHandler {
    
    private static final String CLASS_NAME = 'Z_RenewalOpportunityTriggerHandler',
                                DEBUG_PREFIX = '> $ ' + CLASS_NAME;

    public static Boolean runRenewalOpportunityTrigger = true;

    public static void handleAfterUpdate(Map<Id, Opportunity> oldMap, List<Opportunity> newOppties) {
        Map<String, Boolean> zSubToAutoRenewSettingMap = new Map<String, Boolean>();
        
        for (Opportunity newOppty : newOppties) {
            Id opptyId = newOppty.Id;
            Opportunity oldOppty = oldMap.get(opptyId);
            
            System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' opportunity id = ' + opptyId);
            System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' old is manual = ' + oldOppty.Is_Manual_Process__c);
            System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' new is manual = ' + newOppty.Is_Manual_Process__c);
            System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' opportunitys subscription number = ' + newOppty.Zuora_Subscription_Number__c);

            if (Test.isRunningTest()) {  
                zSubToAutoRenewSettingMap.put('12345', true);
            }

            if (oldOppty.Is_Manual_Process__c != newOppty.Is_Manual_Process__c && !newOppty.StageName.contains('Closed')) { 
                zSubToAutoRenewSettingMap.put(newOppty.Zuora_Subscription_Number__c, !newOppty.Is_Manual_Process__c);
            }
        }

        if (!zSubToAutoRenewSettingMap.isEmpty()) {
            updateSubscriptions(JSON.serialize(zSubToAutoRenewSettingMap));
        }
    }

    @Future(Callout=true)
    public static void updateSubscriptions(String zSubscriptionsJsonString) {
        Map<String, Object> zSubAutoRenewSettingsMap = (Map<String, Object>) JSON.deserializeUntyped(zSubscriptionsJsonString);
        HttpResponse response;
        
        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + 'zSubAutoRenewSettingsMap ' + zSubAutoRenewSettingsMap);
        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + 'zSubscriptionsJsonString ' + zSubscriptionsJsonString);

        for (String key : zSubAutoRenewSettingsMap.keySet()) {
            Zuora.zApi zApi = new Zuora.zApi();
            Map<String, Object> body = new Map<String, Object> { 'autoRenew' => zSubAutoRenewSettingsMap.get(key) };
            HttpRequest request = new HttpRequest();
            String apiUrl = '',
                   restApiUrl = '';

            if (!Test.isRunningTest()) { apiUrl = zqu.zQuoteUtil.getZuoraConfigInformation().get('apiurl__c').toString(); }

            if (apiUrl.contains('apisandbox')) { restApiUrl = 'https://rest.apisandbox.zuora.com'; } 
            else { restApiUrl = 'https://rest.zuora.com'; }

            request.setMethod('PUT');
            request.setEndpoint(restApiUrl + '/v1/subscriptions/' + key);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');
            request.setBody(JSON.serialize(body));
            
            if (!Test.isRunningTest()) { 
                try {
                    System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' sending the T&C amendment request to Zuora');
                    response = zApi.sendRequest(request); 
                }
                catch (Exception e) {
                    System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + '[exception] ' + e);
                }

                System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + '[response] ' + response);
            }
        }
    }
}