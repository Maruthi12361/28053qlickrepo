/**************************************************
* CR# 97533 – Relayware triggers
* Change Log:
* 2016-10-31 BAD Initial creation of class 
* 2017-06-21 UIN Adding null checks
* 2018-02-02 AYS BMW-509 : Terminated Contact gets readded to Partner Portal
**************************************************/
public class ContactPartnerPortalRelaywareHandler {
    public ContactPartnerPortalRelaywareHandler() {
        
    }

    public static void handle(List<Contact> triggerNew, List<Contact> triggerOld) {
        System.debug('ContactPartnerPortalRelaywareHandler: Starting');

        if(!Semaphores.TriggerHasRun('ContactPartnerPortalRelaywareHandler')) 
        {
            Set<Id> sets = new Set<Id>();
            
            for (Integer i = 0; i < triggerNew.size(); i++)
            {
                System.debug('BAD check trigger:' + triggerOld[i].id);
                sets.add(triggerNew[i].AccountID);
            }

            Map<Id, Account> acctMap = new Map<Id, Account>([select Navision_Status__c, Terminated__c from Account where Id in :sets]);
            System.debug('AIN acctMap.Size:' + acctMap.Size());
            
            for (Integer i = 0; i < triggerNew.size(); i++)
            {
                System.debug('BAD OLD check trigger Left_Company__c:' + triggerOld[i].Left_Company__c);
                System.debug('BAD NEW check trigger Left_Company__c:' + triggerNew[i].Left_Company__c);
                System.debug('BAD NEW check trigger Partner_Contact_Approval_Status__c:' + triggerNew[i].Partner_Contact_Approval_Status__c);
                System.debug('BAD NEW check trigger ActiveULC__c:' + triggerNew[i].ActiveULC__c);

                // Check if Contact has been approved 
                if( !triggerNew[i].Partner_Contact_Approval_Status__c && triggerNew[i].ActiveULC__c && !triggerNew[i].Left_Company__c)
                {
                    System.debug('BAD check if Contact has been approved ');

                    if(!acctMap.isEmpty()){ // Start Addded by UIN to prevent null pointer errors
                        //Check if account is an active Partner Account
                        Account acc = acctMap.get(triggerNew[i].AccountID);
                        if(acc!= null){
                            if(acc.Navision_Status__c == 'Partner' && !acc.Terminated__c)
                            {
                                System.debug('BAD Check if account is an active Partner Account');
                                triggerNew[i].Synch_with_Relayware__c = true; 
                                triggerNew[i].Partner_Contact_Approval_Status__c = true; 
                            }
                        }
                    } // End Addded by UIN to prevent null pointer errors
                }

                // Check if Contact has been terminated
                if( triggerOld[i].Left_Company__c != triggerNew[i].Left_Company__c && triggerNew[i].Left_Company__c)
                {
                    triggerNew[i].Partner_Contact_Approval_Status__c = false; 
                }
            }
        }
    }


}