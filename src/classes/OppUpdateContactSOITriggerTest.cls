// Test case to test the triggers: OppUpdateContactSOI
// Changelog:
// 2013-04-03  Madhav Kakani - Fluido Oy Initial development for CR# 7356
// 2013-10-29  CCE  Several changes made as the the alias mkak was no longer
//                  in the system so we had 0% coverage (discovered while
//                  doing testing for CR# 8968)
// 2014-04-17   TJG Billing and shipping country US -> USA
//
// 2014-04-30   TJG Fix validation error:
//                  System.DmlException: Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Alignment section must be completed on large deals.: [Customers_Business_Pain__c]
// 2014-04-30   TJG CR 12222 Auto create solution profile https://eu1.salesforce.com/a0CD000000hhiUI
// 07.02.2017   RVA :   changing QT methods  
// 2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring
// 2020-04-14 Leonardo Grauer - Updating Opp's record type. Direct / Reseller - Std Sales Process was deactivated - OTQ-480  
// 2020-04-16 shubham gupta removing Qlikogram reference  
// 2020-07-09 Leonardo Grauer Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
//				"Deployment Preference" is required in Goal Confirmed stage 
@isTest
private class OppUpdateContactSOITriggerTest {
    static testMethod void runTest() {

      Profile p = [select id from profile where name='System Administrator']; 
      User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com', 
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
      localesidkey='en_US', profileid = p.Id, 
      timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.RunAs(mockSysAdmin)
        {

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
            /*
            Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
            QlikTech_Company__c qtc = new QlikTech_Company__c();
            qtc.name = 'SWE';
            qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
            qtc.Country_Name__c = 'Sweden';
            qtc.CurrencyIsoCode = 'SEK';
            qtc.Subsidiary__c = testSubs.id; 
            insert qtc;
            */
            QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
                 qtc.Country_Name__c = 'Sweden';
                 qtc.CurrencyIsoCode = 'SEK';
            update qtc;
            Account act = new Account(name='Test Account');
            act.OwnerId = mockSysAdmin.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
    
            // Query an existing Opp in the organization. This is because inserting a test opp is failing the test
            // due to the insert operation triggering web service callouts.
            // web service callouts are not supported in test methods.
            //Opportunity opp = [SELECT Id, Name, StageName FROM Opportunity WHERE Name='Test Opp' LIMIT 1];
            //system.assert(opp != null);
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            //Create test Opportunity
           
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
				Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
                StageName = 'Goal Confirmed' ,
                RecordTypeId = '012D0000000khWZ',   // Direct / Reseller - Std Sales Process updated to Sales QCCS
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = ct.AccountId,
                OEM_Partner_Account__c=null,
                Included_Products__c = 'Qlik Sense'
                //GI_Acceptance_Status__c='GI Rejected'

            ); 
             Test.startTest();
            insert testNewOpp;
            
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;         
            insert LineItem;     
                  
            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = testNewOpp.Id;
            soi.Contact__c = ct.Id;
            soi.Role__c = 'Beneficiary';    
            insert soi;
             Test.stopTest(); 
            Semaphores.OppUpdateContactSOI_IsAfter = false;
            testNewOpp.StageName = 'Closed Won';
            //testNewOpp.Send_Qlikogram__c = true;    //CR 12222
            testNewOpp.GI_Acceptance_Status__c='GI Accepted';
            update testNewOpp;
            //Pramod:Test class coverage
           //OppUpdateContactSOIHandler tempObj = new OppUpdateContactSOIHandler();
          
          List<Opportunity> triggerNew =new List<Opportunity> ();  
          List<Opportunity> triggerOld=new List<Opportunity> ();
          Map<Id, Opportunity> triggerOldMap=new Map<Id, Opportunity> ();
          Boolean isUpdate=false;
          Boolean isInsert=true ;
          Boolean isAfter=true;
          
          Boolean isBefore=true;
          Boolean isDelete=false;
          triggerNew.add(testNewOpp);
          triggerOld.add(testNewOpp);
          triggerOldMap.put(testNewOpp.id,testNewOpp);
          //tempObj.handle(triggerNew,triggerOld,triggerOldMap,isUpdate,isInsert,isAfter,isBefore,isDelete);
          OppUpdateContactSOIHandler.handle(triggerNew,triggerOld,triggerOldMap,isUpdate,isInsert,isAfter,isBefore,isDelete);
          
   //handle(List<Opportunity> triggerNew, List<Opportunity> triggerOld, Map<Id, Opportunity> triggerOldMap, Boolean isUpdate, Boolean isInsert, Boolean isAfter, Boolean isBefore, Boolean isDelete) 
            Contact ctRet = [SELECT Is_Closed_Won_SOI__c FROM Contact WHERE Id = :ct.Id];
       //     system.assert(ctRet.Is_Closed_Won_SOI__c == 'Yes');    
    
            delete testNewOpp;
    
        
        }
    }

    static testMethod void runTest1() {
        
        QTTestUtils.GlobalSetUp();
        

        Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'USA');
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        QTTestUtils.SetupNSPriceBook('GBP');
        User mockSysAdmin = Util.createUser('TstSAdminNme', 'Testab1', testProfile.Id, testUserRole.Id);
        
        System.RunAs(me) {   
            
            QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
                 qtc.Country_Name__c = 'Sweden';
                 qtc.CurrencyIsoCode = 'SEK';
            update qtc;

            Account act = new Account(name='Test Account');
            act.OwnerId = me.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test', Contact_Status__c='Goal Identified',
                                    MQL_Start_Date__c = Date.newInstance(2015, 05, 29), SAL_Start_Date_Current__c = Date.newInstance(2014, 07, 07));
            insert ct;
    
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            
            // just execute constructor
            OppUpdateContactSOIHandler tempObj = new OppUpdateContactSOIHandler();
            //Create test Parent Opportunity
            Opportunity testParentOpp = new Opportunity (
                Short_Description__c = 'TestParentOpp - delete me later',
                Name = 'TestParentOpp',
                Type = 'New Customer',
                GI_Acceptance_Status__c = 'GI Accepted',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Discovery',
                RecordTypeId = '012D0000000khWZ',   // Direct / Reseller - Std Sales Process updated to Sales QCCS
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = ct.AccountId
            );
            insert testParentOpp;
            System.assertNotEquals(null, testParentOpp.Id);


            Test.StartTest();
            Sphere_of_Influence__c soiP = new Sphere_of_Influence__c();
            soiP.Opportunity__c = testParentOpp.Id;
            soiP.Contact__c = ct.Id;
            soiP.Role__c = 'Beneficiary';    
            insert soiP;

            //GI_Acceptance_Status__c = null in this Opp
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Discovery',
                RecordTypeId = '01220000000DoEj',   // Deal Split Child
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = ct.AccountId,
                Deal_Split_Parent_Opportunity__c = testParentOpp.Id
            );
            OppUpdateContactSOIHandler.handleFlag = false; // in one context we need to run class twice, so we need to set flag to FALSE
            insert testNewOpp;
            System.debug('--- testNewOpp: ' + testNewOpp.StageName);

            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = testNewOpp.Id;
            soi.Contact__c = ct.Id;
            soi.Role__c = 'Beneficiary';    
            insert soi;

            Contact contact = [SELECT MQL_Start_Date__c, MQL_Start_Date_Current__c, SAL_Start_Date__c, SAL_Start_Date_Current__c FROM Contact WHERE Id = :ct.Id];
            System.assert(contact.MQL_Start_Date__c == Date.newInstance(2015, 05, 29));
            System.assert(contact.MQL_Start_Date_Current__c == null);  
            System.assert(contact.SAL_Start_Date__c == null); 
            System.assert(contact.SAL_Start_Date_Current__c == Date.newInstance(2014, 07, 07));  

            testNewOpp = [Select Id, GI_Acceptance_Status__c, StageName From Opportunity Where Id =: testNewOpp.Id];
            System.debug('--testNewOpp-check ' + testNewOpp);
			testNewOpp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            testNewOpp.StageName = 'Goal Confirmed';
            testNewOpp.GI_Acceptance_Status__c = 'GI Accepted';
            testNewOpp.Included_Products__c = 'Qlik Sense';
            // call this method to clear the flag that lock update at the same session; 
            OppUpdateContactSOIHandler.handleFlag = false; // in one context we need to run class twice, so we need to set flag to FALSE
            Semaphores.TriggerHasRun(1); 
            // now we can update record;
            update testNewOpp;

            delete testNewOpp;
              
            Test.StopTest();        
        }
    }

    static testMethod void runTest2_SolutionProfiles_created() {
        
        QTTestUtils.GlobalSetUp();
        
        Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'USA');
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        QTTestUtils.SetupNSPriceBook('GBP');
        User mockSysAdmin = Util.createUser('TstSAdminNme', 'Testab1', testProfile.Id, testUserRole.Id);
        
        System.RunAs(me) {   
            QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
                 qtc.Country_Name__c = 'Sweden';
                 qtc.CurrencyIsoCode = 'SEK';
            update qtc;
            
            Account act = new Account(name='Test Account');
            act.OwnerId = me.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test', Contact_Status__c='Goal Identified',
                                    MQL_Start_Date__c = Date.newInstance(2015, 05, 29), SAL_Start_Date_Current__c = Date.newInstance(2014, 07, 07));
            insert ct;
    
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            
            // just execute constructor
            OppUpdateContactSOIHandler tempObj = new OppUpdateContactSOIHandler();
            //Create test Parent Opportunity
            Opportunity testParentOpp = new Opportunity (
                Short_Description__c = 'TestParentOpp - delete me later',
                Name = 'TestParentOpp',
                Type = 'New Customer',
                GI_Acceptance_Status__c = 'GI Accepted',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Discovery',
                RecordTypeId = '012D0000000khWZ',   // Direct / Reseller - Std Sales Process updated to Sales QCCS
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = ct.AccountId
            );
            insert testParentOpp;
            System.assertNotEquals(null, testParentOpp.Id);


            Test.StartTest();
            Sphere_of_Influence__c soiP = new Sphere_of_Influence__c();
            soiP.Opportunity__c = testParentOpp.Id;
            soiP.Contact__c = ct.Id;
            soiP.Role__c = 'Beneficiary';    
            insert soiP;

            // GI_Acceptance_Status__c is null on this opp
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Discovery',
                RecordTypeId = '01220000000DoEj',   // Deal Split Child
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = ct.AccountId,
                Deal_Split_Parent_Opportunity__c = testParentOpp.Id
            );
            OppUpdateContactSOIHandler.handleFlag = false; // in one context we need to run class twice, so we need to set flag to FALSE
            insert testNewOpp;
            System.debug('--- testNewOpp: ' + testNewOpp.StageName);

            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = testNewOpp.Id;
            soi.Contact__c = ct.Id;
            soi.Role__c = 'Beneficiary';    
            insert soi;
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            Semaphores.OpportunityTriggerAfterUpdate = false;
            testNewOpp = [Select Id, GI_Acceptance_Status__c, StageName From Opportunity Where Id =: testNewOpp.Id];
            testNewOpp.Short_Description__c = 'TestOpp - Solution_Profiles__c created';
            //testNewOpp.Send_Qlikogram__c = true;
            // call this method to clear the flag that lock update at the same session; 
            OppUpdateContactSOIHandler.handleFlag = false; // in one context we need to run class twice, so we need to set flag to FALSE
            Semaphores.OppUpdateContactSOI_IsAfter = false; 
            // now we can update record;
            update testNewOpp;
system.debug('Check Status');
            // check if Solution_Profiles__c was created for Deal Split Child opp when Send_Qlikogram__c = true;
            //Solution_Profiles__c sp = [Select Id, Name, Opportunity_Name__c From Solution_Profiles__c Where Opportunity_Name__c = :testNewOpp.Id];
            //System.assertNotEquals(null, sp.Id);
            //System.assertNotEquals(testNewOpp.Name, sp.Name);

            delete testNewOpp;
              
            Test.StopTest();        
        }
    }

    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr, 
                                            Country_Name__c = countryName, 
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
}