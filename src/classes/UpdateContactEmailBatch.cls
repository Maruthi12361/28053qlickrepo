/**
	16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors
    1.1 March 23rd 2017 UIN ADded test visible method
**/
global class UpdateContactEmailBatch implements Database.Batchable<Sobject>, Database.Stateful{
    
    String query;
    global List<String> exception_List;
    boolean IsSandbox {get;set;}
        
    global UpdateContactEmailBatch() {
        if(exception_List == null)
            exception_List = new List<String>();
        query = 'select id, email, Name, FirstName, LastName from Contact where email != \'\''; 
        IsSandbox = [select Id, IsSandbox from Organization limit 1].IsSandbox;     
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        if(IsSandbox){
            List <Contact> lstUpdatedContacts = new list<Contact>(); 
            for(Contact l: scope){
                if(String.isNotBlank(l.email)){
                    String temp = l.email + '.sandbox';
                    if(String.isBlank(l.firstname)) l.firstname = l.lastname;
                    if(String.isBlank(l.lastname) && String.isnotBlank(l.firstname)) l.lastname = l.firstname;
                    if(temp.length() > Contact.email.getDescribe().getLength()){
                        l.email = l.email.substring(0, '.sandbox'.length());
                        l.email = l.email + '.sandbox';
                        lstUpdatedContacts.add(l);
                    }else{
                        l.email = temp;
                        lstUpdatedContacts.add(l); 
                    }
                    //l.email = l.email.replace('.sandbox', '.sandbo1');
                    //lstUpdatedContacts.add(l);
                }
            }
            if(System.Test.isRunningTest()){
                String email = 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnbbbbbbbbbbbbbbbbbbbbbbbbbbbb@aaa.com';
                for(Integer i=0;i<202;i++){
                    Contact ct = new Contact(   FirstName = 'Test LeadCountryISOUpdate',
                                Email = email + String.valueOF(i),
                                LastName = '', 
                                QCloudID__c = 'Enterprise',
                                PartnerSourceNo__c = '12345'
                                );
                    lstUpdatedContacts.add(ct);
                }
            }
            List<Database.SaveResult> sResults = Database.update(lstUpdatedContacts,false);
            for(integer i =0; i<sResults.size();i++){
                String msg='';
                If(!sResults[i].isSuccess()){
                    msg += 'Lead Record with Id:'+ lstUpdatedContacts.get(i).Id + ' and Name: '+ lstUpdatedContacts.get(i).Name + 'failed due to the Error: "';        
                    for(Database.Error err: sResults[i].getErrors()){  
                         msg += err.getmessage()+'<br/>';
                    } 
                }
            if(msg!='' && msg.contains('Error:'))
                exception_List.add(msg);
            }

            if(exception_List.size() > 200)
                sendErrorMails(exception_List);
        }
    }
    
    private void sendErrorMails(List<String> exceptions){
        String errMsg = 'The batch Apex job errors for Records processed till now are' +'<br/>';
        String temp='';
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'ullalramakrishna.kini@qlik.com', 'Saul.Santos@qlik.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Contact Email Update Batch Job Status In-Progress');
        if(!exceptions.isEmpty()){
            for(String str:exceptions)
                temp = temp + str;
            mail.setHTMLBody(errMsg + temp);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            exception_List = new List<String>();
        }  
    }
    
    global void finish(Database.BatchableContext BC) {
        if(IsSandbox){
            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()];
            // Send an email to the Apex job's submitter notifying of job completion.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.settargetobjectId(a.CreatedBy.ID);
            mail.setSaveAsActivity(false);
            mail.setSubject('Contact Email Update Batch Job Status ' + a.Status);
            mail.setPlainTextBody('The batch job has finished.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }    
    }
    
}