/*
 File Name: CaseEntitlementProcessSyncTest
 Class:     CaseEntitlementProcessSyncHandler

 2018-04-03  ext_bad     INC0128561
*/
@isTest
public with sharing class CaseEntitlementProcessSyncTest {

    public static testMethod void testMethod1() {
        QTTestUtils.GlobalSetUp();
        
        Account testAct = new Account(
                Name = 'Test Account',
                Navision_Status__c = 'Something',
                QlikTech_Company__c = 'QlikTech Nordic AB',
                QT_Designated_Support_Contact__c = UserInfo.getUserId()
        );
        insert testAct;
        Account_License__c accLic = new Account_License__c();
        accLic.account__c = testAct.id;
        accLic.name = 'AccLic';
        accLic.session_cal__c = 1;
        accLic.named_user_cal__c = 1;
        accLic.document_cal__c = 1;
        accLic.usage_cal__c = 1;
        accLic.uncapped__c = true;
        insert accLic;
        Contact ctct = new Contact(
                Email = 'testEmail@gmail.com',
                FirstName = 'TestFN',
                LastName = 'TestLN',
                AccountId = testAct.Id,
                LeadSource = 'WEB - Web Activity',
                DSE_Access__c = true
        );
        insert ctct;

        SlaProcess slaProcesses = [
                SELECT  id, name, description, isactive, startdatefield, createddate,
                        createdbyid, lastmodifieddate, lastmodifiedbyid, systemmodstamp
                FROM    SlaProcess
                WHERE   isactive = true limit 1];
        Entitlement ent = new Entitlement();
        ent.name = 'Product License';
        ent.AccountId = testAct.Id;
        ent.Account_License__c = accLic.Id;
        ent.slaProcessid = slaProcesses.id;
        ent.startdate = Date.today().addDays(-5);
        ent.enddate = Date.today().addDays(5);
        insert ent;

        Case c1 = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = ctct.Id,
                EntitlementId = ent.Id
        );

        Test.startTest();
        insert c1;
        Test.stopTest();

        c1 = [SELECT Id, Designated_Support_Engineer__c, Entitlement_Process__c FROM Case
                WHERE Id = :c1.Id];
        System.assertEquals(c1.Designated_Support_Engineer__c, UserInfo.getUserId());
        System.assertEquals(c1.Entitlement_Process__c, slaProcesses.Name);
    }
}