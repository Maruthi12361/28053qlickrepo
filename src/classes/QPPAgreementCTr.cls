/****************************************************************
*
*  QPPAgreementCTr
*  22.12.2019 : UIN : THis class is the controller for QPP Launch page.
*  05.06.2019 : UIN : Preventing agreements for partners in russia DGM -328
This class has logic to check if a particular button can be launched or not.
*****************************************************************/
public class QPPAgreementCTr {
    public Account acc {get; set;}
    
    /** Code to check if the user clicking the button is a valid user or not  ***/
    public Boolean accAllowed{set;get
                            {
                                Boolean userAllowed = false;
                                String userid = Userinfo.getUserid();
                                String accownerid = acc.Ownerid;
                                if(accownerid.equalsIgnorecase(userid)){
                                    userAllowed = true;
                                }else{
                                    List<Opportunity> lOpps = new List<Opportunity>();
                                    Set<String> sPSMNames = new Set<String>();
                                    lOpps = [select id,Active_PSM__c,Active_PSM__r.Name from opportunity where accountId = :acc.Id and Active_PSM__c != null and Active_PSM__c != ''];
                                    if(!lopps.isEmpty()){
                                        for(Opportunity opp: lopps){
                                           sPSMNames.add(opp.Active_PSM__r.Name); 
                                        }
                                    } 
                                    if(sPSMNames.contains(Userinfo.getname())){
                                        userAllowed = true;
                                    }
                                }        
                                 return userAllowed; 
                            } 
                            }
    /** Code to check if there is not already a partner agreement in process  ***/                      
    public Boolean accQPPStatuscheck{set;get {
            Boolean clickAllowed = true;
            if(String.isNotBlank(acc.QPP_Status__c)){
                if((acc.QPP_Status__c.contains('Sent') || acc.QPP_Status__c.contains('Send')) && !'Sent to PAM'.equalsIgnorecase(acc.QPP_Status__c))
                    clickAllowed = false;
            }
            return clickAllowed; 
        }  
    }
    
    /*DGM-328  Start */
    public Boolean accQPPRussiacheck{set;get {
            Boolean clickAllowed = true;
            if(String.isNotBlank(acc.Territory_Country__c)){
                if(acc.Territory_Country__c.contains('Russia'))
                    clickAllowed = false;
            }
            return clickAllowed; 
        }  
    }
    /*DGM-328 End */
    
    public ApexPages.StandardController controller;
    public QPPAgreementCTr (ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()){
            stdController.addFields(new List<String>{'AccNumber__c','Owner.name','ownerid','QPP_Status__c', 'Territory_Country__c'});
        }
        acc = (Account)stdController.getRecord();
    }
}