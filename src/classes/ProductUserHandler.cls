/********************************************************
* CLASS: ProductUserTrigger
*
* CHANGELOG:    
*   2019-11-28 - BAD - Added Initial logic
*********************************************************/
public class ProductUserHandler {
    public ProductUserHandler() {}

    public static void OnBeforeInsert(Product_User__c[] newRecords) {
        List<String> lstSubName = new List<string>();

        for (Product_User__c pu : newRecords) {
            if (pu.Zuora_Subscription_ID__c != null) {
                lstSubName.add(pu.Zuora_Subscription_ID__c);
            }
        }

        if (lstSubName.size() > 0){
            Map<String, Zuora__Subscription__c> subscriptionMap = new Map<String, Zuora__Subscription__c>();
            for (Zuora__Subscription__c sub : [SELECT Id, name FROM Zuora__Subscription__c WHERE name IN : lstSubName]) {
                subscriptionMap.put(sub.name, sub);
            }

            for (Product_User__c pu : newRecords) {
                if (subscriptionMap.containskey(pu.Zuora_Subscription_ID__c)){
                    pu.Subscription__c = subscriptionMap.get(pu.Zuora_Subscription_ID__c).Id;                
                }
            }
        }
    }    

}