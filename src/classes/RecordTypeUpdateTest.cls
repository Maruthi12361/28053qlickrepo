/*************************************************
* Modifications: 
* 20151030  CCE     Initial development as there doesn't seem to be an existing test class 
* 20170608  CCE     CHG0031443 - This is to resolve an issue where it is stopping partner leads from being rejected.  See INC0095366.
*                   Test class updated to reflect changes made to the trigger.
*
* 2017-09-22 - AYS -  BMW-396/CHG0032034 - Added logic for Archived Record Type.
* 2019-11-07 - AIN -  IT-2229 - Made emails more unique
****************************************************/
@isTest
private class RecordTypeUpdateTest {
	enum PortalType {  PowerPartner, CustomerSuccess }
    static final String LEADSOURCE = 'PART - Partner';
    static final String RT_PARTEROPPREG = '012D0000000JsWAIA0';    
    static final String RT_STANDARDLEAD = '01220000000DOzwAAG';    
    
	static testMethod void createLeadAndUpdate() {
		QTTestUtils.GlobalSetUp();
        Lead l = new Lead();
        User pu = getPortalUser(PortalType.PowerPartner, null, true);
        pu = [select Id, UserType from User where Id = :pu.Id];
        System.Debug('RecordTypeUpdateTest pu.UserType = ' + pu.UserType);
        System.runAs(pu)
        {
            l.Company = 'Great Good Guys AND Girls';
            l.LastName = 'LastTest';
            l.FirstName = 'FirstTest';
            l.Country='Sweden';
            l.Email=System.now().millisecond() + 'asd@asda324234esdsd.com';
            l.Data_Validated__c = true;
            l.LeadSource = LEADSOURCE;
            l.RecordTypeId = RT_STANDARDLEAD;
                            
            insert l;
        }
        Semaphores.TriggerHasRun(1);

        User newOwner = getPortalUser(PortalType.PowerPartner, null, true);        
        l.OwnerId = newOwner.Id;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update l;
        l = [SELECT Id, RecordTypeId FROM Lead WHERE Id = :l.Id];
        system.assertEquals(RT_PARTEROPPREG, (String)l.RecordTypeId);   //test the record type has been updated

        System.Debug('RecordTypeUpdateTest l = ' + l);
	}

    static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert) {
        // Make sure the running user has a role otherwise an exception will be thrown.
        if(userWithRole == null) {               
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                
                userWithRole = new User(alias = 'hasrole', email= System.now().millisecond() +  'userwithrole@roletest1qeq3233221.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + 'userwithrole@testorg.com');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            
            System.assert(userWithRole.userRoleId != null, 
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        Account a;
        Contact c;
        System.runAs(userWithRole) {
			QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc', 'USA', 'United States');
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
            
            a = new Account(name = 'TEST ACCOUNT', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
            Database.insert(a);     
            c = new Contact(AccountId = a.id, firstname = 'Bluewolf', lastname = 'TestContact');
            Database.insert(c);
            Account_License__c[] als = new Account_License__c[]{};
            als.add(new Account_License__c(Account__c = a.Id, Name='123456789'));
            als.add(new Account_License__c(Account__c = a.Id, Name='987654321'));
            for(Account_License__c al : als)
            {
                al.O_S__c = 'nnn';
                al.Environment__c = 'nnn';
                al.Product__c = 'nnn';
                al.Area__c = 'nnn';
                al.Issue__c = 'nnn';
                al.X3rd_Party_Software__c = 'nnn';
                al.Client_Version__c = 'nnn';
                al.Server_Version__c = 'nnn';
                al.Publisher_Version__c = 'nnn';
                al.Connector_Version__c = 'nnn';
                al.Customer_Patch_Version__c = 'nnn';
            }
            insert als;
        }
        
        Profile p;
        if (portalType.name() == 'CustomerSuccess' ){
            p = [select Id from Profile where Name = 'Customer Portal Case Logging Access' LIMIT 1];
        }else if(portalType.name() == 'PowerPartner'){
            p = [select Id, UserLicense.Name from Profile where Name = 'PRM - Independent Territory + QlikBuy' LIMIT 1];
            System.debug('userLicense Name : ' + p.UserLicense.Name);
        }
        String testemail = 'palpatine@thesenate.darkside.67981637.com';
        User pu = new User(profileId = p.id, username = System.now().millisecond() + testemail, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname', contactId = c.id);
        
        if(doInsert) {
            Database.insert(pu);
        }
        return pu;
    }
	
        @isTest static void test_SuspectStausChanged1() {
        List<RecordType> l_RecordType = new List<RecordType>([
            SELECT id, DeveloperName
            FROM RecordType
        ]);
        Map<String, Id> m_RecordTypes = new Map<String, Id>();
        for(RecordType recordType : l_RecordType) {
            m_RecordTypes.put(recordType.DeveloperName, recordType.id);
        }
        
        insert new ZiftSettings__c(
            RT_StandardLead__c = m_RecordTypes.get('Standard_lead'),
            RT_StandardContact__c = m_RecordTypes.get('Business_Contact')
        );
        

        ZiftSettings__c csZift = ZiftSettings__c.getInstance();

        System.assertEquals(m_RecordTypes.get('Standard_lead'), csZift.RT_StandardLead__c);

        User u = QTTestUtils.createMockUserForProfile('System Administrator'); 
        //User u = [SELECT id FROM User WHERE username LIKE 'jch@qlikview.com%']; //for test validation rule on on DEV sandbox
        System.runAs(u) {
            Lead lead = new Lead(LastName = 'LeadName', Email = System.now().millisecond() + 'LeadName@companyadqwesease.com', Status = 'Unknown', Country = 'Country',
                Company = 'Company', FirstName ='FirstName', RecordTypeId = m_RecordTypes.get('Partner_Referral_Opp_Reg'));
            insert lead;
            
            Lead insertedLead = [SELECT id, Old_Record_Type_Name__c FROM Lead WHERE id = : lead.id];

            System.assertEquals(null, insertedLead.Old_Record_Type_Name__c);

            Semaphores.TriggerHasRun(1);
            Test.startTest();

            insertedLead.Status = 'Suspect';
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
            update insertedLead;

            Test.stopTest();

            Lead updatedLead = [SELECT id, RecordTypeId, Status FROM Lead WHERE id = : lead.id];

            System.assertEquals('Suspect', updatedLead.Status);

            System.assertEquals(m_RecordTypes.get('Standard_lead'), updatedLead.RecordTypeId);

        }
    }

    @isTest static void test_SuspectStausChanged2() {
        List<RecordType> l_RecordType = new List<RecordType>([
            SELECT id, DeveloperName
            FROM RecordType
        ]);
        Map<String, Id> m_RecordTypes = new Map<String, Id>();
        for(RecordType recordType : l_RecordType) {
            m_RecordTypes.put(recordType.DeveloperName, recordType.id);
        }
        
        insert new ZiftSettings__c(
            RT_StandardLead__c = m_RecordTypes.get('Standard_lead'),
            RT_StandardContact__c = m_RecordTypes.get('Business_Contact')
        );
        

        ZiftSettings__c csZift = ZiftSettings__c.getInstance();

        System.assertEquals(m_RecordTypes.get('Standard_lead'), csZift.RT_StandardLead__c);

        User u = QTTestUtils.createMockUserForProfile('System Administrator'); 
        //User u = [SELECT id FROM User WHERE username LIKE 'jch@qlikview.com%']; //for test validation rule on on DEV sandbox
        System.runAs(u) {
            Lead lead = new Lead(LastName = 'LeadName', Email = System.now().millisecond() + 'LeadNameaweqweawe@company.com', Status = 'Unknown', Country = 'Country',
                Company = 'Company', FirstName ='FirstName', RecordTypeId = m_RecordTypes.get('Partner_Referral_Opp_Reg'), Old_Record_Type_Name__c = 'Archived');
            insert lead;
            
            Lead insertedLead = [SELECT id, Old_Record_Type_Name__c FROM Lead WHERE id = : lead.id];

            System.assertEquals('Archived', insertedLead.Old_Record_Type_Name__c);

            Semaphores.TriggerHasRun(1);
            Test.startTest();

            insertedLead.Status = 'Suspect';
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
            update insertedLead;

            Test.stopTest();

            Lead updatedLead = [SELECT id, RecordTypeId, Status FROM Lead WHERE id = : lead.id];

            System.assertEquals('Suspect', updatedLead.Status);

            System.assertEquals(m_RecordTypes.get('Archived'), updatedLead.RecordTypeId);

        }
    }
}