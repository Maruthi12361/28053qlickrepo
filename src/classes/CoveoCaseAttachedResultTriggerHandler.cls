/********************************************************
* Trigger Handler: CoveoCaseAttachedResultTriggerHandler
* Description: Trigger handler for Attached Results object, object is part of Coveo managed package.
* Test class is CoveoCaseAttachedResultTriggerHndlrTest
*
* Change Log:
* -------
* AUTHOR      DATE        DETAIL
*
* AIN         2019-07-09  Initial Development for IT-1959
* AIN         2019-10-01  IT-2105 - Added functionality to remove other attached bugs when attaching a bug.
* AIN         2019-11-07  IT-2105 - Added logic to not delete other attached bugs without defects.
*
**********************************************************/
public with sharing class CoveoCaseAttachedResultTriggerHandler {

    public static void OnAfterInsert(List<CoveoV2__CoveoCaseAttachedResult__c> triggerNew){
        Map<Id, Id> caseToBug = new Map<Id, Id>();
        
        for(CoveoV2__CoveoCaseAttachedResult__c ar : triggerNew){
            if(ar.CoveoV2__case__c != null && ar.BugId__c != null && !caseToBug.containsKey(ar.CoveoV2__case__c))
                caseToBug.put(ar.CoveoV2__case__c, ar.BugId__c);
        }
        if(!caseToBug.isEmpty()) {
            AttachBugsToCases(caseToBug);
        }

            
    }
    public static void OnAfterDelete(List<CoveoV2__CoveoCaseAttachedResult__c> triggerOld){
        Map<Id, Id> caseToBug = new Map<Id, Id>();
        for(CoveoV2__CoveoCaseAttachedResult__c ar : triggerOld){
            if(ar.CoveoV2__case__c != null && ar.BugId__c != null && !caseToBug.containsKey(ar.CoveoV2__case__c))
                caseToBug.put(ar.CoveoV2__case__c, ar.BugId__c);
        }
        if(!caseToBug.isEmpty())
            RemoveBugsFromCases(caseToBug);
    }

    public static void AttachBugsToCases(Map<Id, Id> caseToBug){
        List<Case> cases = [select id, Bug__c from Case where id in :caseToBug.keySet()];
        for(Case casee : cases){
            if(caseToBug.containsKey(casee.Id))
                casee.Bug__c = caseToBug.get(casee.Id);
        }
        update cases;

        string query = 'select id from CoveoV2__CoveoCaseAttachedResult__c where BugId__c  != null';
        for(Id key:caseToBug.keySet()){ 
            if(key != null){
                query += ' and (CoveoV2__case__c = \'' + key + '\' and BugId__c != \'' + caseToBug.get(key) + '\')';
            }
        }
        system.debug('Query: ' +query);
        List<CoveoV2__CoveoCaseAttachedResult__c> detachedBugs = database.query(query);

        if(detachedBugs.size() > 0){
            system.debug(detachedBugs.size());
            delete detachedBugs;
        }
    }
    public static void RemoveBugsFromCases(Map<Id, Id> caseToBug){
        List<Case> cases = [select id, Bug__c from Case where id in :caseToBug.keySet()];
        for(Case casee : cases){
            if(caseToBug.containsKey(casee.Id) && casee.Bug__c == caseToBug.get(casee.Id))
                casee.Bug__c = null;
        }
        update cases;
    }
}