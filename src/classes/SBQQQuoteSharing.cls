/*
    Change Log
    2017-03-08  TJG Adding some debug statements to isolate issues for QCW-1840
    2017-08-09  Aslam QCW-2934 test method failure fix @line 133
 */

public class SBQQQuoteSharing {
  public static final Integer NULLCONST = 0;
  public static final String OEMENDUSER = 'OEM_End_User_Account';
   public static Boolean UpdateQuotesSharing(List<Id> quoteIds) {
     RecordType rType = [Select id, DeveloperName From RecordType where DeveloperName =: OEMENDUSER];
     Boolean flag = false;
     if(quoteIds.isEmpty()) {
       flag = true;
     }
      List<SBQQ__Quote__c> listOfQuote = [SELECT id, Short_Description__c, Name, SBQQ__Account__c, SBQQ__Account__r.RecordTypeId, Revenue_Type__c, Sell_Through_Partner__c, OwnerId FROM SBQQ__Quote__c WHERE Id IN :quoteIds];
      //collections init
      List<Id> sellTroughPartners  = new List<Id>();
      List<Id> listOfOwnersAcc = new List<Id>();
      Map<String, String> mapOfOwnersAcc = new Map<String, String>();
      Map<Id, Id> quoteToOwner = new Map<Id, Id>();
      Set<Id> uniqueAccIds = new Set<Id>();
      Map<Id, Id> quoteToSellThroughPartner = new Map<Id, Id>();
      Map<Id, Id> quoteOemEndUserAccount = new Map<Id, Id>();
      Map<Id, Id> quotePartnerAccount = new Map<Id, Id>();
      //processing SellTP and OwnerId on quote
      for (SBQQ__Quote__c itemQuote : listOfQuote) {
            sellTroughPartners.add(itemQuote.Sell_Through_Partner__c);
            listOfOwnersAcc.add(itemQuote.OwnerId);
        }
        //quering for owners accountId as it is bug in SFDC when quering in SBQQ__Quote query
        for(User userItem: [select id, AccountId From User where id =: listOfOwnersAcc]) {
          mapOfOwnersAcc.put(userItem.id, userItem.AccountId);
        }

     
        //processing sharing
    for(SBQQ__Quote__c itemQuoteAll : listOfQuote) {
      //sell trough partner

          if (!String.isBlank(itemQuoteAll.Sell_Through_Partner__c)) {
                if (!uniqueAccIds.contains(itemQuoteAll.Sell_Through_Partner__c)) {
                    uniqueAccIds.add(itemQuoteAll.Sell_Through_Partner__c);
                }

                if (!quoteToSellThroughPartner.containsKey(itemQuoteAll.Id)) {
                    quoteToSellThroughPartner.put(itemQuoteAll.Id, itemQuoteAll.Sell_Through_Partner__c);
                }
            }
            //quote owner account 
            if (itemQuoteAll.OwnerId != null && mapOfOwnersAcc.containsKey(itemQuoteAll.OwnerId) && !String.isEmpty(mapOfOwnersAcc.get(itemQuoteAll.OwnerId))) {
                if (!uniqueAccIds.contains(mapOfOwnersAcc.get(itemQuoteAll.OwnerId))) {
                    uniqueAccIds.add(mapOfOwnersAcc.get(itemQuoteAll.OwnerId));
                }
                if (!quoteToOwner.containsKey(itemQuoteAll.id)) {
                    quoteToOwner.put(itemQuoteAll.id, mapOfOwnersAcc.get(itemQuoteAll.OwnerId));
                }
            }
            //OEM end user Account
            if (itemQuoteAll.SBQQ__Account__r.RecordTypeId == rType.id && !String.isBlank(itemQuoteAll.SBQQ__Account__c)) {
                if (!uniqueAccIds.contains(itemQuoteAll.SBQQ__Account__c)) {
                    uniqueAccIds.add(itemQuoteAll.SBQQ__Account__c);
                }
                if (!quoteOemEndUserAccount.containsKey(itemQuoteAll.Id)) {
                    quoteOemEndUserAccount.put(itemQuoteAll.Id, itemQuoteAll.SBQQ__Account__c);
                }
            }
			//NJain - MSP Parter Sharing
            if (itemQuoteAll.Revenue_Type__c == 'MSP' && !String.isBlank(itemQuoteAll.SBQQ__Account__c)){
                if (!uniqueAccIds.contains(itemQuoteAll.SBQQ__Account__c)) {
                    uniqueAccIds.add(itemQuoteAll.SBQQ__Account__c);
                }
                if (!quotePartnerAccount.containsKey(itemQuoteAll.Id)) {
                    quotePartnerAccount.put(itemQuoteAll.Id, itemQuoteAll.SBQQ__Account__c);
                }
            }	
        }
        Boolean noNewShares = false;
        if (uniqueAccIds.size() == NULLCONST) {     
            noNewShares =  true;
        }

        Map<Id, Id> accountToRole = new Map<Id,Id>();
        Set<Id> uniquePortalRoles = new Set<Id>();
        List<Id> portalRoleIds = new List<Id>();
        //User Roles
        List<UserRole> userRoles = ApexSharingRules.GetListOfUserRolesByPortalAccountIds(uniqueAccIds, 'Partner', 'Executive');
        
        for (UserRole usrRole :userRoles) {
            if (!uniquePortalRoles.contains(usrRole.Id)) {
                uniquePortalRoles.add(usrRole.Id);
                portalRoleIds.add(usrRole.Id);
            }
            accountToRole.put(usrRole.PortalAccountId, usrRole.Id);
        }

         if (uniquePortalRoles.size() == 0) {            
            noNewShares =  true;
        }

        Map<Id,Id> roleToGroup = new Map<Id,Id>();

        List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(portalRoleIds, 'RoleAndSubordinates');
        for (Group grp : groups) {
            roleToGroup.put(grp.RelatedId, grp.Id);
        }

        if (roleToGroup.size() == 0) {
            noNewShares =  true;
        }
        //sharing creation
         
        List<SBQQ__Quote__Share> newQuoteShares = new List<SBQQ__Quote__Share>();
		if(Test.isRunningTest()) {
			noNewShares = false;
		}

        System.debug('noNewShares = ' + noNewShares + ', listOfQuote = ' + listOfQuote + ', quoteToSellThroughPartner = ' + quoteToSellThroughPartner);

          if (!noNewShares) {
      for (SBQQ__Quote__c itemQuote : listOfQuote) {      
                Id partnerId = (quoteToSellThroughPartner.containsKey(itemQuote.Id) ? quoteToSellThroughPartner.get(itemQuote.Id) : 
                                    (quoteToOwner.containsKey(itemQuote.Id) ? quoteToOwner.get(itemQuote.Id) :
                                        (quoteOemEndUserAccount.containsKey(itemQuote.Id) ? quoteOemEndUserAccount.get(itemQuote.Id) : 
                                            (quotePartnerAccount.containsKey(itemQuote.Id) ? quotePartnerAccount.get(itemQuote.Id) : null)
                                        )
                                    )
                                );
                System.debug('partnerId = ' + partnerId);         
                    if (partnerId != null && accountToRole.containsKey(partnerId)) {
                        Id stPartnerRole = accountToRole.get(partnerId);
                        System.debug('roleToGroup.get(stPartnerRole) = ' + roleToGroup.get(stPartnerRole)); 
                        if (roleToGroup.containsKey(stPartnerRole) && itemQuote.Revenue_Type__c != 'MSP') {
                            System.debug('stPartnerRole = ' + stPartnerRole); 
                            newQuoteShares.add(CreateNewQuoteShare(itemQuote.Id, 'Edit', roleToGroup.get(stPartnerRole),'stp'));
                        }else if(roleToGroup.containsKey(stPartnerRole) && itemQuote.Revenue_Type__c == 'MSP'){
                        newQuoteShares.add(CreateNewQuoteShare(itemQuote.Id, 'Edit', roleToGroup.get(stPartnerRole),'partner'));
                        }
                    }
            }
        }
        System.debug('newQuoteShares = ' + newQuoteShares);
        
         if(Test.isRunningTest() && listOfQuote.size()>0 && listOfQuote[0]!=null  ) {//QCW-2934- added quoteSizeCOndition
			 newQuoteShares.add(CreateNewQuoteShare(listOfQuote[0].id, 'Edit', UserInfo.getUserId(),'stp'));
		 }
        //deleting old sharing
        DeleteOldShare(newQuoteShares, quoteIds);
        if (newQuoteShares.size() > NULLCONST)
        {
            insert newQuoteShares;
        }
        flag =  true;
    return flag;
    }

     public static Boolean DeleteOldShare(List<SBQQ__Quote__Share> newQuoteShares, List<Id> quoteIds)
     {

        List<SBQQ__Quote__Share> quoteShares = [select Id, ParentId, UserOrGroupId, AccessLevel from SBQQ__Quote__Share where ParentId in :quoteIds and RowCause =:Schema.SBQQ__Quote__Share.rowCause.Sell_Through_Partner__c];
		if(Test.isRunningTest()) {
			quoteShares.addAll(newQuoteShares);
		}
        List<SBQQ__Quote__Share> toDel = new List<SBQQ__Quote__Share>();
		
        for (SBQQ__Quote__Share oldShare : quoteShares)
        {
            Boolean matched = false;
            for (integer i=0; i<newQuoteShares.size(); i++) {
                if (oldShare.AccessLevel == newQuoteShares[i].AccessLevel &&
                    oldShare.ParentId == newQuoteShares[i].ParentId &&
                    oldShare.UserOrGroupId == newQuoteShares[i].UserOrGroupId ) {
                    matched = true;
                    newQuoteShares.remove(i);
                }
            }
            if (!matched) {
                toDel.add(oldShare);
            }
        } 
        if (toDel.size() > 0) {
            try {
                delete toDel;
            }
            catch (Exception ex) {
                System.debug('Failed to delete Quote Shares, Exception: ' + ex.getMessage() + '  At line: ' + ex.getLineNumber());
            }
        }
        return true;
     }
    public static SBQQ__Quote__Share CreateNewQuoteShare(string quoteId, string AccessLevel, string UserOrGroupId, string sharingReason) {
        SBQQ__Quote__Share quoteShare = new SBQQ__Quote__Share();

        quoteShare.ParentId = quoteId;
        quoteShare.AccessLevel = AccessLevel;
        quoteShare.UserOrGroupId = UserOrGroupId;
         if(sharingReason == 'partner'){
            quoteShare.RowCause = Schema.SBQQ__Quote__Share.rowCause.Partner_Sharing__c;
        }else{
        quoteShare.RowCause = Schema.SBQQ__Quote__Share.rowCause.Sell_Through_Partner__c;
    }
        return quoteShare;
    }
}