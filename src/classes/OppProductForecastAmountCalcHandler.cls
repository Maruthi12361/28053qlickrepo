/*
* File Name:oppProductForecastAmountCalcHandler
* Description : This handler class is used to calculate forecast amounts on parent opportunities on creation or updation or deletion
of opportunity line items. Updation criteria being only if totalprice/quantity/discount on line item being changed.
Also as part of the change we also recalculate the amounts on the deal split child opportunities as well.
* @author : Ramakrishna Kini
* Modification Log ===============================================================
Ver     Date         Author         Modification
1.0 July 5th 2016 RamakrishnaKini     Added new trigger logic.
2017-04-14  MTM     Run in system mode
2017-05-16  Linus   Added opportunity validation rule bypass
2017-05-23  MTM     QCW-1633
2017-07-05  Linus Add inclusion of subscription only line items.
2017-07-25 Rodion Vakulvoskyi QCW-2695
2017-09-30 QCW-877 Shubham Gupta Added fix by initializing mIdDealSplitOppObjects at line 22 and adding empty check to
avoid unnecessary SOQL at line 114
2017-10-04  Linus QCW-4035 Adding support to calculate forecast amounts for Subscription items.
2017-09-23 Shubham Gupta QCW-2849 added Invokation of oppdealsplitscheduleHandler at line 127 to 133 and 171.
2018-05-11 Linus Löfberg BSL-354
2018-10-08 Linus Löfberg BSL-873
*/

public class OppProductForecastAmountCalcHandler {
    public static Boolean flagToDel = false;
    public static Boolean flagToUpdate = false;
    public static Boolean flagToInsert = false;
    private static List<OpportunityLineItem> oppLineItems;
    private static Map<ID, Opportunity> mIdOppObjects;
    private static Map<ID, Opportunity> mIdDealSplitOppObjects = new Map<ID, Opportunity>();

    public static void onBeforeInsert(List<OpportunityLineItem> inputList) {

    }

    public static void onBeforeDelete(List<OpportunityLineItem> inputList) {

    }

    /*  @Description :This common method is used to calculate the forecast amounts on the opportunities using the opp line item total price on after update trigger context.
    @parameter inputList: Trigger.new opportunity line item list
    @parameter inputMap: Trigger.newMap
    */
    public static void onAfterInsert(List<OpportunityLineItem>  inputList, Map<id, OpportunityLineItem> inputMap) {
        if(!flagToInsert) {
            flagToInsert = true;
            fetchOppLineItemsAndOpportunities(prepParentIdSet(inputList));
            List<Opportunity> finalOpps = new List<Opportunity>();
            finalOpps = calculateOppForecastAmts(oppLineItems, mIdOppObjects, mIdDealSplitOppObjects);
            if(!finalOpps.isEmpty()){
                update finalOpps;
            }
        }
    }

    /*  @Description :This common method is used to calculate the forecast amounts on the opportunities using the opp line item total price on after update trigger context.
    @parameter inputList: Trigger.new opportunity line item list
    @parameter inputMap: Trigger.newMap
    @parameter oInputList: Trigger.old opportunity line item list
    @parameter oInputMap: Trigger.oldMap
    */
    public static Map<ID, Opportunity> onAfterUpdate(List<OpportunityLineItem> inputList, Map<id, OpportunityLineItem> inputMap, List<OpportunityLineItem>  oInputList, Map<id, OpportunityLineItem> oInputMap) {
        //if(!flagToUpdate) {
        //flagToUpdate = true;
        List<OpportunityLineItem> changedOppLineItems = new List<OpportunityLineItem>();

        System.debug('******* Going in *****');
        System.debug(inputList);

        for (OpportunityLineItem oppLi: inputList) {
            System.debug('Partner_Margin__c');
            System.debug(oppLi.Partner_Margin__c);
            System.debug(oInputMap.get(oppLi.Id).Partner_Margin__c);
            System.debug('Discount__c');
            System.debug(oppLi.Discount__c);
            System.debug(oInputMap.get(oppLi.Id).Discount__c);
            System.debug('Discount');
            System.debug(oppLi.Discount);
            System.debug(oInputMap.get(oppLi.Id).Discount);
            System.debug('Net_Price__c');
            System.debug(oppLi.Net_Price__c);
            System.debug(oInputMap.get(oppLi.Id).Net_Price__c);
            System.debug('HasRevenueSchedule');
            System.debug(oppLi.HasRevenueSchedule);
            System.debug(oInputMap.get(oppLi.Id).HasRevenueSchedule);
            System.debug('Quantity');
            System.debug(oppLi.Quantity);
            System.debug(oInputMap.get(oppLi.Id).Quantity);
            System.debug('TotalPrice');
            System.debug(oppLi.TotalPrice);
            System.debug(oInputMap.get(oppLi.Id).TotalPrice);
            System.debug('Partner_Margin_RO__c');
            System.debug(oppLi.Partner_Margin_RO__c);
            System.debug(oInputMap.get(oppLi.Id).Partner_Margin_RO__c);

            if (oppLi.Discount__c != oInputMap.get(oppLi.Id).Discount__c || oppLi.Discount != oInputMap.get(oppLi.Id).Discount || oppLi.Net_Price__c != oInputMap.get(oppLi.Id).Net_Price__c ||
            oppLi.Quantity != oInputMap.get(oppLi.Id).Quantity || oppLi.OpportunityId != oInputMap.get(oppLi.Id).OpportunityId|| oppLi.Net_before_partner_margin_NS__c!= oInputMap.get(oppLi.Id).Net_before_partner_margin_NS__c ||
            oppLi.HasRevenueSchedule != oInputMap.get(oppLi.Id).HasRevenueSchedule || oppLi.TotalPrice != oInputMap.get(oppLi.Id).TotalPrice) {
                changedOppLineItems.add(oppLi);
            }
        }

        System.debug('******* Coming out *****');
        System.debug('*!*!*!*!*!*!*!' + changedOppLineItems);

        List<Opportunity> finalOpps = new List<Opportunity>();

        if (!changedOppLineItems.isEmpty()) {
            fetchOppLineItemsAndOpportunities(prepParentIdSet(changedOppLineItems));      //27.12.2016
            finalOpps = calculateOppForecastAmts(oppLineItems, mIdOppObjects, mIdDealSplitOppObjects);
        }

        System.debug(finalOpps);
        if (!finalOpps.isEmpty()) {
            update finalOpps;
        }
        System.debug('MAp of split is '+mIdDealSplitOppObjects);
        System.debug('semaphore is '+Semaphores.SplitDealOpptyProduct);
        return mIdDealSplitOppObjects;
    }

    /*  @Description :This common method is used to calculate the forecast amounts on the opportunities using the opp line item total price on after delete trigger context.
    @parameter inputList: Trigger.new opportunity list
    @parameter inputMap: Trigger.newMap
    */
    public static void onAfterDelete(List<OpportunityLineItem>  inputList, Map<id, OpportunityLineItem> inputMap) {
        if (!flagToDel) {
            flagToDel = true;
            fetchOppLineItemsAndOpportunities(prepParentIdSet(inputList));
            List<Opportunity> finalOpps = new List<Opportunity>();
            finalOpps = calculateOppForecastAmts(oppLineItems, mIdOppObjects, mIdDealSplitOppObjects);
            if (!finalOpps.isEmpty()) {
                update finalOpps;
            }
            OppDealSplitScheduleHandler.onAfterUpdate(mIdDealSplitOppObjects);
        }
    }

    /**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/

    /*  @Description :This common method is used to reset all parent opportunity forecast amounts before calculation
    @parameter Opp:  opportunity object
    @return Opportunity:  Object object whose amounts have been reset
    */
    private static Opportunity resetOppForecastAmts(Opportunity Opp) {
        //Forecast fields
        opp.License_Forecast_Amount__c = 0.00;
        opp.Subscription_Forecast_Amount__c = 0.00;
        opp.Education_Forecast_Amount__c = 0.00;
        opp.Consultancy_Forecast_Amount__c = 0.00;
        opp.Support_Forecast_Amount__c = 0.00;
        opp.Total_Maintenance_Amount__c = 0.00;

        // Forecast fields - USD
        opp.License_Amount_USD__c = 0.00;
        opp.Subscription_Amount_USD__c = 0.00;
        opp.Consultancy_Forecast_Amount_USD__c = 0.00;
        opp.Education_Forecast_Amount_USD__c = 0.00;
        opp.Misc_Forecast_Amount_USD__c = 0.00;
        opp.Support_Forecast_Amount_USD__c = 0.00;
        opp.Grand_Total_USD__c = 0.00;
        opp.Amount_USD__c = 0.00;

        return Opp;
    }

    /*  @Description :This common method is used to calculate all the parent and child opportunity forecast amounts.
    @parameter oppProducts:  List of opportunity line items
    @parameter mOppIdObjects:  Map of parent opportunities with opportunity id as key
    @parameter mDealSplitOppIdObjects:  Map of deal split child opportunity objects with opportunity id as key
    @return updatedOpps: List of updated opportunities
    */
    private static List<Opportunity> calculateOppForecastAmts(List<OpportunityLineItem> oppProducts, Map<ID, Opportunity> mOppIdObjects, Map<ID, Opportunity> mDealSplitOppIdObjects) {
        //Initialize all forecast amount variables
        Decimal totLicForeCastAmount=0.00, totSubForeCastAmount=0.00, totEduForeCastAmount=0.00, totConsultForeCastAmount=0.00, totSupportForeCastAmount=0.00, totMaintAmount=0.00;
        // List to store opportunities whose forecast amounts have been updated
        List<Opportunity> updatedOpps = new List<Opportunity>();
        System.debug(mOppIdObjects);
        for (Opportunity opp: mOppIdObjects.values()) {

            // Bypass Validation rule to be able to set the forecast fields on opportunities with a primary quote.
            opp.Bypass_Rules__c = true;

            //Reset opportunity forecast amounts
            opp = resetOppForecastAmts(opp);
            //Iterate through every product for a particular opportunity and based on family type calculate the forecast amounts
            for (OpportunityLineItem oppLi:oppProducts) {
                System.debug(oppLi);
                if(oppLi.OpportunityId == opp.Id){
                    if('Licenses'.equalsIgnoreCase(oppLi.Product_Family__c))
                    totLicForeCastAmount += oppLi.Net_Price__c;
                    if('Subscription'.equalsIgnoreCase(oppLi.Product_Family__c))
                    totSubForeCastAmount += oppLi.Net_Price__c;
                    if('Education'.equalsIgnoreCase(oppLi.Product_Family__c))
                    totEduForeCastAmount += oppLi.Net_Price__c;
                    if('Consulting'.equalsIgnoreCase(oppLi.Product_Family__c))
                    totConsultForeCastAmount += oppLi.Net_Price__c;
                    if('Support Services'.equalsIgnoreCase(oppLi.Product_Family__c))
                    totSupportForeCastAmount += oppLi.Net_Price__c;
                    if('Maintenance'.equalsIgnoreCase(oppLi.Product_Family__c))
                    totMaintAmount += oppLi.Net_Price__c;
                }
            }
            System.debug(opp);
            //Assign calculated amounts to the opportunity
            opp.License_Forecast_Amount__c = totLicForeCastAmount;
            opp.Subscription_Forecast_Amount__c = totSubForeCastAmount;
            opp.Education_Forecast_Amount__c = totEduForeCastAmount;
            opp.Consultancy_Forecast_Amount__c = totConsultForeCastAmount;
            opp.Support_Forecast_Amount__c = totSupportForeCastAmount;
            opp.Total_Maintenance_Amount__c = totMaintAmount;
            System.debug(opp);
            updatedOpps.add(opp);

            //Reset Calculation variables
            totLicForeCastAmount=0.00; totSubForeCastAmount=0.00; totEduForeCastAmount=0.00; totConsultForeCastAmount=0.00; totSupportForeCastAmount=0.00; totMaintAmount=0.00;

            // After change in the parent opp forecast amount if there are deal split children for those opportunities recalculate their forecast amounts
            if (!mIdDealSplitOppObjects.isEmpty()) {
                for (Opportunity childOpp: mDealSplitOppIdObjects.values()) {
                    if (childOpp.Deal_Split_Parent_Opportunity__c == opp.Id) {
                        childOpp.License_Forecast_Amount__c = opp.License_Forecast_Amount__c * childOpp.Deal_Split_Percentage__c/100;
                        childOpp.Subscription_Forecast_Amount__c = opp.Subscription_Forecast_Amount__c * childOpp.Deal_Split_Percentage__c/100;
                        childOpp.Education_Forecast_Amount__c = opp.Education_Forecast_Amount__c * childOpp.Deal_Split_Percentage__c/100;
                        childOpp.Consultancy_Forecast_Amount__c = opp.Consultancy_Forecast_Amount__c * childOpp.Deal_Split_Percentage__c/100;
                        childOpp.Support_Forecast_Amount__c = opp.Support_Forecast_Amount__c * childOpp.Deal_Split_Percentage__c/100;
                        childOpp.Total_Maintenance_Amount__c = opp.Total_Maintenance_Amount__c * childOpp.Deal_Split_Percentage__c/100;
                    }
                    //Add all updated child opps to the updated opps list. Since this is a lookup relationship using the same list for updation
                    updatedOpps.add(childOpp);
                }
            }
        }
        handleUSDAmountForOpps(updatedOpps);
        return updatedOpps;
    }

    /*  @Description :This common method is used to fetch all the line items, parent opportunity details and the child opportunity details from the prepared
    opp ids. The values are stored in class level variables
    @parameter parentIds:  Set of parent opp ids
    */
    private static void fetchOppLineItemsAndOpportunities(Set<Id> parentIds) {
        //27.12.2016
        oppLineItems = new List<OpportunityLineItem>();
        mIdOppObjects = new Map<ID, Opportunity>([select id,RecordTypeId, License_Amount_USD__c, Subscription_Amount_USD__c, Grand_Total_USD__c, Grand_Total__c, CurrencyIsoCode,
        Amount_USD__c, Amount, Consultancy_Forecast_Amount_USD__c, Education_Forecast_Amount_USD__c, Misc_Forecast_Amount_USD__c, Support_Forecast_Amount_USD__c, License_Forecast_Amount__c, Subscription_Forecast_Amount__c,
        Total_Maintenance_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c, Consultancy_Forecast_Amount__c, (select id,Status__c,UnitPrice,TotalPrice,Net_Price__c,Product_Family__c,IsDeleted,ListPrice,Name,OpportunityId,PricebookEntryId,Quantity from OpportunityLineItems) from Opportunity where Id in: parentIds]);
        for(Opportunity itemOpps : mIdOppObjects.values()) {
            oppLineItems.addAll(itemOpps.OpportunityLineItems);
        }
        //removed 27.12.2016
        //oppLineItems = [select id,Status__c,UnitPrice,TotalPrice,Product_Family__c,IsDeleted,ListPrice,Name,OpportunityId,PricebookEntryId,Quantity from OpportunityLineItem  where opportunityId in: parentIds];
        //mIdOppObjects = new Map<ID, Opportunity>([select id,RecordTypeId from Opportunity where Id in: parentIds]);
        mIdDealSplitOppObjects = new Map<ID, Opportunity>([select id,RecordTypeId, License_Amount_USD__c, Subscription_Amount_USD__c, Grand_Total_USD__c, Grand_Total__c, CurrencyIsoCode,
        Amount_USD__c, Amount, Consultancy_Forecast_Amount_USD__c, Education_Forecast_Amount_USD__c, Misc_Forecast_Amount_USD__c, Support_Forecast_Amount_USD__c, License_Forecast_Amount__c, Subscription_Forecast_Amount__c,
        Total_Maintenance_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c, Consultancy_Forecast_Amount__c, Deal_Split_Parent_Opportunity__c,Deal_Split_Percentage__c from Opportunity where deal_split_parent_opportunity__c in :parentIds]);
    }

    /*  @Description :This common method is used to parent opportunity set from all the product line items
    @parameter inputList:  Trigger.new opportunity line item list
    @return parentOppIds:  Set of all the parent opportunity ids from opportunity line items
    */
    private static Set<Id> prepParentIdSet(List<OpportunityLineItem> inputList) {
        //Set To store all the parent ids of the opportunities products deleted.
        Set<Id> parentOppIds = new Set<Id>();
        //Populate parent opportunity ids
        for(OpportunityLineItem oppLi: inputList){
            if(String.isNotBlank(oppLi.OpportunityId))
            parentOppIds.add(oppLi.OpportunityId);
        }
        System.debug(parentOppIds);
        return parentOppIds;
    }

    private static void handleUSDAmountForOpps(List<Opportunity> inputList) {
        List<String> isoCodesList = new List<String>();
        for (Opportunity oppItem :inputList) {
            if (oppItem.CurrencyIsoCode != null) {
                isoCodesList.add(oppItem.CurrencyIsoCode);
            }
        }
        Map<String, Double> codeToRate = new Map<String, Double>();
        List<CurrencyType> currencyTypeList = CurrencyQueryHelper.getCurrenciesByISOCodes(isoCodesList);
        for (CurrencyType currItem : currencyTypeList) {
            codeToRate.put(currItem.ISOCode, currItem.ConversionRate);
        }
        for (Opportunity oppItem : inputList) {
            if (oppItem.CurrencyISOCode != null) {
                Double cRate = codeToRate.get(oppItem.CurrencyISOCode);
                System.debug(cRate);
                if (cRate != null && cRate != 0.0) {
                    if (oppItem.License_Forecast_Amount__c != null && oppItem.License_Forecast_Amount__c != 0) {
                        oppItem.License_Amount_USD__c = oppItem.License_Forecast_Amount__c / cRate;
                    }
                    if (oppItem.Subscription_Forecast_Amount__c != null && oppItem.Subscription_Forecast_Amount__c != 0) {
                        oppItem.Subscription_Amount_USD__c = oppItem.Subscription_Forecast_Amount__c / cRate;
                    }
                    if (oppItem.Consultancy_Forecast_Amount__c != null && oppItem.Consultancy_Forecast_Amount__c != 0) {
                        oppItem.Consultancy_Forecast_Amount_USD__c = oppItem.Consultancy_Forecast_Amount__c / cRate;
                    }
                    if (oppItem.Education_Forecast_Amount__c != null && oppItem.Education_Forecast_Amount__c != 0) {
                        oppItem.Education_Forecast_Amount_USD__c = oppItem.Education_Forecast_Amount__c / cRate;
                    }
                    if (oppItem.Misc_Forecast_Amount__c != null && oppItem.Misc_Forecast_Amount__c != 0) {
                        oppItem.Misc_Forecast_Amount_USD__c = oppItem.Misc_Forecast_Amount__c / cRate;
                    }
                    if (oppItem.Support_Forecast_Amount__c != null && oppItem.Support_Forecast_Amount__c != 0) {
                        oppItem.Support_Forecast_Amount_USD__c = oppItem.Support_Forecast_Amount__c / cRate;
                    }
                    Decimal grandTotal = oppItem.License_Forecast_Amount__c + oppItem.Subscription_Forecast_Amount__c + oppItem.Education_Forecast_Amount__c + oppItem.Consultancy_Forecast_Amount__c + oppItem.Total_Maintenance_Amount__c;
                    if (grandTotal != null && grandTotal != 0) {
                        oppItem.Grand_Total_USD__c = grandTotal / cRate;
                        oppItem.Amount_USD__c = oppItem.Grand_Total_USD__c;
                    }
                }
            }
        }
    }
}