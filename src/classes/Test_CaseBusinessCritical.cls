/* 
 *	20140318	CCE This is no longer required as the part of the trigger that it was testing has now been replaced with a Boomi process
 *					CR# 9045 - https://eu1.salesforce.com/a0CD000000ZxcAW
*/
@isTest
private class Test_CaseBusinessCritical 
{
	// Testing that if Case gets Highest Severity then there is created Chatter post on the Account (Account Origin of the Case) 
	static testMethod void testSev1CaseChatterPost() 
    {
        /*
        RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'QlikTech Master Support Record Type' limit 1];
		Id pId = [Select Id From Profile Where Name Like 'System Administrator' Limit 1].Id; 
		User johnBrown = new User();
		johnBrown = [select Id from User where profileId=:pId and isActive=true limit 1];
		
		List<Account> testAccs = new List<Account>();
		
		for(Integer i=0; i< 2; i++)
		{
			testAccs.add(new Account(Name = 'Test account M'+ i));
		}
		test.startTest();
		
		insert testAccs;
						
		List<Case> testCases = new List<Case>();                
        for(Integer i=0; i< 2; i++)
        {
        	Integer no = 100 +i;
        	testCases.add(new Case(RecordTypeId = caseRT.Id, Subject = 'Test Business Critical Case N'+no, Description = 'Desc: Test Case N'+no,    
        	Category__c = 'Applications', 
        	 Requester_Name__c = johnBrown.Id, Priority = 'Medium', 
        	 Account_Origin__c = testAccs[i].Id,
        	 Severity__c = '1'));
        }
		
		insert testCases;
		
		List<Account> accs = [Select Id, Name from Account where Name like 'Test account M%' order by Id];
		List<FeedItem> feeds = [Select ParentId, Id, Body From FeedItem where ParentId in (Select Id from Account where Name like 'Test account M%') order by ParentId]; 
				
		System.assertEquals(feeds.size(), 2);
		for(Integer i=0; i<2; i++)
		{
			System.assertEquals(feeds[i].ParentId, accs[i].Id);	
			System.assertNotEquals(feeds[i].Body.contains('has now been classified as Severity 1'), Boolean.valueOf(false));		
		}
			
		test.stopTest();
		*/
    }
}