/*Test class to cover orignalPopulation class*/
@isTest
private class OriginalPopulationTest {
	static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    static final String OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
	
	@isTest static void testfieldpopulationBatch() {
		User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {
        Semaphores.SetAllSemaphoresToTrue();
		QuoteTestHelper.createCustomSettings();
		
		Subsidiary__c sub = QuoteTestHelper.createSubsidiary();
		insert sub;
		
		QlikTech_Company__c comp = QuoteTestHelper.createQlickTechCompany(sub.id);
		insert comp;
		
		RecordType rTypeEnd = [Select id, DeveloperName From RecordType where DeveloperName = 'End_User_Account' and sobjecttype='Account'];
		Account accCust = QuoteTestHelper.createAccount(comp, rTypeEnd, 'Customer');
		List<Account> accounts = new List<Account>();
		accounts.add(accCust);
		insert accounts;

		Contact con = QuoteTestHelper.createContact(accCust.id);
		insert con;

		Address__c add = QuoteTestHelper.createAddress(accCust.Id, con.Id, 'Shipping');
		insert add;

		RecordType rTypeOpp = [SELECT Id, Name from RecordType where DeveloperName like 'Sales_QCCS' and sobjecttype='Opportunity'];
		Opportunity opp = QuoteTestHelper.createOpportunity(accCust, null, rTypeOpp);
		opp.Revenue_Type__c = 'Direct';
		insert opp;

		RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote' and sobjecttype='SBQQ__Quote__c'];
		SBQQ__Quote__c quote = QuoteTestHelper.createQuote(rTypeQuote, con.id, accCust, null, 'Direct', 'Open', 'Quote', false, opp.Id);
		insert quote;
		List<SBQQ__Quote__c> topass = new List<SBQQ__Quote__c>();
		topass.add(quote);

		Product2 product = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
		insert product;

		SBQQ__QuoteLine__c quoteLine = QuoteTestHelper.createQuoteLine(quote.Id, product.Id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        insert quoteLine;

            Test.startTest();

            OriginalPopulation op = new OriginalPopulation();
            Database.QueryLocator ql = op.start(null);
            op.execute(null,topass);
            op.finish(null);

            Test.stopTest();
        }
	}
	
}