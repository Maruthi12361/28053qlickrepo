/*******************************************
*
*		2019-04-04 AIN Minimalistic controller for pages and component that needs less information
*
* 2019-11-21 AIN IT-2320 Added filter expression for Coveo
*******************************************/
public with sharing class QS_MiniController {

	public string LoginPageURL { get; set; }
    public string CSSBaseURL { get; set; }
    public string LiveAgentAPIEndpoint { get; set; }
    public string OrganizationId { get; set; }
    public string LiveChatDeploymentId { get; set; }
    public string UserName { get {return (UserInfo.getName());} }
    public string UserId { get {return (UserInfo.getUserId());} }
    public string FirstName { get {return (UserInfo.getFirstName());} }
    public boolean supportSidebar { get; set; }
    public string CoveoFilterExpression { get; set; }

	public QS_MiniController() {
		QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        LoginPageURL = partnerPortalURLs.Support_Portal_Login_Page_Url__c;
        CSSBaseURL = partnerPortalURLs.Support_Portal_CSS_Base__c;
        LiveAgentAPIEndpoint = partnerPortalURLs.Support_Portal_Live_Agent_API_Endpoint__c;
        CoveoFilterExpression  = partnerPortalURLs.Coveo_Filter_Expression__c;                   
        OrganizationId = GetOrganizationId();
        LiveChatDeploymentId = GetLiveChatDeploymentId();
        if(ApexPages.currentPage() != null && ApexPages.currentPage().getUrl()!=null)
            supportSidebar = ApexPages.currentPage().getUrl().toLowerCase().contains(Page.QS_CoveoSearch.getUrl());
	}
	public string GetOrganizationId() {
        String orgId = UserInfo.getOrganizationId().Mid(0, 15);
        return orgId;
    }

    public string GetLiveChatDeploymentId() {

        system.debug('LiveChatDeployment AIN: ' + LiveChatDeploymentId);
        List<LiveChatDeployment> lcds = [select Id, MasterLabel from LiveChatDeployment where MasterLabel = 'Support Deployment' limit 1];

        if (lcds.size() > 0) {
            LiveChatDeployment lcd = lcds[0];
            return String.valueOf(lcd.Id).Mid(0, 15);
        } else
                return '';
    }
}