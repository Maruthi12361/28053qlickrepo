//*********************************************************/
// Author: Mark Cane&
// Creation date: 27/09/2010
// Intent:  
//          
// Change History
// --------------
// 2011/09/09 JEM CR# 2891 - https://eu1.salesforce.com/a0CD000000EWwDG
// 2011/09/13 RDZ Changing the test to follow spec from CR#2891 link above
// 2012/02/27 RDZ Add Qliktech company (Field Required) on Account Creation. 
// 2015/10/22 IRN Added a efTestSuiteHelper.createCustomSettings(); since the winter 16 release we can not use seealldata and have to create it instead
// 06.02.2017   RVA :   changing QT methods	
//*********************************************************/

@isTest
private class efTestSuiteRegistrationIntController {
    private static Events__c e;
    private static User registrantUser; 
	static Id AccRecordTypeId = '01220000000DOFu';  //End User Account
    static testMethod void myUnitTest(){

        efTestSuiteHelper.createCustomSettings();
        
        setup();
        
        PageReference p;
                                
        // set runAs for new user.
        System.runAs(registrantUser){
            // set the query string with event id and initialise.
            ApexPages.currentPage().getParameters().put('eventId', e.Id);
            
            efRegistrationInternalController c = new efRegistrationInternalController();        
            System.assert(c.isValidEventId);
            
            System.assert(c.getSalutationSelectOptions().size()>0);
            System.assert(c.getUsStateSelectOptions().size()>0);
            System.assert(c.getCaStateSelectOptions().size()>0);
            System.assert(c.getCountrySelectOptions().size()>0);
            System.assert(c.getJobAreaSelectOptions().size()>0);
            System.assert(c.getCategorySelectOptions().size()>0);
            System.assert(c.getRegionSelectOptions().size()>0);
            
            // Test& - Validation failure.          
            c.editMode = true;
            c.savedMode = false;            
            p = c.submit();
            System.assert(c.errorMessage.length()>0);
            
            // set dummy reg
            populateRegFields(c.rw);
            
            // Test& - validate and transition to readonly/review view.
            c.editMode = true;
            c.savedMode = false;            
            p = c.submit();
            System.debug(c.errorMessage);
            System.debug('We expected c.editMode=false and is: ' + c.editMode);
            System.assert(!c.editMode);
            
            // Test& - back to edit view.
            c.editRegistration();
            System.assert(c.editMode);

            // Test& - and back to readonly view.           
            p = c.submit();
            System.assert(!c.editMode);
            
            // Test& - from readonly, submit invokes save.
            p = c.submit();
            // test edit mode and savedMode        
            System.assert(c.savedMode);
                                    
            // Test& - reload registration fail then success.
            ApexPages.currentPage().getParameters().put('eventId', 'error');
            c = new efRegistrationInternalController(); 
            System.assert(!c.isValidEventId);
            
            ApexPages.currentPage().getParameters().put('eventId', e.Id);
            c = new efRegistrationInternalController(); 
            System.assert(c.isValidEventId);            
        }
    }
        
    static void setup(){
        // create an event.
        
        Id pId = [Select Id From Profile Where Name = 'System Administrator'].Id; 
        Id rId = [Select Id From UserRole Where Name = 'CEO'].Id;
        string tt = Datetime.now().format('ymd');
        /*
        User adminUser = new User(alias = 'adminu', email='adminuser@testorg.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = pId,
                            timezonesidkey='America/Los_Angeles', username='adminuser' + tt + '@testorg.com', userRoleId=rId);
        insert adminUser;
        */
        User adminUser = [Select Name, Id from User where profileid =:pId and userRoleId=:rId and IsActive = true limit 1];
        //CreateUser();        
        PriceBook2 pb = new PriceBook2();
        pb.Name = 'Unit Test Event Pricebook';
        pb.IsActive = true;
        pb.Description = 'Unit Test Event Pricebook';
        insert pb;      
                
        Document td = new Document();
        td.Name = 'UNIT TEST EVENT THUMBNAIL';
        td.DeveloperName = 'UNIT_TEST_EVENT_THUMBNAIL';
        td.FolderId = [Select Id From Folder Where Name='Eventforce' And Type='Document' Limit 1].Id;
        insert td;
        
        Document bd = new Document();
        bd.Name = 'UNIT TEST EVENT BANNER';
        bd.DeveloperName = 'UNIT_TEST_EVENT_BANNER';
        bd.FolderId = [Select Id From Folder Where Name='Eventforce' And Type='Document' Limit 1].Id;
        insert bd;
        
        e = new Events__c();
        e.Name = 'Unit Test Internal Event';
        e.isActive__c = true;       
        e.Event_Start_Date__c = Date.today().addDays(30);
        e.Event_End_Date__c = Date.today().addDays(35);
        e.Venue_Name__c = 'EIC';        
        e.Venue_Website__c = 'http://www.eic.com';
        e.Venue_City__c = 'Edinburgh';
        e.Website__c = 'http://www.eventforce.com';
        e.Contact_Email__c = 'event@force.com';
        e.Banner_Image_Document__c = td.DeveloperName;
        e.Thumbnail_Image_Document__c = bd.DeveloperName;
        e.Pricebook_Name__c = pb.Name;
        e.Opportunity_Owner__c = adminUser.Id;      
        insert e;   
        
        pId = [Select Id From Profile Where Name Like 'Eventforce Portal – Employee' Limit 1].Id; 
        
        // create an Eventforce Portal profile user.
        System.runAs(adminUser){     
		/*         
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();      
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
                Country_Name__c = 'USA',
				Subsidiary__c = testSubs1.id         
            );
            insert QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c, Country_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account a = new Account();
            a.Name = 'Unit Test Account';
            a.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
            a.Billing_Country_Code__c = QTComp.Id;
            insert a;
        */
			User testUser = [Select id From User where id =: UserInfo.getUserId()];
			Account  a = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        a.RecordtypeId = AccRecordTypeId;
	        update a;  
            Contact c = new Contact();
            c.FirstName = 'Claire';
            c.LastName = 'Smythe';
            c.accountId = a.Id;
            insert c;
                         
            registrantUser = new User(alias = 'portalu', email='portaluser@testorg.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = pId,
                            timezonesidkey='America/Los_Angeles', username='portaluser@testorg.com',
                            contactId = c.Id);
            insert registrantUser;
            
            AccountShare ass = new AccountShare();
            ass.AccountId = a.Id;
            ass.AccountAccessLevel = 'Edit';
            ass.CaseAccessLevel = 'None';
            ass.ContactAccessLevel = 'Edit';
            ass.OpportunityAccessLevel = 'None';
            ass.UserOrGroupId = registrantUser.Id;
            insert ass;
        }   
    }
    
    // & temp helpers.
    public static void populateRegFields(efRegistrationInternalController.RegistrationWrapper rwDummy){     
        //Comments not in use for this event
        //rwDummy.setSalutation('Miss.');
        //rwDummy.setStreet('12 The High Street');
        //rwDummy.setCity('London');
        //rwDummy.setState('Greater London');
        //rwDummy.setPostcode('E17 8KI');
        //rwDummy.setCountry('GB');
        rwDummy.setBusinessPhone('+12345');
        rwDummy.setMobilePhone('+12345');
        rwDummy.setJobArea('IT');
        rwDummy.setJobCategory('IT Operations');
        rwDummy.setJobRegion('UK');
        
        
        rwDummy.setFirstName('Sophie');
        rwDummy.setLastName('Grigson');
        rwDummy.setEmail('sftest@qlikview.com.qttest');
        rwDummy.setTitle('Chef');
        rwDummy.setFirstNameOnBadge('Sophie');
        rwDummy.setLastNameOnBadge('Grigson');
        
        
        rwDummy.setEmergencyContactName('Sophie Grigson');
        rwDummy.setEmergencyContactPhone('+1234');
        rwDummy.setEmergencyContactRel('Colleague');
        
        rwDummy.setFlightInfoPassportName('Claire Smythe');
        rwDummy.setFlightInfoDoB('25021972');
        rwDummy.setFlightInfoCitizenship('UK');
        rwDummy.setFlightInfoGender('Female');
        rwDummy.setFlightInfoIssuingCountry('UK');
        rwDummy.setFlightInfoPassportExpiry('19122015');
        rwDummy.setFlightInfoPassportNumber('123121212');
        rwDummy.setFlightInfoSpecialNeeds('Video on demand');
                
        //Hotel info
        rwDummy.setHotelInfoCheckinDate('1/17/2011');
        //rwDummy.setHotelInfoCheckoutDate('1/27/2011'); Not in use for this event
        rwDummy.setHotelInfoRoommate('Claire');
        rwDummy.setHotelInfoRoommateLastName('Smith');
        
        //Accept T&C
        rwDummy.setRegistrationTermsAccepted(true);
        rwDummy.setentryRequirementsAccepted(true);
        
        rwDummy.setGroundTransportArrDate('1/17/2011');
        rwDummy.setGroundTransportArrTime('12:00');
        rwDummy.setGroundTransportArrAirline('BA');
        rwDummy.setGroundTransportArrFlightNo('BA1234');
        rwDummy.setGroundTransportArrAirport('Orlando');
        rwDummy.setGroundTransportDepDate('1/27/2011');
        rwDummy.setGroundTransportDepTime('18:00');
        rwDummy.setGroundTransportDepAirline('BA');
        rwDummy.setGroundTransportDepFlightNo('BA4321');
        rwDummy.setGroundTransportDepAirport('Orlando');   
        
        //New fields about allergies
        rwDummy.setAllergyShellfish(true);  
        rwDummy.setAllergyNuts(true);
        rwDummy.setAllergyLactose(true);
        rwDummy.setAllergyGluten(true);
        rwDummy.setAllergyStoneFruit(true);
        rwDummy.setDietaryVegetarian(true);
        rwDummy.setDietaryKosher(true);
        
        //Medical conditions
        rwDummy.setMedicalDisability(false);
        rwDummy.setMedicalHeartProblem(false);
        rwDummy.setMedicalAsthmatic(false);
        rwDummy.setMedicalAdditional('None');
        rwDummy.CampInvite = 'Basecamp';
    }   
}