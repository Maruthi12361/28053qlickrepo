/********************************************************

ULC_CleanUpValidationAccounts
Apex implementation of the web service class within ULCv3.

ChangeLog:
2015-02-10	AIN 	Initial implementation
***********************************************************/

global with sharing class ULC_CleanUpValidationAccounts implements Schedulable {
	global static void execute (SchedulableContext sc)
	{
		CleanUpValidationAccounts();
	}
	public static void CleanUpValidationAccounts()
	{
		//List<Account> accounts = [SELECT Id FROM Account WHERE IsValidationAccount__c = true AND Id NOT IN (SELECT AccountId FROM Contact WHERE Account.IsValidationAccount__c = true) limit 100];
		List<Account> accounts = [SELECT Id FROM Account WHERE IsValidationAccount__c = true AND 
		Id NOT IN (SELECT AccountId FROM Contact WHERE Account.IsValidationAccount__c = true) AND 
		Id NOT IN (SELECT AccountId FROM Case WHERE Account.IsValidationAccount__c = true)
		limit 100];
		if(accounts.size() > 0)
		{
			try
			{
				delete accounts;
			}
			catch(DmlException ex)
			{
				system.debug('Failed to delete accounts, ' + ex.getMessage());
			}
		}
	}
}