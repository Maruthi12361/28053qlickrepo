public class CurrencyUtil {

    public static Map<String, CurrencyWrapper> currencies;
    
    public static Decimal getAmountInOrgCurrency(Decimal amount, String convertFrom, String convertTo) {
        currencies = getCurrencies();
        Decimal amountInOrgCurrency = (amount != null) ? amount : 0;
        
        if (!convertFrom.equalsIgnoreCase(convertTo)) { 
                if(currencies.get(convertFrom).getConversionRate() != null){
                     amountInOrgCurrency.setScale(currencies.get(convertFrom).getDecimalPlaces());
                     amountInOrgCurrency = (amountInOrgCurrency / currencies.get(convertFrom).getConversionRate()) * currencies.get(convertTo).getConversionRate();
                   
                }
           }
           System.Debug('amountInOrgCurrency===='+amountInOrgCurrency);
        return amountInOrgCurrency;
    }

    public static Map<String, CurrencyWrapper> getCurrencies() {
        Map<String, CurrencyWrapper> currencyMap = new Map<String, CurrencyWrapper>();
        for (SObject cur : Database.query('Select c.IsoCode, c.IsCorporate, c.IsActive, c.DecimalPlaces, c.ConversionRate From CurrencyType c'))
        {
            currencyMap.put((String)cur.get('IsoCode'), new CurrencyWrapper(cur));
        }
        
        return currencyMap;
        
    }

}