/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_MyCaseDataController {

public string UserId{get{return (UserInfo.getUserId());}}
public User currentUser { get; set; }
//Page template & home page
public Boolean unauthenticated {get; set;}
public Boolean isPartnerAccount {get; set;}
public String Casenumber{get; set;}
public String Severity{get; set;}
public Account Acc{ get; set; }
public List<Case> resultsOpen { get; set; }
public Date FromDate {get;set;}
public Date ToDate {get;set;}
public integer count1{get; set;}
public integer count2{get; set;}
public integer count3{get; set;}
public integer count4{get; set;}
public integer count5{get; set;}
public integer count6{get; set;}
public integer count7{get; set;}
public integer count8{get; set;}
public Static Boolean allcases { get; set; }
public boolean selectedAccountNameHasChanged {get; set;}
public boolean resultsLicensesNeedsUpdate { get; set; }
public boolean resultsCaseNeedsUpdate { get; set; }
public List<SelectOption> accountOptionListFiltered {get; set;}
public String AccountFilterText {get; set;}
public String selectedEnviron{get; set;}
public Category_Lookup__c categoryLookupSelectedRecord {get; set;}
// Category Level 1 Picklist
public List<SelectOption> categoryLookupPicklist {get; set;}
public String selectedFilter1 {get; set;}
 // Category Level 2 Picklist
public List<SelectOption> categoryLookupFilter2 {get; set;}
public String selectedFilter2 {get; set;}
    


private transient User v_objUser;
    public User objUser {
        get {
            if(v_objUser == null)
                v_objUser = [Select id, name, accountid, Contact.Account.ispartner From User  where Id = :userinfo.getUserId()  LIMIT 1];
            return v_objUser;
        }
        set {
            v_objUser = value;
        }
        
    }
    private transient boolean v_isPartner;
    public boolean IsPartner {
        get {
            if(v_isPartner == null)
                v_isPartner = (objUser.Contact.Account.ispartner);
            return v_isPartner;
        }
        set {
            v_isPartner = value;
        }
    }

    private transient List<Account> v_accountList;
    public List<Account> accountList {
        get {
            if(v_accountList == null) {
              
                if(!IsPartner) {
                    Id accountId = objUser.AccountId;
                    v_accountList = [Select Id, Name, RecordtypeId, RecordType.Name  From Account 
                                    Where Id = :accountId LIMIT 1];
                } else {
                    v_accountList = [Select Id, Name, RecordtypeId, RecordType.Name
                                    From Account Order By Name, RecordType.Name limit 1000];
                }
            }
            return v_accountList;
        }
        set {
            v_accountList = value;
        }
    }
   

    private transient List<SelectOption> v_accountOptionList;
    public List<SelectOption> accountOptionList {   
        get {
            if(v_accountOptionList == null){
                       
                v_accountOptionList = new list<SelectOption>();
                for(Account acc : accountList) {
                    v_accountOptionList.add(new SelectOption(acc.Id, acc.Name));
                }
            }
            
            return v_accountOptionList;
        }
        set {
            v_accountOptionList = value;
        }
    }
    public integer accountListSize {
        get {
            return accountList.Size();
        }
        set {
        }
    }

private string v_selectedAccountName;
    public String selectedAccountName {
        get {
            if(v_selectedAccountName == null)
                v_selectedAccountName = '';
            return v_selectedAccountName;
        }
        set {
            if(v_selectedAccountName != value) {
                v_selectedAccountName = value;
                selectedAccountNameHasChanged = true;
                resultsCaseNeedsUpdate = true;
                resultsLicensesNeedsUpdate = true;
            }
        }
    }


    
    
    public QS_MyCaseDataController() {
     //  system.debug('QS_ProfilePageController : UserID'+ Userid);
     
       selectedAccountName = '';
       selectedFilter1 = '';
       selectedFilter2= '';
       Casenumber = '';
       Severity = '';
       AccountFilterText = '';
       isPartnerAccount = false;
       
        // retrieving the current user info
       if(userinfo.getUserId() != null) {
            currentUser = [Select Id, Name, FullPhotoUrl , smallphotourl, ContactId, Contact.AccountId, Contact.Persona__c, Contact.Name, Contact.Account.Name, Contact.Account.IsPartner, Contact.Account.IsCustomerPortal, 
                Contact.Account.QT_Designated_Support_Contact__c, Contact.Account.Support_Office__c, Contact.Account.Partner_Support_Contact__c, Contact.Account.Responsible_Partner_count__c, Contact.Account.Navision_Status__c, AccountId
                From User where Id = :userinfo.getUserId() LIMIT 1];
        }
        // Specifying whether the current user is a customer or partner
        isPartnerAccount = (currentUser != null && currentUser.ContactId != null && currentUser.Contact.AccountId != null && currentUser.Contact.Account.IsPartner != null) ? currentUser.Contact.Account.IsPartner : false;

        
        
        unauthenticated = (currentUser == null || currentUser.ContactId == null || currentUser.AccountId == null) ? true : false;
       
       
        User userRec = [select Contact.Id, Contact.AccountId from User where Id = :UserInfo.getUserId() limit 1];
        accountId = userRec.Contact.AccountId;
        contactId2 = userRec.Contact.Id;
        FilterAccounts();
       

       
       
       categoryLookupPicklist = new List<SelectOption>();
        selectedFilter1 = '';
    
        categoryLookupFilter2 = new List<SelectOption>();
        selectedFilter2 = '';
        
      //  selectedPartnerCon = new List<SelectOption>();
     //   SpartnerConList  = '';
        
       
        
        // populate the categoryLookupList with the unique set of category lookup name
            List<Category_Lookup__c> categoryLookupList = filterCategoryRecords('', '');
            
            if(categoryLookupList != null && (!categoryLookupList.isEmpty())) {
                Set<String> uniqueCategoryNameSet = new Set<String>();
                List<String> uniqueCategoryNameList = new List<String>();
                categoryLookupPicklist = new List<SelectOption>();
                for(Category_Lookup__c categoryLookup : categoryLookupList) {
                    if(!uniqueCategoryNameSet.Contains(categoryLookup.Name))
                    {
                        uniqueCategoryNameList.add(categoryLookup.Name);
                        uniqueCategoryNameSet.add(categoryLookup.Name);
                    }
                }
                
                if(uniqueCategoryNameList != null && (!uniqueCategoryNameList.isEmpty())) {
                    for(String uniqueCategoryName : uniqueCategoryNameList) {
                        categoryLookupPicklist.add(new SelectOption(uniqueCategoryName, uniqueCategoryName));
                    }
                }
            }
         if (allcases == null) {
            allcases = false;
        }
         getCases();
         
         getAreacount();

     
     
     
  }
  
   // accountID of existing user
    private Id accountId;

   // contact ID
    private Id contactId2;
    
     public boolean DSEAccess {get; set;}
    public String DSEName {get; set;}
  
  public void FilterAccounts() {
        system.debug('FilterAccounts start');
        
        Id contactId, accId = null, Acc;
        string Accounttype ='';
        User usr = [Select contactid from User where id =: Userinfo.getUserid()];
        if(usr !=null) {
            contactId = usr.contactId;
            List<Contact> contacts = new List<Contact>();
            contacts = [Select AccountId, Account_Type__c, DSE_Access__c, Designated_Support_Engineer__c from Contact where id =: contactId2];
            if(contactId != null && contacts.Size() > 0) {
                Contact contacttype = contacts[0];
                accId = contacttype.AccountId;
                DSEAccess = contacttype.DSE_Access__c ;
                DSEName = contacttype.Designated_Support_Engineer__c ;
                if(contacttype.Account_Type__c != null)
                    Accounttype = contacttype.Account_Type__c ;    
            }
        }
                
        List<SelectOption> newFilteredList = new List<SelectOption>();
        string filterText = AccountFilterText.toLowerCase();

        if(accountOptionList != null)
            for(SelectOption so : accountOptionList)
                if(so.getLabel().toLowerCase().contains(filterText))
                    newFilteredList.Add(so);

        accountOptionListFiltered = new List<SelectOption>();
        
        
        system.debug('newFilteredList.Size(): ' + newFilteredList.Size());
        if(newFilteredList.size() > 100) {
            accountOptionListFiltered.add(new SelectOption('','Too many accounts, please use the filter above ('+newFilteredList.Size()+')'));        
        }
        else if(newFilteredList.size() > 0) {
            if(newFilteredList.size() > 1)
                accountOptionListFiltered.add(new SelectOption('','Please select a value ('+newFilteredList.Size()+')'));
            accountOptionListFiltered.addAll(newFilteredList);
        }
        else
            accountOptionListFiltered.add(new SelectOption('','No accounts found, please use the filter to find an account'));        

        if(accountOptionListFiltered.size() > 0)
        {
            selectedAccountName = accountOptionListFiltered[0].getValue();
          //  GetEnvironments();
            }
     
        
        system.debug('FilterAccounts end');
    }
    
  
  private void getAreacount() {
 
            
             count1 = [Select count() from Case where Area__c =: 'I have an installation issue'];
            count2 = [Select count() from Case where Area__c =: 'I have a performance issue'];
            count3 = [Select count() from Case where Area__c =: 'I have a setup and configuration issue'];
            count4 = [Select count() from Case where Area__c =: 'I have an issue with objects and design'];
            count5 = [Select count() from Case where Area__c =: 'I have an issue with App- and Document Development'];
            count6 = [Select count() from Case where Area__c =: 'I have an issue with the Qlik Websites'];
            count7 = [Select count() from Case where Area__c =: 'I have a license issue'];
            count8 = [Select count() from Case where Area__c =: 'I have a login and access issue'];     
            
            
            
        
    }

 // Method used to query the Category_Lookup__c custom object    
    private List<Category_lookup__c> filterCategoryRecords(String conditions, String limits) {
        
        String queryStr = 'Select Id, Name, Article__c, Service_Request_Type__c, Case_Owner__c, Record_Type_Name__c, Category_Level__c, Category_Level_3__c, Channel__c, '+
                                                    ' Attachment_Upload_Prompt__c, Environment__c, Question_Prompt_1__c, Question_Prompt_2__c, Question_Prompt_3__c, Question_Prompt_4__c, '+
                                                    ' Question_Prompt_5__c, Severity__c, Unauthenticated__c '+
                            ' From Category_Lookup__c ' + conditions + 
                            ' Order by Category_1_Order__c, Category_2_Order__c ' + limits;
        List<Category_Lookup__c> categoryLookupList = ((List<Category_Lookup__c>) Database.Query(queryStr));
        
        return categoryLookupList;
    }
    
    // Method returns the second category selection when the first category is selected
    public PageReference filterCategoryLookupList() {
        
        if(selectedFilter1 != null && selectedFilter1 != '') {
            List<Category_Lookup__c> categoryLookupList = filterCategoryRecords(' where Name = \''+String.escapeSingleQuotes(selectedFilter1)+'\'', '');
            selectedFilter2 = null; 
           
            categoryLookupFilter2 = null;
          
            if(categoryLookupList != null && (!categoryLookupList.isEmpty())) {
             
        
                categoryLookupFilter2 = new List<SelectOption>();
                Set<String> uniqueCategoryNameSet = new Set<String>();
                for(Category_Lookup__c categoryLookup : categoryLookupList) {
                    uniqueCategoryNameSet.add(categoryLookup.Category_Level__c);
                }
                
                if(uniqueCategoryNameSet != null && (!uniqueCategoryNameSet.isEmpty())) {
                    for(String uniqueCategoryName : uniqueCategoryNameSet) {
                        categoryLookupFilter2.add(new SelectOption(uniqueCategoryName, uniqueCategoryName));
                    }
                }
              
              
              
              
              if( userinfo.getUserId() == null  ) {
            system.debug('~~~~buildCaseDetailSection -4 ');
            system.debug('~~~~buildCaseDetailSection - unauthenticatedUser ');
          
            String currentRetUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/QS_CaseWizard';//URL.getCurrentRequestUrl().toExternalForm(); //
           
            if(selectedFilter1 != null && selectedFilter1 != '' && currentRetUrl != null && currentRetUrl != '') {
                currentRetUrl += '?selectedCategory1='+selectedFilter1;
                if(selectedFilter2 != null && selectedFilter2 != '') {
                    currentRetUrl += '&selectedCategory2='+selectedFilter2;
                    
                }
            }
            categoryLookupSelectedRecord = categoryLookupList[0];
            QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
            string LoginPageURL = partnerPortalURLs.Support_Portal_Login_Page_Url__c;

            //String unAuthenticatedUserMsg = '<h3>Please <a href="http://login-stage.qlik.com/login.aspx?returnurl='+currentRetUrl+'">Login / Register</a> before raising the case. </h3><br />';
            String unAuthenticatedUserMsg = '<h3>Please <a href="' + LoginPageURL + '?u='+currentRetUrl+'">Login</a> before raising the case. </h3><br />';
            
           if(categoryLookupSelectedRecord != null && categoryLookupSelectedRecord.Unauthenticated__c == true) {
                } else {
           
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,unAuthenticatedUserMsg));
            }
        }
              
              
            }
        }
        else
        {
            selectedFilter2 = null;
            categoryLookupFilter2 = new List<SelectOption>();            
          //  categoryLookupFilter2 = null;
            categoryLookupSelectedRecord = null;
         }
        return null;
    }
    
    public PageReference processRequests(){
        getCases();
        return null;
    }
    
    private void getCases()
    {
        system.debug('getCases start');
       // List<Case> results;
        
        system.debug('selectedAccountName : ' + selectedAccountName);
        system.debug('selectedFilter1 : ' + selectedFilter1);
        system.debug('selectedFilter2 : ' + selectedFilter2);
        system.debug('Casenumber : ' + Casenumber);
        system.debug('Severity : ' + Severity );
        system.debug('selectedEnviron : ' + selectedEnviron);
        
        String QueryCase = '';      
        String conId = [select contactId from User where Id = :UserInfo.getUserId() limit 1].contactId;      
        List<Case> results = new List<Case>(); 
        
        // Please Dont remove this , This is use only for where clause 
        String cinidd = selectedAccountName  ;
        
        QueryCase = 'select Id, CaseNumber, Account_Origin__r.name , Account.name , AccountId, License_No__c, Subject, Severity__c, ClosedDate, Account_Origin__c, Status, CreatedDate, Environment__c, EnvironmentOfCase__r.name from Case ';
      
      
         if (allcases) {//mineonly cases
         //  results = [select Id, CaseNumber, Account.name , AccountId, License_No__c, Subject, Severity__c, ClosedDate, Account_Origin__c, Status, CreatedDate, EnvironmentOfCase__r.name from Case where contactId = :contactId order by CreatedDate desc LIMIT 1000];// order by CreatedDate desc]
           QueryCase += 'where contactId = :conId '; 
       
         } else { //All cases
       //  results = [select Id, CaseNumber,Account.name , AccountId, License_No__c, Subject, Status, Severity__c, ClosedDate, Account_Origin__c, CreatedDate, EnvironmentOfCase__r.name from Case order by CreatedDate desc LIMIT 1000]; //order by CreatedDate desc
             QueryCase += 'where contactId != :cinidd '; 
         }
        
                                                                                  
               if (selectedAccountName != null && selectedAccountName != ''){ 
                         QueryCase += ' and (Account_Origin__c = :selectedAccountName or AccountId =:selectedAccountName) ';                     
                } 
                     
               if (selectedFilter1 != null && selectedFilter1 != ''){ 
                         QueryCase += ' and Area__c = :selectedFilter1 ';                     
                } 
                 if (selectedFilter2 != null && selectedFilter2 != ''){ 
                         QueryCase += ' and Issue__c = :selectedFilter2 ';                     
                } 
                     
               if (Casenumber != null && Casenumber != ''){ 
                         QueryCase += ' and (CaseNumber = :Casenumber or Subject =:Casenumber) ';                     
                } 
                 if (Severity != null && Severity != ''){ 
                         QueryCase += ' and Severity__c = :Severity ';                     
                } 
                
                if (selectedEnviron != null && selectedEnviron != ''){ 
                         QueryCase += ' and EnvironmentOfCase__r.id = :selectedEnviron ';                     
                } 
               
            
                     
                     QueryCase += ' order by CreatedDate desc LIMIT 1000';
                     
                     results = (List<Case>) Database.query(QueryCase );
                         
                     system.debug('results : ' + results ); 
        

       
        resultsOpen = new List<Case>();
        
        if(results != null && (!results.isEmpty())) {
            for (Case caseObj : results) {
                system.debug('Case.CaseNumber: ' + caseObj.CaseNumber);
                if (caseObj.Status != null)
                {
                            resultsOpen.add(caseObj);                    
                   
                }
            }
        }
       
        system.debug('Amount open: ' + resultsOpen.size());
        

       
    }


   private transient List<SelectOption> v_ENVOptionList;
   public List<SelectOption> ENVOptionList {   
        get {
            if(v_ENVOptionList == null){
                       
                v_ENVOptionList = new list<SelectOption>();
                for(Environment__c envv:  [SELECT Id, name From Environment__c e WHERE (e.Product_License__r.EndDate >= TODAY or e.Product_License__r.Entitlement_Check__c = true) ORDER BY Last_Used_Date__c DESC NULLS Last]) {
                    v_ENVOptionList.add(new SelectOption(envv.Id, envv.Name));
                }
            }
            
            return v_ENVOptionList;
        }
        set {
            v_ENVOptionList = value;
        }
    }







}