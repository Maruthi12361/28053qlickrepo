/*
 File Name: CustomAccountTrigger

 2018-01-01 Viktor@4Front QCW-4609, QCW-4760 Trigger consolidation.
 29/5/2018 BSL-418 done some modifications   shubham gupta
 11/1/2018 BSL-1216 Added Method handleDealDeskSupportUpdate to dummy update all open opties to the account in context whose named account or segment is changed
*/

public with sharing class CustomAccountTriggerHandler {

/*
 Trigger to prevent delete / mergew of accounts with Navision or NetSuite id
 Moved from trigger Account_ControlDelete on Account (before delete)

*/
    public static void handleAccountControlDelete(List<Account> inAccounts) {

        for (Account a : inAccounts) {
            if ((a.Navision_Customer_Number__c != null && a.Navision_Customer_Number__c != '')
                    || (a.INT_NetSuite_InternalID__c != null && a.INT_NetSuite_InternalID__c != '')) {
                System.debug(LoggingLevel.DEBUG,
                        '[CustomAccountTriggerHandler] Cannot delete an account that has an ERP Customer Number, '
                                + ' please contact finance to have this account re-matched ('
                                + a.Navision_Customer_Number__c + ')');
                a.addError(Label.Account_ControlDelete_error_message);
            }
        }
    }

/*
 The trigger is used to start the approval process.
 Moved from trigger Account_LegalApprovalRequest_SubmitForApproval on Account (after update)
 CR# 3643 - eCustoms application implementation
 The Approval process attached to this logic is not active at this point and hence it will not be tested right now
*/
    public static void handleLegalApprovalRequestSubmitForApproval(List<Account> newAccounts, List<Account> oldAccounts) {
        List<Approval.ProcessSubmitRequest> ProcessSubmitRequestList = new List<Approval.ProcessSubmitRequest>();
        //FIXME Optimize cycle!!!
        for (Integer i = 0; i < newAccounts.size(); i++) {
            if (oldAccounts[i].Legal_Approval_Status__c != 'Legal Approval Requested'
                    && newAccounts[i].Legal_Approval_Status__c == 'Legal Approval Requested') {
                //create the new approval request to submit
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Submitted for approval.');
                req.setObjectId(newAccounts[i].Id);
                //add the request to the list
                ProcessSubmitRequestList.add(req);
                System.debug(LoggingLevel.DEBUG,
                        '[CustomAccountTriggerHandler] SubmitForApproval: AccountId = ' + newAccounts[i].Id);
            }
        }
        //submit the approval request list for processing
        List<Approval.ProcessResult> resultList = Approval.process(ProcessSubmitRequestList, false);
        //FIXME what to do with errors?
        // display if the request was successful
        for (Approval.ProcessResult result : resultList)
        {
            System.debug('Submitted for approval successfully: '+ result.isSuccess());
        }
        System.debug('Account_LegalApprovalRequest_SubmitForApproval: Finishing');  
    }

/*
 When Partner Account name is edited on Partner Account record
 then set the TriggerPortalControl = true on Contacts for the portal users.
 Moved from trigger Account_PartnerRecordUpdate on Account (before update)
*/
    public static void handleAccountPartnerRecordUpdate(List<Account> newAccounts, List<Account> oldAccounts) {
        List<Id> listAccountsIDs = new List<Id>();

        for (integer i = 0; i < newAccounts.size(); i++) {
            //01220000000DOFz - Partner Account record type
            if (newAccounts[i].RecordTypeId == '01220000000DOFz' &&
                    newAccounts[i].Name != oldAccounts[i].Name) {
                listAccountsIDs.add(newAccounts[i].Id);
            }
        }

        if (listAccountsIDs.size() > 0) {
            List<Contact> ContactsToUpdate = new List<Contact>();

            for (Contact ContactUser : [
                    SELECT Id,TriggerPortalControl__c
                    FROM Contact
                    WHERE AccountId IN :listAccountsIDs
            ]) {
                ContactUser.TriggerPortalControl__c = true;
                ContactsToUpdate.add(ContactUser);
            }
            if (ContactsToUpdate.size() > 0) {
                update(ContactsToUpdate);
            }
        }
    }

/*
 Moved from trigger UpdateGroupEmpRangeAccount on Account (before insert, before update)
 Created By ~Steve Smith-Retired (INC0091965),  07/08/2009 16:05
 Last Modified By Navneet Kumar,  15/11/2017 08:46
*/
    public static void handleUpdateGroupEmpRangeAccount(List<Account> inAccounts) {

        for (Account A : inAccounts) {
            if (A.NumberOfEmployees != null & A.NumberOfEmployees != 0) {
                A.Employee_Range__c = picklisttranslations.ReturnEmployee(A.NumberOfEmployees);
                A.NumberOfEmployees = null;
            }
        }
    }

/*
 Moved from trigger Set_Named_Account_Dates on Account (before insert, before update)
 Created By Bluewolf Consultants (Retired),  22/05/2009 12:17
 Last Modified By Navneet Kumar,  15/11/2017 08:46
*/
    public static void handleSetNamedAccountDates(List<Account> newAccounts, List<Account> oldAccounts) {
        Date td = System.today();

        if (oldAccounts == null) { // Trigger.isInsert
            for (Account acct : newAccounts) {
                if (acct.Named_Account__c) {
                    acct.Start_Date_for_Named_Account__c = td;
                }
            }
        } else {
            for (Integer i = 0; i < newAccounts.size(); i++) {
                if (newAccounts[i].Named_Account__c != oldAccounts[i].Named_Account__c) {
                    if (newAccounts[i].Named_Account__c) {
                        if (oldAccounts[i].End_Date_for_Named_Account__c != null) {
                            newAccounts[i].End_Date_for_Named_Account__c = null;
                        }

                        newAccounts[i].Start_Date_for_Named_Account__c = td;

                    } else {
                        newAccounts[i].End_Date_for_Named_Account__c = td;

                    }
                }
            }
        }
    }
/*
 Moved from trigger UpdateAssingedLevels on Account (after update)
 Created By Martin Haagen,  13/01/2010 18:00
 Last Modified By  Navneet Kumar,  15/11/2017 09:09
 */
    public static void handleUpdateAssingedLevels(List<Account> newAccounts, List<Account> oldAccounts) {
        List<string> AccountIDs = new List<string>();

        for (integer i = 0; i < newAccounts.size(); i++) {
            // Only process if Navision_Status__c has changed AND is empty, customer or partner
            if ((newAccounts[i].Navision_Status__c != oldAccounts[i].Navision_Status__c)
                    &&
                    (String.isBlank(newAccounts[i].Navision_Status__c)
                            || newAccounts[i].Navision_Status__c == 'Customer'
                            || newAccounts[i].Navision_Status__c == 'Partner')
                    ) {
                System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] Adding to list ' + newAccounts[i].Id);
                AccountIDs.Add(newAccounts[i].Id);
            }
        }

        if (AccountIDs.size() > 0) {
            ULC.CallUpdateAccountLevel(AccountIDs);
        }
    }

/*
 Moved from trigger Account_NavisionStatus_Change_trg on Account (after insert, after update)
 This trigger handles the update of Adopted_Navision_Status__c and
 AdoptedFromId__c. If the Heirarchy is changed an email will be sent
 to the account owner.
 Created By Martin Haagen,  06/09/2010 13:10
 Last Modified By Navneet Kumar,  15/11/2017 09:10

 */
//TODO Can this be merged with handleUpdateAssingedLevels?
    public static void handleAccountNavisionStatusChange(List<Account> newAccounts, List<Account> oldAccounts) {
        List<ID> PossibleMatches = new List<ID>();

        for (integer i = 0; i < newAccounts.size(); i++) {
            Account NewAcc = newAccounts[i];

            if (oldAccounts != null) { // if Trigger.isUpdate
                if (NewAcc.Navision_Status__c == oldAccounts[i].Navision_Status__c
                        && NewAcc.Adopted_Navision_Status__c == oldAccounts[i].Adopted_Navision_Status__c
                        && NewAcc.ParentId == oldAccounts[i].ParentId) {
                    System.debug(LoggingLevel.DEBUG,
                            '[CustomAccountTriggerHandler] Statuses not modified ' + NewAcc.Id + ' ' + NewAcc.Name);
                    continue;
                }
            }

            if (NewAcc.ParentId != null) {
                PossibleMatches.Add(NewAcc.Id);
            }
        }

        // If no parent stop processing
        if (PossibleMatches.size() == 0) {
            System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] There was no accounts to investigate');
            return;
        }

        Map<Id, Account> ParentAccount = new Map<Id, Account>([
                SELECT Id, Name, Navision_Status__c,
                        Adopted_Navision_Status__c, Parent.Id, Parent.Name, Parent.Navision_Status__c,
                        Parent.Adopted_Navision_Status__c, Parent.OwnerId
                FROM Account
                WHERE Id IN :PossibleMatches
        ]);

        List<Account> ToUpdate = new List<Account>();
        List<Account> ToEmail = new List<Account>();

        for (ID Id : PossibleMatches) {
            Account ThisAccount = ParentAccount.get(Id);

            System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] This account: ' + ThisAccount.Name
                    + ' ,' + ThisAccount.Parent.Navision_Status__c
                    + ', ' + ThisAccount.Parent.Adopted_Navision_Status__c);

            if (ThisAccount.Navision_Status__c == '' && ThisAccount.Adopted_Navision_Status__c == '') {
                if (ThisAccount.Parent.Navision_Status__c == 'Partner'
                        || ThisAccount.Parent.Adopted_Navision_Status__c == 'Partner') {
                    ToEmail.Add(ThisAccount);
                    continue;
                }

                if (ThisAccount.Parent.Navision_Status__c == 'Customer'
                        || ThisAccount.Parent.Adopted_Navision_Status__c == 'Customer') {
                    ToEmail.Add(ThisAccount);
                    continue;
                }
            }

            if (ThisAccount.Navision_Status__c == 'Customer' || ThisAccount.Adopted_Navision_Status__c == 'Customer') {
                if (ThisAccount.Parent.Navision_Status__c == 'Partner'
                        || ThisAccount.Parent.Adopted_Navision_Status__c == 'Partner') {
                    ToEmail.Add(ThisAccount);
                    continue;
                }

                if (ThisAccount.Parent.Navision_Status__c == 'Customer'
                        || ThisAccount.Parent.Adopted_Navision_Status__c == 'Customer') {
                    continue;
                }

                System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] Add ' + ThisAccount.Id
                        + ' ' + ThisAccount.Name + ' to the list of accounts to update');
                Account Parent = ThisAccount.Parent;
                System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] There check ' + ThisAccount.Parent);
                Parent.Adopted_From_Id__c = ThisAccount.Id;
                Parent.Adopted_Navision_Status__c = 'Customer';
                ToUpdate.Add(Parent);
            }

            if (ThisAccount.Navision_Status__c == 'Partner' || ThisAccount.Adopted_Navision_Status__c == 'Partner') {
                if (ThisAccount.Parent.Navision_Status__c == 'Partner'
                        || ThisAccount.Parent.Adopted_Navision_Status__c == 'Partner') {
                    continue;
                }
                System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] Add ' + ThisAccount.Id
                        + ' ' + ThisAccount.Name + ' to the list of accounts to update');
                Account Parent = ThisAccount.Parent;
                System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] There check ' + ThisAccount.Parent);
                Parent.Adopted_From_Id__c = ThisAccount.Id;
                Parent.Adopted_Navision_Status__c = 'Partner';
                ToUpdate.Add(Parent);
            }
        }

        // Should we send any emails?
        List<Messaging.SingleEmailMessage> Mails = new List<Messaging.SingleEmailMessage>();
        for (Account Acc : ToEmail) {
            Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
            //Message.setTemplateId('00XS0000000InsK'); //  .QtTest
            Message.setTemplateId('00X20000001M7jj');    //  LIVE
            Message.setTargetObjectId(Acc.Parent.OwnerId);
            Message.setSaveAsActivity(false);
            Message.setWhatId(Acc.Id);
            Message.setUseSignature(false);
            Mails.add(Message);
        }

        System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] There was ' + Mails.size() + ' mails to send');
        if (Mails.size() > 0) {
            Messaging.sendEmail(Mails);
        }

        System.debug(LoggingLevel.DEBUG, '[CustomAccountTriggerHandler] There was ' + ToUpdate.size() + ' accounts to update');
        if (ToUpdate.size() > 0) {
            update ToUpdate;//Here we are updating the same object - Account
        }
    }

/*
 Moved from trigger Account_SelfServe_Change on Account (after insert, after update)
 This trigger handles the update of Self_Serve__c.
 It will also trigger when Navision_Status__c changes to and from Customer.
 Created By Martin Haagen,  16/10/2010 23:16
 Last Modified By Navneet Kumar,  15/11/2017 09:10
*/
    //TODO Can this be merged with handleUpdateAssingedLevels?
    public static void handleAccountSelfServeChange(List<Account> newAccounts, List<Account> oldAccounts) {
        List<string> AccountsToUpdate = new List<string>();

        if (oldAccounts == null) { //Trigger.isInsert
            for (integer i = 0; i < newAccounts.size(); i++) {
                Account Acc = newAccounts[i];

                if (Acc.Navision_Status__c == 'Customer' && Acc.Self_Serve__c == true) {
                    AccountsToUpdate.Add(Acc.Id);
                }
            }
        } else { //Trigger.isUpdate
            for (integer i = 0; i < newAccounts.size(); i++) {
                Account NewAcc = newAccounts[i];
                Account OldAcc = oldAccounts[i];

                if ((OldAcc.Navision_Status__c == 'Customer' && OldAcc.Self_Serve__c == true)
                        && (NewAcc.Navision_Status__c != 'Customer' || NewAcc.Self_Serve__c != true)) {
                    AccountsToUpdate.Add(NewAcc.Id);
                }

                if ((OldAcc.Navision_Status__c != 'Customer' || OldAcc.Self_Serve__c != true)
                        && (NewAcc.Navision_Status__c == 'Customer' && NewAcc.Self_Serve__c == true)) {
                    AccountsToUpdate.Add(NewAcc.Id);
                }
            }
        }

        if (AccountsToUpdate.size() > 0) {
            ULC.SelfServiceChanged(AccountsToUpdate);
        }
    }

/*
 Moved from trigger AccountSharingRulesOnPartnerPortalAccounts on Account (after insert, after update)
 Created By Martin Haagen,  13/10/2009 14:26
 Last Modified By Navneet Kumar,  15/11/2017 08:50
*/  

    public static void handleAccountSharingRulesOnPartnerPortalAccounts(List<Account> newAccounts, List<Account> oldAccounts) {

        system.debug(LoggingLevel.DEBUG, '[REMOVE_LINE]: '
                + Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun
                + ' new:'
                + newAccounts.size()
                + ' old:'
                + ( oldAccounts == null)
        );

        if (Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun || System.isBatch() || System.isFuture()) {
            // if the trigger is already called or in a batch job or a future call then we should
            // go no further because no future calls are allowed inside another futuer call
            system.debug(LoggingLevel.DEBUG, '[REMOVE_LINE]: No more ');
            return;
        }

        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;
        List<Id> accountsToUpdate = new List<Id>();

        if (oldAccounts == null) { //Trigger.isInsert
            for (Integer i = 0; i < newAccounts.size(); i++) {
                accountsToUpdate.add(newAccounts[i].Id);
            }
        } else { //Trigger.isUpdate
            for (Integer i = 0; i < newAccounts.size(); i++) {
                if (newAccounts[i].OwnerId != oldAccounts[i].OwnerId) {
                    accountsToUpdate.add(newAccounts[i].Id);
                }
            }
        }

        if (accountsToUpdate.size() > 0) {
            if (!Test.isRunningTest()) {// to avoid future call from test
                ApexSharingRules.updateAccountSharing(accountsToUpdate);
            } else if (ApexSharingRules.TestingAccountShare) {//TODO What is this ApexSharingRules.TestingAccountShare?
                ApexSharingRules.updateAccountSharingForTest(accountsToUpdate);
            }
        }
    }

/*
 Moved from trigger AccountAfterInsertOrUpdateTrigger on Account (after insert, after update, before delete, after delete)
 Method handles after insert and after update events.
 When a account is created or updated force copying the Account Owner Id
 to Account_Owner__c of Contact object as required in CR# 6397
 This works together with ContactOnInsertUpdateTrigger
 Created By Tao Jiang,  08/11/2012 22:13
 Last Modified By Navneet Kumar,  15/11/2017 09:07
 Change Log:
 20121101   TJG https://eu1.salesforce.com/a0CD000000NKf1w
 20121221   TJG improve efficiency by do more checking
 
 20130214   TJG updated for Data Dispatch project. If any of the following 6 fields
                changes, copy them into those lead records that have the account 
                as Probable Account.
                    No of Employee, text type (No._of _Employees__c, custom field not the standard one named the same)
                    SIC Code Name, lookup type (SIC_Code_Name__c)
                    Yearly Revenue, text type (Annual_Turnover__c)
                    Named Account, checkbox (Named_Account__c from the Account to Named_Account_Lead__c on the Lead)
                    Segment, picklist type (Segment_New__c from the Account to Segment__c on the Lead)
                    Company, text type ( Name from the Account to Company on the Lead)
 20130321   TJG add three more fields as requested
                    Employee Range, picklist type (Site_Employee_Range__c )
                    Owner of the Linked Account, lookup type (Owner from the Account to Owner_of_the_Linked_Account__c on the Lead)
                    Link to Account ID, text type (Id from the Account to Link_to_Account_ID__c on the Lead
 20130703   TJG Case 00186262 https://eu1.salesforce.com/500D000000QnpS3
                catch before delete too
 20140722     MTM  To fix the SOQL limit exception on account transfers https://eu1.salesforce.com/a0CD000000k3arq                  
 20150126    CCE CR# 16699 https://eu1.salesforce.com/a0CD000000ksoid Add Key Account field
 20150422   CCE CR# 22167 https://eu1.salesforce.com/a0CD000000pcfqd Add Navision Type for Mkto field
 20160222   IRN CR42998 It the account owner is updated from a partner or to a partner do not change contact owner
 20161106   BAD CR# 97533 - Relayware
 */
    public static void handleAccountAfterInsertOrUpdate(List<Account> newAccounts, List<Account> oldAccounts, Boolean insrt, Boolean updt, Boolean dlt, Boolean befr, Boolean aftr) {
       //static final String DEBUGPREFIX = '-AAAAAAA-';
    System.debug(' AccountAfterInsertOrUpdateTrigger: Starting');
    //------ beginning of CR# 6397 ------
    // only interested in owner changed accounts
    if (dlt || (!Semaphores.TriggerHasRun('AccountAfterInsertOrUpdateTrigger'))) {
        Set<Id> changedAccountIds = new Set<Id>();
        Map<Id, Id> oldOwnerIds = new Map<Id, Id>(); //map for holding the old account ownerId
        Map<Id, Id> newOwnerIds = new Map<Id, Id>(); //map for holding the new account ownerId

        if (updt) {
            for (integer i = 0; i < newAccounts.Size(); i++) {
                if (newAccounts[i].OwnerId != oldAccounts[i].OwnerId) {
                    changedAccountIds.Add(newAccounts[i].Id);
                    oldOwnerIds.put(newAccounts[i].Id, oldAccounts[i].OwnerId); //put the old OwnerId value in a map
                    newOwnerIds.put(newAccounts[i].Id, newAccounts[i].OwnerId); //put the new OwnerId value in a map
                }
            }
            //Relayware
            if (aftr) {
                system.debug(LoggingLevel.DEBUG,'RELAYWARE OLD:' + newAccounts + ' ' + oldAccounts);
                AccountPartnerPortalRelaywareHandler.handle(newAccounts, oldAccounts);
            }
        } else if (insrt) {
            for (integer i = 0; i < newAccounts.Size(); i++) {
                changedAccountIds.Add(newAccounts[i].Id);
            }
        }
        if (!changedAccountIds.isEmpty()) {
            List<Contact> ContactsToUpdate = [SELECT Account_Owner__c FROM Contact WHERE AccountId IN :changedAccountIds];//cr42998 get all contacts belonging to that account
            Map<Id, Contact> mapOfContactsToUpdate = new Map<Id, Contact>();
            if (ContactsToUpdate.size() > 0) {
                for (Contact ctct : ContactsToUpdate) {
                    ctct.Account_Owner__c = null;
                    mapOfContactsToUpdate.put(ctct.Id, ctct);
                }
                System.debug(' AccountAfterInsertOrUpdateTrigger: size = ' + ContactsToUpdate.size());
            }

            //start of cr 42998 - the contact owner should not be updated if the accountowner is or is changed to a partner user
            Map<Id, Boolean> partnerMap = new Map<Id, Boolean>();
            //check if any of old or new OwnerId is a partner user
            Set<Id> owners = new Set<Id>();
            owners.addAll(oldOwnerIds.values());
            owners.addAll(newOwnerIds.values());
            List<User> users = [Select Id, Is_Partner_User__c from User where Id in :owners];

            for (Integer i = 0; i < users.size(); i++) {
                Boolean res = users[i].Is_Partner_User__c == 'YES' ? true : false;
                partnerMap.put(users[i].Id, res);
            }

            for (Account act : [SELECT Id, (SELECT Id, OwnerId, LastModifiedDate FROM Contacts) FROM Account WHERE Id in :changedAccountIds]) {
                String newOwnerId = newOwnerIds.get(act.Id); //get the new OwnerId value for the account
                String oldOwnerId = oldOwnerIds.get(act.Id); //get the old OwnerId value for the account
                for (Contact c : act.Contacts) { //for all contacts
                    Datetime dateNow = Datetime.now();
                    dateNow = dateNow.addSeconds(-5);
                    System.debug(' partnerMap.get(oldOwnerId) ' + partnerMap.get(oldOwnerId) + ' partnerMap.get(newOwnerId) ' + partnerMap.get(newOwnerId));
                    if ((c.OwnerId == newOwnerId && c.LastModifiedDate > dateNow) && (partnerMap.get(oldOwnerId) || partnerMap.get(newOwnerId))) { // if the old owner or the new owner is a partner user than do not update
                        if (mapOfContactsToUpdate.containsKey(c.Id)) {
                            Contact updatedContact = mapOfContactsToUpdate.get(c.Id);
                            updatedContact.ownerId = oldOwnerId;
                        } else {
                            Contact updatedContact = new Contact(Id = c.Id, OwnerId = oldOwnerId); //create a new Contact sObject
                            mapOfContactsToUpdate.put(c.Id, updatedContact);
                        }
                    }
                }
            }
            List<Contact> updateContacts = new List<Contact>();
            updateContacts.addAll(mapOfContactsToUpdate.values()); // end of cr42998
            update updateContacts; //update the Contacts
        }
        //------ end of CR# 6397 ------


        //------ beginning of Data Dispatch project code ------
        if (updt || dlt) {
            // only if any of the 8 fields changed
            Set<Id> ourAccIds = new Set<Id>();
            Map<Id, Account> myMap = new Map<Id, Account>();
            for (integer i = 0; i < oldAccounts.Size(); i++) {
                if (dlt ||
                        newAccounts[i].Name != oldAccounts[i].Name ||
                        newAccounts[i].No_of_Employees__c != oldAccounts[i].No_of_Employees__c ||
                        newAccounts[i].SIC_Code_Name__c != oldAccounts[i].SIC_Code_Name__c ||
                        newAccounts[i].Annual_Turnover__c != oldAccounts[i].Annual_Turnover__c ||
                        newAccounts[i].Named_Account__c != oldAccounts[i].Named_Account__c ||
                        newAccounts[i].Segment_New__c != oldAccounts[i].Segment_New__c ||
                        newAccounts[i].Site_Employee_Range__c != oldAccounts[i].Site_Employee_Range__c ||
                        newAccounts[i].OwnerId != oldAccounts[i].OwnerId ||
                        newAccounts[i].Key_Account__c != oldAccounts[i].Key_Account__c ||
                        newAccounts[i].Navision_Status__c != oldAccounts[i].Navision_Status__c) {
                    if (dlt) {
                        if (befr) {
                            ourAccIds.Add(oldAccounts[i].Id);
                            myMap.put(oldAccounts[i].Id, oldAccounts[i]);
                        }
                    } else {
                        ourAccIds.Add(newAccounts[i].Id);
                        myMap.put(newAccounts[i].Id, newAccounts[i]);
                    }
                }
            }
            //System.debug(DEBUGPREFIX + ' ourAccIds: ' + ourAccIds);
            //System.debug(DEBUGPREFIX + ' myMap: ' + myMap);
            System.debug(' AccountAfterInsertOrUpdateTrigger Data Dispatch section');

            if (!ourAccIds.IsEmpty()) {
                List<Lead> ourLeads = new List<Lead>([select Probable_Account__c, Company, No_of_Employees__c, SIC_Code_Name__c, Annual_Turnover__c, Named_Account_Lead__c, Segment__c, Key_Account__c, Navision_Type_for_Mkto__c from Lead where IsConverted != true And Probable_Account__c in :ourAccIds]);
                // are there any affected leads?
                if (!ourLeads.IsEmpty()) {
                    Boolean updateNeeded = false;
                    for (Lead aLead : ourLeads) {
                        Account acct = myMap.Get(alead.Probable_Account__c);
                        if (dlt && befr) {
                            updateNeeded = true;
                        }
                        if (null != acct) {
                            updateNeeded = true;
                        }
                    }
                    System.debug(' ourLeads: ' + ourleads);
                    if (updateNeeded) {
                        //System.debug(DEBUGPREFIX + ' AccountAfterInsertOrUpdateTrigger lead update needed');
                        update ourLeads;
                    }
                }
            }
            if (aftr && dlt) {
                // call the future method to update the leads
                LeadProbableAccountUpdate.UpdateLeadFields();
            }
        }
    }
    //------ end of Data Dispatch project code ------
    System.debug(' AccountAfterInsertOrUpdateTrigger: Finishing');

    }

    public static void handleDealDeskSupportUpdate(List<Account> newAccounts, Map<id,Account> oldAccountMap){
        Set<ID> accImpacted = new Set<Id>();
        for(Account acc:newAccounts){
            Account oldaccount = oldAccountMap.get(acc.id);
            if(acc.Named_Account__c != oldaccount.Named_Account__c || acc.Segment_New__c != oldaccount.Segment_New__c){
                accImpacted.add(acc.id);
            }
        }
        List<Opportunity> relatedOppties = [select id from Opportunity where  AccountId in :accImpacted AND (not stagename like '%closed%')];
        Update relatedOppties;
    }

// class CustomAccountTriggerHandler^
}