@isTest

public class Z_NewQuoteButtonValidationTest {

    private static Account testAcc;
    private static Contact testContact;
    private static Zuora__CustomerAccount__c testCustomerAcc;
    private static Opportunity testOpp;
    private static QlikTech_Company__c testQTComp;
    private static Q2CWSettings__c testQ2CWSettings;
    private static Steelbrick_Settings__c testSteelbrickSettings;
    private static OpportunityContactRole billTo;
    private static OpportunityContactRole soldTo;


    static void setup() {

        testQ2CWSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();
        testQTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOpp = Z_RenewalTestFactory.makeOpportunity2(testAcc);

        testAcc.QlikTech_Company__c = testQTComp.name;
        testAcc.Billing_Country_Code__c  = testQTComp.id;

        update testAcc;

        List<sObject> tests = new List<sObject>{
            testQ2CWSettings, testSteelbrickSettings
        };

        insert tests;

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOpp
        };

        insert testObjects;

        Organization_Info__c orgInfo = new Organization_Info__c(isSandbox__c=true);
        insert orgInfo;

        orgInfo.CreatedInOrgId__c = orgInfo.Id;
        update orgInfo;

    }

    @isTest
    static void Z_NewQuoteButtonValidationTest() {

        setup();
        
        String oppId = testOpp.Id;

        Test.StartTest();
        Z_NewQuoteButtonValidation.QuoteValidationResult buttonValidation = Z_NewQuoteButtonValidation.newQuoteButtonValidation(oppId);

        System.debug('Button validation boolean is ' + buttonValidation.success);

        System.debug('Button validation list size is ' + buttonValidation.errorMessages.size());

        System.Assert(!buttonValidation.success);
        
        System.Assert(!buttonValidation.errorMessages.isEmpty());

        billTo = new OpportunityContactRole();
        billTo.ContactId = testContact.Id; 
        billTo.OpportunityId = testOpp.Id;
        billTo.Role = 'Invoice Recipient - Bill To';
        insert billTo; 

        soldTo = new OpportunityContactRole();
        soldTo.ContactId = testContact.Id; 
        soldTo.OpportunityId = testOpp.Id;
        soldTo.Role = 'Quote Recipient - Sold To';
        insert soldTo;

        Z_NewQuoteButtonValidation.QuoteValidationResult buttonValidationRoles = Z_NewQuoteButtonValidation.newQuoteButtonValidation(oppId);

        System.Assert(buttonValidationRoles.success);
        
        System.Assert(buttonValidationRoles.errorMessages.isEmpty());

        Test.StopTest();
    }
}