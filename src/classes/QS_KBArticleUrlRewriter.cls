/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
global class QS_KBArticleUrlRewriter implements Site.UrlRewriter {
    
    String DIRECTORY = '/articles/';
    String VISUALFORCE_PAGE = '/QS_CaseWizardKnowledgeArticle?articleId=';
    global static Map<String, PageReference> articleIdMap;
    
    // The first global method for mapping external URL to an internal one
    global PageReference mapRequestUrl(PageReference myFriendlyUrl) {
        if(myFriendlyUrl != null) {
            String url = myFriendlyUrl.getUrl();
            //url = EncodingUtil.urlDecode(url, 'UTF-8');
         
            if(url != null && url.startsWith(DIRECTORY)){
               String urlName = url.substring(DIRECTORY.length(),url.length());
               
               String articleUrlName = (urlName != null && urlName != '' && (urlName.contains('/') || urlName.indexOf('/') != -1) ? urlName.substring(0, urlName.indexOf('/')) : urlName);
               // SFDC Debugging added 8th Dec 2014
               system.debug(articleUrlName);
               String restSalesforceUrl = '';
               if(urlName != null && urlName != '' && urlName.contains('/')) {
                    String parameterFriendUrl = (urlName.substring(urlName.indexOf('/')+1, urlName.length())).removeEnd('/');
                    restSalesforceUrl = processFriendlyUrl(parameterFriendUrl);
               }
                if(articleUrlName != null && articleUrlName != '') {
                   
                   //Select the ID of the knowledge article version that matches the name from the URL
                    List<KnowledgeArticleVersion> articleVersionList = [Select Id, Title, KnowledgeArticleId, UrlName 
                                                                            From KnowledgeArticleVersion
                                                                            Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND UrlName = :articleUrlName
                                                                            //Order By LastPublishedDate DESC
                                                                            LIMIT 1];
                    // SFDC Debugging added 8th Dec 2014
                    List<KnowledgeArticleVersion> SFDCList = [Select Id, Title, KnowledgeArticleId, UrlName, IsLatestVersion, PublishStatus 
                                                                            From KnowledgeArticleVersion
                                                                            Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND KnowledgeArticleId = 'kA5D00000008REZ'
                                                                            //Order By LastPublishedDate DESC
                                                                            LIMIT 1];
                    system.debug(SFDCList);                                                        
                                                                            
                                                                   
                    if(articleVersionList != null && !articleVersionList.isEmpty()) {   
                        KnowledgeArticleVersion articleVersion =  articleVersionList[0];   
                        //Construct a new page reference in the form of my Visualforce page
                        return new PageReference(VISUALFORCE_PAGE + String.valueOf(articleVersion.Id) + (restSalesforceUrl != null && restSalesforceUrl != '' ? restSalesforceUrl : ''));
                    }
                }
            }
        }
        //return null;
        
        return myFriendlyUrl;

    }
    
    
    // The second global method for mapping internal Ids to URLs
    global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls) {
        
        List<Id> originalOrder = new List<Id>(); 

        if (mySalesforceUrls != null && !mySalesforceUrls.isEmpty() && mySalesforceUrls.size() > 0 && mySalesforceUrls.get(0).getUrl() != null && mySalesforceUrls.get(0).getUrl().startsWith(VISUALFORCE_PAGE)) {
            //A list of pages to return after all the links have been evaluated
            List<PageReference> myFriendlyUrls = new List<PageReference>();
            Set<Id> articleIdSet = new Set<Id>();
            Set<String> allParametersSet = new Set<String>();
            Map<String, String> parameterMap = new Map<String, String> (); // Map which contains ArticleId as key and rest of the parameters as value
            Map<Id, PageReference> articleUrlMap = new Map<Id, PageReference>();
            //System.debug('%%%%Inside generateUrlFor: -mySalesforceUrls - size: '+ mySalesforceUrls.size());
            if(articleIdMap == null || articleIdMap.keySet() == null || articleIdMap.keySet().isEmpty())
                articleIdMap = new Map<String, PageReference>();
                            
            for(PageReference mySalesforceUrl : mySalesforceUrls) {
            
              //Get the URL of the page
              String url = mySalesforceUrl.getUrl();
              
              //If this looks like a page that needs to be mapped, transform it
              if(url != null && url.startsWith(VISUALFORCE_PAGE)) {
                //Extract the ID from the query parameter
                String articleIdRetrieved = url.substring(VISUALFORCE_PAGE.length(), (url.contains('&') ? url.indexOf('&') : url.length()));
                System.debug('%%%% Checking url :'+url);
                System.debug('%%%% Checking articleMap :'+articleIdMap);
                System.debug('%%%% Checking articleIdRetrieved :'+articleIdRetrieved);
                if(articleIdRetrieved != null && articleIdRetrieved != '') {
                    originalOrder.add(articleIdRetrieved); 
                    if(articleUrlMap == null || articleUrlMap.keySet() == null || articleUrlMap.keySet().isEmpty() || !articleUrlMap.containsKey(articleIdRetrieved))
                    {
                        articleUrlMap.put(articleIdRetrieved, mySalesforceUrl);
                    }   
                    if(articleIdMap == null || articleIdMap.keySet() == null || articleIdMap.keySet().isEmpty() || !articleIdMap.containsKey(articleIdRetrieved)) { 
                        allParametersSet.add(url.substring(VISUALFORCE_PAGE.length(), url.length()));
                        articleIdSet.add(articleIdRetrieved);
                        if(url.contains('&otherParam=') && url.indexOf('&otherParam=') != (url.length() - 1)) {
                            if(parameterMap == null || parameterMap.keySet() == null || parameterMap.keySet().isEmpty() || !parameterMap.containsKey(articleIdRetrieved))
                                parameterMap.put(articleIdRetrieved, processRequestURL(url.substring(url.indexOf('&otherParam=')+1, url.length()), '&otherParam='));
                        } else if(url.contains('&caseId=') && url.indexOf('&caseId=') != (url.length() - 1)) {
                            if(parameterMap == null || parameterMap.keySet() == null || parameterMap.keySet().isEmpty() || !parameterMap.containsKey(articleIdRetrieved))
                                parameterMap.put(articleIdRetrieved, processRequestURL(url.substring(url.indexOf('&caseId=')+1, url.length()), '&caseId='));
                        }
                        
                    } else {
                        myFriendlyUrls.add(articleIdMap.get(articleIdRetrieved));
                    }
                }
              } else {
                //If this doesn't match, don't do any transformations
                myFriendlyUrls.add(mySalesforceUrl);
              }
            }
            map<Id, PageReference> newUrls = new map<Id, PageReference>();
            if(articleIdSet != null && !articleIdSet.isEmpty()) {
                //Query for the name of the article page to put in the URL
                List<KnowledgeArticleVersion> articleVersionList = [Select Id, Title, KnowledgeArticleId, UrlName 
                                                                        From KnowledgeArticleVersion
                                                                        Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND (Id IN :articleIdSet OR KnowledgeArticleId IN :articleIdSet) order by LastPublishedDate desc];
                if(articleVersionList != null && !articleVersionList.isEmpty()) {
                    // If article exists then we retrieve the article's url name and form the page reference object    
                    for(KnowledgeArticleVersion articleVersion : articleVersionList) {
                        //Construct the new URL
                        String articleUrlName = articleVersion.UrlName; 
                        String parameterName = (parameterMap != null && parameterMap.keySet() != null && !parameterMap.keySet().isEmpty() && parameterMap.containsKey(articleVersion.Id) 
                                                                    && parameterMap.get(articleVersion.Id) != null && parameterMap.get(articleVersion.Id) != '') 
                                                                    ? parameterMap.get(articleVersion.Id) : '';
                                                                    
                        PageReference tempPageRef = new PageReference(DIRECTORY + articleUrlName + parameterName);
                        // Setting the global variable to hold the values
                        if(articleIdMap == null || articleIdMap.keySet() == null || articleIdMap.keySet().isEmpty() || !articleIdMap.containsKey(articleVersion.Id)) {  
                            articleIdMap.put(articleVersion.Id, tempPageRef);
                        }
                        //newUrls.put((articleVersion.KnowledgeArticleId == null ? articleVersion.Id : articleVersion.KnowledgeArticleId), tempPageRef);
                        for(Id oo : originalOrder)
                        {
                            if(oo == articleVersion.Id)
                            {
                                newUrls.put(articleVersion.Id, tempPageRef);
                                break;
                            }
                            else if(oo == articleVersion.KnowledgeArticleId)
                            {
                                newUrls.put(articleVersion.KnowledgeArticleId, tempPageRef);
                                break;
                            }


                        }
                        
                        //myFriendlyUrls.add(tempPageRef);
                    }
                    for(Id oo : originalOrder)
                    {
                        if(newUrls.containsKey(oo))
                            myFriendlyUrls.add(newurls.get(oo));
                    }
                } else {
                    // If there isn't any article retrieved then just return the salesforce url back
                    for(Id articleId : articleIdSet) {
                        PageReference tempPageRef;  
                        if(articleUrlMap != null && articleUrlMap.keySet() != null && !articleUrlMap.keySet().isEmpty() && articleUrlMap.containsKey(articleId))
                            tempPageRef = articleUrlMap.get(articleId);
                    
                        if(articleIdMap == null || articleIdMap.keySet() == null || articleIdMap.keySet().isEmpty() || !articleIdMap.containsKey(articleId)) {  
                            articleIdMap.put((String) articleId, tempPageRef != null ? tempPageRef : null);
                            if(((String) articleId).length() >= 15)
                                articleIdMap.put(((String) articleId).substring(0,15), tempPageRef != null ? tempPageRef : null);
                        }
                    }
                }
            }
            System.debug('%%%%Inside generateUrlFor: -myFriendlyUrls: '+ myFriendlyUrls);
            System.debug('%%%%Inside generateUrlFor: -mySalesforceUrls: '+ mySalesforceUrls);
            
          //Return the full list of pages
          return myFriendlyUrls;
        } else {
            return mySalesforceUrls;
        }
    }
    
    // converts the salesforce url to friend url with parameters (&caseId=500jksahdkja is changed to /500jksahdkja/)
    private static String processRequestURL(String requestURL, String comparableStr) {
        String responseURL = '';
        System.debug('%%%^^^^^'+ requestURL);
        
        if(requestURL != null && requestURL != '') {
            requestURL.replace(comparableStr, '');
            if(requestURL.contains('%26')) { // %26 is to check if we have any '&' in the parameters
                String [] splitRequestArr = requestURL.split('%26');
                for(String splitRequest : splitRequestArr) {
                    if(splitRequest != null && splitRequest != '' && splitRequest.contains('%3D') && splitRequest.indexOf('%3D') != splitRequest.length() - 1) { // // %3D is to check if we have any '=' in the parameters
                        splitRequest = splitRequest.substring(splitRequest.indexOf('%3D'), splitRequest.length());
                        splitRequest = splitRequest.contains('%3D') ? splitRequest.replace('%3D', '') : splitRequest;
                        if(splitRequest != null && splitRequest != '')
                            responseURL += '/' + splitRequest;
                        else
                            responseURL += '/null';
                    } else {
                        responseURL += '/null';
                    }
                }
            } else {
                responseURL += '/'+requestURL;
            }
            
            if(responseURL != null && responseURL != '') {
                if(responseURL.contains('%0A'))
                    responseURL = responseURL.replace('%0A','_newline_'); // changing the newline parameter manually, otherwise it breaks the url
                if(responseURL.contains('%3F'))
                    responseURL = responseURL.replace('%3F','_questionmark_'); // 
            }
        }
        
        System.debug('%%%^^^^^responseURL:'+ responseURL);
        
        return responseURL;
    }
    
    // converts the url (in /caseId or /true/subject etc.. ) to internal salesforce url (&caseId=005 or &fromCaseWizard=true&subject=test etc.)
    private static String processFriendlyUrl(String friendlyUrl) {
        
        String salesforceUrl = '';
        if(friendlyUrl != null && friendlyUrl != '') {
            
            if(friendlyUrl.contains('/')) {
                // from case wizard
                String[] salesforceUrlParameters = friendlyUrl.split('/');
                System.debug('^^^^^salesforceUrlParameters: '+salesforceUrlParameters);
                Integer counter = 0;
                for(String salesforceUrlParameter : salesforceUrlParameters) {
                    if(salesforceUrlParameter != null && salesforceUrlParameter != '') {
                        //salesforceUrlParameter = EncodingUtil.urlDecode(salesforceUrlParameter, 'UTF-8');
                        if(counter == 0)
                            salesforceUrl += '&fromCaseWizard='+salesforceUrlParameter;
                        else if(counter == 1)
                            salesforceUrl += '&subject='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                        else if(counter == 2)
                            salesforceUrl += '&severity='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                        else if(counter == 3)
                            salesforceUrl += '&description='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                        else if(counter == 4)
                            salesforceUrl += '&recordTypeName='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                            
                        ++counter;
                    }
                }
                if(salesforceUrl != null && salesforceUrl != '') {
                    if(salesforceUrl.contains('%0A'))
                        salesforceUrl = salesforceUrl.replace('%0A','_newline_'); // changing the newline parameter manually, otherwise it breaks the url
                    if(salesforceUrl.contains('%3F'))
                        salesforceUrl = salesforceUrl.replace('%3F','_questionmark_'); // 
                }
                    
            } else {
                // from case detail page
                salesforceUrl += '&caseId='+friendlyUrl+'/';
            }
            
        }
        return salesforceUrl;
    }
}