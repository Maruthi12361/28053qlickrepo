/*
    Initial development: MTM
    2014-05-15  MTM CR# XXXX  UpdateDealSplitChildOpportunities.
*/
	
global  class UpdateDealSplitChildOpportunities implements Database.Batchable<Opportunity>  
{
    global UpdateDealSplitChildOpportunities(){System.Debug('Starting: UpdateDealSplitChildOpportunities');}
    
    global Iterable<Opportunity> start(Database.BatchableContext BC) {        
        // Get all DS opportunities updated in last one hour
        System.Debug('Starting: UpdateDealSplitChildOpportunities :Start collecting DS Opportunities');
        List<Opportunity> oppIds = new List<Opportunity>();
        List<Opportunity> childOpps = new List<Opportunity>();
        datetime runTime = DateTime.Now();
        oppIds = [Select Id  From Opportunity o where o.Split_Opportunity__c = true
        		  and o.LastModifiedDate > :runtime.addHours(-1).addMinutes(10)];
        		  
        childOpps = [Select Id  From Opportunity o where o.Deal_Split_Parent_Opportunity__c = :oppIds];
        System.Debug('DS childrens to update count = ' + childOpps.size());
        System.Debug('Ending: UpdateDealSplitChildOpportunities:Start collecting DS Opportunities');
        return(childOpps);        
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        
        System.Debug('Starting: execute UpdateDealSplitChildOpportunities ');
        Semaphores.UpdateDealSplitChildOpp_BatchIsRunning = true;
        System.Debug('lastModifiedUpdate: UpdateDealSplitChildOpp_BatchIsRunning = ' + Semaphores.UpdateDealSplitChildOpp_BatchIsRunning);
        List<Opportunity> ToUpdate = new List<Opportunity>();        
        for(Opportunity ops: scope)
        {
            ToUpdate.add(ops);
        }
        update ToUpdate;
        System.Debug('Finishing: execute UpdateDealSplitChildOpportunities');    
    }
    
    global void finish(Database.BatchableContext BC) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email, 'mtm@qlikview.com'};
        
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex UpdateDealSplitChildOpportunities status ' + a.Status);
        mail.setPlainTextBody
        ('The batch Apex job  UpdateDealSplitChildOpportunities processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        try
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch(EmailException ex)
        {
            
        }
    } 

}