/*
BSL-156 Batch Created to populate original fields data from prior fields data so that prior fields can be deleted
Created by shubham Gupta on 15/5/2018
*/
public class OriginalPopulation implements Database.Batchable<sobject>, Database.stateful {

	public Map<Id, String> errorMap;

	public OriginalPopulation(){
		errorMap = new Map<Id,String>();
	}
	
	public Database.QueryLocator start(Database.BatchableContext bc){
		string query = 'SELECT id, Bypass_Rules__c,Quote_Document_CALs_Original__c,Quote_Document_CALs_Prior__c,Quote_Named_CALs_Original__c,Quote_Named_CALs_Prior__c,Quote_Session_CALs_Original__c,Quote_Session_CALs_Prior__c,Quote_Usage_CALs_Original__c,Quote_Usage_CALs_Prior__c FROM SBQQ__Quote__c WHERE (Quote_Document_CALs_Prior__c != 0.0 OR Quote_Named_CALs_Prior__c != 0.0 OR Quote_Session_CALs_Prior__c != 0.0 OR Quote_Usage_CALs_Prior__c != 0.0) AND (Quote_Document_CALs_Original__c = NULL AND Quote_Named_CALs_Original__c = NULL AND Quote_Session_CALs_Original__c = NULL AND Quote_Usage_CALs_Original__c = NULL)';
		return Database.getQueryLocator(query);

	}
		
	public void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> scope){
		List<SBQQ__Quote__c> toupdate = new List<SBQQ__Quote__c>();
		for(SBQQ__Quote__c qt:scope){
			qt.Bypass_Rules__c = true;
			qt.Quote_Document_CALs_Original__c = qt.Quote_Document_CALs_Prior__c;
			qt.Quote_Named_CALs_Original__c = qt.Quote_Named_CALs_Prior__c;
			qt.Quote_Session_CALs_Original__c = qt.Quote_Session_CALs_Prior__c;
			qt.Quote_Usage_CALs_Original__c = qt.Quote_Usage_CALs_Prior__c;
			toupdate.add(qt);
		}
		Semaphores.SetAllSemaphoresToTrue();
 		List<Database.SaveResult> dsrs = Database.update(toupdate,false);
 		Integer index = 0;
 		for(Database.SaveResult dsr:dsrs){
 			if(!dsr.isSuccess()){
 				String errMsg = dsr.getErrors()[0].getMessage();
 				errorMap.put(toupdate[index].Id, errMsg);
 			}
 			index++;
 		}
	}

	public void finish(Database.BatchableContext BC){
		String body = 'Your batch job '
             + 'OriginalPopulation '
             + 'has finished. \n' 
             + 'There were '
             + errorMap.size()
             + ' errors. Please find the error list attached to the email.';

         String finalstr = 'Id, Error \n';
         String subject = 'Quote - Apex Batch Error List';
         String attName = 'Quote Errors.csv';
         for(Id id:errorMap.keySet()){
         	string err = errorMap.get(id);
         	string recordString = '"'+id+'","'+err+'"\n';
         	finalstr = finalstr + recordString;
         }
 		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
 
        // Create the email attachment    
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalstr));
 
            // Sets the paramaters of the email
            email.setSubject( subject );
            email.setToAddresses( new String[] {'ext_axf@qlik.com','Celia.Astheimer@qlik.com'} );
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
 
            // Sends the email
            Messaging.SendEmailResult [] r = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
	}

		
	
}