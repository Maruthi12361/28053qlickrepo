/*
    03.03.2017 : Rodion Vakulovskyi : Updated functionality Opportunity_ManageForecastProducts
    2017-04-14  MTM     q2cw related changes
    2017-07-17  Linus L�fberg   Added exclusion of Qlik Online Purchase rt on processInsert()
    2017-10-04  Linus L�fberg   QCW-4035 Add support for product family Subscription
    2017-09-23 Shubham Gupta QCW-2849 Added exclusion to not to include Deal Split opportunity in this code at line 40 and 95,
                coverted Query to check record types from set at line 156
*/
public class OpportunityForecastProductsHandler {
    //private static final String DEALCHILD = 'Deal_Split_Child';
    //private static final String CCSDEALCHILD = 'CCS_Deal_Split_Child';
    //private static final String OEMORDERDEALCHILD = 'OEM_Order_Deal_Split_Child';
    //private static final String OEMRECRDEALCHILD = 'OEM_Recruitment_Deal_Split_Child';
    private static final Set<String> childRecType = new Set<String>{'Deal_Split_Child','OEM_Order_Deal_Split_Child','OEM_Recruitment_Deal_Split_Child'};
    private static List<String> ProductTypes = new List<String> {'Consulting', 'Licenses', 'Education', 'Support Services', 'Subscription', 'Misc'};
    private static  Boolean afterInsertFlag = false;
    private static  Boolean afterUpdateFlag = false;

    public static void afterUpdate(List<Opportunity> inputList, Map<Id, Opportunity> oldMap){
        System.debug('Starting OpportunityForecastProductsHandler:afterUpdate ');
        processUpdate(inputList, oldMap);
        System.debug('Ending OpportunityForecastProductsHandler:afterUpdate ');
    }
    public static void afterInsert(List<Opportunity> inputList){
        System.debug('Starting OpportunityForecastProductsHandler:afterInsert ');
        processInsert(inputList);
        System.debug('Ending OpportunityForecastProductsHandler:afterInsert ');
    }

    private static void processInsert(List<Opportunity> inputList) {
        if(!afterInsertFlag) {
            Set<String> setOfOppsIds = new Set<String>();
            List<Opportunity> listOfOppty = new List<Opportunity>();
            List<OpportunityLineItem> listOfITemsToDelete = new List<OpportunityLineItem>();
            List<OpportunityLineItem> listOfProcessing = new List<OpportunityLineItem>();
            Set<String> setOfavailableRTypes = queryRecordTypes();
            for(Opportunity itemOpps : inputList) {


                    //Exclude Qlik Online Purchase Record Type.
                    if (itemOpps.RecordTypeId.equals(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Qlik Online Purchase').getRecordTypeId()) || setOfavailableRTypes.contains(itemOpps.RecordTypeId)) {
                        continue;
                    }

                    setOfOppsIds.add(itemOpps.id);
                    listOfOppty.add(itemOpps);
            }
            if(!listOfOppty.isEmpty()){
            String priceBookId = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Standard_PriceBook__c;
            Map<Id, Opportunity> oppMapForRtypes = new Map<Id, Opportunity>([SELECT Id, (Select Id, SBQQ__Primary__c From SBQQ__Quotes2__r where SBQQ__Primary__c = true), (select Id, OpportunityId, Product_Family__c, Name__c, PricebookEntryId, UnitPrice from OpportunityLineItems) FROM Opportunity WHERE Id IN : setOfOppsIds]);
            List<PricebookEntry> listOfPriceBookEntry = [SELECT Id, CurrencyIsoCode, IsActive, Name, Pricebook2Id, Product2Id, ProductCode, UnitPrice, UseStandardPrice FROM PricebookEntry WHERE IsActive = TRUE AND Pricebook2Id = :priceBookId and ProductCode in :ProductTypes];
            for(Opportunity itemOpps : listOfOppty) {
                List<SBQQ__Quote__c> listQuote = oppMapForRtypes.get(itemOpps.Id).SBQQ__Quotes2__r;
                if(listQuote.Size() > 0)
                {
                    continue;
                }
                List<OpportunityLineItem> listOfExistingOppLineItem = oppMapForRtypes.get(itemOpps.Id).OpportunityLineItems;
                Boolean OppForecastAmountIsZero = false;
                Boolean oppLineItemExist = false;
            // For License
                processLicenseItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            //For Consulting
                processConsultingItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            //For Education
                processEducationItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            //For Support
                processSupportItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            //For Subscription
                processSubscriptionItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            }
          }
          //
        //Process Oportunity Line Items
                if (listOfProcessing.size() > 0) {
                //throw new myException();
                    upsert listOfProcessing;
                }

                if (listOfITemsToDelete.size() > 0) {
                    System.debug(listOfITemsToDelete);
                    delete listOfITemsToDelete;
                }
                afterInsertFlag = true;
            }
    }

    private static void processUpdate(List<Opportunity> inputList, Map<Id, Opportunity> oldMap) {
    if(!afterUpdateFlag){
        Set<String> setOfOppsIds = new Set<String>();
        List<Opportunity> listOfOppty = new List<Opportunity>();
        List<OpportunityLineItem> listOfITemsToDelete = new List<OpportunityLineItem>();
        List<OpportunityLineItem> listOfProcessing = new List<OpportunityLineItem>();
        Set<String> setOfavailableRTypes = queryRecordTypes();
        for(Opportunity itemOpps : inputList) {
            if(!setOfavailableRTypes.contains(itemOpps.RecordTypeId)) {
                System.Debug('processUpdate: adding Ids');
                setOfOppsIds.add(itemOpps.id);
                listOfOppty.add(itemOpps);
            }
        }
        if(!listOfOppty.isEmpty()){
        String priceBookId = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Standard_PriceBook__c;
        Map<Id, Opportunity> oppMapForRtypes = new Map<Id, Opportunity>([SELECT Id, (Select Id, SBQQ__Primary__c From SBQQ__Quotes2__r where SBQQ__Primary__c = true), RecordTypeId, (select Id, OpportunityId, Name__c, Product_Family__c, PricebookEntryId, UnitPrice from OpportunityLineItems) FROM Opportunity WHERE Id IN : setOfOppsIds]);
        List<PricebookEntry> listOfPriceBookEntry = [SELECT Id, CurrencyIsoCode, IsActive, Name, Pricebook2Id, Product2Id, ProductCode, UnitPrice, UseStandardPrice FROM PricebookEntry WHERE IsActive = TRUE AND Pricebook2Id = :priceBookId and ProductCode in :ProductTypes];
        for(Opportunity itemOpps : listOfOppty) {
            System.debug('11111' + itemOpps.LastModifiedDate);
            System.debug('22222' + oldMap.get(itemOpps.id).LastModifiedDate);
            List<SBQQ__Quote__c> listQuote = oppMapForRtypes.get(itemOpps.Id).SBQQ__Quotes2__r;
            if(listQuote.Size() > 0)
            {
                continue;
            }
            List<OpportunityLineItem> listOfExistingOppLineItem = oppMapForRtypes.get(itemOpps.Id).OpportunityLineItems;
            Boolean OppForecastAmountIsZero = false;
            Boolean oppLineItemExist = false;
            // For License
            if(itemOpps.License_Forecast_Amount__c != oldMap.get(itemOpps.id).License_Forecast_Amount__c){
                System.debug('Licensesss');
                processLicenseItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            }
            //For Consulting
            if(itemOpps.Consultancy_Forecast_Amount__c != oldMap.get(itemOpps.id).Consultancy_Forecast_Amount__c){
                processConsultingItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            }
            //For Education
            if(itemOpps.Education_Forecast_Amount__c != oldMap.get(itemOpps.id).Education_Forecast_Amount__c){
                processEducationItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            }
            //For Support
            if(itemOpps.Support_Forecast_Amount__c != oldMap.get(itemOpps.id).Support_Forecast_Amount__c){
                processSupportItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            }
            //For Subscription
            if(itemOpps.Subscription_Forecast_Amount__c != oldMap.get(itemOpps.id).Subscription_Forecast_Amount__c){
                processSubscriptionItem(itemOpps, OppForecastAmountIsZero, oppLineItemExist, listOfExistingOppLineItem, listOfITemsToDelete, listOfProcessing, listOfPriceBookEntry);
            }
        }
        }
         //Process Oportunity Line Items
            if (listOfProcessing.size() > 0) {
                System.debug(listOfProcessing);
                upsert listOfProcessing;
            }

            if (listOfITemsToDelete.size() > 0) {
                System.debug(listOfITemsToDelete);
                delete listOfITemsToDelete;
            }
            afterUpdateFlag = true;
    }
    }

    private static Set<String> queryRecordTypes() {
        Set<String> setOfrecordTypes = new Set<String>();
        /*for(RecordType item : [Select id From recordType where DeveloperName in :childRecType]) {
            
        }*/
        
        Map<String, RecordTypeInfo> oppRecordTypes = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName(); 
        
        for(String devName: childRecType) {
           	setOfrecordTypes.add(oppRecordTypes.get(devName).getRecordTypeId());
        }
      
    	return setOfrecordTypes;
    }

    private static void processLicenseItem(Opportunity itemOpps, Boolean OppForecastAmountIsZero, Boolean oppLineItemExist, List<OpportunityLineItem> listOfExistingOppLineItem, List<OpportunityLineItem> listOfITemsToDelete, list<OpportunityLineItem> listOfProcessing, List<PricebookEntry> listOfPriceBookEntry) {
        OpportunityLineItem oppLineItemsLicense = new OpportunityLineItem();
        if (itemOpps.License_Forecast_Amount__c == null || itemOpps.License_Forecast_Amount__c == 0) {
            OppForecastAmountIsZero = true;
        }
        System.debug('processLicenseItem: 1111');
        if (listOfExistingOppLineItem.size() > 0) {
            for (OpportunityLineItem licenseItem : listOfExistingOppLineItem) {
                if (licenseItem.Product_Family__c == 'Licenses') {
                System.debug('processLicenseItem:2222');
                    oppLineItemsLicense = licenseItem;
                    oppLineItemExist = true;
                }
            }
        }
        if (OppForecastAmountIsZero && oppLineItemExist) {
            listOfITemsToDelete.add(oppLineItemsLicense);
            System.debug('processLicenseItem: 3333');
        }
        if (oppLineItemExist && !OppForecastAmountIsZero) {
                System.debug('processLicenseItem: 44444');
                oppLineItemsLicense.UnitPrice = itemOpps.License_Forecast_Amount__c;
                System.debug('processLicenseItem: oppLineItemsLicense.UnitPrice = ' + oppLineItemsLicense.UnitPrice);
                listOfProcessing.add(oppLineItemsLicense);
        }
        if(!oppLineItemExist && !OppForecastAmountIsZero) {
                System.debug('processLicenseItem: oppLineItemsLicense.UnitPrice = ' + oppLineItemsLicense.UnitPrice);
                listOfProcessing.add(Opportunity_ManageForecastProductsHelper.CreateProduct(itemOpps, 'Licenses', itemOpps.License_Forecast_Amount__c, listOfPriceBookEntry));
        }
    }

    private static void processConsultingItem(Opportunity itemOpps, Boolean OppForecastAmountIsZero, Boolean oppLineItemExist, List<OpportunityLineItem> listOfExistingOppLineItem, List<OpportunityLineItem> listOfITemsToDelete, list<OpportunityLineItem> listOfProcessing, List<PricebookEntry> listOfPriceBookEntry) {
        OppForecastAmountIsZero = false;
        oppLineItemExist = false;
        OpportunityLineItem oppLineItemConsulting = new OpportunityLineItem();
        if (itemOpps.Consultancy_Forecast_Amount__c == null || itemOpps.Consultancy_Forecast_Amount__c == 0) {
            OppForecastAmountIsZero = true;
        }
        if (listOfExistingOppLineItem.size() > 0) {
            for (OpportunityLineItem itemOppLine : listOfExistingOppLineItem) {
                if (itemOppLine.Product_Family__c == 'Consulting') {
                    oppLineItemConsulting = itemOppLine;
                    oppLineItemExist = true;
                }
            }
        }
        if (OppForecastAmountIsZero && oppLineItemExist) {
            listOfITemsToDelete.add(oppLineItemConsulting);
        }
        if (oppLineItemExist && !OppForecastAmountIsZero) {
                oppLineItemConsulting.UnitPrice = itemOpps.Consultancy_Forecast_Amount__c;
                listOfProcessing.add(oppLineItemConsulting);
        }
        if (!oppLineItemExist && !OppForecastAmountIsZero) {
                listOfProcessing.add(Opportunity_ManageForecastProductsHelper.CreateProduct(itemOpps, 'Consulting', itemOpps.Consultancy_Forecast_Amount__c, listOfPriceBookEntry));
                System.debug('Test');
        }
        System.debug('Test');
    }

    private static void processEducationItem(Opportunity itemOpps, Boolean OppForecastAmountIsZero, Boolean oppLineItemExist, List<OpportunityLineItem> listOfExistingOppLineItem, List<OpportunityLineItem> listOfITemsToDelete, list<OpportunityLineItem> listOfProcessing, List<PricebookEntry> listOfPriceBookEntry) {
        OppForecastAmountIsZero = false;
        oppLineItemExist = false;
        OpportunityLineItem oppLineItemEducation = new OpportunityLineItem();
        if (itemOpps.Education_Forecast_Amount__c == null || itemOpps.Education_Forecast_Amount__c == 0) {
            OppForecastAmountIsZero = true;
        }
        if (listOfExistingOppLineItem.size() > 0) {
            for (OpportunityLineItem itemOppLine : listOfExistingOppLineItem) {
                if (itemOppLine.Product_Family__c == 'Education') {
                    oppLineItemEducation = itemOppLine;
                    oppLineItemExist = true;
                }
            }
        }
        if (OppForecastAmountIsZero && oppLineItemExist) {
            listOfITemsToDelete.add(oppLineItemEducation);
        }
        if (oppLineItemExist && !OppForecastAmountIsZero) {
                oppLineItemEducation.UnitPrice = itemOpps.Education_Forecast_Amount__c;
                listOfProcessing.add(oppLineItemEducation);
        }
        if (!oppLineItemExist && !OppForecastAmountIsZero) {
                listOfProcessing.add(Opportunity_ManageForecastProductsHelper.CreateProduct(itemOpps, 'Education', itemOpps.Education_Forecast_Amount__c, listOfPriceBookEntry));
        }
    }

    private static void processSupportItem(Opportunity itemOpps, Boolean OppForecastAmountIsZero, Boolean oppLineItemExist, List<OpportunityLineItem> listOfExistingOppLineItem, List<OpportunityLineItem> listOfITemsToDelete, list<OpportunityLineItem> listOfProcessing, List<PricebookEntry> listOfPriceBookEntry) {
         OppForecastAmountIsZero = false;
         oppLineItemExist = false;
         OpportunityLineItem oppLineItemSupport = new OpportunityLineItem();
         if (itemOpps.Support_Forecast_Amount__c == null || itemOpps.Support_Forecast_Amount__c == 0) {
            OppForecastAmountIsZero = true;
         }
         if (listOfExistingOppLineItem.size() > 0) {
            for (OpportunityLineItem itemOppLine : listOfExistingOppLineItem) {
                if (itemOppLine.Product_Family__c == 'Support Services') {
                    oppLineItemSupport = itemOppLine;
                    oppLineItemExist = true;
                }
            }
         }
         if (OppForecastAmountIsZero && oppLineItemExist) {
            listOfITemsToDelete.add(oppLineItemSupport);
         }
         if (oppLineItemExist && !OppForecastAmountIsZero) {
                oppLineItemSupport.UnitPrice = itemOpps.Support_Forecast_Amount__c;
                listOfProcessing.add(oppLineItemSupport);
         }
         if (!oppLineItemExist && !OppForecastAmountIsZero) {
                listOfProcessing.add(Opportunity_ManageForecastProductsHelper.CreateProduct(itemOpps, 'Support Services', itemOpps.Support_Forecast_Amount__c, listOfPriceBookEntry));
         }
    }

    //Add method to process Subscription items
    private static void processSubscriptionItem(Opportunity itemOpps, Boolean OppForecastAmountIsZero, Boolean oppLineItemExist, List<OpportunityLineItem> listOfExistingOppLineItem, List<OpportunityLineItem> listOfITemsToDelete, list<OpportunityLineItem> listOfProcessing, List<PricebookEntry> listOfPriceBookEntry) {
         OppForecastAmountIsZero = false;
         oppLineItemExist = false;
         OpportunityLineItem oppLineItemSubscription = new OpportunityLineItem();
         if (itemOpps.Subscription_Forecast_Amount__c == null || itemOpps.Subscription_Forecast_Amount__c == 0) {
            OppForecastAmountIsZero = true;
         }
         if (listOfExistingOppLineItem.size() > 0) {
            for (OpportunityLineItem itemOppLine : listOfExistingOppLineItem) {
                if (itemOppLine.Product_Family__c == 'Subscription') {
                    oppLineItemSubscription = itemOppLine;
                    oppLineItemExist = true;
                }
            }
         }
         if (OppForecastAmountIsZero && oppLineItemExist) {
            listOfITemsToDelete.add(oppLineItemSubscription);
         }
         if (oppLineItemExist && !OppForecastAmountIsZero) {
                oppLineItemSubscription.UnitPrice = itemOpps.Subscription_Forecast_Amount__c;
                listOfProcessing.add(oppLineItemSubscription);
         }
         if (!oppLineItemExist && !OppForecastAmountIsZero) {
                listOfProcessing.add(Opportunity_ManageForecastProductsHelper.CreateProduct(itemOpps, 'Subscription', itemOpps.Subscription_Forecast_Amount__c, listOfPriceBookEntry));
         }
    }

    public class myException extends Exception{}

}