/**************************************************
* Change Log:
* 
* 2019-03-29    CCE Initial development. CHG0035276 - Prepopulate certain fields on new Marketing deliverable records
**************************************************/
public with sharing class DispatcherMDNewController {
	public DispatcherMDNewController(ApexPages.StandardController controller) {
        this.controller = controller;
    }

    public PageReference getRedir() {

        Id IdRT = ApexPages.currentPage().getParameters().get('RecordType');
        System.Debug('DispatcherMDNewController: IdRT = ' + IdRT);
        String IdORName = ApexPages.currentPage().getParameters().get('CF00ND0000003jbdQ');
        System.Debug('DispatcherMDNewController: IdORName = ' + IdORName);


        PageReference newPage;
        Marketing_Deliverable_Default__c MDSettings = Marketing_Deliverable_Default__c.getInstance();
        string defaultAssignedToUser = MDSettings.Default_Assigned_To_User__c;
        string defaultAssignedToUserId = MDSettings.Default_Assigned_To_UserId__c;
        System.Debug('DispatcherMDNewController: defaultAssignedToUser = ' + defaultAssignedToUser);
        System.Debug('DispatcherMDNewController: defaultAssignedToUserId = ' + defaultAssignedToUserId);
        Date todaysDate = addBusinessDays(system.today(), 2);
        string sTodaysDate = todaysDate.format();        

        String url = '/a3d/e?retURL=/a3d/o&nooverride=true';
        url += '&RecordType=' + EncodingUtil.urlEncode(IdRT, 'UTF-8');
        if (String.isNotBlank(IdORName)) {url += '&CF00ND0000003jbdQ=' + EncodingUtil.urlEncode(IdORName, 'UTF-8');}
        url += '&CF00ND0000003jbf2_lkid=' + EncodingUtil.urlEncode(userinfo.getuserid(), 'UTF-8');
        url += '&CF00ND0000003jbf2=' + EncodingUtil.urlEncode(userinfo.getFirstName()+ ' ' + userinfo.getLastName(), 'UTF-8');
        url += '&CF00ND0000003jbb5_lkid=' + EncodingUtil.urlEncode(defaultAssignedToUserId, 'UTF-8');
        url += '&CF00ND0000003jbb5=' + EncodingUtil.urlEncode(defaultAssignedToUser, 'UTF-8');
        url += '&00ND0000003jbcx=' + EncodingUtil.urlEncode(sTodaysDate, 'UTF-8');

        //As we only have these 3 types of record type we must always be doing one of them so we no longer need this test
        //if ((IdRT == '012D0000000Jrq5') || (IdRT == '012D0000000JrqP') || (IdRT == '012D0000000Jrq0')) {   //Data Hygiene or Response Upload or Target List Pull / Campaign Creation
            newPage = new PageReference(url);
            System.Debug('DispatcherMDNewController: newPage = ' + newPage);
            return newPage.setRedirect(true);
        //} else {
        //    System.Debug('DispatcherMDNewController: null IdRT = ' + IdRT);
        //    return null;
        //}

    }

    public static Date addBusinessDays(Date startDate, Integer iDays)
    {
        Integer businessDaysAdded = 0;
        Date currentDate = startDate;
        while (businessDaysAdded < iDays) {
            currentDate = currentDate.addDays(1);
            Datetime d = datetime.newInstance(currentDate.year(), currentDate.month(),currentDate.day());

            if (d.format('E') != 'Sat' && d.format('E') != 'Sun') {
                // it's a business day, so add 1 to the counter that works towards the amount of days to add
                businessDaysAdded = businessDaysAdded + 1;
            } 
        }       
        return currentDate;
    }

    private final ApexPages.StandardController controller;
}