/*********************************************

	Class:LeadZiftClawbackHandlerTest

	Description: 
				Test class for LeadZiftClawbackHandlerTest

	Log History:
	2017-06-21    BAD    Created - CHG0031645
    2020-03-13 CCE Updated for CHG0037726 DGM-55 FUR Revamp 2020
**********************************************/
@isTest
private class LeadZiftClawbackHandlerTest
{


	@isTest 
	static void test_ZiftClawback1() {

		System.debug('Start Test LeadZiftClawbackHandlerTest');

    	User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
    	LeadZiftClawbackHandler tempObj = new LeadZiftClawbackHandler();

        System.RunAs(mockSysAdmin)
        { 

			ZiftSettings__c csZift = ZiftSettings__c.getInstance();
	        if(csZift.CreatedDate == null) {
	            ZiftSettings__c s = LeadZiftClawbackHandlerTest.newSettings();
	            upsert s;
	        }

			Lead lead = LeadZiftClawbackHandlerTest.createMockLead();				

	        Test.startTest();

			Semaphores.TriggerHasRun(1);
	        
	        lead.Zift_Lead_Status__c = 'Closed - Not Converted';
	        lead.Zift_Unqualified_Invalid_Reason__c = 'Already Working With Another Partner';
	        Semaphores.LeadTriggerHandlerAfterUpdate = false;
			update lead;

			Lead l = [SELECT Id, Status, Follow_Up_Disqualified_Reason__c, RecordTypeId FROM Lead WHERE Id=:lead.id];

			system.assertEquals('Follow-Up Disqualified', l.Status);
			system.assertEquals('Already Working', l.Follow_Up_Disqualified_Reason__c);
			system.assertEquals('01220000000DOzwAAG', l.RecordTypeId);
			
			Test.stopTest();
		} 

	}

	@isTest 
	static void test_ZiftClawback2() {

		System.debug('Start Test LeadZiftClawbackHandlerTest');

    	User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
    	LeadZiftClawbackHandler tempObj = new LeadZiftClawbackHandler();

        System.RunAs(mockSysAdmin)
        { 

			ZiftSettings__c csZift = ZiftSettings__c.getInstance();
	        if(csZift.CreatedDate == null) {
	            ZiftSettings__c s = LeadZiftClawbackHandlerTest.newSettings();
	            upsert s;
	        }

			Lead lead = LeadZiftClawbackHandlerTest.createMockLead();					

	        Test.startTest();

			Semaphores.TriggerHasRun(1);
	        
	        lead.Zift_Lead_Status__c = 'Closed - Not Converted';
	        lead.Zift_Unqualified_Invalid_Reason__c = 'Already Working With Qlik';
	        Semaphores.LeadTriggerHandlerAfterUpdate = false;
			update lead;

			Lead l = [SELECT Id, Status, Follow_Up_Disqualified_Reason__c, RecordTypeId FROM Lead WHERE Id=:lead.id];

			system.assertEquals('Follow-Up Disqualified', l.Status);
			system.assertEquals('Already Working', l.Follow_Up_Disqualified_Reason__c);
			system.assertEquals('01220000000DOzwAAG', l.RecordTypeId);
			
			Test.stopTest();
		} 
	}


	@isTest 
	static void test_ZiftClawback3() {

		System.debug('Start Test LeadZiftClawbackHandlerTest');

    	User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
    	LeadZiftClawbackHandler tempObj = new LeadZiftClawbackHandler();

        System.RunAs(mockSysAdmin)
        { 

			ZiftSettings__c csZift = ZiftSettings__c.getInstance();
	        if(csZift.CreatedDate == null) {
	            ZiftSettings__c s = LeadZiftClawbackHandlerTest.newSettings();
	            upsert s;
	        }

			Lead lead = LeadZiftClawbackHandlerTest.createMockLead();		

	        Test.startTest();

			Semaphores.TriggerHasRun(1);
	        
	        lead.Zift_Lead_Status__c = 'Closed - Not Converted';
	        lead.Zift_Unqualified_Invalid_Reason__c = 'Incorrect Data/Junk';
	        Semaphores.LeadTriggerHandlerAfterUpdate = false;
			update lead;

			Lead l = [SELECT Id, OwnerID, Status, Follow_Up_Disqualified_Reason__c, RecordTypeId FROM Lead WHERE Id=:lead.id];

			//system.assertEquals('Follow-Up Rejected', l.Status);
			system.assertEquals('012D0000000KE2FIAW', l.RecordTypeId);
			system.assertEquals('00520000000z3PrAAI', l.OwnerID);

			Test.stopTest();
		} 
	}


	@isTest 
	static void test_ZiftClawback4() {

		System.debug('Start Test LeadZiftClawbackHandlerTest');

    	User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
    	LeadZiftClawbackHandler tempObj = new LeadZiftClawbackHandler();

        System.RunAs(mockSysAdmin)
        { 

			ZiftSettings__c csZift = ZiftSettings__c.getInstance();
	        if(csZift.CreatedDate == null) {
	            ZiftSettings__c s = LeadZiftClawbackHandlerTest.newSettings();
	            upsert s;
	        }

            Lead lead = LeadZiftClawbackHandlerTest.createMockLead();

	        Test.startTest();

			Semaphores.TriggerHasRun(1);
	        
	        lead.Zift_Lead_Status__c = 'Closed - Not Converted';
	        lead.Zift_Unqualified_Invalid_Reason__c = 'Lost to Qlik Competitor';
	        Semaphores.LeadTriggerHandlerAfterUpdate = false;
			update lead;

			Lead l = [SELECT Id, Status, Follow_Up_Disqualified_Reason__c, RecordTypeId FROM Lead WHERE Id=:lead.id];

			system.assertEquals('Follow-Up Disqualified', l.Status);
			system.assertEquals('No Interest', l.Follow_Up_Disqualified_Reason__c);
			system.assertEquals('01220000000DOzwAAG', l.RecordTypeId);

			Test.stopTest();
		} 
	}

	@isTest 
	static void test_ZiftClawback5() {

		System.debug('Start Test LeadZiftClawbackHandlerTest');

    	User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
    	LeadZiftClawbackHandler tempObj = new LeadZiftClawbackHandler();

        System.RunAs(mockSysAdmin)
        { 

			ZiftSettings__c csZift = ZiftSettings__c.getInstance();
	        if(csZift.CreatedDate == null) {
	            ZiftSettings__c s = LeadZiftClawbackHandlerTest.newSettings();
	            upsert s;
	        }

            Lead lead = LeadZiftClawbackHandlerTest.createMockLead();

	        Test.startTest();

			Semaphores.TriggerHasRun(1);
	        
	        lead.Zift_Lead_Status__c = 'Closed - Not Converted';
	        lead.Zift_Unqualified_Invalid_Reason__c = 'No Budget or Interest';
			Semaphores.LeadTriggerHandlerAfterUpdate = false;
			update lead;

			Lead l = [SELECT Id, Status, Follow_Up_Disqualified_Reason__c, RecordTypeId FROM Lead WHERE Id=:lead.id];

			system.assertEquals('Follow-Up Disqualified', l.Status);
			system.assertEquals('No Interest', l.Follow_Up_Disqualified_Reason__c);
			system.assertEquals('01220000000DOzwAAG', l.RecordTypeId);

			Test.stopTest();
		} 
	}


	@isTest 
	static void test_ZiftClawback6() {

		System.debug('Start Test LeadZiftClawbackHandlerTest');

    	User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
    	LeadZiftClawbackHandler tempObj = new LeadZiftClawbackHandler();

        System.RunAs(mockSysAdmin)
        { 

			ZiftSettings__c csZift = ZiftSettings__c.getInstance();
	        if(csZift.CreatedDate == null) {
	            ZiftSettings__c s = LeadZiftClawbackHandlerTest.newSettings();
	            upsert s;
	        }

            Lead lead = LeadZiftClawbackHandlerTest.createMockLead();

	        Test.startTest();

			Semaphores.TriggerHasRun(1);
	        
	        lead.Zift_Lead_Status__c = 'Closed - Not Converted';
	        lead.Zift_Unqualified_Invalid_Reason__c = 'Failed to Reach Contact';
	       	Semaphores.LeadTriggerHandlerAfterUpdate = false;
			update lead;

			Lead l = [SELECT Id, Status, Follow_Up_Disqualified_Reason__c, RecordTypeId FROM Lead WHERE Id=:lead.id];

			system.assertEquals('Follow-Up Disqualified', l.Status);
			system.assertEquals('No Response', l.Follow_Up_Disqualified_Reason__c);
			system.assertEquals('01220000000DOzwAAG', l.RecordTypeId);

			Test.stopTest();
		} 
	}


	@isTest 
	static void test_ZiftClawback7() {

		System.debug('Start Test LeadZiftClawbackHandlerTest');

    	User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
    	LeadZiftClawbackHandler tempObj = new LeadZiftClawbackHandler();

        System.RunAs(mockSysAdmin)
        { 

			ZiftSettings__c csZift = ZiftSettings__c.getInstance();
	        if(csZift.CreatedDate == null) {
	            ZiftSettings__c s = LeadZiftClawbackHandlerTest.newSettings();
	            upsert s;
	        }

            Lead lead = LeadZiftClawbackHandlerTest.createMockLead();

	        Test.startTest();

			Semaphores.TriggerHasRun(1);
	        
	        lead.Zift_Lead_Status__c = 'Closed - Not Converted';
	        lead.Zift_Unqualified_Invalid_Reason__c = 'Already Working With Prospect or Customer';
	        Semaphores.LeadTriggerHandlerAfterUpdate = false;			
			update lead;

			Lead l = [SELECT Id, Status, Follow_Up_Rejected_Reason__c, RecordTypeId FROM Lead WHERE Id=:lead.id];

			system.assertEquals('Follow-Up Rejected', l.Status);
			system.assertEquals('Already Working', l.Follow_Up_Rejected_Reason__c);

			Test.stopTest();
		} 
	}
	public static Lead createMockLead()
	{
        Lead l = New Lead (
            FirstName = 'Lead',
            LastName = 'Test1001',
            Company = 'Test',
            Country = 'Sweden',
            Email = 'asd@ad.com',
            RecordTypeId = '012D0000000JsWAIA0',
            New_Responder_Follow_Up_Required__c = true
        );  
        insert l;	

        return l;
	}

    public static ZiftSettings__c newSettings() {        
        ZiftSettings__c settings = new ZiftSettings__c();                
        settings.InvReason_AlreadyWorkingPart__c = 'Already Working With Another Partner';
		settings.InvReason_AlreadyWorkingQlik__c = 'Already Working With Qlik';
		settings.InvReason_IncorrectDataJunk__c = 'Incorrect Data/Junk';
		settings.InvReason_LostToComp__c = 'Lost to Qlik Competitor';
		settings.InvReason_NoBudget__c = 'No Budget or Interest';
		settings.InvReason_FailReachContact__c = 'Failed to Reach Contact';
		settings.InvReason_AlreadyWorkingProspOrCust__c = 'Already Working With Prospect or Customer';
		settings.RT_StandardLead__c = '01220000000DOzwAAG';
		settings.RT_Archived__c = '012D0000000KE2FIAW';
        settings.LeadOwnerArchivedRT__c = '00520000000z3PrAAI';
		
        return settings;        
    }


}