/***********************
*
* 2019-02-14 - AIN - Fixed errors related to new duplicate rule
*
************************/
@isTest(seeAllData=True)
private class Z_QuoteTriggerHandlerTest {

    private static Account account;
    private static Account account2;
    private static Account account3,account4;
    private static Contact contact;
    private static Contact contact2,contact3,contact4,contact5;
    private static Zuora__CustomerAccount__c customerAccount;
    private static Zuora__CustomerAccount__c customerAccount2,customerAccount3, customerAccount4;
    private static Opportunity opp;
    private static Opportunity opp2,opp3,opp4,opp5;
    private static zqu__Quote__c quote;
    private static zqu__Quote__c quote2;
    private static zqu__Quote__c quote3,quote4,quote5,quote6;
    private static Product2 product;

    private static zqu__ProductRatePlan__c productRatePlan;
    private static zqu__QuoteAmendment__c quoteAmendment;
    private static zqu__QuoteRatePlan__c quoteRatePlan;
    private static zqu__QuoteAmendment__c quoteAmendment2, quoteAmendment3,quoteAmendment4,quoteAmendment5,quoteAmendment6;
    private static zqu__QuoteRatePlan__c quoteRatePlan2,quoteRatePlan3,quoteRatePlan4,quoteRatePlan5,quoteRatePlan6;



    private static void set_up_data() {
        /**Nov 1st update: comment out since ECUSTOMS__vc_config__c is taken care of by Qlik side*/
        //insert new ECUSTOMS__vc_config__c(Name = 'Visual Compliance', Yellow_Country_List__c = 'South Sudan;Lebanon;Venezuela;Russian Federation;Ukraine;Belarus;Zimbabwe;Iraq');
        // insert new ECUSTOMS__vc_config__c(Name = 'Visual Compliance');
        // account = Z_TestFactory.buildAccount();
        //insert account;


        Subsidiary__c subid = TestQuoteUtil.createSubsidiary();
        //QlikTech_Company__c QTCompId=TestQuoteUtil.createQlikTech('France', 'FR', subid.id);
        //Account acc = TestQuoteUtil.createAccount(QTCompId.Id);

        List<Account> accountList = new List<Account>();
        account =Z_TestFactory.makeAccount();
        accountList.add(account);

        account2 = Z_TestFactory.makeAccount('Ukraine', 'A very unique named test account', '','65000-480');
        accountList.add(account2);

        account3 = Z_TestFactory.makeAccount('United Kingdom', 'Best account in the world today', '','80011');
        account3.ECUSTOMS__RPS_Status__c= 'No Matches';
        account3.ECUSTOMS__RPS_Date__c= Date.Today();
        account3.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        account3.BillingStreet='fffff';
        account3.BillingCity='ggggg';
        system.debug('gdfgdfg'+Account3);
        accountList.add(account3);

        account4 = Z_TestFactory.makeAccount('Sweden', 'Uniqueness 100%', '','80221');
        account4.ECUSTOMS__RPS_Status__c= 'No Matches';
        account4.ECUSTOMS__RPS_Date__c= Date.Today();
        account4.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        account4.BillingStreet='hhhh';
        account4.BillingCity='iiiii';
        system.debug('gdfgdfg'+account4);
        accountList.add(account4);

        insert accountList;

        for (Account a : accountList) {
            a.Pending_Validation__c = FALSE;
        }
        update accountList;

        List<Contact> contactList = new List<Contact>();
        contact = Z_TestFactory.makeContact(account);
        contactList.add(contact);
        // insert contact;

        contact3 = Z_TestFactory.makeContact(account3,'aaaa1789','bbbb134534','AA1@gtytyt.com','United Kingdom','','80011');
        contact3.MailingStreet = 'hhhh';
        contact3.MailingCity = 'sdfsdfsd';
        contact3.phone = '404-464-4784';
        contactList.add(contact3);
        //insert contact3;

        contact2 = Z_TestFactory.makeContact(account2,'aaaa','bbbb','AA@ggg.com','Ukraine','','65000-480');
        contact2.MailingStreet = 'xcvxcvchhhh';
        contact2.MailingCity = 'jkljklkj';
        contact2.phone = '555-464-7784';
        contactList.add(contact2);
        //insert contact2;

        contact4 = Z_TestFactory.makeContact(account4,'bbba1789','ddffcvc','ghgh@xcxcxc.com','Sweden','','80221');
        contact4.MailingStreet = 'bnbnbn';
        contact4.MailingCity = 'zzxzxz';
        contact4.phone = '333-494-5784';
        contactList.add(contact4);
        //insert contact4;

        contact5 = Z_TestFactory.makeContact2(account);
        contactList.add(contact5);
        //insert contact5;
        insert contactList;

        List<Zuora__CustomerAccount__c> caList = new List<Zuora__CustomerAccount__c>();
        customerAccount = Z_TestFactory.createBillingAccount(account, 'id0123456789KK');
        caList.add(customerAccount);
        customerAccount2 = Z_TestFactory.createBillingAccount(account2, 'id0123456789UIN');
        caList.add(customerAccount2);
        customerAccount3 = Z_TestFactory.createBillingAccount(account3, 'id0123456789UIN1');
        caList.add(customerAccount3);
        insert caList;

        List<Opportunity> opportunityList = new List<Opportunity>();

        opp = Z_TestFactory.makeOpportunity(account, contact);
        opp.Revenue_Type__c='Direct';
        opportunityList.add(opp);

        //insert opp;

        opp2 = Z_TestFactory.makeOpportunity(account2, contact2,'aaaasas');
        opportunityList.add(opp2);
        opp2.Revenue_Type__c='Direct';

        //insert opp2;

        opp3 = Z_TestFactory.makeOpportunity(account3, contact3,'aaaasas12');
        opportunityList.add(opp3);
        opp3.Revenue_Type__c='Direct';


        //insert opp3;

        opp4 = Z_TestFactory.makeOpportunity(account, contact5,'aaaasas44');
        opportunityList.add(opp4);
        opp4.Revenue_Type__c='Direct';
        insert opportunityList;




        product = Z_TestFactory.makeProduct();
        product.Training_Product__c = 'Yes';
        insert product;

        productRatePlan = Z_TestFactory.makeProductRatePlan(product.Id, 'ratePlanId1');
        insert productRatePlan;

        List<zqu__Quote__c> quoteList = new List<zqu__Quote__c>();

        quote = Z_TestFactory.makeQuote2(opp, account);
        quote.zqu__BillToContact__c = contact.id;
        quote.zqu__SoldToContact__c = contact.id;
        quoteList.add(quote);
        //insert quote;

        quote2 = Z_TestFactory.makeQuote2(opp2, account2);
        quote2.zqu__BillToContact__c = contact.id;
        quote2.zqu__SoldToContact__c = contact2.id;
        quoteList.add(quote2);
        //insert quote2;

        quote3 = Z_TestFactory.makeQuote2(opp3, account3);
        quote3.zqu__BillToContact__c = contact3.id;
        quote3.zqu__SoldToContact__c = contact3.id;
        quoteList.add(quote3);
        //insert quote3;

        quote4 = Z_TestFactory.makeQuote2(opp3, account4);
        quote4.zqu__BillToContact__c = contact4.id;
        quote4.zqu__SoldToContact__c = contact3.id;
        quoteList.add(quote4);
        //insert quote4;


        quote5 = Z_TestFactory.makeQuote2(opp4, account);
        quote5.zqu__BillToContact__c = contact5.id;
        quote5.zqu__SoldToContact__c = contact5.id;
        quoteList.add(quote5);
        insert quoteList;




        quoteAmendment = Z_TestFactory.makeQuoteAmendment(quote);
        insert quoteAmendment;

        /* quoteRatePlan = Z_TestFactory.makeQRP(quote, quoteAmendment, productRatePlan);
        insert quoteRatePlan;

        quoteAmendment2 = Z_TestFactory.makeQuoteAmendment(quote2);
        insert quoteAmendment2;

        quoteRatePlan2 = Z_TestFactory.makeQRP(quote2, quoteAmendment2, productRatePlan);
        insert quoteRatePlan2;

        quoteAmendment3 = Z_TestFactory.makeQuoteAmendment(quote3);
        insert quoteAmendment3;

        quoteRatePlan3 = Z_TestFactory.makeQRP(quote3, quoteAmendment3, productRatePlan);
        insert quoteRatePlan3;

        quoteAmendment4 = Z_TestFactory.makeQuoteAmendment(quote4);
        insert quoteAmendment4;

        quoteRatePlan4 = Z_TestFactory.makeQRP(quote4, quoteAmendment4, productRatePlan);
        insert quoteRatePlan4;*/
    }
    @isTest static void test_sendToZuora() {
        set_up_data();
        quote.Quote_Status__c = 'Accepted by Customer';
        quote.zqu__Status__c = 'New';
        Test.startTest();
        update quote;
        Test.stopTest();
    }



  @isTest static void test_product_Removed() {
        set_up_data();
        quoteRatePlan = Z_TestFactory.makeQRP(quote, quoteAmendment, productRatePlan);
        insert quoteRatePlan;
        quote.zqu__RenewalTerm__c = 12;
        quote.zqu__Previewed_Total__c = 6000;
        product.Training_Product__c = 'No';
        update product;
        Test.startTest();
        update quote;
        Test.stopTest();
    }

    @isTest static void test_product_not_Removed() {
        set_up_data();
        quoteRatePlan = Z_TestFactory.makeQRP(quote, quoteAmendment, productRatePlan);
        insert quoteRatePlan;
        quote.zqu__RenewalTerm__c = 12;
        quote.zqu__Previewed_Total__c = 6000;
        quote.zqu__Previewed_Discount__c = 10;

        Test.startTest();

        update quote;
        System.assertEquals(quote.Training_Product_Removed__c, false);
        Test.stopTest();
    }

    @isTest static void test_price_less_discounts(){
        set_up_data();
        quote.zqu__Previewed_Discount__c = 1;
        quote.zqu__Previewed_Total__c = 10;

        Test.startTest();
        update quote;
        Test.stopTest();

    }

    @isTest static void test_price_greater_discounts(){
        set_up_data();
        quoteRatePlan = Z_TestFactory.makeQRP(quote, quoteAmendment, productRatePlan);
        insert quoteRatePlan;
        quote.zqu__Previewed_Discount__c = 1;
        quote.zqu__Previewed_Total__c = 10;

        Test.startTest();
        update quote;
        System.assertEquals(false, quote.Price_Less_Than_Auto_Discounts__c);
        Test.stopTest();

    }

    @isTest static void test_QuoteApproved(){
        set_up_data();
        //Test.setMock(HttpCalloutMock.class, new zqu.HttpCalloutMockImplementation());
        quote.Quote_Status__c = 'Approved';
        Test.startTest();
            update quote;
        Test.stopTest();
        //System.assertEquals(false, quote.Price_Less_Than_Auto_Discounts__c);

    }
    @isTest static void test_QuoteOpportunity1(){
        set_up_data();

        Test.startTest();

        Opportunity oppty = Z_TestFactory.makeOpportunity(account, contact);
        oppty.Revenue_Type__c='Direct';
        insert oppty;

        List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();
        OpportunityContactRole ocr=new OpportunityContactRole(OpportunityId=oppty.id, ContactId=contact.id, role='Quote Recipient - Sold To');
        ocrList.add(ocr);
        OpportunityContactRole ocr2=new OpportunityContactRole(OpportunityId=oppty.id, ContactId=contact.id, role='Invoice Recipient - Bill To');
        ocrList.add(ocr2);
        insert ocrList;

        quote.zqu__Opportunity__c = oppty.id;
        quote.zqu__SoldToContact__c=contact.id;
        update quote;

        Test.stopTest();
    }
    /*@isTest static void test_QuoteOpportunity2(){
        set_up_data();
        Account acct =Z_TestFactory.makeAccount();
        acct.Type='Partner';
        insert acct;
        Contact partnerContact = Z_TestFactory.makeContact(acct);
        insert partnerContact;
        Opportunity oppty = Z_TestFactory.makeOpportunity(acct, contact);
        oppty.Partner_Contact__c=partnerContact.id;
        oppty.Sell_Through_Partner__c=acct.id;
        oppty.Revenue_Type__c='Reseller';
        insert oppty;
        OpportunityContactRole ocr=new OpportunityContactRole(OpportunityId=oppty.id, ContactId=contact.id, role='Quote Recipient - Sold To');
        insert ocr;
        OpportunityContactRole ocr2=new OpportunityContactRole(OpportunityId=oppty.id, ContactId=contact.id, role='Invoice Recipient - Bill To');
        insert ocr2;
        quote.zqu__Opportunity__c = oppty.id;
        quote.zqu__SoldToContact__c=contact.id;
        Test.startTest();
            update quote;
        Test.stopTest();
    }*/


    @isTest static void test_QuoteDifferentBilltoSoldtosameAccount(){
        set_up_data();

        quote2.Quote_Status__c = 'Approved';
            update quote2;

    }

    @isTest static void test_AccClearedQuote(){
        set_up_data();

        quote3.Quote_Status__c = 'Approved';
            update quote3;

    }

    @isTest static void test_BillToSoldToAccClearedQuote(){
        set_up_data();

        quote4.Quote_Status__c = 'Approved';
            update quote4;

    }

    @isTest
    static void test_selectPCS_Distributor() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'The mortiest morty', '','65000-480');
        insert endUserAccount;

        Subsidiary__c subid = TestQuoteUtil.createSubsidiary();
      //QlikTech_Company__c QTCompId=TestQuoteUtil.createQlikTech('France', 'FR', subid.id);
      //Account acc = TestQuoteUtil.createAccount(QTCompId.Id);

        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;



        Account partner = Z_TestFactory.makePartnerAccount('United States', 'The grand symphony', '','65000-480');
        insert partner;
        partner.Pending_Validation__c =FALSE;
        update partner;

        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Homer Simpsons pet giraffe', '','65000-480');
        insert secondPartner;
        secondPartner.Pending_Validation__c =FALSE;
        update secondPartner;

        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Distributor', 'Distributor', 'Distributor');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Distributor';
        insert testOpp;

        Test.startTest();
        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        insert testQuote;
        Test.stopTest();

        testQuote = [SELECT Id, Partner_Category_Status_STP__c, Partner_Category_Status_Second__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];
        System.assertEquals(pcs.Id, testQuote.Partner_Category_Status_STP__c);
        System.assertEquals(pcs2.Id, testQuote.Partner_Category_Status_Second__c);
    }

    @isTest
    static void test_selectPCS_Distributor_with_Master_Reseller_Second_Partner() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'We have a unique name now', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;

        Account partner = Z_TestFactory.makePartnerAccount('United States', 'I have the unique name', '','65000-480');
        insert partner;
        partner.Pending_Validation__c =FALSE;
        update partner;

        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Nu uh!', '','65000-480');
        insert secondPartner;
        secondPartner.Pending_Validation__c =FALSE;
        update secondPartner;

        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Distributor', 'Distributor', 'Distributor');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'Master Reseller', 'Elite', 'Master Reseller');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);



        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Distributor';



        insert testOpp;

        Test.startTest();
        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        insert testQuote;
        Test.stopTest();

        testQuote = [SELECT Id, Partner_Category_Status_STP__c, Partner_Category_Status_Second__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];
        System.assertEquals(pcs.Id, testQuote.Partner_Category_Status_STP__c);
        System.assertEquals(pcs2.Id, testQuote.Partner_Category_Status_Second__c);
    }

    @isTest
    static void test_selectPCS_Reseller() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Uniqueness level over 9000', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;

        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Over 9000? Theres no way that can be right', '','65000-480');
        insert partner;
        partner.Pending_Validation__c =FALSE;
        update partner;

        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Need more uniqueness!', '','65000-480');
        insert secondPartner;
        
        secondPartner.Pending_Validation__c =FALSE;
        update secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        Test.startTest();
        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        insert testQuote;
        Test.stopTest();

        testQuote = [SELECT Id, Partner_Category_Status_STP__c, Partner_Category_Status_Second__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];
        System.assertEquals(pcs.Id, testQuote.Partner_Category_Status_STP__c);
        System.assertEquals(pcs2.Id, testQuote.Partner_Category_Status_Second__c);
    }

    @isTest
    static void test_selectPCS_Reseller_with_Master_Reseller_Partners() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Account names are not important', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;

        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Only life important', '','65000-480');
        insert partner;
        partner.Pending_Validation__c =FALSE;
        update partner;

        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Lilo Dallas Multipass', '','65000-480');
        insert secondPartner;
        secondPartner.Pending_Validation__c =FALSE;
        update secondPartner;

        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Master Reseller', 'Elite', 'Master Reseller');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'Master Reseller', 'Elite', 'Master Reseller');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        Test.startTest();
        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        insert testQuote;
        Test.stopTest();

        testQuote = [SELECT Id, Partner_Category_Status_STP__c, Partner_Category_Status_Second__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];
        System.assertEquals(pcs.Id, testQuote.Partner_Category_Status_STP__c);
        System.assertEquals(pcs2.Id, testQuote.Partner_Category_Status_Second__c);
    }

    @isTest static void test_driveZuoraQuoteRecordTypes_Pending() {
        Test.startTest();
        set_up_data();
        quote2.ApprovalStatus__c = 'Pending';
        update quote2;
        Test.stopTest();

        quote2 = [SELECT Id, Quote_Status__c, RecordType.DeveloperName FROM zqu__Quote__c WHERE Id = :quote2.Id];
        System.assertEquals('Approval Requested', quote2.Quote_Status__c);
        System.assertEquals('Waiting_For_Approval', quote2.RecordType.DeveloperName);
    }

    // 07-12-2018 - Commenting this test method as SeeAllData=true is causing managed package (ecustoms) webservice.
    // @isTest static void test_driveZuoraQuoteRecordTypes_Approved() {
    //     Test.startTest();
    //     set_up_data();
    //     quote2.ApprovalStatus__c = 'Approved';
    //     update quote2;
    //     Test.stopTest();
    //
    //     quote2 = [SELECT Id, Quote_Status__c, RecordType.DeveloperName FROM zqu__Quote__c WHERE Id = :quote2.Id];
    //     System.assertEquals('Approved', quote2.Quote_Status__c);
    //     System.assertEquals('Approved', quote2.RecordType.DeveloperName);
    // }

    @isTest static void test_driveZuoraQuoteRecordTypes_Rejected() {
        Test.startTest();
        set_up_data();
        quote2.ApprovalStatus__c = 'Rejected';
        update quote2;
        Test.stopTest();

        quote2 = [SELECT Id, Quote_Status__c, RecordType.DeveloperName FROM zqu__Quote__c WHERE Id = :quote2.Id];
        System.assertEquals('Open', quote2.Quote_Status__c);
        System.assertEquals('Default', quote2.RecordType.DeveloperName);
    }

    @isTest
    static void test_validateInvoiceOwner_nobillingaccount() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Account names are not important', '','65000-480');
        insert endUserAccount;

        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Over 9000? Theres no way that can be right', '','65000-480');
        insert partner;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Master Reseller', 'Elite', 'Master Reseller');
        insert pcs;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        testQuote.Quote_Status__c = 'Open';
        testQuote.zqu__InvoiceOwnerId__c = null;
        testQuote.zqu__InvoiceOwnerName__c = null;
        insert testQuote;

        Test.startTest();

        testQuote.Quote_Status__c = 'Approved';
        Semaphores.Z_QuoteTriggerBeforeUpdate = false;
        update testQuote;

        testQuote = [SELECT Id, Quote_Status__c, Revenue_Type__c, zqu__InvoiceOwnerId__c, zqu__Status__c, Sell_Through_Partner_Id__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];

        System.assertNotEquals(null, testQuote.Sell_Through_Partner_Id__c);
        System.assertEquals(null, testQuote.zqu__InvoiceOwnerId__c);
        System.assertNotEquals('Sent to Z-Billing', testQuote.zqu__Status__c);

        Boolean exThrown = false;
        try {
            testQuote.Quote_Status__c = 'Order Placed';
            Semaphores.Z_QuoteTriggerBeforeUpdate = false;
            update testQuote;
        } catch (Exception ex) {
            exThrown = true;
        }

        testQuote = [SELECT Id, Quote_Status__c, Revenue_Type__c, zqu__InvoiceOwnerId__c, zqu__Status__c, Sell_Through_Partner_Id__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];
        System.assertNotEquals('Order Placed', testQuote.Quote_Status__c);
        System.AssertEquals(true, exThrown);

        Test.stopTest();
    }

    @isTest
    static void test_validateInvoiceOwner_withbillingaccount() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Account names are not important', '','65000-480');
        insert endUserAccount;

        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Over 9000? Theres no way that can be right', '','65000-480');
        insert partner;

        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        testOpp.Included_Products__c = 'Qlik Sense';
        insert testOpp;

        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        testQuote.Quote_Status__c = 'Open';
        testQuote.zqu__InvoiceOwnerId__c = null;
        testQuote.zqu__InvoiceOwnerName__c = null;
        insert testQuote;

        Test.startTest();

        List<Zuora__CustomerAccount__c> caList = new List<Zuora__CustomerAccount__c>();
        customerAccount = Z_TestFactory.createBillingAccount(partner, 'id0123456789KK');
        caList.add(customerAccount);
        customerAccount2 = Z_TestFactory.createBillingAccount(partner, 'id0123456789UIN');
        caList.add(customerAccount2);
        insert caList;

        testQuote = [SELECT Id, Quote_Status__c, Revenue_Type__c, zqu__InvoiceOwnerId__c, zqu__Status__c, Sell_Through_Partner_Id__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];

        System.assertNotEquals('Sent to Z-Billing', testQuote.zqu__Status__c);
        System.assertNotEquals(null, testQuote.Sell_Through_Partner_Id__c);
        System.assertEquals(null, testQuote.zqu__InvoiceOwnerId__c);

        testQuote.Quote_Status__c = 'Order Placed';
        Semaphores.Z_QuoteTriggerBeforeUpdate = false;
        update testQuote;

        testQuote = [SELECT Id, Quote_Status__c, Revenue_Type__c, zqu__InvoiceOwnerId__c, zqu__Status__c, Sell_Through_Partner_Id__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];

        System.assertEquals('Order Placed', testQuote.Quote_Status__c);
        System.assertNotEquals(null, testQuote.zqu__InvoiceOwnerId__c);
        Test.stopTest();
    }

    //17.1.2020 BSL-3271: DOB
    @isTest
    static void test_delete_quote() {
        Test.startTest();
        set_up_data();
        quote2.zqu__Primary__c = true;
        update quote2;  
        delete quote2;
        Test.stopTest();
    }
}