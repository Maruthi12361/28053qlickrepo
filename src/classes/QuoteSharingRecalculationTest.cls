/****************************************************************
*
*  QuoteSharingRecalculationTest
*
*  2017-02-02 RVA: Q2CW, test class for QuoteSharingRecalculation, handler for setting sharing rule on a quote.
*   06.02.2017  RVA :   changing CreateAcounts methods
*   27.03.2017 Rodion Vakulovskyi
*   02.09.2017 : Srinivasan PR- fix for query error
*   12-09-2017 Linus Löfberg Q2CW-2953 setting quote recipient to conform with added validation conditions.
*****************************************************************/
@isTest
private class QuoteSharingRecalculationTest {
  public static final String OEMENDUSER = 'OEM_End_User_Account';
  static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    @isTest
  public static void processTest() {
    QuoteTestHelper.createCustomSettings();
        User testUser = [Select id, UserRoleId  From User where id =: UserInfo.getUserId()];

        System.runAs(testUser) {

        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];

        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and sobjecttype='Account']; // fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Account testPartnerAccount2 = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount2;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Contact testContact2 = QuoteTestHelper.createContact(testPartnerAccount2.id);
            insert testContact2;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        RecordType rType2 = [Select id, DeveloperName From RecordType where DeveloperName =: OEMENDUSER];
/*
         UserRole ur = new UserRole(Name = 'Executive');
         insert ur;
        */
         Test.startTest();
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           quoteForTest.OwnerId = UserInfo.getUserId();
           quoteForTest.SBQQ__Primary__c = true;
           insert quoteForTest;
        testAccount.RecordTypeId = rType2.id;
        update testAccount;
        SBQQ__Quote__c quoteForTest2 = QuoteTestHelper.createQuote(rType, testContact2.id, testAccount, testPartnerAccount2.id, 'Reseller', 'Open', 'Quote', false, '');
           quoteForTest2.OwnerId = UserInfo.getUserId();
           quoteForTest2.Quote_Recipient__c = testContact.Id;
        insert quoteForTest2;


            update quoteForTest;
       //  SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
      //  insert testQuoteLine;

    Id batchJobId = Database.executeBatch(new QuoteSharingRecalculation(), 200);
        Test.stopTest();
        List<SBQQ__Quote__Share> listOfSharing = [Select id from SBQQ__Quote__Share];
        System.assertEquals(false, listOfSharing.size() < 0);
    }
  }

  @isTest
  public static void processTest2() {
    QuoteTestHelper.createCustomSettings();
        User testUser = [Select id, UserRoleId  From User where id =: UserInfo.getUserId()];

        System.runAs(testUser) {

        Account testAccount = QTTestUtils.createMockAccount('Exception', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAccount.Name = 'Exception';
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];

        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
                               where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Account testPartnerAccount2 = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount2;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Contact testContact2 = QuoteTestHelper.createContact(testPartnerAccount2.id);
            insert testContact2;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        RecordType rType2 = [Select id, DeveloperName From RecordType where DeveloperName =: OEMENDUSER];
/*
         UserRole ur = new UserRole(Name = 'Executive');
         insert ur;
*/
         Test.startTest();
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           quoteForTest.OwnerId = UserInfo.getUserId();
           quoteForTest.SBQQ__Primary__c = true;
           insert quoteForTest;
        testAccount.RecordTypeId = rType2.id;
        update testAccount;
        SBQQ__Quote__c quoteForTest2 = QuoteTestHelper.createQuote(rType, testContact2.id, testAccount, testPartnerAccount2.id, 'Reseller', 'Open', 'Quote', false, '');
           quoteForTest2.OwnerId = UserInfo.getUserId();
           quoteForTest2.Quote_Recipient__c = testContact.Id;
        insert quoteForTest2;


            update quoteForTest;
         SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
       // insert testQuoteLine;

    Id batchJobId = Database.executeBatch(new QuoteSharingRecalculation(), 200);
        Test.stopTest();
        List<SBQQ__Quote__Share> listOfSharing = [Select id from SBQQ__Quote__Share];
        System.assertEquals(false, listOfSharing.size() < 0);
    }
  }

  private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                            Country_Code_Two_Letter__c = countryAbbr,
                            Country_Name__c = countryName,
                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
}