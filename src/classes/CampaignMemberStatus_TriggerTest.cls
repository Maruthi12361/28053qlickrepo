/***************************************
Class: CampaignMemberStatus_TriggerTest
TestClass for: CampaignMemberStatus_Trigger

Changes Log:
2015-08-13  Linus Löfberg @ 4front      File created.
06.02.2017  RVA :   changing CreateAcounts methods
2018-05-30 CCE CHG0033789 BMW-751 Add new Source URL field with larger character limit
****************************************/

@isTest
private class CampaignMemberStatus_TriggerTest {

    @isTest
    public static void test_CampaignMemberStatusTrigger()
    {
        String Pattern = 'xxDoNotUse MKTOTrigger';
        Campaign newCamp = new Campaign();
        newCamp.Name = 'RDZTestCampaign';
        newCamp.Planned_Opportunity_Value__c = 10000;
        insert(newCamp);
		/*
        Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            Country_Name__c = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = testSubs.id
        );
        insert QTComp;
		*/
		 QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
		insert setting;

        Account newAccount = new Account();
        newAccount.Name='TestAccount';
        newAccount.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
        newAccount.Billing_Country_Code__c = QTComp.Id;
        insert(newAccount);

        Contact newContact = new Contact();
        newContact.AccountId = newAccount.Id;
        newContact.FirstName = 'RDZTest';
        newContact.LastName = 'RDZTest';
        newContact.Source_ID2__c = 'ID2';
        newcontact.Original_Source_ID2__c = 'Original ID2';
        insert(newContact);

        Lead newLead = new Lead();
        newLead.FirstName = 'RDZTestLead';
        newLead.LastName = 'RDZTestLead';
        newLead.Country = 'USA';
        newLead.Email = 'test@test.com';
        newLead.Company = 'TestCompany';
        newLead.Original_Source_ID2__c = 'Original ID2';
        newLead.Source_ID2__c = 'ID2';
        insert(newLead);

        for (Contact c :[Select Id, Source_ID2__c, Email, FirstName from Contact where Original_Source_ID2__c = 'Original ID2' and FirstName = 'RDZTest'])
        {
            System.Debug('!!!!!!!!!!!!!!!!!');
            System.Debug('After insert Contact:');

            System.Debug('ID:'+ c.Id);
            System.Debug('Source_ID2__c:'+ c.Source_ID2__c );
            System.Debug('!!!!!!!!!!!!!!!!!');
        }

        for (Lead c :[Select Id, Source_ID2__c, Email, FirstName from Lead where Email = 'test@test.com' and FirstName = 'RDZTestLead'])
        {
            System.Debug('!!!!!!!!!!!!!!!!!');
            System.Debug('After insert Contact:');

            System.Debug('ID:'+ c.Id);
            System.Debug('Source_ID2__c:'+ c.Source_ID2__c );
            System.Debug('!!!!!!!!!!!!!!!!!');
        }

        CampaignMember cmContact = new CampaignMember();
        cmContact.ContactId = newContact.Id;

        CampaignMember cmLead = new CampaignMember();
        cmLead.LeadId = newLead.Id;
        cmLead.CampaignId = newCamp.Id;
        cmContact.CampaignId = newCamp.Id;
        //Test on insert if the pattern disapear from the status
        cmLead.Status = Pattern +' Responded';
        cmContact.Status = Pattern + ' Sent';
        Test.startTest();
        insert(cmContact);
        insert(cmLead);

        for (Contact c :[Select Id, Source_ID2__c, Email, FirstName from Contact where Original_Source_ID2__c = 'Original ID2' and FirstName = 'RDZTest'])
        {
            System.Debug('!!!!!!!!!!!!!!!!!');
            System.Debug('After insert Contact:');

            System.Debug('ID:'+ c.Id);
            System.Debug('Source_ID2__c:'+ c.Source_ID2__c );
            System.Debug('!!!!!!!!!!!!!!!!!');
        }

        for (Lead c :[Select Id, Source_ID2__c, Email, FirstName from Lead where Email = 'test@test.com' and FirstName = 'RDZTestLead'])
        {
            System.Debug('!!!!!!!!!!!!!!!!!');
            System.Debug('After insert Contact:');

            System.Debug('ID:'+ c.Id);
            System.Debug('Source_ID2__c:'+ c.Source_ID2__c );
            System.Debug('!!!!!!!!!!!!!!!!!');
        }

        //Check cmLead and cmContact Status change to delete the Pattern on Insert

        for (CampaignMember cm : [Select Status, CM_Source_ID2__c, Lead.Source_ID2__c, Contact.Source_ID2__c from CampaignMember where Id in (:cmContact.Id, :cmLead.id)])
        {
            System.assertEquals(false , cm.Status.contains(Pattern));
            //The value should be ID2 and the lead value should become blank
            if (cm.CM_Source_ID2__c != null)
            {
                System.assertEquals(true, cm.CM_Source_ID2__c.contains('ID2'));
            }
        }

        for (CampaignMember cm :  [Select Status, CM_Source_ID2__c,Contact.Original_Source_ID2__c,Lead.Original_Source_ID2__c, Lead.Source_ID2__c, Contact.Source_ID2__c from CampaignMember where Id in (:cmContact.Id, :cmLead.id)])
        {
            System.assertEquals(false , cm.Status.contains(Pattern));

            if (cm.LeadId != null && cm.Lead.Source_ID2__c == null)
            {
                System.assertEquals(true, cm.Lead.Original_Source_ID2__c.contains('ID2'));
                System.assert(true, 'We expect Lead Original_Source_ID2__c to be cleaned up');
            }
            else if (cm.ContactId != null && (cm.Contact.Source_ID2__c == null || cm.Contact.Source_ID2__c == ''))
            {
                System.assertEquals(true, cm.Contact.Original_Source_ID2__c.contains('ID2'));
                System.assert(true, 'Contact Source_ID2 has been cleaned up');
            }
            else
            {
                if (cm.ContactId != null)
                {
                    System.Debug('-------------Contact-----------------: ');
                    System.Debug('ContactId: ' + cm.ContactId);
                    System.Debug('Contact.Original_Source_ID2__c: ' + cm.Contact.Original_Source_ID2__c);
                    System.Debug('cm.Contact.Source_ID2__c: ' + cm.Contact.Source_ID2__c);
                }
                if (cm.LeadId != null)
                {
                    System.Debug('-------------Lead-----------------: ');
                    System.Debug('Lead: ' + cm.ContactId);
                    System.Debug('Contact.Original_Source_ID2__c: ' + cm.Contact.Original_Source_ID2__c);
                    System.Debug('cm.Contact.Source_ID2__c: ' + cm.Contact.Source_ID2__c);
                }
                System.assert(false, 'We expect that Source_ID2__c to be blank and its null');
            }
            if (cm.CM_Source_ID2__c != null)
            {
                System.assertEquals(true, cm.CM_Source_ID2__c.contains('ID2'));

            }
            else
            {
                System.Debug('----------Campaign-----------');
                System.Debug('cm.CM_Source_ID2__c: ' + cm.CM_Source_ID2__c);
                System.assert(false, 'We expect that CM_Source_ID2__c contains ID2 and its: (' + cm.CM_Source_ID2__c + ')');
            }
        }

        //If the form its resubmitted and leads and contacts at this stage has blank values in the fields that we need to populate
        //Setup new values as Marketo form will update them on the Lead and Contact.
        newContact.Source_ID2__c = 'ID2-2Update';
        update(newContact);
        newLead.Source_ID2__c = 'ID2-2Update';
        update(newLead);

        //Try to resummit the Marketo form

        cmLead.Status = Pattern + ' Responded';
        cmContact.Status = Pattern + ' Sent';

        update(cmContact);
        update(cmLead);


        for (CampaignMember cm :  [Select Status, CM_Source_ID2__c,Contact.Original_Source_ID2__c,Lead.Original_Source_ID2__c, Lead.Source_ID2__c, Contact.Source_ID2__c from CampaignMember where Id in (:cmContact.Id, :cmLead.id)])
        {
            System.assertEquals(false , cm.Status.contains(Pattern));

            if (cm.LeadId != null && cm.Lead.Source_ID2__c == null)
            {
                System.assertEquals(true, cm.Lead.Original_Source_ID2__c.contains('Original'));
                System.assert(true, 'We expect Lead Original_Source_ID2__c to be cleaned up');
            }
            else if (cm.ContactId != null && (cm.Contact.Source_ID2__c == null || cm.Contact.Source_ID2__c == ''))
            {
                System.assertEquals(true, cm.Contact.Original_Source_ID2__c.contains('Original'));
                System.assert(true, 'Contact Source_ID2 has been cleaned up');
            }
            else
            {
                if (cm.ContactId != null)
                {
                    System.Debug('-------------Contact-----------------: ');
                    System.Debug('ContactId: ' + cm.ContactId);
                    System.Debug('Contact.Original_Source_ID2__c: ' + cm.Contact.Original_Source_ID2__c);
                    System.Debug('cm.Contact.Source_ID2__c: ' + cm.Contact.Source_ID2__c);
                }
                if (cm.LeadId != null)
                {
                    System.Debug('-------------Lead-----------------: ');
                    System.Debug('Lead: ' + cm.LeadId);
                    System.Debug('Contact.Original_Source_ID2__c: ' + cm.Contact.Original_Source_ID2__c);
                    System.Debug('cm.Contact.Source_ID2__c: ' + cm.Contact.Source_ID2__c);
                }
                System.assert(false, 'We expect that Source_ID2__c to be blank and its null');
            }
            if (cm.CM_Source_ID2__c != null)
            {
                System.assertEquals(true, cm.CM_Source_ID2__c.contains('ID2-2Update'));

            }
            else
            {
                System.Debug('----------Campaign-----------');
                System.Debug('cm.CM_Source_ID2__c: ' + cm.CM_Source_ID2__c);
                System.assert(false, 'We expect that CM_Source_ID2__c contains ID2 and its: (' + cm.CM_Source_ID2__c + ')');
            }
        }
		Test.stopTest();
    }

    @isTest
    public static void test_CampaignMemberStatusTriggerCMFields()
    {

		//Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        Campaign camp = new Campaign(); // Create a sample campaign
        camp.Name = 'Test Campaign';
        camp.Publishable_Name__c = 'Test Campaign';
        camp.IsActive = true;
        camp.Type = 'DM - Direct Marketing';
        camp.Campaign_Sub_Type__c = 'DM - Email Campaign';
        camp.LOB__c = 'Field Marketing';
        camp.Campaign_Objective__c = 'Brand Awareness';
        //camp.Campaign_Audience__c = 'Prospects';
        camp.QlikTech_Company__c = 'QlikTech Inc';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today() + 30;
        camp.Planned_Target_Audience__c = 100;
        camp.Planned_Leads__c = 100;
        camp.Planned_Opportunities__c = 10;
        camp.Planned_Opportunity_Value__c = 100000;
        camp.Campaign_Sector__c = 'Cross Sector';
        camp.Campaign_Job_Function__c = 'Executive';
        camp.Data_Source_Category__c = 'Microsoft';
        camp.Partner_Involvement__c = 'No Partner';
        insert camp;
        system.assert(camp.Id != null);

        //QlikTech_Company__c QTComp = new QlikTech_Company__c(Name = 'USA', Country_Name__c = 'USA', QlikTech_Company_Name__c = 'QlikTech Inc', Subsidiary__c = testSubs.id);
       // insert QTComp;
	    QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Lead ld = new Lead();
        ld.Firstname = 'Test';
        ld.Lastname = 'Lead';
        ld.Company = 'Test Company';
        ld.LeadSource = 'Qlikmarket';
        ld.Country = 'USA';
        ld.Email = 'test@gmail.com';
        ld.Status = 'TEST';
        insert ld;
        system.assert(ld.Id != null);
        
        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
		insert setting;

        Account newAccount = new Account(Name='Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert(newAccount);
        system.assert(newAccount.Id != null);

        Contact ct = new Contact(AccountId = newAccount.Id, FirstName = 'Test', LastName = 'Contact', Contact_Status__c = 'TEST');
        insert(ct);
        system.assert(ct.Id != null);

        Test.startTest();

        CampaignMember cmLead = new CampaignMember(); // Add a lead campaign member
        cmLead.CampaignId = camp.Id;
        cmLead.Status = 'Responded';
        cmLead.LeadId = ld.Id;
        cmLead.CM_Source_Partner__c = 'test';
        cmLead.CM_Source_ID2__c = 'test';
        cmLead.CM_Source_Site__c = 'test';
        cmLead.CM_Keywords__c = 'test';
        cmLead.CM_Source_URL_NEW__c = 'test';
        insert cmLead;
        system.assert(cmLead.Id != null);
        ld = [SELECT Id, Status, Inquiry_Start_Date__c FROM Lead WHERE Id = :ld.Id];
        system.assert(ld.Inquiry_Start_Date__c != null);

        CampaignMember cmCt = new CampaignMember(); // Add a contact campaign member
        cmCt.CampaignId = camp.Id;
        cmCt.Status = 'Responded';
        cmCt.ContactId = ct.Id;
        cmCt.CM_Source_Partner__c = 'test';
        cmCt.CM_Source_ID2__c = 'test';
        cmCt.CM_Source_Site__c = 'test';
        cmCt.CM_Keywords__c = 'test';
        cmCt.CM_Source_URL_NEW__c = 'test';
        insert cmCt;
        system.assert(cmCt.Id != null);

        cmLead.CM_Source_Partner__c = 'changedvalue';
        cmLead.CM_Source_ID2__c = 'changedvalue';
        cmLead.CM_Source_Site__c = 'changedvalue';
        cmLead.CM_Keywords__c = 'changedvalue';
        cmLead.CM_Source_URL_NEW__c = 'changedvalue';

        cmCt.CM_Source_Partner__c = 'changedvalue';
        cmCt.CM_Source_ID2__c = 'changedvalue';
        cmCt.CM_Source_Site__c = 'changedvalue';
        cmCt.CM_Keywords__c = 'changedvalue';
        cmCt.CM_Source_URL_NEW__c = 'changedvalue';

        update cmLead;
        update cmCt;

        cmLead = [SELECT Id, CM_Source_Partner__c, CM_Source_Site__c, CM_Source_ID2__c, CM_Keywords__c, CM_Source_URL_NEW__c FROM CampaignMember WHERE Id = :cmLead.Id];
        System.assertEquals(cmLead.CM_Source_Partner__c, 'test');
        System.assertEquals(cmLead.CM_Source_Site__c, 'test');
        System.assertEquals(cmLead.CM_Source_ID2__c, 'test');
        System.assertEquals(cmLead.CM_Keywords__c, 'test');
        System.assertEquals(cmLead.CM_Source_URL_NEW__c, 'test');

        cmCt = [SELECT Id, CM_Source_Partner__c, CM_Source_Site__c, CM_Source_ID2__c, CM_Keywords__c, CM_Source_URL_NEW__c FROM CampaignMember WHERE Id = :cmCt.Id];
        System.assertEquals(cmCt.CM_Source_Partner__c, 'test');
        System.assertEquals(cmCt.CM_Source_Site__c, 'test');
        System.assertEquals(cmCt.CM_Source_ID2__c, 'test');
        System.assertEquals(cmCt.CM_Keywords__c, 'test');
        System.assertEquals(cmCt.CM_Source_URL_NEW__c, 'test');

        Test.stopTest();
    }

    @isTest
    public static void test_checkAndCleanValues() {

        Test.startTest();

        //Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        Campaign camp = new Campaign(); // Create a sample campaign
        camp.Name = 'Test Campaign';
        camp.Publishable_Name__c = 'Test Campaign';
        camp.IsActive = true;
        camp.Type = 'DM - Direct Marketing';
        camp.Campaign_Sub_Type__c = 'DM - Email Campaign';
        camp.LOB__c = 'Field Marketing';
        camp.Campaign_Objective__c = 'Brand Awareness';
        //camp.Campaign_Audience__c = 'Prospects';
        camp.QlikTech_Company__c = 'QlikTech Inc';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today() + 30;
        camp.Planned_Target_Audience__c = 100;
        camp.Planned_Leads__c = 100;
        camp.Planned_Opportunities__c = 10;
        camp.Planned_Opportunity_Value__c = 100000;
        camp.Campaign_Sector__c = 'Cross Sector';
        camp.Campaign_Job_Function__c = 'Executive';
        camp.Data_Source_Category__c = 'Microsoft';
        camp.Partner_Involvement__c = 'No Partner';
        insert camp;
        system.assert(camp.Id != null);

        //QlikTech_Company__c QTComp = new QlikTech_Company__c(Name = 'USA', Country_Name__c = 'USA', QlikTech_Company_Name__c = 'QlikTech Inc', Subsidiary__c = testSubs.id);
       // insert QTComp;
        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert setting;
        
        Lead ld = new Lead();
        ld.Firstname = 'Test';
        ld.Lastname = 'Lead';
        ld.Company = 'Test Company';
        ld.LeadSource = 'Qlikmarket';
        ld.Country = 'USA';
        ld.Email = 'test@gmail.com';
        ld.Status = 'TEST';
        insert ld;
        system.assert(ld.Id != null);

        Account newAccount = new Account(Name='Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert(newAccount);
        system.assert(newAccount.Id != null);

        Contact ct = new Contact(AccountId = newAccount.Id, FirstName = 'Test', LastName = 'Contact', Contact_Status__c = 'TEST');
        insert(ct);
        system.assert(ct.Id != null);

        CampaignMember cmLead = new CampaignMember(); // Add a lead campaign member
        cmLead.CampaignId = camp.Id;
        cmLead.Status = 'Responded';
        cmLead.LeadId = ld.Id;
        cmLead.CM_Source_Partner__c = 'test';
        cmLead.CM_Source_ID2__c = 'test';
        cmLead.CM_Source_Site__c = 'test';
        cmLead.CM_Keywords__c = 'test';
        cmLead.CM_Source_URL_NEW__c = 'test';
        insert cmLead;
        system.assert(cmLead.Id != null);
        ld = [SELECT Id, Status, Inquiry_Start_Date__c FROM Lead WHERE Id = :ld.Id];
        system.assert(ld.Inquiry_Start_Date__c != null);

        CampaignMember cmCt = new CampaignMember(); // Add a contact campaign member
        cmCt.CampaignId = camp.Id;
        cmCt.Status = 'Responded';
        cmCt.ContactId = ct.Id;
        cmCt.CM_Source_Partner__c = 'test';
        cmCt.CM_Source_ID2__c = 'test';
        cmCt.CM_Source_Site__c = 'test';
        cmCt.CM_Keywords__c = 'test';
        cmCt.CM_Source_URL_NEW__c = 'test';
        insert cmCt;
        system.assert(cmCt.Id != null);

        List<CampaignMember> cmList = new List<CampaignMember>();
        cmList.add(cmLead);
        cmList.add(cmCt);

        CampaignMemberHelper.checkAndCleanValues(cmList);

        Test.stopTest();
    }

    @isTest
    public static void test_PopulateConversicaEventFieldsOnCreate() {

        Campaign camp = new Campaign(); // Create a sample campaign
        camp.Name = 'Test Campaign Conversica1';
        camp.Publishable_Name__c = 'Test Campaign Conversica1';
        camp.IsActive = true;
        camp.Type = 'EV - Event';
        camp.Campaign_Sub_Type__c = 'EV - Industry Trade Show';
        camp.LOB__c = 'Field Marketing';
        camp.Campaign_Objective__c = 'Brand Awareness';
        //camp.Campaign_Audience__c = 'Prospects';
        camp.QlikTech_Company__c = 'QlikTech Inc';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today() + 30;
        camp.Planned_Target_Audience__c = 100;
        camp.Planned_Leads__c = 100;
        camp.Planned_Opportunities__c = 10;
        camp.Planned_Opportunity_Value__c = 100000;
        camp.Campaign_Sector__c = 'Cross Sector';
        camp.Campaign_Job_Function__c = 'Executive';
        camp.Data_Source_Category__c = 'Microsoft';
        camp.Partner_Involvement__c = 'No Partner';
        insert camp;
        system.assert(camp.Id != null);

        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert setting;
        
        Lead ld = new Lead();
        ld.Firstname = 'Test';
        ld.Lastname = 'Lead1';
        ld.Company = 'Test Company';
        ld.LeadSource = 'Qlikmarket';
        ld.Country = 'USA';
        ld.Email = 'test@gmail.com';
        ld.Status = 'TEST';
        insert ld;
        system.assert(ld.Id != null);

        Account newAccount = new Account(Name='Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert(newAccount);
        system.assert(newAccount.Id != null);

        Contact ct = new Contact(AccountId = newAccount.Id, FirstName = 'Test', LastName = 'Contact1', Contact_Status__c = 'TEST');
        insert(ct);
        system.assert(ct.Id != null);

        CampaignMemberStatus cms = new CampaignMemberStatus(CampaignId = camp.Id, HasResponded = true, Label = 'Attended');
        insert(cms);
        system.assert(cms.Id != null);

        Test.startTest();

        CampaignMember cmLead = new CampaignMember(); // Add a lead campaign member
        cmLead.CampaignId = camp.Id;
        cmLead.Status = 'Attended';
        cmLead.LeadId = ld.Id;
        cmLead.CM_Source_Partner__c = 'test';
        cmLead.CM_Source_ID2__c = 'test';
        cmLead.CM_Source_Site__c = 'test';
        cmLead.CM_Keywords__c = 'test';
        cmLead.CM_Source_URL_NEW__c = 'test';
        insert cmLead;
        system.assert(cmLead.Id != null);

        CampaignMember cmCt = new CampaignMember(); // Add a contact campaign member
        cmCt.CampaignId = camp.Id;
        cmCt.Status = 'Attended';
        cmCt.ContactId = ct.Id;
        cmCt.CM_Source_Partner__c = 'test';
        cmCt.CM_Source_ID2__c = 'test';
        cmCt.CM_Source_Site__c = 'test';
        cmCt.CM_Keywords__c = 'test';
        cmCt.CM_Source_URL_NEW__c = 'test';
        insert cmCt;
        system.assert(cmCt.Id != null);
        
        Lead ldret = [SELECT Id, Latest_Event_Campaign_Member_Status__c FROM Lead WHERE Id = :ld.Id];        
        System.assertEquals('Attended', ldret.Latest_Event_Campaign_Member_Status__c);
        
        Contact ctret = [SELECT Id, Latest_Event_Campaign_Member_Status__c FROM Contact WHERE Id = :ct.Id];        
        System.assertEquals('Attended', ctret.Latest_Event_Campaign_Member_Status__c);

        Test.stopTest();
    }

    @isTest
    public static void test_PopulateConversicaEventFieldsOnUpdate() {

        Campaign camp = new Campaign(); // Create a sample campaign
        camp.Name = 'Test Campaign Conversica2';
        camp.Publishable_Name__c = 'Test Campaign Conversica2';
        camp.IsActive = true;
        camp.Type = 'EV - Event';
        camp.Campaign_Sub_Type__c = 'EV - Industry Trade Show';
        camp.LOB__c = 'Field Marketing';
        camp.Campaign_Objective__c = 'Brand Awareness';
        //camp.Campaign_Audience__c = 'Prospects';
        camp.QlikTech_Company__c = 'QlikTech Inc';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today() + 30;
        camp.Planned_Target_Audience__c = 100;
        camp.Planned_Leads__c = 100;
        camp.Planned_Opportunities__c = 10;
        camp.Planned_Opportunity_Value__c = 100000;
        camp.Campaign_Sector__c = 'Cross Sector';
        camp.Campaign_Job_Function__c = 'Executive';
        camp.Data_Source_Category__c = 'Microsoft';
        camp.Partner_Involvement__c = 'No Partner';
        insert camp;
        system.assert(camp.Id != null);

        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert setting;
        
        Lead ld = new Lead();
        ld.Firstname = 'Test';
        ld.Lastname = 'Lead2';
        ld.Company = 'Test Company';
        ld.LeadSource = 'Qlikmarket';
        ld.Country = 'USA';
        ld.Email = 'test@gmail.com';
        ld.Status = 'TEST';
        insert ld;
        system.assert(ld.Id != null);

        Account newAccount = new Account(Name='Test Account2', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert(newAccount);
        system.assert(newAccount.Id != null);

        Contact ct = new Contact(AccountId = newAccount.Id, FirstName = 'Test', LastName = 'Contact2', Contact_Status__c = 'TEST');
        insert(ct);
        system.assert(ct.Id != null);

        CampaignMemberStatus cms = new CampaignMemberStatus(CampaignId = camp.Id, HasResponded = true, Label = 'Attended');
        insert(cms);
        system.assert(cms.Id != null);

        Test.startTest();

        CampaignMember cmLead = new CampaignMember(); // Add a lead campaign member
        cmLead.CampaignId = camp.Id;
        cmLead.Status = 'Sent';
        cmLead.LeadId = ld.Id;
        cmLead.CM_Source_Partner__c = 'test';
        cmLead.CM_Source_ID2__c = 'test';
        cmLead.CM_Source_Site__c = 'test';
        cmLead.CM_Keywords__c = 'test';
        cmLead.CM_Source_URL_NEW__c = 'test';
        insert cmLead;
        system.assert(cmLead.Id != null);

        CampaignMember cmCt = new CampaignMember(); // Add a contact campaign member
        cmCt.CampaignId = camp.Id;
        cmCt.Status = 'Sent';
        cmCt.ContactId = ct.Id;
        cmCt.CM_Source_Partner__c = 'test';
        cmCt.CM_Source_ID2__c = 'test';
        cmCt.CM_Source_Site__c = 'test';
        cmCt.CM_Keywords__c = 'test';
        cmCt.CM_Source_URL_NEW__c = 'test';
        insert cmCt;
        system.assert(cmCt.Id != null);

        cmLead.Status = 'Attended';
        update cmLead;

        cmCt.Status = 'Attended';
        update cmCt;
        
        Lead ldret = [SELECT Id, Latest_Event_Campaign_Member_Status__c FROM Lead WHERE Id = :ld.Id];        
        System.assertEquals('Attended', ldret.Latest_Event_Campaign_Member_Status__c);
        
        Contact ctret = [SELECT Id, Latest_Event_Campaign_Member_Status__c FROM Contact WHERE Id = :ct.Id];        
        System.assertEquals('Attended', ctret.Latest_Event_Campaign_Member_Status__c);

        Test.stopTest();
    }
}