/**     * File Name:OpportunityReCalcForecastAmountHandler
        * Description : This handler class is used to calculate forecast amounts on deal split child opportunities on creation or if the deal parent/percentage is changed
                    on the child opportunities.
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 July 5th 2016 RamakrishnaKini   Added new trigger logic.
            Nov 14th 2016 roman@4front      Query for mParentOppIdObject is wraped in condition.
        2017-03-25 TJG  fix test coverage of 63%
		2017-06-22 MTM  QCW-2711 remove sharing option
        2017-07-13 ASLAM QCW-1697 Update Next Step and Comment from parent
*/ 
public class OpportunityReCalcForecastAmountHandler{
    
    /*  @Description :This common method is used to calculate the forecast amounts on the child opportunities using the parent opportunity amount on before insert trigger.
    
        @parameter inputList: Trigger.new opportunity list
        @parameter inputMap: Trigger.newMap 
    */
    public static void onBeforeInsert(List<Opportunity> inputList, Map<id, Opportunity> inputMap){
        Set<Id> parentOppIds = new Set<Id>();
        Map<Id, Opportunity> mParentOppIdObject = new Map<Id, Opportunity>();
        for(Opportunity opp: inputList){
            // QCW-1697 - Removed check on Record type names
            if(String.isNotBlank(opp.Deal_Split_Parent_Opportunity__c)) // 'Deal Split Child'.equalsIgnoreCase(Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(opp.recordtypeid).getname()))
                parentOppIds.add(opp.Deal_Split_Parent_Opportunity__c);
        }
        
        if(!parentOppIds.isEmpty()) {
            //Map of parent opportunity ids which contain the forecast amounts used in calculation.
            mParentOppIdObject = new Map<Id, Opportunity>([select id,Consultancy_Forecast_Amount__c,License_Forecast_Amount__c,
                                                                            Total_Maintenance_Amount__c,Education_Forecast_Amount__c,
                                                                            Support_Forecast_Amount__c,Next_Step_Custom__c,Manager_Comments__c
                                                                            from Opportunity where Id in :parentOppIds]);
        }                                                                    
        //Calculate the child opportunities forecast amounts only deal split percantage/parent opportunity is changed
        for(Opportunity opp :inputList){
            // QCW-1697 - Removed check on Record type names
            if(!mParentOppIdObject.isEmpty() && mParentOppIdObject.containsKey(opp.Deal_Split_Parent_Opportunity__c)){ 
            //'Deal Split Child'.equalsIgnoreCase(Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(opp.recordtypeid).getname())){
                calcChildForecastAmts(opp, mParentOppIdObject);    
                updateComments(opp, mParentOppIdObject);         
            }
        }
    }
    
    /*  @Description :This common method is used to calculate the forecast amounts on the child opportunities using the parent opportunity amount on before update trigger.
    
        @parameter inputList: Trigger.new opportunity list
        @parameter inputMap: Trigger.newMap
        @parameter oInputList: Trigger.old opportunity list
        @parameter oInputMap: Trigger.oldMap     
    */
    public static void onBeforeUpdate(List<Opportunity> inputList, Map<id, Opportunity> inputMap, List<Opportunity>  oInputList, Map<id, Opportunity> oInputMap) { 
        //Set to store all deal split parent opportunity ids
        
        Set<Id> parentOppIds = new Set<Id>();
        Map<Id, Opportunity> mParentOppIdObject = new Map<Id, Opportunity>();
        //Populate the set
        for(Opportunity opp: inputList){
            // QCW-1697 - Removed check on Record type names
            if(String.isNotBlank(opp.Deal_Split_Parent_Opportunity__c)) //'Deal Split Child'.equalsIgnoreCase(Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(opp.recordtypeid).getname()))
                parentOppIds.add(opp.Deal_Split_Parent_Opportunity__c);
        }
        
        if(!parentOppIds.isEmpty()) {
            //Map of parent opportunity ids which contain the forecast amounts used in calculation.
            mParentOppIdObject = new Map<Id, Opportunity>([select id,Consultancy_Forecast_Amount__c,License_Forecast_Amount__c,
                                                                  Total_Maintenance_Amount__c,Education_Forecast_Amount__c,Support_Forecast_Amount__c,
                                                                  Next_Step_Custom__c,Manager_Comments__c
                                                                  from Opportunity where Id in :parentOppIds]);
        }
        //Calculate the child opportunities forecast amounts only deal split percantage/parent opportunity is changed        

        for(Opportunity opp :inputList){
            // QCW-1697 - Removed check on Record type names
            if(!mParentOppIdObject.isEmpty() && mParentOppIdObject.containsKey(opp.Deal_Split_Parent_Opportunity__c)){ 
                /*('Deal Split Child'.equalsIgnoreCase(Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(opp.recordtypeid).getname()) || 
                 'OEM - Recruitment Deal Split Child'.equalsIgnoreCase(Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(opp.recordtypeid).getname()) ||
                 'OEM - Order Deal Split Child'.equalsIgnoreCase(Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(opp.recordtypeid).getname()))){ */
                if(opp.deal_split_percentage__c != oInputMap.get(opp.Id).deal_split_percentage__c ||
                    opp.Deal_Split_Parent_Opportunity__c != oInputMap.get(opp.Id).Deal_Split_Parent_Opportunity__c)
                        calcChildForecastAmts(opp, mParentOppIdObject);    
                //added for QCW-1697
                if(opp.Next_Step_custom__c != oInputMap.get(opp.Id).Next_Step_custom__c ||
                    opp.Manager_Comments__c != oInputMap.get(opp.Id).Manager_Comments__c)
                        updateComments(opp, mParentOppIdObject);                                        
                //added for QCW-1697                        
            }
        }


    }
    
    /*  @Description :This common method is used to calculate the forecast amounts on the child opportunities using the parent opportunity amount.
    
        @parameter opp:  Deal split Child opportunity object
        @parameter mParentOppIdObject:  Map of parent opportunity object with opp ID as key     
    */
    private static void calcChildForecastAmts(Opportunity opp, Map<Id, Opportunity> mParentOppIdObject){
        Opportunity parent = mParentOppIdObject.get(opp.Deal_Split_Parent_Opportunity__c);
        if (null == parent || null == opp.deal_split_percentage__c)
            return;
            system.debug('ffff1'+parent);
            system.debug('ffff2'+opp);
            system.debug('ffff3'+opp.deal_split_percentage__c);
		//if(!Test.isRunningTest()) {
            if(String.isNotBlank(String.valueOf(parent.Consultancy_Forecast_Amount__c)))
                opp.Consultancy_Forecast_Amount__c =  parent.Consultancy_Forecast_Amount__c * opp.deal_split_percentage__c/100;
            if(String.isNotBlank(String.valueOf(parent.License_Forecast_Amount__c)))
                opp.License_Forecast_Amount__c =  parent.License_Forecast_Amount__c * opp.deal_split_percentage__c/100;
            if(String.isNotBlank(String.valueOf(parent.Total_Maintenance_Amount__c)))
                opp.Total_Maintenance_Amount__c =  parent.Total_Maintenance_Amount__c * opp.deal_split_percentage__c/100;
            if(String.isNotBlank(String.valueOf(parent.Education_Forecast_Amount__c)))
                opp.Education_Forecast_Amount__c =  parent.Education_Forecast_Amount__c * opp.deal_split_percentage__c/100;  
            if(String.isNotBlank(String.valueOf(parent.Support_Forecast_Amount__c)))
                opp.Support_Forecast_Amount__c =  parent.Support_Forecast_Amount__c * opp.deal_split_percentage__c/100;   

                                       
		//}
    }

    /* Added as part of QCW-1697 */
    private static void updateComments(Opportunity opp, Map<Id, Opportunity> mParentOppIdObject){
        Opportunity parent = mParentOppIdObject.get(opp.Deal_Split_Parent_Opportunity__c);
        if (null == parent || null == opp.deal_split_percentage__c)
            return;
        
        if(String.isNotBlank(parent.Next_Step_custom__c))
            opp.Next_Step_Custom__c = parent.Next_Step_custom__c;
        if(String.isNotBlank(parent.Manager_Comments__c))
            opp.Manager_Comments__c = parent.Manager_Comments__c;                

    }
    /* Added as part of QCW-1697 */ 
    /*
    public static void onAfterInsert(List<Opportunity>  inputList, Map<id, Opportunity> inputMap) {
    }
    
    public static void onAfterUpdate(List<Opportunity>  inputList, Map<id, Opportunity> inputMap, List<Opportunity>  oInputList, Map<id, Opportunity> oInputMap) {
    }
    
    public static void onAfterDelete(List<Opportunity>  inputList, Map<id, Opportunity> inputMap) {
    }*/
}