/*
@author: Anthony Victorio, Model Metrics
@date: 01/31/2012
@description: Controller for the QVM Control Panel page

20150717    CCE CR# 50225 - Fix lead export from Qlik Market Control Panel 
# 
*/
public without sharing class QVM_ControlPanelCon {
    
    private final Integer defaultLimit {get;set;}
    public QVM_Partner_Data__c partner {get;set;}
    public Boolean isProfileUpdated {get;set;}
    static final String soqlSelect = 'select Id, First_Name__c, Last_Name__c, Name__c, QVM_Product__c, QVM_Product_Name__c, Type__c, Phone__c, Email__c, CreatedDate, Lead__c, Lead_Contact__c, Company_Name__c, Responsible_Partner__c, Navision_Status_Check_Field__c, QlikTech_Company__c, Shipping_Country_Code__c from QVM_Customer_Products__c '; //CCE CR# 50225
    
    public QVM_ControlPanelCon() {
        
        QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
        
        defaultLimit = settings.Max_CP_Rows__c.intValue();
        
        try {
            partner = [select Id, Profile_Status__c, Last_Partner_Update__c from QVM_Partner_Data__c where Id = :QVM.getUserPartnerDataId()];
        } catch(Exception e) {}
        
        if(partner == null) {
            isProfileUpdated = false;
        } else {
            isProfileUpdated = (partner.Profile_Status__c == 'Draft' || partner.Profile_Status__c == '' || partner.Profile_Status__c == null) ? false : true;
        }
        
    }
    
    public PageReference editPartnerProfile() {
        PageReference p;
        p = Page.QVM_PartnerProfile;
        p.getParameters().put('Id',QVM.getUserPartnerDataId());
        return p;
    }
    
    //Customer Management
    
    public list<QVM_Customer_Products__c> getCustomerList() {
        return [select Id, Lead_Contact__c, Name__c, First_Name__c, Last_Name__c, QVM_Product__c, QVM_Product_Name__c, 
        Responsible_Partner__c, Navision_Status_Check_Field__c, QlikTech_Company__c, Shipping_Country_Code__c,
        Type__c, Email__c, Phone__c, CreatedDate, Company_Name__c
        from QVM_Customer_Products__c where QVM_Partner_Data__r.Partner_Account__c =  :QVM.getUserAccountId()
        order by CreatedDate desc limit :defaultLimit];
    }
    
    public PageReference viewAllCustomers() {
        PageReference p;
        p = Page.QVM_Customers;
        return p;
    }
    
    public PageReference exportCustomersExcel() {
        //PageReference p;
        //p = Page.QVM_CustomersCSV;
        String SoqlQuery = EncodingUtil.urlEncode(soqlSelect + 'where QVM_Partner_Data__c = \'' + QVM.getUserPartnerDataId() + '\' ', 'UTF-8'); //CCE CR# 50225
        System.debug('exportCustomersExcel: SoqlQuery=' + SoqlQuery);
        PageReference p;
        p = Page.QVM_CustomersCSV;
        p.getParameters().put('s',SoqlQuery);
        System.debug('QVM_ControlPanelCon: p = ' + p);
        return p;
    }
    
    //Product Management
    
    public list<QVM_Product_Data__c> getProductList() {
        return [select Id, Product_Name__c, Status__c, Magento_Status__c,
        Version__c, CreatedBy.Name, LastModifiedDate, Magento_Last_Updated__c 
        from QVM_Product_Data__c 
        where QVM_Partner__r.Partner_Account__c = :QVM.getUserAccountId()
        and Status__c != 'Archived'
        order by LastModifiedDate desc 
        limit :defaultLimit];
    }
    
    public PageReference viewAllProducts() {
        PageReference p;
        p = Page.QVM_Products;
        return p;
    }
    
    public PageReference viewArchivedProducts() {
        PageReference p;
        p = Page.QVM_Products;
        p.getParameters().put('showArchived','true');
        return p;
    }

    public PageReference createNewProduct() {
        PageReference next;
        next = Page.QVM_ProductBuilderStep1;
        return next;
    }
    
    //Case management

    public list<Case> getCaseList() {
        return [select Id, Subject, Status, Priority, Type, CreatedBy.Name, 
        CreatedDate from Case where ContactId = :QVM.getUserContactId() 
        order by CreatedDate desc limit :defaultLimit];
    }

    public PageReference viewAllCases() {
        PageReference p;
        p = Page.QVM_Cases;
        return p;
    }

    public PageReference createNewCase() {
        PageReference p = Page.QVM_CreateCase;
        return p;
    }
}