/****************************************************************
*
*  Test_PopUpAutoCloseAndRefresh 
*
*  06.02.2017   RVA :   changing CreateAcounts methods
*  29.03.2017 : Roman Dovbush  : duplicated createCustomSettings() commented out. We call it from QTTestUtils.GlobalSetUp();
*****************************************************************/
@isTest
public class Test_PopUpAutoCloseAndRefresh { 
	static final Id EndUserAccRecTypeId = '01220000000DOFuAAO';
    static final integer NBR_OF_ACCOUNTS = 10;

    public static Opportunity testOpp;
    public static  Account  testAcc;
    public static Contact testContact;

	public static testmethod void testMethod1() {
		QTTestUtils.GlobalSetUp();
		user testUser = [Select id From User Where id=: UserInfo.getUserId()];
		Test.startTest();
		SBQQ__Quote__c quoteForTest = prepareData1();
		 
		system.runAs(testUser){
			
			Test.setCurrentPageReference(new PageReference('Page.PlaceOrderPopupPage')); 
	        System.currentPageReference().getParameters().put('Id', quoteForTest.id);
			PopUpAutoCloseAndRefreshContr testCntr = new PopUpAutoCloseAndRefreshContr();
			testCntr.checkingWindowVisibility();
			//testCntr.updateStatus();
		}
		Test.stopTest();
	}

	public static testmethod void testMethod2() {
		QTTestUtils.GlobalSetUp();
		user testUser = [Select id From User Where id=: UserInfo.getUserId()];
		Test.startTest();
		SBQQ__Quote__c quoteForTest = prepareData2();
		 
		system.runAs(testUser){
			
			Test.setCurrentPageReference(new PageReference('Page.PlaceOrderPopupPage')); 
	        System.currentPageReference().getParameters().put('Id', quoteForTest.id);
			PopUpAutoCloseAndRefreshContr testCntr = new PopUpAutoCloseAndRefreshContr();
			testCntr.checkingWindowVisibility();
			//testCntr.updateStatus();
		}
		Test.stopTest();
	}

	private static SBQQ__Quote__c prepareData1() {
		user testUser = [Select id From User Where id=: UserInfo.getUserId()];
		//QuoteTestHelper.createCustomSettings(); // deplicate
		testAcc = QTTestUtils.createMockAccount('TestCompany', testUser, true);
		/*
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c companyQT = TestQuoteUtil.createQCompany(sub.id);
		*/
        Account testResseller = TestQuoteUtil.createReseller();
        
        //testAcc = TestQuoteUtil.createAccount(companyQT.id);
        
        testAcc.RecordType = [Select id From Recordtype Where DeveloperName = 'End_User_Account'];
        update testAcc;
		testContact = TestQuoteUtil.createContact(testAcc .id);
        testOpp  = TestQuoteUtil.createOpportunity(testAcc , testResseller );
        Test.setCurrentPageReference(new PageReference('Page.RedirectorAndValidator')); 
        System.currentPageReference().getParameters().put('oppId', testOpp.id);
        System.currentPageReference().getParameters().put('type', 'Quote');
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];    
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAcc, '', 'Direct', 'Open', 'Quote', false, '');
        //quoteForTest.SBQQ__PriceBook__c = '01s20000000E0PW';
        //quoteForTest.SBQQ__PricebookId__c = '01s20000000E0PW';
         insert quoteForTest;
        //update quoteForTest;
        return  quoteForTest;
	}

	private static SBQQ__Quote__c prepareData2() {
		user testUser = [Select id From User Where id=: UserInfo.getUserId()];
		//QuoteTestHelper.createCustomSettings(); // deplicate
		/*
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c companyQT = TestQuoteUtil.createQCompany(sub.id);
		*/
			Account testResseller = TestQuoteUtil.createReseller();
			//testAcc = TestQuoteUtil.createAccount(companyQT.id);
			
			testAcc = QTTestUtils.createMockAccount('TestCompany', testUser, true);
			testAcc.RecordType = [Select id From Recordtype Where DeveloperName = 'End_User_Account'];
			update testAcc;
			testContact = TestQuoteUtil.createContact(testAcc .id);
		//testAcc.RecordtypeId = PARTNERACC;
        //update testAcc;
			testOpp  = TestQuoteUtil.createOpportunity(testAcc , testResseller );
			Test.setCurrentPageReference(new PageReference('Page.RedirectorAndValidator')); 
			System.currentPageReference().getParameters().put('oppId', testOpp.id);
			System.currentPageReference().getParameters().put('type', 'Quote');
			RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAcc, '', 'Direct', 'Open', 'Quote', false, '');
         //quoteForTest.SBQQ__PriceBook__c = '01s20000000E0PW';
         //quoteForTest.SBQQ__PricebookId__c = '01s20000000E0PW';

		insert quoteForTest;
        //update quoteForTest;
        return  quoteForTest;
	}
}