global with sharing class SuMoDisplayboardController {

 private final Displayboard__c displayboard;
    
  public SuMoDisplayboardController(ApexPages.StandardController SuMoDisplayboardController) {
    this.displayboard = (Displayboard__c)SuMoDisplayboardController.getRecord();
  }

  Public with Sharing class Leaderboard {
    public String Id  {get; set;}
    public Integer Rank {get; set;}
    public String SmallPhotoUrl {get; set;}
    public String FullPhotoUrl {get; set;}
    public String Name {get; set;}
    public String Experience {get; set;}
    public String ExperienceLabel {get; set;}
  }

@RemoteAction
  global static object fetchUserDisplayboardData(string leaderboard, integer top) {
    
    Map<string, Object> leaderboardusers = (Map<string, Object>) JSON.deserializeUntyped(leaderboard);
    List< Object > items = (List< Object >)leaderboardusers.get('leaderboard');
    Set<id> users = new Set<id>();
    List<Leaderboard> sortedUsers = new List< Leaderboard >();
    Integer i = 0;
    
    for (Object item : items) {
        Map<String, Object> it = (Map<String, Object >)item;
        users.add((Id) it.get('userid'));
        Leaderboard usertoadd = new Leaderboard ();
        usertoadd.Id = (String)it.get('userid');
        usertoadd.Name = (String)it.get('username');
        usertoadd.Experience = (String)it.get('experience');
        usertoadd.ExperienceLabel = (String)it.get('experiencelabel');

        sortedUsers.add(usertoadd);

        i++;
        if (i == top){
           break;
        }
    }

    Map<Id, User> leaderboardList = new Map<Id, User>([select Id,
                        SmallPhotoUrl,
                        FullPhotoUrl
                        from User 
                        where id in :users 
                        and isActive = true limit 100]);
                        
    List<Leaderboard> sortedLeaderboard = new List<Leaderboard>();
    
    Integer rank = 1;
   
    for (Leaderboard l : sortedUsers) {
        Leaderboard usertoadd = new Leaderboard ();
        usertoadd.SmallPhotoUrl = leaderboardList.get(l.Id).SmallPhotoUrl;
        usertoadd.FullPhotoUrl = leaderboardList.get(l.Id).FullPhotoUrl;
        usertoadd.Rank = rank;
        usertoadd.Name = l.name;
        usertoadd.Experience = l.experience;
        usertoadd.ExperienceLabel = l.experiencelabel;

        sortedLeaderboard.add(usertoadd);

        rank++;
    }  
                        
    return sortedLeaderboard;
  }
}