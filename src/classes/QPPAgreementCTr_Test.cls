/****************************************************************
*
*  QPPAgreementCTr
*
*  2020-04-10 UIN Added Logic Testing QPPAGreementCTR class
*****************************************************************/
@isTest
public class QPPAgreementCTr_Test{
    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    
    @testSetup
    public static void testSetup() {
        Test.startTest();
        QuoteTestHelper.createCustomSettings();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        Test.stopTest();
    }
    
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
        Country_Code_Two_Letter__c = countryAbbr,
        Country_Name__c = countryName,
        Subsidiary__c = subsId);
        return qlikTechIns;
    }
    
    
    @isTest
    public static void AgreementOwnerCheck() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {   
        Test.startTest();        
            Account testAccount1 = QTTestUtils.createMockAccount('OEM Part company with a unique name', testUser, true);
            testAccount1.RecordtypeId = PARTNERACC;
            testAccount1.ECUSTOMS__RPS_Status__c = 'No Matches';
            testAccount1.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            testAccount1.Territory_Country__c = 'United Kingdom';
            update testAccount1;
            
            Account testAccount = QTTestUtils.createMockAccount('OEM Test company with a unique name', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAccount.ECUSTOMS__RPS_Status__c = 'No Matches';
            testAccount.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            update testAccount;

            QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

            Contact testContact = QuoteTestHelper.createContact(testAccount1.id);
            insert testContact;

            
            
            RecordType rTypeOpp = [Select id From RecordType Where developerName = 'Sales_QCCS'];
            Opportunity opp = QuoteTestHelper.createOpportunity(testAccount1, '', rTypeOpp);
            opp.End_User_Account_del__c  = testAccount.id;
            insert opp;        
            
            Account accForTest = [Select id,ownerid,owner.name,QPP_Status__c,Territory_Country__c From Account where id =: testAccount1.id];
            
            ApexPages.StandardController sc = new ApexPages.StandardController(accForTest);
            QPPAgreementCTr agreementCntr = new QPPAgreementCTr(sc);
            agreementCntr.acc = accForTest;
            System.assertEquals(true, agreementCntr.accQPPStatuscheck);
            Boolean ret = agreementCntr.accAllowed;
            boolean ret1 = agreementCntr.accQPPRussiacheck;
            Test.stopTest();
        }
    }
    
    @isTest
    public static void AgreementPSMCheck() {
        User testUser = QTtestUtils.createMockUserForProfile('System Administrator');
        System.runAs(testUser) {   
        Test.startTest();        
            User testUser1 = QTtestUtils.createMockUserForProfile('System Administrator');
            Account testAccount1 = QTTestUtils.createMockAccount('OEM Part company with a unique name', testUser, true);
            testAccount1.RecordtypeId = PARTNERACC;
            testAccount1.ECUSTOMS__RPS_Status__c = 'No Matches';
            testAccount1.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            testAccount1.Territory_Country__c = 'United Kingdom';
            testAccount1.QPP_Status__c = 'Agreement Executed';
            testaccount1.ownerid =testUser1.Id;
            update testAccount1;
            
            
            
            Account testAccount = QTTestUtils.createMockAccount('OEM Test company with a unique name', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAccount.ECUSTOMS__RPS_Status__c = 'No Matches';
            testAccount.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            update testAccount;

            QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

            Contact testContact = QuoteTestHelper.createContact(testAccount1.id);
            insert testContact;

            
            system.debug('fff'+testUser.ID);
            RecordType rTypeOpp = [Select id From RecordType Where developerName = 'Sales_QCCS'];
            Opportunity opp = QuoteTestHelper.createOpportunity(testAccount1, '', rTypeOpp);
            opp.End_User_Account_del__c  = testAccount.id;
            insert opp; 
            opp.Active_PSM__c = testUser.Id;
            update opp;       
            system.debug('gggg'+opp.Active_PSM__c);
            Account accForTest = [Select id,ownerid,owner.name,QPP_Status__c From Account where id =: testAccount1.id];
            
            ApexPages.StandardController sc = new ApexPages.StandardController(accForTest);
            QPPAgreementCTr agreementCntr = new QPPAgreementCTr(sc);
            agreementCntr.acc = accForTest;
            System.assertEquals(true, agreementCntr.accQPPStatuscheck);
            Boolean ret = agreementCntr.accAllowed;
            Test.stopTest();
        }
    }
}