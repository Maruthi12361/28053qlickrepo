/*
 File Name: CaseToCRHandler
 Moved from trigger CaseToCR on Case (after insert, after update)
 Description: Populate/ unpopulate/ change lookup field on Change Request record to point
        to the originating Case when Case is saved with Lookup value to a CR

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.

*/

public with sharing class CaseToCRHandler {

    public static void handleCaseToCR(List<Case> newCases, List<Case> oldCases) {
        // Map of Change Control Id, Case Id
        Map<Id, Id> mapChgCCase = new Map<Id, Id>();
        // Change Control Ids
        Set<Id> ChgCtrlIds = new Set<Id>();
        List<SLX__Change_Control__c> updateChgCs = new List<SLX__Change_Control__c>();

        for (Integer i = 0; i < newCases.size(); i++) {
            Case newCaseRec = newCases[i];
            // To update Case Lookup on linked CR/ CRs

            if (newCaseRec.Linked_to_CR__c != null) {
                // Adding new CR Id to Set
                ChgCtrlIds.add(newCaseRec.Linked_to_CR__c);
                // Adding to Map Change Control Id, Case Id
                mapChgCCase.put(newCaseRec.Linked_to_CR__c, newCaseRec.Id);
            }
            if (oldCases != null && newCaseRec.Linked_to_CR__c != null) {
                Case oldCaseRec = oldCases[i];
                if (newCaseRec.Linked_to_CR__c != oldCaseRec.Linked_to_CR__c && oldCaseRec.Linked_to_CR__c != null) {
                    // Adding old CR Id to Set
                    ChgCtrlIds.add(oldCaseRec.Linked_to_CR__c);
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, '[CaseToCRHandler] ChgCtrlIds:' + ChgCtrlIds);
        if (!ChgCtrlIds.isEmpty()) {
            for (SLX__Change_Control__c ChgCtrl : [
                    SELECT Problem_Case_Nr__c, Id
                    FROM SLX__Change_Control__c
                    WHERE Id IN :ChgCtrlIds
            ]) {
// If this is the first Change Request lookup on the Case record
                if (mapChgCCase.keySet().contains(ChgCtrl.Id)) {
                    ChgCtrl.Problem_Case_Nr__c = mapChgCCase.get(ChgCtrl.Id);
                }
// If this Change Request lookup replaced with other on the Case record
                else {
                    ChgCtrl.Problem_Case_Nr__c = null;
                }
                updateChgCs.add(ChgCtrl);
            }
        }
        System.debug(LoggingLevel.DEBUG, '[CaseToCRHandler] updateChgCs:' + updateChgCs);
        if (!updateChgCs.isEmpty()) {
            try {
                update updateChgCs; // Update Change Request Records
            } catch (System.Exception excep) {
                System.debug(LoggingLevel.DEBUG, '[CaseToCRHandler] Change Request Update Error' + excep.getMessage());
                newCases[0].addError('Change Request Update Error');
            }
        }

    }
}