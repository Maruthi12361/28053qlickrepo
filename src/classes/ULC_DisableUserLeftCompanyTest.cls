/********************************************************

ULC_DisableUserLeftCompanyTest
Test class for ULC_DisableUserLeftCompany

ChangeLog:
2015-02-10	AIN 	Initial implementation
2018-02-05      CRW     Modified ULC name field to avoid new validation error.BMW-508
***********************************************************/

@isTest
public with sharing class ULC_DisableUserLeftCompanyTest {

	@testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
       }
	//Should successfully rename the ULC
	public static testmethod void test1() 
	{
		test.startTest();
		Contact c = CreateTestULCContact(true);
		CreateTestULCUser(c);
		CreateTestULCDetails(c);
		CreateTestAssignedULCLevel(c);
		test.stopTest();

		RunTest();

		User testUser = [select Id, UserName, Name, IsActive,FederationIdentifier, Alias, Email, CommunityNickname from User where Name = :firstName + ' ' + lastName];
		system.debug('The below variables should be changed');		
		system.debug('User.UserName: ' + testuser.UserName);		
		system.debug('User.FederationIdentifier: ' + testuser.FederationIdentifier);
		system.debug('User.IsActive: ' + testuser.IsActive);
		system.debug('User.Alias: ' + testuser.Alias);
		system.debug('User.Email: ' + testuser.Email);
		system.debug('User.CommunityNickname: ' + testuser.CommunityNickname);
		
		//ULC_Details__c testULCDetails = [select Id, ULCName__c, ULCStatus__c from ULC_Details__c where ULCName__c = 'DULCTest'];
		ULC_Details__c testULCDetails = [select Id, ULCName__c, ULCStatus__c, ULC_Password__c from ULC_Details__c where ContactId__c = :c.Id];
		system.debug('ULCDetails.ULCStatus__c: ' + testULCDetails.ULCStatus__c);
		system.debug('ULCDetails.ULCName__c: ' + testULCDetails.ULCName__c);
		system.debug('ULCDetails.ULC_Password__c: ' + testULCDetails.ULC_Password__c);

		Assigned_ULC_Level__c CreateTestAssignedULCLevel = [select Id, Name, Status__c from Assigned_ULC_Level__c where ContactId__c = :c.Id];
		system.debug('Assigned_ULC_Level__c.Status__c: ' + CreateTestAssignedULCLevel.Status__c);

		system.assert(!testUser.IsActive);
		system.assertEquals(testULCDetails.ULCStatus__c, ULC_DisableUserLeftCompany.STATUS_LEFT_COMPANY);
		system.assertEquals(CreateTestAssignedULCLevel.Status__c, ULC_DisableUserLeftCompany.STATUS_LEFT_COMPANY);
	}
	//Should not do anything
	public static testmethod void test2() 
	{
		test.startTest();
		Contact c = CreateTestULCContact(false); 
		CreateTestULCUser(c);
		CreateTestULCDetails(c);
		CreateTestAssignedULCLevel(c);
		test.stopTest();
		
		RunTest();

		User testUser = [select Id, UserName, Name, User.INT_NetSuite_InternalID__c, IsActive,FederationIdentifier, Alias, Email, CommunityNickname from User where Name = :firstName + ' ' + lastName];
		system.debug('The below variables should not be changed');		
		system.debug('User.UserName: ' + testuser.UserName);
		system.debug('User.FederationIdentifier: ' + testuser.FederationIdentifier);
		system.debug('User.IsActive: ' + testuser.IsActive);
		system.debug('User.Alias: ' + testuser.Alias);
		system.debug('User.Email: ' + testuser.Email);
		system.debug('User.CommunityNickname: ' + testuser.CommunityNickname);
		system.debug('User.INT_NetSuite_InternalID__c: ' + testuser.INT_NetSuite_InternalID__c);
		
		//ULC_Details__c testULCDetails = [select Id, ULCName__c, ULCStatus__c from ULC_Details__c where ULCName__c = 'DULCTest'];
		ULC_Details__c testULCDetails = [select Id, ULCName__c, ULCStatus__c, ULC_Password__c from ULC_Details__c where ContactId__c = :c.Id];
		system.debug('ULCDetails.ULCStatus__c: ' + testULCDetails.ULCStatus__c);
		system.debug('ULCDetails.ULCName__c: ' + testULCDetails.ULCName__c);
		system.debug('ULCDetails.ULC_Password__c: ' + testULCDetails.ULC_Password__c);

		Assigned_ULC_Level__c CreateTestAssignedULCLevel = [select Id, Name, Status__c from Assigned_ULC_Level__c where ContactId__c = :c.Id];
		system.debug('Assigned_ULC_Level__c.Status__c: ' + CreateTestAssignedULCLevel.Status__c);

		system.assert(testUser.IsActive);
		system.assertEquals(testULCDetails.ULCStatus__c, 'Active');
		system.assertEquals(CreateTestAssignedULCLevel.Status__c, 'Approved');
	}
	public static testmethod void test3()
	{
		Test.StartTest();
		ULC_DisableUserLeftCompanySchedULC sh1 = new ULC_DisableUserLeftCompanySchedULC();
		String sch = '0 0 23 * * ?'; 
		system.schedule('Test ULC_DisableUserLeftCompanySchedULC Check', sch, sh1); 
		Test.stopTest();
	}
	public static testmethod void test4()
	{
		Test.StartTest();
		ULC_DisableUserLeftCompanySchedUser sh1 = new ULC_DisableUserLeftCompanySchedUser();
		String sch = '0 0 23 * * ?'; 
		system.schedule('Test ULC_DisableUserLeftCompanySchedUser Check', sch, sh1); 
		Test.stopTest(); 
	}

	Private static void RunTest() {
		
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
 	    System.runAs (thisUser) 
 	    {
			ULC_DisableUserLeftCompany.DeactivateSFDCUsers();
		}
		ULC_DisableUserLeftCompany.DeactivateULCDetails();
		ULC_DisableUserLeftCompany.DeactivateAssignedULCLevels();
		
	}

	private static string firstName = 'Test';
	private static string lastName = 'Testersson9874654';
	private static Contact CreateTestULCContact(boolean leftCompany)
	{
        Id accountId = CreateTestAccount();

		Contact testContact = new Contact (
                AccountId = accountId, OwnerId = UserInfo.getUserId(), LastName = lastName, 
                FirstName = firstName, Email = firstName + lastName + '@qlikview.com', Left_Company__c = leftCompany);
		insert testContact;

		return testContact;
	}
	private static void CreateTestULCUser(Contact c)
	{
		Profile p = [select id from profile where name = 'Eventforce Portal – Employee'];
		
		string lotsOfCharacters = '0123456789012345678';

		User testUser = new User(
			IsActive = true, FirstName = firstName, LastName = lastName, Username = firstName+lastName+'@qlik.com',
			Email = firstName+lastName+'@qlik.com',	Alias = 'ttest', FederationIdentifier = 'testtest', 
			CommunityNickname = firstName+lastName+lotsOfCharacters, ContactId = c.Id, EmailEncodingKey='UTF-8', 
			LocaleSidKey='en_GB', TimeZoneSidKey='Europe/London', LanguageLocaleKey='en_US', ProfileId = p.Id,
			INT_NetSuite_InternalID__c = 'Test');
		
		insert testUser;

		testUser = [select id, UserName, name, profileid, UserRoleId, SF_License_Name__c from user where name = :firstName + ' ' + lastName];
		system.debug('Id: ' + testUser.Id);
		system.debug('name: ' + testUser.Name);
		system.debug('UserName: ' + testUser.UserName);
		system.debug('profile: ' + testUser.ProfileId);
		system.debug('userrole: ' + testUser.UserRoleId);
		system.debug('SF_License_Name__c: ' + testUser.SF_License_Name__c);
	}
	private static void CreateTestULCDetails(Contact c)
	{
		ULC_Details__c testULCDetails = New ULC_Details__c(
            ULCName__c = 'dulctest', ULC_Password__c = 'abcabcabc', ULCStatus__c = 'Active', ContactId__c = c.Id);
        insert testULCDetails;

        testULCDetails = [select ULCName__c, ULC_Password__c, ULCStatus__c, ContactId__c from ULC_Details__c where ContactId__c = :c.Id];
		system.debug('ULCName__c: ' + testULCDetails.ULCName__c);
		system.debug('ULC_Password__c: ' + testULCDetails.ULC_Password__c);
		system.debug('ULCStatus__c: ' + testULCDetails.ULCStatus__c);
		system.debug('ContactId__c: ' + testULCDetails.ContactId__c);
	}
	private static void CreateTestAssignedULCLevel(Contact c)
	{
		CreateTestULCLevel();
		string CPView = [select Id from ULC_Level__c where Name = 'CPVIEW' LIMIT 1].Id;
		system.debug('CPView: ' + CPView);

		Assigned_ULC_Level__c AssignedLevels = new Assigned_ULC_Level__c(
            ContactId__c = c.Id, ULCLevelId__c = CPView, Status__c = 'Approved');
        insert AssignedLevels;

        AssignedLevels = [select ContactId__c, ULCLevelId__c, Status__c from Assigned_ULC_Level__c where ContactId__c = :c.Id];
		system.debug('ContactId__c: ' + AssignedLevels.ContactId__c);
		system.debug('ULCLevelId__c: ' + AssignedLevels.ULCLevelId__c);
		system.debug('Status__c: ' + AssignedLevels.Status__c);
	}
	private static void CreateTestULCLevel()
	{
		ULC_Level__c level = new ULC_Level__c(
			OwnerId = UserInfo.getUserId(),
			Name = 'CPView',
			Description__c = 'CPView',
			Status__c = 'Active'
			);
		insert level;
	}
	private static Id CreateTestAccount()
	{
		CreateTestQTCompany();

        QlikTech_Company__c company = [Select id, QlikTech_Company_Name__c from QlikTech_Company__c where QlikTech_Company_Name__c = 'QlikTech Nordic AB' limit 1];
        system.debug('QlikTech_Company_Name__c: ' + company.QlikTech_Company_Name__c);
        
        Account TestAccount = new Account(
                OwnerId = UserInfo.getUserId(), Name = 'ULC Test Ltd.', Navision_Status__c = 'Customer',
                QlikTech_Company__c = 'Sweden', Billing_Country_Code__c = company.id, 
                BillingStreet = 'Street 123', BillingCity = 'City 123', BillingState = 'S 123',
        		BillingCountry = 'Sweden', ShippingStreet = 'Street 123', ShippingCity = 'City 123',
        		ShippingState = 'S 123', ShippingCountry = 'Sweden',Account_Creation_Reason__c = 'Test');
        insert TestAccount;
        return TestAccount.Id;
	}
	private static void CreateTestQTCompany()
	{
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c qtc = new QlikTech_Company__c();
        qtc.name = 'SWE';
        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
        qtc.Country_Name__c = 'Sweden';
        qtc.CurrencyIsoCode = 'SEK';
		qtc.Subsidiary__c = testSubs1.id;
        insert qtc;
	}
}