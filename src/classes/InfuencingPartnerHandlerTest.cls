/******************************************************

        InfuencingPartnerHandlerTest
        Kumar Navneet
        Date - 04/21/17

        Changelog:
        05/01/2018 Pramod Kumar V  Test class coverage

    ******************************************************/

@isTest
private class InfuencingPartnerHandlerTest {

    public static TestMethod void TEST_InfuencingPartnerHandler()
    {
        QTTestUtils.GlobalSetUp();
		
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com');
        insert u;
        Subsidiary__c  sub = new Subsidiary__c (
            Name = 'test',
            Legal_Entity_Name__c = 'Test',
			Approver_SVP__c = u.id
        );
         insert sub;
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',Subsidiary__c = sub.id
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        Id partnerAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();


        //Id AccRecordTypeId_OEMEndUserAccount = '012D0000000KBYx'; //OEM End User
        Account TestAccount = new Account(
            Name = 'ULC Test Ltd.',
            Navision_Status__c = 'Customer',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Commercial - Lower SMB'
        );
        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            recordtypeId = partnerAccRecordTypeId,
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Commercial - Lower SMB'                            
        );
        List<Account> AccToInsert = new List<Account>();
        AccToInsert.Add(TestAccount);
        AccToInsert.Add(TestPartnerAccount);
        insert AccToInsert;

        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partnerr',
            LastName = 'PPr',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact;

        Contact PartnerContact1 = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact1;

        test.startTest();

        Opportunity Opp = new Opportunity();
        Opp.AccountId = TestAccount.Id;
        Opp.PRM_Referring_Contact__c = PartnerContact1.Id;
        Opp.Standard_Referral_Margin__c = '10';
        Opp.Partner_Fee_Type__c = 'Influence Fee';
        Opp.Short_Description__c = 'My Test Description';
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'USD';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Identified';
        Opp.Partner_Contact__c = PartnerContact.Id;
        insert Opp;


        Influencing_Partner__c IPC= new Influencing_Partner__c();
        IPC.Opportunity__c = Opp.Id;
        IPC.Influencing_Partner_Account__c = TestPartnerAccount.id;
        IPC.Influencing_Partner_Contact__c= PartnerContact1.id ;
        insert IPC;
 
        IPC.Referral__c = true;
        update IPC;
        Influencing_Partner__c ipartner = [select id, approval_status__C from Influencing_Partner__c where id=:IPC.Id];
        system.assertEquals(ipartner.Approval_Status__c, 'Requested');
        
        test.stopTest();

    }
}