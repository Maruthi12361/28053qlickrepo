/**     * File Name:TestUpdateSpecializationDesignationStatus 
        * Description : Test class of UpdateSpecializationDesignationStatus 
        * @author : Anjuna Baby
        * Modification Log ===============================================================

*/

@isTest() 
public class TestUpdateSpecializationDesigStatus {
    static testMethod void AccUpdateSpecializationHandlerTest(){
    
    Test.startTest();
    
    Account acc=new Account(name ='test');
    insert acc;
    
    Specialization__c spl=new Specialization__c(Account__c=acc.id,Designation_Status__c='Approved',Specialization_Category__c='Technology',Specialization__c='Mobility',Expiration_Date__c=Date.Today()-1);
    insert spl;
    
    UpdateSpecializationDesignationStatus updateDsigStatus = new UpdateSpecializationDesignationStatus ();
    database.executeBatch(updateDsigStatus );
    
    Test.stopTest();
   
    }
}