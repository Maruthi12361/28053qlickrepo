/****************************************************************************************************
  IT-79
    CaseAttachment 
    Description: Test class for Schedular CaseAttachmentPurge_RunBatch

    Change log:
    IT-467 : Custom settings records added to maintain code coverage for CaseAttachmentPurge CRW
    IT-606 : Run scheduled CaseAttachmentPurge_RunBatch - ext_vos - 2018-07-09
****************************************************************************************************/    
@isTest 
private class CaseAttachmentPurge_BatchTest  {
    static testMethod void CaseAttachmentsPurge_RunBatch() {
        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.CaseAttachmentPurgeEmail__c = 'test@test.com';
        settings.QlikNoReplyEmailAddress__c = 'no-reply@test.com';
        insert settings;

        CaseAttachmentPurge_RunBatch csh1 = new CaseAttachmentPurge_RunBatch();

        Test.StartTest();
        csh1.execute();
        Test.stopTest();
    }
}