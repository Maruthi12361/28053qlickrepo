/* 
 * Copyright (c) 2018 Zuora, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to use copy, 
 * modify, merge, publish the Software and to distribute, and sublicense copies of 
 * the Software, provided no fee is charged for the Software.  In addition the
 * rights specified above are conditioned upon the following:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * Zuora, Inc. or any other trademarks of Zuora, Inc.  may not be used to endorse
 * or promote products derived from this Software without specific prior written
 * permission from Zuora, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * ZUORA, INC. BE LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
 *
 * IN THE EVENT YOU ARE AN EXISTING ZUORA CUSTOMER, USE OF THIS SOFTWARE IS GOVERNED
 *
 * BY THIS AGREEMENT AND NOT YOUR MASTER SUBSCRIPTION AGREEMENT WITH ZUORA.
 */
 /*
 *Z_DefaultValuesTest
 *Created by: Ariel Xiao
 *The purpose of this class is to get test coverage for the Z_DefaultValues class
 *   13.02.2019 : Björn Andersson fixed test class error related to new duplicate rules on account and contact
 */
@isTest
public class DefaultValuesTest extends zqu.CreateQuoteController.PopulateDefaultFieldValuePlugin {
   private static String FUNC_STR = 'Z_DefaultValuesTest: '; 

   
	public static final String acctId = '12345678901234567890123456789012';
	public static final String subId = '23456789012345678901234567890123';
	public static final String conId = '34567890123456789012345678901234';

    private static Account account;
    private static Account partner;
    private static Account secondPartner;
    private static Partner_Category_Status__c pcs;
    private static Partner_Category_Status__c pcs2;
    private static Contact endUserContact;
    private static Contact partnerContact;
    private static Contact con;
    private static Zuora__CustomerAccount__c customerAccount;
    private static Opportunity opp;
    private static Product2 product;
    private static zqu__ProductRatePlan__c productRatePlan;
    private static zqu__Quote__c quote;
    private static DefaultValues controller;
    private static PageReference createQuote;
    private static User user;

    @isTest
	private static void testNewSubDefaultValues_partner() {
        set_up_data();

        opp.Sell_Through_Partner__c = partner.Id;
        opp.Partner_Contact__c = partnerContact.Id;
        opp.Second_Partner__c = secondPartner.Id;
        opp.Revenue_Type__c = 'Distributor';
        update opp;

        OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = con.Id;
        ocr2.OpportunityId = opp.Id;
        ocr2.IsPrimary = TRUE;
        ocr2.Role = 'Invoice Recipient - Bill To';
        insert ocr2;

	  	Test.setCurrentPage(createQuote);
        Test.startTest();
        controller.populateDefaultFieldValue(quote, new zqu.PropertyComponentController.ParentController());
        Test.stopTest();

	  	//assertion 
	    System.assertEquals(quote.zqu__InitialTerm__c,36);
	    System.assertEquals(quote.zqu__RenewalTerm__c,12);
	    System.assertEquals(quote.zqu__ValidUntil__c,Date.today().addDays(30));
	    System.assertEquals(quote.zqu__StartDate__c,Date.today());
	    System.assertEquals(quote.zqu__PaymentMethod__c,'Other');
	    System.assertEquals(quote.zqu__AutoRenew__c,TRUE);
	    System.assertEquals(quote.zqu__QuoteBusinessType__c, 'New'); 
	    System.assertEquals(quote.zqu__Primary__c, TRUE);
	    System.assertEquals(quote.Quote_Status__c, 'Open');
	    System.assertEquals(quote.Billing_Frequency__c, 'Annual');
	    System.assertEquals(quote.Subscription_Up_Lift__c,'UseLatestProductCatalogPricing');   

    }

    @isTest
    private static void testNewSubDefaultValues_partner_ocr1() {
        set_up_data();

        opp.Sell_Through_Partner__c = partner.Id;
        opp.Partner_Contact__c = partnerContact.Id;
        opp.Second_Partner__c = secondPartner.Id;
        opp.Revenue_Type__c = 'Distributor';
        update opp;

        Test.setCurrentPage(createQuote);
        Test.startTest();
        controller.populateDefaultFieldValue(quote, new zqu.PropertyComponentController.ParentController());
        Test.stopTest();

        //assertion 
        System.assertEquals(quote.zqu__InitialTerm__c,36);
        System.assertEquals(quote.zqu__RenewalTerm__c,12);
        System.assertEquals(quote.zqu__ValidUntil__c,Date.today().addDays(30));
        System.assertEquals(quote.zqu__StartDate__c,Date.today());
        System.assertEquals(quote.zqu__PaymentMethod__c,'Other');
        System.assertEquals(quote.zqu__AutoRenew__c,TRUE);
        System.assertEquals(quote.zqu__QuoteBusinessType__c, 'New'); 
        System.assertEquals(quote.zqu__Primary__c, TRUE);
        System.assertEquals(quote.Quote_Status__c, 'Open');
        System.assertEquals(quote.Billing_Frequency__c, 'Annual');
        System.assertEquals(quote.Subscription_Up_Lift__c,'UseLatestProductCatalogPricing');
        

    }

    @isTest
    private static void testNewSubDefaultValues_direct() {
        set_up_data();

        opp.Revenue_Type__c = 'Direct';
        update opp;

        OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = con.Id;
        ocr2.OpportunityId = opp.Id;
        ocr2.IsPrimary = TRUE;
        ocr2.Role = 'Invoice Recipient - Bill To';
        insert ocr2;

        Test.setCurrentPage(createQuote);
        Test.startTest();
        controller.populateDefaultFieldValue(quote, new zqu.PropertyComponentController.ParentController());
        Test.stopTest();

        //assertion 
        System.assertEquals(quote.zqu__InitialTerm__c,36);
        System.assertEquals(quote.zqu__RenewalTerm__c,12);
        System.assertEquals(quote.zqu__ValidUntil__c,Date.today().addDays(30));
        System.assertEquals(quote.zqu__StartDate__c,Date.today());
        System.assertEquals(quote.zqu__PaymentMethod__c,'Other');
        System.assertEquals(quote.zqu__AutoRenew__c,TRUE);
        System.assertEquals(quote.zqu__QuoteBusinessType__c, 'New'); 
        System.assertEquals(quote.zqu__Primary__c, TRUE);
        System.assertEquals(quote.Quote_Status__c, 'Open');
        System.assertEquals(quote.Billing_Frequency__c, 'Annual');
        System.assertEquals(quote.Subscription_Up_Lift__c,'UseLatestProductCatalogPricing');

    }

    
    /**
     * @Description tests new renewals to ensure that start date overrides are respected 
     */ 
     private static testMethod void testRenewalStartDateOverride(){

		set_up_data();

        opp.Revenue_Type__c = 'Direct';
        update opp;

		Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(customerAccount, account, '123456789');
		insert sub;

        quote = Z_TestFactory.makeQuote2(opp, account);
      
      
      
        quote.zqu__BillToContact__c = endUserContact.id;
        quote.zqu__SoldToContact__c = endUserContact.id;
		quote.zqu__StartDate__c = Date.newInstance(1983, 1, 11);
		quote.Allow_Start_Date_Override__c = true;

     
		System.PageReference quoteEnhancement = Page.zqu__quoteEnhancement;
     	quoteEnhancement.getParameters().put('quoteType', 'Renewal');
     	quoteEnhancement.getParameters().put('subscriptionId', '123456789');


		Test.setCurrentPage(quoteEnhancement);
		controller = new DefaultValues(); 
		controller.populateDefaultFieldValue(quote, new zqu.PropertyComponentController.ParentController());
			
		System.assertEquals(Date.newInstance(1983, 1, 11), quote.zqu__StartDate__c);

     }
     

    /**
     * @Description tests new renewals to ensure that start date gets set properly if no override is selected
     */ 
     private static testMethod void testRenewalStartDateNoOverride(){

		set_up_data();

        opp.Revenue_Type__c = 'Direct';
        update opp;

		Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(customerAccount, account, '123456789');
		sub.Zuora__TermEndDate__c = Date.newInstance(2100,1,1);
		insert sub;

        quote = Z_TestFactory.makeQuote2(opp, account); 
        quote.zqu__BillToContact__c = endUserContact.id;
        quote.zqu__SoldToContact__c = endUserContact.id;
		quote.zqu__StartDate__c = Date.newInstance(1983, 1, 11);

     
		System.PageReference quoteEnhancement = Page.zqu__quoteEnhancement;
     	quoteEnhancement.getParameters().put('quoteType', 'Renewal');
     	quoteEnhancement.getParameters().put('subscriptionId', '123456789');


		Test.setCurrentPage(quoteEnhancement);
		controller = new DefaultValues(); 
		controller.populateDefaultFieldValue(quote, new zqu.PropertyComponentController.ParentController());
		
		
		System.assertEquals(sub.Zuora__TermEndDate__c, quote.zqu__StartDate__c);     		
    
     }    
    


    private static void set_up_data(){

        QuoteTestHelper.createCustomSettings();
        QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
     
        account = Z_TestFactory.makeAccount('Sweden', 'Test Account 596', '','80221');
        account.Navision_Status__c = 'Partner';
        account.OwnerId = UserInfo.getUserId();
        account.Name = 'Sell Qlik Test Ltd.';
        account.Navision_Status__c = 'Partner';
        account.QlikTech_Company__c = 'QlikTech Inc';
        account.Billing_Country_Code__c = QTComp.Id;
        insert account;
        account.Pending_Validation__c =FALSE;
        update account;

        partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        insert partner;
        partner.Pending_Validation__c =FALSE;
        update partner;

        secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 339', '','65000-480');
        insert secondPartner;
        secondPartner.Pending_Validation__c =FALSE;
        update secondPartner;

        pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Distributor', 'Distributor', 'Distributor');
        insert pcs;

        pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        endUserContact = Z_TestFactory.makeContact(account);
        insert endUserContact;

        partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.Phone = '414-123-4444';
        insert partnerContact;

        customerAccount = Z_TestFactory.createBillingAccount(partner, 'id0123456789KK');
        insert customerAccount;

        con = Z_TestFactory.makeContact(account,'aaaa1789','bbbb134534','AA1@gtytyt.com','United Kingdom','','80011');
        con.MailingStreet = 'hhhh';
        con.MailingCity = 'sdfsdfsd';
        con.phone = '404-464-4784';
        insert con;
        
        opp = Z_TestFactory.makeOpportunity(account, con,'aaaasas12');
        insert opp;

        product = Z_TestFactory.makeProduct();
        product.Training_Product__c = 'Yes';
        insert product;

        productRatePlan = Z_TestFactory.makeProductRatePlan(product.Id, 'ratePlanId1');
        insert productRatePlan ;

        quote = Z_TestFactory.makeQuote2(opp, account);
      
        quote.zqu__BillToContact__c = endUserContact.id;
        quote.zqu__SoldToContact__c = endUserContact.id;
        insert quote;

        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = con.Id;
        ocr.OpportunityId = opp.Id;
        ocr.IsPrimary = TRUE;
        ocr.Role = 'Quote Recipient - Sold To';
        insert ocr;

        controller = new DefaultValues();
        createQuote = Page.zqu__CreateQuote;
        createQuote.getParameters().put('oppId', opp.Id);
        createQuote.getParameters().put('quoteType', 'Subscription');
        createQuote.getParameters().put('stepNumber', '2');
        createQuote.getParameters().put('billingAccountId', acctId);
        
    }
      

}