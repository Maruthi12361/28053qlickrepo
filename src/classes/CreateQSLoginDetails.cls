/********************************************************
* CLASS: CreateQSLoginDetails
* DESCRIPTION: Batch class for adding a Task and Campaign member when a user logins first time to QSD or QSC. 
* This class gets called either from a trigger or a scheduled job.
* Scope query is based on WebActivity wich has a join to ULC_Details.
* From ULC_Details we can retrieve the LeadID or the ContactID, which is needed when task and campaign is created.
* The class also handles no dublicate creation for the task and campaignMember on the Lead/Contact.
*
* CHANGELOG:    
*	2018-10-15 - BAD - Added Initial logic
*********************************************************/
global class CreateQSLoginDetails implements Database.Batchable<sObject> {
	
	String query;
	List<Web_Activity__c> lstWebActivities;
	String campaignID;
	WebActivitySettings__c csWebActivity = WebActivitySettings__c.getInstance();
	String taskSubject;
	Boolean createCMO;
	String loginType;
	global List<String> exception_List;

	global CreateQSLoginDetails(List<Web_Activity__c> inWebActivities, String inFirstLoginType) {

		if(exception_List == null)
            exception_List = new List<String>();
		
		lstWebActivities = inWebActivities;
		loginType = inFirstLoginType;
		campaignID = inFirstLoginType == 'QSC_FirstLogin' ? csWebActivity.QSCLoginCampaignID__c : csWebActivity.QSDLoginCampaignID__c;
		taskSubject = inFirstLoginType == 'QSC_FirstLogin' ? csWebActivity.QSCTaskSubject__c : csWebActivity.QSDTaskSubject__c;
		createCMO = inFirstLoginType == 'QSC_FirstLogin' ? csWebActivity.QSCCreateCMOActive__c : csWebActivity.QSDCreateCMOActive__c;

		query = 'select Id, name, ULC__r.id, ULC__r.ULCName__c, ULC__r.LeadID__c, ULC__r.ContactID__c from Web_Activity__c where id in :lstWebActivities';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		List<Web_Activity__c> lstWA = (List<Web_Activity__c>)(scope); 
		List<Id> lstIDs = new List<Id>();
		List<Task> lstTasks = new List<Task>();
        List<CampaignMember> lstCampaignMembers = new List<CampaignMember>();
		Map<Id, String> mapLeadOrContactException = new Map<Id, String>();

		//Add LeadID or ContactID to list
		for(Web_Activity__c wa: lstWA){
			if(wa.ULC__r.LeadID__c != null)
				lstIDs.add(wa.ULC__r.LeadID__c);
			else if(wa.ULC__r.ContactID__c != null)
				lstIDs.add(wa.ULC__r.ContactID__c);
		}

		//Leads - create Task and CampaignMember
		for(Lead l: [select id, 
						(select subject from Tasks where subject =: taskSubject or subject = 'QlikSense Desktop Login'), 
						(select id from campaignmembers where campaignid =: campaignID)
					 from Lead where id in : lstIDs]){
			
			if(l.tasks.size() ==  0){
            	Task t = createTask(l.Id);
            	if(t != null)
            		lstTasks.add(t);
			}            		
			if(createCMO){ //Only create a CMO if enabled in Custom Setting
				if(l.CampaignMembers.size() == 0){
	            	CampaignMember cm = createCampaignMember(l.Id, 'Lead');
	            	if(cm != null)
						lstCampaignMembers.add(cm);    	
				}
			}
		}

		//Contacts - create Task and CampaignMember
		for(Contact c: [select id, 
							(select subject from Tasks where subject =: taskSubject or subject = 'QlikSense Desktop Login'),
							(select id from campaignmembers where campaignid =: campaignID)
						from Contact where id in : lstIDs]){

			if(c.tasks.size() ==  0){
            	Task t = createTask(c.Id);
            	if(t != null) 
            		lstTasks.add(t);
            }
            if(createCMO){ //Only create a CMO if enabled in Custom Setting
				if(c.CampaignMembers.size() == 0){
	            	CampaignMember cm = createCampaignMember(c.Id, 'Contact');
	            	if(cm != null)
						lstCampaignMembers.add(cm);    	
				}
			}
		}

		//Save Tasks
		if(lstTasks.size() > 0){
			List<Database.SaveResult> sResults_task = Database.insert(lstTasks, false);
			for(integer i = 0; i < sResults_task.size(); i++){
                String msg = '';
                if(!sResults_task[i].isSuccess()){
                    msg += 'Activity creation failed for id '+ lstTasks.get(i).WhoId + ' failed due to the Error: "';        
                    for(Database.Error err: sResults_task[i].getErrors()){  
                         msg += err.getmessage() + '<br/>';
                    } 
                    mapLeadOrContactException.put(lstTasks.get(i).WhoId, msg);
                }
            } 
        }

		//Save CampaignMembers
		if(lstCampaignMembers.size() > 0){
			List<Database.SaveResult> sResults_cm = Database.insert(lstCampaignMembers, false);
			for(integer i = 0; i < sResults_cm.size(); i++){
                String msg = '';
                Id contactOrLeadID = lstCampaignMembers.get(i).LeadId != null ? lstCampaignMembers.get(i).LeadId : lstCampaignMembers.get(i).ContactId;
                if(!sResults_cm[i].isSuccess()){
                    msg += 'Campaign member creation failed due to the Error for record with id ' + contactOrLeadID +': "';        
                    for(Database.Error err: sResults_cm[i].getErrors()){  
                         msg += err.getmessage()+'<br/>';
                    } 
                    if(contactOrLeadID != null)
						mapLeadOrContactException.put(contactOrLeadID , msg);
                }
            } 
        }	

   		
		//Save WebActivities - we need to update status and eventually Audit Trail for exceptions on the Web Activity
		for(Web_Activity__c wa: lstWA){
			Id contactOrLeadID = wa.ULC__r.LeadID__c != null ? wa.ULC__r.LeadID__c: wa.ULC__r.ContactID__c;
			if(mapLeadOrContactException.containsKey(contactOrLeadID)){
				wa.Status__c = 'Failed';
				wa.Audit_Trail__c = mapLeadOrContactException.get(contactOrLeadID);
			}
			else{
				wa.Status__c = 'Completed';
				wa.Audit_Trail__c = '';
			}
		}

		if(lstWA.size() > 0){
			List<Database.SaveResult > sResults_wa = Database.update(lstWA, false);
			for(integer i = 0; i < sResults_wa.size(); i++){
                String msg = '';
                if(!sResults_wa[i].isSuccess()){
                    msg += 'Web Activity update failed due to the Error for record with id' + lstWA.get(i).Id +': "';        
                    for(Database.Error err: sResults_wa[i].getErrors()){  
                         msg += err.getmessage()+'<br/>';
                    } 
                }
            }
        }

	}
	
	global void finish(Database.BatchableContext BC) {
	}

    private Task createTask(Id leadOrContactID){
		Task t = new Task();
	    t.WhoId = leadOrContactID;
	    t.Subject = taskSubject;
	    t.ActivityDate = Date.today();
	    t.Status = 'Completed';
	    t.Priority = 'Normal';
	    t.Type = 'TK - Web Activity'; // This is mandatory in QA/Prod

	    if(loginType == 'QSC_FirstLogin'){
		    t.Incentive_Name__c = 'QSC Login';
		    t.Source_ID2__c = 'QlikSense Cloud';
		    t.Description = 'QSC Login ' + string.valueOf(DateTime.now());
	    }
	    else{
		    t.Incentive_Name__c = 'QSD Login';
		    t.Source_ID2__c = 'QlikSense Desktop';
		    t.Description = 'QSD Login ' + string.valueOf(DateTime.now());
	    }
	    
	    return t;
    }	

    private CampaignMember createCampaignMember(Id leadOrContactID, String sObjectName){
        CampaignMember cm;
        cm = new CampaignMember();
        cm.CampaignId = campaignID;
        cm.Status = 'Form Fill Out';
        cm.Number_Of_Form_Submissions__c = 1;
        if(sObjectName == 'Lead')
        	cm.LeadId = leadOrContactID;
    	else if(sObjectName == 'Contact')
			cm.ContactId = leadOrContactID;

		return cm;
    }	

}