/*

Created 2020-06-11 - DKF
Created for DTR-600

*/

@IsTest
public class OppClosedLostSigUpdateHandlerTest {

    private static Account testAcc;
    private static Contact testContact;
    private static Zuora__CustomerAccount__c testCustomerAcc;
    private static Opportunity testOpp;
    private static zqu__Quote__c testQuote;
    private static zqu__QuoteChargeDetail__c testQCD;
    private static QlikTech_Company__c testQTComp;
    private static Q2CWSettings__c testQ2CWSettings;
    private static Steelbrick_Settings__c testSteelbrickSettings;

    static void setup() {

        testQ2CWSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();
        testQTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOpp = Z_RenewalTestFactory.makeOpportunity2(testAcc);

        testAcc.QlikTech_Company__c = testQTComp.name;
        testAcc.Billing_Country_Code__c  = testQTComp.id;
        testAcc.Signature_Success_Entitled__c = TRUE; 
        testAcc.Signature_Success_End_Date__c = System.Today(); 

        update testAcc;

        List<sObject> tests = new List<sObject>{
            testQ2CWSettings, testSteelbrickSettings
        };

        insert tests;

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOpp
        };

        insert testObjects;

        testQuote = Z_RenewalTestFactory.makeQuote3(testAcc, testContact, testOpp, testCustomerAcc);
        testQCD = Z_RenewalTestFactory.makeQCD(testQuote);
        testQCD.Name = 'SSTestQCD';

        insert testQCD;

        Organization_Info__c orgInfo = new Organization_Info__c(isSandbox__c=true);
        insert orgInfo;

        orgInfo.CreatedInOrgId__c = orgInfo.Id;
        update orgInfo;

    }

    @isTest
    static void OppClosedLostSignatureUpdateTest() {

        setup();

        Semaphores.SetAllSemaphoresToFalse();
        Test.startTest();

        System.AssertEquals(testAcc.Signature_Success_Entitled__c, TRUE, 'Pre-test - Signature Success Entitled was not set to true');

        Semaphores.SetAllSemaphoresToFalse();

        testOpp.StageName = 'Closed Lost';
        testOpp.Primary_reason_lost__c = 'QlikTech cannot provide a solution';
        update testOpp;
        

        System.debug('This is the test opportunity stage: ' + testOpp.StageName);
        System.debug('This is the test account end date: ' + testAcc.Signature_Success_End_Date__c);

        Account testAcc2 = [SELECT Id, Signature_Success_End_Date__c, Signature_Success_Entitled__c
                            FROM Account
                            WHERE Id  = :testOpp.AccountId];

        System.AssertEquals(testAcc2.Signature_Success_Entitled__c, FALSE, 'Signature Success Entitled was not set to false'); 
        System.AssertEquals(testAcc2.Signature_Success_End_Date__c, null, 'Signature Success End Date was not set to null' );

        Test.stopTest();

    }
}