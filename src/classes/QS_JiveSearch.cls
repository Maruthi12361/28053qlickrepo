/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_JiveSearch extends QS_Search {
    
    public static string JSON_LIST = 'list';
    public static string JSON_SUBJECT = 'subject';
    public static string JSON_RESOURCES = 'resources';
    public static string JSON_RESOURCES_HTML = 'html';
    public static string JSON_RESOURCES_HTML_URL = 'ref';
    public static string JSON_PUBLISHED = 'published';
    public static string JSON_CONTENT = 'content';
    public static string JSON_CONTENT_TEXT = 'text';
    public static final integer DESCRIPTION_CHARS = 200;
    public static final string JSON_HEADER = 'throw \'allowIllegalResourceCall is false.\';';
   
    private string serviceEndpoint;
    private integer resultSize = 0;
    private boolean personasBased = false;
    private string jivePlaces;
    private boolean responseError;
    
    public QS_JiveSearch(string searchTerms, integer NumberItems, boolean personasBased) {

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        responseError = false;
        
        if (personasBased) {

            serviceEndPoint =  searchSettings[0].Jive_EndPoint_Personas__c;
            boolean unauthenticated = false;
            User userRec;
            //todo factor into common class
            if(userinfo.getUserId() != null)
                userRec = [Select Id, ContactId, AccountId, Contact.Persona__c 
                           From User where Id = :UserInfo.getUserId()];
            
            if(userRec == null || userRec.ContactId == null || userRec.AccountId == null)
                unauthenticated = true;
            else
                unauthenticated = false;
            
            if(!unauthenticated && userRec != null && userRec.ContactId != null && userRec.Contact.Persona__c != null) {
                string[] personas = userRec.Contact.Persona__c.split(';');
                string persona = '';

                if(personas.size() > 1) {
                    Double rnd =  Math.random();
                    integer index = (integer)(rnd * personas.size());
                    persona = personas[index];
                }
                else
                    persona = personas[0];
                //QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(userRec.Contact.Persona__c);
                system.debug('Persona in JiveSearch: ' + persona);
                QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(persona);
                jivePlaces = categoryRecord != null ? categoryRecord.Community_Forums__c : '';
            } else {
                jivePlaces = searchSettings[0].Jive_DefaultPlaces__c;
            }
            serviceEndPoint = serviceEndPoint.replace('placeTerms', jivePlaces);
        }
        else {
           serviceEndpoint = searchSettings[0].Jive_EndPoint__c;  
        }

       if (searchTerms != null && searchTerms.length() > 1 ) {
                searchTerms = searchTerms.replace(' ', '+');
                serviceEndpoint = serviceEndpoint.replace('searchTerms', searchTerms);
        }

        //set page size
        if (numberItems == 0) {
            pageSize = (integer)searchSettings[0].Page_Size__c;
            
        }
        else {
            pageSize = numberItems;
        } 
      
        serviceEndpoint = serviceEndpoint.replace('pageSize', pageSize.format());
        
        //set default size and index
        startIndex = 0;
        totalSize = 0; 
        doSearch();
        totalPages = getTotalPages();
    }
    
    public override void doSearch() {
                   
            string nextSearchUrl = serviceEndpoint.replace('=index', '=' + startIndex.format());
            string response;
            String responseBody = callJiveWebService(nextSearchUrl);
            if(!responseError)
                response = responseBody.replace(JSON_HEADER, '');
            else
                response = responseBody;

            if(!responseError) {
                //de-serialise response          
                Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response);
                
                //get list of search result elements
                List<Object> resultList = (List<Object>)m.get(JSON_LIST);
                resultSize = (resultList != null && !resultList.isEmpty()) ? resultList.size() : 0;
                
                //populate into list of search wrappers           
                searchResults = new List<QS_SearchWrapper>();
                totalSize = resultSize;
                system.debug('~~~~~~~~~~~~~ Inside Jive Search - resultSize='+resultSize );

                for(integer i = 0; i < resultSize; i++) {
                    
                    QS_SearchWrapper w = new QS_SearchWrapper();
                    
                    //get top level elements
                    Map<String, Object> r1 = (Map<String, Object>)resultList[i];
                    w.title = (string)r1.get(JSON_SUBJECT);
                    system.debug('~~~~~~~~~~~~~ Inside Jive Search - w.title='+w.title );
                    if (r1.get(JSON_PUBLISHED) != null) {
                        system.debug('~~~~~~~~~~~~~ Inside Jive Search - inside published' );
                        string jiveDate = ((string)r1.get(JSON_PUBLISHED)).replace('T', ' ').substring(0,19);
                        w.dateAdded = Datetime.valueOf(jiveDate).format('dd MMMM yyyy');  
                        w.dateAddedDT = Datetime.valueOf(jiveDate);
                    }
                    
                    //get post description
                    Map<String, Object> jiveConentMap = (Map<String, Object>)r1.get(JSON_CONTENT);
                    system.debug('~~~~~~~~~~~~~ Inside Jive Search - jiveConentMap='+jiveConentMap );
                    if (r1.get(JSON_CONTENT) != null) {
                        
                        w.description = ((string)jiveConentMap.get(JSON_CONTENT_TEXT)).stripHtmlTags().normalizeSpace();
                        system.debug('~~~~~~~~~~~~~ Inside Jive Search - w.description='+w.description );
                        if (w.description.length() > DESCRIPTION_CHARS) {
                            w.description = w.description.substring(0, DESCRIPTION_CHARS);
                        }
                    }
                    
                    //get post url
                    Map<String, Object> jiveResources = (Map<String, Object>)r1.get(JSON_RESOURCES);
                    Map<String, Object> jiveHtml = (Map<String, Object>)jiveResources.get(JSON_RESOURCES_HTML);
                    w.url = (string)jiveHtml.get(JSON_RESOURCES_HTML_URL);
                    
                    searchResults.add(w);
                }
            } else {
                searchResults = null;
            }
    }
    
    private string callJiveWebService(string searchUrl) {
        // Retrieving the custom setting record to check if we need to invoke the web service call
        QS_Search_Setting__c jiveAPICallSettings = QS_Search_Setting__c.getInstance('Jive_Search');
        if(jiveAPICallSettings != null && jiveAPICallSettings.Active__c != null && jiveAPICallSettings.Active__c == true) {

            //make call to Jive
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(searchUrl);
            req.setMethod('GET');
            system.debug('~~~~~~~~~~~~~ Inside Jive Search - before call web service' );
            HttpResponse res = http.send(req);
            
            // setting the return message if the web service call is not successful
            String returnMsg = '';
            responseError = false;
            system.debug('~~~~~~~~~~~~~ Inside Jive Search - callJiveWebService -- if' );
            system.debug('~~~~~~~~~~~~~ Inside Jive Search - response.Status: ' + res.getStatus());
            if(res.getStatusCode() >= 300 && res.getStatusCode() != 306 && res.getStatusCode() != 308) {
                system.debug('~~~~~~~~~~~~~ Inside Jive Search - callJiveWebService -- if - inside error' );
                responseError = true;
                returnMsg = System.Label.QS_Jive_Search_Inactive_Message;
                return returnMsg;
            }
            system.debug('Return message: ' + res.getBody());
            return res.getBody();
        } else {
            system.debug('~~~~~~~~~~~~~ Inside Jive Search - callJiveWebService -- else' );
            // setting the return message if the web service is not invoked
            responseError = true;
            String returnMsg = System.Label.QS_Jive_Search_Inactive_Message;
            return returnMsg;
        }
    }
    
    public override Integer getTotalPages() {
        try{
            system.debug('~~~~~~~ totalSize:' +totalSize + '  pageSize:' +  pageSize);
                if (math.mod(totalSize, pageSize) > 0) 
                {
                    return totalSize/pageSize + 1;
                } 
                else 
                {
                    return (totalSize/pageSize);
                }
            }
        catch(Exception e){system.debug('~~~~~~~ error' );return 1; }
    }
    
    public override Integer getPageNumber() {
        return 1;
    }
    
    public override Boolean getDisableNext() { 
        if (resultSize < pageSize) return true; else return false;
    }
}