/*
04 oct 2017 - QCW-2849 Shubham Gupta  Created Test Class to cover all scnearios for OpplineItems creation on deal split and also updation
of opplineItems if quote Lines are updated.
*/

@isTest
private class OppDealSplitScheduleHandlerTest {

    @testSetup static void setup(){
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
            Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            //RecordType rectype = [Select id From RecordType where DeveloperName ='OEM_Partner_Account' limit 1];
            //system.debug('rectype is '+rectype);
            //testAccount.RecordtypeId = rectype.id;

            Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
        Product2 productForTest1 = QuoteTestHelper.createProduct('QlikView Enterprise Education Server', 'Education', 1, 'QlikView', 'QlikView');
        Product2 DummyLiscenseRen = QuoteTestHelper.createProduct('Licenses Renewable', 'Licenses', 1, 'QlikView', 'QlikView');
        Product2 DummyEducationRen = QuoteTestHelper.createProduct('Education Renewable', 'Education', 1, 'QlikView', 'QlikView');
        DummyLiscenseRen.productCode = 'Licenses';
        DummyEducationRen.productCode = 'Education';
            List<product2> prodtoInsert = new List<Product2>();
            prodtoInsert.add(productForTest);
            prodtoInsert.add(DummyLiscenseRen);
            prodtoInsert.add(productForTest1);
            prodtoInsert.add(DummyEducationRen);
            insert prodtoInsert;

            String priceBookId = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Standard_PriceBook__c;
            System.debug(logginglevel.WARN, 'PBId is '+priceBookId);

            PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, priceBookId);
            pbEntryTest.CurrencyIsoCode = 'USD';
            PricebookEntry pbEntryDummy = QuoteTestHelper.createPriceBookEntry(DummyLiscenseRen.id, priceBookId);
            pbEntryDummy.CurrencyIsoCode = 'USD';
            PricebookEntry pbEntryTest1 = QuoteTestHelper.createPriceBookEntry(productForTest1.id, priceBookId);
            pbEntryDummy.CurrencyIsoCode = 'USD';
            PricebookEntry pbEntryDummy1 = QuoteTestHelper.createPriceBookEntry(DummyEducationRen.id, priceBookId);
            pbEntryDummy.CurrencyIsoCode = 'USD';
            List<PricebookEntry> pbtoInsert = new List<PricebookEntry>();
            pbtoInsert.add(pbEntryTest);
            pbtoInsert.add(pbEntryDummy);
            pbtoInsert.add(pbEntryTest1);
            pbtoInsert.add(pbEntryDummy1);
            insert pbtoInsert;

        Opportunity oppCreatedForUpdate1 = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        oppCreatedForUpdate1.name = 'parentopty';
        oppCreatedForUpdate1.Split_Opportunity__c = true;
        insert oppCreatedForUpdate1;
            
        OpportunityLineItem createdLineItem = QuoteTestHelper.createLineItem(oppCreatedForUpdate1.id, pbEntryTest);
        insert createdLineItem; 
    }
}

	@isTest static void childOppCreated() {
		
        Semaphores.OpportunityTriggerAfterInsert = false;
        Account acct = [SELECT Id FROM Account LIMIT 1];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE Accountid = :acct.id limit 1];
        
        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(acct, '', [Select id From RecordType where DeveloperName ='Deal_Split_Child']);
        //oppCreatedForUpdate.License_Forecast_Amount__c = 25;
        oppCreatedForUpdate.name = 'Dealsplitopp';
        oppCreatedForUpdate.Deal_Split_Parent_Opportunity__c = oppty.Id;
        oppCreatedForUpdate.Deal_Split_Percentage__c = 10;

        Test.startTest();
        insert oppCreatedForUpdate;
        Test.stopTest();

        List<OpportunityLineItem> lineCreated = [select id,Product2.name,Opportunity.name from OpportunityLineItem where Opportunity.id = :oppCreatedForUpdate.id];
        System.debug(logginglevel.WARN, 'size of lineitem is '+lineCreated.size());
        system.debug(logginglevel.WARN,'lineitem is '+lineCreated);
        System.assertEquals(false,lineCreated.isEmpty());

        }

    @isTest static void updateQuoteLines(){
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
         System.runAs(testUser) {
        Semaphores.OpportunityTriggerAfterInsert = false;
        Semaphores.SplitDealOpptyProduct = false;
        Account acct = [SELECT Id FROM Account LIMIT 1];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE Accountid = :acct.id limit 1];
        
        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(acct, '', [Select id From RecordType where DeveloperName ='Deal_Split_Child']);
        //oppCreatedForUpdate.License_Forecast_Amount__c = 25;
        oppCreatedForUpdate.name = 'Dealsplitopp';
        oppCreatedForUpdate.Deal_Split_Parent_Opportunity__c = oppty.Id;
        oppCreatedForUpdate.Deal_Split_Percentage__c = 10;

        //Product2 pr2 = [select id from Product2 where Name = 'QlikView Enterprise Education Server' AND Family = 'Education'];
        //PricebookEntry pbEnt = [select id,UnitPrice from PricebookEntry where Pricebook2Id = '01s20000000E0PWAA0' AND Product2Id = :pr2.id];

        Test.startTest();
        insert oppCreatedForUpdate;

        Semaphores.SplitDealOpptyProduct = false;

        //OpportunityLineItem createdLineItem1 = QuoteTestHelper.createLineItem(oppty.id, pbEnt);
        //insert createdLineItem1;
        OpportunityLineItem lineToUpdate = [select id,Quantity from OpportunityLineItem where opportunityID = :oppty.id];
        lineToUpdate.quantity = 10;
        update lineToUpdate; 
        Test.stopTest();
        }
    }


    @isTest static void deleteQuoteLines(){
        Account acct = [SELECT Id FROM Account LIMIT 1];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE Accountid = :acct.id limit 1];
        List<OpportunityLineItem> lineCreated = [select id,Product2.name,Opportunity.name from OpportunityLineItem where Opportunity.id = :oppty.id];

        Test.startTest();
        delete lineCreated;
        Test.stopTest();
    }

	private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr, 
                                            Country_Name__c = countryName, 
                                            Subsidiary__c = subsId);
        return qlikTechIns;
	}
}