/*********************************************
Change Log:
2016-02-19  NAD     Removed Support_Level__c and Support_Per__c references per CR# 33068 (Responsible Partner change)
2016-06-29  AIN     Using Lund Support Office instead of Dusseldorf as it is being deactivated for CR# 88519
07.02.2017  RVA :   changing methods
30.03.2017  UIN :   Test Coverage increase
19.04.2017  UIN :   Test logic change and coverage as per new functionality
11.10.2017  ext_bad\ext_vos
                    CR:CHG0032086 - Add test cases for Entitlments.BusinessHoursId and Entitlments.Difference_Exists__c fields.
05.07.2019  ext_bad IT-1995   Add Subscription__c sync
26.12.2019  extbad  IT-2363   Added Signature Support Entitlement Process
17.02.2020  ext_bad IT-2497   Added Legacy Enterprise Support Entitlement Process
********************************************/
@IsTest
private class EntitlementSyncBatch24hTest {

    private static Id standardSupportSlaProcessId;
    private static String standardSupportLevel;
    private static String premiumSupportLevel;
    private static Id twentyFourSevenBusinessHoursId;
    private static String firstResponseEmailPhrase;
    private static String communicationEmailPhrase;
    private static String qlikTectCompanyName;
    private static Id qlikTectCompanyId;

    @TestSetup
    static void createTestData() {
        QTTestUtils.GlobalSetUp();
        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        System.assert(checkOrgSettings(es), 'Org Not Configured for Entitlements');

        QlikTech_Company__c qtCompany = createAndInsertQTCompany();
        qlikTectCompanyName = qtCompany.QlikTech_Company_Name__c;
        qlikTectCompanyId = qtCompany.Id;
    }

    static testMethod void testEntitlementSyncBatch24h() {
        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest(); //& batch executes following stopTest.
    }

    static testMethod void testEntitlementSyncBatch24h_Sch() {
        Test.startTest();
        EntitlementSyncBatch24h sh1 = new EntitlementSyncBatch24h();
        String sch = '0 0 23 * * ?';
        System.schedule('Test Schedule method', sch, sh1);
        Test.stopTest();
    }

    /*
    ​​​​Verify that When the 'StartDate'(or 'EndDate'), 'SLA_Process_Name__c' or 'Subscription__c' fields are updated manually
                then the Product licenses record will be updated by Job.
    */
    static testMethod void testEntitlementDifferenceExists_TC001() {
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney'), false, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0011111111111111', account, null, sub);

        Entitlement testEntOne = [
                SELECT Difference_Exists__c, Difference_Exists_Support_Office__c, Difference_Exists_Support_Level__c, StartDate, BusinessHoursId
                FROM Entitlement
                WHERE Account_License__c = :license.Id
        ];
        System.assert(!testEntOne.Difference_Exists__c);
        System.assertEquals(bhSydneyId, testEntOne.BusinessHoursId);
        System.assertEquals(license.Support_From__c, testEntOne.StartDate);

        Test.startTest();
        // mannualy updates for Entitlement records
        testEntOne.StartDate = Date.today().addDays(-100);
        update testEntOne;
        Test.stopTest();

        // check: if Difference_Exists__c = true, record will be updated by Job
        testEntOne = [SELECT Difference_Exists_Support_Date__c FROM Entitlement WHERE Id = :testEntOne.Id];
        System.assert(testEntOne.Difference_Exists_Support_Date__c);

        testEntOne.SLA_Process_Name__c = 'Enterprise';
        update testEntOne;

        testEntOne = [SELECT Difference_Exists_Support_Level__c FROM Entitlement WHERE Id = :testEntOne.Id];
        System.assert(testEntOne.Difference_Exists_Support_Level__c);

        testEntOne.Subscription__c = null;
        update testEntOne;

        testEntOne = [SELECT Difference_Exists_Subscription__c FROM Entitlement WHERE Id = :testEntOne.Id];
        System.assert(testEntOne.Difference_Exists_Subscription__c);
    }

    /*
    ​​​​Verify that When the Account License is invalId (Support_To__c < Support_FROM__c)
                then the Product licenses record will NOT be updated by Job.
    */
    static testMethod void testEntitlementDifferenceExists_TC002() {
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(es.Premium_Support_Level__c, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), false, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0011111111111111', account, null, 'Enterprise', sub);

        Entitlement testEnt = [SELECT Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        Test.startTest();
        // mannualy updates for Entitlement
        license.Support_To__c = Date.today().addDays(-100);
        license.Support_From__c = Date.today().addDays(-10);
        update license;
        Test.stopTest();

        // check: if Difference_Exists__c = false, record will NOT be updated by Job
        testEnt = [SELECT Difference_Exists__c FROM Entitlement WHERE Id = :testEnt.Id];
        System.assert(!testEnt.Difference_Exists__c);
    }

    static testMethod void testSupportLevel_TC001() {
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney'), false, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0011111111111111', account, null, sub);

        Entitlement testEntOne = [
                SELECT Difference_Exists__c, SLA_Process_Name__c
                FROM Entitlement
                WHERE Account_License__c = :license.Id
        ];

        System.assert(!testEntOne.Difference_Exists__c);
        System.assert(!testEntOne.SLA_Process_Name__c.contains('Signature'));

        license = [SELECT Id, Signature_End_Date__c FROM Account_License__c WHERE Id = :license.Id];
        license.Signature_End_Date__c = Date.today().addDays(20);
        update license;

        testEntOne = [SELECT Difference_Exists__c, Difference_Exists_Support_Level__c, SLA_Process_Name__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEntOne.Difference_Exists_Support_Level__c);
        System.assert(testEntOne.Difference_Exists__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        testEntOne = [SELECT SLA_Process_Name__c FROM Entitlement WHERE Id = :testEntOne.Id];
        System.assert(testEntOne.SLA_Process_Name__c.contains('Signature'));
    }

    static testMethod void testSupportLevel_TC002() {
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney'), false, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0011111111111111', account, null, sub);

        Entitlement testEntOne = [
                SELECT Difference_Exists__c, StartDate, BusinessHoursId, SLA_Process_Name__c
                FROM Entitlement
                WHERE Account_License__c = :license.Id
        ];
        System.assert(!testEntOne.Difference_Exists__c);
        System.assert(!testEntOne.SLA_Process_Name__c.contains('Signature'));

        account.Premier_Support__c = true;
        update account;
        testEntOne = [SELECT Difference_Exists__c, Difference_Exists_Support_Level__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEntOne.Difference_Exists__c);
        System.assert(testEntOne.Difference_Exists_Support_Level__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        testEntOne = [SELECT SLA_Process_Name__c FROM Entitlement WHERE Id = :testEntOne.Id];
        System.assert(testEntOne.SLA_Process_Name__c.contains('Signature'));
    }

    static testMethod void testSupportLevel_TC003() {
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney'), false, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0011111111111111', account, null, sub);

        Entitlement testEntOne = [
                SELECT Difference_Exists__c, StartDate, BusinessHoursId, SLA_Process_Name__c
                FROM Entitlement
                WHERE Account_License__c = :license.Id
        ];
        System.assert(!testEntOne.Difference_Exists__c);
        System.assert(!testEntOne.SLA_Process_Name__c.contains('Legacy'));

        license.Legacy_Enterprise_End_Date__c = Date.today().addDays(20);
        update license;
        testEntOne = [SELECT Difference_Exists__c, Difference_Exists_Support_Level__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEntOne.Difference_Exists__c);
        System.assert(testEntOne.Difference_Exists_Support_Level__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        testEntOne = [SELECT SLA_Process_Name__c FROM Entitlement WHERE Id = :testEntOne.Id];
        System.assert(testEntOne.SLA_Process_Name__c.contains('Legacy'));
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is checked and BH matches support level
                then the Business Hours is not updated in the Product licenses.
    */
    static testMethod void testBusinessHours_TC001() {
        Id bh24Id = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhLundId, bh24Id, 'Lund'), true, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0111111111111111', account, null, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        testEnt.BusinessHoursId = bhSydneyId;
        update testEnt;

        testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was not updated
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhSydneyId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is checked, BH are not equals to account and support level is changed to Enterprise
                then the Business Hours is not updated in the Product license.
    */
    static testMethod void testBusinessHours_TC002() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;
        createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney');

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), true, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0021111111111111', account, null, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        testEnt.BusinessHoursId = bhSydneyId;
        update testEnt;

        license = [SELECT Id, INT_NetSuite_Support_Level__c FROM Account_License__c WHERE Id = :license.Id];
        license.INT_NetSuite_Support_Level__c = 'Enterprise';
        update license;

        testEnt = [SELECT Difference_Exists_Office_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists_Office_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was not updated to Enterprise
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhSydneyId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is checked, BH are not equals to account and don't match support level
                then the Business Hours is updated in the Product licenses.
    */
    static testMethod void testBusinessHours_TC003() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;
        createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney');

        Account account = createAndInsertAccount('Enterprise', createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), true, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0031111111111111', account, null, 'Enterprise', sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        testEnt.BusinessHoursId = bhSydneyEntId;
        update testEnt;

        testEnt = [SELECT Difference_Exists_Office_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEnt.Difference_Exists_Office_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was updated to Basic
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhSydneyId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is not checked, BH are not equals to account, level is Enterprise
                then the Business Hours is updated in the Product licenses.
    */
    static testMethod void testBusinessHours_TC004() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;

        Account account = createAndInsertAccount('Enterprise', createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), false, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0041111111111111', account, null, 'Enterprise', sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        testEnt.BusinessHoursId = bhSydneyEntId;
        update testEnt;

        testEnt = [SELECT Difference_Exists_Office_Not_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEnt.Difference_Exists_Office_Not_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was updated
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhLundId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is not checked, BH are not equals to account, level is Basic
                then the Business Hours is updated in the Product licenses.
    */
    static testMethod void testBusinessHours_TC005() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), false, false);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0051111111111111', account, null, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        testEnt.BusinessHoursId = bhSydneyId;
        update testEnt;

        testEnt = [SELECT Difference_Exists_Office_Not_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEnt.Difference_Exists_Office_Not_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was updated
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhLundId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is not checked, BH are equals to account Ent, level is Enterprise, Process is Signature
                then the Business Hours should be updated in the Product licenses to the local office.
    */
    static testMethod void testBusinessHours_TC006() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Premium_Support_Level__c != null) {
            premiumSupportLevel = es.Premium_Support_Level__c;
        }
        System.assertNotEquals(null, premiumSupportLevel);

        Account account = createAndInsertAccount(premiumSupportLevel, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), false, true);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0051111111111111', account, null, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);

        testEnt.BusinessHoursId = bhLundEntId;
        update testEnt;

        testEnt = [SELECT Difference_Exists_Office_Not_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEnt.Difference_Exists_Office_Not_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was updated to the Basic
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhLundId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is checked, BH are not equals to account, level is Basic, Process is Signature
                then the Business Hours should not be updated in the Product licenses.
    */
    static testMethod void testBusinessHours_TC007() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), true, true);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0051111111111111', account, null, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        testEnt.BusinessHoursId = bhSydneyId;
        update testEnt;

        testEnt = [SELECT Difference_Exists__c, Difference_Exists_Office_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);
        System.assert(!testEnt.Difference_Exists_Office_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was not updated
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhSydneyId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is checked, BH are not equals to account, BH is Enterprise, Process is Signature
                then the Business Hours should be updated in the Product licenses to the local office.
    */
    static testMethod void testBusinessHours_TC008() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;
        createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney');

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), true, true);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0051111111111111', account, null, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        testEnt.BusinessHoursId = bhSydneyEntId;
        update testEnt;

        testEnt = [SELECT Difference_Exists_Office_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEnt.Difference_Exists_Office_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was updated
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhSydneyId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is not checked, BH are equals to account Ent, level is Enterprise, Process is Legacy even if SignatureSupport is true
                then the Business Hours should be updated in the Product licenses to the enterprise office.
    */
    static testMethod void testBusinessHours_TC009() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Premium_Support_Level__c != null) {
            premiumSupportLevel = es.Premium_Support_Level__c;
        }
        System.assertNotEquals(null, premiumSupportLevel);

        Account account = createAndInsertAccount(premiumSupportLevel, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), false, true);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0051111111111111', account, null, premiumSupportLevel, true, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c, SLA_Process_Name__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(!testEnt.Difference_Exists__c);
        System.assert(testEnt.SLA_Process_Name__c.contains('Legacy'));

        testEnt.BusinessHoursId = bhLundId;
        update testEnt;

        testEnt = [SELECT Difference_Exists_Office_Not_Global__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEnt.Difference_Exists_Office_Not_Global__c);

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was updated to the Enterprise
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhLundEntId, testEnt.BusinessHoursId);
    }

    /*
    ​​​​Verify that When the 'Global Support Account' is checked, BH are not equals to account, BH is Local, Process is Legacy
                then the Business Hours should be updated in the Product licenses to the enterprise office.
    */
    static testMethod void testBusinessHours_TC0010() {
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhSydneyId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney' LIMIT 1].Id;
        Id bhSydneyEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Sydney Enterprise (8x7)' LIMIT 1].Id;
        createAndInsertSupportOffice(bhSydneyId, bhSydneyEntId, 'Sydney');

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        if (es.Standard_Support_Level__c != null) {
            standardSupportLevel = es.Standard_Support_Level__c;
        }
        System.assertNotEquals(null, standardSupportLevel);

        Account account = createAndInsertAccount(standardSupportLevel, createAndInsertSupportOffice(bhLundId, bhLundEntId, 'Lund'), true, true);
        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(account, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.makeSubscription(bill, account, 'testSub');
        insert sub;
        Account_License__c license = createAndInsertAccountLicense('0051111111111111', account, null, standardSupportLevel, true, sub);

        Entitlement testEnt = [SELECT BusinessHoursId, Difference_Exists__c FROM Entitlement WHERE Account_License__c = :license.Id];
        testEnt.BusinessHoursId = bhSydneyId;
        update testEnt;

        testEnt = [SELECT Difference_Exists_Office_Global__c, SLA_Process_Name__c FROM Entitlement WHERE Account_License__c = :license.Id];
        System.assert(testEnt.Difference_Exists_Office_Global__c);
        System.assert(testEnt.SLA_Process_Name__c.contains('Legacy'));

        Test.startTest();
        Database.executeBatch(new EntitlementSyncBatch24h());
        Test.stopTest();

        // check BH - was updated
        testEnt = [SELECT BusinessHoursId FROM Entitlement WHERE Id = :testEnt.Id];
        System.assertEquals(bhSydneyEntId, testEnt.BusinessHoursId);
    }

    private static Boolean checkOrgSettings(Entitlement_Settings__c es) {
        try {
            if (es.Standard_Support_Level__c == null) return false;
            standardSupportLevel = es.Standard_Support_Level__c;
            premiumSupportLevel = es.Premium_Support_Level__c;

            standardSupportSlaProcessId = [SELECT Id FROM SlaProcess WHERE Name = :es.Standard_Entitlement_Process_Name__c LIMIT 1].Id;
            twentyFourSevenBusinessHoursId = [SELECT Id FROM BusinessHours WHERE Name = :es.X24x7_Business_Hours_Name__c LIMIT 1].Id;

            firstResponseEmailPhrase = es.Email_Message_Phrase_First_Response__c;
            communicationEmailPhrase = es.Email_Message_Phrase_Communication__c;

            return true;
        } catch (Exception ex) {
            System.debug('=======>' + ex);
            return false;
        }
        return false;
    }

    private static Support_Office__c createAndInsertSupportOffice(Id businessHours, Id enterpriseBusinessHours, String name) {
        Id pId = [SELECT Id FROM Profile WHERE Name LIKE 'System Administrator' LIMIT 1].Id;
        User johnBrown = [SELECT Id FROM User WHERE ProfileId = :pId AND IsActive = TRUE LIMIT 1];

        Support_Office__c supportOffice =
                new Support_Office__c(Name = name, Regional_Support_Manager__c = johnBrown.Id,
                        Business_Hours__c = businessHours, Enterprise_Business_Hours__c = enterpriseBusinessHours);
        insert supportOffice;
        return supportOffice;
    }

    private static QlikTech_Company__c createAndInsertQTCompany() {
        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'United States', 'United States');
        return [SELECT Id, Name, QlikTech_Company_Name__c FROM QlikTech_Company__c WHERE Id = :QTComp.Id];
    }

    private static Account createAndInsertAccount(String supportLev, Support_Office__c office, Boolean globalAccount, Boolean signatureSupport) {
        Account acc = new Account(Name = 'new account' + System.now(), Global_Support_Account__c = globalAccount,
                Support_Office__c = office != null ? office.Id : null,
                Support_Level__c = supportLev, Premier_Support__c = signatureSupport,
                QlikTech_Company__c = qlikTectCompanyName, Billing_Country_Code__c = qlikTectCompanyId);
        insert acc; return acc;
    }

    private static Account_License__c createAndInsertAccountLicense(String uniqueNumber, Account a, Account_License_Group__c alg,
            Zuora__Subscription__c sub) {
        Account_License__c al =
                new Account_License__c(Name = uniqueNumber, Product__c = 'QSERVER', Account__c = a.Id,
                        INT_NetSuite_Support_Level__c = 'Basic',
                        Support_From__c = Date.today().addDays(-10), Support_To__c = Date.today().addDays(100),
                        O_S__c = 'Windows 3.1', Environment__c = 'MS Windows', Application__c = 'QVSERVER',
                        Account_License_Group__c = alg != null ? alg.Id : null, Subscription__c = sub.Id);
        insert al; return al;
    }

    private static Account_License__c createAndInsertAccountLicense(String uniqueNumber, Account a, Account_License_Group__c alg,
            String level, Zuora__Subscription__c sub) {
        Account_License__c al =
                new Account_License__c(Name = uniqueNumber, Product__c = 'QSERVER', Account__c = a.Id,
                        INT_NetSuite_Support_Level__c = level,
                        Support_From__c = Date.today().addDays(-10), Support_To__c = Date.today().addDays(100),
                        O_S__c = 'Windows 3.1', Environment__c = 'MS Windows', Application__c = 'QVSERVER',
                        Account_License_Group__c = alg != null ? alg.Id : null, Subscription__c = sub.Id);
        insert al; return al;
    }

    private static Account_License__c createAndInsertAccountLicense(String uniqueNumber, Account a, Account_License_Group__c alg,
            String level, Boolean legacy, Zuora__Subscription__c sub) {
        Account_License__c al =
                new Account_License__c(Name = uniqueNumber, Product__c = 'QSERVER', Account__c = a.Id,
                        INT_NetSuite_Support_Level__c = level,
                        Support_From__c = Date.today().addDays(-10), Support_To__c = Date.today().addDays(100),
                        O_S__c = 'Windows 3.1', Environment__c = 'MS Windows', Application__c = 'QVSERVER',
                        Account_License_Group__c = alg != null ? alg.Id : null, Subscription__c = sub.Id);
        if (legacy) {
            al.Legacy_Enterprise_End_Date__c = Date.today().addDays(20);
        }
        insert al; return al;
    }
}