/**
*  27.06.2017 : Nikhil  QCW-2749-Displaying appropriate error mesages based on Order Form Status values
*  05.07.2017 : Nikhil  QCW-2749-Changed the verbiage of error message
**/
public with sharing class QuoteExtController {
 
    private Id quoteId;
    public  Boolean isEmpty{get;set;}
    public SBQQ__Quote__c quote {get; set;}
    public SBAA__Approval__c[] approvals {get; set;}
    public ApexPages.StandardController con;
    
    public QuoteExtController(ApexPages.StandardController stdController) {
        quoteId = stdController.getId();
        quote = (SBQQ__Quote__c)stdController.getRecord();
        con =stdController;
    }
    
    public PageReference onSubmit() {
        if (quoteId != null) {
            SBAA.ApprovalAPI.submit(quoteId, SBAA__Approval__c.Quote__c);
        }
        return null;
    }
    
    public PageReference onRecall(){
        Set<String> setOfStatuses = new Set<String>{'None',NULL,'Quote Approvals Needed','Canceled','Rejected', 'Expired','Failed'};
        if (quoteId != null && setOfStatuses.contains(quote.Order_Form_Status__c) && quote.SBQQ__Status__c != 'Contracting') {
            SBAA.ApprovalAPI.recall(quoteId, SBAA__Approval__c.Quote__c);
            return new PageReference('/' + quoteId);
        }else{
            string message = '';
            if(quote.SBQQ__Status__c == 'Contracting'){
                message = 'This quote cannot be recalled because there is an order form already in progress. Please cancel the order form to be able to recall the quote.';
            }else if(quote.Order_Form_Status__c == 'Out For Signature'){
                message = 'There is an Order Form currently out for signature for this Quote. If you need to make changes to the Quote, you must first cancel the Order Form signature. To do so, open the Order Form in the "In Process" folder then select Send, For Signature, Cancel.';
            }else if(quote.Order_Form_Status__c == 'External Review'){
                message = 'There is an Order Form in process for this Quote.   If you need to make changes to the Quote, you must first recall the review and cancel the Order Form.<br/>'+
                          'To do so:  <br/>'+
                          '&nbsp;&nbsp;   (1) open the Order Form in the "In Process" folder <br/>'+
                          '&nbsp;&nbsp;   (2) select Send, External Review Actions, Cancel External Review <br/>'+
                          '&nbsp;&nbsp;   (3) return to Salesforce and open the document again  <br/>'+
                          '&nbsp;&nbsp;   (4) start my task then Cancel Workflow ';
            }else if(quote.Order_Form_Status__c == 'Sales Review'){
                message = 'An Order Form is currently in process and waiting for your action. Please go to the "In Process" folder and make a choice there.  If you need to reconfigure your Quote, you may select Cancel Workflow.';
            }else if(quote.Order_Form_Status__c == 'Legal Review'){
                message = 'There is an Order Form in process for this Quote.  The Order Form is currently with Legal for redlines. If you need to recall or have questions about your Order Form, reach out to Legal.';
            }else if(quote.Order_Form_Status__c == 'Orders Review'){
                message = 'There is an Order Form in process for this Quote.  The Order Form is currently with Orders for approval. If you need to recall or have questions about your Order Form, reach out to Orders.';
            }else if(quote.Order_Form_Status__c == 'Orders Approval'){
                message = 'There is an Order Form in process for this Quote.  The Order Form is currently with Orders for approval. If you need to recall or have questions about your Order Form, reach out to Orders.';
            }else if(quote.Order_Form_Status__c == 'Fully Executed'){
                message = 'This opportunity is "Closed Won" and the Quote approvals cannot be recalled. ';
            }else{
                message = 'Message not defined for this order form status.';
            }
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
            displayPopup = true;
            return NULL;
        }
        return NULL;
    }
    
    public void ApproverExist(){
        List<sbaa__Approval__c > approvalList = SBAA.ApprovalAPI.preview(quoteId, SBAA__Approval__c.Quote__c);
        if(approvalList.isEmpty()){
            isEmpty = true;
        }
        else{
            isEmpty = false;
        }
    } 

    public PageReference saveAndSubmit(){
        con.save();
        onSubmit();        
        return null;
    }
    
    public PageReference onPreview() {
        if (quoteId != null) {
            approvals = SBAA.ApprovalAPI.preview(quoteId, SBAA__Approval__c.Quote__c);
        }
        return null; 
    }
    
    public boolean displayPopup {get; set;}
    public PageReference returnToQuote(){        
        PageReference quoteDetail = new PageReference('/'+quote.Id);
        quoteDetail.setRedirect(true);
        return quoteDetail;
    }
}