public class Z_QuoteChargeDetailTriggerHandler {
    public static void updateChargeSummaryMetrics(List<zqu__QuoteChargeDetail__c> details){
        Set<Id> quoteIds = new Set<Id>();
        Map<String, zqu__QuoteChargeDetail__c> detailMap = new Map<String, zqu__QuoteChargeDetail__c>();
        for (zqu__QuoteChargeDetail__c detail : details){
            system.debug('charge name: ' + detail.Name);
            detailMap.put(detail.Name, detail);
            if (!quoteIds.contains(detail.zqu__Quote__c))
                quoteIds.add(detail.zqu__Quote__c);
        }
        
        system.debug('Detail Map: ' + detailMap);
        
        List<zqu__QuoteChargeSummary__c> summaries = [
            										  Select
                                                      	Id,
                                                      	Name,
            											zqu__DeltaTCV__c
                                                      from
                                                      	zqu__QuoteChargeSummary__c
                                                      where
                                                      	zqu__QuoteRatePlan__r.zqu__Quote__c in :quoteIds
                                                     ];
        List<zqu__QuoteChargeSummary__c> updatedSummaries = new List<zqu__QuoteChargeSummary__c>();
        for (zqu__QuoteChargeSummary__c summary : summaries){
            zqu__QuoteChargeDetail__c currentDetail = detailMap.get(summary.Name);
            system.debug('charge name: ' + summary.Name);
            system.debug('currentDetail: ' + currentDetail);
            if (currentDetail != null && summary.zqu__DeltaTCV__c != currentDetail.zqu__DeltaTCV__c){
                summary.zqu__DeltaTCV__c = currentDetail.zqu__DeltaTCV__c;
                updatedSummaries.add(summary);
            }
        }
        
        if (!updatedSummaries.isEmpty()){
            update updatedSummaries;
        }
        
    }
}