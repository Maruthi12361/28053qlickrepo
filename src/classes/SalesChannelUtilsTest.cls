/*
    SalesChannelUtilsTest
    
    Author:
        Tao Jiang, tjg@qlikview.com
        
    Changes:
        2017-01-15 TJG  Created.

    Description:
        Test class for SalesChannelUtils.

 */
@isTest
public class SalesChannelUtilsTest {
    static final integer NBR_OF_ACCOUNTS = 6;
    static final Id SysAdminProfileId = '00e20000000yyUzAAI';
    static final Id PortalUserRoleId  = '00E20000000vrJSEAY';
    static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';

    private static testMethod void creationTest(){

        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator
        User testUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        // Create 3 accounts.  
        List<Account> testAccounts = createTestAccounts(testUser, ctList);

        List<Id> cals = new List<Id>();
  
        Id channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[0].Id, Date.today());

        System.assertNotEquals(channelId, null);

        cals.add(channelId);

        channelId = SalesChannelUtils.createSalesChannel(testAccounts[3].Id, testAccounts[4].Id, testAccounts[5].Id, Date.today());

        cals.add(channelId);

        List<Contract> contracts = createTestContracts(testAccounts, cals);

        channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[0].Id, Date.today(), 
            contracts[0].Id);

        System.assertNotEquals(channelId, null);

        channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[0].Id, Date.today(), 
            contracts[0].Id, 'Direct');

        System.assertNotEquals(channelId, null);

        channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[0].Id, Date.today(), 
            contracts[0].Id, 'Direct', 'Active');

        System.assertNotEquals(channelId, null);

        channelId = SalesChannelUtils.createNewSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[0].Id, Date.today(), 
            contracts[0].Id, 'Direct', 'Active', 'Enterprise', 20.0);

        System.assertNotEquals(channelId, null);

        Sales_Channel__c sc = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[1].Id, 'Partner', 'Reseller' , testAccounts[0].Id);
        System.assertEquals(sc.License_Customer__c, testAccounts[0].Id);

        Test.stopTest();
    }

    private static testMethod void updateTest(){

        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator
        User testUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        // Create 3 accounts.  
        List<Account> testAccounts = createTestAccounts(testUser, ctList);
        List<Id> chnlIds = new List<Id>();
  
        Id channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[2].Id, Date.today());
        
        chnlIds.add(channelId);
        
        channelId = SalesChannelUtils.createSalesChannel(testAccounts[3].Id, testAccounts[4].Id, testAccounts[5].Id, Date.today());
        
        chnlIds.add(channelId);

        System.assertNotEquals(channelId, null);

        List<Contract> contracts =   createTestContracts(testAccounts, chnlIds);

        Id contractId = SalesChannelUtils.updateLatestContract(channelId, contracts[1].Id);

        System.assertNotEquals(contractId, null);

        String revType = SalesChannelUtils.updateRevenueType(channelId, 'Direct');

        System.assertEquals(revType, 'Direct');

        Decimal perc = SalesChannelUtils.updateSupportLevelPercentage(channelId, 'Enterprise', 23.0);

        System.assertEquals(perc, 23.0);

        String sLevel = SalesChannelUtils.updateContractAndSupportLevel(channelId, contracts[0].Id, 'Enterprise', 20.0);

        System.assertEquals(sLevel, 'Enterprise');

        contractId = SalesChannelUtils.updateExistingSalesChannel(channelId, contracts[0].Id, 'Reseller', 'Active', 'Enterprise', 23.0);

        System.assertNotEquals(contractId, null);

        Test.stopTest();
    }

    private static testMethod void upsertTest(){

        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator
        User testUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        // Create 6 accounts.  
        List<Account> testAccounts = createTestAccounts(testUser, ctList);

        SalesChannelUtils scu = new SalesChannelUtils();
        List<Id> chnlIds = new List<Id>();
  
        Id channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[2].Id, Date.today());
        
        chnlIds.add(channelId);
        
        channelId = SalesChannelUtils.createSalesChannel(testAccounts[3].Id, testAccounts[4].Id, testAccounts[5].Id, Date.today());
        
        chnlIds.add(channelId);

        System.assertNotEquals(channelId, null);

        List<Contract> contracts =   createTestContracts(testAccounts, chnlIds);

        Id anotherId = SalesChannelUtils.upsertSalesChannel(null, testAccounts[0].Id, testAccounts[1].Id, testAccounts[2].Id, Date.today(), 
            contracts[0].Id, 'Reseller', 'Active', 'Enterprise', 23.0);

        Id contractId = SalesChannelUtils.updateLatestContract(channelId, contracts[0].Id);

        System.assertNotEquals(anotherId, null);
        
        Test.stopTest();
    }

    private static testMethod void findOrCreateTest(){

        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator
        User testUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        // Create 3 accounts.  
        List<Account> testAccounts = createTestAccounts(testUser, ctList);

        List<Id> chnlIds = new List<Id>();
  
        Id channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[2].Id, Date.today(), null, 'Direct');
        
        chnlIds.add(channelId);
        
        channelId = SalesChannelUtils.createSalesChannel(testAccounts[3].Id, testAccounts[4].Id, testAccounts[5].Id, null);
        
        chnlIds.add(channelId);

        System.assertNotEquals(channelId, null);

        List<Contract> contracts = createTestContracts(testAccounts, chnlIds);

        List<Sales_Channel__c> cals = SalesChannelUtils.createSalesChannelsForContracts(contracts);

        System.assertNotEquals(null, cals);

        //Insert cals;

        //System.assertNotEquals(null, cals[0].Id);

        //Sales_Channel__c sch = [select Id from Sales_Channel__c sch where Id = channelId limit 1];

        Boolean matched = SalesChannelUtils.salesChannelMatchesContract(cals[1], contracts[0]);

        System.assertEquals(false, matched);

        matched = SalesChannelUtils.salesChannelMatchesContract(cals[0], contracts[0]);

        System.assertEquals(true, matched);

        Id anotherId = SalesChannelUtils.findOrCreateSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[2].Id, null, 
            contracts[0].Id, 'Direct', 'Active', 'Enterprise', 23.0);
        // this should be the first sales channel created earlier
        System.assertEquals(chnlIds[0], anotherId);

        // this should be one 
        //System.assertEquals(cals[0].Id, anotherId);

        anotherId = SalesChannelUtils.findOrCreateSalesChannel(testAccounts[0].Id, null, testAccounts[2].Id, Date.today(), 
            contracts[0].Id, 'Reseller', 'Active', 'Enterprise', 23.0);
        System.assertNotEquals(chnlIds[0], anotherId);
        System.assertNotEquals(anotherId, null);
        
        Test.stopTest();
    } 

    private static List<Account> createTestAccounts(User aUser, List<Contact> cLst)
    {
        List<Account> aLst = new List<Account>();
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser){
            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Account userAccount = new Account(
                    Billing_Country_Code__c = qtComp.Id,
                    QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                    Name = 'UserAcc' + i,
                    OwnerId = aUser.Id,
                    RecordTypeId = PartnerAccRecTypeId
                    );
                aLst.add(userAccount);  
            }

            Insert aLst;

            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Contact contact = new Contact( FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = aLst[i].Id,
                Email = 'tc' + i + '@test.com');
                cLst.add(contact);  
            }
            insert cLst;
        } 
        return aLst;
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me ) 
    {
        User u 
            = new User(
               alias = 'adminUsr', email='adminUser@tjtest.com.test',
                Emailencodingkey='UTF-8', lastname='adminTest', 
                Languagelocalekey='en_US', localesidkey='en_US',
                Profileid = profId,
                Timezonesidkey='America/Los_Angeles', 
                //ContactId = cont.Id,
                UserRoleId = roleId,
                Username= System.now().millisecond() + '_' + '_newuser@jttest.com.test'
            );

        System.runAs ( me ) 
        {
            insert u;
        }

        return u;
    }

    public static List<Contract> createTestContracts(List<Account> accounts, List<Id> calIds) {
        List<Contract> ctracts = new List<Contract>();
        for (integer i = 0; i < calIds.size(); i++)
        {
            integer j = 3*i;
        Contract c = new Contract(
                Name='test'+i,
                StartDate=Date.Today(),
                Status = 'Draft', 
                AccountId = accounts[j].Id,  
                ContractTerm = 4,
                Revenue_Type__c = 'Direct',
                Support_Level__c = 'Basic',
                Support_Percentage__c = 15,
                Sell_Through_Partner__c = accounts[j+1].Id,
                Support_Provided_By__c = accounts[j+2].Id,
                Support_Provided_By_Type__c = 'Qlik',
                Sales_Channel__c = calIds[i]
                );
        insert c;
        ctracts.add(c);
        }
    return ctracts;
  }
}