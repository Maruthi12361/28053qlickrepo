/******************************************************

    Class: NSSupportContractSharing
    
    Changelog:
        2015-12-21  IRN     Created file
                            
******************************************************/
global class NSSupportContractSharing{


	global void shareContractsToPartner(Map<Id, List<Id>> contractPartnerIds){

		System.debug('shareContractsToPartner ');
		List<Id> partnerIds = new List<Id>();
		List<Id> contractIds = new List<Id>();
		contractIds.addAll(contractPartnerIds.keySet());

		for (Id key : contractPartnerIds.keySet()){
			partnerIds.addAll(contractPartnerIds.get(key));
		}

		List<UserRole> partnerRoles = ApexSharingRules.getPartnerRoles(partnerIds, 'Partner', 'Executive');
      	System.debug('partner roles ' + partnerRoles.size());
      	Map<Id, List<Id>> accountToRole = new Map<Id, List<Id>>();
      	Set<Id> uniquePortalRoleIDs = new Set<Id>();
      	for(Integer i = 0; i< partnerRoles.size(); i++){
      		if(!accountToRole.containsKey(partnerRoles[i].PortalAccountId)){
      			List<Id> temp = new List<Id>();
      			temp.add(partnerRoles[i].Id);
      			accountToRole.put(partnerRoles[i].PortalAccountId, temp);
      		}else{
      			List<Id> temp = accountToRole.get(partnerRoles[i].PortalAccountId);
      			temp.add(partnerRoles[i].Id);
      		}
            uniquePortalRoleIDs.Add(partnerRoles[i].Id);
      	}

      	// Fetch all groups associated with the roles
      	List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(new List<Id>(uniquePortalRoleIDs), 'RoleAndSubordinates');
      	Map<Id, Id>groupMap = new Map<Id, Id>();
      	for(Integer i = 0; i < groups.size(); i++){
          groupMap.put(groups[i].RelatedId ,groups[i].Id);
      	}
      	System.debug('groupMap ' + groupMap);
		System.debug('contractIds ' +contractIds);
		List<NS_Support_Contract__Share> newContractShrs = new List<NS_Support_Contract__Share>();
		List<NS_Support_Contract__Share>  oldsharing = [Select Id, AccessLevel, RowCause, ParentId, UserOrGroupId from NS_Support_Contract__Share where parentId in :contractIds and RowCause= : NS_Support_Contract__Share.RowCause.Partner_account__C];
		System.debug('remove old sharing ' + oldsharing);
		delete oldsharing;


		for (Id key : contractPartnerIds.keySet()){
			List<Id> pIds = contractPartnerIds.get(key);
			for(Integer i = 0; i<pIds.size(); i++){
				List<Id> roleId = accountToRole.get(pIds[i]);
				if(roleId != null){
					for(Integer j = 0; j<roleId.size(); j++){
						Id id = groupMap.get(roleId[j]);
						if(id != null){
							NS_Support_Contract__Share contractShare = new NS_Support_Contract__Share();
							contractShare.UserOrGroupId = id;
							contractShare.ParentId = key;
							contractShare.RowCause = NS_Support_Contract__Share.RowCause.Partner_account__C;
							contractShare.AccessLevel = 'Read';
							newContractShrs.add(contractShare);
						}	
					}
					
				}
			}
		}
	
    Integer batchsize = 200;
    if(newContractShrs != null && newContractShrs.size()>0){
      for(Integer i = 0; i<(newContractShrs.size()/batchsize)+1; i++){
         List<NS_Support_Contract__Share> tempList;
              if (newContractShrs.size() > ((i + 1) * BATCHSIZE))
              {
                  tempList = getRange(newContractShrs, i * BATCHSIZE, BATCHSIZE);
              }
              else
              {
                  tempList = getRange(newContractShrs, i * BATCHSIZE, newContractShrs.size() - (i * BATCHSIZE));
              }

              System.debug('sharing ' + tempList.size());
              insert tempList;  
       }
    }
  }

    public List<NS_Support_Contract__Share> getRange(List<NS_Support_Contract__Share> fromList, Integer fromIndex, Integer amount)
    {
        List<NS_Support_Contract__Share> returnList = new List<NS_Support_Contract__Share>();
        for(Integer i = fromIndex; i < fromIndex + amount; i++){
            returnList.add(fromList.get(i));
        }
        return returnList;
    }

}