/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_EnvironmentSelectionControllerTest {
    
    private static Entitlement selectedLicense;
    private static EnvironmentProduct__c selectedEnvironmentProduct;
    private static Environment__c selectedEnvironment;
    private static Account selectedAccount;
    private static Case selectedCase;

    private static Id recTypeId;
    private static Id accountrecTypeId;

    private static User currentUser;
    

    @isTest static void testPartner() {

        InsertTestData('Partner');

        PageReference pageRef = Page.QS_CaseDetails;
        pageRef.getParameters().put('caseid', Id.valueOf(selectedCase.Id));
        pageRef.getParameters().put('source','Case');
        Test.setCurrentPageReference(pageRef);

        system.runas(currentUser)
        {
            QS_EnvironmentSelectionController controller = new QS_EnvironmentSelectionController();
            controller.setSelectedEnvironment();
            controller.cancelEnvironmentLink();
            id testId = controller.selectedEnvironmentId ;
        }

    }
    @isTest static void testCustomer() {

        InsertTestData('Customer');

        PageReference pageRef = Page.QS_CaseDetails;
        pageRef.getParameters().put('caseid', Id.valueOf(selectedCase.Id));
        pageRef.getParameters().put('source','Case');
        Test.setCurrentPageReference(pageRef);

        system.runas(currentUser)
        {
            QS_EnvironmentSelectionController controller = new QS_EnvironmentSelectionController();
            controller.setSelectedEnvironment();
            controller.cancelEnvironmentLink();
        }
    }


    private static void InsertTestData(string accountType)
    {
        List<User> communityUserList;

        if(accountType == 'Partner')
        {
            //At least one community user will be found
            QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');

            communityUserList = [Select Id, Name, ContactId, Contact.Persona__c, AccountId, ProfileId, (Select MemberId, 
                NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1), Contact.Name, Contact.Account.Name From User where UserType != 'Standard' AND 
                ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Contact.AccountId != null AND 
                Contact.Account.IsPartner = true AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];
        }
        else
        {
            //At least one community user will be found
            QTTestUtils.createMockUserForProfile('Customer Portal Case Logging Access', true);

            communityUserList = [Select Id, Name, ContactId, Contact.Persona__c, AccountId, ProfileId, (Select MemberId, 
                NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1), Contact.Name, Contact.Account.Name From User where UserType != 'Standard' AND 
                ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Contact.AccountId != null AND 
                Contact.Account.IsCustomerPortal = true AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];
        }


        
        system.assert(communityUserList.size() > 0, 'No community users found.');
        
        User communityUser = communityUserList[0];
        currentUser = communityUser;

        system.debug('currentUser.Id: ' + currentUser.Id);
        system.debug('currentUser.Contact.Name: ' + currentUser.Contact.Name);
        system.debug('currentUser.Contact.Account.Name: ' + currentUser.Contact.Account.Name);
        system.debug('currentUser.Contact.Account.Id: ' + currentUser.Contact.Account.Id);
        system.debug('currentUser.Contact.AccountId: ' + currentUser.Contact.AccountId);
        
        //Create Contact
        /*Contact testContact2 = TestDataFactory.createContact('test_Thomas', 'test_Jones_Jr', 'testSandboxTJ2@qlikTech.com', '+44-8888888888', communityUser.AccountId);
        testContact2.Persona__c = 'Business Users';
        testContact2.LeadSource = 'Qlikmarket';
        insert testContact2;
        
        
        //User communityUser2 = communityUserList[1];
        
        testContact2.Id=communityUser.ContactId;
        update testContact2;*/


        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(communityUser.AccountId, 'licenseReference', '5398347387423');
        insert productLicense;

        system.debug('productLicense.Id:' + productLicense.Id);
        selectedLicense = productLicense;
        
        //Create Environmenta
        Environment__c environment = TestDataFactory.createEnvironment(communityUser.AccountId, 'environmentName', 'operatingSystem', productLicense.Id, 'Type', 'version');
        insert environment;

        selectedEnvironment = environment;

        EnvironmentProduct__c environmentProduct = new EnvironmentProduct__c();
        environmentProduct.Environment__c = environment.ID;
        environmentProduct.License__c = productLicense.Id;
        environmentProduct.Operating_System__c = 'Windows 95';
        environmentProduct.Product__c = 'SAP Connector';
        environmentProduct.Version__c = '5.80.2108';

        insert environmentProduct;

        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        

        insert caseObj;

        selectedCase = caseObj;

        system.debug('selectedCase.Id: '+ selectedCase.Id);

        selectedEnvironmentProduct = environmentProduct;
    }
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
    private static Id getAccountRecordTypeId(String recordTypeName) {
        if(accountrecTypeId != null) {
            return accountrecTypeId;
        } else {
                
            accountrecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return accountrecTypeId;
        }
    }
}