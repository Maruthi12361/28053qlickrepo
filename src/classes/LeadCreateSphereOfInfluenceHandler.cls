/*
 Author - Jina Chetia
 Company - Bluewolf
 Date - 09/09/2009

 This trigger is being called on Lead Convert. Create a new Sphere of Influence whenever any 
 Lead is converted and set the 'From Lead' flag to true for the Sphere of Influence record.
 Do not create a new Sphere of Influence record if there is no Opportunity associated
 with the converted Lead record.
 The unit test is test_CreateSOE_LeadConvert and is contained in TEST_Triggers.cls
*/
/****************************************************************
*  2010-09-21 MHG : Added check if Sphere of Influence allready exists on
*                   converted Opportunity.
*                   CR #402: https://emea.salesforce.com/a0C2000000C81aI
*
*  2011-09-07 RDZ : CR #2811: https://eu1.salesforce.com/a0CD000000EWdNC
*  2013-04-10 EXT_YKA : CR #7761: https://eu1.salesforce.com/a0CD000000XP0Hm
*                       to remove duplicate SOI on lead conversion. NOTE: The
*                       function CreateSoIFromOpportunityContactRoles() in web
*                       service sfUtils can also create an SoI.
*
*  2017-10-25 AYS BMW-402 : Migrated from CreateSphereOfInfluence_LeadConvert.trigger
*****************************************************************/

public class LeadCreateSphereOfInfluenceHandler {
	public LeadCreateSphereOfInfluenceHandler() {
		
	}

	public static void handle(List<Lead> triggerNew) {
		List<Sphere_of_Influence__c> sphereList = new List<Sphere_of_Influence__c>();
	    List<Opportunity> OppsToUpdate = new List<Opportunity>();
	    List<Account> AccsToUpdate = new List<Account>();
	    
	    
	    // 2010-09-21 MHG : Added check if Sphere of Influence allready exists on
	    //                  converted Opportunity.
	    //                  CR #402: https://emea.salesforce.com/a0C2000000C81aI
	    //
	    // -- code block starts --
	    List<string> OppIDs = new List<string>();
	    List<string> OEMOppIDs = new List<string>();
	    List<String> OEMAccIDs = new List<String>();
	    for (Lead L : triggerNew)
	    {
	        if (L.ConvertedOpportunityId != null)
	        {
	            OppIDs.Add(L.ConvertedOpportunityId);
	            if (L.OEM__c == true)
	            {
	                OEMOppIDs.Add(L.ConvertedOpportunityId);
	            }
	        }                         
	    }
	    
	    Set<string> HasSphs = new Set<string>();
	    if (OppIDs.size() > 0)
	    {
	        for (Sphere_of_Influence__c Sph : [select Opportunity__c from Sphere_of_Influence__c where Opportunity__c in :OppIDs])
	        {
	            if (!HasSphs.Contains(Sph.Opportunity__c))
	            {
	                HasSphs.Add(Sph.Opportunity__c);
	            }
	        }
	    }
	    
	    if (OEMOppIDs.size() > 0)
	    {
	        for (Opportunity Opp : [select Id, RecordTypeId, AccountId from Opportunity where Id in :OEMOppIDs])
	        {
	            //Live OEM Recruitment (CCS) Record Type '012D0000000JpIxIAK' Test '012M00000004JzLIAU'
	            if (Opp.RecordTypeId != '012D0000000JpIxIAK')
	            {
	                //Set OEM Recruitment (CCS) Live:'012D0000000JpIxIAK' Test:'012M00000004JzLIAU'
	                Opp.RecordTypeId = '012D0000000JpIxIAK';
	                OppsToUpdate.Add(Opp);
	            }
	            
	            //Add to AccountId list to modify their record typt to a OEM account
	            OEMAccIDs.Add(Opp.AccountId);
	            System.Debug('Changing record type to OEM Partner Account');
	        }
	        
	        for (Account Acc : [select Id, RecordTypeId, CreatedDate from Account where Id in :OEMAccIDs])
	        {
	            //Live "OEM Partner Account" Record Type '01220000000DOG4AAO' Test '01220000000DOG4AAO'
	            if (Acc.RecordTypeId != '01220000000DOG4AAO')
	            {
	                //Set "OEM Partner Account" in Live:'Check in live' Test:'01220000000DOG4AAO'
	                //If account is a new acc then set it to OEM Partner Account (The creation time was in the last minute)
	                if (Acc.CreatedDate > Datetime.Now().addMinutes(-1))
	                {
	                    Acc.RecordTypeId = '01220000000DOG4AAO';
	                    AccsToUpdate.Add(Acc);
	                }
	            }   
	        }
	    }


	    // -- code block stops --
	    
	    for(Lead leadData: triggerNew)
	    {
	        // 2010-09-21 MHG : Added HasSphs check here CR #402.
	        if(leadData.IsConverted && leadData.ConvertedOpportunityId != null && !HasSphs.Contains(leadData.ConvertedOpportunityId))
	        {
	            string Goal = '';
	            if (leadData.Goal_Problem_Need_Description__c != null)
	            {
	                Goal = leadData.Goal_Problem_Need_Description__c.substring(0, (leadData.Goal_Problem_Need_Description__c.length() < 250 ? leadData.Goal_Problem_Need_Description__c.length() : 250));
	            }
	            
	            // Adding Role__c = 'Champion' on SOI to avoid duplicate SOI record creation by web service (2013-04-10 EXT_YKA : CR #7761)
	            Sphere_of_Influence__c newSphere = 
	                new Sphere_of_Influence__c(Opportunity__c = leadData.ConvertedOpportunityId, 
	                    Contact__c = leadData.ConvertedContactId, From_Lead__c = true, Goal__c = Goal, Role__c = 'Champion'); 
	            sphereList.add(newSphere);
	        }
	    }
	    if(!sphereList.isEmpty())
	    {
	        insert sphereList;
	    }
	    
	    if (OppsToUpdate.size() > 0)
	    {           
	        update OppsToUpdate;
	    }
	    
	    if (AccsToUpdate.size() > 0)
	    {
	        update AccsToUpdate;
	    }
	}
}