@isTest
private class cilcTest {
    static testMethod void validate() {
    
    // Create a new account object.
    Account testAccount = new Account(Name = 'TestAccount');
    insert testAccount;
  
    
    //CREATE TEST ACCOUNT PLAN
    Account_Plan__c accountplan=new Account_Plan__c ();
    accountplan.account__c=testaccount.id;
    insert accountplan;
    
      //CREATE TEST CONTACT
    Contact contact = new contact(lastname='lname');
    contact.Job_title__c='salesrep';
    insert contact; 
    Contact contact2 = new contact(lastname='lname');
    insert contact2;  
    
  

    Test.startTest();
    
    string searchString='search';
        PageReference pageRef = Page.CustomInfluencerLookup;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('accplanid', accountplan.Id);
        ApexPages.currentPage().getParameters().put('lksrch', searchString);
        CustomInfluencerLookupController controller = new CustomInfluencerLookupController();       
         
            controller.search();
            controller.getFormTag();
            controller.getTextBox();
        
    Test.stopTest();       
         
        
   }
   }