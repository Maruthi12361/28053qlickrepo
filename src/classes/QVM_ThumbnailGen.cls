/*********************************************
*
*   2019-03-05  AIN Moved custom settings to QuoteTestHelper
*
*********************************************/
public with sharing class QVM_ThumbnailGen {

	private String bucket;
	private String mode;
	private integer thumbWidth;
	private integer thumbHeight;

	public static testMethod void testSQS () {
		
		QVM_ThumbnailGen gen = new QVM_ThumbnailGen('bucket','live',100,100);
		gen.generate('someproductid','image.gif');
		
	}

	public QVM_ThumbnailGen (String bucket, String mode, 
	  integer thumbWidth, integer thumbHeight ) {
		this.bucket=bucket;
		this.mode=mode;
		this.thumbWidth=thumbWidth;
		this.thumbHeight=thumbHeight;
		
	}
	
	/**
	* This version of the method allows for bulk processing.  
	* Passing in a map of Strings with productId's as the key and the
	* image name as the value
	*/
	public boolean generate (Map<String,String> productIdImageMap) {
		
		ThumbnailRequest req = new ThumbnailRequest();
		req.bucket=this.bucket;
		req.mode=this.mode;
		req.thumbWidth = this.thumbWidth;
		req.thumbHeight = this.thumbHeight;
		req.productIdImageMap=productIdImageMap;
		
		//Convert the request to JSON to pass to the SQS queue
		JSONGenerator gen = JSON.createGenerator(false);
		gen.writeObject(req);
		String message = gen.getAsString();
	
		System.debug('JSON Message: '+message);
		
		return sendMessage(message);
		
	}
	
	public boolean generate ( String productId, String imageName) {
		Map<String,String> productIdImageMap = new Map<String,String>();
		productIdImageMap.put(productId,imageName);
		return generate(productIdImageMap);
	}
	
	private boolean sendMessage (String message) {
		QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
		
		Map<String,String> params = new Map<String,String>();
		params.put('AWSAccessKeyId',encode(settings.S3_Key__c));
        params.put('Action','SendMessage');
        params.put('MessageBody',encode(message));
        params.put('Timestamp',encode(getCurrentDate()));        
        params.put('SignatureMethod','HmacSHA1');
        params.put('SignatureVersion','2');
        params.put('Version','2009-02-01');
        
        //Take the parameters and build out the signature
        List<String> sortedKeys = new List<String>();
        sortedKeys.addAll(params.keySet());
        sortedKeys.sort();
        String toSign = 'GET\n' + settings.SQS_Endpoint__c +'\n/'+settings.SQS_Thumbnail_Queue__c+'\n';
        
        Integer x = 0;
        for (String key : sortedKeys) {
            String value = params.get(key);
            if (x > 0) {
                toSign += '&';
            }
            x++;
            toSign += key+'='+value;
        }
        
        
        params.put('Signature',getMac(toSign,settings.S3_Secret__c));
        
        String url = 'https://'+ settings.SQS_Endpoint__c+'/'+settings.SQS_Thumbnail_Queue__c+'?';
        x = 0;
        for (String key : params.keySet()) {
            if (x > 0) {
                url += '&';
            }
            x++;
            url += key+'='+params.get(key);
        }

		System.debug('Request URL: '+url);
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndPoint(url);
        httpReq.setMethod('GET');
        Http http = new Http();
		
		//Callout not allowed during test
		if (!Test.isRunningTest()) {
	        try {
	            HttpResponse res = http.send(httpReq);
	        }
	        catch (System.CalloutException e) {
	            System.debug('ERROR: ' + e);
	            return false;
	        }
		}
        
		return true;
	}
	
	private String encode(String message){
        return EncodingUtil.urlEncode(message,'UTF-8').replace('+', '%20').replace('*', '%2A').replace('%7E','~');
    }

    private String getMac(String RequestString, String secretkey) {
        String algorithmName = 'hmacSHA1';
        Blob input = Blob.valueOf(RequestString);
        Blob key = Blob.valueOf(secretkey);
        Blob signing =Crypto.generateMac(algorithmName, input, key);
        return EncodingUtil.urlEncode(EncodingUtil.base64Encode(signing), 'UTF-8');
    }
    
    private String getCurrentDate() {
        return DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
    }
    
	
	
	class thumbnailRequest {
		String bucket;
		String mode;
		integer thumbWidth;
		integer thumbHeight;
		Map<String,String> productIdImageMap;
	}
}