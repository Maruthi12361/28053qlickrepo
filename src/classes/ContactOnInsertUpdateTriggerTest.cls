/*************************************************************************************************************
 Name: ContactOnInsertUpdateTriggerTest
 Author: CCE
 Purpose: This is test class for ContactOnInsertUpdateTrigger
 Created Date : 18th May 2016
 
 Log History:
 2017-09-22 - AYS -  BMW-396/CHG0032034 - Added logic for Archived Record Type.
 2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
 2019-06-12 extbad IT-1923 Disabled Contact Duplicate rule for chats
*************************************************************************************************************/
@isTest
private class ContactOnInsertUpdateTriggerTest {
    
    static testmethod void ContactOnInsertUpdateTriggerTest()
    {
        System.debug('ContactOnInsertUpdateTriggerTest: Starting');
        //Create user
        User aUser = QTTestUtils.createMockOperationsAdministrator();
        System.debug('ContactOnInsertUpdateTriggerTest: aUser = ' + aUser);
        
        //Create Account
        Account acc = QTTestUtils.createMockAccount('ContactOnInsertUpdateTriggerAcc', aUser);
        System.debug('ContactOnInsertUpdateTriggerTest: acc = ' + acc);
        
        //Create Contact
        //Semaphores.TriggerHasRun(1);
        Semaphores.ContactTriggerHandlerBeforeInsert = false;
        Test.startTest();
        Contact con = QTTestUtils.createMockContact(acc.Id);
        System.debug('ContactOnInsertUpdateTriggerTest: con = ' + con);
        
        //Do test
        //Semaphores.TriggerHasRun(1);
        Semaphores.ContactTriggerHandlerBeforeInsert = false;
        Contact cRet = [SELECT Id, Account_Owner__c From Contact WHERE Id =: con.Id];
        system.debug('contact inserted '+cRet);
        System.assertequals(aUser.Id, cRet.Account_Owner__c);
        Test.stopTest();
        System.debug('ContactOnInsertUpdateTriggerTest: Finishing');        
    }
    
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    public static testMethod void test_ContactChange(){
        
        QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator']; 
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, 
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');
        
        System.runAs(u){
            Account  testAcc = QTTestUtils.createMockAccount('TestCompany', u, true);
            Account  testAcc2= QTTestUtils.createMockAccount('TestCompany2', u, true);
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAcc2.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update(new List<Account>{testAcc,testAcc2});
        
            Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , true);

            RecordType rType = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Evaluation_Quote'];
                
            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            Contact testContact = QuoteTestHelper.createContact(testAcc.id);
            insert testContact;
                        
            List<Address__c> lstAdd = new List<Address__c>();
            Address__c billAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Billing');
            billAddress.Country__c = 'United Kingdom';
            Address__c shippAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Shipping');
            lstAdd.add(billAddress);
            lstAdd.add(shippAddress);
            insert lstAdd;
                        
            Test.startTest();
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;                               
                SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
                quoteForTest.SBQQ__Primary__c = true;
                update quoteForTest;
                SBQQ__Quote__c quoteForSelect = [Select id, SBQQ__MasterContract__c, SBQQ__StartDate__c,  Revenue_Type__c, Record_Type_Dev_Name__c, Quote_Recipient__c,
                                                    Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, 
                                                    Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c 
                                                    from SBQQ__Quote__c where id =:quoteForTest.id];
                Semaphores.ContactTriggerHandlerBeforeUpdate = false; 
                try{
                    Contact testCon = [select id,AccountId from Contact where id =: quoteForSelect.Quote_Recipient__c];
                    testCon.AccountId = testAcc2.Id;
                    database.update(testCon);
                }catch(Exception e){}
            Test.stopTest();
            
        }
    }

    @isTest static void test_SuspectStausChanged1() {

        List<RecordType> l_RecordType = new List<RecordType>([
            SELECT id, DeveloperName
            FROM RecordType
        ]);
        Map<String, Id> m_RecordTypes = new Map<String, Id>();
        for(RecordType recordType : l_RecordType) {
            m_RecordTypes.put(recordType.DeveloperName, recordType.id);
        }
            
        insert new ZiftSettings__c(
            RT_StandardContact__c = m_RecordTypes.get('Business_Contact')
        );
            
        ZiftSettings__c csZift = ZiftSettings__c.getInstance();

        System.assertEquals(m_RecordTypes.get('Business_Contact'), csZift.RT_StandardContact__c);

        User u = QTTestUtils.createMockUserForProfile('System Administrator'); 
        //User u = [SELECT id FROM User WHERE username LIKE 'jch@qlikview.com%']; //for test validation rule on on DEV sandbox
        System.runAs(u) {
            Contact contact = new Contact(LastName = 'ContactName', RecordTypeId = m_RecordTypes.get('Resource'));
            insert contact; //Business_Contact //Resource //Old_Record_Type_Name__c = 'nothing'
                
            Contact insertedContact = [SELECT id, Old_Record_Type_Name__c FROM Contact WHERE id = : Contact.id];

            System.assertEquals(null, insertedContact.Old_Record_Type_Name__c);

            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            insertedContact.Contact_Status__c = 'Suspect';
            update insertedContact;

            Contact updatedContact = [SELECT id, RecordTypeId, Contact_Status__c FROM Contact WHERE id = : Contact.id];

            System.assertEquals('Suspect', updatedContact.Contact_Status__c);

            System.assertEquals(m_RecordTypes.get('Business_Contact'), updatedContact.RecordTypeId);

        }
    }    

    @isTest static void test_SuspectStausChanged2() {

        List<RecordType> l_RecordType = new List<RecordType>([
            SELECT id, DeveloperName
            FROM RecordType
        ]);
        Map<String, Id> m_RecordTypes = new Map<String, Id>();
        for(RecordType recordType : l_RecordType) {
            m_RecordTypes.put(recordType.DeveloperName, recordType.id);
        }
            
        insert new ZiftSettings__c(
            RT_StandardContact__c = m_RecordTypes.get('Business_Contact')
        );
            
        ZiftSettings__c csZift = ZiftSettings__c.getInstance();

        System.assertEquals(m_RecordTypes.get('Business_Contact'), csZift.RT_StandardContact__c);

        User u = QTTestUtils.createMockUserForProfile('System Administrator'); 
        //User u = [SELECT id FROM User WHERE username LIKE 'jch@qlikview.com%']; //for test validation rule on on DEV sandbox
        System.runAs(u) {
            Contact contact = new Contact(LastName = 'ContactName', RecordTypeId = m_RecordTypes.get('Business_Contact'), Old_Record_Type_Name__c = 'Resource');
            insert contact; //Business_Contact //Resource //Old_Record_Type_Name__c = 'nothing'
                
            Contact insertedContact = [SELECT id, Old_Record_Type_Name__c FROM Contact WHERE id = : Contact.id];

            System.assertEquals('Resource', insertedContact.Old_Record_Type_Name__c);

            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            insertedContact.Contact_Status__c = 'Suspect';
            update insertedContact;

            Contact updatedContact = [SELECT id, RecordTypeId, Contact_Status__c FROM Contact WHERE id = : Contact.id];

            System.assertEquals('Suspect', updatedContact.Contact_Status__c);

            System.assertEquals(m_RecordTypes.get('Resource'), updatedContact.RecordTypeId);

        }
    }

    @isTest
    static void testCleanSkipDuplicateRule() {
        Account  testAcc = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        Contact testContact = QuoteTestHelper.createContact(testAcc.id);
        testContact.Skip_Duplicate_Email_Rule__c = 'true';

        Test.startTest();
        insert testContact;
        Test.stopTest();

        testContact = [SELECT Skip_Duplicate_Email_Rule__c FROM Contact WHERE Id = :testContact.Id];
        System.assert(String.isEmpty(testContact.Skip_Duplicate_Email_Rule__c));
    }
}