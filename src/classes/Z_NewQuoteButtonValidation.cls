public with sharing class Z_NewQuoteButtonValidation {
    @AuraEnabled(cacheable=true)
    public static QuoteValidationResult newQuoteButtonValidation(String oppId) {

        QuoteValidationResult qvResult = new QuoteValidationResult();

        List<OpportunityContactRole> oppBillToContacts = [SELECT Id, Contact.MailingStreet, Contact.MailingCity, Contact.MailingCountry
                                                            FROM OpportunityContactRole
                                                            WHERE OpportunityId = :oppId 
                                                            AND Role = 'Invoice Recipient - Bill To'];
        
        List<OpportunityContactRole> oppSoldToContacts = [SELECT Id, Contact.MailingStreet, Contact.MailingCity, Contact.MailingCountry
                                                            FROM OpportunityContactRole
                                                            WHERE OpportunityId = :oppId 
                                                            AND Role = 'Quote Recipient - Sold To'];

        List<OpportunityContactRole> oppBillToAddress = [SELECT Id, Contact.MailingStreet, Contact.MailingCity, Contact.MailingCountry
                                                            FROM OpportunityContactRole
                                                            WHERE OpportunityId = :oppId 
                                                            AND Role = 'Invoice Recipient - Bill To'
                                                            AND (Contact.MailingStreet = null OR  Contact.MailingCity = null OR Contact.MailingCountry = null)];

        List<OpportunityContactRole> oppSoldToAddress = [SELECT Id, Contact.MailingStreet, Contact.MailingCity, Contact.MailingCountry
                                                            FROM OpportunityContactRole
                                                            WHERE OpportunityId = :oppId 
                                                            AND Role = 'Quote Recipient - Sold To'
                                                            AND (Contact.MailingStreet = null OR  Contact.MailingCity = null OR Contact.MailingCountry = null)];

        if(oppBillToContacts.isEmpty()){
            qvResult.errorMessages.add(new QuoteValidationError('Missing "Bill To" Contact',
                                                                'You must select a Bill To contact before creating a quote.'));
        }

        if(!oppBillToAddress.isEmpty()){
            qvResult.errorMessages.add(new QuoteValidationError('Missing Contact Information',
                                                                 'The mailing address on the Bill To contact must be fully populated.'));
        }

        if(oppSoldToContacts.isEmpty()){
            qvResult.errorMessages.add( new QuoteValidationError('Missing "Sold To" Contact',
                                                                 'You must select a Sold To contact before creating a quote.'));
        }

        if(!oppSoldToAddress.isEmpty()){
            qvResult.errorMessages.add(new QuoteValidationError('Missing Contact Information',
                                                                'The mailing address on the Sold To contact must be fully populated.'));
        }

        if(!qvResult.errorMessages.isEmpty()){ qvResult.success = FALSE; }

        Id siteId = Site.getSiteId();
        if (siteId != null){qvResult.community = TRUE;}
            else if (siteId == null){qvResult.community = FALSE;}

        return qvResult;
    }

    @AuraEnabled(cacheable=true)
    public static String communityUrl(){
        String commURL = PageReference.forResource('URLStaticResource').getUrl().substringBefore('/resource/') + '/s/sfdcpage/';
        return commUrl;
    }

    @AuraEnabled(cacheable=true)
    public static String internalUrl(){
        String intURL = PageReference.forResource('URLStaticResource').getUrl().substringBefore('/resource/') + Page.zqu__QuoteOption.getUrl();
        return intUrl;
    }

    public class QuoteValidationResult{
        @AuraEnabled
        public Boolean success; 

        @AuraEnabled
        public Boolean community;

        @AuraEnabled
        public List<QuoteValidationError> errorMessages;

        public QuoteValidationResult(){
            this.success = TRUE;
            this.errorMessages = new List<QuoteValidationError>();
        }
    }

    public class QuoteValidationError{
        @AuraEnabled
        public String title = 'ERROR';

        @AuraEnabled
        public String message = 'An Error has occurred.';

        public QuoteValidationError(String title, String message){
            this.title = title;
            this.message = message;
        }
    }

}