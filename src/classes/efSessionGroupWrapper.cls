//*********************************************************/
// Author: Mark Cane&
// Creation date: 24/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efSessionGroupWrapper {
    private List<efSessionWrapper> itemList = new List<efSessionWrapper>();
    private String groupName;
    private DateTime groupDate;
    private Boolean hasSpecialActivities;
    
    public String getGroupName(){
        return groupName;
    }
    
    public void setGroupName(String groupName){
        this.groupName = groupName;
    }
    
    public Datetime getGroupDate(){
        return groupDate;
    }
    
    public void setGroupDate(DateTime groupDate){
        this.groupDate = groupDate;
    } 
    
    public String getGroupLongDate(){
    	return groupDate != null ? groupDate.format('EEEE, MMM d') : '';
    }
    
    public Boolean getHasSpecialActivities(){
    	return hasSpecialActivities;
    }
    
    public void setHasSpecialActivities(Boolean has){
    	hasSpecialActivities = has;
    }
    
    public List<efSessionWrapper> getGroupItems(){
        return itemList;
    }
    
    public Boolean getIsEmpty(){
    	return itemList.size() == 0;
    }
    
    public void addItem(efSessionWrapper s){
        itemList.add(s);
    }
    
    public void setItemList(List<efSessionWrapper> itemList){
       itemList = itemList;
    }   
}