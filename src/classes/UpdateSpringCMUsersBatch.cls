/*******************************************************************
* Name:  UpdateSpringCMUsersBatch
* Description: this batch job for update Users after refreshing sandboxes
*
* Log History:
*  29.06.2020  ext_bjd	ITRM-464 Initial Development
********************************************************************/

global class UpdateSpringCMUsersBatch implements Database.Batchable<SObject>, Database.Stateful {

    private Set<Id> exceptionalIds;
    private Map<Id, String> errorMap = new Map<Id, String>();
    private Map<Id, User> IdToUserMap = new Map<Id, User>();

    public static Boolean isSandbox = PostRefreshHandler.isSandboxFinder();

    public UpdateSpringCMUsersBatch(Set<Id> exceptionalIds) {
        this.exceptionalIds = exceptionalIds;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, Name, SpringCMEos__SpringCM_User__c FROM User ' +
                'WHERE SpringCMEos__SpringCM_User__c = TRUE ' +
                'AND ID not in: exceptionalIds ORDER BY LastModifiedDate desc' + (Test.isRunningTest()?' LIMIT 200':'');
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<User> scope) {
        if (isSandbox || (!isSandbox && Test.isRunningTest())) {
            List<User> modifiedUsers = new List<User>();
            for (User us : scope) {
                us.SpringCMEos__SpringCM_User__c = false;
                modifiedUsers.add(us);
            }
            if (modifiedUsers.size() > 0) {
                if (!Test.isRunningTest()){
                    CustomUserTriggerHandler.ENABLE_USER_TRIGGER = false;
                }
                Database.SaveResult[] srList = Database.update(modifiedUsers, false);
                Integer index = 0;
                for (Database.SaveResult dsr : srList) {
                    if (!dsr.isSuccess()) {
                        String errMsg = dsr.getErrors()[0].getMessage();
                        errorMap.put(modifiedUsers[index].Id, errMsg);
                        IdToUserMap.put(modifiedUsers[index].Id, modifiedUsers[index]);
                    }
                    index++;
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        if (!errorMap.isEmpty()) {
            AsyncApexJob a = [
                    SELECT Id, ApexClassId,
                            JobItemsProcessed, TotalJobItems,
                            NumberOfErrors, CreatedBy.Email
                    FROM AsyncApexJob
                    WHERE Id = :bc.getJobId()
            ];
            String body = 'Your batch job '
                    + 'UpdateSpringCMUsersBatch '
                    + 'has finished. \n'
                    + 'There were '
                    + errorMap.size()
                    + ' errors. Please find the error list attached to the Case.';

            // Creating the CSV file
            String finalStr = 'Id, Name, Error \n';
            String subject = 'Apex Batch Error List';
            String attName = 'Errors.csv';
            for (Id id : errorMap.keySet()) {
                String err = errorMap.get(id);
                User us = IdToUserMap.get(id);
                String recordString = '"' + id + '","' + (String)us.get('Name') + '","' + err + '"\n';
                finalStr = finalStr + recordString;
            }

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

            // Create the email attachment
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalStr));

            // Sets the paramaters of the email
            email.setSubject( subject );
            String[] to = new String[] {UserInfo.getUserEmail()};
            email.setToAddresses(to);
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
    }
}