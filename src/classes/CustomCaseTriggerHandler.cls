/*
 File Name: CustomCaseTriggerHandler

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761 Trigger consolidation.
 2017-01-24 ext_bad:   LCE-48      Add Live Chat record type
 2018-06-22 ext_vos:   CHG0034223  Populate 2 more Case.Regional_Support_Manager emails.
 2018-09-26 ext_vos:   Remove duplicated methods.
 2018-12-17 ext_vos:   INC0154629  Use 'without sharing' class for fixSeverity3 (Guest portal users has no access to Entitlements).
 2019-11-27 ext_bad:   IT-2324 Populdate Case.Resolution_Date
 2020-03-26 ext_cqb:   IT-2622 Tracking highest elevation level of Wait Internal On
*/
public with sharing class CustomCaseTriggerHandler {

    public static Set<Id> qtRecordTypeIds {
        get {
            if (qtRecordTypeIds == null) {
                Set<String> recordTypeNames = new Set<String>{
                        'QT Support Partner Portal Record Type',
                        'QT Support Customer Portal Record Type',
                        'QlikTech Master Support Record Type'
                };
                qtRecordTypeIds = new Map<Id, RecordType>([
                        SELECT Id
                        FROM RecordType
                        WHERE Name IN :recordTypeNames
                ]).keySet();
            }
            return qtRecordTypeIds;
        }
        set;
    }

/*
* Moved from trigger CaseTrigger on Case (before Insert, before Update)
*/
    public static void handleCaseBeforeInsert(List<Case> newCases) {
        checkCaseSeverity(newCases);
        checkCaseEntitlementChanges(newCases, null);
        CaseServices.assignPartnerSuppContactToCase(newCases, null);
        CasePopulateCaseFromCategoryLookup.PopulateCase(newCases);
        CaseServices.updateAccountLicenseTechInfo(newCases);
        CaseEntitlementProcessSyncHandler.handleCaseEntitlementProcessSync(newCases, null);
        CaseServices.handleCaseEndUserAccountNameCopy(newCases);
        CaseServices.populateResolutionDate(newCases, null);
        CaseServices.setHighestElevation(newCases, null);
        populateArticleFeedbackCase(newCases);
    }

    public static void handleCaseBeforeUpdate(List<Case> newCases, List<Case> oldCases, Map<Id, Case> oldCasesMap) {
        checkCaseEntitlementChanges(newCases, oldCases);
        checkCaseSeverity(newCases);
        CaseServices.updateAccountLicenseTechInfo(newCases);
        CaseServices.assignPartnerSuppContactToCase(newCases, oldCasesMap); 
        CaseEntitlementProcessSyncHandler.handleCaseEntitlementProcessSync(newCases, oldCases);
        CaseServices.handleCaseEndUserAccountNameCopy(newCases);
        CaseServices.populateResolutionDate(newCases, oldCasesMap);
        CaseServices.setHighestElevation(newCases, oldCasesMap);
    }

    private static void populateArticleFeedbackCase(List<Case> cases) {
        String rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Article Feedback').getRecordTypeId();
        List<Case> casesToFix = new List<Case>();
        for (Case c : cases) {
            if (rtId == c.RecordTypeId && String.isEmpty(c.Feedback_to_submitter_Actions_taken__c)) {
                c.Feedback_to_submitter_Actions_taken__c = 'We\'ve reviewed your input and have taken the following action(s):<br/><br/>';
            }
        }
    }
    
    private static void checkCaseSeverity(List<Case> cases) {
        List<Case> casesToFix = new List<Case>();
        for (Case c : cases) {
            if (qtRecordTypeIds.contains(c.RecordTypeId) && c.EntitlementId <> null && c.Support_Office__c <> null) {
                casesToFix.add(c);
            }
        }
        if (casesToFix.size() > 0) {
            CaseServiceWithoutSharing.fixSeverity3(casesToFix);
        }
    }

    private static void checkCaseEntitlementChanges(List<Case> newCases, List<Case> oldCases) {
        if (oldCases == null) {
            CaseServices.updateCasesFromAcntLicenseViaEntitlement(newCases);
        } else {
            //& filter cases where entitlement has changed.
            List<Case> casesWhereEntitlementChanged = new List<Case>();
            for (Integer i = 0; i < newCases.size(); i++) {
                if (newCases[i].EntitlementId != oldCases[i].EntitlementId) {
                    casesWhereEntitlementChanged.add(newCases[i]);
                }
            }
            if (casesWhereEntitlementChanged.size() > 0) {
                CaseServices.updateCasesFromAcntLicenseViaEntitlement(casesWhereEntitlementChanged);
            }
        }
    }
/*
 Moved from trigger CaseQuickClose on Case (before insert, before update)
 Trigger that closes the case called in the parameter and sets values for the fields Closure Code, System and Reported Issue
*/
    public static void handleCaseQuickClose(List<Case> newCases, Map<Id, Case> oldCasesMap) {
        string STATUS_CLOSED = 'Closed';
        string VALUE_WEBEX = 'Webex';
        string CLOSURE_CODE_USER_CREATION_MODIFICATION = 'User Creation/Modification';
        string REPORTED_ISSUE_GENERAL_QUERY = 'General Query';

        List<Case> casesToUpdate = new List<Case>();
        Case oldCase;
        Map<string, List<String>> quickCloseSetValues = new Map<string, List<String>>();
        Boolean atLeastOneRecordUpdated = false;
        string quickClose;
        for (Case caseObj : newCases) {
            //Checks the Quick_Close__c field, a value is required
            if (String.isNotBlank(caseObj.Quick_Close__c)) {
                //If it's an update, we need to check to see if the value of Quick_Close__c has changed.
                if (oldCasesMap != null) { //trigger.isUpdate
                    //For safety, should never fail
                    if (caseObj.Id != null && oldCasesMap.containsKey(caseObj.Id)) {
                        //Compare the values of the record before and after update.
                        oldCase = oldCasesMap.get(caseObj.Id);
                        //If Quick_Close__c has changed, we add the record to the list of records to be updated.
                        if (oldCase.Quick_Close__c != caseObj.Quick_Close__c) {
                            casesToUpdate.add(caseObj);
                        }
                    }
                } else {//If not an update, it's an insert
                    casesToUpdate.add(caseObj);
                }
            }
        }
        if (casesToUpdate.size() == 0) {
            return;
        }

        //Contains the quick close code and the corresponding values for the picklists in order
        //Closure code, System, Reported issue, Status
        quickCloseSetValues.put('New User Creation', new List<string>{
                CLOSURE_CODE_USER_CREATION_MODIFICATION, 'User Creation', REPORTED_ISSUE_GENERAL_QUERY, STATUS_CLOSED
        });
        quickCloseSetValues.put('User Termination', new List<string>{
                CLOSURE_CODE_USER_CREATION_MODIFICATION, 'User Termination', REPORTED_ISSUE_GENERAL_QUERY, STATUS_CLOSED
        });
        quickCloseSetValues.put('Webex', new List<string>{
                VALUE_WEBEX, VALUE_WEBEX, VALUE_WEBEX, STATUS_CLOSED
        });
        quickCloseSetValues.put('ULC Details Sent', new List<string>{
                'ULC Related', 'SFDC', 'ULC Issues', STATUS_CLOSED
        });
        quickCloseSetValues.put('Duplicate Case', new List<string>{
                'Duplicate Case', null, null, STATUS_CLOSED
        });

        for (Case caseObj : casesToUpdate) {
            quickClose = caseObj.Quick_Close__c;
            //Compares the value of the Quick_Close__c field with the predefined values
            if (quickCloseSetValues.containsKey(quickClose)) {
                //Sets the values according to the preset values
                List<string> setValues = quickCloseSetValues.get(quickClose);

                caseObj.Closure_Code__c = setValues[0];
                caseObj.System__c = setValues[1];
                caseObj.Reported_Issue__c = setValues[2];
                caseObj.Status = setValues[3];
            }
        }
    }

/*
Moved from trigger CasePopulateSupportEmailFields on Case (before insert, before update)
*/
    public static void handleCasePopulateSupportEmailFields(List<Case> newCases, List<Case> oldCases) {

        List<Case> cases = new List<Case>();// Added by UIN
        Set<Id> accountIds = new Set<Id>();

        for (integer i = 0; i < newCases.size(); i++) {
            if (oldCases == null || //Trigger.isInsert
                    (newCases[i].AccountId != oldCases[i].AccountId // Trigger.isUpdate
                            && String.isNotBlank(newCases[i].AccountId))) {// Modified by UIN to reduce soql execution
                cases.add(newCases[i]);// Added by UIN
                if (newCases[i].AccountId != null) {
                    accountIds.add(newCases[i].AccountId);
                }
            }
        }

        if (!accountIds.isEmpty()) {// Added by UIN to improve soql execution count
            system.debug(LoggingLevel.DEBUG, '[handleCasePopulateSupportEmailFields] !accountIds.isEmpty()');
            Map<Id, Account> accountQueryMap = new Map<Id, Account>([
                    SELECT Support_Office__r.Regional_Support_Manager__r.Email, Support_Office__r.Regional_Support_Manager_2__r.Email,
                            Support_Office__r.Regional_Support_Manager_3__r.Email,
                            Support_Office__r.Team_lead_1__r.Email, Support_Office__r.Team_lead_2__r.Email,
                            Support_Office__r.Team_lead_3__r.Email
                    FROM Account
                    WHERE Id IN :accountIds
            ]);
            for (Case c : cases) {// Modified by UIN
                if (accountQueryMap.containsKey(c.AccountId)) {
                    c.Support_Office_Team_Lead_1_Email__c = accountQueryMap.get(c.AccountId).Support_Office__r.Team_lead_1__r.Email;
                    c.Support_Office_Team_Lead_2_Email__c = accountQueryMap.get(c.AccountId).Support_Office__r.Team_lead_2__r.Email;
                    c.Support_Office_Team_Lead_3_Email__c = accountQueryMap.get(c.AccountId).Support_Office__r.Team_lead_3__r.Email;
                    c.Regional_Support_Manager_Email__c = accountQueryMap.get(c.AccountId).Support_Office__r.Regional_Support_Manager__r.Email;
                    c.Regional_Support_Manager_2_Email__c = accountQueryMap.get(c.AccountId).Support_Office__r.Regional_Support_Manager_2__r.Email;
                    c.Regional_Support_Manager_3_Email__c = accountQueryMap.get(c.AccountId).Support_Office__r.Regional_Support_Manager_3__r.Email;
                }
            }
        }// Added by UIN to improve soql execution count
    }
}