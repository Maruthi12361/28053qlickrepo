/* 2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation. */
@isTest
private class ContactCallMarketoWebServiceHelperTest {
	
	@isTest static void test_method_one() {
		User testUser = [Select id From User where id =: UserInfo.getUserId()];
		System.runAs(testUser) {
			List<Contact> contacts = new List<Contact>();            
        	Contact contact = new Contact(   FirstName = 'Test LeadCountryISOUpdate',
                                Email = 'asd@asd.com',
                                LastName = 'Lastname', 
                                QCloudID__c = 'Enterprise',
                                PartnerSourceNo__c = '12345',
                                Trigger_Webservice_Shadow__c = true,
                                Trigger_Webservice__c = true
                                );
       
        	contacts.add(contact);
        
        	Semaphores.ContactTriggerHandlerBeforeInsert = false;
        // Do insert tests
        	insert(contacts); 
		}	
	}
}