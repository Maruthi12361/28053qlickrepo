/*******************************************************************
* Class CampaignMarketingAssetLinkControllerTest
*
* Test class for CampaignMarketingAssetLinkController
* 
* Author: Linus Löfberg @ 4front
*
* Changelog:
* 2015-08-03 CR# 45769 Marketing Assets object update
*
**********************************************************************/

@isTest
private class CampaignMarketingAssetLinkContollerTest {

	@testSetup
	static void setup() {

	}
		
	@isTest static void test_getOrigin() {
		ApexPages.StandardController stdController = new ApexPages.StandardController(new Link_Marketing_Asset__c());
		CampaignMarketingAssetLinkController controller = new CampaignMarketingAssetLinkController(stdController);

		Test.startTest();
		System.assertEquals('', controller.getOrigin());

		ApexPages.currentPage().getParameters().put('Origin', 'Campaign');
		System.assertEquals('Campaign', controller.getOrigin());

		ApexPages.currentPage().getParameters().put('Origin', 'MarketingAsset');
		System.assertEquals('MarketingAsset', controller.getOrigin());
		Test.stopTest();
	}

	@isTest static void test_goBack() {
		Campaign campaign = new Campaign(Name = 'CampaignTester1');
		Marketing_Asset_NEW__c ma = new Marketing_Asset_NEW__c(Name = 'MATEST1', CurrencyIsoCode = 'USD', Asset_Title__c = 'MA-Title1', Function__c = 'IT', Language__c = 'English', Source__c = 'TP - Third Party', Type__c = 'Video');
		insert ma;
		insert campaign;

		ApexPages.StandardController stdController = new ApexPages.StandardController(new Link_Marketing_Asset__c());
		CampaignMarketingAssetLinkController controller = new CampaignMarketingAssetLinkController(stdController);

		Test.startTest();
		PageReference pageref = controller.goBack();
		System.assertEquals(null, pageref);

		ApexPages.currentPage().getParameters().put('Origin', 'Campaign');
		ApexPages.currentPage().getParameters().put('CampaignId', campaign.Id);
		controller = new CampaignMarketingAssetLinkController(stdController);
		controller.myLinkMarketingAsset.Marketing_Asset_NEW__c = ma.Id;
		System.assertEquals('/' + campaign.Id, controller.goBack().getUrl());

		ApexPages.currentPage().getParameters().put('Origin', 'MarketingAsset');
		ApexPages.currentPage().getParameters().put('MarketingAssetId', ma.Id);
		controller = new CampaignMarketingAssetLinkController(stdController);
		controller.myLinkMarketingAsset.Campaign__c = campaign.Id;
		System.assertEquals('/' + ma.Id, controller.goBack().getUrl());
		Test.stopTest();
	}

	@isTest static void test_customSaveLink() {
		Campaign campaign = new Campaign(Name = 'CampaignTester2');
		Marketing_Asset_NEW__c ma = new Marketing_Asset_NEW__c(Name = 'MATEST2', CurrencyIsoCode = 'USD', Asset_Title__c = 'MA-Title2', Function__c = 'IT', Language__c = 'English', Source__c = 'TP - Third Party', Type__c = 'Video');
		insert ma;
		insert campaign;

		ApexPages.StandardController stdController = new ApexPages.StandardController(new Link_Marketing_Asset__c());
		CampaignMarketingAssetLinkController controller = new CampaignMarketingAssetLinkController(stdController);

		Test.startTest();
		PageReference pageref = controller.customSaveLink();
		System.assertEquals(null, pageref);

		ApexPages.currentPage().getParameters().put('Origin', 'Campaign');
		ApexPages.currentPage().getParameters().put('CampaignId', campaign.Id);
		stdController = new ApexPages.StandardController(new Link_Marketing_Asset__c());
		controller = new CampaignMarketingAssetLinkController(stdController);
		controller.myLinkMarketingAsset.Marketing_Asset_NEW__c = ma.Id;
		System.assertEquals('/' + campaign.Id, controller.customSaveLink().getUrl());

		ApexPages.currentPage().getParameters().put('Origin', 'MarketingAsset');
		ApexPages.currentPage().getParameters().put('MarketingAssetId', ma.Id);
		stdController = new ApexPages.StandardController(new Link_Marketing_Asset__c());
		controller = new CampaignMarketingAssetLinkController(stdController);
		controller.myLinkMarketingAsset.Campaign__c = campaign.Id;
		System.assertEquals('/' + ma.Id, controller.customSaveLink().getUrl());
		Test.stopTest();
	}
	
	@isTest static void test_saveNewMarketingAsset() {
		Campaign campaign = new Campaign(Name = 'CampaignTester3');
		insert campaign;

		ApexPages.StandardController stdController = new ApexPages.StandardController(new Link_Marketing_Asset__c());
		CampaignMarketingAssetLinkController controller = new CampaignMarketingAssetLinkController(stdController);

		Test.startTest();
		PageReference pageref = controller.saveNewMarketingAsset();
		System.assertEquals(null, pageref);

		ApexPages.currentPage().getParameters().put('Origin', 'Campaign');
		ApexPages.currentPage().getParameters().put('CampaignId', campaign.Id);
		stdController = new ApexPages.StandardController(new Link_Marketing_Asset__c());
		controller = new CampaignMarketingAssetLinkController(stdController);

		controller.MarketingAsset = new Marketing_Asset_NEW__c(Name = 'TesterMarketingAsset', CurrencyIsoCode = 'USD', Asset_Title__c = 'Hello Test', Function__c = 'IT', Language__c = 'English', Source__c = 'TP - Third Party', Type__c = 'Video');
		System.assertEquals('/' + campaign.Id, controller.saveNewMarketingAsset().getUrl());

		Integer c = [SELECT COUNT() FROM Marketing_Asset_NEW__c WHERE Name = 'TesterMarketingAsset'];
		System.assertEquals(1, c);

		Test.stopTest();
    }
}