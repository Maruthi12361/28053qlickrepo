/*
Name:       TestTaskUpdateQlikviewSupportEscalation
Purpose:    Test class for TaskQlikviewSupportEscalationHandler.cls 
		
History
------- 
VERSION AUTHOR  DATE        CR
1.0     MTM     2013-07-02  CR#8287         https:https://eu1.salesforce.com/a0CD000000Z8JHe
        ext_vos 2017-11-28  CR#CHG0030519   Change 'goal' class. No test changes.
*/
@isTest
public class TestTaskUpdateQlikviewSupportEscalation{
    static testMethod void testTaskUpdateQlikviewSupportEscalationTrigger() {
        ID standardTaskRTId = Task.SObjectType.getDescribe().getRecordTypeInfosByName().get('Standard Tasks').getRecordTypeId();        
        
        Case c = new Case();
        insert c;
        
        Qlikview_Support_Escalation__c escalation = new Qlikview_Support_Escalation__c(Case__c = c.Id);
        insert escalation;
        System.Assert(escalation.Last_customer_communication__c == null);
        
        String description = 'Additional To:cyc@qlick.comCC:BCC:mmm@qlick.comSubject:Test descrption.';
        Task t = new Task(RecordTypeId = standardTaskRTId, WhatId = escalation.Id, Description = description, Type = 'TK - Email', Subject = 'Internal', Add_as_Case_Comment__c = false);
        insert t;
        
        t = [SELECT LastModifiedDate FROM Task WHERE Id = :t.Id];
        escalation = [SELECT Last_customer_communication__c FROM Qlikview_Support_Escalation__c where Id = :escalation.Id];
        
        //check that a casecomment was created with the same description
        System.assertEquals(t.LastModifiedDate, escalation.Last_customer_communication__c);
    }

}