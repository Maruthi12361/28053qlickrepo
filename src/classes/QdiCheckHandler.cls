/*24/02/2020 Shubham Gupta OTQ-44 handler created to cover the scenario(if partner account is updated with new QDI value, update all open zuora oppties
 	using a Batch Class)
*/
public class QdiCheckHandler {
    public static void qdiCheckUpdate(List<Account> newAccounts, Map<Id,Account> oldAccountMap){
        Map<Id,String> accountoValue = new Map<Id,String>();
        for (Account newAcc: newAccounts){
            Account oldAcc = oldAccountMap.get(newAcc.Id); 
            system.debug('@@@mainaccount method new value'+ newAcc.QDIPartnerApproved__c + ' old value'+ oldAcc.QDIPartnerApproved__c);
            system.debug('@@@mainaccount method new value'+ newAcc.Name + ' old value'+ oldAcc.Name);
            if(newAcc.QDIPartnerApproved__c != oldAcc.QDIPartnerApproved__c){
                accountoValue.put(newAcc.id, newAcc.QDIPartnerApproved__c);
            }
        }
        if(!accountoValue.isEmpty()){
            QDIOppUpdateBatch qdiBatch = new QDIOppUpdateBatch(accountoValue);
            Database.executeBatch(qdiBatch);
        }
    }
}