@IsTest
public class ReferralTriggerControllerTest {
    
    public static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
    public static Schema.DescribeSObjectResult accDescribe= gd.get('Account').getDescribe();
    public static Schema.DescribeSObjectResult oppDescribe= gd.get('Opportunity').getDescribe();
    
    public static String END_USER_ACCOUNT_RT = accDescribe.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
    public static String PARTNER_ACCOUNT_RT =  accDescribe.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
    public static String SALES_QCCS_OPP_RT = oppDescribe.getRecordTypeInfosByName().get('Qlikbuy CCS Standard').getRecordTypeId();
    
    @TestSetup
    static void createTestData() {
        
        /* Set up partner user - START*/
        Id p = [SELECT Id FROM Profile WHERE Name='PRM - Sales Dependent Territory + QlikBuy'].Id;
        Id apId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        Account acc = new Account(name ='Test Partner Account', RecordTypeId=PARTNER_ACCOUNT_RT) ;
        /* Test End User Account - START */
        
        insert acc;
            
            /* Test End User Account - END */
            
            Contact con = new Contact(LastName ='Test Partner Contact',AccountId = acc.Id,
                                     SFDCAccessGranted__c='LeadsOppsQuotes');
        
        insert con;  
        
        User partner = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='atester123@noemail.com');
        
         User accountManager = new User(alias = 'mgr123', email='manager123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = apId, IsActive =true,
                             timezonesidkey='America/Los_Angeles', username='adminer123@noemail.com');
        
        insert new List<User>{partner,accountManager};
            
        Account eua1 = new Account(name ='Test EUA1', RecordTypeId=END_USER_ACCOUNT_RT, OwnerId=accountManager.Id) ;
        insert eua1;
        
        // Share the End User Account and Account Manager user with the partner, without it partner can not create Referral
        insert new AccountShare(AccountId = eua1.Id, AccountAccessLevel='Read',RowCause='Manual',
                                UserOrGroupId=partner.Id, OpportunityAccessLevel='Read',
                               CaseAccessLevel='Read', ContactAccessLevel='Read');
        insert new UserShare(UserId = accountManager.Id, UserAccessLevel='Read',RowCause='Manual',
                                UserOrGroupId=partner.Id);
        
    }
    
    @isTest
    public static void testReferralTermsAndConditions(){
		User partner = [SELECT Id, Name,alias FROM User WHERE alias='test123' LIMIT 1];
        User accountManager = [SELECT Id, Name,alias FROM User WHERE alias='mgr123' LIMIT 1];
        Account ptAcc = [SELECT Id FROM Account WHERE name ='Test Partner Account' LIMIT 1];
        Account euAcc = [SELECT Id FROM Account WHERE name ='Test EUA1' LIMIT 1];

        Referral__c ref = new Referral__c();
        System.runAs(partner){
            ref = new Referral__c(End_User_Account__c= euAcc.Id,Referring_Partner_Account__c= ptAcc.Id,
                                  End_User_Contact_Email__c='test@noemail.com', End_User_Contact_Name__c='test contact',
                                  OwnerId= partner.Id, 
                                  Partner_Fee_PAM__c = 'Test',
                                  Accepted_Terms_and_Conditions__c = false,
                                  Partner_Fee_Type__c = 'Finder\'s Fee',
                                  Partner_Manager__c= accountManager.Id,
                                  Referral_Status__c = 'Draft');
            insert ref;
            
        }
        
        Semaphores.ReferralTriggerHandlerBeforeUpdate = false;
        try{
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
            app.setObjectId(ref.id);
            Approval.ProcessResult result = Approval.process(app);
        } catch(Exception ex){
            system.debug('Pending exception');
        }
    }
    
    @isTest
    public static void testReferralRejectReason(){
		User partner = [SELECT Id, Name,alias FROM User WHERE alias='test123' LIMIT 1];
        User accountManager = [SELECT Id, Name,alias FROM User WHERE alias='mgr123' LIMIT 1];
        Account ptAcc = [SELECT Id FROM Account WHERE name ='Test Partner Account' LIMIT 1];
        Account euAcc = [SELECT Id FROM Account WHERE name ='Test EUA1' LIMIT 1];

        Referral__c ref = new Referral__c();
        System.runAs(partner){
            ref = new Referral__c(End_User_Account__c= euAcc.Id,Referring_Partner_Account__c= ptAcc.Id,
                                  End_User_Contact_Email__c='test@noemail.com', End_User_Contact_Name__c='test contact',
                                  OwnerId= partner.Id, 
                                  Partner_Fee_PAM__c = 'Test',
                                  Partner_Fee_Type__c = 'Finder\'s Fee',
                                  Accepted_Terms_and_Conditions__c = true,
                                  Partner_Manager__c= accountManager.Id,
                                  Referral_Status__c = 'Draft');
            insert ref;
            
        }
        
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(ref.id);
        Approval.ProcessResult result = Approval.process(app);
        system.debug('Approval.ProcessResult - ' + result);
        
        Semaphores.ReferralTriggerHandlerBeforeUpdate = false;
        try{
            ref.Referral_Status__c = 'Rejected';
            update ref;
        } catch(Exception ex){
            system.debug('Rejected exception');
        }
    }
    
    @isTest
    public static void testReferralUpdateOppPAMFields(){
        QTTestUtils.GlobalSetUp();
		User partner = [SELECT Id, Name,alias FROM User WHERE alias='test123' LIMIT 1];
        User accountManager = [SELECT Id, Name,alias FROM User WHERE alias='mgr123' LIMIT 1];
        Account ptAcc = [SELECT Id FROM Account WHERE name ='Test Partner Account' LIMIT 1];
        Account euAcc = [SELECT Id FROM Account WHERE name ='Test EUA1' LIMIT 1];
        
        User user = QTTestUtils.createMockOperationsAdministrator();
        
        Id qbId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
        system.assert(qbId != null);
        Opportunity opp = QTTestUtils.createMockOpportunity('Test Opp', 'Test Opp', 'New Customer', 'Direct', 'Prospecting', qbId, 'GBP', user, true);
            	    

        Referral__c ref = new Referral__c();
        System.runAs(partner){
            ref = new Referral__c(End_User_Account__c= euAcc.Id,Referring_Partner_Account__c= ptAcc.Id,
                                  End_User_Contact_Email__c='test@noemail.com', End_User_Contact_Name__c='test contact',
                                  OwnerId= partner.Id, 
                                  Partner_Fee_PAM__c = 'Test',
                                  Partner_Fee_Type__c = 'Finder\'s Fee',
                                  Accepted_Terms_and_Conditions__c = true,
                                  Partner_Manager__c= accountManager.Id,
                                  Referral_Status__c = 'Pending SSD Approval');
            insert ref;
               
        }
        Test.startTest();
        system.runAs(accountManager){
            ref.opportunity__c = opp.Id;
            ref.SSD_Approved__c = true;
            update ref;
            Semaphores.ReferralTriggerHandlerBeforeUpdate = false;
            opp.StageName = 'Closed Won';
            update opp;
        }
        
        Test.stopTest();
    }
    
    
    @isTest
    public static void testReferralOpportunityAssociation(){
		QTTestUtils.GlobalSetUp();
        
        User partner = [SELECT Id, Name,alias FROM User WHERE alias='test123' LIMIT 1];
        User accountManager = [SELECT Id, Name,alias FROM User WHERE alias='mgr123' LIMIT 1];
        Account ptAcc = [SELECT Id FROM Account WHERE name ='Test Partner Account' LIMIT 1];
        Account euAcc = [SELECT Id FROM Account WHERE name ='Test EUA1' LIMIT 1];
        User user = QTTestUtils.createMockOperationsAdministrator();
        
        Id qbId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
        system.assert(qbId != null);
        Opportunity opp = QTTestUtils.createMockOpportunity('Test Opp', 'Test Opp', 'New Customer', 'Direct', 'Prospecting', qbId, 'GBP', user, true);
            	    
        Referral__c ref = new Referral__c();
        System.runAs(partner){
            ref = new Referral__c(End_User_Account__c= euAcc.Id,Referring_Partner_Account__c= ptAcc.Id,
                                              End_User_Contact_Email__c='test@noemail.com', End_User_Contact_Name__c='test contact',
                                              OwnerId= partner.Id, 
                                  			  Partner_Fee_PAM__c = 'Test',
                                  			  Partner_Fee_Type__c = 'Finder\'s Fee',
                                  			  Accepted_Terms_and_Conditions__c = true,
                                  			  Partner_Manager__c= accountManager.Id,
                                              Referral_Status__c = 'Draft');
            insert ref;
            
        }
        
        Semaphores.ReferralTriggerHandlerBeforeUpdate = false;
        try{
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
            app.setObjectId(ref.id);
            Approval.ProcessResult result = Approval.process(app);
        } catch(Exception ex){
            system.debug('Pending exception');
        }
        
    }

}