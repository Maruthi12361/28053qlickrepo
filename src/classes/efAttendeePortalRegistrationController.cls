//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efAttendeePortalRegistrationController{
    private User user;
    private efRegistrationManager regManager;
    private efRegistrationValidator regValidator;
    private efAppNavigator navigator;
    private boolean isInvalidData = false;
   	private Boolean inEdition = false;      
    private boolean separateNameOnBadge = true;
    private boolean validatedOnRegisterPage = false;
    private String message = '';
    private String eventBannerUrl;
    private string contactId;
    
    
	// Initialisation methods.
        
    public efAttendeePortalRegistrationController(){
        if(user==null){
            initialize();
        }
    }
    
    public String getEventName(){
    	return regManager.getEventWrapper().getEventName();
    }    
    
    public void initialize(){
		// Comment & : validate input registration Id.
    	String registrationId = ApexPages.currentPage().getParameters().get('registrationId');    	
    	contactId = efUtility.getContactIdFromRegistration(registrationId);
    	if (checkInitialState()!=null) return;    	
    	
    	// Comment& : efRegistrationManager creates and holds 
    	// efRegistrationWrapper, efContactWrapper instances.     	
        regManager = new efRegistrationManager(contactId, registrationId);        
        regValidator = new efRegistrationValidator(regManager);
        
        // Comment& : efAppNavigator encapsulates page navigation logic.
        navigator = new efAppNavigator(regManager.getEventWrapper(), registrationId);
        navigator.setCurrentPage(efAppNavigator.ATTENDEE_PORTAL_REGISTRATION);
    }
    
    public PageReference checkInitialState(){
    	try {
    		String regId = ApexPages.currentPage().getParameters().get('registrationId');
    		if (regId==null || regId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    		if (contactId==null || contactId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);     		
    		return efRegistrationManager.checkRegistrationIdPortal(regId);
    	} catch (Exception e){
    		return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    	}
    }    
    
    public PageReference checkState(){
        if (regManager==null) // Comment & : no registration manager instance = invalid state.
        	return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
        else
        	return null;
    }
    
    public Boolean getShowSessions(){
    	if (regManager==null)
    		return false;
    	else
    		return regManager.getEventWrapper().getShowSessions();
    }
    
    public Boolean getShowActivities(){
    	if (regManager==null)
    		return false;
    	else    	
    		return regManager.getEventWrapper().getShowActivities();
    }
    // End Initialisation methods.


	// Furniture methods.

    public String getEventBannerUrl(){    	
    	if (eventBannerUrl==null || eventBannerUrl.length()==0){
    		eventBannerUrl = getDocumentUrl(regManager.getEventWrapper().getEvent().Banner_Image_Document__c);
    	}
    	return eventBannerUrl;
    }
    
    private String getDocumentUrl(String documentUniqueName){
    	Document doc = [Select d.Id From Document d Where d.DeveloperName=:documentUniqueName Limit 1];
    	String imageid = doc.id; 
    	imageid = imageid.substring(0,15);
    	return '/servlet/servlet.FileDownload?file=' + imageid;
    }
	// End Furniture methods.

	
	// Registration methods.
    
    public boolean getIsEmployee(){   
        return efUtility.isAttendeeType(efConstants.ATTENDEE_TYPE_EMPLOYEE,
                                      regManager.getRegistrationWrapper().getRegistration());
    }
    
    public efRegistrationWrapper getAttendeeRegistration(){
    	if (regManager==null) return null;
        return regManager.getRegistrationWrapper();
    }
    
    public efContactWrapper getAttendeeContact(){
    	if (regManager==null) return null;
        return regManager.getContactWrapper();
    }
    
    public String getIndustry(){
        return efUtility.giveValue(regManager.getAccount().Industry);
    }
    
  	public void setIndustry(String i){
        regManager.getAccount().Industry = i;
    }
    
    public PageReference cancelRegistration(){       
        
        regManager.getRegistrationWrapper().setStatus(efConstants.REG_STATUS_CANCELLATION_REQUESTED);        
        try{
        	regManager.flushRegistration();
        } catch(Exception e) {
            //System.debug('Error cancelreg');
        }
        return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    }
    
    
    // Accessors.
    
    public String getAttendeeType(){
        return regManager.getAttendeeType();
    }
    
    public boolean getSeparateNameOnBadge(){
        return this.separateNameOnBadge;
    }
    
    public String getSeparateNameOnBadgeText(){
        if(this.separateNameOnBadge!=null && this.separateNameOnBadge)
            return 'Yes';
        else
           return 'No';
    }
    
    public void setSeparateNameOnBadge(boolean separateNameOnBadge){
        this.separateNameOnBadge = separateNameOnBadge;
    }
    
    //----BUSINESS INFORMATION
    public String getBusinessName(){
        return efUtility.giveValue(regManager.getAccount().Name);
    }
    
    public void setBusinessName(String bName){
        regManager.getAccount().Name = bName;
    }
    
    public String getBusinessPhone(){
        return efUtility.giveValue(regManager.getAccount().Phone);
    }
    
    public void setBusinessPhone(String businessPhone){
        regManager.getAccount().Phone = businessPhone;
    }
    
    public boolean getValidatedOnRegisterPage(){
        return this.validatedOnRegisterPage;
    }
    
    public String getMessage(){
        return message;
    }
    
    public efPicklists getPicklist(){
        efPicklists p = new efPicklists();
        return p;
    }
    
    public boolean getIsInvalidData(){
        return this.isInvalidData;
    }

    public efRegistrationValidator getRegValidator(){
        return regValidator;
    }
    
    public boolean isValidRegisterData(){
        if (!regValidator.getIsValidRegistrationData()){
            validatedOnRegisterPage = true;
            return false;
        }
        else
            return true;
    }

    public PageReference saveAdditional(){
        if (!isValidRegisterData()){   
           isInvalidData = true; 
           return new PageReference('/apex/efAttendeePortalRegistration') ;
        } else{   
            isInvalidData = false ; 

            try{
                regManager.flushRegistration();
            }
            catch(Exception e){
                //System.debug('ERROR UPDATING RECORDS');
            }   
            
            regValidator = new efRegistrationValidator(regManager);
            
            inEdition = false;            
            return new PageReference('/apex/efAttendeePortalRegistration') ;
        }
    }
           
    public boolean isAttendeeType(string stype){   
        return efUtility.isAttendeeType(stype, regManager.getRegistrationWrapper().getRegistration());
    }
    
    public efRegistrationWrapper getRegistration(){
        return regManager.getRegistrationWrapper();
    }
    
    public String getRegistrationId(){
        return regManager.getRegistrationWrapper().getRegistration().Id ;
    }
            
    public String getCompanySize(){   
        return efUtility.giveValue(regManager.getAccount().company_Size__c);
    }
    
    public void setCompanySize(String cs){
        regManager.getAccount().Company_Size__c = cs;
    } 

    public Boolean getInEdition(){
        return inEdition;
    }
    
    public void startEdition(){
        inEdition = true;
        validatedOnRegisterPage = false;
    }
     
    public PageReference cancelEdition(){
        PageReference pageRef = new PageReference('/apex/efAttendeePortalRegistration');        
        pageRef.setRedirect(true);        
        return pageRef;
    }
}