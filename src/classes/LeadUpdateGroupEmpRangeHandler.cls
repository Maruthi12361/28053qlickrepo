/******************************************************
Class: LeadUpdateGroupEmpRangeHandler

Changelog:    
	2017-10-25 AYS BMW-402 : Migrated from UpdateGroupEmpRangeLead.trigger.
******************************************************/ 

public class LeadUpdateGroupEmpRangeHandler {
	public LeadUpdateGroupEmpRangeHandler() {
		
	}

	public static void handle(List<Lead> triggerNew) {
		for (Lead L:triggerNew)
		{
			if (L.NumberOfEmployees != null & L.NumberOfEmployees != 0) {
					L.Group_Employee_Range__c = picklisttranslations.ReturnEmployee(L.NumberOfEmployees);
					system.debug('*** Trigger says GER = ' + L.Group_Employee_Range__c);
					
					L.NumberOfEmployees = null;
					system.debug('*** Trigger now says GER = ' + L.Group_Employee_Range__c);
	
			}
		}
	}
}