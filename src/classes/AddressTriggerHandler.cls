/****************************************************************
*
* AddressTriggerHandler
*
* Address trigger handler class.
*
* 2016-10-01 UIN Added reset logic for valid address checkbox on before insert
* 24.02.2017 ROV
* 2017-06-14 QCW-1733 Linus Löfberg - Add catching addresses before manual approvals and set to pending status if equal to another.
*
*****************************************************************/
public with sharing class AddressTriggerHandler {
    private boolean m_isExecuting = false;
    private boolean tryToCheckIns = false;//Rodion adding
    private boolean tryToCheckUpd = false;//Rodion adding
    private integer BatchSize = 0;
    ECustomsHelper helper;
    //AddressNotificationSender notifier;
    public static Boolean beforeUpdateFlag = false;

    public AddressTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
        helper = new ECustomsHelper();
        //notifier = new  AddressNotificationSender();
    }

    public void OnBeforeInsert(Address__c[] newAddresss){
        for(Address__c add: newAddresss){
            if(add.isClone())
                add.Valid_Address__c = false;
        }
        //helper.updateCheckRedCountry(newAddresss);
    }

    public void OnAfterInsert(Address__c[] newAddresss){

    }

    @future (callout=true)
    public static void OnAfterInsertAsync(Set<ID> newAddressIDs){
         AddressTriggerHelper.VerifyAddress(newAddressIDs);
    }

    public void OnBeforeUpdate(Address__c[] oldAddresss, Address__c[] updatedAddresss, Map<ID, Address__c> newAddressMap, Map<ID, Address__c> oldAddressMap){
        //processAddressInvalidation(updatedAddresss, oldAddressMap);
        System.debug('Start before update!');
        //helper.updateCheckRedCountry(updatedAddresss);
        //AddressApprovalHelper.catchEqualAddresses(oldAddresss, updatedAddresss, newAddressMap, oldAddressMap);
        System.debug('End before update!');
    }

    public void OnAfterUpdate(Address__c[] oldAddresss, Address__c[] updatedAddresss, Map<ID, Address__c> newAddressMap, Map<ID, Address__c> oldAddressMap){
        //AddressTriggerHelper.VerifyAddress(oldAddressMap.keySet());
        //notifier.onAfterUpate(updatedAddresss, oldAddressMap);
        System.debug('Start after update!');
        //AddressApprovalHelper.catchGrantedOrApprovedAddresses(oldAddresss, updatedAddresss, newAddressMap, oldAddressMap);
        System.debug('End after update!');
    }

    @future (callout=true)
    public static void OnAfterUpdateAsync(Set<ID> updatedAddressIDs){
		System.debug('Update address');
        AddressTriggerHelper.VerifyAddress(updatedAddressIDs);
    }

    public void OnBeforeDelete(Address__c[] AddresssToDelete, Map<ID, Address__c> AddressMap){

    }

    public void OnAfterDelete(Address__c[] deletedAddresss, Map<ID, Address__c> AddressMap){

    }

    @future public static void OnAfterDeleteAsync(Set<ID> deletedAddressIDs){

    }

    public void OnUndelete(Address__c[] restoredAddresss){

    }

    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }

    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }

    private static void processAddressInvalidation(List<Address__c> newInputList, Map<ID, Address__c> oldAddressMap) {
            /* Commented as part of BSL-449 as method not used anymore
			for(Address__c itemAddressNew : newInputList) {
                if(itemAddressNew.Address__c != oldAddressMap.get(itemAddressNew.id).Address__c) {
					if(!Test.isRunningTest()){
						itemAddressNew.Valid_Address__c = false;
					}
                }
            } */
    }
}