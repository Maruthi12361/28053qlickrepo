/*
* File ChangeControlBussOwnerApprl_SubmitTest
    * @description : Unit test for ChangeControlBusinessOwnerApprovalCtrl
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       28.12.2017   Pramod Kumar V      Created Class

*/
@isTest(seeAlldata=false)
private class ChangeControlBussOwnerApprl_SubmitTest{
    
   
    static testMethod void test_ChangeControlBusinessOwnerApprovalCtrl() {
    
    User u = [SELECT Id, Name FROM User where Id= :System.Userinfo.getUserId()];
    Id rtId4 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
    Account testAccount3 =new Account(name='Test AccountName2', recordtypeid=rtId4,QlikTech_Company__c='test',Territory_Country__c='India',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222');
          insert testAccount3;
          testAccount3.Legal_Approval_Status__c='Legal Approval Requested';
          update testAccount3;
   PageReference pageRef=page.ChangeControlBusinessOwnerApproval;
   Test.setCurrentPage(pageRef);
    ApexPages.StandardController sc=new ApexPages.StandardController(testAccount3);
    ChangeControlBusinessOwnerApprovalCtrl  BusinessApprl=new ChangeControlBusinessOwnerApprovalCtrl (sc);  
    BusinessApprl.save();
    BusinessApprl.ErrorMessage='test';
    BusinessApprl.Visible=true;
    BusinessApprl.ProcessInstanceWorkItemId='test';
       
    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
    req.setComments('Submitted for approval.');
    req.setObjectId(testAccount3.Id);
    req.setSubmitterId(u.Id);
    req.setProcessDefinitionNameOrId('Test_Migration');
    req.setSkipEntryCriteria(true);
    //Approval.ProcessResult result = Approval.process(req);
      //add the request to the list
      
    List<Approval.Processsubmitrequest> ProcessSubmitRequestList = new List<Approval.Processsubmitrequest>();
    ProcessSubmitRequestList.add(req);
    List<Approval.ProcessResult> resultList = Approval.process(ProcessSubmitRequestList, false);
     
     List<Id> newWorkItemIds = resultList[0].getNewWorkitemIds();

    
    Approval.ProcessWorkitemRequest req2 =new Approval.ProcessWorkitemRequest();
    req2.setComments('Approving request.');
    req2.setAction('Approve');
    //req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

    //req2.setWorkitemId(newWorkItemIds.get(0));
   // Approval.ProcessResult result2 =  Approval.process(req2);
    ApexPages.currentPage().getParameters().put('id',testAccount3.Id);
    
    
    }
  }