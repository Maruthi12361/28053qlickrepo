/**********************************************************
* Class: 		PseProjectTriggerHandler
* Description: 	Implements logic for PseProjectTrigger trigger.
* Autor:		ext_vos
* Date:			2017-11-13
* Changes Log:
*	Date        author				CR#			Description
*
*	2010-09-05	Jason Favors (Appirio)			A trigger to check that at least one budget is approved before activating a Project.
*												(Moved from PseValidateApprovedBudgetTrigger.trigger)	
*	2010-09-14	Jason Favors (Appirio)			Handle project closures, to include 'Cancelled'. 
*												When a project is cancelled (or closed or completed), all assignments are closed.
*												(Moved from PseProjectClosedTrigger.trigger)		
*	2012-11-13	Alberto de Diego	6338		Initial Development	(Moved logic from ProjectPreviousProjectManager.trigger)
*	2013-10-07	Madhav Kakani, 		CR#9634		Logic for CR#9634 (Moved from PseProjectClosedTrigger.trigger)
*				Fluido Oy		
*	2017-11-14	ext_vos				CHG0031444	Add the logic: fill in Engagement Partner
*   2018-03-29  ext_vos             CHG0031444  Add new: 'Payment Terms' this field should be populated from primary opportunity quote.
*   2019-04-02  ext_vos             CHG0034979  Update validateApprovedBudgetOnUpdate condition: check pse__Status__c too.
*************************************************************/
public class PseProjectTriggerHandler {
	
    private static String errorBudgetMessage = 'Cannot mark a project as \'Billable\' without an approved budget.';

	public static void handleBeforeInsert(List<pse__Proj__c> newProjects) {
		for (pse__Proj__c newProject : newProjects) {
			validateApprovedBudgetOnInsert(newProject);
			fillEngagementPartner(newProject);            
		}
        fillPaymentTerms(newProjects);
	}
	
	public static void handleBeforeUpdate(List<pse__Proj__c> oldProjects, List<pse__Proj__c> newProjects, 
											Map<Id, pse__Proj__c> oldMapProjects, Map<Id, pse__Proj__c> newMapProjects) {
		
		validateApprovedBudgetOnUpdate(oldMapProjects, newMapProjects);

		for (Integer i = 0; i < newProjects.size(); i++) {
	        pse__Proj__c oldProject = oldProjects[i];
	        pse__Proj__c newProject = newProjects[i];
			
			validatePreviousManager(oldProject, newProject);
			fillEngagementPartner(newProject);
		}
	}

	public static void handleAfterInsert(List<pse__Proj__c> newProjects) {
		createProjectsSE(newProjects);
	}

	public static void handleAfterUpdate(List<pse__Proj__c> oldProjects, List<pse__Proj__c> newProjects,
											Map<Id, pse__Proj__c> oldMapProjects, Map<Id, pse__Proj__c> newMapProjects) {
		updateAssignments(newProjects, oldMapProjects, newMapProjects);
		updateProjectsSE(newProjects);
	}

	public static void handleAfterDelete(List<pse__Proj__c> oldProjects) {
		deleteProjectsSE(oldProjects);
	}


	//CR# 9634 
	private static void createProjectsSE(List<pse__Proj__c> projects) {
		List<Project_SE__c> lstprojSE = new List<Project_SE__c>();        

        for (pse__Proj__c project : projects) { // copy the project
            Project_SE__c projSE = new Project_SE__c();
            projSE.Name = project.Name;
            projSE.Master_Project_Id__c = project.Id;
            projSE.Account__c = project.pse__Account__c;
            projSE.Is_Active__c = project.pse__Is_Active__c;
            projSE.Start_Date__c = project.pse__Start_Date__c;
            projSE.End_Date__c = project.pse__End_Date__c;
            projSE.Primary_Contact__c = project.Primary_Contact__c;
            projSE.Project_Manager__c = project.pse__Project_Manager__c;
            projSE.Region__c = project.pse__Region__c;
            
            lstprojSE.add(projSE);
        }
        if ((lstprojSE != null) && (lstprojSE.size() > 0)) { 
            database.insert(lstprojSE, false); 
        }
	}

	//CR# 9634 
	private static void updateProjectsSE(List<pse__Proj__c> projects) {
		Map<Id, pse__Proj__c> mapProj = new Map<Id, pse__Proj__c>(); // map of projects
        for (pse__Proj__c project : projects) { 
            mapProj.put(project.Id, project); 
        }

        // get the list of matching project SE objects
        List<Project_SE__c> projSE = [SELECT Id, Master_Project_Id__c, Name, Is_Active__c, Start_Date__c,
                                        End_Date__c, Primary_Contact__c, Project_Manager__c, Account__c, Region__c
                                        FROM Project_SE__c 
                                        WHERE Master_Project_Id__c IN :mapProj.keySet()];
        
        for (Integer i = 0; i < projSE.size(); i++) {
            pse__Proj__c project = mapProj.get(projSE[i].Master_Project_Id__c);
            
            if (project == null) continue; 

            projSE[i].Name = project.Name;
            projSE[i].Account__c = project.pse__Account__c;
            projSE[i].Is_Active__c = project.pse__Is_Active__c;
            projSE[i].Start_Date__c = project.pse__Start_Date__c;
            projSE[i].End_Date__c = project.pse__End_Date__c;
            projSE[i].Primary_Contact__c = project.Primary_Contact__c;
            projSE[i].Project_Manager__c = project.pse__Project_Manager__c;
            projSE[i].Region__c = project.pse__Region__c;

        }
        if ((projSE != null) && (projSE.size() > 0)) { 
            database.update(projSE, false); 
        }
	}

	//CR# 9634 
	private static void deleteProjectsSE(List<pse__Proj__c> projects) {
		Set<Id> masterProj = new Set<Id>(); // list of projects
        for (pse__Proj__c project : projects) { 
            masterProj.add(project.Id); 
        }
        // get the list of matching project SE objects
        List<Project_SE__c> projSE = [SELECT Id FROM Project_SE__c WHERE Master_Project_Id__c IN :masterProj];        
        if ((projSE != null) && (projSE.size() > 0)) { 
            database.delete(projSE, false); 
        }
	}

	//2010-09-14	Jason Favors (Appirio)
	private static void updateAssignments(List<pse__Proj__c> newProjects, Map<Id, pse__Proj__c> oldMapProjects, Map<Id, pse__Proj__c> newMapProjects) {
		Set<Id> closedProjectIds = new Set<Id>();
        pse__Proj__c newProject;
        pse__Proj__c oldProject;
        for (pse__Proj__c project : newProjects) {
            newProject = newMapProjects.get(project.Id);
            oldProject = oldMapProjects.get(project.Id);

            if (newProject.pse__Stage__c != oldProject.pse__Stage__c 
            		&& (newProject.pse__Stage__c == 'Cancelled' || newProject.pse__Stage__c == 'Completed' || newProject.pse__Stage__c == 'Closed')) {
                closedProjectIds.add(project.Id);
            }
        }

        // close related assignments
        List<pse__Assignment__c> closeAssignments = [SELECT Id 
                                                    FROM pse__Assignment__c 
                                                    WHERE pse__Project__c in :closedProjectIds AND pse__Status__c != 'Closed'];
        for (pse__Assignment__c assignment : closeAssignments) {
            assignment.pse__Status__c = 'Closed';
        }
        update closeAssignments;
	}

	//2012-11-13 Alberto de Diego: CR# 6338 Initial Development
	//don't modify if previous manager is null or previous manager hasn't changed or manager hasn't changed
	private static void validatePreviousManager(pse__Proj__c oldProject, pse__Proj__c newProject) {
	    Id prevManagerId = oldProject.OwnerId;      
        
        //don't modify if previous manager is null or previous manager hasn't changed or manager hasn't changed
        if (prevManagerId != null && newProject.Previous_Owner__c != prevManagerId && newProject.OwnerId != prevManagerId) {            
            newProject.Previous_Owner__c = prevManagerId;
        }
	}

	// 2010-09-05	Jason Favors (Appirio)
	// Can't insert if billable and active, as no approved budgets
	private static void validateApprovedBudgetOnInsert(pse__Proj__c project) {
        if (project.pse__Is_Billable__c && project.pse__Is_Active__c) {
            project.addError(errorBudgetMessage);
        }        
	}

	// 2010-09-05	Jason Favors (Appirio)
	private static void validateApprovedBudgetOnUpdate(Map<Id, pse__Proj__c> oldMapProjects, Map<Id, pse__Proj__c> newMapProjects) {
		// assemble a list of the project IDs
        Set<Id> projectIds = newMapProjects.keySet();

        // assemble a list of projects with budgets
        List<pse__Proj__c> projectsIncludingBudgets = [SELECT Id, Name, (SELECT Id, pse__Approved__c FROM pse__Budgets__r 
                                                                        WHERE pse__Approved__c = true or pse__Status__c = 'Approved')
										                FROM pse__Proj__c
										                WHERE Id in :projectIds];

        if (projectsIncludingBudgets != null && projectsIncludingBudgets.size() > 0) {

            List<pse__Budget__c> approvedBudgets;
            pse__Proj__c newProject;
            pse__Proj__c oldProject;
            for (pse__Proj__c project : projectsIncludingBudgets) {
                approvedBudgets = project.pse__Budgets__r;
                newProject = newMapProjects.get(project.Id);
                oldProject = oldMapProjects.get(project.Id);
                
                // only check if changing Billable/Active
                if (newProject.pse__Is_Billable__c != oldProject.pse__Is_Billable__c 
                		|| newProject.pse__Is_Active__c != oldProject.pse__Is_Active__c) {
                	// Can't update if billable and active with no approved budgets
                    if (newProject.pse__Is_Billable__c && newProject.pse__Is_Active__c && (approvedBudgets == null || approvedBudgets.size() <= 0)) {
                        newProject.addError(errorBudgetMessage);
                    } else {
                        System.debug('Update Project - Total approved budgets: ' + approvedBudgets.size());
                    }
                }
            }
        }
	}

	private static void fillEngagementPartner(pse__Proj__c newProject) {	
		if (newProject.Engagement_Model__c == 'Partner-Led') {
			newProject.Engagement_Partner__c = newProject.pse__Account__c;
		}
	}

    // CHG0031444
    private static void fillPaymentTerms(List<pse__Proj__c> newProjects) {
        Set<Id> opportunities = new Set<Id>();
        for (pse__Proj__c pr : newProjects) {
            if (pr.pse__Opportunity__c != null) {
                opportunities.add(pr.pse__Opportunity__c);
            }
        }
        Map<String, String> opportunityAndTerms = new Map<String, String>();
        if (!opportunities.isEmpty()) {
            List<Opportunity> opps = [select SBQQ__PrimaryQuote__c, SBQQ__PrimaryQuote__r.SBQQ__PaymentTerms__c 
                                        from Opportunity where Id in: opportunities];
            for (Opportunity opp : opps) {
                if (opp.SBQQ__PrimaryQuote__c != null && opp.SBQQ__PrimaryQuote__r.SBQQ__PaymentTerms__c != null) {
                    opportunityAndTerms.put(opp.Id, opp.SBQQ__PrimaryQuote__r.SBQQ__PaymentTerms__c);
                }
            }
        }
        if (!opportunityAndTerms.isEmpty()) {
            for (pse__Proj__c proj : newProjects) {
                if (proj.pse__Opportunity__c != null && opportunityAndTerms.get(proj.pse__Opportunity__c) != null) {
                    proj.Payment_Terms__c = opportunityAndTerms.get(proj.pse__Opportunity__c);
                }
            }
        }
    }
}