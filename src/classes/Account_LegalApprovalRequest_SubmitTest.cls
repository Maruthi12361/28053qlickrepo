/*
* File Account_LegalApprovalRequest_SubmitTest
    * @description : Unit test for Account_LegalApprovalRequest_SubmitForApproval
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       20.12.2017   Pramod Kumar V         Created Class
    29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta

*/
@isTest(seeAlldata=false)
private class Account_LegalApprovalRequest_SubmitTest{
    
   
    static testMethod void test_AccountLegalApprovalRequestSubmitForApproval() {
       Id rtId4 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
          Account testAccount3 =new Account(name='Test AccountName2', recordtypeid=rtId4,QlikTech_Company__c='test',Territory_Country__c='India',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222');
          insert testAccount3;
          testAccount3.Legal_Approval_Status__c='Legal Approval Requested';
          Semaphores.CustomAccountTriggerAfterUpdate = false;
          update testAccount3;
          //Contact conPartner1 = QuoteTestHelper.createContact(testAccount3.id);
          //insert conPartner1;
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
      req.setComments('Submitted for approval.');
      req.setObjectId(testAccount3.Id);
      //add the request to the list
     // Approval.ProcessResult result = Approval.process(req);
     List<Approval.Processsubmitrequest> ProcessSubmitRequestList = new List<Approval.Processsubmitrequest>();
      ProcessSubmitRequestList.add(req);
       List<Approval.ProcessResult> resultList = Approval.process(ProcessSubmitRequestList, false);
    }
  }