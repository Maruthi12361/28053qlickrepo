/**
  2019_08_08 : CCE BMW-1663
**/
global class DeactivatePartnerGoldLicenses Implements Schedulable
{
    public static String schedDaily = '0 00 04 * * ?';  //Daily 4am
    // used for scheduling a repeating job
    global static String scheduleMeDaily() {
        DeactivatePartnerGoldLicenses job = new DeactivatePartnerGoldLicenses(); 
        if (test.isRunningTest()) return null;
        else return System.schedule('DeactivateGoldLicenses', schedDaily, job);
    }
    global void execute(SchedulableContext sc)
    {
        DeactivateGoldLicenses();
    }

    public void DeactivateGoldLicenses()
    {
        List<User> lstUsers = new List<User>();
        lstUsers = [Select Id, IsActive From User Where IsActive = true AND Profile.UserLicense.Name = 'Gold Partner' AND LastModifiedDate != LAST_N_DAYS:30 AND (LastLoginDate = null OR LastLoginDate != LAST_N_DAYS:180) AND (Profile__c = 'PRM - Base' OR Profile__c = 'PRM - Base (READ ONLY)' OR Profile__c = 'PRM - Base JAPAN' OR Profile__c = 'PRM - Sales Dependent Territory + QlikBuy' OR Profile__c = 'PRM - Sales Dependent Territory + QlikBuy (READ ONLY)') Limit 200];
        //lstUsers = [Select Id, IsActive From User Where IsActive = true AND Profile.UserLicense.Name = 'Gold Partner' AND LastModifiedDate != LAST_N_DAYS:30 AND Contact.LastModifiedDate != LAST_N_DAYS:30 AND (LastLoginDate = null OR LastLoginDate != LAST_N_DAYS:180) AND (Profile__c = 'PRM - Base' OR Profile__c = 'PRM - Base (READ ONLY)' OR Profile__c = 'PRM - Base JAPAN' OR Profile__c = 'PRM - Sales Dependent Territory + QlikBuy' OR Profile__c = 'PRM - Sales Dependent Territory + QlikBuy (READ ONLY)') Limit 200];
        for(User usr : lstUsers) { usr.IsActive = false; }
        system.Debug('DeactivatePartnerGoldLicenses: lstUsers.size = ' + lstUsers.size());
        if ((lstUsers.size() > 0) && (!test.isRunningTest())) { update lstUsers; }
    }
}