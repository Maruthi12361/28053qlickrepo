/**     * File Name:PopulateSlaProcessName
        * Description : This is the batch class to populate the sla process name
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Feb 3rd 2017 RamakrishnaKini     added batch logic
*/
global class PopulateSlaProcessName implements Database.Batchable<sObject>, Database.Stateful{
    
    String query;
    global List<String> exception_List;
    
    global PopulateSlaProcessName() {
        if(exception_List == null)
            exception_List = new List<String>();
        query = 'select id, SlaProcessid, SlaProcess.name from Entitlement';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Entitlement> scope) {
        List<Entitlement> lstUpdatedEntitlements = new list<Entitlement>(); 
        for(Entitlement e: scope){
            if(String.isNotBlank(e.SlaProcessId)){
                e.SLA_Process_Name__c = e.slaprocess.name;
                lstUpdatedEntitlements.add(e);
            }
        }
        List<Database.SaveResult> sResults = Database.update(lstUpdatedEntitlements,false);
        for(integer i =0; i<sResults.size();i++){
            String msg='';
            If(!sResults[i].isSuccess()){
                msg += 'Entitlement Record with Id:'+ lstUpdatedEntitlements.get(i).Id + ' and Name: '+ lstUpdatedEntitlements.get(i).Name + 'failed due to the Error: "';        
                for(Database.Error err: sResults[i].getErrors()){  
                     msg += err.getmessage()+'<br/>';
                } 
            }
        if(msg!='' && msg.contains('Error:'))
            exception_List.add(msg);
        } 
        
        if(exception_List.size() > 200)
            sendErrorMails(exception_List);
    }
    @TestVisible
    private void sendErrorMails(List<String> exceptions){
        String errMsg = 'The batch Apex job errors for Records processed till now are' +'<br/>';
        String temp='';
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'ain@qlik.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Populate SLA Process Name Batch Job Status In-Progress');
        if(!exceptions.isEmpty()){
            for(String str:exceptions)
                temp = temp + str;
            mail.setHTMLBody(errMsg + temp);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            exception_List = new List<String>();
        }  
    }
    
    global void finish(Database.BatchableContext BC) {
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                            TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob WHERE Id =
                            :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.settargetobjectId(a.CreatedBy.ID);
        mail.setSaveAsActivity(false);
        mail.setSubject('Populate SLA Process Name Batch Job Status ' + a.Status);
        mail.setPlainTextBody('The batch job has finished.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}