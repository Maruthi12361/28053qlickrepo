/****************************************************************
*
*  QuoteLineTableController
*
* 06.10.2017 : Reshma Ravi - QCW-4035[Included Subscription Product family]
* 03/08/2019   : Anjuna Baby - BSL-1609 - Added Asset query and created new list of quoteline subline for new license delivery email template.
* 16/04/2019 : Anjuna Baby - BSL-1877 - Modified the logic to work for all scenarios like new, buy more, amend etc.
* 6/6/2019   : Anjuna Baby - BSL-2049 - Added Signed License Key to CPQ License emails.
*****************************************************************/
public with sharing class QuoteLineTableController {
    public Id quoteId {get; set;}
    public Id userId {get; set;}
    public String customerAcc{get; set;}
    public String quoteName{get; set;}
    public String partnerAccName{get; set;}
    public Boolean dualUseSite {get;set;}
    public Boolean isSLKAvailable{get;set;}
    
    private static final String PROFILECONST1 = '';
    private static final String PROFILECONST2 = '';   
    
    public List<Quote_Line_Sub_Line__c> getQuoteLineWrapperList() {
        List<Quote_Line_Sub_Line__c> listOfQuoteLineWrapperToReturn = new List<Quote_Line_Sub_Line__c>();
        Set<Id> quoteLineSet = new Set<Id>();
        

        for(SBQQ__QuoteLine__c item : [Select id, SBQQ__Quote__c,Name,SBQQ__Quantity__c,SBQQ__Product__r.Name, SBQQ__StartDate__c,SBQQ__EndDate__c,SBQQ__Product__r.Family, Evaluation_Expiry_Date__c,Control_Number__c, License_Key_Number__c From SBQQ__QuoteLine__c Where SBQQ__Quote__c =: quoteId AND (SBQQ__Product__r.Family = 'Licenses' OR SBQQ__Product__r.Family = 'Subscription')]) {
         quoteLineSet.add(item.id);
        }

        for(Quote_Line_Sub_Line__c item : [Select id, Name, Qty__c, Exclude_from_License_Email__c, LicenseProductID__c ,License_Number__c, Total_Qty__c, Sort_Order__c, Quote_Line__c, Quote_Line__r.SBQQ__Quote__r.Name, Quote_Line__r.SBQQ__StartDate__c,Quote_Line__r.SBQQ__EndDate__c,Quote_Line__r.SBQQ__Product__r.Name, Quote_Line__r.Evaluation_Expiry_Date__c, Control_Number__c From Quote_Line_Sub_Line__c where Quote_Line__c IN : quoteLineSet AND Exclude_from_License_Email__c = false Order by Sort_Order__c ASC ]) {
            listOfQuoteLineWrapperToReturn.add(item);
         
        }
       
        return listOfQuoteLineWrapperToReturn;
    }
    
    
    //BSL-1877 - Generic method for CPQ License Email
     public List<QuoteLineTableWrapper> getTableWrapperList() {
     
        Set<Id> quoteLineSet = new Set<Id>();
        Map<String, Set<SBQQ__QuoteLine__c >> serverCalMap = new Map<String, Set<SBQQ__QuoteLine__c >>();
        Map<String, Set<Quote_Line_Sub_Line__c>> qlslDetail = new Map<String, Set<Quote_Line_Sub_Line__c>>();
        Map<String, QuoteLineTableWrapper> tableWrapper = new Map<String,QuoteLineTableWrapper>();
        Map<String, String> quoteLineName = new Map<String,String>();
        Map<String, List<String>> assetToLicenseKey = new Map<String, List<String>>();
        Map<String, String> productCodeMap = new Map<String,String>();
        Decimal qty;

        dualUseSite = false;
        isSLKAvailable = false;

        for(SBQQ__QuoteLine__c item : [SELECT id, SBQQ__Quote__c,Name,SBQQ__Quantity__c,
                                            SBQQ__Product__r.Name, SBQQ__StartDate__c,
                                            SBQQ__EndDate__c,SBQQ__Product__r.Family, 
                                            Evaluation_Expiry_Date__c,Control_Number__c, 
                                            License_Key_Number__c,SBQQ__RequiredBy__c, Product_Code__c,
                                            SBQQ__RequiredBy__r.SBQQ__Description__c ,SBQQ__Description__c
                                        FROM SBQQ__QuoteLine__c 
                                        WHERE SBQQ__Quote__c =: quoteId 
                                        AND (SBQQ__Product__r.Family = 'Licenses' 
                                            OR SBQQ__Product__r.Family = 'Subscription')]) {
            system.debug('item======='+item);
            
            quoteLineName.put(item.id, item.SBQQ__Product__r.Name);
            productCodeMap.put(item.id, item.Product_Code__c);
            
            if(item.SBQQ__RequiredBy__c == NULL) { //server quoteline
                if(!serverCalMap.containsKey(item.id)) {
                    serverCalMap.put(item.id, new Set<SBQQ__QuoteLine__c>{item});
                } 
            } else {
                if(serverCalMap.containsKey(item.SBQQ__RequiredBy__c)) {
                    serverCalMap.get(item.SBQQ__RequiredBy__c).add(item);
                } else {
                    serverCalMap.put(item.SBQQ__RequiredBy__c, new Set<SBQQ__QuoteLine__c >{item});
                }
            }
            quoteLineSet.add(item.id);
        }

        //Fetch all quoteline sublines and group them based on license key and parent quoteline
        for(Quote_Line_Sub_Line__c item : [SELECT id, Name, Qty__c, Exclude_from_License_Email__c, 
                                            LicenseProductID__c ,License_Number__c, Total_Qty__c, 
                                            Sort_Order__c, Quote_Line__c, Quote_Line__r.SBQQ__Quote__r.Name, 
                                            Quote_Line__r.SBQQ__StartDate__c,Quote_Line__r.SBQQ__EndDate__c,
                                            Quote_Line__r.SBQQ__Product__r.Name, Quote_Line__r.Evaluation_Expiry_Date__c, 
                                            Control_Number__c,Quote_Line__r.SBQQ__Quantity__c,
                                            Quote_Line__r.Additional_Quantity__c,LicenseProductJTI__c, 
                                            LicenseProductJWT__c
                                          FROM Quote_Line_Sub_Line__c 
                                          WHERE Quote_Line__c IN : quoteLineSet 
                                            AND Exclude_from_License_Email__c = false 
                                            ORDER BY Sort_Order__c ASC ]) {

            if(item.Qty__c != NULL && item.Qty__c != 0 ) {
                qty = item.Qty__c;
            } else {
                qty = item.Total_Qty__c;
            }

            if(assetToLicenseKey.containsKey(item.License_Number__c)) {
                assetToLicenseKey.get(item.License_Number__c).add(qty +' '+item.Quote_Line__r.SBQQ__Product__r.Name);
            } else {
                assetToLicenseKey.put(item.License_Number__c, New List<String>{qty +' '+item.Quote_Line__r.SBQQ__Product__r.Name});
            }
            
            
            if(qlslDetail.containsKey(item.Quote_Line__c)) {
                qlslDetail.get(item.Quote_Line__c).add(item);
            } else {
                qlslDetail.put(item.Quote_Line__c, new Set<Quote_Line_Sub_Line__c>{item});
            }
           
        }
        
        //for each server product, there should be one table if there is QLSL either in server or in CALs
        
        for(String serverId : serverCalMap.keyset()) {
            if(qlslDetail.containsKey(serverId)) {
                for(Quote_Line_Sub_Line__c qlsl : qlslDetail.get(serverId )) {
                    QuoteLineTableWrapper prodTable = new QuoteLineTableWrapper();
                    prodTable.Name = quoteLineName.get(serverId);
                    prodTable.controlNumber = qlsl.Control_Number__c;
                    prodTable.licenseKey = qlsl.License_Number__c;
                    prodTable.endDate = qlsl.Quote_Line__r.Evaluation_Expiry_Date__c;
                    if(assetToLicenseKey.containsKey(qlsl.License_Number__c)) {
                        prodTable.assetToLicenseKey.addAll(assetToLicenseKey.get(qlsl.License_Number__c));
                    }
					//----
					// BSL - 2412, BSL - 2903, BSL - 2465 - by ext_cdx
                    if(productCodeMap.containsKey(serverId) && 
						(productCodeMap.get(serverId) == '8070' || productCodeMap.get(serverId) == '8070' || productCodeMap.get(serverId) == '8011' || productCodeMap.get(serverId) == '8054') && 
						qlsl.LicenseProductJWT__c != null) {
                        
						prodTable.slk = qlsl.LicenseProductJWT__c;
						prodTable.controlNumber = 'NA';										
						//----
                        isSLKAvailable = true;
                        if(dualUseSite == false && quoteLineName.get(serverId) == 'Qlik Sense Enterprise Dual Use Site'){
                            dualUseSite =true;
                        }
                    } 
                    tableWrapper.put(qlsl.License_Number__c, prodTable);
                }
            } 
            //For amend , there will not be any qlsl for server products. So iterate through CALs to create table.
            else {
                if(serverCalMap.get(serverId ).size()>0) {
                
                    for(SBQQ__QuoteLine__c cals : serverCalMap.get(serverId )) {
                        if(qlslDetail.containsKey(cals.Id)) {
                        
                            for(Quote_Line_Sub_Line__c qlsl : qlslDetail.get(cals.Id)) {
                                if(!tableWrapper.containsKey(qlsl.License_Number__c)) {
                                    QuoteLineTableWrapper prodTable = new QuoteLineTableWrapper();
                                    prodTable.Name = quoteLineName.get(serverId);
                                    prodTable.controlNumber = qlsl.Control_Number__c;
                                    prodTable.licenseKey = qlsl.License_Number__c;
                                    prodTable.endDate = qlsl.Quote_Line__r.Evaluation_Expiry_Date__c;
                                    
                                    if(assetToLicenseKey.containsKey(qlsl.License_Number__c)) {
                                        prodTable.assetToLicenseKey.addAll(assetToLicenseKey.get(qlsl.License_Number__c));
                                    }
									//----
									// BSL - 2412, BSL - 2903, BSL - 2465 - by ext_cdx
                                    if(productCodeMap.containsKey(serverId) && 
										(productCodeMap.get(serverId) == '8070' || productCodeMap.get(serverId) == '8070' || productCodeMap.get(serverId) == '8011' || productCodeMap.get(serverId) == '8054') &&  
										qlsl.LicenseProductJWT__c != null) {
										
										prodTable.slk = qlsl.LicenseProductJWT__c;
										prodTable.controlNumber = 'NA';										
										//----
                                        isSLKAvailable = true;
                                        if(dualUseSite == false && quoteLineName.get(serverId) == 'Qlik Sense Enterprise Dual Use Site'){
                                            dualUseSite =true;
                                        }
                                    } 

                                    tableWrapper.put(qlsl.License_Number__c, prodTable);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(tableWrapper.values().size() > 0) {
            return tableWrapper.values();
        } else {
            return null;   
        }
        
    }
    
    
    //Wrapper is used to store all details needed to create table.
    public class QuoteLineTableWrapper{
    
        public String Name {get; set;}
        public String controlNumber {get; set;}
        public String licenseKey {get; set;}
        public Date endDate {get; set;}
        public String slk {get;set;}
        public List<String> assetToLicenseKey{get; set;}
        
        public QuoteLineTableWrapper() {
            assetToLicenseKey = new List<String>();
            slk = 'NA';
        }
    }

    

}