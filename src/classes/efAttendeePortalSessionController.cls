//*********************************************************/
// Author: Mark Cane&
// Creation date: 20/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efAttendeePortalSessionController{
    private efRegistrationManager regManager;
    private User user;
    private PageReference pr;
    private efAppNavigator navigator;
	private String eventBannerUrl;
    
    private String topFilter;
    private String sortBy;
    private String sortDirection;
    private String criteriaKeyword;
    private String criteriaTrack;
    private List<String> criteriaDates = new List<String>();
    private List<String> criteriaExperiences = new List<String>();
    private List<efSessionWrapper> initialSessions;
    private List<efSessionWrapper> filteredSessions;
    private Map<ID, efSessionWrapper> initialSessionsMap;
    private String selectedSessionID;
    private efSessionWrapper selectedSession;
    private Boolean selectedSessionEnrolled;
    private Boolean selectedSessionConflict;
    private Map<String,String> sessionMap = efCustomSettings.getValues(efCustomSettings.SESSION_MAP); 
    private Integer totalSessionCount;
    private Integer currentPageNumber;
    private List<PageNumber> pageList;
    private Integer pageSize;
    private Map<ID, efSessionWrapper> sessionBucket = new Map<ID, efSessionWrapper>();
    private ID sessionRemoveId;
    private string contactId;            
    private static final String TOP_FILTER_NONE = 'None';
    private static final String TOP_FILTER_RECOMMENDED = 'Recommended';
    private static final String TOP_FILTER_HIGHLIGHTED = 'Highlighted';
    private static final String TOP_FILTER_KEYWORD = 'Keyword';
    private static final String TOP_FILTER_PRESELECT = 'Preselect';
    
    private static final String SORT_BY_NAME = 'Name';
    private static final String SORT_BY_DATE = 'Date';
    private static final String SORT_BY_TRACK = 'Track';
    
    private static final String SORT_DIRECTION_ASC = 'ASC';
    private static final String SORT_DIRECTION_DESC = 'DESC';
    
    private static final String EXPERIENCE_LEVEL_ALL = 'All';
    
    
    // Intialisation methods.
    
   	public efAttendeePortalSessionController(){
		if(user==null){
            try{
                initialize();
            } catch(Exception e){}
        }
	}
	
	public void initialize(){
    	// Comment & : validate input registration Id.
    	String registrationId = ApexPages.currentPage().getParameters().get('registrationId');
    	contactId = efUtility.getContactIdFromRegistration(registrationId);    	
   		pr = checkInitialState();    	   	
    	if (pr!=null) return;

    	// Comment& : efRegistrationManager creates and holds 
    	// efRegistrationWrapper, efContactWrapper instances.     	
        regManager = new efRegistrationManager(contactId, registrationId);
        
        // Comment& : efAppNavigator encapsulates page navigation logic.
        navigator = new efAppNavigator(regManager.getEventWrapper(), registrationId);
    	navigator.setCurrentPage(efAppNavigator.ATTENDEE_PORTAL_SESSION);
    	
		topFilter = getShowRecommendedSessions() ? TOP_FILTER_RECOMMENDED : TOP_FILTER_NONE;
	        
        pageSize = Integer.valueOf(sessionMap.get('pagesize'));
        
        sortBy = SORT_BY_NAME;
        sortDirection = SORT_DIRECTION_ASC;
        
        refreshSessions();
	}	
	
	public PageReference checkInitialState(){
    	try {
    		String regId = ApexPages.currentPage().getParameters().get('registrationId');
    		if (regId==null || regId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    		if (contactId == null) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);    		
    		return efRegistrationManager.checkRegistrationIdPortal(regId);
    	} catch (Exception e){
    		return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    	}
    }
    
    public PageReference checkState(){
        if (pr!=null)
        	return pr;
        else
        	return null;
    }
    
    public String getRegistrationId(){
    	if (regManager==null)
    		return '';
    	else    	
    		return regManager.getRegistrationWrapper().getId();
    }
    
    public Boolean getShowSessions(){
    	if (regManager==null)
    		return false;
    	else
    		return regManager.getEventWrapper().getShowSessions();
    }
    
    public Boolean getShowActivities(){
    	if (regManager==null)
    		return false;
    	else    	
    		return regManager.getEventWrapper().getShowActivities();
    }
    // End Intialisation methods.
    
    
    // Page furniture methods.
    
	public String getEventBannerUrl(){
		if (regManager==null)
			return '';
		else {
    		if (eventBannerUrl==null || eventBannerUrl.length()==0){
    			eventBannerUrl = getDocumentUrl(regManager.getEventWrapper().getEvent().Banner_Image_Document__c);
    		}
    		return eventBannerUrl;
		}
    }
    
    private String getDocumentUrl(String documentUniqueName){
    	Document doc = [Select d.Id From Document d Where d.DeveloperName=:documentUniqueName Limit 1];
    	String imageid = doc.id; 
    	imageid = imageid.substring(0,15);
    	return '/servlet/servlet.FileDownload?file=' + imageid;
    }
    // End page furniture methods.
    
    
    // Sessions methods.
    
    public Boolean getShowRecommendedSessions(){
        return regManager.getRegistrationWrapper().getRegistration().Track_Of_Interest_Lkup__c != null;
    }
    
    private void resetCriteria(){
        criteriaTrack = '';
        criteriaDates = new List<String>();
        criteriaExperiences = new List<String>();
    }
    
    private void refreshSessions(){
        resetCriteria();
        selectedSession = null;
        selectedSessionID = null;
        refreshInitialSessions();
        createPagination(initialSessions);
        
        if (topFilter == TOP_FILTER_PRESELECT){
        	selectSession();
        }
    }
    
    private void createPagination(List<efSessionWrapper> sessions){
        filteredSessions = sessions;
        paginateSessions(sessions);
    }
    
    private String listToString(List<String> l){
        String result = '(';
        
        for (String s:l){
            if (result != '('){
                result += ',';
            }            
            result += '\'' + String.escapeSingleQuotes(s) + '\'';
        }        
        return result + ')';
    }
    
    private String setToString(Set<String> l){
        String result = '(';
        
        for (String s:l){
            if (result != '('){
                result += ',';
            }            
            result += '\'' + String.escapeSingleQuotes(s) + '\'';
        }        
        return result + ')';
    }
    
    private void refreshInitialSessions(){
        String sessionQuery ;
        
        try{
	        sessionQuery =  'Select s.Id,';
	        sessionQuery += '       s.Name,';
	        sessionQuery += '       s.Session_Theme__c,';
	        sessionQuery += '       s.HighlightedSession__c,';
	        sessionQuery += '       s.Session_Start_Time__c,';
	        sessionQuery += '       s.Session_End_Time__c,';
	        sessionQuery += '       s.Track__r.Name,';
	        sessionQuery += '       s.Balance__c,';
	        sessionQuery += '       s.Used__c,';
	        sessionQuery += '       (';
	        sessionQuery += '           Select Id,';
            sessionQuery += '                  Session_Topic__c,';
            sessionQuery += '                  Session__r.Session_Start_Time__c,'; 
            sessionQuery += '                  Session__r.Session_End_Time__c,';                                     
	        sessionQuery += '                  Status__c';
	        sessionQuery += '           From Session__r';
	        sessionQuery += '           Where Registration__c = \'' + String.escapeSingleQuotes(regManager.getRegistrationWrapper().getId()) + '\'';
	        sessionQuery += '       )';
	        sessionQuery += '       from Session__c s where s.Active__c = true';
	        sessionQuery += '       and s.Track__r.Event__r.Id = \'' + String.escapeSingleQuotes(regManager.getEventWrapper().getEvent().Id) + '\'';
	        sessionQuery += '       and s.RecordTypeId = \'' + String.escapeSingleQuotes(sessionMap.get('sessionrecordtypeid')) + '\'';
	        sessionQuery += '       and s.Name != null';
	        sessionQuery += '       and s.Session_Start_Time__c != null';
	        sessionQuery += '       and s.Track__c != null';
	        sessionQuery += '       and s.Session_End_Time__c != null';
	
	        if (topFilter == TOP_FILTER_RECOMMENDED){
	            sessionQuery += ' and (s.Session_Theme__c IN ' + listToString(regManager.getRegistrationWrapper().getWantToLearn());
	            sessionQuery += ' or ((s.Level__c = \'' + EXPERIENCE_LEVEL_ALL + '\')';
	            sessionQuery += ' and s.Track__c = \'' + String.escapeSingleQuotes(regManager.getRegistrationWrapper().getTrackInterest()) + '\'))';
	        } else if (topFilter == TOP_FILTER_HIGHLIGHTED){
	            sessionQuery += ' and s.HighlightedSession__c = true';    
	        } else if (topFilter == TOP_FILTER_KEYWORD){
                Set<String> sessionIDs = new Set<String>();

				//SOSL to search in all Session Fields
                String searchKeyword = '*' + criteriaKeyword.trim() + '*';
                List<List<SObject>> keywordResult = [FIND :searchKeyword IN ALL FIELDS RETURNING Session__c(Id)];
                for (Session__c s:(List<Session__c>)keywordResult[0]){
                	sessionIDs.add(s.Id);
                } 
				//SOQL to search speaker's formula field storing account name and contact name
				String speakerQuery =  'Select Session__c ';
				speakerQuery        += 'From Speaker__c ';
				speakerQuery        += 'Where Is_Ready_to_Publish__c = true and ';
				speakerQuery        += '(Clean_Account_Name__c like \'%' + String.escapeSingleQuotes(criteriaKeyword) + '%\'';
				speakerQuery        += ' or Clean_Contact_Name__c like \'%' + String.escapeSingleQuotes(criteriaKeyword) + '%\') limit 500';
				
                for (Speaker__c s:Database.query(speakerQuery)){
                    sessionIDs.add(s.Session__c);
                } 
				//Union of above both results
                if (sessionIDs.size()>0){
                    sessionQuery += ' and Id IN ' + setToString(sessionIDs);
                } else{
                	sessionQuery = '';
                }
            }            
            List<Session__c> tempSessions;
            
            if (sessionQuery != ''){
		        sessionQuery += sortBy == SORT_BY_DATE ? ' order by Session_Start_Time__c' : ' order By Name';
		        sessionQuery += sortDirection == SORT_DIRECTION_ASC ? ' asc' : ' desc';
	
	            tempSessions = Database.query(sessionQuery);
            } else{
                tempSessions = new List<Session__c>();	
            }            
	        initialSessions = new List<efSessionWrapper>();
	        initialSessionsMap = new Map<ID, efSessionWrapper>();
	        
	        for (Session__c s:tempSessions){
	            efSessionWrapper sWrapper = new efSessionWrapper(s,true);
	            
	            initialSessions.add(sWrapper);
	            initialSessionsMap.put(s.ID, sWrapper);
	        }
	        
	        // Don't forget the previously selected session, if any
	        if (!efUtility.isNull(selectedSessionID) &&
	            initialSessionsMap.get(selectedSessionID) != null){
	            initialSessionsMap.get(selectedSessionID).setIsSelected(true);
	        }
	        
	        // Don't forget the sessions in bucket, if any
	        for (ID sessionID:sessionBucket.keySet()){
	        	if (initialSessionsMap.get(sessionID) != null){
	            	initialSessionsMap.get(sessionID).setIsInBucket(true);
	            }
	        }
        } catch (Exception e){
        	System.debug('FINDME: ' + e.getMessage());
        }
    }

    public List<efSessionWrapper> getPageSessions(){
        return getSessionsForPage(filteredSessions, currentPageNumber);
    }

    /******************* FILTERING *************************/
    
    public List<SelectOption> getCriteriaTrackList(){
    	if (regManager==null) return null;
    	
        efPicklists p = new efPicklists();    
        return p.getTrackList(regManager.getEventWrapper().getEvent().Id);
    }
    
    public String getCriteriaTrack(){
        return criteriaTrack;
    }
    
    public void setCriteriaTrack(String criteriaTrack)
    {
        this.criteriaTrack = criteriaTrack;
    }
    
    public List<SelectOption> getCriteriaDateList(){
    	if (regManager==null) return null;
    	
        efPicklists p = new efPicklists();     
        return p.getEventDateList(regManager.getEventWrapper().getEvent().Event_Start_Date__c, regManager.getEventWrapper().getEvent().Event_End_Date__c);
    }
    
    public List<String> getCriteriaDates(){
        return criteriaDates;
    }
    
    public void setCriteriaDates(List<String> criteriaDates){
        this.criteriaDates = criteriaDates;
    }
    
    public List<String> getCriteriaExperiences(){
        return criteriaExperiences;
    }
    
    public void setCriteriaExperiences(List<String> criteriaExperiences){
        this.criteriaExperiences = criteriaExperiences;
    }

    public String getCriteriaKeyword(){
        return criteriaKeyword;
    }
    
    public void setCriteriaKeyword(String criteriaKeyword){
        this.criteriaKeyword = criteriaKeyword;
    }
    
    public void applyCriteria(){
        selectedSession = null;
        selectedSessionID = null;
        
        List<DateTime> dates = new List<DateTime>();
        
        for (String d:criteriaDates){
            dates.add(efUtility.getStringToDateNonGMT(d));
        }
        
        List<efSessionWrapper> tempSessions = new List<efSessionWrapper>();
        
        for (efSessionWrapper s:initialSessions){
            Boolean matchesCriteria = false;
            
            if (efUtility.isNull(criteriaTrack) ||
                s.getSessions().Track__c == criteriaTrack){
                if (dates.size() > 0){
                    for (DateTime d:dates){
                        if (s.getSDate().isSameDay(d)){
                            matchesCriteria = true;
                        }
                    }
                }
                else{
                    matchesCriteria = true;
                }
                
                if (matchesCriteria){
                    matchesCriteria = false;
                    
                    if (criteriaExperiences != null && criteriaExperiences.size() > 0){
                        for (String e:criteriaExperiences){
                            if (!efUtility.isNull(s.getSessions().Level__c) &&
                                (s.getSessions().Level__c == e ||
                                 s.getSessions().Level__c == EXPERIENCE_LEVEL_ALL)){
                                matchesCriteria = true;
                            }
                        }
                    }
                    else{
                        matchesCriteria = true;
                    }
                    
                    if (matchesCriteria){
                        tempSessions.add(s);
                    }
                }
            }
        }        
        createPagination(tempSessions);
    }
    
    public String getAttendeeTrack(){
        return regManager.getRegistrationWrapper().getTrackInterestName();
    }

    public void seeRecommendedSessions(){
        criteriaKeyword = '';
        topFilter = TOP_FILTER_RECOMMENDED;
        refreshSessions();
    }
    
    public void seeHighlightedSessions(){
        criteriaKeyword = '';
        topFilter = TOP_FILTER_HIGHLIGHTED;
        refreshSessions();
    }

    public void seeAllSessions(){
    	criteriaKeyword = '';
        topFilter = TOP_FILTER_NONE;
        refreshSessions();
    }

    public void seeKeywordSessions(){
        topFilter = TOP_FILTER_KEYWORD;
        
        if (getIsValidKeyword()){
        	refreshSessions();
        }
    }    
    
    public Boolean getIsValidKeyword(){
    	System.debug('&&& criteriaKeyword : '+criteriaKeyword);
    	return !efUtility.isNull(criteriaKeyword) &&
    	       criteriaKeyword.trim().length() > 2 &&
    	       criteriaKeyword.indexOf('%') == -1;
    }
    
    public String getTopFilter(){
        return topFilter;
    }
    
    /****************** SORTING ****************/
    public String getSortBy(){
        return sortBy;
    }
    
    public String getSortDirection(){
        return sortDirection;
    }
    
    public void sortByName(){
        setSortBy(SORT_BY_NAME);
    }
    
    public void sortByDate(){
        setSortBy(SORT_BY_DATE);
    }
    
    public void sortByTrack(){
        setSortBy(SORT_BY_TRACK);
    }

    private void setSortBy(String newSortBy){
        if (sortBy == newSortBy){
            sortDirection = sortDirection == SORT_DIRECTION_ASC ? SORT_DIRECTION_DESC : SORT_DIRECTION_ASC;
        } else{
            sortDirection = SORT_DIRECTION_ASC;
        }        
        sortBy = newSortBy;        
        refreshInitialSessions();
        applyCriteria();
    }
    
    /**************** PAGINATION *******************/
    public Integer getTotalSessionCount(){
        return totalSessionCount;
    }

    public PageNumber getCurrentPage(){
    	if (pageList==null) return null;
        return pageList.get(currentPageNumber - 1);
    }
    
    public Integer getCurrentPageNumber(){
        return currentPageNumber;
    }
    
    public void setCurrentPageNumber(Integer currentPageNumber){
        this.currentPageNumber = currentPageNumber;
    }
    
    public Boolean getShowPageList(){
        return pageList != null && pageList.size() > 1;
    }
    
    public List<PageNumber> getPageList(){
        return pageList;
    }
    
    private void paginateSessions(List<efSessionWrapper> sessions){
        totalSessionCount = sessions.size();
        currentPageNumber = 1;
        
        createPageNumbers(sessions.size());
    }
    
    private void createPageNumbers(Integer recordCount){ 
        pageList = new  List<PageNumber>();
        
        Integer pageStart = 1;
        Integer pageEnd = pageSize;
        Integer pageCount = 0;

        for (pageCount = 0; pageCount < recordCount / pageSize; pageCount++){
            PageNumber pn = new PageNumber();
            pn.Startnum = pageStart;
            pn.Endnum = pageEnd;
            pn.PageNumber = pageCount + 1 ;
            pageList.add(pn) ;
            pageStart += pageSize;
            pageEnd += pageSize;
        }
        
        if (recordCount > pageEnd - pageSize){
            PageNumber pn = new PageNumber();
            pn.Startnum =  pageStart;
            pn.Endnum    =   recordCount ;
            pn.PageNumber = pageCount + 1 ;
            pageList.add(pn) ;                                                   
        }
    }

    public void nextPage(){
        currentPageNumber++;
    }
    
    public void previousPage(){
        currentPageNumber--;
    }
    
    public Boolean getIsFirstPage(){
        return currentPageNumber == 1;
    }
    
    public Boolean getIsLastPage(){
        return currentPageNumber == pageList.size();
    }
    
    private List<efSessionWrapper> getSessionsForPage(List<efSessionWrapper> sessions, Integer page){
        List<efSessionWrapper> pageSessions = new List<efSessionWrapper>();
        
        if (sessions!=null){        
	        for (Integer i = (page - 1) * pageSize;
	             i < page * pageSize && i < sessions.size();
	             i++){
	            pageSessions.add(sessions.get(i));
	        }
        }
        return pageSessions;
    }
    
    /**************************** OPERATIONS ON SESSIONS *******************************/

    public String getSelectedSessionID(){
        return selectedSessionID;
    }
    
    public void setSelectedSessionID(String selectedSessionID){
        this.selectedSessionID = selectedSessionID;
    }
       
    public void selectSession(){
        Session__c s = [Select s.Id,
                               Name,
                               Session_Theme__c,
                               HighlightedSession__c,
                               Room__r.Room_Number__c,
                               Room__r.Name,
                               Session_End_Time__c,
                               Session_Start_Time__c,
                               Session_Type__c,
                               Session_Abstract__c,
                               Track__r.Name,
                               Balance__c,
                               Used__c,
                               Level__c,
                               (
                                     Select Clean_Account_Name__c,
                                            Clean_Contact_Name__c
                                     From Speakers__r
                                     Where Is_Ready_to_Publish__c = true
                               ),
                               (
                                     Select a.Id,
                                            a.Attendee_Name__c,
                                            a.FacebookProfileId__c,
                                            a.Registration__r.FacebookProfileId__c,
                                            a.Session__r.Session_Start_Time__c, 
                                            a.Session__r.Session_End_Time__c
                                     From Session__r a
                                     Where a.Registration__c != :regManager.getRegistrationWrapper().getId()
                               )
                               From Session__c s Where Id = :selectedSessionID];

        // This for is for highlighting the selected session on the left panel with the session list
        for (efSessionWrapper is:initialSessions){
            is.setIsSelected(is.getSessionId() == s.Id);
        }
        
        selectedSession = new efSessionWrapper(s);
        selectedSessionEnrolled = initialSessionsMap.get(selectedSessionID).getIsAddedToSchedule();
        
        List<efSessionAttendanceWrapper> allSessionAttendanceList = new List<efSessionAttendanceWrapper>();
        for(Session_Attendance__c sa : [Select Id,
                                           Session__r.Session_Start_Time__c,
                                           Session__r.Session_End_Time__c
                                        From Session_Attendance__c Where 
                                        Registration__c = :regManager.getRegistrationWrapper().getRegistration().Id And
                                        Session__r.RecordTypeID = :sessionMap.get('sessionrecordtypeid') And
                                        Attendee_Name__c = :regManager.getContactWrapper().getContact().Id And
                                        Status__c ='Registered']){
			allSessionAttendanceList.add(new efSessionAttendanceWrapper(sa));
        }
        selectedSessionConflict = initialSessionsMap.get(selectedSessionID).getIsConflictSchedule(allSessionAttendanceList);        
    }
    
    public efSessionWrapper getSelectedSession(){
        return selectedSession;
    }
    
    public Boolean getSelectedSessionEnrolled(){
        return selectedSessionEnrolled;
    }

    public Boolean getSelectedSessionConflict(){
        return selectedSessionConflict;
    }
    
    public void createSessionAttendance(){
        efSessionWrapper s = getSelectedSession();

        if (!s.getIsSessionFull()){   
            Session_Attendance__c sa = new  Session_Attendance__c();
            
            sa.Session__c = s.getSessionId();
            sa.Registration__c = regManager.getRegistrationWrapper().getId();
            sa.Status__c = efConstants.SESSION_ATTENDANCE_REGISTERED;
            sa.Attendee_Name__c = regManager.getContactWrapper().getContact().Id;
            s.getSessions().Used__c = s.getSessions().Used__c == null ? 1 : s.getSessions().Used__c + 1; 
            insert sa;
            update s.getSessions();
            initialSessionsMap.get(s.getSessionId()).addSessionAttendance(sa);
            selectedSessionEnrolled = true;
        }
    }
    // End Sessions methods.

	public class PageNumber{	
		public Integer Startnum = 0;
		public Integer Endnum = 0;
		public Integer PageNumber = 0;
		public boolean isSelected = false;
		public boolean isNext = false;
		public boolean isPrevious = false;
	
		public Integer getStartnum(){
			return this.Startnum;
		}
	
		public void setStartnum(Integer val){
			this.Startnum = val;
		}
		
		public Integer getEndnum(){
			return this.Endnum;
		}
		
		public void setEndnum(Integer val){
			this.Endnum = val;
		}
			
		public Integer getPageNumber(){
			return this.PageNumber;
		}
		
		public void setPageNumber(Integer val){
			this.PageNumber = val;
		}
		
		public boolean getIsSelected(){
			return this.isSelected ;
		}
		
		public void setIsSelected(Boolean isSel){
			 this.isSelected =  isSel;
		}
		
		public boolean getIsNext(){
			return this.isNext ;
		}
		
		public void setIsNext(Boolean isSel){
			 this.isNext =  isSel;
		}
		
		public boolean getIsPrevious(){
			return this.isPrevious ;
		}
		
		public void setIsPrevious(Boolean isSel){
			 this.isPrevious =  isSel;
		}
	}    
}