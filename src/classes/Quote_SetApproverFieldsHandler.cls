/**
 * 2017-07-31   Sergii Grushai QCW-1954     create method "populateApproverFields" for populate Approver fields
 * 2017-08-28   Sergii Grushai  Add webservice method for recalculate approver fields. 1 Method: recalculateApproverFields, change CLass from public to Global as webservice method should be in global class.
 * 2017-08-29   AIN Modified recalculateApproverFields to use an ID instead of a Quote object. Added PopulateSalesRep
**/
global class Quote_SetApproverFieldsHandler {
    public static Boolean beforeUpdateFlag = false;
    //public static Boolean runTriggerFromButton = false;

    public static  Map<ID,User> idToUser = new Map<ID, User>([SELECT id,INT_NetSuite_InternalID__c, Approval_Manager__c,Quote_Approval_Role__c FROM User Where Quote_Approval_Role__c!=null]);

    public static void onBeforeInsert(List<SBQQ__Quote__c> inputList) {
        system.debug('Quote_SetApproverFieldsHandler onBeforeInsert start');

        System.debug(getNotEmptySalesRepQuotes(inputList));
        PopulateSalesRep(inputList);
        populateSalesApproverFields(getNotEmptySalesRepQuotes(inputList));
        populateConsultingFields(inputList/*,quoteIds*/);
        populateEducationFields(inputList/*,quoteIds*/);
        
        populateApproverFields(inputList);
        system.debug('Quote_SetApproverFieldsHandler onBeforeInsert end');
    }

    public static void onBeforeUpdate(List<SBQQ__Quote__c> oldList, List<SBQQ__Quote__c> newList, map<Id, SBQQ__Quote__c> newMap/*, Set<Id> quoteIds*/) {
        system.debug('Quote_SetApproverFieldsHandler onBeforeUpdate start');
        //update only Quote with changed Sales Rep field
        if(!beforeUpdateFlag){
            beforeUpdateFlag = true;
            //List<SBQQ__Quote__c> listtoUpdate = new List<SBQQ__Quote__c>();
            System.debug('after update SalesReps');
            /*for(SBQQ__Quote__c oldQuote : oldList)
            {
                SBQQ__Quote__c newQuote = (SBQQ__Quote__c)Trigger.newMap.get(oldQuote.Id);
                //if(newQuote.SBQQ__SalesRep__c != oldQuote.SBQQ__SalesRep__c || runTriggerFromButton)
                if(newQuote.SBQQ__SalesRep__c != oldQuote.SBQQ__SalesRep__c)
                    listtoUpdate.add(newQuote);
            }
            System.debug(getNotEmptySalesRepQuotes(listtoUpdate));*/

            //PopulateSalesRep(listtoUpdate);
            //populateSalesApproverFields(getNotEmptySalesRepQuotes(listtoUpdate));
            PopulateSalesRep(newList);
            populateSalesApproverFields(getNotEmptySalesRepQuotes(newList));
            populateConsultingFields(newList);
            populateEducationFields(newList);
            
            
            populateApproverFields(newList);
        }
        system.debug('Quote_SetApproverFieldsHandler onBeforeUpdate end');
    }

    public static void  onAfterUpdate(Map<ID, SBQQ__Quote__c> SBQQQuoteMap, Map<ID, SBQQ__Quote__c> SBQQQuoteOldMap) {
    }

    public static void onAfterInsert(Set<id> quotesId) {
        /*
        system.debug('Quote_SetApproverFieldsHandler onAfterInsert start');
        List<SBQQ__Quote__c> newQuotesAfterInsert = [Select id,SBQQ__SalesRep__c,SBQQ__Account__c,
                                                       Sales_Approver_1_Role__c,Sales_Approver_1__c,
                                                       Sales_Approver_2_Role__c,Sales_Approver_2__c,
                                                       Sales_Approver_3_Role__c,Sales_Approver_3__c,
                                                       Sales_Approver_4_Role__c,Sales_Approver_4__c,
                                                       Sales_Approver_5_Role__c,Sales_Approver_5__c,
                                                       Consulting_Approver_1__c,
                                                       Consulting_Approver_2__c,
                                                       Consulting_Approver_3__c,
                                                       Education_Approver_1__c,
                                                       Education_Approver_2__c,
                                                       Education_Approver_3__c,
                                                       Consulting_Approver_1_Role__c,
                                                       Consulting_Approver_2_Role__c,
                                                       Consulting_Approver_3_Role__c,
                                                       Education_Approver_1_Role__c,
                                                       Education_Approver_2_Role__c,
                                                       Education_Approver_3_Role__c
                                                       from SBQQ__Quote__c where id in :quotesId ];
        List<SBQQ__Quote__c> toUpdateQuote = populateEducationFields(populateConsultingFields(populateSalesApproverFields(newQuotesAfterInsert)));
        if(Test.isRunningTest()) {
            getNotEmptySalesRepQuotes(newQuotesAfterInsert);
            getNotEmptyCosultingQuotes(newQuotesAfterInsert);
        }
        try{
            update toUpdateQuote;
        }   
        catch(DmlException e) {
            System.debug('Quote_SetApproverFieldsHandler line 39  DmlException');
            System.debug(e.getMessage());
        }
        system.debug('Quote_SetApproverFieldsHandler onAfterInsert end');*/
    }
    
    /*webservice static void recalculateApproverFields(String quoteId) {
        system.debug('Quote_SetApproverFieldsHandler recalculateApproverFields start');
        //List<SBQQ__Quote__c> inputList = new List<SBQQ__Quote__c>();
        List<SBQQ__Quote__c> inputList = [select id,SBQQ__SalesRep__c,SBQQ__Account__c,
                                            Sales_Approver_1_Role__c,Sales_Approver_1__c,
                                            Sales_Approver_2_Role__c,Sales_Approver_2__c,
                                            Sales_Approver_3_Role__c,Sales_Approver_3__c,
                                            Sales_Approver_4_Role__c,Sales_Approver_4__c,
                                            Sales_Approver_5_Role__c,Sales_Approver_5__c,
                                            Consulting_Approver_1__c,
                                            Consulting_Approver_2__c,
                                            Consulting_Approver_3__c,
                                            Education_Approver_1__c,
                                            Education_Approver_2__c,
                                            Education_Approver_3__c,
                                            Consulting_Approver_1_Role__c,
                                            Consulting_Approver_2_Role__c,
                                            Consulting_Approver_3_Role__c,
                                            Education_Approver_1_Role__c,
                                            Education_Approver_2_Role__c,
                                            Education_Approver_3_Role__c,
                                            Revenue_Type__c,
                                            SBQQ__Account__r.Owner.UserType,
                                            Sell_Through_Partner__r.OwnerId,
                                            SBQQ__Account__r.OwnerId
                                            from SBQQ__Quote__c where id = :quoteId];
        if(inputList.size() > 0 )
        {
            SBQQ__Quote__c quote = inputList[0];
            system.debug('quote.SBQQ__SalesRep__c: ' + quote.SBQQ__SalesRep__c);
        }

        runTriggerFromButton = true;
        update inputList;
        
        system.debug('Quote_SetApproverFieldsHandler recalculateApproverFields end');
    }*/

    private static  List<SBQQ__Quote__c> populateSalesApproverFields(List<SBQQ__Quote__c> inputList) {
        system.debug('Quote_SetApproverFieldsHandler populateSalesApproverFields start');
        User salesRep = new User();
        User salesApprover1 = new User();
        User salesApprover2 = new User();
        User salesApprover3 = new User();
        User salesApprover4 = new User();  
        User salesApprover5 = new User();

        System.debug('idToUser: ' + idToUser);
        System.debug('inputList: ' + inputList);
        for (SBQQ__Quote__c quoteItem: inputList){
            if(idToUser.containsKey(quoteItem.SBQQ__SalesRep__c)){
                salesRep = idToUser.get(quoteItem.SBQQ__SalesRep__c);
                quoteItem.Sales_Rep_Approval_Role__c = salesRep.Quote_Approval_Role__c;
                System.debug(salesRep);
                System.debug(idToUser.containsKey(salesRep.Approval_Manager__c));
                if(String.isNotEmpty(salesRep.Approval_Manager__c) && idToUser.containsKey(salesRep.Approval_Manager__c)) {
                    salesApprover1 = idToUser.get(salesRep.Approval_Manager__c);

                    if(salesApprover1 != null) {
                        if(String.isNotEmpty(salesApprover1.INT_NetSuite_InternalID__c))
                            quoteItem.Sales_Approver_1_NS_Id__c = salesApprover1.INT_NetSuite_InternalID__c;                                
                            if(!String.isEmpty(salesApprover1.Quote_Approval_Role__c)) {
                                quoteItem.Sales_Approver_1_Role__c = salesApprover1.Quote_Approval_Role__c;
                            }
                        quoteItem.Sales_Approver_1__c = salesApprover1.id;
                        System.debug(quoteItem.Sales_Approver_1__c);
                    }
                }
                else{
                   quoteItem.Sales_Approver_1_Role__c =null;
                   quoteItem.Sales_Approver_1__c=null;
                }

                if(String.isNotEmpty(salesApprover1.Approval_Manager__c) && idToUser.containsKey(salesApprover1.Approval_Manager__c)){
                    salesApprover2 = idToUser.get( salesApprover1.Approval_Manager__c);
                    if(salesApprover2 != null) {
                        if(!String.isEmpty(salesApprover2.Quote_Approval_Role__c)) {
                            quoteItem.Sales_Approver_2_Role__c = salesApprover2.Quote_Approval_Role__c;
                        }
                        quoteItem.Sales_Approver_2__c = salesApprover2.id;
                    }
                }
                else{
                   quoteItem.Sales_Approver_2_Role__c =null;
                   quoteItem.Sales_Approver_2__c=null;
                   
                }
                if  (String.isNotEmpty(salesApprover2.Approval_Manager__c) && idToUser.containsKey(salesApprover2.Approval_Manager__c)){
                    salesApprover3 = idToUser.get( salesApprover2.Approval_Manager__c);
                    if(salesApprover3 != null) {
                        if(!String.isEmpty(salesApprover3.Quote_Approval_Role__c)) {
                            quoteItem.Sales_Approver_3_Role__c = salesApprover3.Quote_Approval_Role__c;
                        }
                        quoteItem.Sales_Approver_3__c = salesApprover3.id;
                    }
                }
                else{
                   quoteItem.Sales_Approver_3_Role__c =null;
                   quoteItem.Sales_Approver_3__c=null;
                }
                if (String.isNotEmpty(salesApprover3.Approval_Manager__c) && idToUser.containsKey(salesApprover3.Approval_Manager__c)){
                    salesApprover4 =idToUser.get( salesApprover3.Approval_Manager__c);
                    if(salesApprover4 != null) {
                        if(!String.isEmpty(salesApprover4.Quote_Approval_Role__c)) {
                            quoteItem.Sales_Approver_4_Role__c = salesApprover4.Quote_Approval_Role__c;
                        }
                        quoteItem.Sales_Approver_4__c = salesApprover4.id;
                    }
                }
                else{
                   quoteItem.Sales_Approver_4_Role__c =null;
                   quoteItem.Sales_Approver_4__c=null;
                }
                if  (String.isNotEmpty(salesApprover4.Approval_Manager__c) && idToUser.containsKey(salesApprover4.Approval_Manager__c)){
                  salesApprover5 =idToUser.get( salesApprover4.Approval_Manager__c);
                    if(salesApprover5 != null) {
                        if(!String.isEmpty(salesApprover5.Quote_Approval_Role__c)) {
                            quoteItem.Sales_Approver_5_Role__c = salesApprover5.Quote_Approval_Role__c;
                        }
                        quoteItem.Sales_Approver_5__c = salesApprover5.id;
                    }
                }
                else{
                   quoteItem.Sales_Approver_5_Role__c =null;
                   quoteItem.Sales_Approver_5__c=null; 
                }
                System.debug(quoteItem);
            }
            else{
                CleanApproverFields(quoteItem);
            }
        }
        system.debug('Quote_SetApproverFieldsHandler populateSalesApproverFields end');
        return inputList;
    }

    private static List<SBQQ__Quote__c> populateEducationFields(List<SBQQ__Quote__c> inputList/*, Set<Id> quoteIds*/) {
    system.debug('Quote_SetApproverFieldsHandler populateEducationFields start');
      User educationApprover1 = new User();
      User educationApprover2 = new User();
      User educationApprover3 = new User();
      Account accountFromQuote = new Account();
      Map<ID,SBQQ__Quote__c> accountIdToQuote = new Map<ID, SBQQ__Quote__c>();
        for (SBQQ__Quote__c quoteItem: inputList){
          accountIdToQuote.put(quoteItem.SBQQ__Account__c, quoteItem);
        }
      Map<ID,Account> idToAccount= new Map<ID, Account>([Select id, Billing_Country_Code__r.Education_Quote_Approver__c from Account where id in :accountIdToQuote.keySet()]);
      for (SBQQ__Quote__c quoteItem: inputList){
         accountFromQuote = idToAccount.get(quoteItem.SBQQ__Account__c);
           if  (accountFromQuote != null && idToUser.containsKey(accountFromQuote.Billing_Country_Code__r.Education_Quote_Approver__c) && 
                String.isNotEmpty(accountFromQuote.Billing_Country_Code__r.Education_Quote_Approver__c))
                {

                 if  (idToUser.containsKey(accountFromQuote.Billing_Country_Code__r.Education_Quote_Approver__c)){
                 educationApprover1 = idToUser.get(accountFromQuote.Billing_Country_Code__r.Education_Quote_Approver__c);
                    if(educationApprover1 != null) {
                        quoteItem.Education_Approver_1__c =  educationApprover1.id;
                        if(!String.isEmpty(educationApprover1.Quote_Approval_Role__c)) {
                            quoteItem.Education_Approver_1_Role__c = educationApprover1.Quote_Approval_Role__c;
                        }
                    }
                 }
                  else{
                         quoteItem.Education_Approver_1_Role__c =null;
                         quoteItem.Education_Approver_1__c=null;   
                     }
                  if  (String.isNotEmpty(EducationApprover1.Approval_Manager__c) && idToUser.containsKey(EducationApprover1.Approval_Manager__c)) {
                    educationApprover2 =idToUser.get(educationApprover1.Approval_Manager__c);
                        if(educationApprover2 != null) {
                            if(!String.isEmpty(educationApprover2.Quote_Approval_Role__c)) {
                                quoteItem.Education_Approver_2_Role__c = educationApprover2.Quote_Approval_Role__c;
                            }
                            quoteItem.Education_Approver_2__c = educationApprover2.id;
                        }
                  }
                   else{
                       quoteItem.Education_Approver_2_Role__c =null;
                       quoteItem.Education_Approver_2__c=null; 
                   }
                  if  (String.isNotEmpty(EducationApprover1.Approval_Manager__c) && idToUser.containsKey(EducationApprover1.Approval_Manager__c)) {
                    educationApprover3 =idToUser.get(educationApprover2.Approval_Manager__c);
                        if(educationApprover3 != null) {
                            if(!String.isEmpty(educationApprover3.Quote_Approval_Role__c)) {
                            quoteItem.Education_Approver_3_Role__c = educationApprover3.Quote_Approval_Role__c;
                            }
                            quoteItem.Education_Approver_3__c = educationApprover3.id;
                        }
                  }
                   else{
                       quoteItem.Education_Approver_3_Role__c =null;
                       quoteItem.Education_Approver_3__c=null; 
                   }
              }
              else{
              CleanConsultingEducationFields(quoteItem);
          }
        }
        system.debug('Quote_SetApproverFieldsHandler populateEducationFields end');
        return inputList;
    }

    private static  List<SBQQ__Quote__c> populateConsultingFields(List<SBQQ__Quote__c> inputList/*, Set<Id> quoteIds*/) {
        system.debug('Quote_SetApproverFieldsHandler populateConsultingFields start');
        User consultingApprover1 = new User();
        User consultingApprover2 = new User();
        User consultingApprover3 = new User();
        Account accountFromQuote = new Account();
        Map<ID,SBQQ__Quote__c> accountIdToQuote = new Map<ID, SBQQ__Quote__c>();
        for (SBQQ__Quote__c quoteItem: inputList){
            accountIdToQuote.put(quoteItem.SBQQ__Account__c, quoteItem);
            System.debug('0C'+quoteItem.SBQQ__Account__c);
        }
        System.debug('1C'+accountIdToQuote);
        Map<ID,Account> idToAccount= new Map<ID, Account>([Select id, Billing_Country_Code__r.Consulting_Quote_Approver__c from Account where id in :accountIdToQuote.keySet()]);
        System.debug('2C'+idToAccount);
        for (SBQQ__Quote__c quoteItem: inputList){
            accountFromQuote = idToAccount.get(quoteItem.SBQQ__Account__c);
            //System.debug('3C'+accountFromQuote.id);
            
            if(accountFromQuote != null && idToUser.containsKey(accountFromQuote.Billing_Country_Code__r.Consulting_Quote_Approver__c) && 
                String.isNotEmpty(accountFromQuote.Billing_Country_Code__r.Consulting_Quote_Approver__c)){
                if(idToUser.containsKey(accountFromQuote.Billing_Country_Code__r.Consulting_Quote_Approver__c)){
                    consultingApprover1 = idToUser.get(accountFromQuote.Billing_Country_Code__r.Consulting_Quote_Approver__c);
                        if(consultingApprover1 != null) {
                            quoteItem.Consulting_Approver_1__c =  consultingApprover1.id;
                                if(!String.isEmpty(consultingApprover1.Quote_Approval_Role__c)) {
                                    quoteItem.Consulting_Approver_1_Role__c = consultingApprover1.Quote_Approval_Role__c;
                                }       
                        }
                }else{
                    quoteItem.Consulting_Approver_1_Role__c =null;
                    quoteItem.Consulting_Approver_1__c=null;   
                }
                if (String.isNotEmpty(consultingApprover1.Approval_Manager__c) && idToUser.containsKey(consultingApprover1.Approval_Manager__c)) {
                consultingApprover2 =idToUser.get(consultingApprover1.Approval_Manager__c);
                    if(consultingApprover2 != null) {
                        if(!String.isEmpty(consultingApprover2.Quote_Approval_Role__c)) {
                            quoteItem.Consulting_Approver_2_Role__c = consultingApprover2.Quote_Approval_Role__c;
                        }
                        quoteItem.Consulting_Approver_2__c = consultingApprover2.id;
                    }
                }else{
                    quoteItem.Consulting_Approver_2_Role__c =null;
                    quoteItem.Consulting_Approver_2__c=null; 
                }
                if(String.isNotEmpty(consultingApprover1.Approval_Manager__c) && idToUser.containsKey(consultingApprover1.Approval_Manager__c)) {
                consultingApprover3 =idToUser.get(consultingApprover2.Approval_Manager__c);
                    if(consultingApprover3 != null) {
                        if(!String.isEmpty(consultingApprover3.Quote_Approval_Role__c)) {
                            quoteItem.Consulting_Approver_3_Role__c = consultingApprover3.Quote_Approval_Role__c;
                        }
                        quoteItem.Consulting_Approver_3__c = consultingApprover3.id;
                    }
                }else{
                    quoteItem.Consulting_Approver_3_Role__c =null;
                    quoteItem.Consulting_Approver_3__c=null; 
                }
            }else{
                CleanConsultingEducationFields(quoteItem);
            }
        }
        system.debug('Quote_SetApproverFieldsHandler populateConsultingFields end');
        return inputList;
    }
    
    private static void populateApproverFields(List<SBQQ__Quote__c> inputList) {
        system.debug('Quote_SetApproverFieldsHandler populateApproverFields start');
        Set<id> accIds = new Set<id>();
        for(SBQQ__Quote__c quote : inputList) {
            if(quote.SBQQ__Account__c != null) {
                accIds.add(quote.SBQQ__Account__c);
            }
        }
        
        List<Account> acclist = [SELECT Billing_Country_Code__r.Subsidiary__r.Approver_Alliances_Director__r.Id, 
                                 Billing_Country_Code__r.Subsidiary__r.Finance_Approver_Business_Partner__r.Id, 
                                 Billing_Country_Code__r.Subsidiary__r.Finance_Approver_Director__r.Id 
                                FROM Account WHERE id IN: accIds];
        for(Account acc : accList) {
            for(SBQQ__Quote__c quote : inputList) {
                if(quote.SBQQ__Account__c == acc.id) {
                    if(acc.Billing_Country_Code__c != null && acc.Billing_Country_Code__r.Subsidiary__c != null) {
                        if(acc.Billing_Country_Code__r.Subsidiary__r.Approver_Alliances_Director__c != null)
                            quote.Approver_Alliances_Director__c = acc.Billing_Country_Code__r.Subsidiary__r.Approver_Alliances_Director__r.Id;
                        if(acc.Billing_Country_Code__r.Subsidiary__r.Finance_Approver_Business_Partner__c != null)
                            quote.Approver_Finance_Business_Partner__c = acc.Billing_Country_Code__r.Subsidiary__r.Finance_Approver_Business_Partner__r.Id;
                        if(acc.Billing_Country_Code__r.Subsidiary__r.Finance_Approver_Director__c != null)
                            quote.Approver_Finance_Director__c = acc.Billing_Country_Code__r.Subsidiary__r.Finance_Approver_Director__r.Id;
                    }
                }
            }
        }
        system.debug('Quote_SetApproverFieldsHandler populateApproverFields end');
    }
    //Used for testing
    public static boolean isPowerPartner = false;
    private static void PopulateSalesRep(List<SBQQ__Quote__c> quotes){
        system.debug('Quote_SetApproverFieldsHandler PopulateSalesRep start');
        Set<Id> accountIds = new Set<Id>();
        Map<Id, Account> accounts = new Map<Id, Account>();
        for(SBQQ__Quote__c quote : quotes) {
            if(quote.SBQQ__Account__c != null && !accountIds.contains(quote.SBQQ__Account__c))
                accountIds.add(quote.SBQQ__Account__c);
            if(quote.Sell_Through_Partner__c != null && !accountIds.contains(quote.Sell_Through_Partner__c))
                accountIds.add(quote.Sell_Through_Partner__c);
        }
        if(accountIds.size() > 0)
            accounts = new Map<Id, Account>([select id, OwnerId, Owner.UserType from account where id in :accountIds]);
        for(SBQQ__Quote__c quote : quotes) {
            system.debug('quote.SBQQ__SalesRep__c before: ' + quote.SBQQ__SalesRep__c);
            system.debug('quote.Revenue_Type__c : ' + quote.Revenue_Type__c);
            system.debug('quote.Revenue_Type__c : ' + quote.Revenue_Type__c);
            
            if((quote.Revenue_Type__c == 'Reseller' || quote.Revenue_Type__c == 'Distributor' || 
                quote.Revenue_Type__c == 'Fulfillment' || quote.Revenue_Type__c == 'Reseller QSG' || 
                quote.Revenue_Type__c == 'Reseller Services-Deferred') && (accounts.get(quote.SBQQ__Account__c).Owner.UserType == 'PowerPartner' || isPowerPartner))
            {
                quote.SBQQ__SalesRep__c = accounts.get(quote.Sell_Through_Partner__c).OwnerId;
                system.debug('Setting salesrep to quote.Sell_Through_Partner__c');
            }
            else
            {
                quote.SBQQ__SalesRep__c = accounts.get(quote.SBQQ__Account__c).OwnerId;
                system.debug('Setting salesrep to quote.SBQQ__Account__c');
            }
            system.debug('quote.SBQQ__SalesRep__c after: ' + quote.SBQQ__SalesRep__c);
        }

        system.debug('Quote_SetApproverFieldsHandler PopulateSalesRep end');
    }



    public static void CleanApproverFields(List<SBQQ__Quote__c> cleanList) {
        system.debug('Quote_SetApproverFieldsHandler CleanApproverFields start');
        for(SBQQ__Quote__c  quoteItem : cleanList){
            quoteItem.Sales_Rep_Approval_Role__c= null;
            quoteItem.Sales_Approver_1_Role__c= null;
            quoteItem.Sales_Approver_2_Role__c= null;
            quoteItem.Sales_Approver_3_Role__c= null;
            quoteItem.Sales_Approver_4_Role__c= null;
            quoteItem.Sales_Approver_5_Role__c= null;
            quoteItem.Sales_Approver_1__c= null;
            quoteItem.Sales_Approver_2__c= null;
            quoteItem.Sales_Approver_3__c= null;
            quoteItem.Sales_Approver_4__c= null;
            quoteItem.Sales_Approver_5__c= null;
        }
        system.debug('Quote_SetApproverFieldsHandler CleanApproverFields end');
    }
    private static void CleanApproverFields(SBQQ__Quote__c quoteToClean) {
        system.debug('Quote_SetApproverFieldsHandler CleanApproverFields (Single) start');
        quoteToClean.Sales_Rep_Approval_Role__c= null;
        quoteToClean.Sales_Approver_1_Role__c= null;
        quoteToClean.Sales_Approver_2_Role__c= null;
        quoteToClean.Sales_Approver_3_Role__c= null;
        quoteToClean.Sales_Approver_4_Role__c= null;
        quoteToClean.Sales_Approver_5_Role__c= null;
        quoteToClean.Sales_Approver_1__c= null;
        quoteToClean.Sales_Approver_2__c= null;
        quoteToClean.Sales_Approver_3__c= null;
        quoteToClean.Sales_Approver_4__c= null;
        quoteToClean.Sales_Approver_5__c= null;
        system.debug('Quote_SetApproverFieldsHandler CleanApproverFields (Single) end');
    }
      private static void CleanConsultingEducationFields(SBQQ__Quote__c quoteToClean) {
        system.debug('Quote_SetApproverFieldsHandler CleanConsultingEducationFields start');
        quoteToClean.Consulting_Approver_1__c= null;
        quoteToClean.Consulting_Approver_2__c= null;
        quoteToClean.Consulting_Approver_3__c= null;
        quoteToClean.Education_Approver_1__c= null;
        quoteToClean.Education_Approver_2__c= null;
        quoteToClean.Education_Approver_3__c= null;
        quoteToClean.Consulting_Approver_1_Role__c= null;
        quoteToClean.Consulting_Approver_2_Role__c= null;
        quoteToClean.Consulting_Approver_3_Role__c= null;
        quoteToClean.Education_Approver_1_Role__c= null;
        quoteToClean.Education_Approver_2_Role__c= null;
        quoteToClean.Education_Approver_3_Role__c= null;
        system.debug('Quote_SetApproverFieldsHandler CleanConsultingEducationFields end');
    }

    private static List<SBQQ__Quote__c> getNotEmptySalesRepQuotes(List<SBQQ__Quote__c> inputList){
        system.debug('Quote_SetApproverFieldsHandler getNotEmptySalesRepQuotes start');
        List<SBQQ__Quote__c> toWork = new List<SBQQ__Quote__c>();
        List<SBQQ__Quote__c> toClean = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c  quoteItem : inputList){
             if(String.isEmpty( quoteItem.SBQQ__SalesRep__c )){
                 toClean.add(quoteItem);   
                }
              if(String.isNotEmpty( quoteItem.SBQQ__SalesRep__c )){
                 toWork.add(quoteItem);   
                }
             
        }
        System.debug(toWork);
        CleanApproverFields(toClean);
        system.debug('Quote_SetApproverFieldsHandler getNotEmptySalesRepQuotes end');
        return toWork;
        
    }
      /*private static List<SBQQ__Quote__c> getNotEmptyCosultingQuotes(List<SBQQ__Quote__c> inputList){
        system.debug('Quote_SetApproverFieldsHandler getNotEmptyCosultingQuotes start');
        List<SBQQ__Quote__c> toWork = new List<SBQQ__Quote__c>();
        List<SBQQ__Quote__c> toClean = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c  quoteItem : inputList){
             if(String.isEmpty( quoteItem.SBQQ__SalesRep__c )){
                 toClean.add(quoteItem);   
          }
              if(String.isNotEmpty( quoteItem.SBQQ__SalesRep__c )){
                 toWork.add(quoteItem);   
          }
       
        }
        CleanApproverFields(toClean);
        system.debug('Quote_SetApproverFieldsHandler getNotEmptyCosultingQuotes end');
        return toWork;

    }*/
    
    public class stException extends Exception{}
}