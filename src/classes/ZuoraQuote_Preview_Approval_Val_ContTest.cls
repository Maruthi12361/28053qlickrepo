@isTest
public class ZuoraQuote_Preview_Approval_Val_ContTest {
	public static String ADDR_COUNTRY     = 'United States';
    public static String ADDR_STATE       = 'California';
    public static final String ADDR_CITY        = 'Foster City';
    public static final String ADDR_STREET  = '1051 E Hillsdale Blvd';
    public static String ADDR_ZIP         = '94404';
    public static final String ZCON_ID = '33333333333333333333333333333333';
    
    @testSetup public static void setupTest(){
        
        QuoteTestHelper.createCustomSettings();
        
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Uniqueness level over 9000', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Revenue_Type__c = 'Direct';
        insert testOpp;

        Product2 testProduct = new Product2();
        testProduct.Name = 'Qlik Data Integration for Data Warehouse Automation';
        testProduct.zqu__SKU__c = 'test113432';
        testProduct.zqu__ZuoraId__c = 'Zu3ora32oductID'+'2345';
        testProduct.zqu__Type__c='Standalone';
        testProduct.zqu__EffectiveStartDate__c = date.newinstance(2000, 1, 1);
        testProduct.zqu__EffectiveEndDate__c = date.newinstance(2050, 1, 1);
        testProduct.zqu__Deleted__c = false;
        insert testProduct;

        zqu__ZProduct__c ZPROD = new zqu__ZProduct__c();
        ZPROD.Name = 'Qlik Data Integration for Data Warehouse Automation';
        ZPROD.zqu__SKU__c = 'SKU-23123331';
        ZPROD.zqu__ZuoraId__c = '2c923233504016b76baf19741e8';
        insert ZPROD;

        zqu__ProductRatePlan__c prp = new zqu__ProductRatePlan__c();
        prp.Name='QDI Data Warehouse for RDBMS/SAP - 1 Target';
        prp.zqu__ProductRatePlanFullName__c = 'QDI Data Warehouse for RDBMS/SAP - 1 Target';
        PRP.zqu__Product__c = testProduct.ID;
        PRP.zqu__ZProduct__c = ZPROD.id;
        PRP.zqu__ActiveCurrencies__c = 'USD';
        insert prp;

        zqu__ProductRatePlanCharge__c testprpc = new zqu__ProductRatePlanCharge__c();
        testprpc.zqu__ProductRatePlan__c = prp.id;
        testprpc.name='QDI Data Warehouse for RDBMS/SAP - 1 Target';
        testprpc.zqu__ZuoraId__c = 'prpc' + 'test'+'12345';
        //testprpc.zqu__UOM__c = 'test UOM';
        testprpc.zqu__Type__c = 'Recurring';
        testprpc.zqu__RecurringPeriod__c = 'Month';
        testprpc.zqu__PrepaymentPeriods__c = 1;
        testprpc.zqu__Model__c = 'Flat Fee Pricing';
        testprpc.zqu__ListPrice__c = 5;
        testprpc.zqu__Description__c = 'Test charge';
        testprpc.zqu__DefaultQuantity__c = 10;
        testprpc.zqu__Discount_Apply_Type__c = 0;
        testprpc.zqu__Discount_Level__c = 'RatePlan';
        testprpc.zqu__Upto_How_Many_Periods__c = 0;
        testprpc.zqu__Deleted__c = false;
        testprpc.zqu__ListPriceBase__c = 'Per Billing Period';
        insert testprpc;

        Zuora__CustomerAccount__c billAcc = new Zuora__CustomerAccount__c();
        billAcc.Name = 'Test Billing Account';
        billAcc.Zuora__Account__c = endUserAccount.Id;
        billAcc.Zuora__AccountNumber__c = 'A-001';
        billAcc.Zuora__AutoPay__c = false;
        billAcc.Zuora__Batch__c = 'Batch1';
        billAcc.Zuora__BillCycleDay__c = '1';
        billAcc.Zuora__Balance__c = 0;
        billAcc.Zuora__BillToCity__c = ADDR_CITY;
        billAcc.Zuora__BillToCountry__c = ADDR_COUNTRY;
        billAcc.Zuora__BillToAddress1__c = ADDR_STREET;
        billAcc.Zuora__BillToState__c = ADDR_STATE;
        billAcc.Zuora__BillToPostalCode__c = ADDR_ZIP;
        billAcc.Zuora__BillToId__c = ZCON_ID;
        billAcc.Zuora__BillToName__c = 'Test Name';
        billAcc.Zuora__BillToWorkEmail__c = 'test@test.com';
        billAcc.Zuora__MRR__c = 100;
        billAcc.Zuora__Credit_Balance__c = 100;
        billAcc.Zuora__Currency__c = 'USD';
        billAcc.Zuora__External_Id__c = 'id0123' + String.valueOf((Math.random()*999999).intValue()) + 'KI';
        billAcc.Zuora__PaymentTerm__c = 'Due Upon Receipt';
        billAcc.Zuora__Status__c = 'Active';
        billAcc.Zuora__Zuora_Id__c = 'id0123' + String.valueOf((Math.random()*999999).intValue()) + 'KI';
        insert billAcc;  

        zqu__Quote__c quote = new zqu__Quote__c();
        quote.Name = 'Test Quote';
        quote.zqu__Opportunity__c = testOpp.Id;
        quote.zqu__Account__c = endUserAccount.id;
        quote.zqu__BillToContact__c = endUserContact.id;
        quote.zqu__SoldToContact__c = endUserContact.id;
        quote.zqu__Currency__c = 'USD';
        quote.Billing_Frequency__c ='Annual';
        quote.zqu__Status__c = 'New';
        quote.zqu__SubscriptionTermStartDate__c = Date.today();
        quote.zqu__startdate__c =  Date.today();
        quote.zqu__InitialTerm__c = 12;
        quote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);
        quote.zqu__SubscriptionType__c ='New Subscription';
        quote.zqu__RenewalTerm__c = 12;
        quote.zqu__InvoiceOwnerId__c = billAcc.Zuora__External_Id__c;
        quote.zqu__InvoiceOwnerName__c = billAcc.Name;
        insert quote;
        system.assert(String.isNotBlank(quote.id));

         zqu__QuoteAmendment__c QA = new zqu__QuoteAmendment__c();
           QA.zqu__Quote__c =quote.id;
           QA.Name='amendtest';
           QA.zqu__Type__c ='NewProduct';
 
          insert QA;
        
        //Create Quote Rate Plan
         zqu__QuoteRatePlan__c qrp = new zqu__QuoteRatePlan__c();
        qrp.Name = 'QDI Data Warehouse for RDBMS/SAP - 1 Target';
        qrp.zqu__AmendmentType__c = 'NewProduct';
        qrp.zqu__Quote__c = quote.id;
        qrp.zqu__QuoteAmendment__c = QA.id;  
        qrp.zqu__Product__c = testProduct.ID;
        qrp.zqu__ProductRatePlan__c = prp.id;
        insert qrp;

        zqu__QuoteRatePlanCharge__c qrpc = new zqu__QuoteRatePlanCharge__c();
        qrpc.Name = 'QDI Data Warehouse for RDBMS/SAP - 1 Target';
             qrpc.zqu__QuoteRatePlan__c = qrp.id;
             qrpc.zqu__EffectivePrice__c = 30;
             qrpc.zqu__ProductRatePlanCharge__c = testprpc.id;
             qrpc.zqu__Total__c = 500;
             qrpc.zqu__ListPrice__c = 200;
             qrpc.zqu__Quantity__c = 20;
             qrpc.zqu__ChargeType__c = 'Recurring';
             qrpc.zqu__Model__c = 'Flat Fee Pricing';
             qrpc.zqu__Period__c = 'Annual';
             qrpc.zqu__Description__c = 'Qlik Sense Analyzer Capacity';
            insert  qrpc;
        
        zqu__QuoteChargeDetail__c qcd = new zqu__QuoteChargeDetail__c();
    qcd.Name = 'QDI Data Warehouse for RDBMS/SAP - 1 Target';
    qcd.zqu__Quote__c = quote.id;
    qcd.zqu__ProductRatePlan__c = prp.id;
    qcd.zqu__ProductRatePlanCharge__c = testprpc.id;
    qcd.zqu__ChargeNumber__c = 'saaddfadsda';
    insert qcd;
    }
    
    @isTest
    public static void testValidationpositive(){
        test.startTest();

		 zqu__Quote__c quote = [select id,Name,QDI_Product_Count__c,QDISourceAndTargetExists__c from zqu__Quote__c Limit 1];
        PageReference pageRef = Page.Zuora_Quote_Approval_validation;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('id', quote.id);
        ZuoraQuote_Preview_Approval_Val_Cont controller = new ZuoraQuote_Preview_Approval_Val_Cont();
		
        //controller.redirectToApproval();
        controller.saveQuote();
		system.assert(!controller.ValidationSucceeded);
        test.stopTest();
    }
    
    @isTest
    public static void testValidationnegative(){
        test.startTest();

        zqu__Quote__c quote = [select id,Name,QDI_Product_Count__c,QDISourceAndTargetExists__c,Quote_Approval_Reason__c from zqu__Quote__c Limit 1];
        
        quote.Quote_Approval_Reason__c = 'Approved';
        update quote;
        
        QDI_Data_Sources_And_Target__c qdisrctarg = new QDI_Data_Sources_And_Target__c();
        qdisrctarg.QDI_Zuora_Quote__c = quote.id;
        qdisrctarg.Source__c = 'MongoDB';
        qdisrctarg.Source_Group__c = 'RDBMS';
        qdisrctarg.Targets__c = 'Amazon EMR';
        insert qdisrctarg;
        
        PageReference pageRef = Page.Zuora_Quote_Approval_validation;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('id', quote.id);
        ZuoraQuote_Preview_Approval_Val_Cont controller = new ZuoraQuote_Preview_Approval_Val_Cont();
		
        //controller.redirectToApproval();
        controller.saveQuote();
        controller.onSubmit();
        
        system.assert(controller.ValidationSucceeded);
		system.assert(controller.approvalReasonNotNeeded);
        test.stopTest();
    }

}