/*******************************************************************
* Class UserAddUserToChatterGroupsTest
*
* Test class for UserAddUserToChatterGroups
* 
* Change Log:
* 2014-04-25   AIN (Andreas Nilsson)        Initial Development
*                                           CR# 11324
* 2014-11-11    RDZ                         Fixing test for deployment ext_abj has been
*                                           modified for ext_abg as ext_abj is being used
* 2015-06-03    AIN                         Test class will now insert chatter groups if they are not found
* 2015-10-02    AIN                         Replaced abm with abx as abm existed already
* 2017-04-26   AIN (Andreas Nilsson)        Added logic to avoid SOQL queries in handler, adapted test class
**********************************************************************/
// //
@isTest 
private  class UserAddUserToChatterGroupsTest {
    private static testmethod void Test1() {

        system.debug('----------------------------------------');
        system.debug('- Running insert success test');
        system.debug('----------------------------------------');

        User u = new User();
        //Chatter Free User
        u.ProfileId = '00e20000001P0HW';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.TimeZoneSidKey = 'Europe/London';
        u.DefaultCurrencyIsoCode = 'USD';
        u.FirstName = 'Test';
        u.LastName = 'Testersson';
        u.Alias = 'zyx';
        u.Email  = 'zyx@qlikview.com.qttest';
        u.Username  = 'zyx@qlikview.com.qttest';
        u.CommunityNickname = 'zyx';
        u.FederationIdentifier = 'zyx';

        User u2 = new User();
        //Chatter Free User
        u2.ProfileId = '00e20000001P0HW';
        u2.LocaleSidKey = 'en_US';
        u2.LanguageLocaleKey = 'en_US';
        u2.EmailEncodingKey = 'UTF-8';
        u2.TimeZoneSidKey = 'Europe/London';
        u2.DefaultCurrencyIsoCode = 'USD';
        u2.FirstName = 'Test';
        u2.LastName = 'Testersson';
        u2.Alias = 'zyy';
        u2.Email  = 'zyy@qlikview.com.qttest';
        u2.Username  = 'zyy@qlikview.com.qttest';
        u2.CommunityNickname = 'zyy';
        u2.FederationIdentifier = 'zyy';

        test.startTest();

        
        InsertChatterGroups();

        List<User> users = new List<User>();
        users.add(u);
        users.add(u2);
        insert u;
        system.debug('User ' + u.Id + ' inserted');
        semaphores.UserAddUserToChatterGroupsHasRun = false;
        insert u2;
        system.debug('User ' + u2.Id + ' inserted');


        VerifyUsersInChatterGroups(users, true);

        test.stopTest();


    }

    private static testmethod void Test2() {
        Id pId = [Select Id From Profile Where Name = 'System Administrator'].Id; 
        Id rId = [Select Id From UserRole Where Name = 'CEO'].Id;
        string tt = Datetime.now().format('ymd');

        User adminUser = new User(alias = 'adminu', email='adminuser@testorg.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = pId,
                            timezonesidkey='America/Los_Angeles', username='adminuser' + tt + '@testorg.com', userRoleId=rId);

        system.debug('----------------------------------------');
        system.debug('- Running insert failure test');
        system.debug('----------------------------------------');

        User u = new User();
        //Chatter Free User
        u.ProfileId = '00e20000001P0HW';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.TimeZoneSidKey = 'Europe/London';
        u.DefaultCurrencyIsoCode = 'USD';
        u.FirstName = 'Test';
        u.LastName = 'Testersson';
        u.Alias = 'zyx';
        //Wrong email, will not be added to chatter groups
        u.Email  = 'zyx@gmail.com.qttest';
        u.Username  = 'zyx@qlikview.com.qttest';
        u.CommunityNickname = 'zyx';
        u.FederationIdentifier = 'zyx';

        User u2 = new User();
        //Chatter Free User
        u2.ProfileId = '00e20000001P0HW';
        u2.LocaleSidKey = 'en_US';
        u2.LanguageLocaleKey = 'en_US';
        u2.EmailEncodingKey = 'UTF-8';
        u2.TimeZoneSidKey = 'Europe/London';
        u2.DefaultCurrencyIsoCode = 'USD';
        u2.FirstName = 'Test';
        u2.LastName = 'Testersson';
        u2.Alias = 'zyy';
        u2.Email  = 'zyy@qlikview.com.qttest';
        u2.Username  = 'zyy@qlikview.com.qttest';
        u2.CommunityNickname = 'zyy';
        u2.FederationIdentifier = 'zyy';
        //Not active, will not be added to chatter groups
        u2.IsActive = false;

        User u3 = new User();
        //Chatter Free User
        u3.ProfileId = '00e20000001P0HW';
        u3.LocaleSidKey = 'en_US';
        u3.LanguageLocaleKey = 'en_US';
        u3.EmailEncodingKey = 'UTF-8';
        u3.TimeZoneSidKey = 'Europe/London';
        u3.DefaultCurrencyIsoCode = 'USD';
        u3.FirstName = 'Test';
        u3.LastName = 'Testersson';
        //Consultant, will not be added to chatter groups
        u3.Alias = 'ext_zyy';
        u3.Email  = 'ext_zyy@qlikview.com.qttest';
        u3.Username  = 'ext_zyy@qlikview.com.qttest';
        u3.CommunityNickname = 'ext_zyy';
        u3.FederationIdentifier = 'ext_zyy';

        User u5 = new User();
        //Chatter Free User
        u5.ProfileId = '00e20000001P0HW';
        u5.LocaleSidKey = 'en_US';
        u5.LanguageLocaleKey = 'en_US';
        u5.EmailEncodingKey = 'UTF-8';
        u5.TimeZoneSidKey = 'Europe/London';
        u5.DefaultCurrencyIsoCode = 'USD';
        u5.FirstName = 'Test';
        u5.LastName = 'Testersson';
        u5.Alias = 'xty';
        u5.Email  = 'helpdesk@qlikview.com.qttest';
        u5.Username  = 'xty@qlikview.com.qttest';
        u5.CommunityNickname = 'xty';
        u5.FederationIdentifier = 'xty';

        List<User> users = new List<User>{u, u2, u3, u5};

        test.startTest();

        InsertChatterGroups();
    
        insert users;

        VerifyUsersInChatterGroups(users, false);
        test.stopTest();
    }

    

    private static testmethod void Test3() {
        ////
        system.debug('----------------------------------------');
        system.debug('- Running update already present test');
        system.debug('----------------------------------------');

        User u = new User();
        //Chatter Free User
        u.ProfileId = '00e20000001P0HW';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.TimeZoneSidKey = 'Europe/London';
        u.DefaultCurrencyIsoCode = 'USD';
        u.FirstName = 'Test';
        u.LastName = 'Testersson';
        u.Alias = 'xty';
        u.Email  = 'xty@qlikview.com.qttest';
        u.Username  = 'xty@qlikview.com.qttest';
        u.CommunityNickname = 'xty';
        u.FederationIdentifier = 'xty';

        List<User> users = new List<User>{u};

        test.startTest();

        InsertChatterGroups();

        insert users;

        VerifyUsersInChatterGroups(users, true);

        update u;
        VerifyUsersInChatterGroups(users, true);

        test.stopTest();
    }

    private static testmethod void Test4() {
        system.debug('----------------------------------------');
        system.debug('- Running group not found test');
        system.debug('----------------------------------------');

        UserAddUserToChatterGroups.ChatterGroups = new List<string> {'Not existing group'};

        User u = new User();
        //Chatter Free User
        u.ProfileId = '00e20000001P0HW';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.TimeZoneSidKey = 'Europe/London';
        u.DefaultCurrencyIsoCode = 'USD';
        u.FirstName = 'Test';
        u.LastName = 'Testersson';
        u.Alias = 'xty';
        u.Email  = 'xty@qlikview.com.qttest';
        u.Username  = 'xty@qlikview.com.qttest';
        u.CommunityNickname = 'xty';
        u.FederationIdentifier = 'xty';

        List<User> users = new List<User>{u};

        test.startTest();

        insert users;

        //InsertChatterGroups();

        VerifyUsersInChatterGroups(users, false);

        test.stopTest();

        UserAddUserToChatterGroups.ChatterGroups = new List<string> {'All QlikTech', 'Customer Success Chatter Group'};
    }
    
    private static void VerifyUsersInChatterGroups(List<User> users, boolean expected)
    {
        List<CollaborationGroupMember> cgms;
        List<CollaborationGroup> cgs;
        List<string> chatterGroups = UserAddUserToChatterGroups.ChatterGroups;
        List<Id> chatterGroupIds;

        for(User u : users)
        {
            system.debug('Result for user ID: ' + u.id);  
            chatterGroupIds = new List<Id>();

            cgs = [select id, name from collaborationgroup where name in :ChatterGroups];

            if(cgs.size() == 0)
            {
                system.debug('No chatter groups found with the correct names, failing');
                system.assert(!expected);
            }
            else
            {

                for (CollaborationGroup cg : cgs)
                {
                    for(string chatterGroup : chatterGroups)
                    {
                        if(cg.Name == chatterGroup)
                            chatterGroupIds.add(cg.id);
                    }
                }
            }

            cgms = [select MemberId, CollaborationGroupId from CollaborationGroupMember where (CollaborationGroupId in :chatterGroupIds) and MemberId = :u.id];

            system.debug('Group membership found for ' + cgms.size() + '/'  + chatterGroupIds.size() + ' groups');
            if(cgms.size() == 0)
            {
                system.debug('No Group membership found');
                system.assert(!expected);
            }
            else if(cgms.size() != chatterGroupIds.size())
            {
                system.debug('Group membership not found for all groups');
                //Group membership should be found for all or none
                system.assert(false);
            }
            else
            {
                system.debug('Found group membership for the following users / collaboration groups:');
                for(CollaborationGroupMember cgm : cgms)
                {
                    system.debug('MemberId: ' + cgm.MemberId + ', CollaborationGroupId; ' + cgm.CollaborationGroupId);
                }
                system.assert(expected);
            }
        }
    }
    private static void InsertChatterGroups()
    {
        UserAddUserToChatterGroups.ChatterGroups = new List<string>();
        UserAddUserToChatterGroups.ChatterGroups.Add('UserAddUserToChatterGroupsTest1');
        UserAddUserToChatterGroups.ChatterGroups.Add('UserAddUserToChatterGroupsTest2');
        List<string> chatterGroups = UserAddUserToChatterGroups.ChatterGroups;
        List<CollaborationGroup> cgs;
        CollaborationGroup cg;

        cgs = [select id, name from collaborationgroup where name in :chatterGroups];
        if(cgs.size() == 0)
        {
            cgs = new List<CollaborationGroup>();
            system.debug('No chatter groups from list found, inserting');
            for(string cgName : chatterGroups)
            {
                cg = new CollaborationGroup();
                cg.CollaborationType = 'Public';
                cg.CanHaveGuests = false;
                cg.Description = 'Test 1';
                cg.InformationBody = 'test';
                cg.InformationTitle = 'test';
                cg.Name = cgName;
                cg.OwnerId = Userinfo.getUserId();

                cgs.add(cg);
            }
            insert cgs;
            system.debug('after insert');
        }
    }
}