/******************************************************

    Class: AssetSharing
    Asset record is shared if

    1.  Partner is the “Sell Through Partner” on the Asset record
        Level of access: read/write

    2.  Partner is set as the End User Account on the Asset record 
        Level of access: read/write

    Changelog:
        2017-01-21  TJG     Created file
******************************************************/
                            
global without sharing class AssetSharing implements Database.Batchable<SObject> {
	
	private List<Id> assetIds;
	
	//Constructor
	global AssetSharing(List<Id> assIds){
		this.assetIds = assIds;	
	}

    private Boolean UpdateAssetSharing(List<Asset> assets)
    {
	   
        // recalculate sharing for these assets 
        Set<Id> uniqueAccIds = new Set<Id>();
        Map<Id, Id> AssetToSellThroughPartner = new Map<Id, Id>();
        Map<Id, Id> AssetToEndUserAccount = new Map<Id, Id>();
        // Fetch Lists of Reseller Partners
        for (Asset anyAsset : assets)
        {
            if (!isNullOrEmpty(anyAsset.Reseller__c))
            {
                if (!uniqueAccIds.contains(anyAsset.Reseller__c))
                {
                    uniqueAccIds.add(anyAsset.Reseller__c);
                }

                if (!AssetToSellThroughPartner.containsKey(anyAsset.Id))
                {
                    AssetToSellThroughPartner.put(anyAsset.Id, anyAsset.Reseller__c);
                }
            }

            if (anyAsset.AccountId != null)
            {
                if (!uniqueAccIds.contains(anyAsset.AccountId))
                {
                    uniqueAccIds.add(anyAsset.AccountId);
                }
                if (!AssetToEndUserAccount.containsKey(anyAsset.Id))
                {
                    AssetToEndUserAccount.put(anyAsset.Id, anyAsset.AccountId);
                }
            }
        }

        Boolean noNewShares = false;
        // If no UniqueAccounts found we can quite now
        if (uniqueAccIds.size() == 0)
        {
            System.debug('UpdateAssetSharing: No unique accounts found.');            
            noNewShares =  true;
        }

        // Fetch all the PortalRoles
        Map<Id, Id> accountToRole = new Map<Id,Id>();
        Set<Id> uniquePortalRoles = new Set<Id>();
        List<Id> portalRoleIds = new List<Id>();

        List<UserRole> userRoles = ApexSharingRules.GetListOfUserRolesByPortalAccountIds(uniqueAccIds, 'Partner', 'Executive');
        
        for (UserRole usrRole :userRoles)
        {
            if (!uniquePortalRoles.contains(usrRole.Id))
            {
                uniquePortalRoles.add(usrRole.Id);
                portalRoleIds.add(usrRole.Id);
            }
            accountToRole.put(usrRole.PortalAccountId, usrRole.Id);
        }

        System.debug('uniqueAccIds=' + uniqueAccIds + ', uniquePortalRoles=' + uniquePortalRoles + '###### userRoles=' + userRoles);

        if (uniquePortalRoles.size() == 0)
        {            
            System.debug('UpdateAssetSharing: No unique PortalRoles found.');
            noNewShares =  true;
        }

        Map<Id,Id> roleToGroup = new Map<Id,Id>();

        List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(portalRoleIds, 'RoleAndSubordinates');
        for (Group grp : groups)
        {
            roleToGroup.put(grp.RelatedId, grp.Id);
        }

        if (roleToGroup.size() == 0)
        {
            System.debug('UpdateAssetSharing: No unique Groups found.');
            noNewShares =  true;
        }
        // All data loaded continue to create sharing rules
        List<AssetShare> newAssetShares = new List<AssetShare>();

        if (!noNewShares)
        {
            for (Asset anAsset : assets)
            {
                System.debug('AssetId = ' + anAsset.Id + ', Status = ' + anAsset.Status);
                Id partnerId = ( AssetToSellThroughPartner.containsKey(anAsset.Id) ) ? AssetToSellThroughPartner.get(anAsset.Id) : AssetToEndUserAccount.get(anAsset.Id);

                System.debug('partnerId=' + partnerId);
                System.debug('accountToRole=' + accountToRole);

                if (partnerId != null && accountToRole.containsKey(partnerId))
                {
                    Id stPartnerRole = accountToRole.get(partnerId);
                    if (roleToGroup.containsKey(stPartnerRole))
                    {
                        newAssetShares.add(CreateNewAssetShare(anAsset.Id, 'Edit', roleToGroup.get(stPartnerRole)));
                    }
                }
            }
        }

        // retrieving existing Manual Asset Sharing
        List<AssetShare> assetShares = [select Id,AssetId, UserOrGroupId, AssetAccessLevel from AssetShare 
                    where AssetId in :this.assetIds and RowCause = 'Manual'];

        // filter out those need to be deleted
        List<AssetShare> toDel = new List<AssetShare>();
        for (AssetShare oldShare : assetShares)
        {
            Boolean matched = false;
            for (integer i=0; i<newAssetShares.size(); i++)
            {
                if (oldShare.AssetAccessLevel == newAssetShares[i].AssetAccessLevel &&
                    oldShare.AssetId == newAssetShares[i].AssetId &&
                    oldShare.UserOrGroupId == newAssetShares[i].UserOrGroupId )
                {
                    matched = true;
                    // already in place, no need to insert again
                    newAssetShares.remove(i);
                }
            }
            if (!matched)
            {
                toDel.add(oldShare);
            }
        } 
        // delete those sharing that are no longer valid
        if (toDel.size() > 0)
        {
            System.debug('delete AssetSharing ' + toDel);
            try
            {
                Delete toDel;
            }
            catch (Exception ex)
            {
                System.debug('Failed to delete Asset Shares, Exception: ' + ex.getMessage() + '  At line: ' + ex.getLineNumber());
            }
        }
        System.debug('newAssetShares = ' + newAssetShares);
        // Insert sharing if there is the need
        if (newAssetShares.size() > 0)
        {
            Database.Insert(newAssetShares, false);
        }
        return true;
    }

	private static Boolean isNullOrEmpty(String s) {
        return (null == s) || (0 == s.trim().length());
    }

    private static AssetShare CreateNewAssetShare(string AssetId, string AccessLevel, string UserOrGroupId)
    {
        AssetShare assetShare = new AssetShare(
            AssetId = AssetId,
            AssetAccessLevel = AccessLevel,
            UserOrGroupId = UserOrGroupId,
            RowCause = Schema.AssetShare.RowCause.Manual
        );
        return assetShare;
    }

	global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
		
		List<Asset> asset4Ids = [
            SELECT
                AccountId,
                Name,
                Reseller__c,
                Status
            FROM
                Asset
            WHERE
                Id IN :this.assetIds 
        ];

		List<Id> endUsers = new List<Id>();
        List<Id> sellTPs  = new List<Id>();

        for (Asset asset : asset4Ids)
        {
			if(asset.AccountId != null)
            endUsers.add(asset.AccountId);
			if(asset.Reseller__c != null)
            sellTPs.add(asset.Reseller__c);
        }
		        
		return Database.getQueryLocator( [SELECT
                AccountId,
                Application__c,
                Name,
                Reseller__c,
                Status
            FROM
                Asset
            WHERE
                Id IN :this.assetIds 
                OR Reseller__c IN :sellTPs
                OR AccountId IN :endUsers 
        ]);
    }
    global void execute(Database.BatchableContext bc, List<SObject> records){
        // process each batch of records
		UpdateAssetSharing((Asset[])records);
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    


}