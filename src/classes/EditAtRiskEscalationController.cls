/**
* Change log:
*
*   2019-09-30 ext_bad  IT-2173: Multiple escalations page.
*   2019-09-30 ext_cqb  IT-2336: Remove Completion Date populating 
*/
public with sharing class EditAtRiskEscalationController {
    public At_risk_escalation__c escalation {
        get{
            if (escalation == null) {
                initEscalationFromAccount();
            }
            return escalation;
        }
        set; }
    public Boolean existingRiskEscalation { get; set; }
    public Boolean rejected { get; set; }
    public Attachment attachmentFile { get; set; }
    public AddAtRiskEscalationByAccountController escFromAccountController { get; set; }
    public List<SelectOption> statusOptions { get; set; }
    private ApexPages.StandardController stdController;
    private Account currentAccount;
    private String accId;
    private String escId;

    private static final String REASON_REQUIRED = 'Reason is required';

    public EditAtRiskEscalationController(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        escId = ((At_risk_escalation__c) stdController.getRecord()).Id;
        accId = escId == null ? ((At_risk_escalation__c) stdController.getRecord()).Account__c : null;
        initEscalation();
    }

    public EditAtRiskEscalationController() {
    }

    public void initEscalationFromAccount() {
        accId = escFromAccountController.accId;
        escId = escFromAccountController.escId;
        initEscalation();
    }

    public void initEscalation() {
        if (escId == null) {
            escalation = new At_risk_escalation__c();
            existingRiskEscalation = false;
            escalation.Escalated_By_Name__c = UserInfo.getName();
            escalation.Date_of_Esculation__c = Date.today();
            escalation.Account__c = accId;
        } else {
            existingRiskEscalation = true;
            escalation = [
                    SELECT Id, Account__c, Account__r.Name, Name, Date_of_Esculation__c, Team__c, Reason__c, Comments__c,
                            Comments_for_second_section__c, Completion_date__c, ICSM_Owner__c, Status__c,
                            Escalated_Reason__c, Escalation_reason_correct__c, Reason_for_second_section__c,
                            Rejected__c, Rejected_Date__c, Rejected_Reason__c, ICSM_Confirmed_Reason__c, Escalated_By_Name__c
                    FROM At_risk_escalation__c
                    WHERE Id = :escId
            ];
            rejected = escalation.Status__c == 'Rejected';
            accId = escalation.Account__c;
        }
        currentAccount = [SELECT Id, Escalation_is_in_progress__c FROM Account WHERE Id = :accId];
        attachmentFile = new Attachment();

        initStatusOptions();
    }

    private void initStatusOptions() {
        statusOptions = new List<SelectOption>();
        statusOptions.add(new SelectOption('', '--None--'));
        statusOptions.add(new SelectOption('Complete renewed', 'Complete renewed'));
        statusOptions.add(new SelectOption('Completed lost', 'Completed lost'));
        statusOptions.add(new SelectOption('Partial renewal', 'Partial renewal'));
        statusOptions.add(new SelectOption('In process', 'In process'));
    }

    public PageReference save() {
        if (existingRiskEscalation == true) {
            update escalation;
        } else {
            if (attachmentFile.Body != null) {
                attachmentFile.OwnerId = UserInfo.getUserId();
                attachmentFile.ParentId = currentAccount.Id;
                attachmentFile.IsPrivate = true;
                insert attachmentFile;
            }
            escalation.Submitter_Email__c = UserInfo.getUserEmail();
            insert escalation;
        }
        updateEscalationInProgress();
        return escFromAccountController == null ? goToAccount() : escFromAccountController.back();
    }

    private PageReference goToAccount() {
        PageReference pg = new PageReference('/' + currentAccount.Id);
        pg.setRedirect(true);
        return pg;
    }

    public PageReference cancel() {
        return escFromAccountController == null ? stdController.cancel() : escFromAccountController.back();
    }

    public PageReference saveReason() {
        if (String.isEmpty(escalation.Rejected_Reason__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, REASON_REQUIRED));
            return null;
        }
        escalation.Status__c = 'Rejected';
        escalation.Rejected__c = true;
        escalation.Rejected_Date__c = system.now();
        update escalation;
        updateEscalationInProgress();
        return escFromAccountController == null ? goToAccount() : escFromAccountController.back();
    }

    public void updateEscalationInProgress() {
        List<At_risk_escalation__c> escList = [
                SELECT Id, Completion_date__c, Rejected__c
                FROM At_risk_escalation__c
                WHERE Account__c = :currentAccount.Id AND Completion_date__c = NULL AND Rejected__c = FALSE
        ];
        if (escList.isEmpty() && currentAccount.Escalation_is_in_progress__c) {
            currentAccount.Escalation_is_in_progress__c = false;
            update currentAccount;
        }
        if (!escList.isEmpty() && !currentAccount.Escalation_is_in_progress__c) {
            currentAccount.Escalation_is_in_progress__c = true;
            update currentAccount;
        }
    }
}