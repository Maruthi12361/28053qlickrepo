/**********************************************************
* Class: LiveChatTranscriptTest
* Changes Log:
*   2018-02-13 ext_bad LCE-52 populate Case fields FROM LiveChatTranscript
*   2018-03-01 ext_bad LCE-17 change visibility of ChatTranscriptPost
*************************************************************/
@IsTest
private class LiveChatTranscriptHandlerTest {
    static testMethod void runTest() {
        String liveChatRecordTypeId;
        RecordTypeInfo liveChatRecordTypeInfo = Schema.SObjectType.Case.getRecordTypeInfosByName()
                .get('Live Chat Support Record Type');
        if (liveChatRecordTypeInfo != null) {
            liveChatRecordTypeId = liveChatRecordTypeInfo.getRecordTypeId();
        }

        LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;

        Contact contact1 = new Contact(LastName = 'LastTest1',
                FirstName = 'FirstTest1',
                Email = 'test1@test1.com');
        insert contact1;

        Case caseObj = new Case();
        caseObj.RecordTypeId = liveChatRecordTypeId;
        insert caseObj;

        visitor = [SELECT Id, Name FROM LiveChatVisitor WHERE SessionKey = :visitor.SessionKey];
        LiveChatTranscript transcript = new LiveChatTranscript(Status = 'Completed',
                Customer_Email__c = 'test1@test1.com', CaseId = caseObj.Id,
                LiveChatVisitorId = visitor.Id, StartTime = System.now().addHours(-1), OwnerId = UserInfo.getUserId(),
                EndTime = System.now(), EndedBy = 'Visitor');

        FeedItem item = new FeedItem(Type = 'ChatTranscriptPost', ParentId = caseObj.Id, Visibility = 'InternalUsers');
        insert item;

        Test.startTest();
        insert transcript;
        Test.stopTest();

        caseObj = [SELECT Id, Chat_Start_Time__c, Chat_End_Time__c, Chat_Ended_By__c, OwnerId FROM Case WHERE Id = :caseObj.Id];
        transcript = [SELECT Id, StartTime, EndTime, EndedBy
                        FROM LiveChatTranscript WHERE Id = :transcript.Id];
        item = [SELECT Id, Visibility FROM FeedItem WHERE Id = :item.Id];
        System.assertEquals(caseObj.Chat_Start_Time__c, transcript.StartTime);
        System.assertEquals(caseObj.Chat_End_Time__c, transcript.EndTime);
        System.assertEquals(caseObj.Chat_Ended_By__c, transcript.EndedBy);
        System.assertEquals(caseObj.OwnerId, UserInfo.getUserId());
        System.assertEquals(item.Visibility, 'AllUsers');
    }
}