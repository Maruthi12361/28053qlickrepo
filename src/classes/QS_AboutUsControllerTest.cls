/*
* Change log:
* 2018-02-28	ext_vos		Create test method with revise.
* 2018-11-13    ext_vos     Replace outdated logic. Add testing of redirect to actual page: QS_Home_Page
* 2019-05-05    AIN         IT-1597 Updated for Support Portal Redesign
*/
@isTest
public class QS_AboutUsControllerTest {
	
	static testMethod void test_redirect() {
        QS_AboutUsController controller = new QS_AboutUsController();
        String redirectUrl = controller.redirectToActualPage().getUrl();
        System.assert(redirectUrl.contains('QS_Home_Page'));

        // variables were not set 
        System.assertEquals(null, controller.isPartnerAccount);
        System.assertEquals(null, controller.isCustomer);
        System.assertEquals(null, controller.isGuestUser);
    }
}