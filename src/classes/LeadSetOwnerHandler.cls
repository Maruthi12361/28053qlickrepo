/******************************************************
Authour: Enrico Varriale Fluido Sweden
     
   1. When a Lead change status to "Archived" or "False" or "Lead - Never"
     1.1 update "Archived: Original Owner" with the current owner
     1.2 update the owner to Anna Forsgren Kap
     1.3 The Lead Record Type shall be changed to a new Record Type named "Archived"
     1.4 Let see the previous owner name and record type name 
  

   2. When a user then update the Lead Status value from "Archived" or "False" or "Lead - Never"to another value then the following should happen:
    2.1 update the owner to the "Archived: Original Owner"
    2.2 change the Record Type from "Archived" to the previous Record Type value
    2.3 Let see the previous owner name and record type name

2015-06-01  CCE CR# 31870 Adding extra values to initiate updating the owner, also added Semaphore.
2015-08-19  CCE CR# 20788 Replacing the commented section (updating Lead to Archived state) with a workflow (Lead: SetOwner)
2016-04-26  CCE CR# 82611 - Removal of Not Follow-Up Required Reason field
2017-06-30  CCE CHG0031025 - Addition of Follow-Up DisqualifiedJunk
2017-10-25 AYS BMW-402 : Migrated from LeadSetOwner.trigger.
2018-28-25 AYS BMW-666 : Inactive User Validation added.
******************************************************/ 
public class LeadSetOwnerHandler {
	
	public static void handle(List<Lead> triggerNew, Map<Id,Lead> triggerOldMap){
		System.debug('LeadSetOwner: Starting');
	    if(!Semaphores.TriggerHasRun('LeadSetOwner'))
	    {
	        System.debug('LeadSetOwner: Running');
	        // CREATE MAPS
	        MAP<id,LEAD> toupdate_username = new MAP<id,LEAD> ();
	        MAP<id,LEAD> toupdate_queuename = new MAP<id,LEAD> ();
	        MAP<id,LEAD> toupdate_recordtype = new MAP<id,LEAD> ();
	        // PICKLIST VALUES TO CHECK
	        Set< String > structure = new Set< String >{'Archived', 'False', 'Lead - Never', 'Junk', 'Follow-Up RejectedJunk', 
	                                                    'Follow-Up RejectedInvalid Job Function', 
	                                                    'Follow-Up RejectedCompetitor', 'Follow-Up DisqualifiedNo Fit', 
	                                                    'Follow-Up DisqualifiedLeft Company', 'Follow-Up DisqualifiedJunk'};

	        //Set< String > structure = new Set< String >{'Archived', 'False', 'Lead - Never', 'Junk', 'Follow-Up RejectedJunk', 
	        //                                            'Follow-Up RejectedNot Follow-Up RequiredInvalid Job Function', 
	        //                                            'Follow-Up RejectedNot Follow-Up RequiredCompetitor', 'Follow-Up DisqualifiedNo Fit', 
	        //                                            'Follow-Up DisqualifiedLeft Company'}; //CR# 82611

	        //GET USER ID AND RECORD TYPE ID FROM CUSTOM SETTINGS.
	        SetOwner__c cs = SetOwner__c.getValues('standardset');
	        
	        ZiftSettings__c csZift = ZiftSettings__c.getInstance();

	        //BMW-666 BEGIN

	        Set<Id> archivedOriginalOwnerIDSet = new Set<Id>();
	        
	        for (Lead l: triggerNew){
	            if(l.Archived_Original_Owner_ID__c != null && l.Archived_Original_Owner_ID__c.startsWith('005')){
	            	archivedOriginalOwnerIDSet.add(l.Archived_Original_Owner_ID__c);
	            }
	        }
	        Map<Id, User> userArchivedMap = new Map<Id, User>([
	        	SELECT id, Name, isActive
	        	FROM User
	        	WHERE Id IN: archivedOriginalOwnerIDSet
	        ]);
	        //BMW-666 END

	        for (Lead l: triggerNew){
	            
	            //BMW-396 BEGIN
	            if(l.Status == 'Suspect' && triggerOldMap.get(l.id).Status != 'Suspect'){      
	                if(l.Old_Record_Type_Name__c != null){
	                    try{
	                        l.RecordTypeId =  Schema.SObjectType.Lead.getRecordTypeInfosByName().get(l.Old_Record_Type_Name__c).getRecordTypeId();
	                    }catch(exception e){
	                        l.RecordTypeId = csZift.RT_StandardLead__c;
	                        System.debug('debug_Old_Record_Type_Name_error: ' + e.getMessage());
	                    }
	                }else{
	                    l.RecordTypeId = csZift.RT_StandardLead__c;
	                }                   
	            }
	            //BMW-396 END

	            //GET OLD LEAD'S RECORD
	            Lead  oldLead = triggerOldMap.get(l.Id);   
	            
	            //Creating combinedStatus string as CR# 31870 asks that we are able to test for various status values with specific reasons              
	            string combinedStatus = l.Status;
	            if (combinedStatus == 'Follow-Up Rejected') {
	                combinedStatus += l.Follow_Up_Rejected_Reason__c;
	                //if (combinedStatus == 'Follow-Up RejectedNot Follow-Up Required') {combinedStatus += l.Not_Follow_Up_Required_Reason__c;}  //CR# 82611
	            } else if (combinedStatus == 'Follow-Up Disqualified') {
	                combinedStatus += l.Follow_Up_Disqualified_Reason__c;
	            }
	            System.debug('LeadSetOwner: combinedStatus = ' + combinedStatus);

	            //Creating oldcombinedStatus string as CR# 31870 asks that we are able to test for various status values with specific reasons              
	            string oldcombinedStatus = oldLead.Status;
	            if (oldcombinedStatus == 'Follow-Up Rejected') {
	                oldcombinedStatus += oldLead.Follow_Up_Rejected_Reason__c;
	                //if (oldcombinedStatus == 'Follow-Up RejectedNot Follow-Up Required') {oldcombinedStatus += oldLead.Not_Follow_Up_Required_Reason__c;}  //CR# 82611
	            } else if (oldcombinedStatus == 'Follow-Up Disqualified') {
	                oldcombinedStatus += oldLead.Follow_Up_Disqualified_Reason__c;
	            }
	            System.debug('LeadSetOwner: oldcombinedStatus = ' + oldcombinedStatus);

	            //Replacing the commented section (updating Lead to Archived state) with a workflow (Lead: SetOwner)
	            //CHECK IF LEAD STATUS IS CHANGED TO 'Archived' OR 'False'        

	            if ((oldcombinedStatus != combinedStatus) && (l.Archived_Original_Owner_ID__c!=null)
	            	&& (structure.contains(oldcombinedStatus)) 
	            	&& (structure.contains(combinedStatus) == false )) {
					
					User archivedUser = null;
	                if(l.Archived_Original_Owner_ID__c.startsWith('005') && userArchivedMap.containsKey(l.Archived_Original_Owner_ID__c)){
	                	archivedUser = userArchivedMap.get(l.Archived_Original_Owner_ID__c);
	                }

	                //IF CHANGE OWNER__SAVE ARCHIVED

	                if(archivedUser == null || archivedUser.isActive){
		                string temp=l.OwnerId;
		                l.OwnerId= l.Archived_Original_Owner_ID__c; 
		                l.Archived_Original_Owner_ID__c= temp;
	                }
	                
	                //  FIND THE OWNER'S NAME 
	                string txtLdID = (String)l.Archived_Original_Owner_ID__c;
	                if(txtLdID.startsWith('005')){
	                	toupdate_username.put(l.Archived_Original_Owner_ID__c,l);
	                }else{
	                	toupdate_queuename.put(l.Archived_Original_Owner_ID__c,l);
	                }
	                 // RECORD TYPE
	                 //Change the Record Type from "Archived" to the previous Record Type value
	                If (l.OldRecordType__c!=null){
	                    string recordtypetemp=l.recordtypeid;
	                    l.recordtypeid=l.OldRecordType__c;
	                    l.OldRecordType__c= recordtypetemp;
	                    toupdate_recordtype.put(l.OldRecordType__c,l); 
	                }
	            }   
	        }     
	               
	        if(toupdate_username.size()>0){
	            // USER
	            Map<id,recordtype> mappart = new Map<id,recordtype>([SELECT Id,name from recordtype where ID in : toupdate_recordtype.keyset()]);
	            //Map<id,user> mappauser = new Map<id,user>([SELECT name,id From User where ID in : toupdate_username.keyset()]);
	            for (Lead lu: toupdate_username.values() ){
 					if(userArchivedMap.containsKey(lu.Archived_Original_Owner_ID__c) 
 						&& userArchivedMap.get(lu.Archived_Original_Owner_ID__c).isActive){
	 					
	 					string name=userArchivedMap.get(lu.Archived_Original_Owner_ID__c).name;
		                System.debug('LeadSetOwner: toupdate_username name = ' +  name);
		                lu.Archived_Original_Owner_Name__c=name;
 					}
	                      
	                string rectname=mappart.get(lu.OldRecordType__c).name; 
	                System.debug('LeadSetOwner: toupdate_username rectname = ' +  rectname);
	                lu.Old_Record_Type_Name__c=rectname;
	            }
	        }               
	                         
	        if(toupdate_queuename.size()>0){
	            //QUEUE
	            Map<id,recordtype> mappart = new Map<id,recordtype>([SELECT Id,name from recordtype where ID in : toupdate_recordtype.keyset()]);
	            Map<id,group> mappaqueue = new Map<id,group>([SELECT Id,name from Group where ID in : toupdate_queuename.keyset()]);
	            for (Lead luq: toupdate_queuename.values() ){
	                string name=mappaqueue.get(luq.Archived_Original_Owner_ID__c).name;
	                luq.Archived_Original_Owner_Name__c=name;
	                System.debug('LeadSetOwner: toupdate_queuename name = ' +  name);

	                If (luq.OldRecordType__c!=null){
	                        string rectname=mappart.get(luq.OldRecordType__c).name; 
	                        luq.Old_Record_Type_Name__c=rectname; 
	                        System.debug('LeadSetOwner: toupdate_queuename rectname = ' +  rectname);
	                }
	            }
	        }
	    }    
	    System.debug('LeadSetOwner: Finishing');       
	}

}