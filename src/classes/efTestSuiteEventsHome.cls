//*********************************************************/
// Author: Mark Cane&
// Creation date: 27/08/2010
// Intent:  
//			
// Change History
// --------------
// Author: IRN
// Change date: 2015-10-22 
// Intent: Added mock since the winter16 release
//*********************************************************/
@isTest
private class efTestSuiteEventsHome{	

    static testMethod void controllerUnitTest(){
    	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
    	efTestSuiteHelper.setup();
					
		System.runAs(efTestSuiteHelper.registrantUser){    	    	
	    	efEventForceHomeController ctl = new efEventForceHomeController();    	
			System.assert(ctl.listEvent.size()>0);
	    	
	    	ApexPages.currentPage().getParameters().put('eventId', efTestSuiteHelper.eNoReg.Id);		
			System.assert(ctl.goRegister()!=null);
			//System.assert(ctl.goGroupRegister()!=null);
			
			ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
	    	System.assert(ctl.goAttendeePortal()!=null);
	    	
	    	System.assert(ctl.goLogout()!=null);
		}   
    }
}