/*********************************************
TestOppCreatedBynameTrigger
Description: Test code to validate 
             Opp_CreatedBy_Copy__c field is same as
             CreatedBy field when the
             Opp.Original_Created_by_Function__c is equal
             to BDR, iBDR, EIS


Log History:
2013-07-05    KMH    Initial Development
2014-01-14    KMH    Added Test function for Academic Contact Id
2014-01-02    KMH    Replace UserInfo.getName() with UserInfo.getFirstName()+UserInfo.getLastName() to fix test failures.
2014-06-18    MHG    Fixed broken test method due to new forecasting.
2015-04-20    SLH    Added function and solution area for OP006 validation rule
2015-11-20    NAD    Replaced opportunity stage name 'Academic Program - Open' with 'AP - Open' per CR 66252
2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring
2020-07-09 EXTCDX  Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
				"Deployment Preference" is required in Goal Confirmed stage  
**********************************************/


@isTest
public class TestOppCreatedBynameTrigger {

static testMethod void TestOppCreatedBynameTrigger () {
QTTestutils.GlobalSetup();
 test.startTest(); 
 
   // Adding an account with BillingCountryCode and Qliktech Company
    Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();      
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'United Kingdom',
            CurrencyIsoCode = 'GBP',
            Language__c = 'English',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
   insert QTComp;
   QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
   Account acc = new Account(Name = 'CreatedByName Test',
   Original_Created_by_Function__c='BDR;' , QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
   insert acc;

  
          
   Opportunity testOpp = new Opportunity(
    Name = 'CreatedBy Test Opp 1',
	Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
    StageName = 'Goal Confirmed',
    ForecastCategoryName = 'Omitted',   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
    CloseDate = Date.today(),
    AccountId = acc.Id,
    CurrencyISOCode = 'GBP',   
    Original_Created_by_Function__c = 'IT',
    Function__c = 'Other',
    Solution_Area__c = 'Other',
    Signature_Type__c = 'Digital Signature',
    Included_Products__c = 'Qlik Sense',
    Amount = 100.0
    );
    insert testOpp;        
      
       
    testOpp= [SELECT Id,Name,Original_Created_by_Function__c ,Opp_CreatedBy_Copy__c from Opportunity WHERE Id=:testOpp.Id];
    
    System.Debug(' Createed by ' + testOpp.Opp_CreatedBy_Copy__c);
    
    System.assertEquals(testOpp.Opp_CreatedBy_Copy__c, null);
    
    Semaphores.OpportunityTriggerBeforeUpdate = false;
    Semaphores.OpportunityTriggerAfterUpdate = false;
    testOpp.Original_Created_by_Function__c ='BDR;';
    update(testOpp);
    
    testOpp= [SELECT Id,Name,Original_Created_by_Function__c ,Opp_CreatedBy_Copy__c from Opportunity WHERE Id=:testOpp.Id];
    
    //Opp_CreatedBy_Copy_C field should now be populated by CreatedBy field.    
    System.Debug('Createed by ' + testOpp.Opp_CreatedBy_Copy__c);
    String username =  UserInfo.getFirstName() +' '+ UserInfo.getLastName();
    System.assertEquals(testOpp.Opp_CreatedBy_Copy__c , username);
    
    
  test.stopTest();    
   
  }
  
  static testMethod void TestOppAcademicContactId () {
    QTTestutils.GlobalSetup();
 test.startTest(); 
 
   // Adding an account with BillingCountryCode and Qliktech Company
    Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();      
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'United Kingdom',
            CurrencyIsoCode = 'GBP',
            Language__c = 'English',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
   insert QTComp;
   QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
   Account acc = new Account(Name = 'AcademicAcc Test',
   Original_Created_by_Function__c='BDR;' , QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
   insert acc;

  
   RecordType recordType  = [SELECT Id, Name from  RecordType where sobjecttype = 'Opportunity' and Name = 'Academic Program Opportunity'];
   
   //Create test Contact
   Contact testContact = new Contact( FirstName='Test1', LastName='Contact1', AccountId=acc.Id, LeadSource='WEB - Web Activity');
   insert testContact;

   System.debug('Contact Id Inserted'+ testContact.Id + 'Contact Name'+ testContact.Name);      
   System.debug('Academic RecordType ID'+ recordType.Id);
   Opportunity testOpp = new Opportunity(
    Name = 'Academic Contact',
    StageName = 'AP - Open',
    RecordTypeId = recordType.Id, // Academic Program Opportunity
    Type = 'Existing Customer',
    Revenue_Type__c = 'Academic Program',
    ForecastCategoryName = 'Omitted',
    CloseDate = Date.today() +7,
    AccountId = acc.Id,
    CurrencyISOCode = 'GBP',  
    Email__c ='kmh@qlikview.com',
    Signature_Type__c = 'Digital Signature',
    Academic_Contact_Name__c = testContact.Id
    
    );
    insert testOpp;              
       
    testOpp= [SELECT Id,Name,Academic_ContactId__c from Opportunity WHERE Id=:testOpp.Id];
    
    System.Debug(' Academic Contact Type ' + testOpp.Academic_ContactId__c);
    
    System.assertEquals(testOpp.Academic_ContactId__c, testContact.Id);
    
    test.stopTest();    
   
  }
  
}