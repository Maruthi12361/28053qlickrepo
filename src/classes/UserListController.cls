/******************************************************

	UserListController        
        
    Changelog:
    	2014-09-16  MTM 	CR# 16915 Initial development                                                       
******************************************************/
public class UserListController {
	List<User> primeTousers;
	private User thisUser;
	private ApexPages.StandardController controller;
	public UserListController(ApexPages.StandardController controller) {
        this.controller = controller;
        thisUser = (User) controller.getRecord();
	}
	public List<User> getPrimeToUsers()
	{
		primeTousers = [SELECT Prime_User__c,Name, Title, Alias, UserName, IsACtive, Manager.Name, Id from User where Prime_User__c =: thisUser.Id];
		return primeTousers;
	}    
}