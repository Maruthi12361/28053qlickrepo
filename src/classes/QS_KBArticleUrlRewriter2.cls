/*****************************************************************
* 2017-09-19 AIN Initial implementation
* 2018-06-12 AIN Now strips away parameters from external links before forwarding
* 2018-12-13 extbad change page param from 'id' to 'articleId' to prevent 'invalid parameter value: []' error on the article page
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*****************************************************************/

global class QS_KBArticleUrlRewriter2 implements Site.UrlRewriter {

    String DIRECTORY = '/articles/';
    String DIRECTORY_ARTICLES_SHORT = '/a/';
    String INTERNALARTICLEDIRECTORYBASIC = '/articles/Basic/';
    String INTERNALARTICLEDIRECTORYDIAG = '/articles/Diagnostic/';
    String VISUALFORCE_PAGE = '/QS_CaseWizardKnowledgeArticle?articleId=';
    global static Map<String, PageReference> articleIdMap;


    // The first global method for mapping external URL to an internal one
    global PageReference mapRequestUrl(PageReference myFriendlyUrl) {
        system.debug('QS_KBArticleUrlRewriter2 mapRequestUrl start');
        if(myFriendlyUrl != null) {
            String url = myFriendlyUrl.getUrl();

            if(url != null && url.toLowerCase().startsWith(DIRECTORY)){

                String urlName = url.substring(DIRECTORY.length(),url.length());
                system.debug('urlName: ' + urlName);
                String articleUrlName = (urlName != null && urlName != '' && (urlName.contains('/') || urlName.indexOf('/') != -1) ? urlName.substring(0, urlName.indexOf('/')) : urlName);

                if(articleUrlName.contains('?'))
                    articleUrlName = articleUrlName.substring(0,articleUrlName.indexOf('?'));
                system.debug('articleUrlName: ' + articleUrlName);

                String urlNameBasic = (INTERNALARTICLEDIRECTORYBASIC.length() <= url.length() ? url.substring(INTERNALARTICLEDIRECTORYBASIC.length(),url.length()) : 'WILLNOTBEFOUNDADSADS');
                system.debug('urlNameBasic: ' + urlNameBasic);
                String articleUrlNameBasic = (urlNameBasic != null && urlNameBasic != '' && (urlNameBasic.contains('/') || urlNameBasic.indexOf('/') != -1) ? urlNameBasic.substring(0, urlNameBasic.indexOf('/')) : urlNameBasic);
                if(articleUrlNameBasic.contains('?articleId='))
                    articleUrlNameBasic = articleUrlNameBasic.substring(0,articleUrlNameBasic.indexOf('?articleId='));

                system.debug('articleUrlNameBasic: ' + articleUrlNameBasic);

                String urlNameDiagnostic = (INTERNALARTICLEDIRECTORYDIAG.length() <= url.length() ? url.substring(INTERNALARTICLEDIRECTORYDIAG.length(),url.length()) : 'WILLNOTBEFOUNDADSADS');
                system.debug('urlNameDiagnostic: ' + urlNameDiagnostic);
                String articleUrlNameDiagnostic = (urlNameDiagnostic != null && urlNameDiagnostic != '' && (urlNameDiagnostic.contains('/') || urlNameDiagnostic.indexOf('/') != -1) ? urlNameDiagnostic.substring(0, urlNameDiagnostic.indexOf('/')) : urlNameDiagnostic);
                if(articleUrlNameDiagnostic != null && articleUrlNameDiagnostic.contains('?articleId='))
                    articleUrlNameDiagnostic = articleUrlNameDiagnostic.substring(0,articleUrlNameDiagnostic.indexOf('?articleId='));

                system.debug('articleUrlNameDiagnostic: ' + articleUrlNameDiagnostic);

                // SFDC Debugging added 8th Dec 2014
                String restSalesforceUrl = '';
                //for parameters using /
                if(articleUrlName != null && articleUrlName != '') {



                    if(url != null && url.startsWith(INTERNALARTICLEDIRECTORYBASIC)){
                        system.debug('Expected url= ' + VISUALFORCE_PAGE + articleUrlNameBasic + (restSalesforceUrl != null && restSalesforceUrl != '' ? restSalesforceUrl : ''));
                        return new PageReference(VISUALFORCE_PAGE + articleUrlNameBasic + (restSalesforceUrl != null && restSalesforceUrl != '' ? restSalesforceUrl : ''));
                    }
                    else if(url != null && url.startsWith(INTERNALARTICLEDIRECTORYDIAG)){
                        return new PageReference(VISUALFORCE_PAGE + articleUrlNameDiagnostic + (restSalesforceUrl != null && restSalesforceUrl != '' ? restSalesforceUrl : ''));
                    }
                    else if(url != null && url.startsWith(DIRECTORY)){
                        system.debug('QS_KBArticleUrlRewriter2 mapRequestUrl end');
                        return new PageReference(VISUALFORCE_PAGE + articleUrlName + (restSalesforceUrl != null && restSalesforceUrl != '' ? restSalesforceUrl : ''));
                    }
                }

            }
            else if(url != null && url.toLowerCase().startsWith(DIRECTORY_ARTICLES_SHORT)){

                String urlName = url.substring(DIRECTORY_ARTICLES_SHORT.length(),url.length());

                String articleUrlName = (urlName != null && urlName != '' && (urlName.contains('/') || urlName.indexOf('/') != -1) ? urlName.substring(0, urlName.indexOf('/')) : urlName);
                // SFDC Debugging added 8th Dec 2014
                String restSalesforceUrl = '';
                //for parameters using /
                /*
                if(urlName != null && urlName != '' && urlName.contains('/')) {
                    String parameterFriendUrl = (urlName.substring(urlName.indexOf('/')+1, urlName.length())).removeEnd('/');
                    restSalesforceUrl = processFriendlyUrl(parameterFriendUrl);
                }*/
                if(articleUrlName != null && articleUrlName != '') {
                    system.debug('QS_KBArticleUrlRewriter2 mapRequestUrl end');
                    return new PageReference(VISUALFORCE_PAGE + articleUrlName + (restSalesforceUrl != null && restSalesforceUrl != '' ? restSalesforceUrl : ''));
                }

            }
            //return null;
        }
        system.debug('QS_KBArticleUrlRewriter2 mapRequestUrl end');
        return myFriendlyUrl;
    }

    global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls) {
        /*system.debug('generateUrlFor start');
        
        
        if (mySalesforceUrls == null || mySalesforceUrls.isEmpty() || mySalesforceUrls.size() == 0) {
            return mySalesforceUrls;
        }
        List<PageReference> myFriendlyUrls = new List<PageReference>();
        for(PageReference mySalesforceUrl : mySalesforceUrls) {
            string url = mySalesforceUrl.getUrl();
            String restSalesforceUrl = '';
            system.debug('generateUrlFor url: ' + url);
            if(url != null && url.startsWith(INTERNALARTICLEDIRECTORY)) {
                String urlName = url.substring(INTERNALARTICLEDIRECTORY.length(),url.length());
               
               String articleUrlName = (urlName != null && urlName != '' && (urlName.contains('/') || urlName.indexOf('/') != -1) ? urlName.substring(0, urlName.indexOf('/')) : urlName);
                String parameterFriendUrl = (urlName.substring(urlName.indexOf('/')+1, urlName.length())).removeEnd('/');
                restSalesforceUrl = processFriendlyUrl(parameterFriendUrl);
                system.debug('restSalesforceUrl: '  + restSalesforceUrl);
                myFriendlyUrls.add(mySalesforceUrl);
            }
            else
                myFriendlyUrls.add(mySalesforceUrl);
        }
        system.debug('generateUrlFor end');*/
        return mySalesforceUrls;
    }

    // converts the url (in /caseId or /true/subject etc.. ) to internal salesforce url (&caseId=005 or &fromCaseWizard=true&subject=test etc.)
    /*private static String processFriendlyUrl(String friendlyUrl) {
        
        String salesforceUrl = '';
        if(friendlyUrl != null && friendlyUrl != '') {
            
            if(friendlyUrl.contains('/')) {
                // from case wizard
                String[] salesforceUrlParameters = friendlyUrl.split('/');
                System.debug('^^^^^salesforceUrlParameters: '+salesforceUrlParameters);
                Integer counter = 0;
                for(String salesforceUrlParameter : salesforceUrlParameters) {
                    if(salesforceUrlParameter != null && salesforceUrlParameter != '') {
                        //salesforceUrlParameter = EncodingUtil.urlDecode(salesforceUrlParameter, 'UTF-8');
                        if(counter == 0)
                            salesforceUrl += '&fromCaseWizard='+salesforceUrlParameter;
                        else if(counter == 1)
                            salesforceUrl += '&subject='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                        else if(counter == 2)
                            salesforceUrl += '&severity='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                        else if(counter == 3)
                            salesforceUrl += '&description='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                        else if(counter == 4)
                            salesforceUrl += '&recordTypeName='+(salesforceUrlParameter == 'null' ? '' : salesforceUrlParameter);
                            
                        ++counter;
                    }
                }
                if(salesforceUrl != null && salesforceUrl != '') {
                    if(salesforceUrl.contains('%0A'))
                        salesforceUrl = salesforceUrl.replace('%0A','_newline_'); // changing the newline parameter manually, otherwise it breaks the url
                    if(salesforceUrl.contains('%3F'))
                        salesforceUrl = salesforceUrl.replace('%3F','_questionmark_'); // 
                }
                    
            } else {
                // from case detail page
                salesforceUrl += '&caseId='+friendlyUrl+'/';
            }
            
        }
        return salesforceUrl;
    }*/
}