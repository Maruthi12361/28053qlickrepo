/*
 File Name: UpdateCaseSharingBatch

 2020-04-23 extcqb IT-2768 Fix #Attunity Case Sharing Rules.
*/
global class UpdateCaseSharingBatch implements Database.Batchable<SObject> {

    private String query;

    global UpdateCaseSharingBatch(String query) {
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Case> scope){
        List<Id> caseIds = new List<Id>();
        for (Case theCase : scope) {
            caseIds.add(theCase.Id);
        }
        CaseSharing.UpdateCaseSharing(caseIds);
    }

    global void finish(Database.BatchableContext bc) {}
}