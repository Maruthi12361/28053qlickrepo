/*
Change log
2017-03-26  TJG Fix INVALID_CROSS_REFERENCE_KEY, Record Type ID: this ID value isn't valid for the user: 012D0000000KEKOIA4
*/

@isTest
private class TestUpdateOppContactRolesTrigger
{
    static testMethod void test_UpdateOpportunitySOE()
    {
        test.startTest();
       QTTestUtils.GlobalSetUp();
       QTTestUtils.SetupNSPriceBook('GBP');
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
      insert QTComp;
       List<Sphere_of_Influence__c> sphList = new List<Sphere_of_Influence__c>();
       List<Sphere_of_Influence__c> sphUpList = new List<Sphere_of_Influence__c>();
       //QTTestUtils.createMockOpportunity(String sShortDescription, String sName, String sType, String sRevenueType, String sStage, String sRecordTypeId, String sCurrencyIsoCode, User user);
       User user = QTTestUtils.createMockOperationsAdministrator();
       //RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];
       RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
       Opportunity opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Stage', rt.Id, 'GBP', user);
       //Contact[] cont = [select Id, Name from Contact LIMIT 2];

      Account acc = QTTestUtils.createMockAccount('Test account', user, true);

       Contact[] cont = new Contact[2];
       cont[0] = QTTestUtils.createMockContact(acc.Id);
       cont[1] = QTTestUtils.createMockContact(acc.Id);

       test.stopTest();

       //Test Insert Sphere of Influence
       for(Integer i=0; i<10; i++)
       {
           Sphere_of_Influence__c sph = new Sphere_of_Influence__c(Opportunity__c = opp.Id, 
            Contact__c = cont[0].Id, Role__c = 'Decision Maker;User');
           sphList.add(sph);
       }
       for(Integer i=0; i<10; i++)
       {
           Sphere_of_Influence__c sph = new Sphere_of_Influence__c(Opportunity__c = opp.Id, 
            Contact__c = cont[1].Id, Role__c = 'Implementer');
           sphList.add(sph);
       }
       insert sphList;
       
       List<Sphere_of_Influence__c> sphUpdate = [select Id, Opportunity__c, Role__c, Contact__c
        from Sphere_of_Influence__c LIMIT 4];
       //Test Update Sphere of Influence
       for(Sphere_of_Influence__c sphUp: sphUpdate)
       {
            sphUp.Role__c = 'Beneficiary';
            sphUpList.add(sphUp);
       }
       update sphUpList;
    }
    
    static testMethod void test_DeleteSOE()
    {
      test.startTest();
      QTTestUtils.GlobalSetUp();
      QTTestUtils.SetupNSPriceBook('GBP');
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
      insert QTComp;
      User user = QTTestUtils.createMockOperationsAdministrator();
       //RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];
       RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
       List<Sphere_of_Influence__c> sphDelList = new List<Sphere_of_Influence__c>();
       List<Sphere_of_Influence__c> sphInsertList = new List<Sphere_of_Influence__c>();
       //Opportunity opp = [select Id, Name, OwnerId from Opportunity LIMIT 1];
       Opportunity opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Stage', rt.Id, 'GBP', user);
       //Contact[] cont = [select Id, Name from Contact LIMIT 2];

       Account acc = QTTestUtils.createMockAccount('Test account', user, true);

       Contact[] cont = new Contact[2];
       cont[0] = QTTestUtils.createMockContact(acc.Id);
       cont[1] = QTTestUtils.createMockContact(acc.Id);
      
       Sphere_of_Influence__c sph1 = new Sphere_of_Influence__c(Opportunity__c = opp.Id, 
        Contact__c = cont[0].Id, Role__c = 'Decision Maker');
       Sphere_of_Influence__c sph2 = new Sphere_of_Influence__c(Opportunity__c = opp.Id, 
        Contact__c = cont[0].Id, Role__c = 'Decision Maker;Implementer;User');
           
       Sphere_of_Influence__c sph3 = new Sphere_of_Influence__c(Opportunity__c = opp.Id, 
        Contact__c = cont[1].Id, Role__c = 'Decision Maker');
       Sphere_of_Influence__c sph4 = new Sphere_of_Influence__c(Opportunity__c = opp.Id, 
        Contact__c = cont[0].Id, Role__c = 'Implementer');
       sphInsertList.add(sph1);
       sphInsertList.add(sph2);
       sphInsertList.add(sph3);
       sphInsertList.add(sph4);
       sphDelList.add(sph1);
       sphDelList.add(sph3);

       test.stopTest();
   
       insert sphInsertList;
       sph2.Role__c = 'Implementer;User';
       update sph2;
       delete sphDelList;
    }
}