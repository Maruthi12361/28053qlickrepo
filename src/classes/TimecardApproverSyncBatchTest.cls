/******************************************************************
*
* 2017-02-17 AIN Initial implementation, this batch run changes the approver on the timecard to the PM of the related project 
* if they are different, any approvals that are set to the old PM is also changed to the new PM
* 2019-12-13 extbad Populate pse__Approver__c from linked project to fix the error after pse update 
*			('This approval request requires the next approver to be determined by the Approver field. This value is empty')
* 2020-01-28 extcqb IT-2439 set	Time_Approval_Method__c = 'Project Manager' to match entry criteria of new approval processes
*
******************************************************************/

@isTest
private class TimecardApproverSyncBatchTest {
	
	static testmethod void testBatch() {

		//User that runs test class must have correct currency
		User adminUser = qttestutils.createMockSystemAdministrator();
		adminUser.CurrencyIsoCode = 'USD';
		update adminUser;

		system.runAs(adminUser) {
			User projectmanagerUser1 = qttestutils.createMockSystemAdministrator();
			projectmanagerUser1.FirstName = 'PM User 1';
			update projectmanagerUser1;

			User projectmanagerUser2 = qttestutils.createMockSystemAdministrator();
			projectmanagerUser2.FirstName = 'PM User 2';
			update projectmanagerUser2;

			Account projectmanagerAccount1 = new Account();
			projectmanagerAccount1.Name = 'Project manager 1 account';

			Account projectmanagerAccount2 = new Account();
			projectmanagerAccount2.Name = 'Project manager 2 account';
			
			List<Account> accounts = new List<Account>();
			accounts.add(projectmanagerAccount1);
			accounts.add(projectmanagerAccount2);
			insert accounts;

			Contact projectManagerContact1 = new Contact();
			projectManagerContact1.accountId = projectmanagerAccount1.Id;
			projectManagerContact1.pse__External_Resource__c = true;
			projectManagerContact1.pse__Is_Resource__c = true;
			projectManagerContact1.pse__Is_Resource_Active__c = true;
			projectManagerContact1.pse__Salesforce_User__c = projectmanagerUser1.Id;
			projectManagerContact1.lastname = 'Project manager 1';
			projectManagerContact1.firstname = 'Test';

			Contact projectManagerContact2 = new Contact();
			projectManagerContact2.accountId = projectmanagerAccount2.Id;
			projectManagerContact2.pse__External_Resource__c = true;
			projectManagerContact2.pse__Is_Resource__c = true;
			projectManagerContact2.pse__Is_Resource_Active__c = true;
			projectManagerContact2.pse__Salesforce_User__c = projectmanagerUser2.Id;
			projectManagerContact2.lastname = 'Project manager 2';
			projectManagerContact2.firstname = 'Test';

			List<Contact> contacts = new List<Contact>();
			contacts.add(projectManagerContact1);
			contacts.add(projectManagerContact2);
			insert contacts;

			TestUtil.createBasicTestData();

			TestUtil.testProject.pse__Project_Manager__c = projectManagerContact1.Id;
			TestUtil.testProject.Time_Approval_Method__c = 'Project Manager';
			update TestUtil.testProject;

            pse__Proj__c prTest = [SELECT Id, pse__Project_Manager__c, pse__Project_Manager__r.pse__Salesforce_User__c
                                      FROM pse__Proj__c WHERE Id = :TestUtil.testProject.Id];
            
			pse__Timecard_Header__c timecard = new pse__Timecard_Header__c();
			timecard.pse__Project__c = TestUtil.testProject.Id;
			timecard.pse__Resource__c = TestUtil.testResource.Id;
			timecard.pse__Start_Date__c = TestUtil.testAssignmentStartDate;
			timecard.pse__End_Date__c = TestUtil.testAssignmentEndDate;
			timecard.pse__Assignment__c = TestUtil.testAssignment.Id;
			timecard.pse__Status__c = 'Submitted';
            timecard.pse__Approver__c = prTest.pse__Project_Manager__r.pse__Salesforce_User__c;
			insert timecard;

			List<pse__Timecard_Header__c> cards = [select id, Name, pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c, 
	            pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__r.Name, pse__Approver__c, pse__Approver__r.Name, 
	            Approver_is_Project_Manager__c from pse__Timecard_Header__c where pse__Status__c = 'Submitted'];

			system.assertEquals(cards.size(), 1, 'Number of timecards should be one');

			system.debug('Project Manager: ' + cards[0].pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__r.Name);
			system.debug('Approver: ' + cards[0].pse__Approver__r.Name);
			system.debug('Approver_is_Project_Manager__c: ' + cards[0].Approver_is_Project_Manager__c);

	        List<ProcessInstanceWorkitem> workItems = [select Id, ActorId, OriginalActorId, ProcessInstanceId, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem
		            where ProcessInstance.TargetObjectId = :cards[0].Id];
	        system.debug('Number of workitems found: ' + workItems.size());
	        system.debug('Workitem actor: ' + workItems[0].Actorid);
	        system.debug('Workitem ori actor: ' + workItems[0].OriginalActorId);
	        system.debug('Project Manager Id: ' + cards[0].pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c);
			system.debug('Approver Id: ' + cards[0].pse__Approver__c);

			system.assertEquals(workItems.size(), 1, 'Number of workitems should be one');
			system.assertEquals(workItems[0].Actorid, cards[0].pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c, 'Approver should be PM 1');
			system.assertEquals(workItems[0].Actorid, projectmanagerUser1.Id, 'Approver should be PM 1');

			//Change PM on the project, this is the trigger for the batch job
			TestUtil.testProject.pse__Project_Manager__c = projectManagerContact2.Id;
			update TestUtil.testProject;
	        
			Test.startTest();
			Id pId = Database.executeBatch(new TimecardApproverSyncBatch());		
			Test.stopTest();

			cards = [select id, Name, pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c, 
	            pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__r.Name, pse__Approver__c, pse__Approver__r.Name, 
	            Approver_is_Project_Manager__c from pse__Timecard_Header__c]; 

	        system.assertEquals(cards.size(), 1, 'Number of timecards should be one');
			system.assertEquals(cards[0].pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c, cards[0].pse__Approver__c, 'Project manager should be approver');

			workItems = [select Id, ActorId, OriginalActorId, ProcessInstanceId, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem
		            where ProcessInstance.TargetObjectId = :cards[0].Id];
	        system.debug('Number of workitems found: ' + workItems.size());
	        system.debug('Workitem actor: ' + workItems[0].Actorid);
	        system.debug('Workitem ori actor: ' + workItems[0].OriginalActorId);
	        system.debug('Project Manager Id: ' + cards[0].pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c);
			system.debug('Approver Id: ' + cards[0].pse__Approver__c);

			system.assertEquals(workItems.size(), 1, 'Number of workitems should be one');
			system.assertEquals(workItems[0].Actorid, cards[0].pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c, 'Approver should be PM 2');
			system.assertEquals(workItems[0].Actorid, projectmanagerUser2.Id, 'Approver should be PM 2');
		}
	}	
	public static testMethod void testSchedule() {
		Test.StartTest();
		TimecardApproverSyncBatch sh1 = new TimecardApproverSyncBatch();
		String sch = '0 0 23 * * ?'; 
		system.schedule('TimecardApproverSyncBatchTest Check', sch, sh1); 
		Test.stopTest(); 
	}
}