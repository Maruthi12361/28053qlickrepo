/*
    Class: Test_multiSoEEdit
    
    Changelog:
        2013-04-19  CCE     Initial development. Test class for multiSoEEdit_Controller and multiSoEEdit page.
        2014-06-18  MHG     Fixing broken test case due to new forecasting 
        2015-04-20  SLH     Adding function and solution area for OP006 validation rule
		2020-07-09  EXTCDX  Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
				"Deployment Preference" is required in Goal Confirmed stage 
                                    
 */
@isTest
private class Test_multiSoEEdit {
    
    static testMethod void TestAddingNewRecord() {

        QTTestUtils.GlobalSetUp();
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test Opp 1';
		Opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        Opp.StageName = 'Goal Confirmed';           
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        Opp.CloseDate = Date.today();
        Opp.AccountId = acc.Id; 
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Included_Products__c = 'Qlik Sense';
        insert Opp;
        
        List<Sequence_of_Event__c> Events = new List<Sequence_of_Event__c>();
        Sequence_of_Event__c SOE = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today(),
            Proposed_Event__c = 'Interview with Key Players',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE);
        
        Sequence_of_Event__c SOE1 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(1),
            Proposed_Event__c = 'Define Success Metrics',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE1);
        
        Sequence_of_Event__c SOE2 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(2),
            Proposed_Event__c = 'Other',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE2);
        insert Events;
        
        test.startTest();
        PageReference pageRef = new PageReference('/apex/multiSoEEdit?opportunityid=' + Opp.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController StandardController = new ApexPages.StandardController(Opp);
        multiSoEEdit_Controller controller = new multiSoEEdit_Controller(StandardController);

        List<Sequence_of_Event__c> SoEList = controller.getSequence_of_Events();
        System.assert(SoEList != null);
        //we should get our original 3 records plus the new blank one
        System.assert(SoEList.size() == 4); 
        
        //Next we will modify an existing record
        Sequence_of_Event__c existingSoE = SoEList[1];
        existingSoE.Proposed_Event__c = 'Provide contracts for legal review';
        // and edit the new blank record
        Sequence_of_Event__c newSoE = SoEList[3];
        newSoE.S_o_E_Date__c = Date.today().adddays(4);
        newSoE.Proposed_Event__c = 'Customer Task';
        controller.QuickSaveSet();  
        
        List<Sequence_of_Event__c> newSoEList = controller.getSequence_of_Events();
        System.assert(newSoEList != null);
        //we should get our original 3 records plus the new one plus a new blank one
        System.assert(newSoEList.size() == 5);
        //test we modified an existing record   
        Sequence_of_Event__c testSoEModify = SoEList[1];
        System.assert(testSoEModify.Proposed_Event__c == 'Provide contracts for legal review');             
                
        test.stopTest();
    }
    
    static testMethod void TestCancelingNewRecord() {
        QTTestUtils.GlobalSetUp();
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test Opp 1';
		Opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        Opp.StageName = 'Goal Confirmed';   
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        Opp.CloseDate = Date.today();
        Opp.AccountId = acc.Id; 
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Included_Products__c = 'Qlik Sense';
        insert Opp;
        
        List<Sequence_of_Event__c> Events = new List<Sequence_of_Event__c>();
        Sequence_of_Event__c SOE = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today(),
            Proposed_Event__c = 'Interview with Key Players',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE);
        
        Sequence_of_Event__c SOE1 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(1),
            Proposed_Event__c = 'Define Success Metrics',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE1);
        
        Sequence_of_Event__c SOE2 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(2),
            Proposed_Event__c = 'Other',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE2);
        insert Events;
        
        test.startTest();
        PageReference pageRef = new PageReference('/apex/multiSoEEdit?opportunityid=' + Opp.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController StandardController = new ApexPages.StandardController(Opp);
        multiSoEEdit_Controller controller = new multiSoEEdit_Controller(StandardController);

        List<Sequence_of_Event__c> SoEList = controller.getSequence_of_Events();
        System.assert(SoEList != null);
        //we should get our original 3 records plus the new blank one
        System.assert(SoEList.size() == 4); 
        
        //Edit the new blank record and then Cancel it
        Sequence_of_Event__c newSoE = SoEList[3];
        newSoE.S_o_E_Date__c = Date.today().adddays(4);
        newSoE.Proposed_Event__c = 'Customer Task';
        //do the Cancel
        controller.CancelSet(); 
        
        List<Sequence_of_Event__c> newSoEList = controller.getSequence_of_Events();
        System.assert(newSoEList != null);
        //we should get our original 3 records plus a new blank one
        System.assert(newSoEList.size() == 4);  
                
        test.stopTest();
    }
    
    static testMethod void TestSaveWithoutAddingNewRecord() {
        QTTestUtils.GlobalSetUp();
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test Opp 1';
		Opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        Opp.StageName = 'Goal Confirmed';   
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        Opp.CloseDate = Date.today();
        Opp.AccountId = acc.Id; 
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Included_Products__c = 'Qlik Sense';
        insert Opp;
        
        List<Sequence_of_Event__c> Events = new List<Sequence_of_Event__c>();
        Sequence_of_Event__c SOE = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today(),
            Proposed_Event__c = 'Interview with Key Players',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE);
        
        Sequence_of_Event__c SOE1 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(1),
            Proposed_Event__c = 'Define Success Metrics',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE1);
        
        Sequence_of_Event__c SOE2 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(2),
            Proposed_Event__c = 'Other',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE2);
        insert Events;
        
        test.startTest();
        PageReference pageRef = new PageReference('/apex/multiSoEEdit?opportunityid=' + Opp.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController StandardController = new ApexPages.StandardController(Opp);
        multiSoEEdit_Controller controller = new multiSoEEdit_Controller(StandardController);

        List<Sequence_of_Event__c> SoEList = controller.getSequence_of_Events();
        System.assert(SoEList != null);
        //we should get our original 3 records plus the new blank one
        System.assert(SoEList.size() == 4); 
        
        //Next we will modify an existing record
        Sequence_of_Event__c existingSoE = SoEList[1];
        existingSoE.Proposed_Event__c = 'Provide contracts for legal review';
        //and Save the change
        controller.SaveSet();
        
        List<Sequence_of_Event__c> newSoEList = controller.getSequence_of_Events();
        System.assert(newSoEList != null);
        //we should get our original 3 records plus a new blank one
        System.assert(newSoEList.size() == 4);  
        
        //test we modified an existing record   
        Sequence_of_Event__c testSoEModify = SoEList[1];
        System.assert(testSoEModify.Proposed_Event__c == 'Provide contracts for legal review');             
                
        test.stopTest();
    }

    static testMethod void TestErrorWhileSaving() {
        QTTestUtils.GlobalSetUp();
        System.debug('Test_multiSoEEdit>TestErrorWhileSaving: Starting');
        /*
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
		insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test Opp 1';
		Opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        Opp.StageName = 'Goal Confirmed';   
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        Opp.CloseDate = Date.today();
        Opp.AccountId = acc.Id;
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other'; 
        Opp.Included_Products__c = 'Qlik Sense';
        insert Opp;
        
        List<Sequence_of_Event__c> Events = new List<Sequence_of_Event__c>();
        Sequence_of_Event__c SOE = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today(),
            Proposed_Event__c = 'Interview with Key Players',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE);
        
        Sequence_of_Event__c SOE1 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(1),
            Proposed_Event__c = 'Define Success Metrics',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE1);
        
        Sequence_of_Event__c SOE2 = new Sequence_of_Event__c(
            S_o_E_Date__c = Date.today().adddays(2),
            Proposed_Event__c = 'Other',
            Opportunity_ID__c = Opp.Id
        );
        Events.add(SOE2);
        insert Events;
        
        test.startTest();
        PageReference pageRef = new PageReference('/apex/multiSoEEdit?opportunityid=' + Opp.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController StandardController = new ApexPages.StandardController(Opp);
        multiSoEEdit_Controller controller = new multiSoEEdit_Controller(StandardController);

        List<Sequence_of_Event__c> SoEList = controller.getSequence_of_Events();
        System.assert(SoEList != null);
        //we should get our original 3 records plus the new blank one
        System.assert(SoEList.size() == 4); 
        
        //Edit the new blank record but do not fill in all the Required fields
        // (a validation rule checks we have filled in the Required fields, as
        // we have not we will not be able to Save the page so should still have 4 records)
        Sequence_of_Event__c newSoE = SoEList[3];
        newSoE.S_o_E_Date__c = Date.today().adddays(4);
        
        //try and do the Save
        controller.SaveSet();
        
        //we cannot, so we should still have only four records
        List<Sequence_of_Event__c> ErrSoEList = controller.getSequence_of_Events();
        System.assert(ErrSoEList != null);
        System.assert(ErrSoEList.size() == 4);
                
        test.stopTest();
        System.debug('Test_multiSoEEdit>TestErrorWhileSaving: Finishing');   
    }

}