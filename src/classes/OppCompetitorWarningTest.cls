/**********************************************************************************************
    *Test Class for OppCompetitorWarning

    
        Changelog:
            2014-10-31 MTM      CR# 18551     Initial development
            2015-10-27 IRN      Winter 16 Issue- removed seeAllData, added call to global setup    
			29.03.2017 Rodion Vakulvoskyi fix test failures commenting GlobalSetup();        
***********************************************************************************************/
@isTest
public class OppCompetitorWarningTest
{
		public static testMethod void TestUpdateCompWarning()
        {
            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
            User user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
            Account account = QTTestUtils.CreateAccountForInternalUsers('MatmanAccount', user);
            Test.startTest();
            Opportunity opp = QTTestUtils.createOpportunity('CompTests', 'CompOpp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account);
            System.assertEquals(opp.CompWarning__c, false); 
            OppCompetitorWarning.UpdateCompWarning(opp.Id);            
            opp = [Select Id, CompWarning__c from Opportunity o where o.Id =: opp.Id];
            System.assertEquals(opp.CompWarning__c, true);
            Test.stopTest(); 
        }
}