/**
	CR 7097 
	2013-03-15	TJG	Created
	2014-11-06	TJG	Recreated from backup and updated
 */
@isTest
private class PartnerLeadSharingAfterSubmissionTrTest {

    static testMethod void testPartnerLeadSharing() {
		QTCustomSettings__c qtSettings = QTCustomSettings__c.getValues('Default');
		Subsidiary__c tesForSubs = TestQuoteUtil.createSubsidiary();
		String	partnerOppReg  = qtSettings != null ? qtSettings.LeadRecordTypePartnerOppReg__c : '012D0000000JsWA'; // Lead Record Type - 'Partner Opp Reg'
		String	QualifiedB = qtSettings != null ? qtSettings.LeadStatusQualifiedB__c : 'Qualified - B';
	    
	    // find an active partner contact
	    //User partnerA = [select Id, ContactId from user where isActive = true and contactId<>null and profile__c='PRM - Independent Territory + QlikBuy' limit 1];
	    User partnerA = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');
    		
    	Lead lead0 = new Lead(
			FirstName = 'Tom',
			LastName = 'Jones',
			Email = 'po179@test.com',
			Company = 'Courtly Ltd',
			Phone = '7776213421',
			City = 'Norwich',
			State = 'Norfolk',
			Street = 'New Road',
			PostalCode = 'NR4 4TJ',
			Country = 'UK',
			Industry = 'Telecom',
			Job_Title__c = 'Director',
			HasOptedOutOfEmail = false,
			Goal_Problem_Need_Description__c = 'Problem Description',
			G_P_N_linked_to_person_s_job__c = 'Yes',
			G_P_N_Relevant_to_our_Solution__c = 'Yes',
			Is_the_person_paid_on_this__c = 'No',
			Estimated_Net_License_Revenue__c = 5000.0,
			Opportunity_Description__c = 'Opp Open Description',
			Estimated_Number_of_Users__c = 300,
			Partner_Contact__c = partnerA.contactId,
			Estimated_Close_Date__c = date.Today().addMonths(3),	
			Status = QualifiedB
		);
		
		Insert(lead0);
	
		User qweb = [select Id from user where name like 'QlikTech Webservice%' and isActive = true limit 1];

		lead0.OwnerId = qweb.Id;
		lead0.Partner_Opp_Reg_Submit_Date__c = Date.Today();
		lead0.RecordTypeId = partnerOppReg;
		Semaphores.LeadTriggerHandlerAfterUpdate = false;
		update(lead0);
	
		List<LeadShare> ls = [Select Id from LeadShare where RowCause = 'Manual' and LeadId = :lead0.Id];
	
		System.debug('TestPartnerLeadSharingAfterSubmission leadShare = ' + ls);
	
		System.assertEquals(1, ls.size());
    }
}