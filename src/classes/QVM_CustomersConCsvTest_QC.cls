/*
@author: Malay Desai, Slalom LLC
@date: 01/24/2019
@description: Test class for all Qlik Market Customer pages

    2019-03-05  AIN Moved custom settings to QuoteTestHelper
*/
@isTest
private class QVM_CustomersConCsvTest_QC {
	
  static testMethod void test_ControllerCsv() {
        //Commented because to avoid error 'DUPLICATE_VALUE' during deployment,
        //this error related with duplication QVM_Settings__c
        //QTTestUtils.GlobalSetUp();

        QVM_Settings__c settings = new QVM_Settings__c();
        settings.Pagination_After_Row__c = 25;
        settings.Default_Lead_Record_Type__c = 'Partner Opp Reg';
        settings.Default_Lead_Status__c = 'Raw - Prospect';
        settings.Default_Lead_Source__c = 'PART - Partner';
        insert settings;



        System.debug('QVM_CustomersConCsvTest.test_ControllerCsv: Starting');

        test.startTest(); 

        QVM_CustomersConCsv_QC conCsv = new QVM_CustomersConCsv_QC();

        System.assertEquals(null, conCsv.CusProducts);

        //User jgltest = [select  Id from user where contactId <> null and Profile.Name like 'PRM%' limit 1];
        user jgltest = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');

        jgltest = [select id, name, Contact.Id, Contact.Account.Id from user where id =:jgltest.id];
        system.debug('jgltest.Contact.Id: ' + jgltest.Contact.Id);
        system.debug('jgltest.Contact.Account.Id: ' + jgltest.Contact.Account.Id);

        QVM_Partner_Data__c partner_data = new QVM_Partner_Data__c();
        partner_data.Partner_Account__c = jgltest.Contact.Account.Id;
        partner_data.Approved__c = true;
        insert partner_data;

        System.RunAs(jgltest){

        	PageReference pageRef = Page.QVM_Customers_QC;
    		
    		Test.setCurrentPage(pageRef);

        	QVM_CustomersCon_QC contrl = new QVM_CustomersCon_QC();

        	PageReference p = contrl.exportCustomersExcel();

        	Test.setCurrentPage(p);

        	QVM_CustomersConCsv_QC  cCsv = new QVM_CustomersConCsv_QC();

        	System.assertNotEquals(null, cCsv.CusProducts);

        	PageReference exPage = cCsv.exportCustomersExcel();

        	System.assertNotEquals(null, exPage);
    	
    	}

        test.stopTest();

        System.debug('QVM_CustomersConCsvTest.test_ControllerCsv: Ended'); 
    } 	
}