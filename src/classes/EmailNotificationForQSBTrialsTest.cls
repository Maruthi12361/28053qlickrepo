/*************************************************************************************************************
 Name: EmailNotificationForQSBTrialsTest
 Author: CCE
 Purpose: This is test class for EmailNotificationForQSBTrials
  
 Log History:
 2019-11-13 CCE BMW-1829 Initial Development
*************************************************************************************************************/
@isTest
private class EmailNotificationForQSBTrialsTest {
    
    static testmethod void EmailNotificationForQSBTrials_Test1()
    {
        System.debug('EmailNotificationForQSBTrialsTest: Starting');

        EmailNotificationForQSBTrials.scheduleMeDaily();
        EmailNotificationForQSBTrials enQSBTrials = new EmailNotificationForQSBTrials();

        EmailNotificationForQSBTrialsTest.createLeadEmailNotificationSettings();
        User u = QTTestUtils.createMockSystemAdministrator();
        Account acc0 = QTTestUtils.createMockAccount('TestAccount', u);
        acc0.Segment_New__c = 'Enterprise - Priority';
        update acc0;
        User v = QTTestUtils.createMockSystemAdministrator();
        Account acc1 = QTTestUtils.createMockAccount('MyTestAccount', v);
        Contact c = QTTestUtils.createMockContact(acc0.Id);
        c.Account_Owner__c = acc0.OwnerId;
        c.Last_Campaign_Response_Name__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Dashboard';
        c.Last_Campaign_Response_Date__c = System.now();
        update c;

        List<Lead> testLeadsAcc0 = new List<Lead>();
        for (Integer i = 0; i < 4; i++) {
            Lead l = new Lead(
                    Firstname = 'TestFirstname' + i,
                    Lastname = 'TestLastname',
                    Company = 'TestQTCompany',
                    Country = 'United Kingdom',
                    LeadSource = 'Part - Partner',
                    Phone = '12345',
                    LeanData__Reporting_Matched_Account__c = acc0.Id,
                    Segment__c = 'Commercial - Mid-Market',
                    Last_Campaign_Response_Name__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Dashboard',
                    Last_Campaign_Response_Date__c = System.now(),
                    Owner_of_the_Linked_Account__c = acc0.OwnerId
            );
            testLeadsAcc0.add(l);
        }
        insert testLeadsAcc0;

        List<Lead> ldsret = [select Id, Name, LeanData__Reporting_Matched_Account__c, Owner_of_the_Linked_Account__c FROM Lead WHERE Firstname = 'TestFirstname1'];
        System.debug('EmailNotificationForQSBTrialsTest: ldsret' + ldsret);

        List<Lead> testLeadsAcc1 = new List<Lead>();
        for (Integer i = 0; i < 4; i++) {
            Lead l = new Lead(
                    Firstname = 'TstFirstname' + i,
                    Lastname = 'TestLastname',
                    Company = 'TestQTCompany',
                    Country = 'United Kingdom',
                    LeadSource = 'Part - Partner',
                    Phone = '12345',
                    LeanData__Reporting_Matched_Account__c = acc1.Id,
                    Segment__c = 'Commercial - Mid-Market',
                    Last_Campaign_Response_Name__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Dashboard',
                    Last_Campaign_Response_Date__c = System.now(),
                    Owner_of_the_Linked_Account__c = acc1.OwnerId
            );
            testLeadsAcc1.add(l);
        }
        insert testLeadsAcc1;
        
        Test.startTest();
        Test.stopTest();       
        enQSBTrials.execute(null);
        System.debug('EmailNotificationForQSBTrialsTest: Finishing');
    }

    public static void createLeadEmailNotificationSettings() {
        Lead_Email_Notification_Settings__c lens = new Lead_Email_Notification_Settings__c();
        
        lens.QSB_Trial_Campaign_Name_1__c = 'Global-DM-WebsiteForm-Qlik Sense Business Trial,GLBL-ADV-PaidSearch-Qlik-Sense-Business,GLBL-ADV-PaidSearch-Qlik-Sense-Business-BI,GLBL-ADV-PaidSearch-Qlik-Sense-Business-BigData';
        lens.QSB_Trial_Campaign_Name_2__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Brand-Cloud,GLBL-ADV-PaidSearch-Qlik-Sense-Business-Brand-QV,GLBL-ADV-PaidSearch-Qlik-Sense-Business-BusinessAnalytics,GLBL-ADV-PaidSearch-Qlik-Sense-Business-Cloud';
        lens.QSB_Trial_Campaign_Name_3__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-DataViz,GLBL-ADV-PaidSearch-Qlik-Sense-Business-Display,GLBL-ADV-PaidSearch-Qlik-Sense-Business-Excel';
        lens.QSB_Trial_Campaign_Name_4__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Reporting,GLBL-ADV-PaidSearch-Qlik-Sense-Business-VisualAnalytics,Global-DM-WebsiteForm-Qlik-Sense-Business-Trial';
        lens.QSB_Trial_Campaign_Name_5__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-DataDiscovery';
        lens.QSB_Trial_Campaign_Name_6__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Remarketing';
        lens.QSB_Trial_Campaign_Name_7__c = 'Global-DM-Web-ContactUs-Qlik-Sense-Business';
        lens.QSB_Trial_Campaign_Name_8__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Brand';
        lens.QSB_Trial_Campaign_Name_9__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-Dashboard';
        lens.QSB_Trial_Campaign_Name_10__c = 'GLBL-ADV-PaidSearch-Qlik-Sense-Business-KPI';
        insert lens;
    }
}