/**     
 * File Name : Z_RenewalAutomationUtilityTest
 * Description : This is a test class to test the Z_RenewalAutomationUtility class with the following test methods:
 *               - getSubscriptionFieldsMapTest
 *               - getFieldApiNamesTest
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

@IsTest
private class Z_RenewalAutomationUtilityTest {
    
    private static Zuora__Subscription__c testSubscription;

    static void setup() {
        testSubscription = Z_RenewalTestFactory.makeSubscription();
        insert testSubscription;
    }

    @IsTest
    static void getSubscriptionFieldsMapTest() {
        setup();

        Test.startTest();

        Map<String, Map<String, Object>> subscriptionFieldsMap = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();

        System.assertEquals(subscriptionFieldsMap.size(), 1);

        Test.stopTest();
    }

    @IsTest
    static void getFieldApiNamesTest() {
        setup();

        Test.startTest();

        DescribeSObjectResult opptyDescResult = Opportunity.getSObjectType().getDescribe();
        String fieldSetApiName = 'Renewal_Opp_Query_Fields';
        
        List<String> fieldApiNamesList = Z_RenewalAutomationUtility.getFieldApiNames(opptyDescResult, fieldSetApiName);

        System.assert(fieldApiNamesList.size() > 0);

        Test.stopTest();
    }
}