global class SuMoKnowledgeScheduler implements Schedulable{

   public static string PROCESS = 'ProcessInToSuMoEvents';
   public static final string CRON = '0 0 * * * ? *';


// CaseArticleCreated
   public static id scheduleCaseArticleCreatedProcessing(){
        string query = 'select id, createdById from CaseArticle where createdDate >: runFromDate';
        return createSchedule('CaseArticleCreated' + PROCESS, CRON, query);   
    }
    
    public static void abortCaseArticleCreatedProcessing(){
        abortSchedule('CaseArticleCreated' + PROCESS);
    }

// KnowledgeArticleFirstCreated    
    public static id scheduleKnowledgeArticleFirstCreatedProcessing(){
        string query = 'select id, createdById from KnowledgeArticle where createdDate >: runFromDate';
        return createSchedule('KnowledgeArticleFirstCreated' + PROCESS, CRON, query);
    }
    
    public static void abortKnowledgeArticleFirstCreatedProcessing(){
        abortSchedule('KnowledgeArticleFirstCreated' + PROCESS);
    }

// KnowledgeArticlePublished
    public static id scheduleKnowledgeArticlePublishedProcessing(){
        string query = 'select id, createdById from KnowledgeArticle where LastPublishedDate >: runFromDate';
        return createSchedule('KnowledgeArticlePublished' + PROCESS, CRON, query);
    }
    
    public static void abortKnowledgeArticlePublishedProcessing(){
        abortSchedule('KnowledgeArticlePublished' + PROCESS);
    }

  
    
    private static void abortSchedule(string name){
         for(CronTrigger cronTriggerRecord : [SELECT Id
                                              FROM CronTrigger 
                                              WHERE CronJobDetail.Name =: name]){
             system.abortJob(cronTriggerRecord.id);                         
         }
    }
    
    
    private static id createSchedule(string name, string cronExpression, string query){
        try{
            return System.schedule(name, cronExpression,  new SuMoKnowledgeScheduler(query, name));
        }
        
        catch(exception e){
            system.debug(
                string.format(
                    'SuMoKnowledgeScheduler.{0} {1} : {2}',
                     new list<string>{name, e.getMessage(),e.getStackTraceString()}
                )
            );
            throw e;
        }
    }
    
    private static datetime getLastRunDateForProcess(string name){
        DateTime lastRunDate;
        for(AggregateResult ar : [SELECT Max(CreatedDate) lastCreatedDate
                                  FROM SuMo_Service_Event__c 
                                  WHERE Event_Type__c =: name.remove(PROCESS)]){
            lastRunDate = (DateTime) ar.get('lastCreatedDate');
        }
        
        return lastRunDate == null ? datetime.now().addHours(-1) : lastRunDate;
    }
     
 
    public SuMoKnowledgeScheduler(string soql, string name){
        this(soql,name,getLastRunDateForProcess(name));
    }
    
    public SuMoKnowledgeScheduler(string soql, DateTime runFromDate){
        this(soql,'adHoc', runFromDate);
    }
    
    
    private SuMoKnowledgeScheduler(string soql, string name, DateTime runFromDate){
        this.soql = soql;
        this.name = name;
        this.runFromDate = runFromDate;
    }
    
    
    private DateTime runFromDate;
    private string soql;
    private final string name;
    
   

    global void execute(SchedulableContext sc){
       process();
        
    }
    
    public void process(){
           
            if(runFromDate == null){
                runFromDate = getLastRunDateForProcess(name);
            }
           
            list<SuMo_Service_Event__c> eventRecords = new list<SuMo_Service_Event__c>();
            for(Sobject record : database.query(soql)){
                eventRecords.add(
                    new SuMo_Service_Event__c(
                        User__c = (id) record.get('createdById'), 
                        Amount__c = 1 , 
                        Event_Type__c = name.remove(PROCESS))); 
            }
            insert eventRecords;
              
            runFromDate = null;
       
    }

}