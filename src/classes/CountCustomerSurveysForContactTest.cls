/************************************************************************
*
*   CountCustomerSurveysForContactTest
*   
*   Test class for CountCustomerSurveysForContact.trigger
*       
*   Changelog:
*       2014-10-10  CCE     Initial development for CR# 13717 - Flag for Customer Survey - https://eu1.salesforce.com/a0CD000000j3vD6
*
*************************************************************************/
@isTest
private class CountCustomerSurveysForContactTest {
    
    private static User adminUser;
    private static Account testAccount;
    private static List<Contact> testContacts;
    
    //A simple test that adds and then deletes a Customer Survey to a Contact and tests the count increments and decrements correctly
    //This test provides 100% coverage of the trigger
    static testmethod void AddAndDeleteCustomerSurveyToContact() {

        Setup();
        //Add a Customer Survey to the Contact
        Customer_Survey__c csTest = new Customer_Survey__c(Contact__c = testContacts[0].Id, Name = 'Q123456');

        Test.StartTest();
        insert csTest;

        //and test that the count field and checkbox have been updated correctly
        Contact cs = [SELECT Id, Customer_Survey_Count__c, Responded_to_Customer_Survey__c FROM Contact WHERE Id = :testContacts[0].Id LIMIT 1];
        System.assertEquals(1, cs.Customer_Survey_Count__c);
        System.assertEquals(true, cs.Responded_to_Customer_Survey__c);

        //and now delete the survey and test that the count field and checkbox have been updated correctly
        delete csTest;
        cs = [SELECT Id, Customer_Survey_Count__c, Responded_to_Customer_Survey__c FROM Contact WHERE Id = :testContacts[0].Id LIMIT 1];
        System.assertEquals(0, cs.Customer_Survey_Count__c);
        System.assertEquals(false, cs.Responded_to_Customer_Survey__c);
        
        Test.StopTest();
    }

    //A more complex test that adds multiple Customer Surveys and tests the count increments correctly on multiple Contacts
    static testmethod void AddMultipleCustomerSurveyToContacts() {

        Setup();
        //Add 1 Customer Survey to the first Contact, 2 Customer Surveys to the second Contact and 3 Customer Surveys to the third Contact 
        List<Customer_Survey__c> csToUpdate = new List<Customer_Survey__c>();
        
        Customer_Survey__c csTest0 = new Customer_Survey__c(Contact__c = testContacts[0].Id, Name = 'Q1234560');
        csToUpdate.add(csTest0);     
        Customer_Survey__c csTest1 = new Customer_Survey__c(Contact__c = testContacts[1].Id, Name = 'Q1234561');
        csToUpdate.add(csTest1);     
        Customer_Survey__c csTest2 = new Customer_Survey__c(Contact__c = testContacts[1].Id, Name = 'Q1234562');
        csToUpdate.add(csTest2);     
        Customer_Survey__c csTest3 = new Customer_Survey__c(Contact__c = testContacts[2].Id, Name = 'Q1234563');
        csToUpdate.add(csTest3);     
        Customer_Survey__c csTest4 = new Customer_Survey__c(Contact__c = testContacts[2].Id, Name = 'Q1234564');
        csToUpdate.add(csTest4);     
        Customer_Survey__c csTest5 = new Customer_Survey__c(Contact__c = testContacts[2].Id, Name = 'Q1234565');
        csToUpdate.add(csTest5);        
     
        Test.StartTest();
        insert csToUpdate;

        //and test that the count fields and checkboxs have been updated correctly
        Contact cs = [SELECT Id, Customer_Survey_Count__c, Responded_to_Customer_Survey__c FROM Contact WHERE Id = :testContacts[0].Id LIMIT 1];
        System.assertEquals(1, cs.Customer_Survey_Count__c);
        System.assertEquals(true, cs.Responded_to_Customer_Survey__c);

        cs = [SELECT Id, Customer_Survey_Count__c, Responded_to_Customer_Survey__c FROM Contact WHERE Id = :testContacts[1].Id LIMIT 1];
        System.assertEquals(2, cs.Customer_Survey_Count__c);
        System.assertEquals(true, cs.Responded_to_Customer_Survey__c);

        cs = [SELECT Id, Customer_Survey_Count__c, Responded_to_Customer_Survey__c FROM Contact WHERE Id = :testContacts[2].Id LIMIT 1];
        System.assertEquals(3, cs.Customer_Survey_Count__c);
        System.assertEquals(true, cs.Responded_to_Customer_Survey__c);

                
        Test.StopTest();
    }

    //Create an Account and add multiple Contacts to it
    private static void Setup()
    {
        adminUser = QTTestUtils.createMockSystemAdministrator();
        testAccount = QTTestUtils.createMockAccount('CustSurveyCountTest', adminUser);
        testAccount.Navision_Status__c = 'Customer';
        update testAccount;
        testContacts = QTTestUtils.createMockContacts(testAccount.Id, 3);   
    }
}