/*********************************************
*	Change Log:
*	2017-11-13	ext_vos				Created. 
*									Move test methods from TestPseValidateApprovedBudgetTrigger: testInsertInvalidProjects() and testUpdateToInvalidProjects();
									Move test method from TestPseProjectClosedTrigger.cls;
*									Add test methods for Project Previous Manager: testPreviousProjectManager_update() and testPreviousProjectManager_doNotUpdate();
*	2017-11-14	ext_vos	CHG0031444	Add testFillEngagementPartner test method.
*	2017-11-16  ext_vos 			Added the test cases for 'isAfter' trigger actions.
*   2018-03-29  ext_vos CHG0031444  Added test data for required Engagement_Model__c and testFillingPaymentTerms()
    2018-07-18  ext_bad             add GlobalSetUp
********************************************/
@isTest
private class PseProjectTriggerTest {

    static testMethod void testInsertInvalidProjects() {
        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;

        List<pse__Proj__c> testProjects = new List<pse__Proj__c>();
        for (Integer i = 0; i < 20; i++) {
            pse__Proj__c invalidProject = new pse__Proj__c(
                Name = 'Test Project ' + i, 
                pse__Is_Billable__c = true, 
                pse__Is_Active__c = true,
                pse__Stage__c = 'In Progress',
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                pse__Practice__c = practice.Id,
                Invoicing_Type__c = 'Deferred');
            testProjects.add(invalidProject);
        }
        try {
          	Semaphores.PseProjectTriggerBeforeInsert = false;
            insert testProjects;
        } catch (DmlException e) {
            // Assert error message
            System.assert(e.getMessage().contains('Cannot mark a project as \'Billable\''), e.getMessage());
            System.assertEquals(e.getDmlStatusCode(0), 'FIELD_CUSTOM_VALIDATION_EXCEPTION');
            System.debug(e);
        }
    }

    static testMethod void testUpdateToInvalidProjects() {
        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;

        List<pse__Proj__c> testProjects = new List<pse__Proj__c>();
        for (Integer i = 0; i < 20; i++) {
            pse__Proj__c invalidProject = new pse__Proj__c(
                Name = 'Test Project ' + i, 
                pse__Is_Billable__c = false, pse__Is_Active__c = true, 
                pse__Allow_Timecards_Without_Assignment__c = true,
                pse__Stage__c = 'In Progress',
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                pse__Practice__c = practice.Id,
                Invoicing_Type__c = 'Deferred');
            testProjects.add(invalidProject);
        }
        insert testProjects;

        // Update projects incorrectly
        for (pse__Proj__c invalidProject : testProjects) {
            invalidProject.pse__Is_Billable__c = true;
        }
        try {
            Semaphores.PseProjectTriggerBeforeUpdate = false;
            update testProjects;
        } catch (DmlException e) {
            // Assert error message
            System.assert(e.getMessage().contains('Cannot mark a project as \'Billable\''));
            System.assertEquals(e.getDmlStatusCode(0), 'FIELD_CUSTOM_VALIDATION_EXCEPTION');
            System.debug(e);
        }
    }

    static testMethod void testPreviousProjectManager_update() {    	
    	ID adminProfileId = [select id from profile where name = 'System Administrator' limit 1].Id;
		User admin = new User(alias = 'adm', email = 'adminadmin@testorg.com',
                emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                localesidkey = 'en_US', profileid = adminProfileId,
                timezonesidkey = 'America/Los_Angeles', username = 'adminadmin@testorg.com');
		insert admin;

        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;

        pse__Proj__c project = new pse__Proj__c(
			            Name = 'test update project', 
			            pse__Is_Billable__c = false, 
			            pse__Is_Active__c = true, 
			            pse__Allow_Timecards_Without_Assignment__c = true,
			            pse__Stage__c = 'In Progress',
			            pse__Start_Date__c = Date.today().addDays(-10),
			            pse__End_Date__c = Date.today().addDays(30),
			            Key_Engagement_Features__c = 'QlikView',
			            Purpose_of_Engagement__c = 'Test',
			            Sales_Classification__c = 'Internal',
			            Customer_Critial_Success_Factors__c = 'Test',
                        pse__Practice__c = practice.Id,
			            Invoicing_Type__c = 'Deferred');
	    insert project;
		pse__Proj__c toTest = [select Previous_Owner__c, OwnerId from pse__Proj__c where id = :project.Id];
      
        System.assertEquals(UserInfo.getUserId(), toTest.OwnerId);
        System.assertEquals(null, toTest.Previous_Owner__c);
		
   		Semaphores.PseProjectTriggerBeforeUpdate = false;
        project.OwnerId = admin.Id;
        update project;

        toTest = [select Previous_Owner__c, OwnerId from pse__Proj__c where id = :project.Id];
        System.assertEquals(admin.Id, toTest.OwnerId);
        System.assertEquals(UserInfo.getUserId(), toTest.Previous_Owner__c);
    }

    static testMethod void testPreviousProjectManager_doNotUpdate() {
        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;

    	pse__Proj__c project = new pse__Proj__c(
			            Name = 'test do not update project', 
			            pse__Is_Billable__c = false, 
			            pse__Is_Active__c = true, 
			            pse__Allow_Timecards_Without_Assignment__c = true,
			            pse__Stage__c = 'In Progress',
			            pse__Start_Date__c = Date.today().addDays(-10),
			            pse__End_Date__c = Date.today().addDays(30),
			            Key_Engagement_Features__c = 'QlikView',
			            Purpose_of_Engagement__c = 'Test',
			            Sales_Classification__c = 'Internal',
			            Customer_Critial_Success_Factors__c = 'Test',
                        pse__Practice__c = practice.Id,
			            Invoicing_Type__c = 'Deferred');
	    insert project;

		pse__Proj__c toTest = [select Previous_Owner__c, OwnerId from pse__Proj__c where id = :project.Id];
        System.assertEquals(UserInfo.getUserId(), toTest.OwnerId);
		System.assertEquals(null, toTest.Previous_Owner__c);

   		Semaphores.PseProjectTriggerBeforeUpdate = false;
	    project.Name = 'do not update';
	    project.OwnerId = UserInfo.getUserId();
	    update project;
	    toTest = [select Previous_Owner__c from pse__Proj__c where id = :project.Id];
        System.assertEquals(null, toTest.Previous_Owner__c);
    }

    static testMethod void testFillingEngagementPartner() {
    	Account acc = new Account(name ='test');
        insert acc;

        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;

        List<pse__Proj__c> projects = new List<pse__Proj__c>();
    	pse__Proj__c projectOne = new pse__Proj__c(
			            Name = 'project for population', 
			            pse__Is_Billable__c = false, 
			            pse__Is_Active__c = true, 
			            pse__Allow_Timecards_Without_Assignment__c = true,
			            pse__Stage__c = 'In Progress',
			            pse__Start_Date__c = Date.today().addDays(-10),
			            pse__End_Date__c = Date.today().addDays(30),
			            pse__Account__c = acc.Id,
			            Key_Engagement_Features__c = 'QlikView',
			            Purpose_of_Engagement__c = 'Test',
			            Sales_Classification__c = 'Internal',
			            Customer_Critial_Success_Factors__c = 'Test',
			            Invoicing_Type__c = 'Deferred',
                        pse__Practice__c = practice.Id,
			            Engagement_Model__c = 'Partner-Led');
	    projects.add(projectOne);

		pse__Proj__c projectTwo = new pse__Proj__c(
			            Name = 'project without population', 
			            pse__Is_Billable__c = false, 
			            pse__Is_Active__c = true, 
			            pse__Allow_Timecards_Without_Assignment__c = true,
			            pse__Stage__c = 'In Progress',
			            pse__Start_Date__c = Date.today().addDays(-10),
			            pse__End_Date__c = Date.today().addDays(30),
			            Key_Engagement_Features__c = 'QlikView',
			            Purpose_of_Engagement__c = 'Test',
			            Sales_Classification__c = 'Internal',
			            Customer_Critial_Success_Factors__c = 'Test',
			            Invoicing_Type__c = 'Deferred',
                        pse__Practice__c = practice.Id,
			            Engagement_Model__c = 'No Partner');
	    projects.add(projectTwo);
    	Semaphores.PseProjectTriggerBeforeInsert = false;
	    insert projects;

	    pse__Proj__c toTest = [select Engagement_Partner__c from pse__Proj__c where Id = :projectOne.Id];
	    System.assertEquals(acc.Id, toTest.Engagement_Partner__c);

	    toTest = [select Engagement_Partner__c from pse__Proj__c where Id = :projectTwo.Id];
	    System.assertNotEquals(acc.Id, toTest.Engagement_Partner__c);

   		Semaphores.PseProjectTriggerBeforeUpdate = false;

	    projectTwo.pse__Account__c = acc.Id;
	    projectTwo.Engagement_Model__c = 'Partner-Led';
	    update projectTwo;

	    toTest = [select Engagement_Partner__c from pse__Proj__c where Id = :projectTwo.Id];
	    System.assertEquals(acc.Id, toTest.Engagement_Partner__c);
    }

    static testMethod void testIsAfterActions() {
        QTTestUtils.GlobalSetUp();
        // sample user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert u;
        // Run as user with permission for assignments
        System.runAs(u) {
            // setup account
             Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
             QlikTech_Company__c QTComp = new QlikTech_Company__c(
                                    			Name = 'GBR',
                                    			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
                                    			Country_Name__c = 'United Kingdom',
                                    			Subsidiary__c = testSubs1.id);
			insert QTComp;
			QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
			Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        	insert testAccount;

            pse__Practice__c practice = new pse__Practice__c(Name = 'test');
            insert practice;
    
            // setup Project
            List<pse__Proj__c> projects = new List<pse__Proj__c>();
            for (Integer j = 0; j <= 2; j ++) {
                projects.add(new pse__Proj__c(Name = 'Name' + j, pse__Stage__c = 'Open', 
                                                pse__Account__c = testAccount.Id,
                                                Engagement_Partner__c = testAccount.Id,
                                                pse__Start_Date__c = Date.today().addDays(-10),
                                                pse__End_Date__c = Date.today().addDays(30),
                                                Key_Engagement_Features__c = 'QlikView',
                                                Purpose_of_Engagement__c = 'Test',
                                                Sales_Classification__c = 'Internal',
                                                Customer_Critial_Success_Factors__c = 'Test',
                                                pse__Practice__c = practice.Id,
                                                Invoicing_Type__c = 'Deferred'));
            }
            Semaphores.PseProjectTriggerAfterInsert = false;
            insert projects;

            // setup resource
            Contact resource = new Contact(LastName = 'Test Resource', pse__Is_Resource__c = true, 
                                    pse__Is_Resource_Active__c = true, AccountId = testAccount.Id);
            insert resource;
    
            // permissions
            pse__Permission_Control__c pc = new pse__Permission_Control__c(pse__User__c = u.Id, pse__Resource__c = resource.Id, 
                                                    pse__Cascading_Permission__c = true, pse__Staffing__c = true);
            insert pc;
            List<pse__Permission_Control__c> controls = new List<pse__Permission_Control__c>();
            for (pse__Proj__c pr : projects) {
                controls.add(new pse__Permission_Control__c(pse__User__c = u.Id, pse__Cascading_Permission__c = true, 
                                pse__Staffing__c = true, pse__Project__c = pr.Id));
            }
            insert controls;
    
            // setup assignments
            List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();
            for (pse__Proj__c pr : projects) {
                assignments.add(new pse__Assignment__c(Name = 'Test Assignment' + pr.Name, 
                    pse__Project__c = pr.Id, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                    pse__Resource__c = resource.Id));
            }   
            insert assignments;

            // Test closing project           
            // cancel
            pse__Proj__c pr = projects[0];
            pr.pse__Stage__c = 'Cancelled';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            String testStatus = [SELECT pse__Status__c FROM pse__Assignment__c WHERE pse__Project__c = :pr.Id limit 1].pse__Status__c;
            System.assertEquals('Closed', testStatus);

            // complete
            pr = projects[1];
            pr.Engagement_Model__c = 'No Partner';
            pr.pse__Stage__c = 'Completed';
            pr.Invoice_Contact__c = 'test';
            pr.Payment_Terms__c = '20';
            pr.Expense_Type__c = 'Other';
            pr.Expense_Fixed_Daily_Rate_Price__c = 11;
            pr.Expense_Comments__c = '1111';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            testStatus = [SELECT pse__Status__c FROM pse__Assignment__c WHERE pse__Project__c = :pr.Id limit 1].pse__Status__c;
            System.assertEquals('Closed', testStatus);

            // close
            pr = projects[2];
            pr.pse__Stage__c = 'Closed';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            testStatus = [SELECT pse__Status__c FROM pse__Assignment__c WHERE pse__Project__c = :pr.Id limit 1].pse__Status__c;
            System.assertEquals('Closed', testStatus);

            // test update project
            Project_SE__c seProj = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c = :pr.Id];
            system.assertNotEquals(null, seProj);

            pr.Name = 'new Name 2';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            seProj = [Select Id, Name FROM Project_SE__c WHERE Master_Project_Id__c = :pr.Id];
            system.assertEquals(pr.Name, seProj.Name);

            // test delete project
            Semaphores.PseProjectTriggerAfterDelete = false;
            delete pr;
            List<Project_SE__c> seProjects = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c = :pr.Id];
            system.assertEquals(0, seProjects.size());            
        }
    }

    static testMethod void testFillingPaymentTerms() {
        Account acc = new Account(name ='test');
        insert acc;

        QTTestUtils.GlobalSetUp();
        Steelbrick_Settings__c cs3 = new Steelbrick_Settings__c();
        cs3.Name = 'SteelbrickSettingsDetails';
        cs3.SBPricebookId__c = '01s20000000E0PWAA0';
        cs3.Steelbrick_OppRecordType__c = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
        insert cs3;
        Id oppSalesQCCS_recType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id;

        List<Opportunity> listOpp = new List<Opportunity>();
        Opportunity opp = new Opportunity(
            name='Test Opp 1',
            recordtypeid=oppSalesQCCS_recType,
            StageName = 'Marketing Qualified',
            CurrencyIsoCode = 'GBP',
            CloseDate = System.today(), 
            Signature_Type__c = 'Digital Signature',       
            AccountId = acc.Id,
            Finance_Approved__c = false        
        );
        listOpp.add(opp);
        // no quote
        Opportunity opp2 = new Opportunity(
            name='Test Opp 2',
            recordtypeid=oppSalesQCCS_recType,
            StageName = 'Marketing Qualified',
            CurrencyIsoCode = 'GBP',
            CloseDate = System.today(), 
            Signature_Type__c = 'Digital Signature',       
            AccountId = acc.Id,
            Finance_Approved__c = false        
        );
        listOpp.add(opp2);
        insert listOpp;

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__Account__c = acc.Id;
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Primary__c = true;
        insert quote;

        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;
        
        List<pse__Proj__c> listPr = new List<pse__Proj__c>();
        pse__Proj__c project1 = new pse__Proj__c(
                        Name = 'project for population', 
                        pse__Is_Billable__c = false, 
                        pse__Is_Active__c = true, 
                        pse__Allow_Timecards_Without_Assignment__c = true,
                        pse__Stage__c = 'In Progress',
                        pse__Start_Date__c = Date.today().addDays(-10),
                        pse__End_Date__c = Date.today().addDays(30),
                        pse__Account__c = acc.Id,
                        Key_Engagement_Features__c = 'QlikView',
                        Purpose_of_Engagement__c = 'Test',
                        Sales_Classification__c = 'Internal',
                        Customer_Critial_Success_Factors__c = 'Test',
                        Invoicing_Type__c = 'Deferred',
                        Engagement_Model__c = 'Partner-Led',
                        pse__Opportunity__c = opp.Id,
                        pse__Practice__c = practice.Id
        );
        Semaphores.PseProjectTriggerBeforeInsert = false;
        listPr.add(project1);
        pse__Proj__c project2 = new pse__Proj__c(
                        Name = 'project for population', 
                        pse__Is_Billable__c = false, 
                        pse__Is_Active__c = true, 
                        pse__Allow_Timecards_Without_Assignment__c = true,
                        pse__Stage__c = 'In Progress',
                        pse__Start_Date__c = Date.today().addDays(-10),
                        pse__End_Date__c = Date.today().addDays(30),
                        pse__Account__c = acc.Id,
                        Key_Engagement_Features__c = 'QlikView',
                        Purpose_of_Engagement__c = 'Test',
                        Sales_Classification__c = 'Internal',
                        Customer_Critial_Success_Factors__c = 'Test',
                        Invoicing_Type__c = 'Deferred',
                        Engagement_Model__c = 'Partner-Led',
                        pse__Practice__c = practice.Id,
                        pse__Opportunity__c = opp2.Id);
        Semaphores.PseProjectTriggerBeforeInsert = false;
        listPr.add(project2);
        insert listPr;

        Opportunity opport = [select Id, SBQQ__PrimaryQuote__c from Opportunity where Id =: opp.Id];
        SBQQ__Quote__c quo = [select SBQQ__PaymentTerms__c from SBQQ__Quote__c where Id =: opport.SBQQ__PrimaryQuote__c];
        pse__Proj__c toTest = [select Payment_Terms__c from pse__Proj__c where Id = :project1.Id];
        System.assertEquals(quo.SBQQ__PaymentTerms__c, toTest.Payment_Terms__c);
       

        toTest = [select Payment_Terms__c from pse__Proj__c where Id = :project2.Id];
        System.assertEquals(null, toTest.Payment_Terms__c);

    }
}