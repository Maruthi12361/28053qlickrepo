/**************************************************
* Author: Alberto De Diego from Fluido Oy
*
* Change Log:
* 2012-11-07 Alberto de Diego: modified for CR# 5894
* 2015-11-25 CCE CR# 63778 Redirect users who land on CaseNew to the support portal
*
**************************************************/
public with sharing class CaseNewControllerExtension {
    
    private Case caze;
    private ApexPages.StandardController controller;
    public List<String> nonConsumerEntitlements { get; private set; }

    public CaseNewControllerExtension(ApexPages.StandardController controller) {
        this.controller = controller;
        caze = (Case) controller.getRecord();
        //query custom setting for using in javascript on the page
        List<Non_Consumer_Entitlements__c> aux = [SELECT Id__c FROM Non_Consumer_Entitlements__c];
        nonConsumerEntitlements = new List<String>();
        for(Non_Consumer_Entitlements__c nce : aux) {
        	nonConsumerEntitlements.add(nce.Id__c);
        }
    }
    
    //redirect to default or custom page depending on the record type
    public PageReference redirect() {
        //default new case page in sandbox: 
        //https://cs14.salesforce.com/500/e?retURL=%2F500%2Fo&RecordType=01220000000DZqG&ent=Case       
        String url = '/500/e?'+defaultUrlParameters()+'&nooverride=1';  
        String QlikMarketID = ApexPages.currentPage().getParameters().get('RecordType');
        if(QlikMarketID != null && QlikMarketID.contains('012D0000000K3mZ'))
        {
            PageReference p = Page.QVM_CreateCase;
            return p;
        }
        else if(CaseUtils.queryInternalRecordTypeIds().contains(caze.RecordTypeId)) 
        {
            return new PageReference(url);
        }
        else
        {
            //redirect to Support Portal if user has PRM in their profile (CR# 63778)
            List<Profile> profile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            String profileName = profile[0].Name;
            system.debug('profileName = ' + profileName);
            if (profileName.contains('PRM')) {
                QS_Partner_Portal_Urls__c cs = QS_Partner_Portal_Urls__c.getInstance();
                String urlr = cs.Support_Portal_Url__c+'QS_Home_Page';
                PageReference p = new PageReference(urlr);
                return p;
            }
            else return init();
        }
    }
  
    private static String defaultUrlParameters() {      
        Set<String> withoutParams = new Set<String>(new String[]{'save_new','sfdc.override'});
        return defaultUrlParameters(withoutParams);
    }
    
    private static String defaultUrlParameters(Set<String> removeParams) {
        String res = '';
        List<String> keys = new List<String>(Apexpages.currentPage().getParameters().keySet());
        
        if (removeParams.size() > 0) {
            List<String> aux = new list<String>();
            for (String key: keys) {
                if (!removeParams.contains(key)) aux.add(key);
            }
            keys = aux;
        }
        
        for(Integer i = 0; i < keys.size(); i++) {
            if (keys[i] == '') continue; 
            res += keys[i] + '=' + Apexpages.currentPage().getParameters().get(keys[i]);
            if (i < keys.size() - 1)  res += '&';
        }
        return res;
    }
    
    public User curUser {get; private set;}
    public Account curUserAcc {get; private set;}
    public Boolean partner {get; private set;}
    
    public PageReference init() {
        //Get some information about the current User
        curUser = [select Id, Name, AccountId, ContactId from User where Id = :UserInfo.getUserId()];   
        
        Account curUserAccount = null;
        List<Account> curUserAccounts = [select Id, Name from Account where Id = :curUser.AccountId];        
        if (curUserAccounts.size() > 0) curUserAcc = curUserAccounts[0];
                                    
        //Get some information about the current Contact. ContactId will only be valid if the current User is a Partner or Customer
        Contact curContact = null;
        List<Contact> curContacts = [select Id, Account_Type__c from Contact where Id = :curUser.ContactId];
        if (curContacts.size() > 0) curContact = curContacts[0];
        
        partner = curContact != null && curContact.Account_Type__c == 'Partner';
        return null;
    }
    
            
    //Merged from older controller CaseNewExtension.cls
    /******************************************************

    CaseNewExtension
        
    Extends the CaseNew vf page
    Prepopulates various fields on the CaseNew vf page      
        
    Changelog:
        2012-04-24  CCE         Initial development for CR# 4357 - Changes to CaseNew page
        2012-06-28	FLD			Entitlement set to null so Case doesn't enter the Entitlement process, 
        						the Entitlement is then passed to the edit form as a parameter
                                                      
    ******************************************************/
    public PageReference doSaveAndEdit()
    {       
        SObject so = controller.getRecord();
        upsert so;
    
        string s = '/' + ('' + so.get('Id')) + '/e?retURL=/' + ('' + so.get('Id'));
        System.debug('CaseNewExtension ' + s);
                            
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info, s));
        return new Pagereference(s);
    }
    

    public static testMethod void TEST_doSaveAndEdit()
    {
        PageReference pageRef = new PageReference('/apex/CaseNew');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        CaseNewControllerExtension controllerExtension = new CaseNewControllerExtension(sc);
        System.assertNotEquals(null, controllerExtension.controller);
        Pagereference pr = controllerExtension.doSaveAndEdit();
        string s = pr.getUrl();
        System.assert(s.contains('/e'), 'Assertion failed as /e was not added to the Pagereference');
    }

    //Adding for coverage
    public static testMethod void TEST_redirect()
    {
        PageReference pageRef = new PageReference('/apex/CaseNew');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        CaseNewControllerExtension controllerExtension = new CaseNewControllerExtension(sc);
        System.assertNotEquals(null, controllerExtension.controller);
        Pagereference pr = controllerExtension.redirect();
    }
    
}