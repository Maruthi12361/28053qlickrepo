/*-----------------------------
*Author: MTM Initial development
* Change log
* 2014-09-26 customer portal project
*
* Change Log: 
* 2018-11-01    ext_vos INC0148657: add filter for Licenses. Format style, delete unused commented code.
* 2019-05-05    AIN IT-1597 Updated for Support Portal Redesign
 -----------------------------*/
public with sharing class QS_EnvironmentDetailsController {
    
    //private Id envId;
    
    private static final String UNWANTED_LICENSE_RECORD_TYPE_NAME = 'Obsolete';
    // limit for picklist options on page
    public static final Integer LIMIT_LICENSE_IN_LIST = 100;

    public Id envId { get; set; }
    public Id selectedProductId { get; set; }
    public EnvironmentProduct__c selectedProduct { get; set; }
    
    private Environment__c env;
    public Environment__c environment {get; set;}
    public List<Case> RelatedCases {get; set;}
    
    public List<SelectOption> licenseOptionListFiltered { get; set;}
    public String licenseFilterText {get; set;}
    public Boolean isLicenseLimitExceeded {get; set;}
    private List<Entitlement> allLicenses {get; set;}        
    
    public boolean isPartnerAccount {get; set;}    
    private boolean isCustomerAccount;

    // temporary saved here to avoid deployment errors
    public List<SelectOption> licenseList {get; set;} 

    public QS_EnvironmentDetailsController() {        
        system.debug('QS_EnvironmentDetailsController start');
        if (ApexPages.currentPage().getParameters().size() > 0 && ApexPages.currentPage().getParameters().get('Id') != null) {
            envId = ApexPages.currentPage().getParameters().get('Id');
            retrieveEnvironment();
            RelatedCases = GetCases();
        } else {
            displayEdit = false;
            ApexPages.Message infoMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Environment');
            ApexPages.addMessage(infoMessage);
        }                
        // Specifying whether the current user is customer or partner
        User userRec;// {get; set;}
        if (userinfo.getUserId() != null) {
            userRec = [select Id, Name, FirstName, LastName, Email, ContactId, Contact.AccountId, Contact.Persona__c, Contact.Name, 
                        Contact.Account.Name, Contact.Account.IsPartner, Contact.Account.IsCustomerPortal, 
                        Contact.Account.QT_Designated_Support_Contact__c, Contact.Account.Partner_Support_Contact__c
                    from User 
                    where Id = :userinfo.getUserId() 
                    limit 1];
        }
        isPartnerAccount = (userRec != null && userRec.ContactId != null && userRec.Contact.AccountId != null && userRec.Contact.Account.IsPartner != null) 
                                ? userRec.Contact.Account.IsPartner 
                                : false;
        isCustomerAccount = (userRec != null && userRec.ContactId != null && userRec.Contact.AccountId != null && userRec.Contact.Account.IsCustomerPortal != null) 
                                ? userRec.Contact.Account.IsCustomerPortal 
                                : false;
    }

    private void retrieveEnvironment() {
        System.debug('%%%% retrieveEnvironment staart: ');
        environment = [select e.Virtual__c, 
                                e.Version__c, 
                                e.Type__c, 
                                e.Product__c, 
                                e.Product_License__c,
                                e.Product_License__r.Name,
                                e.Operating_System__c, 
                                e.Name, 
                                e.Description__c, 
                                e.Clustered__c, 
                                e.Architecture__c, 
                                e.Account__c, 
                                e.Account__r.Name,
                                e.Account__r.Billing_Contact__c,
                                e.Customer_Patch_Version__c
                                from Environment__c e 
                                where e.Id =: envId];
    }

    public List<EnvironmentProduct__c> GetEnvironmentProducts() {
        List<EnvironmentProduct__c> environmentProducts = [select Id,Name, 
                                                           Product__c, 
                                                           License__c, 
                                                           License__r.Name 
                                                           from EnvironmentProduct__c 
                                                           where Environment__c =: EnvId];
        return environmentProducts;
    }

    // boolean to toggle of displaying edit version of Environment
    public boolean displayEdit {get; set;}  

    public PageReference EditEnvironment() {
        try {   
            if (EnvId != null) {
                displayEdit = true;
                licenseFilterText = '';
                allLicenses = [select id, Name from Entitlement 
                                where Account.Id = :environment.Account__c 
                                    and (EndDate >= TODAY or Entitlement_Check__c = true)
                                    and RecordType.Name != :UNWANTED_LICENSE_RECORD_TYPE_NAME];
                isLicenseLimitExceeded = allLicenses.size() > LIMIT_LICENSE_IN_LIST;
                setPreviousLicenseValue();
            }            
        } catch (DmlException ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }    

    public PageReference CancelEnvironment() {
        System.debug('%%%QS_EnvDetailController CancelEnvironment');
        try {  
            if (EnvId != null) {
                retrieveEnvironment();
                displayEdit = false;               
            }
        } catch (DmlException ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }    

    public PageReference SaveEnvironment() {
        System.debug('%%%QS_EnvDetailController SaveEnvironment');
        try {  
            if (envId == null) {
                PageReference ref = new PageReference(Page.QS_EnvironmentDetails.getUrl() + '?Id=' + EnvId);
                return ref;
            }
            if (environment.Name == null || environment.Name == '') {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Environment Name cannot be blank!');
                ApexPages.addMessage(error);
                return null;
            }
            System.debug('do save = ' + EnvId);
            update Environment;
            displayEdit = false;
            retrieveEnvironment();
            PageReference ref = new PageReference(Page.QS_EnvironmentDetails.getUrl() + '?Id=' + EnvId);
            return ref;
            
        } catch(DmlException ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }    

    public PageReference redirectToListPage() {
        PageReference ref = Page.QS_EnvironmentListPage;
        return ref;
    }

    // boolean to toggle of displaying pop up dialog
    public boolean displayPopup {get; set;}     

    public PageReference SaveProduct() {        
        displayPopup = false;    
        try {   
            if (selectedProduct != null) {
                update selectedProduct;
            }            
        } catch (DmlException ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }  
 
    public PageReference CancelProduct() {        
        displayPopup = false;    
        try {   
            if (selectedProduct != null) {
                GetEnvironmentProducts();
            }            
        } catch (DmlException ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }  

    public PageReference DeleteProduct() {
        try {   
            System.debug('Product to delete = ' + selectedProductId);
            if (selectedProductId != null) {
                System.debug('do delete = ' + selectedProductId);
                selectedProduct = [select Id from EnvironmentProduct__c where Id =:selectedProductId limit 1];
                delete selectedProduct;
            }
        } catch (DmlException ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }

    public void setPreviousLicenseValue() {
        refreshLicenseList();
        if (isLicenseLimitExceeded && environment.Product_License__c != null) {
            List<Entitlement> currentLicenses = [select id, Name from Entitlement where Id =: environment.Product_License__c];
            if (!currentLicenses.isEmpty()) {
                Entitlement currentOne = currentLicenses[0];
                licenseOptionListFiltered.add(new SelectOption(currentOne.Id, currentOne.Name));
            }
        }          
    }

    public void refreshLicenseList() {
        licenseOptionListFiltered = new List<SelectOption>();
        List<SelectOption> tempLicenseOptions = new List<SelectOption>();
        if (allLicenses != null && !allLicenses.isEmpty()) {
            for (Entitlement accLic : allLicenses) {
                if (isLicenseLimitExceeded) {
                    String filterText = licenseFilterText.toLowerCase();
                    if (accLic.Name.contains(filterText)) {
                        tempLicenseOptions.add(new SelectOption(accLic.Id, accLic.Name));
                    }
                } else { 
                    tempLicenseOptions.add(new SelectOption(accLic.Id, accLic.Name));
                }                
            }
            // check size of the filtered list
            if (tempLicenseOptions.size() > LIMIT_LICENSE_IN_LIST) {
                licenseOptionListFiltered.add(new SelectOption('', 'Too many licenses, please use the filter above (' + tempLicenseOptions.Size() + ')'));   
            } else if (tempLicenseOptions.size() > 0) {
                if (tempLicenseOptions.size() > 1) {
                    licenseOptionListFiltered.add(new SelectOption('', 'Please select a value (' + tempLicenseOptions.Size() + ')'));
                }
                licenseOptionListFiltered.addAll(tempLicenseOptions);
            } else {
                licenseOptionListFiltered.add(new SelectOption('', 'No licenses found'));
            }           
        }
    }

    public PageReference EditProduct() {
        try {   
            displayPopup = true; 
            System.debug('Product to edit = ' + selectedProductId);
            if (selectedProductId != null) {
                System.debug('do edit = ' + selectedProductId);
                selectedProduct = [select Id, Name, Product__c, License__c, Version__c from EnvironmentProduct__c where Id =:selectedProductId limit 1];
            }
        } catch (DmlException ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    } 

    public List<Case> GetCases() {
        System.debug('envId = ' + envId);
        List<Case> tempEnvironments = [select Id, CaseNumber, Status, Environment__c, CreatedDate, Subject
                                        from Case 
                                        where EnvironmentOfCase__c = : envId order by CreatedDate desc];
        return tempEnvironments;
    }
}