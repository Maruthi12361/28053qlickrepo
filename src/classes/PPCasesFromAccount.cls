/************************************************************************
 * @author  KMH
 * 2015-06-16 Suppor Portal Project
 *  
 * On Partner Portal on Account detail page Case related list is replaced with
   Custom VF page(related list) to allow user to Navigate to new Support Portal
   from Case links.
 *
 * addig dummy empty line for testing -Rachana B
 *************************************************************************/

public class PPCasesFromAccount {
    public Account acc;
    public List <Case> Cases{get; set;}
    public PPCasesFromAccount (ApexPages.StandardController controller) {
        this.acc = (Account)controller.getRecord();
    }
 
 
    public List <Case> getCases1() {
        Cases= new List <Case> ();
        Cases = [SELECT ID,
                             AccountId,
                             CaseNumber,
                             Subject,
                             Priority,
                             CreatedDate,
                             OwnerId,
                             Status
                             
                             FROM Case
                             WHERE AccountId = :acc.ID  
                             
                             ];
            System.debug('Contactlist' + Cases);
            return Cases;
            
 
    }
}