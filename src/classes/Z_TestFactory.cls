/*   Copyright (c) 2018 Zuora, Inc.
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy of
 *   this software and associated documentation files (the "Software"), to use copy,
 *   modify, merge, publish the Software and to distribute, and sublicense copies of
 *   the Software, provided no fee is charged for the Software.  In addition the
 *   rights specified above are conditioned upon the following:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   Zuora, Inc. or any other trademarks of Zuora, Inc.  may not be used to endorse
 *   or promote products derived from this Software without specific prior written
 *   permission from Zuora, Inc.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 *   ZUORA, INC. BE LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *   IN THE EVENT YOU ARE AN EXISTING ZUORA CUSTOMER, USE OF THIS SOFTWARE IS GOVERNED
 *   BY THIS AGREEMENT AND NOT YOUR MASTER SUBSCRIPTION AGREEMENT WITH ZUORA.
 */
@isTest
public class Z_TestFactory {

    public static final String ZACC_ID = '11111111111111111111111111111111';
    public static final String ZSUB_ID = '22222222222222222222222222222222';
    public static final String ZCON_ID = '33333333333333333333333333333333';
    public static final String TIER_ID = '34444444444444444444444444444444';
    //Address information
    public static String ADDR_COUNTRY     = 'United States';
    public static String ADDR_STATE       = 'California';
    public static final String ADDR_CITY        = 'Foster City';
    public static final String ADDR_STREET  = '1051 E Hillsdale Blvd';
    public static String ADDR_ZIP         = '94404';
    //Test Date helpers
    public static final Date DATE_TODAY  = System.today();
    public static final Date DATE_PAST   = DATE_TODAY.addYears(-1);
    public static final Date DATE_FUTURE = DATE_TODAY.addYears(1);

    private static integer ratePlanId = 154657;
    private static integer ratePlanChargeId = 125481;
    public static integer NumCreated = 0;

    public static Account makeAccount() {
        Account testAcc = buildAccount();
        return testAcc;
    }

    public static Account makeAccount(String country, String cName, String nState, String zipcode) {
        ADDR_COUNTRY = country;
        ADDR_State = nState;
        ADDR_ZIP = zipcode;
        Account testAcc = buildAccount();
        testAcc.name = cName;
        ADDR_COUNTRY     = 'United States';
        ADDR_STATE       = 'California';
        ADDR_ZIP         = '94404';


        return testAcc;
    }

    public static Account makePartnerAccount(String country, String cName, String nState, String zipcode) {
        ADDR_COUNTRY = country;
        ADDR_State = nState;
        ADDR_ZIP = zipcode;
        Account testAcc = buildAccount();
        testAcc.name = cName;
        ADDR_COUNTRY     = 'United States';
        ADDR_STATE       = 'California';
        ADDR_ZIP         = '94404';
        testAcc.Partner_Margin__c = 12;
		testAcc.Navision_Status__c = 'Partner';
		testAcc.Territory_Country__c = 'United States';
        testAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();

        return testAcc;
    }

    public static Partner_Category_Status__c createSpecificPCS(Id partnerAccountId, String programVersion, String partnerLevel, String partnerCategory) {
        Partner_Category_Status__c pcs = new Partner_Category_Status__c(
        Partner_Account__c = partnerAccountId,
        Program_Version__c = programVersion,
        Partner_Level__c = partnerLevel,
        Partner_Category__c = partnerCategory,
        Partner_Level_Status__c = 'Active');

        return pcs;
    }

    public static Account buildAccount() {
        NumCreated++;
        Account testAcc = new Account();
        testAcc.Name = 'my test account'  + '_' + System.now().millisecond() + '_' + NumCreated;
        testAcc.BillingStreet = ADDR_STREET;
        testAcc.BillingCity = ADDR_CITY;
        testAcc.BillingState = ADDR_STATE;
        testAcc.BillingCountry = ADDR_COUNTRY;
        testAcc.BillingPostalCode = ADDR_ZIP;
        testAcc.Territory_Country__c =  ADDR_COUNTRY;

        /*add Nov 5th*/
        Subsidiary__c newSub = createSubsidiary();
        //QlikTech_Company__c newQlikCompany = createQlickTechCompany(String.valueOf(newSub.id));
        //testAcc.QlikTech_Company__c = newQlikCompany.QlikTech_Company_Name__c;
        QlikTech_Company__c qtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        upsert qtComp;
        testAcc.QlikTech_Company__c = qtComp.name;
        testAcc.Billing_Country_Code__c  = qtComp.id;
        /*End added Nov 5th*/

        //Comment out based on request
       // testAcc.SFDC_1_Acct_ID__c = 'testid' + generateRandomString(7);
        return testAcc;
    }
     public static Account buildAccount2() {
        NumCreated++;
        Account testAcc = new Account();
        testAcc.Name = 'Secondmy test account' + '_' + System.now().millisecond() + '_' + NumCreated;
        testAcc.BillingStreet = ADDR_STREET;
        testAcc.BillingCity = ADDR_CITY;
        testAcc.BillingState = ADDR_STATE;
        testAcc.BillingCountry = ADDR_COUNTRY;
        testAcc.BillingPostalCode = ADDR_ZIP;
        testAcc.Territory_Country__c =  ADDR_COUNTRY;

        /*add Nov 5th*/
        Subsidiary__c newSub = createSubsidiary();
       // QlikTech_Company__c newQlikCompany = createQlickTechCompany(String.valueOf(newSub.id));
         QlikTech_Company__c qtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
         upsert qtComp;
        testAcc.QlikTech_Company__c = qtComp.name;
        testAcc.Billing_Country_Code__c  =qtComp.id;
        /*End added Nov 5th*/

        //Comment out based on request
       // testAcc.SFDC_1_Acct_ID__c = 'testid' + generateRandomString(7);
        return testAcc;
    }

    public static Contact makeContact(Account testAcc) {
        NumCreated++;
        Contact testContact = new Contact();
        testContact.FirstName = 'DTNA' + '_' + System.now().millisecond() + '_' + NumCreated;
        testContact.LastName = 'Support' + '_' + System.now().millisecond() + '_' + NumCreated;
        testContact.Email = 'email'+ '_' + System.now().millisecond() + '_' + NumCreated + '_' + '@mail' + System.now().millisecond() + NumCreated + '.me';
        testContact.MailingCountry = ADDR_COUNTRY;
        //testContact.MailingCountryCode = 'US';
        testContact.MailingState = ADDR_STATE;
        //testContact.MailingStateCode = 'CA';
        testContact.MailingPostalCode = ADDR_ZIP;
        testContact.MailingStreet = ADDR_STREET;
        testContact.MailingCity = ADDR_CITY;
        testContact.Phone = '404-444-4444';
        testContact.AccountId = testAcc.Id;
        //Comment out based on request
        //testContact.SFDC_1_Contact_ID__c = 'testid' + generateRandomString(7);
        return testContact;
    }


     public static Contact makeContact2(Account testAcc) {
        Contact testContact = new Contact();
        testContact.FirstName = 'DTNA';
        testContact.LastName = 'Support';
        testContact.Email = 'email@mail.me';
        testContact.MailingCountry = 'France';
        //testContact.MailingCountryCode = 'US';
        testContact.MailingState = ADDR_STATE;
        //testContact.MailingStateCode = 'CA';
        testContact.MailingPostalCode = ADDR_ZIP;
        testContact.MailingStreet = ADDR_STREET;
        testContact.MailingCity = ADDR_CITY;
        testContact.Phone = '404-444-4444';
        testContact.AccountId = testAcc.Id;
        testContact.OtherCountry = ADDR_COUNTRY;
        testContact.OtherState = ADDR_STATE;
        testContact.OtherStreet = ADDR_STREET;


        //Comment out based on request
        //testContact.SFDC_1_Contact_ID__c = 'testid' + generateRandomString(7);
        return testContact;
    }

    public static Contact makeContact(Account testAcc, String fname, String lname, String uemail,String country, String nState, String zipcode) {
        Contact testContact = new Contact();
        testContact.FirstName = fname;
        testContact.LastName = lname;
        testContact.Email = uemail;
        testContact.MailingCountry = country;
        //testContact.MailingCountryCode = 'US';
        testContact.MailingState = nState;
        //testContact.MailingStateCode = 'CA';
        testContact.MailingPostalCode = zipcode;
        testContact.MailingStreet = ADDR_STREET;
        testContact.MailingCity = ADDR_CITY;
        testContact.Phone = '404-444-4444';
        testContact.AccountId = testAcc.Id;
        //Comment out based on request
        //testContact.SFDC_1_Contact_ID__c = 'testid' + generateRandomString(7);
        return testContact;
    }

    public static Opportunity makeOpportunity(Account testAcc, Contact contact) {
       /*Nov 1st comment out Opportunity testOpp = buildOpportunity(testAcc, contact);*/
        Opportunity testOpp = buildOpportunity(testAcc);
        return testOpp;
    }

    public static Opportunity makeOpportunity(Account testAcc, Contact contact, String oppName) {
        /*Nov 1st comment out Opportunity testOpp = buildOpportunity(testAcc, contact);*/
        Opportunity testOpp = buildOpportunity(testAcc);
        testOpp.name= oppName;
        return testOpp;
    }


    /**
    Nov 5th:added the cmakeOpportunity2
    * Create a test Opportunity   opp.Revenue_Type__c = 'Reseller';
    **/

      public static Opportunity makeOpportunity2(Account testAcc, Contact contact, String oppName) {
        /*Nov 1st comment out Opportunity testOpp = buildOpportunity(testAcc, contact);*/
        Opportunity testOpp = buildOpportunity2(testAcc);
        testOpp.name= oppName;
        return testOpp;
    }
    /*Nov 1st: comment out in order to mathch the opportunity trigger
    public static Opportunity buildOpportunity(Account testAcc, Contact contact) {
        Opportunity testOpp = new Opportunity();
        testOpp.Name = 'my test opportunity';
        testOpp.StageName = 'Hypothesize';
        testOpp.ForecastCategoryName = 'Omitted';
        testOpp.CloseDate = System.today();
        testOpp.AccountId = testAcc.Id;
        //Comment out based on request
        //testOpp.SFDC_1_Opp_ID__c = 'testid' + generateRandomString(7);
       //Comment out based on request
       // testOpp.Sales_Person__c = 'testPerson';

        return testOpp;
    }*/


    /**
    Nov 1st:added the createTestOpportunity
    * Create a test Opportunity
    **/
    public static Opportunity buildOpportunity(Account acc)
    {
        Opportunity opp = new Opportunity();
        opp.ForecastCategoryName = 'Omitted';
        opp.CloseDate = System.today().addMonths(1);
        //opp.StageName = 'Closed Won';
        opp.StageName = 'Goal Identified';
        opp.Type = 'New Business';
        opp.Name = 'Test Opportunity';

        if (acc != null) {
            opp.AccountId = acc.Id;
        }


        return opp;
    }



    /**
    Nov 5th:added the createTestOpportunity
    * Create a test Opportunity   opp.Revenue_Type__c = 'Reseller';
    **/
    public static Opportunity buildOpportunity2(Account acc)
    {
        Opportunity opp = new Opportunity();
        opp.ForecastCategoryName = 'Omitted';
        opp.CloseDate = System.today().addMonths(1);
        //opp.StageName = 'Closed Won';
        opp.StageName = 'Goal Identified';
        opp.Type = 'New Business';
        opp.Name = 'Test Opportunity';


        if (acc != null) {
            opp.AccountId = acc.Id;
        }


        return opp;
    }



    /*Nov 5th:
    Copied from QuoteTestHelper
    */
    public static QlikTech_Company__c createQlickTechCompany(String subsId) {
        QlikTech_Company__c qTComp = new QlikTech_Company__c(
        Name = 'GBR',
        Country_Name__c = 'France',
        CurrencyIsoCode = 'GBP',
        Language__c = 'English',
        Subsidiary__c = subsId,
        QlikTech_Company_Name__c = 'QlikTech UK Ltd'
        );
        return qTComp;
    }

   /*Nov 5th:
    Copied from QuoteTestHelper
    */
     public static Subsidiary__c createSubsidiary() {
        Subsidiary__c subs = new Subsidiary__c(
            Legal_Entity_Name__c = 'test',
            Netsuite_Id__c = 'test',
            Legal_Country__c = 'France',
            Legal_Address__c = '1020 Esidale Road'

        );
    return subs;
    }


    /*add QlikTech_company*/
    public static Zuora__CustomerAccount__c createBillingAccount(Account acc, String zId) {

        Zuora__CustomerAccount__c billAcc = new Zuora__CustomerAccount__c();
        billAcc.Name = 'Test Billing Account';
        billAcc.Zuora__Account__c = acc.Id;
        billAcc.Zuora__AccountNumber__c = 'A-001';
        billAcc.Zuora__AutoPay__c = false;
        billAcc.Zuora__Batch__c = 'Batch1';
        billAcc.Zuora__BillCycleDay__c = '1';
        billAcc.Zuora__Balance__c = 0;
        billAcc.Zuora__BillToCity__c = ADDR_CITY;
        billAcc.Zuora__BillToCountry__c = ADDR_COUNTRY;
        billAcc.Zuora__BillToAddress1__c = ADDR_STREET;
        billAcc.Zuora__BillToState__c = ADDR_STATE;
        billAcc.Zuora__BillToPostalCode__c = ADDR_ZIP;
        billAcc.Zuora__BillToId__c = ZCON_ID;
        billAcc.Zuora__BillToName__c = 'Test Name';
        billAcc.Zuora__BillToWorkEmail__c = 'test@test.com';
        billAcc.Zuora__MRR__c = 100;
        billAcc.Zuora__Credit_Balance__c = 100;
        billAcc.Zuora__Currency__c = 'USD';
        billAcc.Zuora__External_Id__c = zId;
        billAcc.Zuora__PaymentTerm__c = 'Due Upon Receipt';
        billAcc.Zuora__Status__c = 'Active';
        billAcc.Zuora__Zuora_Id__c = zId;


        return billAcc;
    }
    
    public static Zuora__Subscription__c makeSubscription(Zuora__CustomerAccount__c bill, Account acc, String zId){
        Zuora__Subscription__c sub = new Zuora__Subscription__c();
        sub.Zuora__Account__c = acc.Id;
        sub.Zuora__CustomerAccount__c = bill.Id;
        sub.Name = 'Test Sub';
        sub.Zuora__External_Id__c = zId;
        sub.Zuora__Zuora_Id__c = zId;
        return sub;
    }
    
    public static Zuora__SubscriptionProductCharge__c makeSPC(Zuora__Subscription__c sub, Account acc, String zId){
        Zuora__SubscriptionProductCharge__c spc = new Zuora__SubscriptionProductCharge__c();
        spc.Zuora__Account__c = acc.Id;
        spc.Zuora__External_Id__c = zId;
        spc.Zuora__Zuora_Id__c = zId;
        spc.Name = 'Test SPC';
        return spc;
    }

    public static zqu__Quote__c makeQuote(Opportunity testOpp, Account acct) {

        Zuora__CustomerAccount__c ca = createBillingAccount(acct, 'id0123' + String.valueOf((Math.random()*999999).intValue()) + 'KI');
        insert ca;
        
        zqu__Quote__c testQuote = new zqu__Quote__c();
        testQuote.Name = 'my test quote';
        testQuote.zqu__Opportunity__c = testOpp.Id;
        testQuote.zqu__Account__c = acct.id;
        testQuote.zqu__Currency__c = 'USD';
        testQuote.zqu__Status__c = 'New';
        testQuote.zqu__SubscriptionTermStartDate__c = Date.today();
        testQuote.zqu__startdate__c =  Date.today();
        testQuote.zqu__InitialTerm__c = 12;
        testQuote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);
        testQuote.zqu__SubscriptionType__c ='New Subscription';
        testQuote.zqu__RenewalTerm__c = 12;
        testQuote.zqu__Primary__c = true;
        testQuote.zqu__InvoiceOwnerId__c = ca.Zuora__External_Id__c;
        testQuote.zqu__InvoiceOwnerName__c = ca.Name;

        /*Nov 5th: added Quote_Status__c*/
        testQuote.Quote_Status__c = 'Accepted by Customer';

        return testQuote;
    }
    public static zqu__Quote__c makeQuote2(Opportunity testOpp, Account acct) {

        Zuora__CustomerAccount__c ca = createBillingAccount(acct, 'id0123' + String.valueOf((Math.random()*999999).intValue()) + 'KI');
        insert ca;

        zqu__Quote__c testQuote = new zqu__Quote__c();
        testQuote.Name = 'my NEW test quote';
        testQuote.Billing_Frequency__c ='Annual';
        testQuote.zqu__InvoiceOwnerId__c = ca.Zuora__External_Id__c;
        testQuote.zqu__InvoiceOwnerName__c = ca.Name;
       
        testQuote.zqu__Opportunity__c = testOpp.Id;
        testQuote.zqu__Account__c = acct.id;
        testQuote.zqu__Currency__c = 'USD';
        testQuote.zqu__Status__c = 'New';
        testQuote.zqu__SubscriptionTermStartDate__c = Date.today();
        testQuote.zqu__startdate__c =  Date.today();
        testQuote.zqu__InitialTerm__c = 12;
        testQuote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);
        testQuote.zqu__SubscriptionType__c ='New Subscription';
        testQuote.zqu__RenewalTerm__c = 12;
        testQuote.zqu__Primary__c = true;
        testQuote.zqu__Previewed_Discount__c=4;
        testQuote.zqu__Previewed_Total__c =2;

        /*Nov 5th: added Quote_Status__c*/
        testQuote.Quote_Status__c = 'Accepted by Customer';

        return testQuote;
    }

    public static zqu__QuoteRatePlan__c makeQRP(zqu__Quote__c quote, zqu__QuoteAmendment__c qa, zqu__ProductRatePlan__c prp){
        zqu__QuoteRatePlan__c qrp = new zqu__QuoteRatePlan__c();
        qrp.Name = 'Test Quote Rate Plan';
        qrp.zqu__QuoteAmendment__c = qa.Id;
        qrp.zqu__Quote__c = quote.Id;
        qrp.zqu__ProductRatePlan__c = prp.Id;
        qrp.zqu__ProductRatePlan__c = prp.id;
        return qrp;
    }
    
    public static zqu__QuoteRatePlanCharge__c makeQRPC(zqu__Quote__c quote, zqu__QuoteRatePlan__c qrp, zqu__ProductRatePlanCharge__c prpc){
        return makeQRPC(quote, qrp, prpc, 'Test Quote Rate Plan Charge');
    }
    
    public static zqu__QuoteRatePlanCharge__c makeQRPC(zqu__Quote__c quote, zqu__QuoteRatePlan__c qrp, zqu__ProductRatePlanCharge__c prpc, String name){
        zqu__QuoteRatePlanCharge__c qrpc = new zqu__QuoteRatePlanCharge__c();
        qrpc.Name = name;
        qrpc.zqu__ProductRatePlanCharge__c = prpc.Id;
        qrpc.zqu__QuoteRatePlan__c = qrp.id;
        qrpc.zqu__DeltaTCV__c = null;
        return qrpc;
    }
    
    public static zqu__QuoteChargeDetail__c makeQCD(zqu__Quote__c quote){
        return makeQCD(quote, 'Test Quote Charge Detail');
    }
    
	public static zqu__QuoteChargeDetail__c makeQCD(zqu__Quote__c quote, String name){
        zqu__QuoteChargeDetail__c qcd = new zqu__QuoteChargeDetail__c();
        qcd.Name = name;
        qcd.zqu__Quote__c = quote.id;
        qcd.zqu__DeltaTCV__c = 50;
        return qcd;
    }
        
    public static zqu__QuoteChargeSummary__c makeQCS(zqu__Quote__c quote, zqu__QuoteRatePlan__c qrp, zqu__ProductRatePlanCharge__c prpc){
        return makeQCS(quote, qrp, prpc, 'Test Quote Charge Summary');
    }
    
	public static zqu__QuoteChargeSummary__c makeQCS(zqu__Quote__c quote, zqu__QuoteRatePlan__c qrp, zqu__ProductRatePlanCharge__c prpc, String name){
        zqu__QuoteChargeSummary__c qcs = new zqu__QuoteChargeSummary__c();
        qcs.Name = name;
        qcs.zqu__ProductRatePlanCharge__c = prpc.Id;
        qcs.zqu__QuoteRatePlan__c = qrp.id;
        qcs.zqu__DeltaTCV__c = null;
        return qcs;
    }
    
	public static zqu__QuoteAmendment__c makeQuoteAmendment(zqu__Quote__c quote){
        zqu__QuoteAmendment__c qa = new zqu__QuoteAmendment__c();
        qa.Name = 'Test Quote Amendment';
        qa.zqu__Quote__c = quote.Id;
        qa.zqu__Type__c = 'NewProduct';
        return qa;
    }

    public static Product2 makeProduct(){
        Product2 testProduct = new Product2();
        testProduct.Name = 'testing'+generateRandomString(3);
        testProduct.zqu__SKU__c = 'testSKU';
        testProduct.zqu__ZuoraId__c = 'ZuoraProductID'+generateRandomString(3);
        testProduct.zqu__EffectiveStartDate__c = date.newinstance(2000, 1, 1);
        testProduct.zqu__EffectiveEndDate__c = date.newinstance(2050, 1, 1);
        testProduct.zqu__Deleted__c = false;

        /*Nov 1st Deferred_Revenue_Account__c dded*/
        testProduct.Deferred_Revenue_Account__c ='26000 Deferred Revenue';
        testProduct.Income_Account__c= '26000 Deferred Revenue';

        return testProduct;
    }

    public static Product2 makeProduct(String prodName){
        Integer numZIDCharsneeded = 32-prodName.length();
        if (numZIDCharsneeded < 0){
            numZIDCharsneeded = 0;
        }

        Product2 testProduct = new Product2();
        testProduct.Name = prodName;
        testProduct.zqu__SKU__c = 'testSKU' + prodName;
        testProduct.zqu__ZuoraId__c = generateRandomString(numZIDCharsneeded);
        testProduct.zqu__EffectiveStartDate__c = date.newinstance(2000, 1, 1);
        testProduct.zqu__EffectiveEndDate__c = date.newinstance(2050, 1, 1);
        testProduct.zqu__Deleted__c = false;
        insert testProduct;
        return testProduct;
    }

    public static zqu__ProductRatePlan__c makeProductRatePlan(String zproduct, String zuoraId){
        zqu__ProductRatePlan__c testrp = new zqu__ProductRatePlan__c();
        testrp.name = 'test rp';
        testrp.zqu__Product__c = zproduct;
        testrp.zqu__ZuoraId__c = zuoraId;
        testrp.zqu__Deleted__c = false;
        //set custom fields
        //Needed for pricing queries
        testrp.zqu__EffectiveStartDate__c = Date.newInstance(2010, 1, 1);
        testrp.zqu__EffectiveEndDate__c = Date.newInstance(2020, 1, 1);
        return testrp;
    }

    public static zqu__ProductRatePlanCharge__c makeProductRatePlanCharge(String prp, String type, String model, String name){
        zqu__ProductRatePlanCharge__c testprpc = new zqu__ProductRatePlanCharge__c();
        testprpc.zqu__ProductRatePlan__c = prp;
        testprpc.name= name;
        testprpc.zqu__ZuoraId__c = String.valueOf(ratePlanChargeId);
        ratePlanChargeId++;
        testprpc.zqu__UOM__c = 'test UOM';
        testprpc.zqu__Type__c = type;
        testprpc.zqu__RecurringPeriod__c = 'Month';
        testprpc.zqu__PrepaymentPeriods__c = 1;
        testprpc.zqu__Model__c = model;
        testprpc.zqu__MinQuantity__c = 0;
        testprpc.zqu__MaxQuantity__c = 1000;
        testprpc.zqu__ListPrice__c = 5;
        testprpc.zqu__Description__c = 'Test charge';
        testprpc.zqu__DefaultQuantity__c = 5;
        testprpc.zqu__Discount_Apply_Type__c = 0;
        testprpc.zqu__Discount_Level__c = 'RatePlan';
        testprpc.zqu__Upto_How_Many_Periods__c = 0;
        testprpc.zqu__Deleted__c = false;

        insert testprpc;
        return testprpc;
    }

    public static zqu__ProductRatePlanChargeTier__c makeProductRatePlanChargeTier(String chargeId, Integer tierNumber) {
        zqu__ProductRatePlanChargeTier__c tier = new zqu__ProductRatePlanChargeTier__c();
        tier.Name = String.valueOf(tierNumber);
        tier.zqu__Currency__c = 'USD';
        tier.zqu__Currency2__c = 'USD';
        tier.zqu__Deleted__c = false;
        //tier.zqu__EndingUnit__c = 0.0;
        tier.zqu__IsOveragePrice__c = false;
        tier.zqu__Price__c = 10;
        tier.zqu__PriceFormat__c = 'Flat Fee';
        if(tierNumber==1){
            tier.zqu__StartingUnit__c = 0;
        }
        else{
            tier.zqu__StartingUnit__c=tierNumber-.99;
        }
        tier.zqu__EndingUnit__c= tierNumber;
        tier.zqu__Tier__c = tierNumber;
        tier.zqu__ProductRatePlanCharge__c = chargeId;
        tier.zqu__ZuoraId__c = tierNumber+chargeId;
        insert tier;
        return tier;
    }

    public static zqu__QuoteCharge__c makeQuoteCharge(zqu__Quote__c testQuote, zqu__ProductRatePlanCharge__c testPRPC) {

        zqu__QuoteCharge__c testQuoteCharge = new zqu__QuoteCharge__c();

        testQuoteCharge.Name = 'my test quote charge';
        testQuoteCharge.zqu__Quote__c = testQuote.Id;
        testQuoteCharge.zqu__ProductRatePlanCharge__c = testPRPC.Id;

        insert testQuoteCharge;
        return testQuoteCharge;
    }

    public static zqu__QuoteCharge_Tier__c makeQuoteChargeTier(Id chargeId, Integer tierNumber) {
        zqu__QuoteCharge_Tier__c tier = new zqu__QuoteCharge_Tier__c();
        tier.Name = String.valueOf(tierNumber);
        tier.zqu__Currency__c = 'USD';
        tier.zqu__Deleted__c = false;
        tier.zqu__IsOveragePrice__c = false;
        tier.zqu__Price__c = 10;
        tier.zqu__PriceFormat__c = 'Flat Fee';
        if(tierNumber==1){
            tier.zqu__StartingUnit__c = 0;
        }
        else{
            tier.zqu__StartingUnit__c=tierNumber-.99;
        }
        tier.zqu__EndingUnit__c= tierNumber;
        tier.zqu__Tier__c = tierNumber;
        tier.zqu__QuoteRatePlanCharge__c = chargeId;

        tier.zqu__ZuoraId__c = TIER_ID;
        insert tier;
        return tier;
    }

    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr;
    }
}