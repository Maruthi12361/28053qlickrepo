public class ConvertToSubscriptionController {
    Id oppId;
    public ConvertToSubscriptionController(ApexPages.StandardController controller){
        oppId= (Id)controller.getRecord().Id;
    }
    
    public pageReference updateOpp(){
        list<SBQQ__Quote__c> listSCPQuotes = [select Id from SBQQ__Quote__c where SBQQ__Opportunity2__c = :oppId ];
        if(listSCPQuotes.size()>0) Delete listSCPQuotes;
        
        Id SalesZuoraQCCSRecordtypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS Zuora Subscription').getRecordTypeId();   
        //updating opp 
        Opportunity opp = new Opportunity();
        opp.Id = oppId;
        opp.RecordTypeId =SalesZuoraQCCSRecordtypeId;
        opp.License_Forecast_Amount__c =0;
       	Update opp;
        return new pageReference('/'+oppId);
        
    }
}