@isTest
private class ZuoraLicenseEmailService_Test
{
    @testSetup
    static void testSetup() {
        QTTestUtils.GlobalSetUp();

        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Uniqueness level over 9000', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c = FALSE;
        update endUserAccount;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Revenue_Type__c = 'Direct';
        insert testOpp;

        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        insert testQuote;
    }

    @isTest
    static void testCreateTaskFromInboundEmail() {

        zqu__Quote__c theQuote = [SELECT Id, Name, zqu__Number__c FROM zqu__Quote__c LIMIT 1];

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        email.subject = 'Your Qlik License Has Been Created ' + theQuote.zqu__Number__c;
        email.plainTextBody = 'These are your fake license keys: Key1, Key2, Key3';
        email.htmlBody = email.plainTextBody;
		email.toAddresses = new List<String>{'test@test.com'};
        env.fromAddress = 'fakezuoraworkflow@email.test.com';

        Test.startTest();
        ZuoraLicenseEmailService serviceClass = new ZuoraLicenseEmailService();
        serviceClass.handleInboundEmail(email, env);
        Test.stopTest();
    }
}