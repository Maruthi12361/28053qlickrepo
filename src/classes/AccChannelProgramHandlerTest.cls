/****************************************************************
*
*  AccChannelProgramHandlerTest
*
*  2020-04-10 UIN Added Logic for oem. Code coverage 91%. Class covered AccChannelProgramHandler

* test RB - 2
*****************************************************************/
@isTest
public class AccChannelProgramHandlerTest{

static final String AccRecordTypeId_PartnerAccount = '01220000000DOFz';

    @testSetup
    public static void testSetup() {
        Test.startTest();
        QuoteTestHelper.createCustomSettings();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id, 'FRA'), createQlikTech('Afghanistan', 'AF', testSubs.id,'AFG'), createQlikTech('Japan', 'JP', testSubs.id,'JPN'), createQlikTech('Albania', 'AL', testSubs.id,'ALB')};
        insert listOfcreate;
        
        List<ChannelProgram> lChp = new List<ChannelProgram>{createCP('QlikTech UK Ltd'), createCP('QlikTechFrance - EEA'),createCP('QlikTechAfghanistan - EEA'), createCP('QlikTechJapan - EEA'), createCP('QlikTechAlbania - EEA')};
        insert lChp ;
        Test.stopTest();
    }
    
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId, String qtname) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
        Country_Code_Two_Letter__c = countryAbbr,
        Name = qtname,
        Country_Name__c = countryName,
        Subsidiary__c = subsId,
        Market_Name__c= 'EEA',
        QlikTech_Company_Name__c= 'Qliktech' + countryname);
        return qlikTechIns;
    }
    
    private static ChannelProgram createCP(String cpName) {
        ChannelProgram qlikTechIns = new ChannelProgram(
        Category = 'Reseller',
        Name = cpName,
        IsActive = true);
        return qlikTechIns;
    }
    
    @isTest
    public static void PlaceOrderOEM() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {
        
            QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c,Name  From QlikTech_Company__c limit 1];
            
            Account testAccount = QTTestUtils.createMockAccount('Partner Test company with a unique name', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_PartnerAccount;
            testAccount.ECUSTOMS__RPS_Status__c = 'No Matches';
            testAccount.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            testAccount.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            testAccount.navision_Status__c ='Pending Send';
            testaccount.Billing_Country_Code__c = testQtCompany.Id;
            testaccount.isPartner =true;
            update testAccount;

            Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

            Test.startTest();
            
            
            Map<Id, Account> mTestAccount1 = new  Map<Id, Account>([select id, ispartner,recordtypeid,Billing_Country_Code__c, navision_status__c from account where id = :testaccount.id]);
            system.debug('iAMHEREDOG'+mTestAccount1 );
            testaccount.navision_Status__c  = 'Partner';   
            update testAccount;
            Map<Id, Account> mTestAccount2 = new  Map<Id, Account>([select id, ispartner,recordtypeid,Billing_Country_Code__c, navision_status__c from account where id = :testaccount.id]);
            system.debug('iAMHEREDOG2'+mTestAccount2 );
            List<Account> tAccList = new List<Account>{mTestAccount2.values()};
            system.debug('iAMHEREDOG3'+tAccList);
            AccChannelProgramHandler.onAfterUpdate(tAccList, mTestAccount2 , mTestAccount1);
            List<ChannelProgramMember> cp = [SELECT Id, PartnerId FROM ChannelProgramMember where partnerid = :mTestAccount2.keySet()];
            system.assertequals(1, cp.size(), 'Channel program member not created');
            Test.stopTest();
            AccChannelProgramHandler acp = new AccChannelProgramHandler();
        }
    }

}