/*24/02/2020 Shubham Gupta OTQ-44 Batch created to cover the scenario(if partner account is updated with new QDI value, update all open zuora oppties
    using a Batch class)
	14/05/2020 shubham Gupta OTQ-668 Adding subscription renewals record type in the querry
*/
public class QDIOppUpdateBatch implements Database.Batchable<sObject>{
    
    private Map<Id,String> changedAccountMap;
    Set<Id> accountID;
    public QDIOppUpdateBatch(Map<Id,String> inputMap){
        changedAccountMap = inputMap;
        accountID = changedAccountMap.keySet();
    }
    
    
    public Database.QueryLocator start(Database.BatchableContext BC){
         Id zuoraRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS Zuora Subscription').getRecordTypeId();
         Id zuoraRenewalsRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription Renewal').getRecordTypeId();
        String query = 'Select id,QDIPartnerCheck__c,stageName,Sell_Through_Partner__c from Opportunity where Sell_Through_Partner__c In:accountID and (NOT stageName like \'%Closed%\') and (RecordType.Id=:zuoraRecordTypeId or RecordType.Id=:zuoraRenewalsRecordTypeId)';
        return Database.getQueryLocator(query);
   }

   public void execute(Database.BatchableContext BC, List<Opportunity> scope){
       if(!scope.isEmpty()){
            List<Opportunity> opptoupdate = new List<Opportunity>();
            for(Opportunity oppty:scope){
                oppty.QDIPartnerCheck__c = changedAccountMap.get(oppty.Sell_Through_Partner__c);
                opptoupdate.add(oppty);
            }
           Database.SaveResult[] srList = Database.Update(opptoupdate, false);
       }
    }

   public void finish(Database.BatchableContext BC){
   }
}