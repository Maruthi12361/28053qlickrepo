/* 
Purpose: Partner Expertise Main Progress bar test
Created Date: 4 Jan 2016
Initial development MTM 4 Jan 2016
Change log:
 **/
@isTest
public class EP_MainProgressBarControllerTest {

    Static Account account { get; set; }
    //public static List<Solution_Profiles__c> SolutionProfiles = new List<Solution_Profiles__c>();
    Static Partner_Expertise_Area__c ExpertiseArea {get; set;}
    Static Partner_Expertise_Application_Lookup__c ExpertLookup {get; set;} 
    
   // public Map<Id, Partner_Expertise_Area__c> ExpertiseAreaMap{get; set;}
    //public Map<Id, List<Solution_Profiles__c>> MapofExperttoSolutionProfiles{get; set;}
    
    static testmethod void test_getMapofExpertiseArea() 
    {
        Setup();
        Test.startTest();
        EP_MainProgressBarController controller = new EP_MainProgressBarController();
        controller.AccountId = account.Id;
        controller.ExpertiseAreaMap  = controller.getMapofExpertiseArea();
        Partner_Expertise_Area__c peobject = controller.ExpertiseAreaMap.get(ExpertiseArea.Id);
        System.assertNotEquals(null, peobject);
        System.assertEquals(ExpertiseArea.Id,peobject.Id);
        Test.stopTest();
    }
    
    static testmethod void test_getExpertToSolProfMaps() 
    {
        Setup();
        Test.startTest();
        EP_MainProgressBarController controller = new EP_MainProgressBarController();
        controller.AccountId = account.Id;
        controller.ExpertiseAreaMap  = controller.getMapofExpertiseArea();
        controller.MapofExperttoSolutionProfiles = controller.getExpertToSolProfMaps(controller.ExpertiseAreaMap.keySet());
        List<Solution_Profiles__c> listSolutions = controller.MapofExperttoSolutionProfiles.get(ExpertiseArea.Id);
        System.assertEquals(4, listSolutions.size());
        Test.stopTest();
    }
    
    static testmethod void test_SetExpertiseDesignationStatus()
    {
        Setup();
        Test.startTest();
        EP_MainProgressBarController controller = new EP_MainProgressBarController();
        controller.AccountId = account.Id;
        controller.ExpertiseAreaMap  = controller.getMapofExpertiseArea();
        controller.MapofExperttoSolutionProfiles = controller.getExpertToSolProfMaps(controller.ExpertiseAreaMap.keySet());
        controller.SetExpertiseDesignationStatus(ExpertiseArea.Id, 'Under Review');
        ExpertiseArea = [Select
                         Id,
                         Expertise_Designation_Status__c,
                         Expertise_Area__c,
                         Dedicated_Expertise_Contact__c,
                         Dedicated_Expertise_Contact__r.Name,
                         Expertise_Application_Eligibility__c
                         from Partner_Expertise_Area__c 
                         where Partner_Account_Name__c =: account.Id
                         AND Id =: ExpertiseArea.Id];
        System.assertEquals('Under Review', ExpertiseArea.Expertise_Designation_Status__c);
        Test.stopTest();
    }
    
    static testmethod void test_getApplicationLookups() 
    {
        Setup();
        Test.startTest();
        EP_MainProgressBarController controller = new EP_MainProgressBarController();
        controller.AccountId = account.Id;
        List<Partner_Expertise_Application_Lookup__c> applookup  = controller.getApplicationLookups();        
        System.assertEquals(1, applookup.size());
        Test.stopTest();
    }
    static testmethod void testEP_MainProgressBarController() 
    {
        Setup();
        Test.startTest();
        EP_MainProgressBarController controller = new EP_MainProgressBarController();
        controller.AccountId = account.Id;
        controller.ExpertiseAreaMap  = controller.getMapofExpertiseArea();
        controller.MapofExperttoSolutionProfiles = controller.getExpertToSolProfMaps(controller.ExpertiseAreaMap.keySet());
        controller.expert = ExpertiseArea.Id;
        controller.SubmitforReview();
        ExpertiseArea = [Select
                         Id,
                         Expertise_Designation_Status__c,
                         Expertise_Area__c,
                         Dedicated_Expertise_Contact__c,
                         Dedicated_Expertise_Contact__r.Name,
                         Expertise_Application_Eligibility__c
                         from Partner_Expertise_Area__c 
                         where Partner_Account_Name__c =: account.Id
                         AND Id =: ExpertiseArea.Id];
        System.assertEquals('Under Review', ExpertiseArea.Expertise_Designation_Status__c);
        Test.stopTest();
    }
    
    public Static void Setup()
    {    
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        
        //Create Account
        account = TestDataFactory.createAccount('Test AccountName', qtc);
        insert account;
         
		//Create Contacts
		Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', account.Id);
        insert testContact;
        
        //Create ExpertiseArea
        ExpertiseArea = EP_TestDataSetup.CreatExpertiseArea(account.Id, 'Communcation', testContact);        
        //Create ExpertiseArea lookup object
        ExpertLookup = EP_TestDataSetup.CreatExpertiseLookup(account.Id, ExpertiseArea.Id);        
        
        Solution_Profiles__c sol1 = EP_TestDataSetup.CreateSolutionProfile('Solution1', account.Id, ExpertiseArea.Id);
        Solution_Profiles__c sol2 = EP_TestDataSetup.CreateSolutionProfile('Solution2', account.Id, ExpertiseArea.Id);
        Solution_Profiles__c sol3 = EP_TestDataSetup.CreateSolutionProfile('Solution3', account.Id, ExpertiseArea.Id);
        Solution_Profiles__c sol4 = EP_TestDataSetup.CreateSolutionProfile('Solution4', account.Id, ExpertiseArea.Id);
        
        //Assign Solution_Profiles to lookup object        
        ExpertLookup.Solution_Profile_1_Name__c = sol1.Id;
        ExpertLookup.Solution_Profile_2_Name__c = sol2.Id;
        ExpertLookup.Solution_Profile_3_Name__c = sol3.Id;
        ExpertLookup.Solution_Profile_4_Name__c = sol4.Id;                
    }        
}