/**     
 * File Name : Z_RenewalQuotePostJob
 * Description : This is a schedulable job that closes renewal quotes by updating the Quote_Status__c field as follows:
 *               - 'Accepted by Customer' : is a partner quote
 *               - 'Order Placed' : is not a partner quote
 * @author : Inki Hong
 * Modification Log ======================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

global class Z_RenewalQuotePostJob implements Schedulable {

    global void execute(SchedulableContext ctx) {
        run();
    }

    global void run() {
        Renewal_Automation_Settings__c settings = Renewal_Automation_Settings__c.getOrgDefaults();

        if (Test.isRunningTest()) {
            Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;
        }

        Database.SaveResult[] updateRenewalQuotesResults;
        
        Integer badQuoteCounter = 0, 
                recordsLimit = Integer.valueOf(settings.QuoteClosureRecordsLimit__c);

        List<zqu__Quote__c> renewalQuotes = new List<zqu__Quote__c>();
        Map<Id, String> quoteToOpptyMap = new Map<Id, String>();

        for (List<zqu__Quote__c> batch : [SELECT zqu__Opportunity__c, zqu__Opportunity__r.Revenue_Type__c, Id, Quote_Status__c 
                                          FROM zqu__Quote__c 
                                          WHERE zqu__StartDate__c = :Date.today() 
                                          AND zqu__Opportunity__r.Is_Manual_Process__c = FALSE 
                                          AND zqu__SubscriptionType__c = 'Renew Subscription'
                                          AND zqu__Opportunity__r.RecordType.Name = 'Subscription Renewal'
                                          AND zqu__Opportunity__r.StageName != 'Closed Lost'
                                          AND zqu__Status__c = 'New'
                                          AND zqu__Primary__c = TRUE
                                          LIMIT :recordsLimit]) {

            for (zqu__Quote__c q : batch) {
                quoteToOpptyMap.put(q.Id, q.zqu__Opportunity__c);

                if (q.zqu__Opportunity__r.Revenue_Type__c.equals('Direct')) {
                    renewalQuotes.add(new zqu__Quote__c(Id=q.Id, Quote_Status__c='Accepted by Customer'));
                } else {
                    renewalQuotes.add(new zqu__Quote__c(Id=q.Id, Quote_Status__c='Order Placed'));
                }
            }
        }

        if (Test.isRunningTest()) {
            badQuoteCounter++;
        }

        updateRenewalQuotesResults = Database.update(renewalQuotes, false);

        String htmlMessage = '<table border="1"><tr><td>Opportunity ID</td><td>Quote ID</td><td>Affected Fields</td><td>Status Code</td><td>Error Message</td></tr>';

        for (Integer i = 0; i < renewalQuotes.size(); i++) {
            zqu__Quote__c renewalQuote = renewalQuotes.get(i);
            Database.SaveResult result = updateRenewalQuotesResults[i];
            
            if (!result.isSuccess()) {
                badQuoteCounter++;
                String badQuoteId = renewalQuote.Id,
                       badOpportunityId = quoteToOpptyMap.get(badQuoteId),
                       affectedFields, statusCode, errorMessage;
                
                for (Database.Error error : result.getErrors()) {
                    affectedFields = String.join(error.getFields(), ',');
                    statusCode = error.getStatusCode().name();
                    errorMessage = error.getMessage();
                }

                htmlMessage += '<tr><td>' + badOpportunityId + '</td><td>' + badQuoteId + '</td><td>' + affectedFields + '</td><td>' + statusCode + '</td><td>' + errorMessage + '</td></tr>';
            }
        }

        if (Test.isRunningTest()) {
            badQuoteCounter++;
        }

        if (badQuoteCounter > 0) {
            List<Id> distroListUsers = new List<Id>();
            List<String> distroList = new List<String>();

            String orgId = UserInfo.getOrganizationId().substring(0,15);

            for (GroupMember groupMember : [SELECT Id, UserOrGroupId 
                                            FROM GroupMember 
                                            WHERE Group.Name = 'Renewal Automation Group']) {
                
                distroListUsers.add(groupMember.UserOrGroupId);
            }

            for (User user : [SELECT Id, Email 
                              FROM User 
                              WHERE Id IN :distroListUsers]) {
                
                distroList.add(user.Email);
            }

            htmlMessage += '</table>';
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(distroListUsers);
            mail.setSenderDisplayName('Z_RenewalOpportunityJob');
            mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Quote Closure Job Failure Notification');
            mail.setHtmlBody(htmlMessage);
            
            if (!Test.isRunningTest()) { Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); }
        }
    }
}