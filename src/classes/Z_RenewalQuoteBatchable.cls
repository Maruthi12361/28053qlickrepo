/**     
 * File Name : Z_RenewalQuoteBatchable
 * Description : This is a batchable job to create renewal quotes that is triggered by Z_RenewalQuoteInitJob schedulable class.  
 *               It calls the following methods in this order:
 *               1. start - returns QueryLocator to pass a list of previous quotes
 *               2. execute - sends an email in case some renewal quotes insertion failed
 *               2a. createRenewalQuotes - creates the renewal quotes using the previous quotes from step 1
 *               3. finish - terminates the job, and sends email notification in case of system exception
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 * 2       05.30.2020   Inki Hong      Adding zqu__Is_Charge_Expired__c field to creating a renewal quote (RTR-604)
 */

global with sharing class Z_RenewalQuoteBatchable implements Database.Batchable<SObject>, Database.Stateful {
    
    private Renewal_Automation_Settings__c settings;

    private final String CLASS_NAME = 'Z_RenewalQuoteInitJob',
                         DEBUG_PREFIX = '> $ ' + this.CLASS_NAME;

    private final Id RENEWAL_QUOTE_RECORD_TYPE_ID = Schema.SObjectType.zqu__Quote__c
                                                    .getRecordTypeInfosByName()
                                                    .get('Renewal')
                                                    .getRecordTypeId();

    private Map<String, Map<String, Object>> subscriptionFieldsMap;
    private Map<String, String> renewalOpportunityNamesMap;
    private List<String> previousOpptyIds;
    private List<Id> successRenewalQuotes;

    public List<Database.SaveResult> insertRenewalQuotesResult;

    global Z_RenewalQuoteBatchable(Map<String, Map<String, Object>> subscriptionFieldsMap, Map<String, String> renewalOpportunityNamesMap, List<String> previousOpptyIds) {
        this.subscriptionFieldsMap = subscriptionFieldsMap;
        this.renewalOpportunityNamesMap = renewalOpportunityNamesMap;
        this.previousOpptyIds = previousOpptyIds;

        if (Test.isRunningTest()) {
            Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;
        }

        this.insertRenewalQuotesResult = new List<Database.SaveResult>();
        this.successRenewalQuotes = new List<Id>();

        this.settings = Renewal_Automation_Settings__c.getOrgDefaults();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [start start] number of records in this.insertRenewalQuotesResult list: ' + this.insertRenewalQuotesResult.size());
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [start start] number of records in this.successRenewalQuotes list: ' + this.successRenewalQuotes.size());

        DescribeSObjectResult quoteDescResult = zqu__Quote__c.getSObjectType().getDescribe();
        List<String> quoteFieldApiNames = Z_RenewalAutomationUtility.getFieldApiNames(quoteDescResult, 'Renewal_Quote_Query_Fields');
        Set<String> subscriptionNumbers = this.subscriptionFieldsMap.keySet();
        Integer recordLimit = this.settings.QuoteCreationRecordsLimit__c  == null ? 0 : Integer.valueOf(this.settings.QuoteCreationRecordsLimit__c );

        String qString = 'SELECT ' + String.join(quoteFieldApiNames, ',') + ' '
                       + 'FROM zqu__Quote__c '
                       + 'WHERE zqu__Opportunity__c IN :previousOpptyIds '
                       + 'AND zqu__Subscription_Name__c IN :subscriptionNumbers '
                       + 'AND zqu__Primary__c = TRUE '
                       + 'AND zqu__Status__c = \'Sent to Z-Billing\' '
                       + 'AND Quote_Status__c IN (\'Accepted by Customer\', \'Order Placed\') '
                       + 'ORDER BY zqu__SubscriptionTermEndDate__c ASC ';
        
        if (recordLimit > 0) {
            qString += 'LIMIT ' + recordLimit;
        }

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' base oppty query string = ' + qString);

        return Database.getQueryLocator(qString);
    }

    global void execute(Database.BatchableContext bc, List<zqu__Quote__c> previousQuotes) {
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [execute start] - number of records in this.insertRenewalQuotesResult list: ' + this.insertRenewalQuotesResult.size());
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [execute start] - number of records in this.successRenewalQuotes list: ' + this.successRenewalQuotes.size());

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' ' + previousQuotes);
        
        List<zqu__Quote__c> renewalQuotes = createRenewalQuotes(previousQuotes);

        Integer badQuoteCounter = 0;
        String htmlMessage = '<table border="1"><tr><td>Quote ID</td><td>Opportunity ID</td><td>Affected Fields</td><td>Status Code</td><td>Error Message</td></tr>';
        
        for (Integer i = 0; i < renewalQuotes.size(); i++) {
            Database.SaveResult result = this.insertRenewalQuotesResult.get(i);
            
            if (result.isSuccess()) {
                System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' renewal quote id = ' + result.getId());
                this.successRenewalQuotes.add(result.getId());
            } else {
                badQuoteCounter++;
                zqu__Quote__c previousQuote = previousQuotes.get(i);
                String badQuoteId = previousQuote.Id,
                       badOpportunityId = previousQuote.zqu__Opportunity__r.Next_Renewal_Opportunity__c,
                       affectedFields, statusCode, errorMessage;

                for (Database.Error error : result.getErrors()) {
                    affectedFields = String.join(error.getFields(), ',');
                    statusCode = error.getStatusCode().name();
                    errorMessage = error.getMessage();
                }
                
                htmlMessage += '<tr><td>' + badQuoteId + '</td><td>' + badOpportunityId + '</td><td>' + affectedFields + '</td><td>' + statusCode + '</td><td>' + errorMessage + '</td></tr>';
            }
        }

        if (Test.isRunningTest()) {
            badQuoteCounter++;
        }

        if (badQuoteCounter > 0) {
            List<Id> distroListUsers = new List<Id>();
            List<String> distroList = new List<String>();

            String orgId = UserInfo.getOrganizationId().substring(0,15);

            for (GroupMember groupMember : [SELECT Id, UserOrGroupId 
                                            FROM GroupMember 
                                            WHERE Group.Name = 'Renewal Automation Group']) {
                
                distroListUsers.add(groupMember.UserOrGroupId);
            }

            for (User user : [SELECT Id, Email 
                              FROM User 
                              WHERE Id IN :distroListUsers]) {
                
                distroList.add(user.Email);
            }
            
            htmlMessage += '</table>';
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(distroList);
            mail.setSenderDisplayName('Z_RenewalQuoteInitJob');
            mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Quote Init Job Failure Notification');
            mail.setHtmlBody(htmlMessage);
            
            if (!Test.isRunningTest()) { Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); }
        }

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [execute end] number of records in this.insertRenewalQuotesResult list: ' + this.insertRenewalQuotesResult.size());
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [execute end] number of records in this.successRenewalQuotes list: ' + this.successRenewalQuotes.size());
    }

    global void finish(Database.BatchableContext bc) {
        String extendedStatus = '';

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [finish start] number of records in this.insertRenewalQuotesResult list: ' + this.insertRenewalQuotesResult.size());
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' [finish start] number of records in this.successRenewalQuotes list: ' + this.successRenewalQuotes.size());
        
        System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' inside finish method');

        if (!Test.isRunningTest()) { 
            AsyncApexJob job = [SELECT Id, ExtendedStatus, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()]; 
            extendedStatus = job.ExtendedStatus;
        }

        if (null != extendedStatus) {
            String orgId = UserInfo.getOrganizationId().substring(0,15),
                   errorEmailDL = this.settings.ErrorEmailDL__c,
                   htmlMessage = '<p>Renewal Opportunity Job encountered a system error.  Error message was: <b>' + extendedStatus + '</b></p>';

            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' ' + htmlMessage);

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] { errorEmailDL });
            mail.setSenderDisplayName(this.CLASS_NAME);
            mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Opportunity Job Failure Notification');
            mail.setHtmlBody(htmlMessage);

            if (!Test.isRunningTest()) { Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); }
        } else {
            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' number of successful renewal quotes = ' + this.successRenewalQuotes.size());
            if (!this.successRenewalQuotes.isEmpty()) {
                System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' build and save batchable start');
                Database.executeBatch(new Z_RenewalQuoteBuildAndSaveBatchable(this.successRenewalQuotes), 1);
            }
        }   
    }

    public List<zqu__Quote__c> createRenewalQuotes(List<zqu__Quote__c> previousQuotes) {
        List<zqu__Quote__c> renewalQuotes = new List<zqu__Quote__c>();
        
        for (zqu__Quote__c previousQuote : previousQuotes) {
            Map<String, Object> subscriptionFields = this.subscriptionFieldsMap.get(previousQuote.zqu__Subscription_Name__c);
            renewalQuotes.add(new zqu__Quote__c(RecordTypeId = this.RENEWAL_QUOTE_RECORD_TYPE_ID,

                                                zqu__Amendment_Name__c = this.renewalOpportunityNamesMap.get(previousQuote.zqu__Opportunity__r.Id),
                                                zqu__Account__c = previousQuote.zqu__Account__c,
                                                zqu__AutoRenew__c = true,
                                                zqu__BillingEntity__c = previousQuote.zqu__BillingEntity__c,
                                                zqu__BillToContact__c = previousQuote.zqu__BillToContact__c,
                                                zqu__Currency__c = previousQuote.zqu__Currency__c,
                                                zqu__ExistSubscriptionID__c = subscriptionFields.get('Zuora__External_Id__c').toString(),
                                                zqu__Hidden_Subscription_Name__c = previousQuote.zqu__Hidden_Subscription_Name__c,
                                                zqu__InitialTerm__c = (Decimal) subscriptionFields.get('Zuora__InitialTerm__c'),
                                                zqu__InvoiceOwnerId__c = subscriptionFields.get('Zuora__InvoiceOwner__r.Zuora__Zuora_Id__c').toString(), 
                                                zqu__InvoiceOwnerName__c = subscriptionFields.get('Zuora__InvoiceOwner__r.Name').toString(), 
                                                zqu__Is_Charge_Expired__c = true,
                                                zqu__Opportunity__c = previousQuote.zqu__Opportunity__r.Next_Renewal_Opportunity__c,
                                                zqu__PaymentTerm__c = previousQuote.zqu__PaymentTerm__c,
                                                zqu__Primary__c = true,
                                                zqu__QuoteBusinessType__c = 'Renewal',
                                                zqu__RenewalTerm__c = previousQuote.zqu__RenewalTerm__c,
                                                zqu__SoldToContact__c = previousQuote.zqu__SoldToContact__c,
                                                zqu__StartDate__c = (Date) subscriptionFields.get('Zuora__SubscriptionEndDate__c'),
                                                zqu__Status__c = 'New',
                                                zqu__SubscriptionTermEndDate__c = (Date) subscriptionFields.get('Zuora__SubscriptionEndDate__c'),
                                                zqu__SubscriptionTermStartDate__c = (Date) subscriptionFields.get('Zuora__SubscriptionStartDate__c'),
                                                zqu__SubscriptionType__c = 'Renew Subscription',
                                                zqu__Subscription_Term_Type__c = subscriptionFields.get('Zuora__TermSettingType__c').toString(),
                                                zqu__SubscriptionVersion__c = (Decimal) subscriptionFields.get('Zuora__Version__c'),
                                                zqu__ValidUntil__c = (Date) subscriptionFields.get('Zuora__SubscriptionEndDate__c'),
                                                zqu__ZuoraEntityId__c = previousQuote.zqu__ZuoraEntityId__c,
                                                zqu__ZuoraAccountID__c = previousQuote.zqu__ZuoraAccountID__c,
                                                zqu__Zuora_Account_Number__c = subscriptionFields.get('Zuora__CustomerAccount__r.Zuora__AccountNumber__c').toString(),
                                                
                                                Billing_Frequency__c = previousQuote.Billing_Frequency__c,
                                                Quote_Status__c = 'Open'));
        }

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' whats about to be inserted: ' + renewalQuotes);
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' how many in this list: ' + renewalQuotes.size());

        try {
            this.insertRenewalQuotesResult = Database.insert(renewalQuotes, false);
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' exception thrown while inserting renewal quotes');
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + e.getStackTraceString());
        }

        return renewalQuotes;
    }
}