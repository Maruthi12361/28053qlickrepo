/*
 File Name: UpdateCaseSharingBatchTest

 2020-04-23 extcqb IT-2768 Fix #Attunity Case Sharing Rules.
*/
@IsTest
private class UpdateCaseSharingBatchTest {
    @IsTest
    static void testBehavior() {
        //for code coverage only, logic covered in CaseSharingHandlerTest
        Case cs = new Case(
                Product__c = 'Not product related',
                OwnerId = UserInfo.getUserId()
        );
        insert cs;

        Test.startTest();
        UpdateCaseSharingBatch myBatchObject = new UpdateCaseSharingBatch('SELECT Id FROM Case WHERE Id = \'' + cs.Id + '\'');
        Id batchId = Database.executeBatch(myBatchObject);
        Test.stopTest();

        List<CaseShare> caseShares = [SELECT Id FROM CaseShare WHERE RowCause = 'Manual' AND CaseId = :cs.Id];
        System.assertEquals(0, caseShares.size());
    }
}