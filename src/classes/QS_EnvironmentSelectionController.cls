/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public class QS_EnvironmentSelectionController {
    
    public boolean isPartnerAccount { get; set; }
    public boolean isCustomerAccount { get; set; }

    public Id resultsForAccount { get; set; }

    private Id accountId;
    private Account accountDetails;
    
    public Environment__c selectedEnvironment { get; set; }
    public Id selectedEnvironmentId { get; set; }

    public Id caseId { get; set; }
    public String source { get; set; }
    


    public QS_EnvironmentSelectionController(){

        isCustomerAccount = false;
        isPartnerAccount = false;

        system.debug ('~~~~~~~~pageRef.getParameters()' + ApexPages.CurrentPage().getparameters().get('caseid') );
        caseId = ApexPages.CurrentPage().getparameters().get('caseid');
        source = ApexPages.CurrentPage().getparameters().get('source');

        selectedEnvironment = new Environment__c();
        system.debug('Userinfo.getUserid(): ' +Userinfo.getUserid()  );
        accountId = getUsersAccountId( Userinfo.getUserid() );

        system.debug('accountId: '+ accountId);
        setAccount( accountId );
        
        if ( isCustomerAccount ) {
            resultsForAccount = accountId;
        }
        
    }

        
    /** 
    * gets account id of logged in user 
    */    
    private Id getUsersAccountId( Id userId ) {
        return [ Select contact.AccountId from user where id =: userId ].contact.AccountId ;
    }            

    private void setAccount( Id accountId ){
        List<Account> accountDetailsList = [Select Id, IsPartner, IsCustomerPortal from Account where id =: accountid];
        if(accountDetailsList.Size() == 0)
            return;
        accountDetails = accountDetailsList[0];
        if (accountDetails != null && accountDetails.isPartner){
            isPartnerAccount = true;
        }
        if (accountDetails != null && accountDetails.IsCustomerPortal){
            isCustomerAccount = true;
        }
    }
    
    public PageReference setSelectedEnvironment (){
        system.debug ( '~~~~QS_EnvironmentSelectionController~selectedEnvironment~' + selectedEnvironment );
        if ( caseId != null && selectedEnvironment != null ){
            // set the environment on the case 
            Case caseToUpdate = new Case ( id = caseId, EnvironmentOfCase__c= selectedEnvironment.id );
            update caseToUpdate;
            
        }
        
        String URLToNavigateTo = '/support/';
        if ( source == 'wizard' ){
            URLToNavigateTo = URLToNavigateTo + 'QS_CaseWizard';
        } else if ( source == 'case' ) {
            URLToNavigateTo = URLToNavigateTo + 'QS_CaseDetails';
        }
        
        if ( caseId != null ){
            URLToNavigateTo = URLToNavigateTo + '?caseId=' + caseId;
        }
                                                   
        PageReference pageRef = new PageReference( URLToNavigateTo );
        return pageRef;
    }
    
    public PageReference CancelEnvironmentLink (){
        PageReference pageRef = new PageReference('/support/QS_CaseDetails');
        pageRef.getParameters().put('caseid', caseId);
        return pageRef;
    }

}