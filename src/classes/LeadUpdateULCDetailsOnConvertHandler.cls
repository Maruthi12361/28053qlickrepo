/******************************************************

	Class: LeadUpdateULCDetailsOnConvertHandler
	Test class: LeadUpdateULCDetailsOnConvertHandlerTest
	Initiator: Project ULCv3 
	
	Changelog:
		2009-12-04	MHG		Created file	
		2010-09-09  MHG		CR #152 ULC Rollup summary field		
							https://emea.salesforce.com/a0C2000000ALlrw
		2016-12-09  UIN		Added logic for updating user to sense group details on lead convert.

		2017-10-25 AYS BMW-402 : Migrated from UpdateULCDetailsOnConvert.trigger.
		2018-19-03 AYS BMS-604 : Added debug rows
		2019-07-03 CCE BMW-1595 : Added debug calls to logging object, added try/catch statements.
		2020-04-01 CCE DGM-124 : Remove debug calls to logging object.
******************************************************/

public class LeadUpdateULCDetailsOnConvertHandler {

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld, Boolean isAfter, Boolean isBefore) {
		System.debug('LeadUpdateULCDetailsOnConvertHandler isAfter = ' + isAfter + ' isBefore = ' + isBefore);

		if (isAfter){
 			System.debug('LeadUpdateULCDetailsOnConvertHandler in isAfter');		
			Set<Id> Leads = new Set<Id>();
			Map<Id, Id> LeadToContact = new Map<Id, Id>();
			List<ULC_Details__c> ULCDetailsToBeUpdated = new List<ULC_Details__c>();
			List<ULC_Login_History__c> ToBeInserted = new List<ULC_Login_History__c>();
			List<Contact> ContactsToBeUpdated = new List<Contact>();
			//User to sense group records to be updated
			List<User_To_Sense_Group__c> userSenseGroupsToBeUpd = new List<User_To_Sense_Group__c>();

			for (integer i = 0; i < triggernew.size(); i++){
				System.debug('LeadUpdateULCDetailsOnConvertHandler loop1');
				System.debug('debug_triggerNew_IsConverted ' + triggerNew[i].IsConverted );
				System.debug('debug_var_triggerOld_IsConverted ' + triggerOld[i].IsConverted );
				System.debug('debug_triggerNew_ConvertedContactId ' + triggerNew[i].ConvertedContactId );
				if (triggerNew[i].IsConverted == true && triggerOld[i].IsConverted == false && triggerNew[i].ConvertedContactId != null){
					
					Leads.add(triggerNew[i].Id);
					LeadToContact.put(triggerNew[i].Id, triggerNew[i].ConvertedContactId);
					System.debug('LeadUpdateULCDetailsOnConvertHandler LeadToContact = ' + LeadToContact);	
				}				
			}

			if(!Leads.isEmpty()){
				System.debug('LeadUpdateULCDetailsOnConvertHandler Leads not empty');
			    //WriteLogInfo('handle', 'Leads.size = (' + Leads.size() + ') LeadToContact = ' + LeadToContact);
				for (ULC_Details__c ULCDetail : [select Id, ContactId__c, LeadId__c from ULC_Details__c where LeadId__c in :Leads]){
					ULCDetail.ContactId__c = LeadToContact.get(ULCDetail.LeadId__c);
					ULCDetail.LeadId__c = null;		
					ULCDetailsToBeUpdated.Add(ULCDetail);
					System.debug('LeadUpdateULCDetailsOnConvertHandler ULCDetailsToBeUpdated = ' + ULCDetailsToBeUpdated);
					
					ULC_Login_History__c LoginHistory = new ULC_Login_History__c (
							ULC_Detail__c = ULCDetail.Id,
							Status__c = 'Success',
							Description__c = 'Lead converted to Contact'
						);
					ToBeInserted.Add(LoginHistory);			
				}
				//Update related to user sense group records on lead convert
				for (User_To_Sense_Group__c userSenseGrp : [select Id, Member_Contact__c, Member_Lead__c from 
											User_To_Sense_Group__c where Member_Lead__c in :Leads]){
					userSenseGrp.Member_Contact__c = LeadToContact.get(userSenseGrp.Member_Lead__c);
					userSenseGrp.Member_Lead__c = null;		
					userSenseGroupsToBeUpd.add(userSenseGrp);
				}
				System.debug('LeadUpdateULCDetailsOnConvertHandler userSenseGroupsToBeUpd.size() = ' + userSenseGroupsToBeUpd.size());
				if (userSenseGroupsToBeUpd.size() > 0){
					try {
						update userSenseGroupsToBeUpd;
						System.debug('LeadUpdateULCDetailsOnConvertHandler userSenseGroupsToBeUpd = ' + userSenseGroupsToBeUpd);
					}
					catch (Exception ex) {
						//WriteLogError('handle', 'userSenseGroupsToBeUpd = ' + userSenseGroupsToBeUpd, ex);
						System.debug('LeadUpdateULCDetailsOnConvertHandler.handle userSenseGroupsToBeUpd: Caught exception: ' + ex.getMessage());
					}
				}
				System.debug('LeadUpdateULCDetailsOnConvertHandler ULCDetailsToBeUpdated.size() = ' + ULCDetailsToBeUpdated.size());
				if (ULCDetailsToBeUpdated.size() > 0){
					try {
						update ULCDetailsToBeUpdated;
				        System.debug('LeadUpdateULCDetailsOnConvertHandler ULCDetailsToBeUpdated = ' + ULCDetailsToBeUpdated);		
					}
					catch (Exception ex) {
						//WriteLogError('handle', 'ULCDetailsToBeUpdated = ' + ULCDetailsToBeUpdated, ex);
						System.debug('LeadUpdateULCDetailsOnConvertHandler.handle ULCDetailsToBeUpdated: Caught exception: ' + ex.getMessage());
					}
				}
				System.debug('LeadUpdateULCDetailsOnConvertHandler ToBeInserted.size() = ' + ToBeInserted.size());
				if (ToBeInserted.size() > 0){
					try {
						insert ToBeInserted;
				    	System.debug('LeadUpdateULCDetailsOnConvertHandler ToBeInserted = ' + ToBeInserted);
				    }
					catch (Exception ex) {
						//WriteLogError('handle', 'ToBeInserted = ' + ToBeInserted, ex);
						System.debug('LeadUpdateULCDetailsOnConvertHandler.handle ToBeInserted: Caught exception: ' + ex.getMessage());
					}
				}
			}
		}
		else if (Trigger.isBefore){
			for (Lead L : triggerNew){
				System.debug('LeadUpdateULCDetailsOnConvertHandler L.IsConverted = ' + L.IsConverted);
				if (L.IsConverted == true){
					L.ActiveULC__c = false;
				}
			}
		}		
	}

	public static void WriteLogInfo(String sMethod, String sData) {
		new ApexDebugLog().createLog(new ApexDebugLog.Information('LeadUpdateULCDetailsOnConvertHandler', sMethod, sData, NULL));
	}

	public static void WriteLogError(String sMethod, String sData, Exception ex) {
		new ApexDebugLog().createLog(new ApexDebugLog.Error('LeadUpdateULCDetailsOnConvertHandler', sMethod, sData, ex));
	}
}