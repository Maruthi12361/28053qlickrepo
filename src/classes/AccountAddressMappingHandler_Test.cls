/**     * File Name:AccountAddressMappingHandler_Test
        * Description : This test class for AccountAddressMappingHandler
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Oct 10th 2016 RamakrishnaKini   Added testing logic
        2.0 Feb 13th 2019 Andreas Nilsson   Changed accounts names for new duplicate rule on account
*/
@isTest
private class AccountAddressMappingHandler_Test {
	
	//Test Data Setup Method
    @testSetup static void  createTestData() { 

    	QlikTech_Company__c qtComp_SWE = QTTestUtils.createMockQTCompany('QlikTech Benelux', 'SWE', 'Sweden', 'SEEMEA');
        //QTCompany[1] = qtComp_HUN.Id;

    	//Create and insert the Accounts 
        List<Account> accList = new List<Account>();
        string accName = ''; 
            accName = 'Test Account for SetSupportOffice0'; 
            accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Customer', BillingCountry = 'Sweden', BillingCity = 'aaa', BillingStreet='aasdasd', 
            	BillingPostalCode = '12345', BillingState ='aaaaa',
            	Support_Office__c = null) );

            accName = 'A very unique name for a SetSupportOffice 1'; 
            accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Customer', Territory_Country__c = 'Sweden', Territory_city__c = 'aaa', Territory_street__c='aasdasd', 
            	Territory_Zip__c = '12345', Teritory_State_Province__c ='aaaaa',Territory_State_Province_free_input__c ='aaaaa',
            	Support_Office__c = null) );
        insert accList;

	}
	
	@isTest
    static void checkAccountInsertMapping(){
    	List<Account> testActs_all = [select id,name,BillingCity,BillingStreet,BillingCountry,Territory_city__c,Territory_Country__c,Territory_street__c,Territory_Zip__c,Teritory_State_Province__c,Territory_State_Province_free_input__c from account where name like 'Test Account for SetSupportOffice%' or name like 'A very unique name for a SetSupportOffice 1']; 
    	system.assertEquals(2, testActs_all.size());
        List<Account> testActs = [select id,name,BillingCity,BillingCountry,BillingStreet,BillingPostalCode,BillingState,Territory_city__c,Territory_Country__c,Territory_street__c,Territory_Zip__c,Teritory_State_Province__c,Territory_State_Province_free_input__c from account where name = 'Test Account for SetSupportOffice0']; 
		system.assertEquals(1, testActs.size());
		system.assertEquals('aaa' , testActs[0].Territory_city__c);
		system.assertEquals('Sweden' , testActs[0].Territory_Country__c);
		system.assertEquals('aasdasd' , testActs[0].Territory_street__c);
		system.assertEquals('12345' , testActs[0].Territory_Zip__c);
		system.assertEquals('aaaaa' , testActs[0].Teritory_State_Province__c);
		system.assertEquals('aaaaa' , testActs[0].Territory_State_Province_free_input__c);

		List<Account> testActs1 = [select id,name,BillingCity,BillingCountry,BillingStreet,BillingPostalCode,BillingState,Territory_city__c,Territory_Country__c,Territory_street__c,Territory_Zip__c,Teritory_State_Province__c,Territory_State_Province_free_input__c from account where name = 'A very unique name for a SetSupportOffice 1'];
		system.assertEquals(1, testActs1.size());
		system.assertEquals(testActs1[0].Territory_city__c , testActs1[0].BillingCity);
		system.assertEquals(testActs1[0].Territory_Country__c , testActs1[0].BillingCountry);
		system.assertEquals(testActs1[0].Territory_street__c , testActs1[0].BillingStreet);
		system.assertEquals(testActs1[0].Territory_Zip__c , testActs1[0].BillingPostalCode);
		system.assertEquals(testActs1[0].Teritory_State_Province__c , testActs1[0].BillingState);
		system.assertEquals(testActs1[0].Territory_State_Province_free_input__c , testActs1[0].BillingState);
		AccountAddressMappingHandler accMap = new AccountAddressMappingHandler();
	}
	
	@isTest
    static void checkAccountUpdMapping(){
    	List<Account> testActs_all = [select id,name,BillingCity,BillingStreet,BillingCountry,Territory_city__c,Territory_Country__c,Territory_street__c,Territory_Zip__c,Teritory_State_Province__c,Territory_State_Province_free_input__c from account where name like 'Test Account for SetSupportOffice%' or name like 'A very unique name for a SetSupportOffice 1']; 
    	system.assertEquals(2, testActs_all.size());
    	Test.startTest();
    	testActs_all[0].BillingState ='bbbb';
    	testActs_all[1].Teritory_State_Province__c ='';
    	testActs_all[1].Territory_State_Province_free_input__c ='ffff';
    	update testActs_all;
    	Test.stopTest();
	}
}