/******************************************************

    Class: MyAccountsForPartnersController
    Description: Controller for myAccountsForPartners Lightning Component used in QlikCommerce community for Partners

    Changelog:
        2019-01-07  Malay Desai, Slalom      Created file
                            
******************************************************/
public  with sharing class MyAccountsForPartnersController {
    
    /*Description:  Fetches End User Account Ids for all the account that Partner Account interacted with via
     *              Opportunity (Owner or Sell Through Parter), Referral (Referring Partner), Contact (Owner) or 
     *              Contract(Supported by Parter, Second Partner or Sell Through Partner).
    */
    public static final String END_USER_ACCOUNT = [SELECT Id, MasterLabel FROM Qlik_Commerce__mdt  WHERE DeveloperName = 'End_User_Account'].MasterLabel;
    
    public static List<String> fetchRelatedAccountIds() {
        List<String> accountIds = new List<String>();
        List<Account> accounts;
        String partnerUserAccount;
        
        List<User> users = [SELECT Id, Contact.AccountId FROM User 
                             WHERE Id =: UserInfo.getUserId()
                             AND UserType LIKE '%Partner%' LIMIT 1];
        if(!users.isEmpty()){
            partnerUserAccount = users.get(0).Contact.AccountId;
            for(Opportunity opp : [SELECT AccountId FROM Opportunity 
                                   WHERE (Sell_Through_Partner__c =: partnerUserAccount
                                   OR OwnerId =: partnerUserAccount)]){
                   if(opp.AccountId != null) accountIds.add(opp.AccountId);                           
            }
                
            for(Referral__c ref : [SELECT End_User_Account__c FROM Referral__c 
                                   WHERE Referring_Partner_Account__c =: partnerUserAccount]){
                   if(ref.End_User_Account__c != null) accountIds.add(ref.End_User_Account__c);                           
            }
                
            for(Contact contact : [SELECT AccountId FROM Contact 
                               WHERE OwnerId =: partnerUserAccount
                               AND Account.RecordType.Name = :END_USER_ACCOUNT]){
                   if(contact.AccountId != null) accountIds.add(contact.AccountId);                           
            }
                
            for(Contract contract : [SELECT AccountId FROM Contract 
                               WHERE (Support_Provided_By__c =: partnerUserAccount
                               OR Second_Partner__c =: partnerUserAccount
                               OR Sell_Through_Partner__c = :partnerUserAccount)]){
                   if(contract.AccountId != null) accountIds.add(contract.AccountId);                           
            }

        }
        return accountIds;
    }
    /* Description: Retrieves the account identified by accountIds or 
     * account owned by Parter User 
     */ 
    @AuraEnabled
    public static List<Account> getAccounts(String[] accountIds, Integer limits , Integer offsets){
        Integer intlimits  = Integer.valueof(limits);
        Integer intoffsets = Integer.valueof(offsets);
        
        List<Account> accounts= [SELECT Id, Name, RecordType.Name, Territory_street__c,Territory_city__c,
                                 Territory_Country__c, Territory_Zip__c, Sector__c, Industry_from_SIC_Code_Lookup__c
                     FROM Account 
                     WHERE (Id IN :accountIds OR OwnerId = :UserInfo.getUserId())
                     ORDER by Name 
                     LIMIT :intlimits 
                     OFFSET :intoffsets];
                //Add isAccessible() check
            return accounts;
    }
     /* Description: Retrieves total count and accountIds */ 
    @AuraEnabled
    public static AccountWrapper getRecordCountAndIds(){
        List<String> accountIds = fetchRelatedAccountIds();
        AggregateResult ar = [SELECT count(Id) total FROM Account
                              WHERE (Id IN :accountIds OR OwnerId =:UserInfo.getUserId())];
        AccountWrapper aw = new AccountWrapper();
        aw.accountIds = accountIds;
        aw.count = (Integer) ar.get('total');
        return aw;
    }
    
    /* Description : Inner wrapper to facilitate returning multiple items in a single server call */
    public Class AccountWrapper{
        @AuraEnabled
        public List<String> accountIds;
        @AuraEnabled
        public Integer count;
    }
}