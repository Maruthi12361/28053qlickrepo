/**     * File Name:AccUpdatePartnerLevelStatusHandler
        * @author : Rodion Vakulovskyi
        * Modification Log ===============================================================
        Ver     Date         Author              Modification 
        1       07.04.2017   Rodion Vakulovskyi  Created Class
        2       07.04.2017   Navneet             Pick list is set with all of the different “Partner Category_Partner Levels” from the related list entries (no spaces)
        2       07.05.2017   Navneet             Added method for Partner Type field in Account
        3       19.07.2017   Pramod Kumar V      Added methods for update of Checkbox on Account for Validation rule (PAEX 71)
        4       23.08.2017   Pramod Kumar V      Added methods for PAEX 227
        5       29.08.2017   Pramod Kumar V      Added methods for PAEX 240
		6       04.09.2017   Rodion Vakulovskyi  Added methods for PAEX 240
    7       03.10.2018 BSL-910 adding logic to populate partner version on account with program version on pcs in processPartnerCategory function
*/
public class AccUpdatePartnerLevelStatusHandler {

    public static void onAfterInsert(List<Partner_Category_Status__c> inputList, Map<Id, Partner_Category_Status__c> newMap) {
        processStatus(inputList);
        processPartnerCategory(inputList);
        partnerFeeInsert(inputList);
		processQPPCategoryPartnerLevel(inputList);
    }

    public static void onAfterUpdate(List<Partner_Category_Status__c> inputList, Map<Id, Partner_Category_Status__c> newMap, Map<Id, Partner_Category_Status__c> oldMap) {
        List<Partner_Category_Status__c> listToProcess = new List<Partner_Category_Status__c> ();
        List<Partner_Category_Status__c> listToProcessPartnercat = new List<Partner_Category_Status__c> ();
		List<Partner_Category_Status__c> listToProcessQPP = new List<Partner_Category_Status__c>();
        Set<Partner_Category_Status__c> listPartnerLevel= new Set<Partner_Category_Status__c>();
        for(Partner_Category_Status__c itemLevel : inputList) {
            if(itemLevel.Partner_Level_Status__c != oldMap.get(itemLevel.Id).Partner_Level_Status__c || itemLevel.Partner_Level__c !=  oldMap.get(itemLevel.Id).Partner_Level__c  || itemLevel.Partner_Category__c !=  oldMap.get(itemLevel.Id).Partner_Category__c) {
                listToProcess.add(itemLevel);
            }
            if(itemLevel.Partner_Level_Status__c != oldMap.get(itemLevel.Id).Partner_Level_Status__c || itemLevel.Partner_Category__c !=  oldMap.get(itemLevel.Id).Partner_Category__c) {
                listToProcessPartnercat.add(itemLevel);
            }
			if(itemLevel.Partner_Level_Status__c != oldMap.get(itemLevel.Id).Partner_Level_Status__c || itemLevel.Program_Version__c !=  oldMap.get(itemLevel.Id).Program_Version__c || itemLevel.Partner_Level__c != oldMap.get(itemLevel.Id).Partner_Level__c) {
				listToProcessQPP.add(itemLevel);
			}
            
        }  
        if(listToProcess.size() > 0) {
            processStatus(inputList);
        }      
        if(listToProcessQPP.size() > 0) {
            processQPPCategoryPartnerLevel(listToProcessQPP);
        }
        if(listToProcessPartnercat.size() > 0) {
            processPartnerCategory(inputList);
        }
        
            partnerFeeUpdate(inputList,newMap,oldMap);
            
           
    }

    public static void onAfterDelete(List<Partner_Category_Status__c> inputList, Map<Id, Partner_Category_Status__c> newMap, Map<Id, Partner_Category_Status__c> oldMap) {
        processStatus(inputList);
        processPartnerCategory(inputList);
        partnerFeeDelete(inputList);
		processQPPCategoryPartnerLevel(inputList);
       
        
    }
    
    private static void processStatus(List<Partner_Category_Status__c> inputList) {
        Set<Id> setOfAccs = new Set<Id>();
        for(Partner_Category_Status__c statusItem : inputList){
            setOfAccs.add(statusItem.Partner_Account__c);
        }
        Map<Id, Account> mapOfAccs = new Map<Id, Account>([SELECT Name, Detailed_Partner_Levels__c, RecordType.Name,(SELECT Id, Partner_Account__c, Partner_Level_Status__c, Partner_Level__c, Program_Version__c, Partner_Category__c FROM Partner_Category_Status__r)  FROM Account WHERE Id in :setOfAccs]);
        Map<Id, Set<String>> mapOfAccountLevels = new Map<Id, Set<String>>();
        for(Id accountItem : mapOfAccs.keySet()) {
           if(mapOfAccs.get(accountItem).Recordtype.Name=='Partner Account'||mapOfAccs.get(accountItem).Recordtype.Name=='OEM Partner Account'){
            mapOfAccs.get(accountItem).Detailed_Partner_Levels__c = '';
            for(Partner_Category_Status__c statusItem : mapOfAccs.get(accountItem).Partner_Category_Status__r) {
                if(statusItem.Partner_Level_Status__c  == 'Active') {
                    if(!mapOfAccountLevels.containsKey(accountItem)) {
                        mapOfAccountLevels.put(accountItem, new Set<String>{statusItem.Partner_Category__c.deleteWhitespace()+'_'+statusItem.Partner_Level__c.deleteWhitespace()});
                    }
                    mapOfAccountLevels.get(accountItem).add(statusItem.Partner_Category__c.deleteWhitespace()+'_'+statusItem.Partner_Level__c.deleteWhitespace());
                }
            }
          }  
        }
        for(Id accountItem : mapOfAccountLevels.keySet()) {
            List<String> listOfLevels = new List<String>();
            listOfLevels.addAll(mapOfAccountLevels.get(accountItem));
            for(String itemLevelString : listOfLevels) {
                mapOfAccs.get(accountItem).Detailed_Partner_Levels__c += itemLevelString + ';';
            }
        }
        if(mapOfAccs.values().size() > 0){
            update mapOfAccs.values();
        }
    }
    
  
    private static void processPartnerCategory(List<Partner_Category_Status__c> inputList) {
        Set<Id> setOfAccs = new Set<Id>();
        for(Partner_Category_Status__c statusItem : inputList){
            setOfAccs.add(statusItem.Partner_Account__c);
        }
        Map<Id, Account> mapOfAccs = new Map<Id, Account>([SELECT Name, PartnerType__c, Detailed_Partner_Levels__c,Partner_Version__c,RecordType.Name, (SELECT Id, Partner_Account__c, Partner_Level_Status__c, Partner_Level__c, Program_Version__c , Partner_Category__c FROM Partner_Category_Status__r)  FROM Account WHERE Id in :setOfAccs]);
        //map to store acctid v/s partner category
        Map<Id, Set<String>> mapOfAccountLevels = new Map<Id, Set<String>>();
        //BSl-910 map to store accid v/s program version
        Map<Id, Set<String>> mapOfAccountVersion = new Map<Id, Set<String>>();
        for(Id accountItem : mapOfAccs.keySet()) {
          if(mapOfAccs.get(accountItem).Recordtype.Name=='Partner Account'||mapOfAccs.get(accountItem).Recordtype.Name=='OEM Partner Account'){
            //BSL-910 initializing partner type and partner version on account to blank if no active pcs record
            mapOfAccs.get(accountItem).PartnerType__c = '';
            mapOfAccs.get(accountItem).Partner_Version__c = '';
            //populating maps created on line 103 and 105 
            for(Partner_Category_Status__c statusItem : mapOfAccs.get(accountItem).Partner_Category_Status__r) {
                if(statusItem.Partner_Level_Status__c  == 'Active') {
                    if(!mapOfAccountLevels.containsKey(accountItem)) {
                        mapOfAccountLevels.put(accountItem, new Set<String>{statusItem.Partner_Category__c});
                    }
                    mapOfAccountLevels.get(accountItem).add(statusItem.Partner_Category__c);
                    if(!mapOfAccountVersion.containsKey(accountItem)){
                      mapOfAccountVersion.put(accountItem, new Set<String>{statusItem.Program_Version__c});
                    }
                    mapOfAccountVersion.get(accountItem).add(statusItem.Program_Version__c);
                }
            }
          } 
        }
        //assigning partner catogory from pcs to partner type on account
        for(Id accountItem : mapOfAccountLevels.keySet()) {
            List<String> listOfLevels = new List<String>();
            listOfLevels.addAll(mapOfAccountLevels.get(accountItem));
            for(String itemLevelString : listOfLevels) {
                mapOfAccs.get(accountItem).PartnerType__c += itemLevelString + ';';
            }
        }
        // assigning program verion from pcs to partner version on account
        for(Id accountItem:mapOfAccountVersion.keySet()){
          for(string versionString:mapOfAccountVersion.get(accountItem)){
            mapOfAccs.get(accountItem).Partner_Version__c += versionString + ';';
          }
        }

        if(mapOfAccs.values().size() > 0){
            update mapOfAccs.values();
        }
    }
    
    private static void partnerFeeInsert(List<Partner_Category_Status__c> inputList) {
     Set<Id> setOfAccs = new Set<Id>();
     Set<Id> accSet = new Set<Id>();
     Map<Id, String> mapOfAccs = new Map<Id, String>();
     for(Partner_Category_Status__c statusItem : inputList){
         
          if(statusItem.Partner_Level_Status__c=='Active' && statusItem.Program_Version__c== 'QPP' && statusItem.Partner_Category__c== 'Influence' && statusItem.Partner_Level__c== 'Authorized') {
              setOfAccs.add(statusItem.Partner_Account__c);
          }
      }
      List<Account> itemToUpdate = new List<Account>();
      for(Account item : [SELECT Id, RecordType.Name, PartnerFeeOpportunity__c FROM Account WHERE Id IN : setOfAccs AND PartnerFeeOpportunity__c = false]) {
        item .PartnerFeeOpportunity__c = true;
        itemToUpdate.add(item );
      } 

      if(itemToUpdate.size()>0) {
        update itemToUpdate;
       } 
    
     }
     
     private static void partnerFeeUpdate(List<Partner_Category_Status__c> inputList, Map<Id, Partner_Category_Status__c> newMap, Map<Id, Partner_Category_Status__c> oldMap){
     Set<Id> setOfAccs = new Set<Id>();
     Map<Id, String> mapOfAccs = new Map<Id, String>();
     for(Partner_Category_Status__c statusItem : inputList){
          if(statusItem.Partner_Level_Status__c=='Active' && statusItem.Program_Version__c== 'QPP' && statusItem.Partner_Category__c== 'Influence'&& statusItem.Partner_Level__c== 'Authorized') {
              setOfAccs.add(statusItem.Partner_Account__c);
              }
    
      }
      List<Account> itemToUpdate = new List<Account>();
      for(Account item : [SELECT Id, PartnerFeeOpportunity__c FROM Account WHERE Id IN : setOfAccs AND PartnerFeeOpportunity__c = false]) {
        item .PartnerFeeOpportunity__c = true;
        itemToUpdate.add(item );
      } 
      if(itemToUpdate.size()>0) {
        update itemToUpdate;
       }  

      Set<Id> setOfAccsDel = new Set<Id>();
      List<Account> itemToUpdateDel = new List<Account>();
      
      for(Partner_Category_Status__c statusItem : inputList){
       if(oldMap.get(statusItem.Id).Partner_Level_Status__c=='Active' && oldMap.get(statusItem.Id).Program_Version__c == 'QPP' && oldMap.get(statusItem.Id).Partner_Category__c== 'Influence'&& oldMap.get(statusItem.Id).Partner_Level__c== 'Authorized') {
          if(oldMap.get(statusItem.Id).Partner_Level_Status__c!= statusItem.Partner_Level_Status__c ||oldMap.get(statusItem.Id).Program_Version__c != statusItem.Program_Version__c || oldMap.get(statusItem.Id).Partner_Category__c != statusItem.Partner_Category__c || oldMap.get(statusItem.Id).Partner_Level__c != statusItem.Partner_Level__c) {
              setOfAccsDel.add(statusItem.Partner_Account__c);
              } 
             }        
      }
      for(Account item : [SELECT Id, PartnerFeeOpportunity__c FROM Account WHERE Id IN : setOfAccsDel AND PartnerFeeOpportunity__c = true]) {
       item.PartnerFeeOpportunity__c = false;
       itemToUpdateDel.add(item);
      } 
      if(itemToUpdateDel.size()>0) {
        update itemToUpdateDel;
       }  

     }
     
     
    private static void partnerFeeDelete(List<Partner_Category_Status__c> inputList) {
     Set<Id> setOfAccs = new Set<Id>();
     Map<Id, String> mapOfAccs = new Map<Id, String>();
     for(Partner_Category_Status__c statusItem : inputList){ 
     System.debug('inputList-->'+inputList);   
     if(statusItem.Program_Version__c== 'QPP' && statusItem.Partner_Category__c == 'Influence' && statusItem.Partner_Level__c == 'Authorized' && statusItem.Partner_Level_Status__c == 'Active'){
        setOfAccs.add(statusItem.Partner_Account__c);
        }
   
      }     
      List<Account> listoFAccs = new List<Account>();
      for(account accountItem : [select id,PartnerFeeOpportunity__c from account where id in: setOfAccs]) {
          
              accountItem.PartnerFeeOpportunity__c = false;
              listoFAccs.add(accountItem);
          }         
        if(listoFAccs.size()>0) {
        update listoFAccs;
          }

     
      }

	private static void processQPPCategoryPartnerLevel(List<Partner_Category_Status__c> inputList) {
		Set<Id> setOfAccs = new Set<Id>();
		for(Partner_Category_Status__c statusItem : inputList){
			setOfAccs.add(statusItem.Partner_Account__c);
		}
		Map<Id, Account> mapOfAccs = new Map<Id, Account>([SELECT Name, Overall_Partner_Level__c, RecordType.Developername, (SELECT Id, Partner_Account__c, Partner_Level_Status__c, Partner_Level__c, Program_Version__c , Partner_Category__c FROM Partner_Category_Status__r)  FROM Account WHERE Id IN :setOfAccs]);
        Map<Id, List<String>> mapOfAccountLevels = new Map<Id, List<String>>();
        for(Id accountItem : mapOfAccs.keySet()) {
          if(mapOfAccs.get(accountItem).Recordtype.Developername=='Partner_Account'){
			mapOfAccs.get(accountItem).Overall_Partner_Level__c = '';
            for(Partner_Category_Status__c statusItem : mapOfAccs.get(accountItem).Partner_Category_Status__r) {
                if(statusItem.Partner_Level_Status__c  == 'Active' && statusItem.Program_Version__c == 'QPP') {
                    if(!mapOfAccountLevels.containsKey(accountItem)) {
                        mapOfAccountLevels.put(accountItem, new List<String>{statusItem.Partner_Level__c});
                    }
                    mapOfAccountLevels.get(accountItem).add(statusItem.Partner_Level__c);
                }
            }
          } 
        }
        for(Id accountItem : mapOfAccountLevels.keySet()) {
			mapOfAccs.get(accountItem).Overall_Partner_Level__c = locateOrderValue(mapOfAccountLevels.get(accountItem));
        }
        if(mapOfAccs.values().size() > 0){
            update mapOfAccs.values();
        }
    }

	private static String locateOrderValue(List<String> listOfvalues) {
		Map<String, Integer> mapOfOrdeValues = new Map<String, Integer>{'Elite' => 1, 'Select' => 2, 'Authorized' => 3, 'Authorized Reseller' => 4};
		String finalValue = '';
		for(String itemValue : listOfvalues) {
			if(mapOfOrdeValues.containsKey(itemValue)){
				if(String.isBlank(finalValue)) {
					finalValue = itemValue;
				}
				if(!String.isBlank(finalValue)) {
					if(mapOfOrdeValues.get(itemValue) <= mapOfOrdeValues.get(finalValue)) {
						finalValue = itemValue;
					}
				}
			}
		}
		return finalValue;
	}
    
}