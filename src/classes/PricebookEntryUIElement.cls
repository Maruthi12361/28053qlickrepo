public class PricebookEntryUIElement {

	Boolean pbeIsSelected;
	PricebookEntry pbe;
		
	
	public PricebookEntryUIElement()
	{
		
	}
	
	public Boolean getPbeIsSelected()
	{ 
		return pbeIsSelected; 
	}
 	public void setPbeIsSelected(Boolean p) 
 	{ 
 		pbeIsSelected = p;
 	 		
 	}
	
	
	public PricebookEntry getPbe()
	{ 
		return pbe; 
	} 	
 	public void setPbe(PricebookEntry pbe)
 	{ 
 		this.pbe = pbe; 
 	}

}