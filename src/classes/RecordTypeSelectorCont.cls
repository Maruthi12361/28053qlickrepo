/*	29/04/2020 OTQ-566 This function will fetch the RecordTypes of provided object and will return a map of recordTypeId and recordTypeNames
it excludes 'master' record type 
note - currently it is enabled for QCCS and Zuora in future to enable for other types just remove condition from if clause
*/
public class RecordTypeSelectorCont {
    
    public static Map<Id, String> recordtypemap;
    
    @AuraEnabled        
    public static Map<Id, String> fetchRecordTypeValues(String objectName){
        
        List<Schema.RecordTypeInfo> recordtypes = Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfos();    
        
        recordtypemap = new Map<Id, String>();
        
        for(RecordTypeInfo rt : recordtypes){
            if((rt.getName() != 'Master' && rt.getName().trim() != '' && rt.isActive() && rt.isAvailable()) 
               && (rt.getName() == 'Sales QCCS Zuora Subscription' || rt.getName() == 'Sales QCCS'))
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
        }        
        return recordtypemap;
    }
}