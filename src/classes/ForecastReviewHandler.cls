public with sharing class ForecastReviewHandler {

    private static final String BEST_CASE = 'Best Case';
    private static final String EXPECTED_CASE = 'Expected';
    private static final String WORST_CASE = 'Worst Case';

    public ForecastReviewHandler() { }

    public static void processForecastReviewCreation(List<Id> fcIds) {
        List<pse__Forecast_Enhanced_Summary_Category__c> newFedcList = new List<pse__Forecast_Enhanced_Summary_Category__c>();
        Map<Id, Forecast_Summary_Review__c> reviewMap = new Map<Id, Forecast_Summary_Review__c>();
        Map<Id, List<pse__Forecast_Enhanced_Summary_Category__c>> fescMap = new Map<Id, List<pse__Forecast_Enhanced_Summary_Category__c>>();
        Set<Id> fedcIds = new Set<Id>();
        Set<Id> summaryIds = new Set<Id>();

        System.debug('fcIds: ' + fcIds);

        List<pse__Forecast_Enhanced_Summary_Category__c> fescList = [SELECT Id, pse__Forecast_Enhanced_Summary__c, pse__Forecast_Enhanced_Summary__r.pse__Forecast_Enhanced_Calculation__c,
                                                                            pse__Category_Label__c
                                                                     FROM pse__Forecast_Enhanced_Summary_Category__c
                                                                     WHERE pse__Forecast_Enhanced_Summary__r.pse__Forecast_Enhanced_Calculation__c IN :fcIds];

        System.debug('fescList: ' + fescList);
        // if (Trigger.isInsert || Trigger.isUpdate) {
            // for (pse__Forecast_Enhanced_Summary_Category__c fesc : (List<pse__Forecast_Enhanced_Summary_Category__c>) Trigger.new) {
            for (pse__Forecast_Enhanced_Summary_Category__c fesc : fescList) {
                // if (Trigger.isInsert) {
                    // newFedcList.add(fedc);
                    if (fesc.pse__Forecast_Enhanced_Summary__c != null) {
                        // summaryIds.add(fedc.pse__Forecast_Enhanced_Summary__c);
                        if (!fescMap.containsKey(fesc.pse__Forecast_Enhanced_Summary__c)) {
                            fescMap.put(fesc.pse__Forecast_Enhanced_Summary__c, new List<pse__Forecast_Enhanced_Summary_Category__c>());
                        }
                        fescMap.get(fesc.pse__Forecast_Enhanced_Summary__c).add(fesc);
                    }

                // }
                // if (Trigger.isUpdate) {
                //     pse__Forecast_Enhanced_Summary_Category__c oldFesc = (pse__Forecast_Enhanced_Summary_Category__c) Trigger.oldMap.get(fesc.Id);
                //     if (isFescDifferent(oldFesc, fesc)) {
                //         // newFedcList.add(fedc);
                //         // fedcIds.add(fedc.Id);
                //         if (fesc.pse__Forecast_Enhanced_Summary__c != null) {
                //             // summaryIds.add(fedc.pse__Forecast_Enhanced_Summary__c);
                //             if (!fescMap.containsKey(fesc.pse__Forecast_Enhanced_Summary__c)) {
                //                 fescMap.put(fesc.pse__Forecast_Enhanced_Summary__c, new List<pse__Forecast_Enhanced_Summary_Category__c>());
                //             }
                //             fescMap.get(fesc.pse__Forecast_Enhanced_Summary__c).add(fesc);
                //         }
                //     }
                // }
            }
        // }

        if (fescMap != null && !fescMap.isEmpty()) {
            List<Forecast_Summary_Review__c> reviewQueryList = [SELECT Id, Forecast_Summary__c
                                                                FROM Forecast_Summary_Review__c
                                                                WHERE Forecast_Summary__c IN :fescMap.keyset()];

            if (reviewQueryList != null && !reviewQueryList.isEmpty()) {
                for (Forecast_Summary_Review__c fsr : reviewQueryList) {
                    reviewMap.put(fsr.Forecast_Summary__c, fsr);
                }
            }

            Map<Id, pse__Forecast_Enhanced_Summary__c> summaryMap = new Map<Id, pse__Forecast_Enhanced_Summary__c>(
                [SELECT Id, pse__Forecast_Enhanced_Calculation__c, pse__Forecast_Enhanced_Calculation__r.pse__Time_Period__c,
                        pse__Region__c, pse__Region__r.Operational_Manager__c, pse__Region__r.Regional_Manager__c
                 FROM pse__Forecast_Enhanced_Summary__c
                 WHERE Id IN :fescMap.keyset()]);

            for (Id key : fescMap.keyset()) {
                pse__Forecast_Enhanced_Summary__c fes;
                if (summaryMap != null && summaryMap.containsKey(key)) {
                    fes = summaryMap.get(key);
                }
                Forecast_Summary_Review__c fsr;
                if (reviewMap != null && reviewMap.containsKey(key)) {
                    fsr = reviewMap.get(key);
                } else {
                    fsr = new Forecast_Summary_Review__c();
                    fsr.Forecast_Summary__c = key;
                    if (fes != null) {
                        fsr.Forecast_Calculation__c = fes.pse__Forecast_Enhanced_Calculation__c;
                        fsr.Region__c = fes.pse__Region__c;
                        fsr.Time_Period__c = fes.pse__Forecast_Enhanced_Calculation__r.pse__Time_Period__c;
                    }
                }
                fsr.Operational_Manager__c = fes.pse__Region__r.Operational_Manager__c;
                fsr.Regional_Manager__c = fes.pse__Region__r.Regional_Manager__c;
                fsr.Accepted_by_Operational_Manager__c = false;
                fsr.Accepted_by_Regional_Manager__c = false;

                for (pse__Forecast_Enhanced_Summary_Category__c summ : fescMap.get(key)) {
                    if (BEST_CASE == summ.pse__Category_Label__c) {
                        fsr.Best_Forecast_Summary_Category__c = summ.Id;
                    }
                    if (EXPECTED_CASE == summ.pse__Category_Label__c) {
                        fsr.Expected_Forecast_Summary_Category__c = summ.Id;
                    }
                    if (WORST_CASE == summ.pse__Category_Label__c) {
                        fsr.Worst_Forecast_Summary_Category__c = summ.Id;
                    }
                }
                reviewMap.put(fsr.Forecast_Summary__c, fsr);
            }

            if (reviewMap != null && !reviewMap.isEmpty()) {
                upsert reviewMap.values();
            }
        }
    }

    private static Boolean isFescDifferent(pse__Forecast_Enhanced_Summary_Category__c oldFesc, pse__Forecast_Enhanced_Summary_Category__c newFesc) {
        if (oldFesc.pse__Forecast_Value__c != newFesc.pse__Forecast_Value__c || oldFesc.pse__Override_Value__c != newFesc.pse__Override_Value__c) {
            return true;
        }
        return false;
    }


}
