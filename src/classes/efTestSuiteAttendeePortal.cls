//*********************************************************/
// Author: Mark Cane&
// Creation date: 24/08/2010
// Intent:  
//			
// Change History
// --------------
// Author: IRN
// Change date: 2015-10-22	 
// Intent: Added webserviceMock since the winter 16 release
//*********************************************************/
@isTest
private class efTestSuiteAttendeePortal{
    static testMethod void interactionUnitTest(){
    	Boolean b;
    	PageReference p;
    	String s;
    	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
        efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
        
        System.runAs(efTestSuiteHelper.registrantUser){
        	// Comment& : In this context we need a registered registration.        	
        	efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
        	update efTestSuiteHelper.r;
        	
        	efAttendeePortalController ctl = new efAttendeePortalController();

			p = ctl.checkInitialState();    
		   	p = ctl.checkState();
		    s =  ctl.getEventBannerUrl();    
		    s =  ctl.getAttendeeLabelActivityIntro();
		    s =  ctl.getEventName();    
		    b =  ctl.getShowSessions();    
		    b =  ctl.getShowActivities();    
		    s =  ctl.getRegistrationId();    
		    b =  ctl.hasClassesByGroup;    
    		List<efClassWrapper> listClasses = ctl.getClassesByGroup();
    		List<efProductWrapper> listProducts = ctl.getTrainingAndActivities();
		    b =  ctl.isValidActivityData();
		    b =  ctl.getValidatedOnActivityPage();
		            
    		efActivityUtil eUtil = ctl.getActivity();
		    //s =  ctl.getUserId();
    		efRegistrationWrapper eReg = ctl.getAttendeeRegistration();		    
		    b =  ctl.getHasAnnouncements();    
    		List<efAnnouncementWrapper> eAnnouncementList = ctl.getAnnouncements();   
		    s =  ctl.getMessage();
    		efSessionWrapper[] sessions = ctl.getActivities();    	    
    		ctl.setActivities(sessions);     
		    p = ctl.addActivities();
		    p = ctl.removeActivities();
		    
		    p = ctl.goHome();
        }
    }
    
    static TestMethod void sessionInteractionTest(){
    	Boolean b;
    	PageReference p;
    	String s;
    	Integer i;    	
    	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
    	efAttendeePortalSessionController ctl;
    	
        efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
        
        System.runAs(efTestSuiteHelper.registrantUser){        
    		// Comment& : In this context we need a registered registration.        	
	    	efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
	    	update efTestSuiteHelper.r;        	
	    	
	    	ctl = new efAttendeePortalSessionController();

			p = ctl.checkInitialState(); 
		    p = ctl.checkState();
		    s = ctl.getRegistrationId();
			s = ctl.getEventBannerUrl();      
		    b = ctl.getShowRecommendedSessions();
		    
        }
        
        // Comment & : System mode needed, as select and update dml to Session__c (not available to registrant directly.)
	    List<efSessionWrapper> listSession = ctl.getPageSessions();
	    List<SelectOption> listOption = ctl.getCriteriaTrackList();    
	    
	    s = ctl.getCriteriaTrack();    
	    ctl.setCriteriaTrack(efTestSuiteHelper.t.Id);    
	    
	    listOption = ctl.getCriteriaDateList();
	    List<String> listString = ctl.getCriteriaDates();
	    ctl.setCriteriaDates(listString);
	    
	    listString = ctl.getCriteriaExperiences();		    		   
	    ctl.setCriteriaExperiences(listString);
	    
	    s =  ctl.getCriteriaKeyword();
	    ctl.setCriteriaKeyword('');		    
	    ctl.applyCriteria();
	    
	    s = ctl.getAttendeeTrack();
	    ctl.seeRecommendedSessions();
	    ctl.seeHighlightedSessions();
	   	
	   	b = ctl.getIsValidKeyword();
	   	ctl.seeKeywordSessions();   
	   	
	    ctl.seeAllSessions();  
	       
	    s = ctl.getTopFilter();
	    s = ctl.getSortBy();
	    s = ctl.getSortDirection();    
	    ctl.sortByName();
	    ctl.sortByDate();   
	    ctl.sortByTrack();    
	    i = ctl.getTotalSessionCount();
	    
	    ctl.seeAllSessions();
	    efAttendeePortalSessionController.PageNumber pn = ctl.getCurrentPage();    
	    i = ctl.getCurrentPageNumber();    
	    ctl.setCurrentPageNumber(i);    
	    b = ctl.getShowPageList();    
	    List<efAttendeePortalSessionController.PageNumber> listPN = ctl.getPageList();
	    ctl.nextPage();
	    ctl.previousPage();    
	    b = ctl.getIsFirstPage();    
	    b = ctl.getIsLastPage();       		      
	    ctl.setSelectedSessionID(efTestSuiteHelper.s.Id);       
	    ctl.selectSession();
	    efSessionWrapper session = ctl.getSelectedSession();
	    b = ctl.getSelectedSessionEnrolled();
	    b = ctl.getSelectedSessionConflict();
	    
	    b = ctl.getShowSessions();
		b = ctl.getShowActivities();
		            
	    ctl.createSessionAttendance();
    }
    
    public static testMethod void testCalendarPrintController(){
    	Boolean b;
    	PageReference p;
    	String s;    	
    	efTestSuiteHelper.setup();
        Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
    	
    	System.runAs(efTestSuiteHelper.registrantUser){        
	    	// Comment& : In this context we need a registered registration.        	
		    efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
		    update efTestSuiteHelper.r;  
    	
    	
	        efAttendeePortalCalendarPrintController c = new efAttendeePortalCalendarPrintController();
	        List<efClassWrapper> cgList = new List<efClassWrapper>();
	        
	        p = c.checkState();
			s = c.getEventBannerUrl();
			s = c.getEventName();
			b = c.getHasMessage();
			s = c.getRegistrationId();
			cgList = c.getClassesByGroup();
			s = c.getMessage();
    	}
    }
    
    public static testMethod void testSessionWrapper(){
    	Boolean b;
    	PageReference p;
    	String s; 
    	DateTime d;   	
    	efTestSuiteHelper.setup();
        Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
    	
    	System.runAs(efTestSuiteHelper.registrantUser){        
	    	// Comment& : In this context we need a registered registration.        	
		    efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
		    update efTestSuiteHelper.r;
		    
		   	efSessionWrapper sw = new efSessionWrapper(efTestSuiteHelper.s);
		    sw = new efSessionWrapper(efTestSuiteHelper.s, true);
		        	
			s = sw.getSessionId();			    
			Session__c sess = sw.getSessions();
						    
			s = sw.getIndex();			    
			sw.setCounter(1);
			s = sw.getCounter();			
			b = sw.getAttendeeStatus();			    
			s = sw.getSessionAttendeeId();			
			b = sw.getRecordCreated();			    
			sw.setRecordCreated(true);			       
			List<Speaker__c> listSpeaker = sw.getSpeakerList();			   
			s = sw.getSpeakerNames();			    
			s = sw.getSessionName();			
			s = sw.getEscapedName();			    
			s = sw.getSid();			
			s = sw.getSdescription();			
			s = sw.getTrackName();			    
			s = sw.getDescription();			    
			s = sw.getCTSpeakerName();			    
			s = sw.getVenue();			    
			s = sw.getLevel();			            
			s = sw.getTheme();			    
			s = sw.getCategory();			    
			s = sw.getRoom();						
			sw.setSpecialRequirement('specialRequirement');
			s = sw.getSpecialRequirement();						    			            
			b = sw.getIsAddedToSchedule();			
			s = sw.getBalance();			    
			b = sw.getIsSessionFull();									   
			b = sw.getHasTopics();			    
			s = sw.getSessionDate();			    
			s = sw.getSessionEndDate();			    
			s = sw.getSDaynDate();			
			s = sw.getTimeRange();			    
			d = sw.getSDate();			    
			d = sw.getEDate();			    
			s = sw.getSessionLongTime();			    
			s = sw.getSessionTime();			
			s = sw.getFormatedDate();			    
			s = sw.getFormatedTime();			    
			s = sw.getLongStartDay();			    
			s = sw.getShortStartDay();			    
			s = sw.getLongStartHour();			    
			b = sw.getIsSelected();			    
			sw.setIsSelected(true);			    
			b = sw.getIsInBucket();			    
			sw.setIsInBucket(true);			
			b = sw.getIsExpanded();			   
			sw.setIsExpanded(true);			
			b = sw.getIsSelectedforPage();			    
			sw.setIsSelectedforPage(true);			
			b = sw.getAddTableDivider();			    
			sw.setAddTableDivider(true);			    
			b = sw.getDefaultSelected();			    
			sw.setDefaultSelected(true);			    
			b = sw.getIsMandatory();			    
			sw.setIsMandatory(true);			
			List<SelectOption> listOptions = sw.getTopicList();			
			s = sw.getSelectedTopic();			    
			sw.setSelectedTopic('selectedTopic');			    
			s = sw.getFormattedSelectedTopic(); 	
    	}
    }

	public static testMethod void testAnnouncementWrapper(){
    	String s;
    	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());   	
    	efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
    	
    	System.runAs(efTestSuiteHelper.registrantUser){        
	    	// Comment& : In this context we need a registered registration.        	
		    efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
		    update efTestSuiteHelper.r;
		    
		    efAnnouncementWrapper aw = new efAnnouncementWrapper(efTestSuiteHelper.an);
		    s = aw.getFormatedTime();
		    s = aw.getAnnouncementName();
		   	s = aw.getAnnouncementText();
    	}
    } 
    
	public static testMethod void testSessionGroupWrapper(){
    	Boolean b;
    	PageReference p;
    	String s; 
    	DateTime dt;
    	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
    	efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
    	
    	System.runAs(efTestSuiteHelper.registrantUser){        
	    	// Comment& : In this context we need a registered registration.        	
		    efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
		    update efTestSuiteHelper.r;
		    
		    efSessionGroupWrapper sw = new efSessionGroupWrapper();
		    
			sw.setGroupName('groupName');
			s = sw.getGroupName();
			sw.setGroupDate(Date.today());
			dt = sw.getGroupDate();   
			s= sw.getGroupLongDate();
			b = sw.getHasSpecialActivities();
			sw.setHasSpecialActivities(true);  
			List<efSessionWrapper> listSW = sw.getGroupItems();
			b = sw.getIsEmpty();
			
			efSessionWrapper w = new efSessionWrapper(efTestSuiteHelper.s);
			sw.addItem(w);
			sw.setItemList(new List<efSessionWrapper>{w});    					
    	}
    }

	public static testMethod void testSessionAttendanceWrapper(){
    	Boolean b;
    	PageReference p;
    	String s; 
    	DateTime dt;
    	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
    	efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
    	
    	System.runAs(efTestSuiteHelper.registrantUser){        
	    	// Comment& : In this context we need a registered registration.        	
		    efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
		    update efTestSuiteHelper.r;
		    		    
		    Session_Attendance__c sa = new Session_Attendance__c();
		    sa.Session__c = efTestSuiteHelper.s.Id;
		    sa.Attendee_Name__c = efTestSuiteHelper.registrantUser.Id;
		    
		    efSessionAttendanceWrapper sw = new efSessionAttendanceWrapper(sa);
		    sa = sw.getSessionAttendance();		    
			Session__c newSession = sw.getSession(); 					
    	}
    }
    
    public static testMethod void testAttendeePortalRegistration(){
    	Boolean b;
    	PageReference p;
    	String s; 
    	DateTime dt;
    	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
    	efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
    	
    	System.runAs(efTestSuiteHelper.registrantUser){        
	    	// Comment& : In this context we need a registered registration.        	
		    efTestSuiteHelper.r.Status__c=efConstants.REG_STATUS_REGISTERED;
		    update efTestSuiteHelper.r;
		    		    
		    efAttendeePortalRegistrationController ctl = new efAttendeePortalRegistrationController();    
		    ctl.initialize();   
		    p = ctl.checkInitialState();
		    p = ctl.checkState();
		    s = ctl.getEventBannerUrl();
		    b = ctl.getIsEmployee();
		    ctl.setIndustry('industry');
		    s = ctl.getIndustry();		    		    
		    s = ctl.getAttendeeType();
		    ctl.setSeparateNameOnBadge(true);
		    b = ctl.getSeparateNameOnBadge();		    
		    s = ctl.getSeparateNameOnBadgeText();		        
		    ctl.setBusinessName('ABC Inc');
		    s = ctl.getBusinessName();		    
		    ctl.setBusinessPhone('23543543');
		    s = ctl.getBusinessPhone();		    
		    b = ctl.getValidatedOnRegisterPage();
		    s = ctl.getMessage();
		    p = ctl.cancelRegistration();
		    b = ctl.getIsInvalidData();
		    efRegistrationValidator val = ctl.getRegValidator(); 
		    b = ctl.isValidRegisterData();
		    p = ctl.saveAdditional();     
		    b = ctl.isAttendeeType('Customer');
		    s = ctl.getRegistrationId();
		    ctl.setCompanySize('10-1000');
		    s = ctl.getCompanySize();		    
		    
		    ctl.startEdition();
		    b = ctl.getInEdition();
		    p = ctl.cancelEdition();
		    
		    b = ctl.getShowSessions();
			b = ctl.getShowActivities();		    
    	}    	
    }
}