/********
* NAME : Opportunity
* Description:Helper class for the opportunity object - called from create new opportunity (s-control)
* Create by: IRN 2016-02-05
*
*Change Log:
******/
global class OpportunityHelperClass
{
    public static List<String> closedStages = new List<String>{'ER - Closed Won', 'ER - Closed Lost', 'OEM - Closed Won','OEM - Closed Lost',
    'OEM Pre-Pay Closed Won','Deal Split-closed Won','Closed Won','Closed Lost','Finance Rejected','QSG Closed Won', 'AP - Closed Won', 
    'AP - Closed Lost','Closed NFR'};
    
    webservice static boolean doOpenOpportunitiesExist(Id accountId){
        Integer count = [Select count() from Opportunity where AccountId = :accountId and stageName not in : closedStages]; 
        if(count > 0){
            return true;    
        } else{
            return false;
        }
        
    }
}