/******************************************************************
*
* 2017-02-17 AIN Initial implementation for CR# 85565, this batch run changes the approver on the timecard to the PM of the related project 
* if they are different, any approvals that are set to the old PM is also changed to the new PM
*
******************************************************************/
global class TimecardApproverSyncBatch implements Database.Batchable<sObject>, Schedulable {
    public TimecardApproverSyncBatch() {
        system.debug('Constructor');
        pse__Timecard_Header__c header = new pse__Timecard_Header__c();
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('Start method');
        return Database.getQueryLocator([select id, Name, pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c, 
            pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__r.Name, pse__Approver__c, pse__Approver__r.Name, 
            Approver_is_Project_Manager__c from pse__Timecard_Header__c where pse__Status__c = 'Submitted' and Approver_is_Project_Manager__c = false and pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c != null]);
    }
    global void execute(SchedulableContext sc){
        system.debug('Execute SchedulableContext');
        Id pId = Database.executeBatch(new TimecardApproverSyncBatch()); //& default scope of 200.
    }    
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        system.debug('Execute batchable');
        //Update timecards
        List<pse__Timecard_Header__c> timecards = (List<pse__Timecard_Header__c>)scope;
        List<Id> timecardIds = new List<Id>();
        Map<Id, Id> timecardOldApprovers = new Map<Id, Id>();
        Map<Id, Id> timecardNewApprovers = new Map<Id, Id>();
        for(pse__Timecard_Header__c timecard : timecards){
            system.debug('Setting approver from ' + timecard.pse__Approver__r.Name + ' to ' + timecard.pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__r.Name + ' on timecard ' + timecard.Name);
            timecardIds.Add(timecard.Id);
            timecardOldApprovers.put(timecard.Id, timecard.pse__Approver__c);
            timecardNewApprovers.put(timecard.Id, timecard.pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c);
            timecard.pse__Approver__c = timecard.pse__Project__r.pse__Project_Manager__r.pse__Salesforce_User__c;
        }
        if(timecards.size() > 0){
            Database.SaveResult[] srList = Database.update(timecards, false);
        }
        //Update approver on those timecards (Max 200)
        system.debug('Before change approver on time cards');
        if(timecardIds.size() > 0) {
            system.debug('Inside timecardIds.size() > 0');
            List<ProcessInstanceWorkitem> workItems = [select Id, ActorId, OriginalActorId, ProcessInstanceId, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem
            where IsDeleted = false and ProcessInstance.TargetObjectId in :timecardIds  and ActorId in :timecardOldApprovers.values() limit 200];
            system.debug('Number of workitems found: ' + workItems.size());
            List<ProcessInstanceWorkitem> workItemsToUpdate = new List<ProcessInstanceWorkitem>();
            for(ProcessInstanceWorkitem workItem : workItems) {
                system.debug('Workitem: ' + workItem.Id);
                if(workItem.ActorId == timecardOldApprovers.get(workItem.ProcessInstance.TargetObjectId)){
                    system.debug('Adding workitem ' + workItem.Id + ' to update.');
                    workItem.ActorId = timecardNewApprovers.get(workItem.ProcessInstance.TargetObjectId);
                    workItemsToUpdate.add(workItem);
                }
            }
            system.debug('Number of workitems to update: ' + workItemsToUpdate.size());
            if(workItemsToUpdate.size() > 0)
                update workItemsToUpdate;
        }
    }
    global void finish(Database.BatchableContext BC){
      system.debug('Finish');
    } 

    //Cron expression to start this job in a sandbox: System.schedule('TimecardApproverSyncBatch','0 20 13 * * ?', new TimecardApproverSyncBatch());   
    //Use anonymous apex to start
}