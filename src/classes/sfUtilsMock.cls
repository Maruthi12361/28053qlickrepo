/*****************************************************************************************
* 2015-09-28 AIN Webservice Mock for class sfUtilsQlikviewCom, using WebServiceMockDispatcher is prefered, see
* that class for details.
****************************************************************************************/
@isTest
public class sfUtilsMock implements WebServiceMock {

  public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    system.debug('sfUtilsMock Start');
    if(request instanceof sfUtilsQlikviewCom.UpdateIntegrationOwner_element)
    {
      system.debug('instanceof sfUtilsQlikviewCom.UpdateIntegrationOwner_element');
      response.put('response_x', new sfUtilsQlikviewCom.UpdateIntegrationOwnerResponse_element());
    }
    system.debug('sfUtilsMock End');
       
  }
}