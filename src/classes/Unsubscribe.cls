/******************************************************

	Class: Unsubscribe
	
	Class that unsubscribe contacts from email (contacts
	and leads).
	
	Source:
	http://wiki.developerforce.com/index.php/Force.com_Email_Services_Unsubscribe
	
	Initiator: Tina Jensen 
	
	Changelog:
		2009-10-06	MBH		Created file
		
		
******************************************************/
global class Unsubscribe implements Messaging.inboundEmailHandler{

	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env ) {

		System.Debug('Unsubscribe.handleInboundEmail: Starting');
		System.Debug('Got email from: ' + env.fromAddress);
		System.Debug('Got email to:   ' + env.toAddress);
		
		// Create an inboundEmailResult object for returning 
		// the result of the Apex Email Service				
		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
		 
		List<Contact> lc = new List <Contact>();
		List<Lead> ll = new List <Lead>();
		 
		if (email.subject.toLowerCase().contains('unsubscribe')) {
		 	
			try {
		 		
		 	// Lookup all contacts with a matching email address
				for (Contact c : [Select Id, Name, Email, HasOptedOutOfEmail From Contact Where Email = :env.fromAddress And hasOptedOutOfEmail = false	Limit 100]) {
								
					// Add all the contacts into the List	
		            c.hasOptedOutOfEmail = true;
					lc.add(c);					               
				}	 
				// update all the Contact records
		  		update lc;
			} catch (System.QueryException e) {
				System.debug('Contact Query Issue: ' + e);
			} 	
		
		 	try {
		 		// Lookup all leads matching the email address
				for (Lead l : [Select Id, Name, Email, HasOptedOutOfEmail From Lead	Where Email = :env.fromAddress And isConverted = false And hasOptedOutOfEmail = false Limit 100]) {
					// Add all the leads to the List		
					l.hasOptedOutOfEmail = true;
					ll.add(l);
							           
		           System.debug('Lead Object: ' + l);   
				}	 
				
				// Update all Lead records in the query
				update ll;
			} catch (System.QueryException e) {
				System.debug('Lead Query Issue: ' + e);
			} 	
		
			System.debug('Found the unsubscribe word in the subject line.');
		 } else {
		 	System.debug('No Unsuscribe word found in the subject line.' );
		 }

		// Return true and exit
		// True will confirm it is complete and no bounced email 
		// should be send the sender of the unsubscribe request. 
		result.success = true;
		return result;
	}	
	
}