/**********************
*
* 2015-10-30 AIN Callout moved from QVM_ScheduleLogoBatch to the start() method to avoid callout in schedulable apex
*
*********************/
global class QVM_LogoBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	static String query = 'Select Id, (Select Id,Name from Attachments where Name like \'logo_%\') from Account where RecordType.Name=\'Partner Account\'';
	
	static QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
		
	private String OwnerId;

	public static S3.AmazonS3 as3;
	
	global QVM_LogoBatch () {
		
	}
		
	global database.queryLocator start(Database.BatchableContext BC) {

		//QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
        as3 = new S3.AmazonS3(settings.S3_Key__c,settings.S3_Secret__c); // credentials 
        Datetime now = Datetime.now();

		S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));
        String bucketOwnerId = allBuckets.Owner.Id;
        this.OwnerID = bucketOwnerId;

		if (Test.isRunningTest()) {
			query = query + ' limit 200';
		}
		return database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, list <Account> accounts) {
		
		//as3 = new S3.AmazonS3(settings.S3_Key__c,settings.S3_Secret__c); // credentials
		
		String bucketName = settings.S3_Bucket__c;
		
		for (Account account:accounts) {
			for (Attachment attachment:account.attachments) {
				System.debug('Logo: '+attachment.Name);
				Datetime now = Datetime.now();
	            String folder = 'logos/';
	            String permission = 'public-read';
	            
	            Id attachmentId = attachment.Id;
	            Attachment attachmentBody = [select Body,BodyLength from Attachment where Id = :attachmentId];
	            String base64Body = EncodingUtil.base64Encode(attachmentBody.Body);
	            
	            Boolean putObjResult = false;
	            
	            //Not allowed during testing so skip it
	            if (!Test.isRunningTest()) {
	            	 putObjResult = as3.PutObjectInline_ACL(bucketName,folder + account.Id+'.gif', null,base64Body, attachmentBody.BodyLength ,permission,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId);
	            }
	            
	            if(!putObjResult) {
	                System.debug('Error storing partner logo: '+account);
	            }
			}
		}
	}	
	
	global void finish(Database.BatchableContext BC) {
    
    }//End of Finish
}