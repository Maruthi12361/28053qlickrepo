/*
@author: Anthony Victorio, Model Metrics
@date: 02/06/2012
@description: Controller for the QVM registration page
*/
public without sharing class QVM_ControlPanelRegistrationCon {
    
    public QVM_Partner_Data__c partnerData {get; set;}
    public User accountOwner {get; set;}
    public Boolean showPendingMessage {get;set;}
    
    public QVM_Terms_Conditions__c terms {get; set;}
    
    public QVM_ControlPanelRegistrationCon() {
        QVM_Partner_Data__c existingPartnerData;
        try {
            existingPartnerData = [select Id, Agreed_to_T_C__c, Partner_Success_Manager__r.Name, CreatedDate
            from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() limit 1];
        } catch(Exception e) {}
        partnerData = (existingPartnerData == null) ? new QVM_Partner_Data__c() : existingPartnerData;
        showPendingMessage = (existingPartnerData == null) ? false : true;
        if(QVM.getUserAccountOwnerId() != null)
        {
        accountOwner = [select Id, FirstName, LastName, Phone, Email from User where Id = :QVM.getUserAccountOwnerId()];
        }
        
        QVM_Terms_Conditions__c existingTerms;
        try {
            existingTerms = [select Id, Content__c from QVM_Terms_Conditions__c where Active__c = true limit 1];
        } catch(Exception e) {}
        terms = (existingTerms == null) ? new QVM_Terms_Conditions__c() : existingTerms;
    }
    
    public String getRegistrationSubmittedMsg() {
        return String.format(Label.QVM_Registration_Submitted, new String[] {
            partnerData.CreatedDate.format('dd/MM/yyyy', 'PST'),
            partnerData.Partner_Success_Manager__r.Name
        });
    }
    
    public String getRegistrationEditProfileMsg() {
        return String.format(Label.QVM_Registration_Edit_Profile, new String[] {
            partnerData.Id
        });
    }
    
    public PageReference submitForm() {
        PageReference next;
        
        if(partnerData.Agreed_to_T_C__c == null || !partnerData.Agreed_to_T_C__c) {
            ApexPages.addMessages(new QVM.newException(Label.QVM_Registration_Agree_TC_Error) );
        } else {
            
            try {
                
                partnerData.Partner_Account__c = QVM.getUserAccountId();
                partnerData.Contact__c = QVM.getUserContactId();
                partnerData.Partner_Success_Manager__c = QVM.getUserAccountOwnerId();
                upsert partnerData;
                
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Submitting request for approval.');
                req.setObjectId(partnerData.id);
                Approval.ProcessResult result = Approval.process(req);
                
                next = Page.QVM_ControlPanelMain;
                
            } catch (Exception e) { ApexPages.addMessages(e); }
            
        }
        
        return next;
             
    }

}