@isTest 
private class MyCustomLookupControllerTestClass {
    static testMethod void validate() {
    
    // Create a new account object.
    Account testAccount = new Account(Name = 'TestAccount');
    insert testAccount;
  
    
    //CREATE TEST ACCOUNT PLAN
    Account_Plan__c accountplan=new Account_Plan__c ();
    accountplan.account__c=testaccount.id;
    insert accountplan;
    
    //CREATE TEST CONTACT
    Contact contact = new contact(lastname='lname');
    contact.Job_title__c='salesrep';
    insert contact; 
    Contact contact2 = new contact(lastname='lname');
    insert contact2;  
    
    //CREATE TEST INFLUENCER
    Influencer__C influencertest = new Influencer__c();
    influencertest.Account_Plan__c= accountplan.id;
    Influencertest.Title__c = contact.Job_title__c;
    influencertest.contact__c=contact2.id;
       

    Test.startTest();
        PageReference pageRef2 = Page.MyCustomLookup;
        Test.setCurrentPage(pageRef2);

        ApexPages.currentPage().getParameters().put('id', accountplan.Id);
        MyCustomLookupController controller = new MyCustomLookupController();       
            controller.Influencer = influencertest;
            controller.save();
            controller.cancel();
            
            
            
            
        // NEW SECTION TO TEST THE INFLUENCER EXTENSION CONTROLLER   
         ApexPages.StandardController controller_2 = new ApexPages.StandardController(influencertest);
  
         InfluencereditpageController  iepc = new  InfluencereditpageController (controller_2);
         iepc.save();
        
         PageReference pageRef = Page.Influencer_edit;
         pageRef.getParameters().put('id', String.valueOf(influencertest.Id));
         Test.setCurrentPage(pageRef);
        
              
    Test.stopTest();       
         
        
   }
}