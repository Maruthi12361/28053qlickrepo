/*
 File Name: CaseBusinessCriticalHandler
 Moved from trigger CaseBusinessCritical on Case (before insert, after insert, after update)
 Does: Will get fired when there is a change in Case record. The After trigger is written to create Chater Feed
 on End User Account. This happens when an external support case gets severity level of 1. Ref CR# 8269.

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.
            Due code analisys it used only before insert!
            Can be more optimized.
 2019-26-12 extbad IT-2363 Add Signature Support Entitlement Process
 2020-21-02 extbad IT-2497 Add Legacy Enterprise Support Entitlement Process

*/

public with sharing class CaseBusinessCriticalHandler {

    public static void handleCaseBusinessCritical(List<Case> newCases) {
        Set<Id> entitlementIds = new Set<Id>();
        Map<Id, Entitlement> entitlements = new Map<Id, Entitlement>();

        for (Case caze : newCases) {
            if (caze.Severity__c == '1' && !caze.Business_Critical__c) {
                caze.Business_Critical__c = true;
            }
            if (!caze.Business_Critical__c) {
                continue;
            }
            if (caze.EntitlementId != null) {
                entitlementIds.add(caze.EntitlementId);
            }
        }

        if (entitlementIds.size() > 0) {
            entitlements =
                    new Map<Id, Entitlement>([
                            SELECT Id, SlaProcessId
                            FROM Entitlement
                            WHERE Id IN :entitlementIds
                    ]);
        }

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        List<SlaProcess> slaProcesses = [
                SELECT Id
                FROM SlaProcess
                WHERE Name = :es.Premium_Entitlement_Process_Name__c OR Name = :es.Signature_Entitlement_Process_Name__c
                OR Name = :es.Enterprise_Entitlement_Process_Name__c
        ];
        List<String> slaIds = new List<String>();
        for (SlaProcess sla : slaProcesses) {
            slaIds.add(sla.Id);
        }

        //Set all Send_Email fields from the support offices for the new business critical and non-premium cases to true
        //so that an email will be sent from a workflow to the team leads and the support manager of the Support Office.
        //If the case is a Premium case or the Support Office is null the flag Business_Critical_Send_Email__c will be set
        //and en email will be sent to the Regional Team Lead or Regional Service Desk Manager role instead
        List<Support_Office__c> officesToUpdate = new List<Support_Office__c>();
        for (Case caze : newCases) {
            if (!caze.Business_Critical__c) {
                continue;
            }
            caze.Severity__c = '1';
            caze.Priority = 'Urgent';

            Entitlement entitlement = entitlements.get(caze.EntitlementId);

            if (caze.Support_Office__c == null
                    || (entitlement != null
                    && entitlement.SlaProcessId != null
                    && slaIds.contains(entitlement.SlaProcessId))) {
                caze.Business_Critical_Send_Email__c = true;
            }
        }
    }
}