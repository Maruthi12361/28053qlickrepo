/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * AddEditPartnerExpertise_Test
 * Description:
 * Create unit test for Partner Expertise Area object
 *
 * Change Log:
 * 2012-10-30   SAN     Development by SAN CR#5825
 * 2013-02-15   RDZ     CR# 6729 and 6833, making new relationship 1 to many between Partner Expertise Area - Solution Profiles
 *                      and Partner Expertise Area - QVM Product Data. Multiselect does not use QlikMarket_Applications__c and Solution_Profiles__c semicolon separated list.
 *                      We use the relation ship, partner expertise area id on QVM Products and Solution Profiles. 
 * 2013-11-12   CCE     Commented out CheckInsertDuplicatedPEAByQonnectManager test method as we no longer have the Custom: Qonnect Manager profile
 * 06.02.2017   RVA :   changing QT methods 
 *  09.03.2017 : Bala : included ORDER BY in fetching record type query
 * 26.12.2017  : Pramod Kumar V: Test class coverage
 */
@isTest
public class AddEditPartnerExpertise_Test {
    
    @testSetup static void setup() {
        QuoteTestHelper.createCustomSettings();
    }
        
    static testMethod void MultiSelectSaveTest() {
        test.startTest();
        
        // create test data
        /*
        Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
            Subsidiary__c = testSubs.id             
        );
        insert QTComp;
        */
        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
        //Get record type
        RecordType PARecordType = [select Id, Name from RecordType 
                                    where Name = 'Partner Account' and sobjecttype='Account'];// fix for query error
        
        //Create Acc record type Partner Acc (01220000000DOFzAAO) or OEM Partner Acc (01220000000DOG4AAO)
        Account acc = new Account(Name='TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId =PARecordType.Id);
        insert acc;
        
        
        QVM_Partner_Data__c qvmpartner = new QVM_Partner_Data__c(Partner_Account__c=acc.Id);
        insert qvmpartner;
        
        
        Contact c = new Contact();
        c.FirstName = 'Sophie';
        c.LastName = 'Grigson';
        c.accountId = acc.Id;
        insert c;
        
        
        Partner_Expertise_Area__c pea = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Sciences');
        insert pea;
              
        
        Solution_Profiles__c sp01 = new Solution_Profiles__c(Name='sptest', Account_Name__c = acc.Id);
        Solution_Profiles__c sp02 = new Solution_Profiles__c(Name='sptest', Account_Name__c = acc.Id);
        QVM_Product_Data__c qvmp01 = new QVM_Product_Data__c(Product_Name__c='qvmptest01', QVM_Partner__c=qvmpartner.Id, Partner_Expertise_Area_ID__c=pea.Id);
        QVM_Product_Data__c qvmp02 = new QVM_Product_Data__c(Product_Name__c='qvmptest02', QVM_Partner__c=qvmpartner.Id );
        
        List<SObject> soToInsert = new List<SObject>();
        soToInsert.add(sp01);
        soToInsert.add(sp02);       
        soToInsert.add(qvmp01);
        soToInsert.add(qvmp02);
        insert soToInsert;
                        
        
        
        
        ApexPages.StandardController std = new ApexPages.StandardController(pea);
        
        AddEditPartnerExpertiseController controller = new AddEditPartnerExpertiseController(std);

        //controller status before save and add or remove qlikmarket products or solutions profiles
        System.AssertEquals(1,controller.unselectedQlikMarkets.size()); //[qvmp02]
        System.AssertEquals(1,controller.selectedQlikMarkets.size()); // [qvmp01]
        System.AssertEquals(2,controller.unselectedSolutions.size()); //[sp01, sp02]
        System.AssertEquals(0,controller.selectedSolutions.size()); // []
        
        
        //Set qvmp01 as unselected when already was selected for the current partner expertise area
        //Partner expertise area id on qlik product data object should be set to blank
        //Set qvmp02 which does not belong to any partner expertise area on pea.id
        //we should finish with qvmp01.Partner_Expertise_Area__c = blank
        //and qvmp02.Partner_Expertise_Area__c = pea.Id
        
        //unselect qvmp01  from partner expertise area 
        for(Integer j=0; j<controller.selectedQlikMarkets.Size(); j++)
        {
            if(controller.selectedQlikMarkets[j].getValue() == qvmp01.id)
            {
                controller.selectedQlikMarkets.remove(j);
                controller.unselectedQlikMarkets.add(new SelectOption(qvmp01.id, qvmp01.Product_Name__c));
            }
        }
        //and select qvmp02 to belong to partner expertise area
        for(Integer j=0; j<controller.unselectedQlikMarkets.Size(); j++)
        {
            if(controller.unselectedQlikMarkets[j].getValue() == qvmp02.id)
            {
                controller.unselectedQlikMarkets.remove(j);
                controller.selectedQlikMarkets.add(new SelectOption(qvmp02.id, qvmp02.Product_Name__c));
            }
        }
                
        
        //Solution profile should set partner expertise area id attribute from blank to pea.id
        //controller.unselectedSolutions =  [sp01, sp02] as no one has been selected yet.
        //controller.selectedSolutions = []
        //We will add sp02 to selectedSolutions and remove it from unselected solutions
        //Remove from unselectedSolutions sp02 as has been selected.
        for(Integer j=0; j<controller.unselectedSolutions.Size(); j++)
        {
            if(controller.unselectedSolutions[j].getValue() == sp02.Id)
            {
                controller.unselectedSolutions.remove(j);
            }
        }
        controller.selectedSolutions.add(new SelectOption(sp02.Id, sp02.Name)); // [sp02]
         
        //controller.deletewithredirect();
        
        
        controller.save();
        
        System.assertEquals(1, controller.selectedQlikMarkets.size());
        System.assertEquals(1, controller.selectedSolutions.size());
        controller.backtoConnectProgram();
        controller.deletewithredirect();
        Boolean flag = controller.isTest;
        List<QVM_Product_Data__c> qvmlist = [Select Name, Id, Partner_Expertise_Area_ID__c, Product_Name__c from QVM_Product_Data__c where Partner_Expertise_Area_ID__c =:pea.Id];
        
        List<Solution_Profiles__c> splist = [Select Name, Id, Partner_Expertise_Area_ID__c from Solution_Profiles__c where Partner_Expertise_Area_ID__c =:pea.Id];
        System.assertEquals(1,qvmlist.size());
        System.assertEquals(1,splist.size()); 
        System.assertEquals(pea.Id,qvmlist[0].Partner_Expertise_Area_ID__c);
        System.assertEquals('qvmptest02', qvmlist[0].Product_Name__c);       
        System.assertEquals(pea.Id,splist[0].Partner_Expertise_Area_ID__c);
        System.assertEquals(sp02.Id,splist[0].Id);
       

        test.stopTest();
    }
    
    
    
    static testMethod void MultiSelectCancelTest() {
        test.startTest();
        // create test data
            /*
            Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
                Subsidiary__c = testSubs.id                
            );
            insert QTComp;
            */
            QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        //Get record type
        RecordType PARecordType = [select Id, Name from RecordType 
                                    where Name = 'OEM Partner Account' and sobjecttype='Account'];// fix for query error
        
        Account acc = new Account(Name='TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId=PARecordType.Id);
        insert acc;
        
        Contact c = new Contact();
        c.FirstName = 'Sophie';
        c.LastName = 'Grigson';
        c.accountId = acc.Id;
        insert c;
        
        Partner_Expertise_Area__c pea = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Sciences');
        insert pea;
        
        QVM_Partner_Data__c qvmpartner = new QVM_Partner_Data__c(Partner_Account__c=acc.Id);
        insert qvmpartner;      
        
        Solution_Profiles__c sp = new Solution_Profiles__c(Name='sptest');
        QVM_Product_Data__c qvmp01 = new QVM_Product_Data__c(Product_Name__c='qvmptest01', QVM_Partner__c=qvmpartner.Id);
        QVM_Product_Data__c qvmp02 = new QVM_Product_Data__c(Product_Name__c='qvmptest02', QVM_Partner__c=qvmpartner.Id);
        
        List<SObject> soToInsert = new List<SObject>();
        soToInsert.add(sp);
        soToInsert.add(qvmp01);
        soToInsert.add(qvmp02);
        insert soToInsert;
        
        ApexPages.StandardController std = new ApexPages.StandardController(pea);
        
        AddEditPartnerExpertiseController controller = new AddEditPartnerExpertiseController(std);
 
        
        
        SelectOption[] selectedQlikMarkets=new List<SelectOption>();
        SelectOption[] unselectedQlikMarkets=new List<SelectOption>();
        
        unselectedQlikMarkets.add(new SelectOption(qvmp01.id, qvmp01.Product_Name__c));
        selectedQlikMarkets.add(new SelectOption(qvmp02.id, qvmp02.Product_Name__c));
        SelectOption[] selectedSolutions=new List<SelectOption>();
        SelectOption[] unselectedSolutions=new List<SelectOption>();
        selectedSolutions.add(new SelectOption(sp.Id, sp.Name));
        
        controller.cancel();
        
        //If we cancel the selection to add new products to current expertise area we should get no products as selected and 2 products as unselected
        //for solution profiles we should get all as unselected as well.
        System.assertEquals(0, controller.selectedQlikMarkets.size());
        System.assertEquals(0, controller.selectedSolutions.size());
        
        
        List<QVM_Product_Data__c> qvmlist = [Select Name, Id, Partner_Expertise_Area_ID__c, Product_Name__c from QVM_Product_Data__c where Partner_Expertise_Area_ID__c =:pea.Id];
        
        List<Solution_Profiles__c> splist = [Select Name, Id, Partner_Expertise_Area_ID__c from Solution_Profiles__c where Partner_Expertise_Area_ID__c =:pea.Id];
        System.assertEquals(0,qvmlist.size());
        System.assertEquals(0,splist.size()); 
                
        System.assertEquals(controller.getExpertiseContacts().size(),2);

        test.stopTest();
    }    
   
   
    
    /*
    * Test method to check that two partner expertise area of same expertise area cannot be created.
    * We should not allow partner accounts to have 2 partner expertise of Communications.
    * Expertise Areas available are: 
            --None--
            Manufacturing &amp; High Tech
            Retail &amp; Services
            Energy &amp; Utilities
            Communications
            Life Sciences
            Healthcare
            Financial Services
            QlikView Mobile
    * We should have just one PartnerExpertiseArea by Expertise Area Type.
    */
  
    static testMethod void PEADontAllowAlreadyExistingExpertiseAreaTypeTest() {
        
        test.startTest();
        
        // create test data
        
        //Get record type
        RecordType PARecordType = [select Id, Name from RecordType 
                                    where Name = 'OEM Partner Account' and sobjecttype='Account'];// fix for query error
        User u = [SELECT Id, Name FROM User where Id= :System.Userinfo.getUserId()];
        /*
        Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
            Subsidiary__c = testSubs.id             
        );
        */
        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
         RecordType rType = [Select id, DeveloperName From RecordType where DeveloperName ='End_User_Account'];
            Account acc = QuoteTestHelper.createAccount(QTComp , rType, 'ForTest');
            insert acc;
        acc.RecordTypeId = PARecordType.Id;
        update acc;
                
        Contact c = new Contact();
        c.FirstName = 'Sophie';
        c.LastName = 'Grigson';
        c.accountId = acc.Id;
        insert c;
        
        Partner_Expertise_Area__c pea0 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Life Sciences');
        
        Partner_Expertise_Area__c pea1 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Communications');
        
        Partner_Expertise_Area__c pea2 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Healthcare');
        
        
        List<SObject> soToInsert = new List<SObject>();
        soToInsert.add(pea0);
        soToInsert.add(pea1);
        soToInsert.add(pea2);
        insert soToInsert;
        
        Partner_Expertise_Area__c pea3 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Communications');
        try{
            insert pea3;
        }
        catch(DMLException ex)
        {
            //as we expected
            System.assert(true, 'We expect this to fail as we cannot add two PEA of type communication');
        }
                
        test.stopTest();
    }  


        static testMethod void CheckInsertDuplicatedPEAByOperationAdministrator()
    {
        System.runAs(QTTestUtils.createMockUserForProfile('Operations Administrator'))
        {
            PEADontAllowAlreadyExistingExpertiseAreaTypeTest();
        }
    }
    
    
   static testMethod void MethodsTest()
    {
    
        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
        //Get record type
        RecordType PARecordType = [select Id, Name from RecordType 
                                    where Name = 'Partner Account' and sobjecttype='Account'];// fix for query error
        
        //Create Acc record type Partner Acc (01220000000DOFzAAO) or OEM Partner Acc (01220000000DOG4AAO)
        Account acc = new Account(Name='TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId =PARecordType.Id);
        insert acc;
        
        
        QVM_Partner_Data__c qvmpartner = new QVM_Partner_Data__c(Partner_Account__c=acc.Id);
        insert qvmpartner;
        
        
        Contact c = new Contact();
        c.FirstName = 'test';
        c.LastName = 'test';
        c.accountId = acc.Id;
        insert c;
        
        
        Partner_Expertise_Area__c pea = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Sciences',Expertise_Start_Date__c=System.today(),
                                                                     Expertise_Education_Requirement__c=true,Expertise_Marketing_Requirement__c=true);
        insert pea;
        
       //Partner_Expertise_Area__c pea1=new Partner_Expertise_Area__c (Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id);
       // insert pea1;
       ApexPages.StandardController std = new ApexPages.StandardController(pea);
        
        AddEditPartnerExpertiseController controller = new AddEditPartnerExpertiseController(std);
        controller.deletewithredirect();
        
        Partner_Expertise_Area__c pea1 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Sciences',Expertise_Start_Date__c=System.today(),
                                                                     Expertise_Education_Requirement__c=true,Expertise_Marketing_Requirement__c=true);
        insert pea1;
        ApexPages.StandardController std1 = new ApexPages.StandardController(pea1);
        
        AddEditPartnerExpertiseController controller1 = new AddEditPartnerExpertiseController(std1);
        controller1.save();
        
    }
//Commented out as we no longer have the Custom: Qonnect Manager profile    
/*  static testMethod void CheckInsertDuplicatedPEAByQonnectManager()
    {
        try{
            System.runAs(QTTestUtils.createMockUserForProfile('Custom: Qonnect Manager'))
            {
                PEADontAllowAlreadyExistingExpertiseAreaTypeTest();
                System.assert(false,'Error: Qonnect Manager should have just read access to PEA');
            }
        }
        catch(DMLException ex){
            System.assert(true,'We expect this to happen as Qonnect Manager should have just read access to PEA');
        }
        
    }*/



}