/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_SupportTechspertThursdaysTest {
    
    @isTest static void SupportTechspertThursdaysTest1() {
        // Implement test code

		List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
		if(searchSettings.Size() == 0)
		{
			QS_SearchSettings__c ss = new QS_SearchSettings__c();
			ss.Name = 'SearchSettings';
			ss.Page_Size__c = 20;
			ss.Jive_DefaultPlaces__c ='/places/217522';
			ss.Jive_EndPoint__c ='https://community.qlik.com/api/core/v3/search/contents?startIndex=index&callback=?&count=pageSize&filter=search(searchTerms)';
			ss.Instance_Name__c = 'cs14';
			ss.Jive_EndPoint_Personas__c = 'https://community.qlik.com/api/core/v3/contents/trending?&callback=?&count=pageSize&startIndex=index&filter=place(placeTerms)';
			insert ss;
		}

        setupKB();
        QS_SupportTechspertThursdays tempObj = new QS_SupportTechspertThursdays();      
        
        
    }
    
    
	public static void setupKB() // inserts content and download history
    {
    	List<Basic__kav> basicKavs = new List<Basic__kav>(); 

        Basic__kav basicKav1 = TestDataFactory.createBasicKnowledgeArticle('STT Webinar', 'STT Webinar', 'first-test-apex-Basic', 'en_US');
        basicKav1.IsVisibleInCsp = true;
        basicKav1.IsVisibleInPkb = true;
        basicKav1.IsVisibleInPRM = true;
        basicKavs.add(basicKav1);

        Basic__kav basicKav2 = TestDataFactory.createBasicKnowledgeArticle('STT Webinar 2', 'STT Webinar 2', 'second-test-apex-Basic', 'en_US');
        basicKav2.IsVisibleInCsp = true;
        basicKav2.IsVisibleInPkb = true;
        basicKav2.IsVisibleInPRM = true;
        basicKavs.add(basicKav2);

        insert basicKavs;

        Set<ID> basicKavIds = new Set<ID>();
        basicKavIds.Add(basicKav1.ID);
        basicKavIds.Add(basicKav2.ID);

        basicKavs = [SELECT KnowledgeArticleId, ArticleNumber FROM Basic__kav WHERE Id in :basicKavIds];

        KbManagement.PublishingService.publishArticle(basicKavs[0].KnowledgeArticleId, true);
        KbManagement.PublishingService.publishArticle(basicKavs[1].KnowledgeArticleId, true);
   }     
    
}