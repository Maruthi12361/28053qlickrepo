/****************************************************************************************************
*   IT-79
*   CaseAttachmentPurge
*   Description: Test class for CaseAttachmentPurge
*   IT-467 :Custom settings records added to maintain code coverage for CaseAttachmentPurge CRW
*
*   Change log:
*   
*   2018-07-09  ext_vos     CHG0033954: update list of Case Record Types.
*   2020-01-29  ext_bad     IT-2347: Added Closure Code 'License Delivery' to exceptions
*
****************************************************************************************************/    
@IsTest
private class CaseAttachmentsPurgeTest  {
    static testMethod void CaseAttachmentsPurge() {
        Account a = new Account(Name = 'Cloud Environment Test');
        insert a;
        Contact con = new Contact(LastName = 'testcontact', LeadSource = 'TestLeadSource', Contact_Status__c = 'TestConStatus', AccountId = a.Id);
        insert con;

        List<RecordType> entStdRecType = [SELECT Id FROM RecordType WHERE DeveloperName= 'QlikTech_Master_Support_Record_Type' AND SobjectType = 'Case'];
        if (entStdRecType.isEmpty()) {
            System.assert(false, 'Missing record type!');
        }
        Case cse = new Case(Type = 'Electrical', AccountId = a.Id, ContactId = con.Id, Status = 'Closed', RecordTypeId = entStdRecType[0].Id);
        insert cse;
        Case cse2 = new Case(Type = 'Electrical', AccountId = a.Id, ContactId = con.Id, Status = 'In Progess', RecordTypeId = entStdRecType[0].Id);
        insert cse2;
        Case cse3 = new Case(Type = 'Electrical', AccountId = a.Id, ContactId = con.Id, Status = 'Closed', RecordTypeId = entStdRecType[0].Id, Closure_Code__c = 'License Delivery');
        insert cse3;
        //Attachment
        Attachment attach = new Attachment();      
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.Body = bodyBlob;
        attach.ParentId = cse.Id;
        insert attach;

        Attachment attach2 = new Attachment();      
        attach2.Name = 'Unit Test Attachment';
        Blob bodyBlob2 = Blob.valueOf('Unit Test Attachment Body');
        attach2.Body = bodyBlob2;
        attach2.ParentId = cse2.Id;
        insert attach2;

        Attachment attach3 = new Attachment();
        attach3.Name = 'Unit Test Attachment';
        Blob bodyBlob3 = Blob.valueOf('Unit Test Attachment Body');
        attach3.Body = bodyBlob3;
        attach3.ParentId = cse3.Id;
        insert attach3;

        QTCustomSettings__c settings1 = new QTCustomSettings__c();
        settings1.Name = 'Default';
        settings1.CaseAttachmentPurgeEmail__c = 'test@test.com';
        settings1.QlikNoReplyEmailAddress__c = 'no-reply@test.com';
        insert settings1;

        List<String> caseIds = new List<String>{cse.Id, cse2.Id, cse3.Id};
        List<Attachment> testAtt = [SELECT Id FROM Attachment WHERE ParentId IN :caseIds];
        System.assertEquals(3, testAtt.size());

        Test.startTest();
        CaseAttachmentPurge.TEST_ERROR_ID = attach2.Id;
        Database.executeBatch(new CaseAttachmentPurge(), 5);
        Test.stopTest();
        System.assert(CaseAttachmentPurge.TEST_EMAIL_SENDING);

        testAtt = [SELECT Id FROM Attachment WHERE ParentId =: cse2.Id];
        System.assert(!testAtt.isEmpty());

        testAtt = [SELECT Id FROM Attachment WHERE ParentId =: cse.Id];
        System.assert(testAtt.isEmpty());

        testAtt = [SELECT Id FROM Attachment WHERE ParentId =: cse3.Id];
        System.assert(!testAtt.isEmpty());
    }
}