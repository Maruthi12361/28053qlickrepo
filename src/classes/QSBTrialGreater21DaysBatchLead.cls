/**
  20200521 : CCE DGM-55 FURRevamp This class is called from the QSBTrialGreater21Days scheduled class and checks to see if any 
             Leads have the QSB Trial field set for more than 21 days without the Overall Follow-Up Required field being set.
**/
global class QSBTrialGreater21DaysBatchLead implements Database.Batchable<sObject>, Database.stateful {
    global final String rtStandardLead = '01220000000DOzw';
    global final String rtAcademicProgramLead = '012D0000000KBYn';
    global String query;
    global QSBTrialGreater21DaysBatchLead() {
        query = 'SELECT Id, QSB_Trial__c, QSB_Trial_Start_Date__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c, Trial_QSB_Trial_greater_21__c FROM Lead'
        + ' WHERE LastModifiedDate = LAST_N_DAYS:30 AND (RecordTypeId = \'' + rtStandardLead + '\' OR RecordTypeId = \'' + rtAcademicProgramLead + '\')'
        + ' AND QSB_Trial__c = true AND Overall_Follow_up_Required__c = false AND QSB_Trial_Start_Date__c != null AND QSB_Trial_Start_Date__c != LAST_N_DAYS:21'
        + ' AND IsConverted = false AND IsDeleted = false';

        if (test.isRunningTest()) {
            query = 'SELECT Id, QSB_Trial__c, QSB_Trial_Start_Date__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c, Trial_QSB_Trial_greater_21__c FROM Lead'
                + ' WHERE Firstname = \'Chris\' and LastName = \'QSBTrialGreater21DaysBatchLead\'';
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Database.QueryLocator ql = Database.getQueryLocator(query);
        return ql;
    }

    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        for (Lead l : scope) {
            l.QSB_Trial__c = false; 
            l.Trial_QSB_Trial_greater_21__c = true;            
        }
        System.debug('QSBTrialGreater21DaysBatchLead: scope size = ' + scope.size());
        Database.update(scope, false);
    }

    global void finish(Database.BatchableContext BC) {
    }
}