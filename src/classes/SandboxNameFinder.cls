/*
* File SandboxNameFinder
    * @description : Unit test for SandboxNameFinder
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       06.04.2018   Pramod Kumar V      Created Class

*/
public class SandboxNameFinder {
    Public String SandboxNameHelper() {
        String lSandBoxName = UserInfo.getUserName();
        String gSandboxName;
        if (lSandBoxName.contains('com.')) {
            List <String> lSandBoxSplit = lSandBoxName.split('com.', 2);
            gSandboxName = lSandBoxSplit[1];
        } else {
            gSandboxName = 'test';
        }
        return gSandboxName ;
    }
}