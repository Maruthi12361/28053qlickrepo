/**************************************************
* CR# 97533 – Relayware triggers
* Change Log:
* 2016-10-31 BAD Initial creation of test class 
**************************************************/
@isTest
private class AccountPartnerPortalRelaywareHandlerTest {

    @testSetup static void setup() {
        QuoteTestHelper.createCustomSettings();
    }

    @isTest
    static void test_Account_portal_access_Relayware_approved() {
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        //we don't use this instance, but create it to execute the class constructor
        AccountPartnerPortalRelaywareHandler tempObj = new AccountPartnerPortalRelaywareHandler();

        System.RunAs(mockSysAdmin) {
            //CODE FOR DEV/PROD
            //Account acc = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
            //system.assert(acc.Id != null);

            //CODE FOR QA
            Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
            insert subs;
            QlikTech_Company__c qCompany = QuoteTestHelper.createQlickTechCompany(subs.id);
            insert qCompany;
            RecordType rType = [Select id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
            Account acc = QuoteTestHelper.createAccount(qCompany, rType, 'ForTest');
            insert acc;
            system.assert(acc.Id != null);
            ////

            Test.startTest();

            Semaphores.TriggerHasRun(1);
            Semaphores.SetAllSemaphoresToFalse();

            //Enable Account as Partner
            System.debug('BAD check Navision Init' + acc.Navision_Status__c);
            acc.Navision_Status__c = 'Partner';
            update acc;

            Account accTemp = [select Navision_Status__c, Synch_with_Relayware__c,Account_Approval_Status__c from Account where id = :acc.id];
            System.debug('BAD check AccountID' + accTemp.id);
            System.debug('BAD check SyncRW' + accTemp.Synch_with_Relayware__c);
            System.debug('BAD check NavisionStatus' + accTemp.Navision_Status__c);
            system.assertEquals(true, accTemp.Synch_with_Relayware__c);
            system.assertEquals(true, accTemp.Account_Approval_Status__c);

            Test.stopTest();
        }
    }


    @isTest
    static void test_Account_portal_access_Relayware_terminated() {
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {
            Account acc = QTTestUtils.createMockAccount('Test Account Terminated by field Terminated', mockSysAdmin);
            system.assert(acc.Id != null);

            Test.startTest();

            Semaphores.TriggerHasRun(1);

            //Set account to terminated through field Terminated
            acc.Terminated__c = true;
            update acc;

            Account accTemp = [select Account_Approval_Status__c from Account where id = :acc.id];
            system.assertEquals(false, accTemp.Account_Approval_Status__c);

            Test.stopTest();
        }
    }


    @isTest
    static void test_Account_portal_access_Relayware_terminated_ERPStatus() {
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {
            Account acc = new Account();
            acc.Name = 'Test Account Terminated by field ERPStatus';
            acc.Navision_Status__c = 'Partner';
            insert acc;
            system.assert(acc.Id != null);

            Test.startTest();

            Semaphores.TriggerHasRun(1);

            //Set account to terminated through field ERPStatus
            acc.Navision_Status__c = 'Customer';
            update acc;

            Account accTemp = [select Account_Approval_Status__c from Account where id = :acc.id];
            system.assertEquals(false, accTemp.Account_Approval_Status__c);

            Test.stopTest();
        }
    }


}