/**********************************************************************************************
    *Test Class for Deal Split Opprotunities.
    *Test Deal split close
    *Test Deal Split parent's split percentage as cumulative split percentages of children
    *Test children to Closed Won if Parent is Closed won
    *Test Deal Split children amount when Split percentage is changed
    
    *Test class covers triggers lastModifiedUpdate, Opportunity_WhenEISCreatesOpps and 
     OpportunityIfPartnerDealRecordPSMAtClosedWon
    
        Changelog:
            2013-09-10 MTM      CR# 8535     Initial development
            2014-02-26 MTM      CR# 11156    Quick Add field for Navision Invoice Number in Child opps
            2014-02-20 MTM      CR# 10922    Add new field in the Sales Credit Area

            2014-06-04 TJG      Fix test error
                                System.DmlException: Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY, 
                                Record Type ID: this ID value isn't valid for the user: 01220000000J1KRAA0: [RecordTypeId]
            2015-01-28 MTM      CR# 10000 Deal split related

            2016-06-08 TJG      Fix too many soql query errors during QT Clean Up project

            2016-06-14 TJG      Migrated to OpportunityLastModifiedHandlerTest for QT Clean Up
			2017-03-24 Rodion Vakulovskyi 
            2017-03-29 Roman Dovbush : commented out createCustomSettings(), it was duplicated.
			2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring 
***********************************************************************************************/    
@isTest
public class TestDealSplitClose 
{    
    //static final String OppRecordTypeId = '012D0000000KEKO'; //QTNS '012f0000000CuglAAC'; //Qlikbuy CCS Standard II on QTNS  2014-06-04
  /*
    public static testMethod void TestDealSplitCloseCheckDealSplitStage()
    {
        InsertCustomSettings();
        resetSemaphores();
        SetUpData();
        
        Test.startTest();
        Semaphores.DealSplitUpdateHasRun = false;
        dsParent.StageName = 'Deal Split-Closed Won';
        update dsParent;
        
        dsChild1 = [select StageName, Amount from Opportunity where Id = :dsChild1.Id];
        System.assertEquals(dsChild1.StageName, 'Closed Won');           
        System.assertEquals(dsChild1.Amount, 10000);
        
        dsParent = [select Deal_Split_Percentage__c, ForecastCategoryName from Opportunity where Id = :dsParent.Id];           
        System.assertEquals(dsParent.ForecastCategoryName, 'Omitted');
        System.assertEquals(dsParent.Deal_Split_Percentage__c, 10);    

        Semaphores.DealSplitUpdateHasRun = false;
        dsParent.StageName = 'OEM - Closed Won';
        update dsParent;        

        dsChild1 = [select StageName, Amount from Opportunity where Id = :dsChild1.Id];
        System.assertEquals(dsChild1.StageName, 'OEM - Closed Won');           
        System.assertEquals(dsChild1.Amount, 10000);
        
        Test.stopTest();
    }
    
    public static testMethod void TestDealSplitCheckDealSplitChildTeamMember()
    {
        InsertCustomSettings();
        resetSemaphores();
        SetUpData();
        
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {    
            Test.startTest();
            
            dsChild1 = [select Id, CreatedById from Opportunity where Id = :dsChild1.Id];
            
            OpportunityTeamMember teamMember = [select UserId, TeamMemberRole from OpportunityTeamMember where OpportunityId =:dsChild1.Id];
            
            System.assertEquals(dsChild1.CreatedById,teamMember.UserId);
            System.assertEquals(teamMember.TeamMemberRole, 'Sales Rep');
            
            Test.stopTest();
        }
    }
    
    public static testMethod void TestDealSplitCheckDealSplitChildAmount()
    {
        InsertCustomSettings();
        resetSemaphores();
        SetUpData();
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {                      
            Test.startTest();
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 10000);
            Semaphores.DealSplitUpdateHasRun = false;
            dsParent.Amount = 500000;
            update dsParent;
            
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 50000);
            
            Test.stopTest();
        }
    }*/
    
    public static testMethod void TestDealSplitCheckDealSplitPercentageOnAddingNewChild()
    {
        InsertCustomSettings();
        //QuoteTestHelper.createCustomSettings(); // duplicate: we have it in InsertCustomSettings()
        resetSemaphores();
        user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.createMockAccount('Deal Split Test Account', user);
        dsParent = createOpportunity('DSTests', 'DS Parent test Opp', 'New Customer', 'Direct', 'Deal Split Create', OppRecordTypeId, 'GBP', user, account);
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            Test.startTest();
            
            Semaphores.DealSplitUpdateHasRun = false;
            Semaphores.OpportunityTriggerBeforeUpdate = false;
       		Semaphores.OpportunityTriggerAfterUpdate = false;
            Semaphores.OpportunityTriggerBeforeInsert = false;
       		Semaphores.OpportunityTriggerAfterInsert = false;
            Opportunity newDSChild = QTTestUtils.createOpportunity('Child2 Tests', 'DS Child2 test Opp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account, dsParent, 50);            
            
            System.assertEquals(newDSChild.Deal_Split_Percentage__c, 50);
            
            dsParent = [select Deal_Split_Percentage__c from Opportunity where Id = :dsParent.Id];           
            
            System.assertEquals(dsParent.Deal_Split_Percentage__c, 50);
            
            Test.stopTest();
        }
        
    }
    /*    
    public static testMethod void TestDealSplitCheckAmountOnAddingNewChild()
    {
        InsertCustomSettings();
        resetSemaphores();
        SetUpData();
        
        Opportunity newDSChild = QTTestUtils.createOpportunity('Child2 Tests', 'DS Child2 test Opp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account, dsParent, 50);
        //System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            Test.startTest();
            
            System.assertEquals(newDSChild.Deal_Split_Percentage__c, 50);
            
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 10000);
            
            newDSChild = [select Amount from Opportunity where Id = :newDSChild.Id];
            System.assertEquals(newDSChild.Amount, 50000);
            
            Test.stopTest(); 
        }
    }
   
    public static testMethod void TestDealSplitCheckDealSplitPercentage()
    {
        InsertCustomSettings();
        resetSemaphores();
        SetUpData();
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            Test.startTest();
            dsChild1.Deal_Split_Percentage__c = 30;
            Semaphores.DealSplitUpdateHasRun = false;
            update dsChild1;
            
            Opportunity dsParent1 = [select Id, Deal_Split_Percentage__c from Opportunity where Id = :dsParent.Id];           
            
            System.assertEquals(dsParent1.Deal_Split_Percentage__c, 30);
            
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 30000);
            
            Test.stopTest();
        }
    }
    
    public static testMethod void TestDealSplitCheckChildNavisionInvoiceNumber()
    {
        InsertCustomSettings();
        resetSemaphores();
        SetUpData();
            
        Test.startTest();
        dsParent.Navision_Invoice_Number__c = '1234567';
        Semaphores.DealSplitUpdateHasRun = false;
        update dsParent;
        
        dsChild1 = [select Navision_Invoice_Number__c from Opportunity where Id = :dsChild1.Id];
        System.assertEquals(dsChild1.Navision_Invoice_Number__c, '1234567');
        
        Test.stopTest();        
    } 
    
    public static testMethod void TestAccountISEROnOpportunityClose()
    {
        InsertCustomSettings();

        user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.CreateAccountForInternalUsers('Deal Split Test Account', user);        
        
        dsParent = createOpportunity('DSTests', 'DS Parent test Opp', 'New Customer', 'Direct', 'Deal Split Create', OppRecordTypeId, 'GBP', user, account);
        QTTestUtils.createMockSOI(dsParent, true);                                      
        dsParent.StageName = 'Deal Split-Closed Won';
        resetSemaphores();
        Semaphores.OppUpdateAccOwnerNameHasRun = false;
        update dsParent;
        
        Test.startTest();
        
        Opportunity dsParent1 = [select Id, 
                                        Account_ISER__c, 
                                        Account_ISER_at_Closed_Won__c, 
                                        Account_ISER_at_Closed_Won_ID__c 
                                from Opportunity 
                                where Id = :dsParent.Id];           
        
        System.assertEquals(dsParent1.Account_ISER__c, User.Id);
        System.assertEquals(dsParent1.Account_ISER_at_Closed_Won__c, 'Testing');            
        System.assertEquals(dsParent1.Account_ISER_at_Closed_Won_ID__c, User.Id);
        Test.stopTest();        
    }*/
  
    private static void SetUpData()
    {   
        QuoteTestHelper.createCustomSettings();
        user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.CreateAccountForInternalUsers('Deal Split Test Account', user);                                      
        dsParent = createOpportunity('DSTests', 'DS Parent test Opp', 'New Customer', 'Direct', 'Deal Split Create', OppRecordTypeId, 'GBP', user, account);
        Semaphores.DealSplitUpdateHasRun = false;
        QTTestUtils.CreateMockSOI(dsParent, true);
        dsChild1 = QTTestUtils.createOpportunity('Child1 Tests', 'DS Child1 test Opp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account, dsParent, 10);
        System.assertEquals(dsChild1.StageName, 'Goal Identified');           
    }
    
    private static Opportunity createOpportunity(String sShortDescription, String sName, String sType, String sRevenueType, String sStage, Id OppRecordId, String sCurrencyIsoCode, User user, Account acc)
    {
        Opportunity opp = New Opportunity (
                Short_Description__c = sShortDescription,
                Name = sName,
                Type = sType,
                Revenue_Type__c = sRevenueType,
                CloseDate = Date.today(),
                StageName = sStage, 
                RecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id, 
                CurrencyIsoCode = sCurrencyIsoCode,
                AccountId = acc.Id,
                Split_Opportunity__c = true,
                Signature_Type__c = 'Digital Signature',
                Amount = 100000
                ); 
           insert opp;
           return opp;
    }
    static User user;
    static Account account;
    static Contact contact;
    static Opportunity dsParent;
    static Opportunity dsChild1;
    static Opportunity dsChild2;

    public static void InsertCustomSettings()
    {
		System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
            settings.Name = 'NSSettingsDetail';
            settings.Qlikbuy_II_Opp_Record_Type__c = OppRecordTypeId; // Qlikbuy_II Opp Record Type was deleted
            settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
            settings.UserRecordTypeIdsToExclude__c = '005D0000002URF0';
            settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
            insert settings;
    
            QTCustomSettings__c settings2 = new QTCustomSettings__c();
            settings2.Name = 'Default';
            settings2.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
            settings2.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
            settings2.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
            settings2.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
            insert settings2;
        }
        QuoteTestHelper.createCustomSettings();

    }
    static Id OppRecordTypeId
    {
        get
        {
           return QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
        }
    }
    static void resetSemaphores()
    {
        //Semaphores.TriggerHasRun('OppUpdateContactSOIisAfterisInsert');
        //Semaphores.TriggerHasRun('OppUpdateContactSOIisAfterisUpdate');
        //Semaphores.TriggerHasRun('OppUpdateContactSOIisBeforeisInsert');
        //Semaphores.TriggerHasRun('OppUpdateContactSOIisBeforeisUpdate');
        //Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisAfterisInsert');
        //Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisBeforeisInsert');
        //Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisAfterisUpdate');
        //Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisBeforeisUpdate');
        Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery');
        Semaphores.TriggerHasRun('OppUpdateContactSOI');
        Semaphores.OppUpdateContactSOI_IsAfter = true;
        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;        
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
        Semaphores.Opportunity_ManageForecastProductsIsInsert = true;
        Semaphores.Opportunity_ExecDealsVisibility_Insert = true;
        Semaphores.Opportunity_ExecDealsVisibility_Update = true;
        ApexSharingRules.TestingOpportunityShare = false;
        Semaphores.DealSplitUpdateHasRun = false;        
    }
}