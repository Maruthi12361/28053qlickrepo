/*
BSl-1224 shubham Gupta 9/1/2019
Test class to cover NewCustomerHandler and DunsHandlerAccount
*/

@isTest
private class Test_DunsNewCustomerHandler {
	
	@isTest static void dunsHandlerCheck() {
		RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
       QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);

       Account customeracc = QuoteTestHelper.createAccount(QtComp, recTypeAccEndUser , 'test');
       customeracc.Name = 'Test Duns 1';
       customeracc.Domestic_Ultimate_DUNS__c = '12345';
       customeracc.AccountStatus__c = 'Customer';
       insert customeracc;

       Account newaccount = QuoteTestHelper.createAccount(QtComp, recTypeAccEndUser , 'test');
       newaccount.Name = 'Test Duns 2';
       insert newaccount;

       Test.startTest();
       Semaphores.CustomAccountTriggerBeforeUpdate = false;

       newaccount.Domestic_Ultimate_DUNS__c = '12345';
       Update newaccount;

       List<Account> acc = [select id,AccountStatus__c from Account where id =:newaccount.id];
       System.assertEquals(acc[0].AccountStatus__c,customeracc.AccountStatus__c);

       Test.stopTest();


	}
	
    @isTest static void newCustomerCheck() {
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        insert subs;
       
       QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
       insert QTcomp;

       Account accountone = QuoteTestHelper.createAccount(QtComp, recTypeAccEndUser , 'test');
       accountone.Name = 'Test Duns 1';
       accountone.Domestic_Ultimate_DUNS__c = '12345';
       insert accountone;

       Account accounttwo = QuoteTestHelper.createAccount(QtComp, recTypeAccEndUser , 'test');
       accounttwo.Name = 'Test Duns 2';
       accounttwo.Domestic_Ultimate_DUNS__c = '12345';
       insert accounttwo;

       QuoteTestHelper.createCustomSettings();

       Opportunity oppCreatedForUpdate1 = QuoteTestHelper.createOpportunity(accountone, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        oppCreatedForUpdate1.Name = 'test duns1 opp';

        Opportunity oppCreatedForUpdate2 = QuoteTestHelper.createOpportunity(accounttwo, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        oppCreatedForUpdate2.Name = 'test duns2 opp';

        
        Test.startTest();
        Semaphores.OpportunityTriggerBeforeInsert = false;
        
        insert oppCreatedForUpdate1;

        Semaphores.OpportunityTriggerBeforeInsert = false;
        insert oppCreatedForUpdate2;

        List<Account> accts = [select id,AccountStatus__c from Account where id =:accountone.id OR id=:accounttwo.id];
        system.assertEquals('Prospect',accts[0].AccountStatus__c);
        system.assertEquals('Prospect',accts[1].AccountStatus__c);

        Semaphores.OpportunityTriggerAfterUpdate = false;

        oppCreatedForUpdate1.stageName = 'Closed Won';
        oppCreatedForUpdate1.Included_Products__c = 'Qlik Sense';
        update oppCreatedForUpdate1;

        List<Account> acc = [select id,AccountStatus__c from Account where id =:accounttwo.id];
        system.assertEquals('Customer',acc[0].AccountStatus__c);

        List<Opportunity> opp = [select id,New_Customer__c from Opportunity where id =:oppCreatedForUpdate2.id];
        system.assertEquals(false,opp[0].New_Customer__c);

        Test.stopTest();

    }

    @isTest static void AccountStatusCheck() {
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        insert subs;
       
       QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
       insert QTcomp;

       Account accountone = QuoteTestHelper.createAccount(QtComp, recTypeAccEndUser , 'test');
       accountone.Name = 'Test Duns 1';
       insert accountone;

       QuoteTestHelper.createCustomSettings();

       Opportunity oppCreatedForUpdate1 = QuoteTestHelper.createOpportunity(accountone, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        oppCreatedForUpdate1.Name = 'test duns1 opp';

      Test.startTest();
        Semaphores.OpportunityTriggerBeforeInsert = false;
        insert oppCreatedForUpdate1;

        accountone.Domestic_Ultimate_DUNS__c = '8796543';
              Semaphores.CustomAccountTriggerBeforeUpdate = false;
              Update accountone;

       List<Account> acc = [select id,AccountStatus__c from Account where id =:accountone.id];
       System.assertEquals(acc[0].AccountStatus__c,'Prospect');

       Test.stopTest();
   }
	
	
}