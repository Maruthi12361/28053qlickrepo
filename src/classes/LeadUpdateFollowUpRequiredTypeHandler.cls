/*****************************************************************************************************************
Change Log:

20150106	CCE     Initial development CR# 16194 Update Lead Lifecycle Process - https://eu1.salesforce.com/a0CD000000kld2k
					Create trigger:
					"If Overall Follow-Up Required is true, then update lead status to Follow-Up Required
					If lead status was Follow-Up Rejected or Follow-Up Disqualified, then update Follow-Up Required Type to Revived
					Otherwise update Follow-Up Required Type to New"
                    NOTE: Do not add a semaphore to this trigger as it will stop it working. The trigger tests for
                    the "Overall: Follow-up Required" changing and Salesforce rerunning the triggers (see Salesforce Order of Execution document)
20150720    CCE     CR# 49405 - https://eu1.salesforce.com/a0CD000000vX8rp Clear Follow Up Rejected and Disqualified Reasons
20160422    CCE     CR# 82611 - https://eu1.salesforce.com/a0CD000000xSrSI Velocify Lead Lifecycle Changes - removing "Not Follow-Up Required Reason" field.
2017-10-25 AYS BMW-402 : Migrated from UpdateFollowUpRequiredType.trigger.
20200303   CCE    To be deactivated/removed as part of CHG0037726 DGM-55 FUR Revamp 2020
******************************************************************************************************************/
public class LeadUpdateFollowUpRequiredTypeHandler {
	/*public LeadUpdateFollowUpRequiredTypeHandler() {
		
	}
	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld, Boolean isUpdate) {

		System.Debug('LeadUpdateFollowUpRequiredTypeHandler: Starting');
    	
	    for (Integer i = 0; i < triggerNew.size(); i++)
		{
	        //if ((!isUpdate || triggerOld[i].Overall_Follow_up_Required__c != true) && triggerNew[i].Overall_Follow_up_Required__c == true && triggerNew[i].Status != 'Follow-Up Required')
	        if (((!isUpdate || triggerold[i].Event_Follow_Up_Required__c != true) && triggernew[i].Event_Follow_Up_Required__c == true && triggerNew[i].Status != 'Follow-Up Required') ||
               ((!isUpdate || triggerold[i].Existing_Follow_Up_Required__c != true) && triggernew[i].Existing_Follow_Up_Required__c == true && triggerNew[i].Status != 'Follow-Up Required') ||
               ((!isUpdate || triggerold[i].New_Follow_Up_Required__c != true) && triggernew[i].New_Follow_Up_Required__c == true && triggerNew[i].Status != 'Follow-Up Required'))
            {
	            System.Debug('LeadUpdateFollowUpRequiredTypeHandler: new Status = ' + triggerNew[i].Status + ' FU Required Type = ' + triggerNew[i].Follow_Up_Required_Type__c);
	            if ((triggerNew[i].Status == 'Follow-Up Rejected') || (triggerNew[i].Status == 'Follow-Up Disqualified'))
	            {
	            	triggerNew[i].Follow_Up_Required_Type__c = 'Revived';
	            }
	            else
	            {
	            	triggerNew[i].Follow_Up_Required_Type__c = 'New';
	            }
	            triggerNew[i].Follow_Up_Rejected_Reason__c = null; //if we're changing the Status to 'Follow-Up Required' then we don't want this set
	            triggerNew[i].Follow_Up_Disqualified_Reason__c = null; //if we're changing the Status to 'Follow-Up Required' then we don't want this set
	            //triggerNew[i].Not_Follow_Up_Required_Reason__c = null; //if we're changing the Status to 'Follow-Up Required' then we don't want this set
	            triggerNew[i].Status = 'Follow-Up Required';
	        }
	        if (isUpdate) {
	            System.Debug('LeadUpdateFollowUpRequiredTypeHandler: isUpdate');
	            //if ((triggerOld[i].Status == 'Follow-Up Rejected') && (triggerNew[i].Status == 'Follow-Up Rejected') && (triggerOld[i].Follow_Up_Rejected_Reason__c == 'Not Follow-Up Required') && (triggerNew[i].Follow_Up_Rejected_Reason__c != 'Not Follow-Up Required')) {
	            //    triggerNew[i].Not_Follow_Up_Required_Reason__c = null;
	            //}
	            //else if ((triggerOld[i].Status == 'Follow-Up Rejected') && (triggerNew[i].Status != 'Follow-Up Rejected')) {
	            if ((triggerOld[i].Status == 'Follow-Up Rejected') && (triggerNew[i].Status != 'Follow-Up Rejected')) {
	                triggerNew[i].Follow_Up_Rejected_Reason__c = null;
	            //    triggerNew[i].Not_Follow_Up_Required_Reason__c = null;
	            }
	            else if ((triggerOld[i].Status == 'Follow-Up Disqualified') && (triggerNew[i].Status != 'Follow-Up Disqualified')) {
	                triggerNew[i].Follow_Up_Disqualified_Reason__c = null;
	            }
	        }
	    }
	   
	    System.Debug('LeadUpdateFollowUpRequiredTypeHandler: Finishing');	
	}*/
}