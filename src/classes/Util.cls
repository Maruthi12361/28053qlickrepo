/***********************************************************
* Change Log:
* XXXX-XX-XX    XXX     Initial class
* 2012-04-11    RDZ     Adding a util class to create test users
*
*
* 2018-10-12    Cognizant  BSL-1040 Commented code as part of Clean Up Activity 
**************************************************************/


public class Util {
    
    public static final String CONFIG_NAME ='MDF_CONFIGURATIONS';
    public static final String ACCOUNT_OBJECT_NAME ='Account';
    public static final String FUND_REQUEST = 'FR';
    public static final String FUND_CLAIM = 'FC';
    public static final String NUMBER_TYPE ='Number';
    public static final String String_TYPE ='String';
    public static final String NUMBER_STRING_TYPE ='NUMBER_STRING';
    
    
    /**
     * Method to check if the org has multi currency enabled.
     * @return multiCurrencyEnabled.
     */
    
    public static boolean isMultiCurrencyEnabled() {
        boolean multiCurrencyEnabled = false;
        Map<String,Schema.SObjectType> objTypeMap = Schema.getGlobalDescribe();
        if (objTypeMap != null && objTypeMap.size() > 0) {
            if (objTypeMap.containsKey('CurrencyType')) {
                multiCurrencyEnabled = true;
            }
        }
        return multiCurrencyEnabled;
    }
    
    
  
    
    /* Submits an object for approval*/
    
    /*public static Approval.ProcessResult submitForApproval(Id objId, String message) {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments(message);
        req.setObjectId(objId);
        Approval.ProcessResult result = Approval.process(req,false);
        return result;
    }*/
    
    /**
     * Tests specified new and old records for changes in specified set of fields.
     * 
     * @return <code>true</code> if changed; <code>false</code> otherwise.
     */
     
    /*public static Boolean hasChanges(Set<String> fieldNames, SObject oldRecord, SObject newRecord) {
        if (oldRecord == null) {
            return true;
        }
        Boolean changed = false;
        for (String field : fieldNames) {
            changed = (oldRecord.get(field) != newRecord.get(field));
            if (changed) break;
        }
        return changed;
    }
    
    public static Boolean hasChanges(String field, SObject oldRecord, SObject newRecord) {
        if (oldRecord == null) {
            return true;
        }
        return (oldRecord.get(field) != newRecord.get(field));
    } 
    
    public static Map<Id, Integer> getMaxSequenceForGivenAccounts(Set<Id> accountIdSet, String objectName) {
        Map<Id, Integer> accountSeqMap = new Map<Id, Integer>();
        AggregateResult[] groupedResults = null;
        if (objectName == FUND_REQUEST) {
            groupedResults   = [SELECT Max(Sequence__c) seq  , Account__c accountID  FROM Fund_Request__c where Account__c IN : accountIdSet  group by Account__c ];
        } else if (objectName == FUND_CLAIM) {
            groupedResults   = [SELECT Max(Sequence__c) seq  , Account__c accountID  FROM Fund_Claim__c where Account__c IN : accountIdSet  group by Account__c ];
        }

        Integer seq = 0;
        for (AggregateResult ar : groupedResults)  {
            seq = 0;
            if (ar.get('seq') != null) {
                seq = ((Decimal)ar.get('seq')).intValue();
            }
            accountSeqMap.put(((Id)ar.get('accountID')),seq );
        }
        
        return accountSeqMap;
    }    
    
    public static Map<Id, Decimal> getAvlBudgetForGivenAccounts(Set<Id> accountIdSet) {
        Map<Id, Decimal> accountAvlBudgetMap = new Map<Id, Decimal>();
        AggregateResult[] groupedResults   = [select Account__c accountId,sum(Available_Budget__c) budgetSum from Fund_Budget__c where  Fund_Request_Deadline__c >= : system.today() and (Account__c IN : accountIdSet OR Account__c = null)  group by Account__c];
        System.debug('groupedResults&&&&&&&&&&&&&'+ groupedResults);
        Decimal avlBudgetSum = 0;
        for (AggregateResult ar : groupedResults)  {
            avlBudgetSum = 0;
            if (ar.get('budgetSum') != null) {
                avlBudgetSum = (Decimal)ar.get('budgetSum');
            }
            System.debug('(Id)ar.get(accountID)&&&&&&&&&&&&&'+ (Id)ar.get('accountID'));
            System.debug('budgetSum&&&&&&&&&&&&&'+ avlBudgetSum);
            accountAvlBudgetMap.put(((Id)ar.get('accountID')),avlBudgetSum );
        }
        
        return accountAvlBudgetMap;
    }  
   
    public static Fund_Budget__c createFundBudgetGivenDetails(Account account, Fund_Accrual__c fundAccural, MDF_Configurations__c mdfConfigurations) {
      return createFundBudgetGivenDetails(account, fundAccural,mdfConfigurations, null) ;
    }   
    
    public static Fund_Budget__c createFundBudgetGivenDetails(Account account, Fund_Accrual__c fundAccural, MDF_Configurations__c mdfConfigurations, Fund_Budget__c fundBudget) {
        if (fundBudget == null) {
          fundBudget = new Fund_Budget__c();
        }
        fundBudget.Account__c = account.Id;
        fundBudget.Period_Revenue__c = fundAccural.Revenue__c;
        if (fundAccural.Budget_Name__c != null) {
            fundBudget.Budget_Name__c = fundAccural.Budget_Name__c;
        }
        
        if (mdfConfigurations != null && mdfConfigurations.Account_CO_OP_Field_Name__c != null) {
            fundBudget.CoOp_Earned__c = (Decimal)account.get(mdfConfigurations.Account_CO_OP_Field_Name__c.toLowerCase());
        } else {
            fundBudget.CoOp_Earned__c = (Decimal)account.get('Co_op_Percentage__c');
        }
        if (fundBudget.CoOp_Earned__c != null && fundBudget.Period_Revenue__c != null) {
            fundBudget.Amount__c = (fundAccural.Revenue__c * (fundBudget.CoOp_Earned__c/ 100));
        }
        Datetime todaysDate = datetime.now();
        if(fundAccural.Start_Earned_Date__c == null) {
          fundBudget.Start_Earned_Date__c = Datetime.newInstance(todaysDate.year(), todaysDate.month(), 1).date();
        } else {
            fundBudget.Start_Earned_Date__c = fundAccural.Start_Earned_Date__c;
        }
        if(fundAccural.End_Earned_Date__c == null) {
          Integer days = 31;
          if(todaysDate.month() == 2) {
            days = 28;
          } else {
            Integer remainder = math.mod(todaysDate.month(), 2);
            if (remainder != 0) {
              days = 30;
            }
          }
          fundBudget.End_Earned_Date__c =Datetime.newInstance(todaysDate.year(), todaysDate.month(), days).date();
        } else {
            fundBudget.End_Earned_Date__c = fundAccural.End_Earned_Date__c;
        }
        if (mdfConfigurations != null && mdfConfigurations.Fund_Request_DeadLine__c != null && mdfConfigurations.Fund_Request_DeadLine__c > 0) {
            fundBudget.Fund_Request_Deadline__c = fundBudget.End_Earned_Date__c.addDays(mdfConfigurations.Fund_Request_DeadLine__c.intValue());
        } else {
            fundBudget.Fund_Request_Deadline__c = fundBudget.End_Earned_Date__c;
        }
        
        if (mdfConfigurations != null && mdfConfigurations.Fund_Claim_DeadLine__c != null && mdfConfigurations.Fund_Claim_DeadLine__c > 0) {
            fundBudget.Fund_Claim_Deadline__c = fundBudget.End_Earned_Date__c.addDays(mdfConfigurations.Fund_Claim_DeadLine__c.intValue());
        } else {
            fundBudget.Fund_Claim_Deadline__c = fundBudget.End_Earned_Date__c;
        }        
        return fundBudget;
    } 
   
    public static MDF_Configurations__c getConfigurations() {
        MDF_Configurations__c mdfConfigurations = null;
        Map<String,MDF_Configurations__c> mdfConfigMap =  MDF_Configurations__c.getAll();
        if(mdfConfigMap != null && mdfConfigMap.containsKey(Util.CONFIG_NAME)) {
             mdfConfigurations = mdfConfigMap.get(Util.CONFIG_NAME);
        }
        return mdfConfigurations;
    }  
    
    public static void createConfigurations() {
         Map<String,MDF_Configurations__c> mdfConfigMap =  MDF_Configurations__c.getAll();
         if(!mdfConfigMap.containsKey(Util.CONFIG_NAME)) {
            MDF_Configurations__c mdfConfig = new MDF_Configurations__c();
            mdfConfig.Name = Util.CONFIG_NAME;
            mdfConfig.Fund_Claim_DeadLine__c =40;
            mdfConfig.Fund_Expriring_Alert_Days__c= 40;
            mdfConfig.Fund_Request_DeadLine__c =40;
            mdfConfig.Block_Insufficient_Funds_Request__c = false;
            mdfConfig.Alert_Claim_Without_Attachments__c = true;
            mdfConfig.Alert_FC_or_FR_Without_Allocations__c =true;
            mdfConfig.Payment_Details_Validation__c =false;
            mdfConfig.Allow_FR_Cancellation_FC_Status_Not__c='Paid';
            mdfConfig.Regions_Without_Fund_Allocations__c = 'US';
            mdfConfig.Account_Region_Field_Name__c ='Global_Region__c';
            mdfConfig.Account_Identity_Field_Name__c ='Id';
            insert mdfConfig;
        }       
    }
    
    
    public static List<SObject> getAccountDetailsGivenNames(Set<String> accountNameSet,MDF_Configurations__c mdfConfigurations) {
      List<SObject> accountList = null; 
      try {
          String soql = 'Select Id,Name';
          if (mdfConfigurations != null && mdfConfigurations.Account_CO_OP_Field_Name__c != null) {
              soql += ','+ mdfConfigurations.Account_CO_OP_Field_Name__c.toLowerCase();
          } else {
              soql +=',Co_op_Percentage__c';
          }
          if (mdfConfigurations != null && mdfConfigurations.Account_Identity_Field_Name__c != null) {
            if (mdfConfigurations.Account_Identity_Field_Name__c.equalsIgnoreCase('Id')) {
                  soql +=  ' From Account where '+ mdfConfigurations.Account_Identity_Field_Name__c +' IN ';
            } else {
              soql +=  ','+ mdfConfigurations.Account_Identity_Field_Name__c +' From Account where '+ mdfConfigurations.Account_Identity_Field_Name__c +' IN ';
            }
          } else {
              soql +=  ' From Account where Name IN ';
          }
          
          System.debug('soql----------->'+ soql);
          soql +=  buildInExpression(accountNameSet,mdfConfigurations);
          accountList = Database.query(soql);
      } catch (Exception ex) {
        
      }
        return accountList;
    }
    
    public static Map<Id,SObject> getAccountDetailsGivenIds(Set<Id> accountIdSet,MDF_Configurations__c mdfConfigurations) {
      Map<Id,SObject> accountMap = new Map<Id,SObject>();
      String soql = 'Select Id,Name';
        if (mdfConfigurations != null && mdfConfigurations.Account_CO_OP_Field_Name__c != null) {
            soql += ','+ mdfConfigurations.Account_CO_OP_Field_Name__c.toLowerCase();
        } else {
            soql +=',Co_op_Percentage__c';
        }
        soql +=  ' From Account where Id IN ';
        soql +=  buildInExpression(accountIdSet);
        List<SObject> accountList = Database.query(soql);
      if (accountList != null && accountList.size() > 0) {
          for(SObject accObj : accountList) {
              accountMap.put((Id)accObj.get('Id'), accObj);
          }
      }
       return accountMap;
    }
    
    public static boolean isAccountIdentityNumber(String fieldName) {
        boolean isNumber = false;
        Map<String, Schema.SObjectField> accountMap = Schema.SObjectType.Account.fields.getMap();
        if (accountMap != null && accountMap.containsKey(fieldName)) {
             Schema.DescribeFieldResult identityField = accountMap.get(fieldName).getDescribe() ;
             if(identityField.getType() == Schema.DisplayType.Integer) { 
                isNumber = true;
             }
        }                
        return isNumber;
    }
    
    public static String buildInExpression(Set<String> accountNameSet,MDF_Configurations__c mdfConfigurations) {
        boolean isNumber = false;
        if (mdfConfigurations != null && mdfConfigurations.Account_Identity_Field_Name__c != null) {
            isNumber = isAccountIdentityNumber( mdfConfigurations.Account_Identity_Field_Name__c);
        }
        String names = '(';
        for (String name : accountNameSet) {
            if (isNumber) {
                names += ( name + ',');
            } else {
                names += ('\'' + name + '\',');
            }
        }
        return names.substring(0,names.length() - 1) + ')';
    }
    
    public static String buildInExpression(Set<Id> accountNameSet) {
        String names = '(';
        for (Id name : accountNameSet) {
            names += ('\'' + name + '\',');
        }
        return names.substring(0,names.length() - 1) + ')';
    }
    
    public static Map<Id,SObject> getAccountRegions(Set<Id> accountIdSet,MDF_Configurations__c mdfConfigurations) {
        Map<Id,SObject> accountMap = new Map<Id,SObject>();
        if (accountIdSet != null && accountIdSet.size() > 0) {
            String soql = 'Select Id,Name';
            if (mdfConfigurations != null && mdfConfigurations.Account_Region_Field_Name__c != null) {
                soql += ','+ mdfConfigurations.Account_Region_Field_Name__c.toLowerCase();
            }
            soql +=  ' From Account where Id IN ';
            soql +=  buildInExpression(accountIdSet);
            List<SObject> accountList = Database.query(soql);
            if (accountList != null && accountList.size() > 0) {
                for(SObject accObj : accountList) {
                    accountMap.put((Id)accObj.get('Id'), accObj);
                }
            }
        }
        
        return accountMap;
    }
    
    public static boolean checkValidationFundRequired(SObject account,MDF_Configurations__c mdfConfigurations) {
        boolean validate = true;
        String[] regionArray = mdfConfigurations.Regions_Without_Fund_Allocations__c.split(',');  
        if (regionArray != null) {
            for (String regionName : regionArray) {
                if (regionName.equalsIgnoreCase((String)account.get(mdfConfigurations.Account_Region_Field_Name__c))) {
                    validate = false;
                    break;
                }
            }
        }       
        return validate;
    }
    
    public static Decimal getClaimedAmtsForAGivenFundRequest(String fundRequestId) {
        AggregateResult[] groupedResults   = [Select sum(Approved_Amount__c) claimSum From Fund_Claim__c  where Fund_Request__c = : fundRequestId];
        Decimal claimedAmount = 0;
        for (AggregateResult ar : groupedResults)  {
            if (ar.get('claimSum') != null) {
                claimedAmount = (Decimal)ar.get('claimSum');
            }
        }
        
        return claimedAmount;
    }   
    
    public static String getAccountIdentityFieldName(MDF_Configurations__c mdfConfigurations) {
        String fieldName='Name';
        if (mdfConfigurations != null && mdfConfigurations.Account_Identity_Field_Name__c != null) {
            fieldName = mdfConfigurations.Account_Identity_Field_Name__c;
        }
        return fieldName;
    }  
    
    
    public static Map<Id, Decimal> getClaimedAmtsForAGivenFundRequest(Set<Id> fundRequestIdSet) {
        Map<Id, Decimal> requestClaimedMap = new Map<Id, Decimal>();
        AggregateResult[] groupedResults   = [Select Fund_Request__c fundRequestId,sum(Approved_Amount__c) claimSum From Fund_Claim__c  where Fund_Request__c IN : fundRequestIdSet and Status__c ='Approved' group by Fund_Request__c];
        Decimal claimedAmount = 0;
        for (AggregateResult ar : groupedResults)  {
            claimedAmount = 0;
            if (ar.get('claimSum') != null) {
                claimedAmount = (Decimal)ar.get('claimSum');
            } 
            requestClaimedMap.put(((Id)ar.get('fundRequestId')),claimedAmount);
        }
        
        return requestClaimedMap;
    }*/
    
    /**
     * Method to get the all the currencies that are enabled in the org and are active.
     */
     
    public static Map<String, CurrencyWrapper> getCurrencies() {
        Map<String, CurrencyWrapper> currencyMap = new Map<String, CurrencyWrapper>();
        for (SObject cur : Database.query('Select c.IsoCode, c.IsCorporate, c.IsActive, c.DecimalPlaces, c.ConversionRate From CurrencyType c Where c.IsActive='+ true))
         {
            currencyMap.put((String)cur.get('IsoCode'), new CurrencyWrapper(cur));
        }
        return currencyMap;
    } 
    
    /*public static void convertAmountToCorporateCurrency( Map<String, CurrencyWrapper>  currencyMap, SObject fundObj) {
        CurrencyWrapper corporateCurrency = getCorporateCurrency(currencyMap);
        String recordIsoCode = (String)fundObj.get('CurrencyIsoCode');
        Decimal amountInOrgCurrency = (Decimal)fundObj.get('Amount__c');
        if(amountInOrgCurrency != null) {
          amountInOrgCurrency.setScale(2); 
          if (!recordIsoCode.equalsIgnoreCase(corporateCurrency.getIsoCode())) {
              CurrencyWrapper recordCurrency = currencyMap.get(recordIsoCode);
              amountInOrgCurrency.setScale(recordCurrency.getDecimalPlaces());
              amountInOrgCurrency = amountInOrgCurrency.divide(recordCurrency.getConversionRate(), 2,System.Roundingmode.UP);
          } 
          fundObj.put('Amount_In_Corporate_Currency__c', amountInOrgCurrency);
        }
          
    } */
    
   
    
    public static CurrencyWrapper getCorporateCurrency(Map<String, CurrencyWrapper> currencyMap)  {
        CurrencyWrapper corporateCurrency = null;
        for (CurrencyWrapper currenyInfo : currencyMap.values()) {
            if (currenyInfo.isCorporateCurrency()) {
                corporateCurrency = currenyInfo;
                break;
            }
        }
        
        return corporateCurrency;
    }
    
   /* @future
    public static void updateFundAccuralWithAccountAndBudgetDetails(Map<String,String> fundAccuralAndAccountIdMap,Map<String,String> accountIdAndBudgetIdMap) {
      
      String accountId = null;
      String fundBudgetId = null;
      List<Fund_Accrual__c> updateList = new List<Fund_Accrual__c>();
      for(String fundAccuralId : fundAccuralAndAccountIdMap.KeySet()) {
        accountId =fundAccuralAndAccountIdMap.get(fundAccuralId);
        fundBudgetId = accountIdAndBudgetIdMap.get(accountId);
        updateList.add(new Fund_Accrual__c(Id=fundAccuralId,Account__c=accountId,Fund_Budget__c=fundBudgetId));
      }
      if(updateList.size() > 0) {
        update updateList;
      }
      
    } */
    
    // 2018-10-12 Cognizant BSL-1040 Commented code as part of Clean Up Activity 
	public static Decimal getFundRequestApprovedAmount(Id fundRequestId, Id fudReqAllocationsRecordId) {
     // AggregateResult[] groupedResults   = [Select sum(CTSK_PFM__Amount__c) approvedSum From CTSK_PFM__Fund_Budget_Allocation__c  where CTSK_PFM__Fund_Request__c = : fundRequestId AND RecordTypeId =: fudReqAllocationsRecordId];
        Decimal approvedAmount = 0;
       /* for (AggregateResult ar : groupedResults)  {
            if (ar.get('approvedSum') != null) {
                approvedAmount = (Decimal)ar.get('approvedSum');
            }
        }  //This block was already commented earlier
        for (CTSK_PFM__Fund_Budget_Allocation__c a : [Select CTSK_PFM__Amount__c From CTSK_PFM__Fund_Budget_Allocation__c  where CTSK_PFM__Fund_Request__c = : fundRequestId AND RecordTypeId =: fudReqAllocationsRecordId]) {
            approvedAmount += (a.CTSK_PFM__Amount__c == null) ? 0 : a.CTSK_PFM__Amount__c;
        } */
        return approvedAmount;
      
    }

    public static Decimal getClaimedAmtsForAGivenFundRequest(String fundRequestId) {
        //AggregateResult[] groupedResults   = [Select sum(CTSK_PFM__Approved_Amount__c) claimSum From CTSK_PFM__Fund_Claim__c  where CTSK_PFM__Fund_Request__c = : fundRequestId];
        Decimal claimedAmount = 0;        
       /* for (AggregateResult ar : groupedResults)  {   
            if (ar.get('claimSum') != null) {  
                claimedAmount = (Decimal)ar.get('claimSum');
            }
        } //This block was already commented earlier
        for (CTSK_PFM__Fund_Claim__c claim : [Select CTSK_PFM__Approved_Amount__c From CTSK_PFM__Fund_Claim__c  where CTSK_PFM__Fund_Request__c = : fundRequestId]) {
             claimedAmount = (claim.CTSK_PFM__Approved_Amount__c == null) ? 0 : claim.CTSK_PFM__Approved_Amount__c;
        }  */      
        return claimedAmount;    
    }   
    
    
    public static User createUser( String userName, String lName, Id profileId, Id roleId )
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        User newUser = new User(FirstName=lName,
                                LastName=lName,
                                UserName=userName + randomName + '@testorg.com',
                                Email=userName + randomName + '@testorg.com',
                                Alias=lName,
                                LocaleSidKey='en_US',
                                LanguageLocaleKey='en_US',
                                EmailEncodingKey='ISO-8859-1',
                                CommunityNickname=lName + randomName,
                                ProfileId = profileId,
                                TimeZoneSidKey='America/New_York',
                                UserRoleId = roleId,
                                Country = 'USA');
        return newUser;
}
    
        
}