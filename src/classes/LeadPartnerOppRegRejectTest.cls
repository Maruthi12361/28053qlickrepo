@isTest
private class LeadPartnerOppRegRejectTest {

    static testMethod void myUnitTest() {
        //QTTestUtils.GlobalSetUp();
        WebServiceMockDispatcher dispatcher = new WebServiceMockDispatcher();
        Id oppRegId = [SELECT Id FROM RecordType WHERE Name = 'Partner Opp Reg'][0].Id;
        Id oppRegIdRejected = [SELECT Id FROM RecordType WHERE Name = 'Partner Opp Reg Rejected'][0].Id;
    
        User partner = createUser('partner123@test.com','p123');    
        User internal = createUser('internal123@test.com','i123');      
                
        //create a Opp Reg Lead and reject it, assert that owner has been changed back and record type has been changed

        Lead oppRegLead = insertAndRejectLead(oppRegId, internal.Id, partner.Id);
                
        System.assertEquals(partner.Id, oppRegLead.OwnerId);
        System.assert(oppRegLead.Opp_Reg_Rejected_Date__c != null);
        System.assertEquals(oppRegIdRejected, oppRegLead.RecordTypeId);
        
        Semaphores.LeadTriggerHandlerBeforeInsert = false;
        //create a non Opp Reg lead and reject it, assert that the owner hasn't changed and record type is the same
        Lead nonOppRegLead = insertAndRejectLead(null, internal.Id, partner.Id);
        
        System.assertEquals(internal.Id, nonOppRegLead.OwnerId);                
        
    }
    
    private static Lead insertAndRejectLead(Id recordTypeId, Id owner, Id originalPartnerId) {
        Lead lead = new Lead(FirstName = 'First name test', LastName = 'test', Company = 'Test company', Opp_Reg_Rejected__c = false,
                             OwnerId = owner, Original_Partner_OwnerId__c = originalPartnerId, Country = 'Spain', Email = 'test@testtesttest.com');
        if (recordTypeId != null) lead.RecordTypeId = recordTypeId;
        insert lead;
        
        lead.Opp_Reg_Rejected__c = true;
        lead.Opp_Reg_Rejected_Reason__c = 'Goal not confirmed';
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update lead;
        
        return [SELECT RecordTypeId, Opp_Reg_Rejected_Reason__c, Opp_Reg_Rejected_Date__c, OwnerId FROM Lead WHERE Id = :lead.Id][0];
    }
    
    private static User createUser(String username, String nick) {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
        User user = new User(Username = System.now().millisecond() + '_' + username, LastName = 'Test Lastname', Email = username, 
                             Alias = 'tula', CommunityNickname = nick, TimeZoneSidKey = 'Europe/Paris', 
                             LocaleSidKey = 'es_ES', EmailEncodingKey = 'ISO-8859-1', ProfileId = profile.Id, LanguageLocaleKey = 'en_US'); insert user;
                             return user;
    }
}