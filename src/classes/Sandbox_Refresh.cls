/*
* File Sandbox_Refresh 
    * @description : Class for Post deployemnt activity for updating post deployment activity
                     It will be executed after QA refresh in prod.
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       09.01.2018   Pramod Kumar V      Created Class 
    2       20.01.2018   Pramod Kumar V      Added logic from SandboxRefresh_Controller class
                                             Automated Jira Tickets: SFDC: Sandbox Refresh App,SFDC: Contact email updates,SFDC: Lead Email Updates

    3       28.04.2018   Pramod Kumar v      ITRM-60 Fixed Mixed DML exception and added logic to create subsidary
    4       21.06.2018   Maksim Karpitski    Added a method call to create Campaign
    5       23.07.2018   ext_bjd             QAR-64 Fixed call the Post Refresh logic from one place instead of duplicating in two classes
    6       03.02.2020   ext_bjd             ITRM-380 code refactored
*/
global class Sandbox_Refresh implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        if (context != null) {
            String sandboxName = context.sandboxName();
            PostRefreshHandler.postRefreshHelper(sandboxName);
        }
    }
}