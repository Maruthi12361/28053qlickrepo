/******************************************************

        CurrencyQueryHelper
    
        This class is helper to query CurrencyType object and retrieve
        records by id/key. It will try to use cache to reduce number of queries.
    
        Changelog:
            2016-05-30  Roman@4front     Created class
    ******************************************************/

public without sharing class CurrencyQueryHelper {

	public CurrencyQueryHelper() {

	}

	private static Map<String, CurrencyType> currencyMap;

	public static List<CurrencyType> getCurrenciesByISOCodes(List<String> isoCodes, Boolean reset) {
		currencyMap = null;
		return getCurrenciesByISOCodes(isoCodes);
	}

	public static List<CurrencyType> getCurrenciesByISOCodes(List<String> isoCodes) {
		List<String> isoCodesMissingInCache = new List<String>();
		List<CurrencyType> returnValues = new List<CurrencyType>();
		if(currencyMap == null) {
			currencyMap = new Map<String, CurrencyType>();
		}
		for(String iso : isoCodes) {
			if(!currencyMap.containsKey(iso)) {
				isoCodesMissingInCache.add(iso);
			}
		}
		if(!isoCodesMissingInCache.isEmpty()) {
			List<CurrencyType> currencies = new List<CurrencyType>([SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and ISOCode in :isoCodesMissingInCache]);
			for(CurrencyType ct : currencies){
				currencyMap.put(ct.ISOCode, ct);
			}
		}
		// now we're retrieving data from the currencies list
		for(String isoCode : isoCodes) {
			returnValues.add(currencyMap.get(isoCode));
		}
		System.debug('--Test returnISO' + returnValues);
		return returnValues;
	}

	public static CurrencyType getCurrencyByISOCode(String isoCode) {
		List<String> isoCodes = new List<String>();
		isoCodes.add(isoCode);
		List<CurrencyType> returnCurrencies = getCurrenciesByISOCodes(isoCodes);
		if(!returnCurrencies.isEmpty()) {
			return returnCurrencies[0];
		}
		else {
			return null;
		}
	}
}