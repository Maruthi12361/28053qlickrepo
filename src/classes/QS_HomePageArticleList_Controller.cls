/************************************************************
* 2015-05-22 AIN Added support for multiselect on Persona
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
* 2019-05-13 extbad IT-1685 Remove visit QS_DataCapture creation
************************************************************/
public with sharing class QS_HomePageArticleList_Controller {
    
    public Boolean unauthenticated {get; set;}
    public String searchCategory {get; set;}
    public Map<string, string> VersionMap {get; set;}
    public Map<string, string> ProductMap {get; set;}
    
     // selected Product
    public static String selectedProduct {get; set;}
    private transient List<SelectOption> v_ProductOptionList;

    public List<SelectOption> ProductOptionListFiltered
     {
      get {     
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Product:Qlik_Sense_Cloud','Qlik Sense Cloud'));
        options.add(new SelectOption('Product:Server_Publisher','Server Publisher'));
        options.add(new SelectOption('Product:Desktop','Desktop'));
        options.add(new SelectOption('Product:Qlik_DataMarket','Qlik DataMarket'));
        options.add(new SelectOption('Product:NPrinting_Server','NPrinting Server'));
        v_ProductOptionList = options;
        return v_ProductOptionList;
        }
        set {
            v_ProductOptionList = value;
        }
        }
    
    public QS_HomePageArticleList_Controller() {
        
        VersionMap = new Map<string, string>();
        ProductMap = new Map<string, string>();
        searchCategory = '';
        unauthenticated = false;
        User userRec;
        if(userinfo.getUserId() != null)
            userRec = [Select Id, ContactId, AccountId, Contact.Persona__c 
                            From User 
                            where Id = :userinfo.getUserId() AND IsActive = true];
        
        if(userRec == null || userRec.ContactId == null || userRec.AccountId == null)
            unauthenticated = true;
        else
            unauthenticated = false;
            
        if(!unauthenticated && userRec != null && userRec.ContactId != null && userRec.Contact.Persona__c != null) {
            System.debug ('Kauser Persona' + userRec.Contact.Persona__c );

            searchCategory = QS_Utility.GetQS_Persona_Category(userRec.Contact.Persona__c);
            
          

            /*string[] searchCategories = searchCategory.split(',');

            string persona = '';

            if(personas.size() > 1)
            {
                Double rnd =  Math.random();
                integer index = (integer)(rnd * personas.size());
                persona = personas[index];*/

            system.debug('searchCategory: ' + searchCategory);
            
            /*for(string s : userRec.Contact.Persona__c.split(';'))
            {
                s = s.trim();
                QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(userRec.Contact.Persona__c);

                searchCategory = searchCategory == '' ? '' : ',' +  categoryRecord != null ? categoryRecord.Article_Categories__c : '';
            }*/
            //QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(userRec.Contact.Persona__c);
            //searchCategory = categoryRecord != null ? categoryRecord.Article_Categories__c : '';
        } else {
            searchCategory = '';
        }
        List<DescribeDataCategoryGroupResult> describeCategoryResult;

        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;

        List<String> objType = new List<String>();
        objType.add('KnowledgeArticleVersion');
        describeCategoryResult = Schema.describeDataCategoryGroups(objType);
        //Creating a list of pair objects to use as a parameter

        //for the describe call

        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
        //Looping throught the first describe result to create
        //the list of pairs for the second describe call
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
            DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();

            p.setSobject(singleResult.getSobject());
            p.setDataCategoryGroupName(singleResult.getName());
            pairs.add(p);
        }

        //describeDataCategoryGroupStructures()
        describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);
        //Getting data from the result

        for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){

            //Getting the name of the category
            system.debug('Name: ' + singleResult.getName());
            system.debug('Label: ' + singleResult.getLabel());
            system.debug('Description: ' + singleResult.getDescription());

            
            //Get the top level categories
            DataCategory [] toplevelCategories = singleResult.getTopCategories();
            //Recursively get all the categories
            List<DataCategory> allCategories = getAllCategories(toplevelCategories);

            string categoryType = singleResult.getName();




            for(DataCategory category : allCategories) {

                //Get the name of the category

                system.debug('Category Name: ' + category.getName());
                //Get the label of the category
                system.debug('Category Label: ' + category.getLabel());
                //Get the list of sub categories in the category
                DataCategory [] childCategories = category.getChildCategories();
                if(categoryType == 'Version')
                    VersionMap.put(category.getLabel(), category.getName());
                else if(categoryType == 'Product')
                    ProductMap.put(category.getLabel(), category.getName());
            }
        }
        system.debug('VersionMap: ' + VersionMap);
        system.debug('ProductMap: ' + ProductMap);
    }
    
    public void refreshProductCategory()
    {
    
    system.debug('refreshProductCategory searchCategory: ' + searchCategory);
        if(selectedProduct != null)
        {
              searchCategory = selectedProduct ;
        }
    system.debug(' refreshProductCategory searchCategory: ' + searchCategory);
            
    }

    private static DataCategory[] getAllCategories(DataCategory [] categories){
        if(categories.isEmpty()){
            return new DataCategory[]{};
        } else {
            DataCategory [] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[]{category};
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            return allCategories;
      }
   }

    public PageReference redirect() {
        PageReference pr = Page.QS_CoveoSearch;
        pr.setRedirect(true);
        return pr;
    }
}