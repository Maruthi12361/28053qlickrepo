/*
* Change log:
*
* ext_vos   2018-02-14 CHG0032935 - Remove "sendEmail" flag. Formatting CommentBody before sending in email.
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*/
public class QS_FeedCommentTriggerHandler {
    
    public static void afterTriggerHandler(map<Id,FeedComment> commMap){
        

        /*map<Id,list<FeedComment>> commByCaseId = new map<Id,list<FeedComment>>();
        set<Id> caseIds = new set<Id>();
        set<Id> userIds = new set<Id>();*/
        Set<Id> feedItemIds = new Set<Id>();
        for(FeedComment comm : commMap.values()){
            feedItemIds.add(comm.FeedItemId);
        }
        Map<Id, FeedItem> feeds = new Map<Id, FeedItem>([select Id, Visibility from FeedItem where Id in :feedItemIds]);
        List<FeedItem> feedItems = new List<FeedItem>();
        for (FeedComment comm : commMap.values()) {
            /*if(comm.ParentID != null && ((String) comm.ParentId).startsWith('500')) {
                if(!commByCaseId.containsKey(comm.ParentId)) commByCaseId.put(comm.ParentId, new list<FeedComment>());
                commByCaseId.get(comm.ParentId).add(comm);
                caseIds.add(comm.ParentId);
                userIds.add(comm.InsertedById);
            }*/
            FeedItem fi = new FeedItem();
            fi.ParentId = comm.ParentId;
            fi.Body = comm.CommentBody != null ? '<br/>' + comm.CommentBody.replace('\n', '<br/>') + '<br/>' : '';
            fi.Visibility = feeds.get(comm.FeedItemId).Visibility;
            fi.Type = comm.CommentType;
            feedItems.Add(fi);
        }

        QS_FeedItemTriggerhandler.emailCaseFollowers(feedItems);
        
        // retrieve all the related cases
        /*map<Id,Case> caseMap = new map<Id,Case>();
        
        if(caseIds != null && !caseIds.isEmpty())
            caseMap.putAll([SELECT Id, Status, Subject, (Select Id, ParentId, SubscriberId, NetworkId,  Subscriber.name, Subscriber.email, Subscriber.ContactId, Subscriber.Contact.QS_Opt_out_case_emails__c 
                                                      From FeedSubscriptionsForEntity), CaseNumber
                         FROM Case 
                         WHERE Id in :caseIds]);
        
        //Get user info
        map<Id,User> userMap = new map<Id,User>();
        
        userMap.putAll([select Id, Profile.Name from User where Id IN :userIds]);
        
        list<Case> caseUpdateList = new list<Case>();
        
        if(caseMap != null && !caseMap.Values().isEmpty()) {
            // Change the status for the case only when the case is not currently under 'Ready To Close' status     
            for (FeedComment comm: commMap.values()) {
                if(caseMap.get(comm.ParentID).Status != System.Label.QS_CloseCase) {
                    //Check the Current User's profile and set the Case status
                    if((userMap.get(comm.InsertedById).Profile.Name.startsWith('Customer') || userMap.get(comm.InsertedById).Profile.Name.startsWith('PRM') )&& caseMap.containsKey(comm.ParentId) && caseMap.get(comm.ParentID).Status != System.Label.QS_CaseClosed)
                        caseUpdateList.add(new Case(Id=comm.ParentID, Status = System.Label.QS_CaseFeedResponseExt_Status));
                    else if (userMap.get(comm.InsertedById).Profile.Name.startsWith('Custom: Support') )
                        caseUpdateList.add(new Case(Id=comm.ParentID, Status = System.Label.QS_CaseFeedResponse_Support_Status));
                 
                }
            }
        }
        
        if(caseUpdateList != null && caseUpdateList.size() >0){
            update caseUpdateList;
        }
     */   
    }
}