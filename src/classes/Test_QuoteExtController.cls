/****************************************************************
*
*  Test_QuoteExtController 
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*  23.03.2017 : Rodion Vakulvsokyi 
*  22.06.2017 : Nikhil Jain
* 02.09.2017 : Srinivasan PR- fix for query error
*****************************************************************/
@isTest
public class Test_QuoteExtController{
    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    private static testMethod void testMethod1() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        update testAccount;
        
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType 
		                            where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
        insert  testPartnerAccount;
        
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        insert testContact;
        
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
        insert quoteForTest;
            
        Test.startTest();
            update quoteForTest;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
            QuoteExtController quoteExtCntr = new QuoteExtController(sc);
            quoteExtCntr.onSubmit();
            quoteExtCntr.onRecall();
            quoteExtCntr.ApproverExist();
            quoteExtCntr.saveAndSubmit();
            quoteExtCntr.onPreview();
            
            quoteForTest.Order_Form_Status__c = 'Out For Signature';
            update quoteForTest;
            quoteExtCntr.onRecall();
            
            quoteForTest.Order_Form_Status__c = 'External Review';
            update quoteForTest;
            quoteExtCntr.onRecall();
            
            quoteForTest.Order_Form_Status__c = 'Sales Review';
            update quoteForTest;
            quoteExtCntr.onRecall();
            
            quoteForTest.Order_Form_Status__c = 'Legal Review';
            update quoteForTest;
            quoteExtCntr.onRecall();
            
            quoteForTest.Order_Form_Status__c = 'Orders Review';
            update quoteForTest;
            quoteExtCntr.onRecall();
            
            quoteForTest.Order_Form_Status__c = 'Orders Approval';
            update quoteForTest;
            quoteExtCntr.onRecall();
            
            quoteForTest.Order_Form_Status__c = 'Fully Executed';
            update quoteForTest;
            quoteExtCntr.onRecall();
            
            quoteForTest.Order_Form_Status__c = 'Quote Approvals Needed';
            update quoteForTest;
            quoteExtCntr.onRecall();
        Test.stopTest();

        Opportunity createdOpp = [Select id From Opportunity];

        System.assertEquals([select id, SBQQ__Opportunity2__c From SBQQ__Quote__c Where id =:quoteForTest.id].SBQQ__Opportunity2__c, createdOpp.id);
        }
    }
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr, 
                                            Country_Name__c = countryName, 
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
}