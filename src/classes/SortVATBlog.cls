public with sharing class SortVATBlog 
{
    
    public VATBlog__c[] getSorted() 
    {
        // Defaults to this year
        Date d = Date.newInstance(Date.today().year(), 1, 1);
        
        if (System.currentPagereference().getParameters().containsKey('Year'))
        {
            try
            {
                d = Date.newInstance(Integer.valueOf(System.currentPagereference().getParameters().get('Year')), 1, 1);
            }
            catch(System.Exception Ex)
            {
                
            }
        }   
        
        return [SELECT Id, Rider__c, CreatedDate, Blog_Text__c, Mood__c, FileName__c FROM VATBlog__c WHERE CreatedDate >= :d ORDER BY CreatedDate DESC];
    }
 
 
        public static testMethod void ControllerTest_1()
        {
                SortVATBlog VB = new SortVATBlog();
                
                VatBlog__c[] BlogEntries = VB.getSorted();
                                
                System.currentPageReference().getParameters().put('Year', '2000');
                
                BlogEntries = VB.getSorted();           
        }   
}