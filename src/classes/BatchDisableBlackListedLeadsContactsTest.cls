/******************************************************

    Class: BatchDisableBlackListedLeadsContactsTest

    Changelog:
        2017-09-22 - AYS - BMW-390/CHG0030491 - Created file 
        2019-01-09 - AIN - IMP-1464 - Added update of contact, seems the country and city are not being set correctly on insert.

******************************************************/
@isTest
private class BatchDisableBlackListedLeadsContactsTest {
	
	final static private String STATUS_LEAD_BANNED = 'Follow-Up Flagged in Error';
	final static private String STATUS_CONTACT_BANNED  = 'Follow-Up Flagged in Error';
	final static private String STATUS_ULS_BANNED  = 'Disabled'; 

	@testSetup static void testSetup() {

		Lead lead = new Lead(LastName = 'LeadName', Email = 'LeadName@company.com', Country = 'Cuba',
			Company = 'Company', FirstName ='FirstName');
		insert lead;

		Contact contact = new Contact(LastName = 'ContactName', MailingCountry = 'Ukraine', MailingCity = 'Simferopol', Allow_Partner_Portal_Access__c = true);
		insert contact; 

		insert new List<ULC_Details__c> {
			new ULC_Details__c(LeadId__c = lead.id, ULCName__c = 'single1', ULCStatus__c = 'Active'),
			new ULC_Details__c(ContactId__c = contact.id, ULCName__c = 'single2', ULCStatus__c = 'Active')
		};
		
		insert new List<Black_Listed_Portal_Region__c>{
			new Black_Listed_Portal_Region__c(Name = 'Syria', isCountry__c = true),
			new Black_Listed_Portal_Region__c(Name = 'Cuba', isCountry__c = true),
			new Black_Listed_Portal_Region__c(Name = 'North Korea', isCountry__c = true),
			new Black_Listed_Portal_Region__c(Name = 'Iran', isCountry__c = true),
			new Black_Listed_Portal_Region__c(Name = 'North Sudan', isCountry__c = true),
			new Black_Listed_Portal_Region__c(Name = 'Kerch', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Керч', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Simferopol', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Сімферополь', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Crimea', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Sevastopol', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Yevpatoriya', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Евпатория', isCountry__c = false),
			new Black_Listed_Portal_Region__c(Name = 'Feodosiya', isCountry__c = false)
		};

		QuoteTestHelper.createCustomSettings();
				
	}

	@isTest static void test_method_generalLead() {
		
		BatchDisableBlackListedLeadsAndContacts job = new BatchDisableBlackListedLeadsAndContacts();
                      
        Test.startTest();
        ID batchprocessid = Database.executeBatch(job);   
        Test.stopTest();

        AsyncApexJob aj = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id = :batchprocessid];
        System.debug('\n\nFinal results are: '+aj);
        System.AssertEquals('Completed', aj.status);
        System.AssertEquals(0, aj.NumberOfErrors);

        Lead lead = [SELECT id, Status FROM Lead WHERE LastName = 'LeadName'];
        System.assertEquals(STATUS_LEAD_BANNED, lead.Status);

        ULC_Details__c uls_detail = [SELECT id, ULCStatus__c FROM ULC_Details__c WHERE LeadId__c =: lead.id];
        System.assertEquals(STATUS_ULS_BANNED, uls_detail.ULCStatus__c);

	}
	
	@isTest static void test_method_crimeaContact() {
	    
	    Contact contact = [SELECT id, Allow_Partner_Portal_Access__c, MailingCountry, MailingCity FROM Contact WHERE LastName = 'ContactName'];
        System.assertEquals(true, contact.Allow_Partner_Portal_Access__c);

        //AIN - MailingCountry and MailingCity are not set correctly on insert, reason unknown.
        contact.MailingCountry = 'Ukraine';
        contact.MailingCity = 'Simferopol';
        update contact;

	    Test.startTest();    

            BatchDisableBlackListedLeadsAndContacts scheduleClass = new BatchDisableBlackListedLeadsAndContacts();
            String scheduleTime = '0 0 7 * * ?'; //6 in SW
            String jobId = System.schedule('BatchDisableBlackListedLeadsAndContacts_test', scheduleTime, scheduleClass);
            scheduleClass.execute(null);

        Test.stopTest();

        contact = [SELECT id, Contact_Status__c, Allow_Partner_Portal_Access__c FROM Contact WHERE LastName = 'ContactName'];
        System.assertEquals(STATUS_CONTACT_BANNED, contact.Contact_Status__c);
        System.assertEquals(false, contact.Allow_Partner_Portal_Access__c);

        ULC_Details__c uls_detail = [SELECT id, ULCStatus__c FROM ULC_Details__c WHERE ContactId__c =: contact.id];
        System.assertEquals(STATUS_ULS_BANNED, uls_detail.ULCStatus__c);

 
    }
}