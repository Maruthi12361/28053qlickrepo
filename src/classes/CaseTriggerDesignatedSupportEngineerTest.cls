/****************************************************************************************************

    CaseEntitlementProcessSyncTest
    
    Test of Trigger to update case entitlement process      
        
    Changelog:

        2013-03-18  SLH/SAN Created
        2016-03-31  AIN designated_support_engineer__c is now set if the contact has DSE_Access
****************************************************************************************************/

@isTest
private class CaseTriggerDesignatedSupportEngineerTest {
    static testMethod void runTest() {
        Semaphores.SetAllSemaphoresToTrue();
        User dse = QTTestutils.createMockSystemAdministrator();

        Account act = new Account(name = 'Test Account');
        act.QT_Designated_Support_Contact__c = dse.Id;
        insert act;

        Contact con = new Contact();
        con.DSE_Access__c = true;
        con.FirstName = 'Test';
        con.LastName = 'Testersson';
        con.AccountId = act.Id;
        insert con;

        Case c = new Case();
        c.ContactID = con.Id;
        c.AccountId = act.Id;
        Semaphores.SetAllSemaphoresToFalse();
        insert c;

        List<Case> lstC = [Select Designated_Support_Engineer__c FROM Case WHERE Id = :c.Id];
        system.assert(dse.Id == lstC[0].Designated_Support_Engineer__c);

    }
}