/*-----------------------------
*Author: MTM Initial development
* 2014-09-30 customer portal project
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*
* Change Log:
 -----------------------------*/
global without sharing class QS_EnvironmentProductController {
    public EnvironmentProduct__c envProduct;
    public Id selectedId {get; set;}
    public String AccountName{get;set;}
    public String errorMessageEnvInsert{get;set;}
    public List<String> errorMessageEnvInsertList {get;set;}
    private list<Entitlement> licenses;
    private Id accountId;
    public String selectedLicense { get; set; }
    private String strMode;
    public list<selectOption> licenseList { get; set; }      
    public static String AccName;

    public QS_EnvironmentProductController()
    {
       Initiate();
    }
    
    public void Initiate()
    {
        errorMessageEnvInsertList = new List<string>();
        system.debug('QS_EnvironmentProductController start ' + selectedId); 
       
       // Get accpount name for License drop down filter
       String EnvId= ApexPages.currentPage().getParameters().get('Id');
       if(EnvId!=null)
        {
            Environment__c  environment = [SELECT  e.Account__c, e.Account__r.Name From Environment__c e  where e.Id =: envId];
            AccountName = environment.Account__r.Name;  
            accountId = environment.Account__c;
            selectedLicense = '--None--';
            //refreshLicenseList();
        } 
        else
            AccountName = '';
                                
        if(selectedId != null)
        {
              
            envProduct = [Select Id, Name, Product__c, License__c, Version__c From EnvironmentProduct__c where Id = :selectedId];
            RenderCreateTop = false;        
        }
        else
        {
            NewEnvironmentProduct();
        }
        SaveButtonText = 'Save';
    }
 
    public String Mode 
    {
        get
        {
             if (strMode != null)
                return strMode;
            else
                return '';
        }
        set
        {
            strMode = value;
            if (value != null && value == 'NewEnvironment')
            {
                ShowForm(); 
            }
        }
    }

    public void Create()
    {
         try
         {

            if(envProduct.Version__c == null || envProduct.Version__c == '' || envProduct.Product__c == null || envProduct.Product__c == '') {
                errorMessageEnvInsertList = new List<string>();
                errorMessageEnvInsert = 'Please enter the following mandatory fields: <br />';
                errorMessageEnvInsert += (envProduct.Version__c == null || envProduct.Version__c == '') ? '<b> Version </b> <br />' : '';
                errorMessageEnvInsert += (envProduct.Product__c == null || envProduct.Product__c == '') ? '<b> Product </b> <br />' : '';
                System.debug('%%%%errorMessageEnvInsert: '+errorMessageEnvInsert);

                if(envProduct.Version__c == null || envProduct.Version__c == '')
                    errorMessageEnvInsertList.add('Version');
                if(envProduct.Product__c == null || envProduct.Product__c == '')
                    errorMessageEnvInsertList.add('Product');
            }
            else
            {
                system.debug( '~~~~~~selectedLicense--'+ selectedLicense );
                 
                if(selectedLicense != null && selectedLicense != '' && !selectedLicense.contains('--None--'))
                    envProduct.License__c = selectedLicense;
                else
                    envProduct.License__c = null;

                insert envProduct;
                NewEnvironmentProduct();
                errorMessageEnvInsert = '';
                selectedLicense = '';
            }

        }
        catch(System.DMLException ex)
        {
            system.debug('DMLException: ' + ex);
            ApexPages.addMessages(ex);
            return;                                           
        }
    }
    
    public void Cancel()
    {
        RenderCreateTop = false;
    }
    
    public void CreateNewClicked()
    {
        RenderCreateTop ^= true;
        NewModeORRenderCreateTop = true;
    }

    public void EditClicked()
    {
        //if(selectedId != null)
        //{
        //    envProduct = [Select Id, Name, Product__c, License__c, Version__c From EnvironmentProduct__c where Id = :selectedId];
        //    RenderCreateTop = false;
        //}
        RenderCreateTop ^= true;
        NewModeORRenderCreateTop = true;
    }    
    public boolean IsNewEnvMode {get; set;}
    public boolean NewModeORRenderCreateTop {get;set;}
    public string SaveButtonText { get; set;}
    public boolean RenderCreateTop {get; set;}
    Public Environment__c env;
              
    private void NewEnvironmentProduct()
    {
        envProduct = new EnvironmentProduct__c();        
        RenderCreateTop = false;        
    }
    
    public EnvironmentProduct__c EnvironmentProduct
    {
        get { return envProduct; }
        set;
    }
    

    // Hide "Add Product" button and show the form
    public void ShowForm()
    {
        IsNewEnvMode = true;
        SaveButtonText='Add';
        NewModeORRenderCreateTop = true;
    }

  
    public void  refreshLicenseList(){
        list<selectOption> licenseListTemp = new list<selectOption>();
        licenseListTemp.add( new SelectOption ('--None--', '--None--') );
        if ( licenses == null || licenses.size() == 0 ){
            licenses = [select id, name, License_Reference__c, Full_Version__c, Account_License__r.product__c
                                                                        from Entitlement where Account.Id = :accountId order by Name];
        }
        if ( licenses != null && licenses.size() > 0 ){
            for ( Entitlement accLic : licenses ){
                //if ( envProduct != null && envProduct.Product__c != null && envProduct.Product__c == accLic.Product__c && 
                //                            envProduct.version__c != null && envProduct.version__c == accLic.Full_Version__c ){
                    licenseListTemp.add ( new SelectOption ( accLic.id, accLic.name  ) );
                //}
            }
        }
        system.debug('~~~~~Inside refreshLicenseList - 6 - licenseList.size()-' + licenseListTemp.size());
        
        this.licenseList = licenseListTemp;
    }
    
    @RemoteAction
    global static List<Entitlement> getLicenseList(string selectedAccountName) {
        
        system.debug('getLicenseList start' + selectedAccountName);
                
        List<Entitlement> licenseList;
        // Find out whether the current user is customer or partner
        User userRec;// {get; set;}
         
        if(userinfo.getUserId() != null)
        {   
            userRec = [Select Id, Name, FirstName, LastName, Email, ContactId, Contact.AccountId, Contact.Persona__c, Contact.Name, Contact.Account.Name, Contact.Account.IsPartner,
                            Contact.Account.IsCustomerPortal, Contact.Account.QT_Designated_Support_Contact__c, Contact.Account.Partner_Support_Contact__c
                            From User 
                            where Id = :userinfo.getUserId() 
                            LIMIT 1];
        }
        boolean isPartnerAccount = (userRec != null && userRec.ContactId != null && userRec.Contact.AccountId != null && userRec.Contact.Account.IsPartner != null) ? userRec.Contact.Account.IsPartner : false;
        if(isPartnerAccount && selectedAccountName!=null)
        {  
            licenseList = [Select Id, Name, AccountId, Account.Name, Status From Entitlement where Account.Name = :selectedAccountName Order By Name, Account.Name, Status LIMIT 1];
        }
        else
        {
             licenseList = [Select Id, Name, AccountId, Account.Name, Status From Entitlement Order By Name, Account.Name, Status LIMIT 1];
        } 
        return licenseList;
    }
}