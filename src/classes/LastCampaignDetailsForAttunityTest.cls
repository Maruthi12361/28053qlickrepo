/** File Name:LastCampaignDetailsForAttunityTriggerTest
*   Description : Test class for LastCampaignDetailsForAttunity
*   @author : Slalom LLC
*   Modification Log *
Ver     Date         Author         Modification 
1.0 Aug 28 2019 Slalom LLC     Added initial test cases.
*/

@isTest
public without sharing class LastCampaignDetailsForAttunityTest {
    @isTest static void testData() {
        //Create Custom Settings
        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
		insert setting;

        //Create Campaigns
        Campaign newCamp = new Campaign();
        newCamp.Name = 'RDZTestCampaign';
        newCamp.RecordTypeId = '01220000000HR2l';
        newCamp.Type = 'WB - Web';
        newCamp.Planned_Opportunity_Value__c = 10000;
        insert newCamp;

        //Create Contacts
        Contact newContact = new Contact();
        newContact.FirstName = 'RDZTest';
        newContact.LastName = 'RDZTest';
        insert newContact;

        //Create Leads
        Lead newLead = new Lead();
        newLead.FirstName = 'RDZTestLead';
        newLead.LastName = 'RDZTestLead';
        newLead.Country = 'USA';
        newLead.Email = 'test@test.com';
        newLead.Company = 'TestCompany'+1;
        newLead.Overall_Follow_up_Required__c = false;
        insert newLead;

        CampaignMember cmLead = new CampaignMember();
        cmLead.LeadId = newLead.Id;
        cmLead.CampaignId = newCamp.Id;
        cmLead.Status = 'Responded';
        
        CampaignMember cmContact = new CampaignMember();
        cmContact.ContactId = newContact.Id;
        cmContact.CampaignId = newCamp.Id;
        //cmContact.Status = 'Responded';

        List<CampaignMember> cmList = new List<CampaignMember>();
        cmList.add(cmLead);
        cmList.add(cmContact);
        insert cmList;

        List<Lead> leadList = [SELECT Id, Last_Campaign_Response_Name__c, Last_Campaign_Response_Date__c
                                FROM Lead WHERE Id = :newLead.Id];
        
        if(leadList.size() > 0){
            Lead testLead = leadList.get(0);
            System.assertEquals('RDZTestCampaign', testLead.Last_Campaign_Response_Name__c);
        }

        List<Contact> contactList = [SELECT Id, Last_Campaign_Response_Name__c, Last_Campaign_Response_Date__c
                                    FROM Contact WHERE Id = :newContact.Id];

        if(contactList.size() > 0){
            Contact testCon = contactList.get(0);
            System.assertNotEquals(null, testCon.Last_Campaign_Response_Name__c);
        }
    }

}