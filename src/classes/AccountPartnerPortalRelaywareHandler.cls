/*********************************************
AccountPartnerPortalRelaywareHandler

Description to handled trigger: 
			When Partner Account fields ERP Status or Terminated are
            edited on Account record then
            set Relayware fields.

Log History:
2016-10-24    BAD    Created
**********************************************/

public with sharing class AccountPartnerPortalRelaywareHandler {
	public AccountPartnerPortalRelaywareHandler() {
		
	}

	public static void handle(List<Account> triggerNew, List<Account> triggerOld) {
		System.debug('AccountPartnerPortalRelaywareHandler: Starting');

        if(!Semaphores.TriggerHasRun('AccountPartnerPortalRelaywareHandler')) //might not work in
        {
        	List<Id> AccountsApprovedList = new List<Id>();
        	List<Id> AccountsTerminatedList = new List<Id>();
			List<Account> AccountsToUpdate = new List<Account>();

			for (Integer i = 0; i < triggerNew.size(); i++)
			{
				System.debug('BAD check NavisionStatus Old ' + triggerOld[i].Navision_Status__c);
				System.debug('BAD check NavisionStatus New ' + triggerNew[i].Navision_Status__c);
				System.debug('BAD check LeftComp Old ' + triggerOld[i].Terminated__c);
				System.debug('BAD check LeftComp New ' + triggerNew[i].Terminated__c);
				// Check if Account has been approved
				if( triggerOld[i].Navision_Status__c != 'Partner' && 
					triggerNew[i].Navision_Status__c == 'Partner' &&
					!triggerNew[i].Terminated__c)
				{
						AccountsApprovedList.add(triggerNew[i].Id);
				}

				// Check if Account has been terminated
				if( (triggerNew[i].Terminated__c != triggerOld[i].Terminated__c &&
					 triggerNew[i].Terminated__c) || 
					(triggerOld[i].Navision_Status__c == 'Partner' &&
					 triggerNew[i].Navision_Status__c != 'Partner'))
				{
						AccountsTerminatedList.add(triggerNew[i].Id);
				}
			}


			// Prep Accounts that has been approved
		    for (Account acc : [SELECT Id, Synch_with_Relayware__c, Account_Approval_Status__c FROM Account WHERE  Id IN :AccountsApprovedList])
		    {       
		        acc.Synch_with_Relayware__c = true; 
		        acc.Account_Approval_Status__c = true; 
		        AccountsToUpdate.add(acc);
		    }

			// Prep Accounts that has been terminated
		    for (Account acc : [SELECT Id, Synch_with_Relayware__c, Account_Approval_Status__c FROM Account WHERE  Id IN :AccountsTerminatedList])
		    {       
		        acc.Account_Approval_Status__c = false; 
		        AccountsToUpdate.add(acc);
		    }

		    update(AccountsToUpdate);

		    //Approve Contacts associated to the Accounts 
		    if(AccountsApprovedList.size() > 0)
		    {
		    	Database.executeBatch(new AccountUpdateContactRelaywareBatch(AccountsApprovedList), 100);  
		    }

        }

	}

}