/**************************************************************
* Change Log:
* 2019-12-12   extbad   IT-2329    Initial Development
* 2020-02-11   extbad   IT-2479    Disable changes to 'Chat Resolved' Status also
***************************************************************/
@IsTest
private class CaseClosedAttunityHandlerTest {
    static testMethod void testCases() {
        String dtnow = '' + System.now();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName' + dtnow, qtc);
        insert testAccount;
        Contact testContact = TestDataFactory.createContact('test_Thomas' + dtnow, 'test_Jones' + dtnow, 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;
        Profile profileRec = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Case Logging Access - Attunity' LIMIT 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        User communityUser = TestDataFactory.createUser(profileId, 'tSboxAttunityCs@qlikTech.com', 'tSboxAttunityCs', String.valueOf(testContact.Id));
        insert communityUser;

        Case cs = new Case();
        System.runAs(communityUser) {
            cs.Product__c = 'Not product related';
            cs.OwnerId = UserInfo.getUserId();
            insert cs;
        }

        cs.Status = CaseClosedAttunityHandler.CASE_CLOSED_STATUSES.get(0);
        update cs;

        Test.startTest();
        String error = '';
        System.runAs(communityUser) {
            try {
                cs.Severity__c = '2';
                Semaphores.CustomCaseTriggerBeforeUpdate = false;
                update cs;
            } catch (DmlException ex) {
                error = ex.getMessage();
            }
        }
        Test.stopTest();

        System.assert(String.isNotBlank(error));
        System.assert(error.contains(CaseClosedAttunityHandler.ATTUNITY_CASE_ERROR_MESSAGE));
        cs = [SELECT Id, Severity__c FROM Case WHERE Id = :cs.Id];
        System.assertNotEquals('2', cs.Severity__c);
    }

    static testMethod void testChatResolvedCases() {
        String dtnow = '' + System.now();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName' + dtnow, qtc);
        insert testAccount;
        Contact testContact = TestDataFactory.createContact('test_Thomas' + dtnow, 'test_Jones' + dtnow, 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;
        Profile profileRec = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Case Logging Access - Attunity' LIMIT 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        User communityUser = TestDataFactory.createUser(profileId, 'tSboxAttunityCs@qlikTech.com', 'tSboxAttunityCs', String.valueOf(testContact.Id));
        insert communityUser;

        Case cs = new Case();
        System.runAs(communityUser) {
            cs.Product__c = 'Attunity Visibility';
            cs.OwnerId = UserInfo.getUserId();
            insert cs;
        }

        cs.Status = CaseClosedAttunityHandler.CASE_CLOSED_STATUSES.get(1);
        update cs;

        Test.startTest();
        String error = '';
        System.runAs(communityUser) {
            try {
                cs.Severity__c = '2';
                Semaphores.CustomCaseTriggerBeforeUpdate = false;
                update cs;
            } catch (DmlException ex) {
                error = ex.getMessage();
            }
        }
        Test.stopTest();

        System.assert(String.isNotBlank(error));
        System.assert(error.contains(CaseClosedAttunityHandler.ATTUNITY_CASE_ERROR_MESSAGE));
        cs = [SELECT Id, Severity__c FROM Case WHERE Id = :cs.Id];
        System.assertNotEquals('2', cs.Severity__c);
    }
}