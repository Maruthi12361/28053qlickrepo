/******************************************************
Name:  Kumar Navneet 

Purpose:
-------
    Updating opportunity values
======================================================

   Date : 2017/03/15    
****Rodion Vakulovskyi 15/03/2017 : Too many Soql error****
Date : 2017/03/16    
****Ramakrishna 15/03/2017 : Cleaned up code written by navneet to prevent unnecessary SOQL runs****

Date : 2017/05/17   
Kumar Navneet - Updated the SPD Eligibility for PAEX-72

Date : 26/07/2017
Pramod Kumar  - Updated Partner Fee% and Partner Fee Type to default values(PAEX 71)

Date : 2017/08/28   
Kumar Navneet - commented PAEX-72 related changes

Date : 2017/11/2  
Sergii Grushai - QCW-4269 comment out set value to "5" in field "Partner Fee %"

Date : 2018/4/12
Shubham Gupta - BSL-195 Commenting out population of "Partner_Fee_Level__c" line 150 and line 278

******************************************************/

public class OpportunityPartnerprogramHandler {
    public static Boolean onInsertFlag = false;
    public static Boolean onUpdateFlag = false;

  public OpportunityPartnerprogramHandler() {    
  }

    public static Boolean Inserthandler(List<Opportunity> triggerNew) {      
        Map<ID, String> prtnConMap = new Map<ID, String>();      
        Set<Id> prtnCnIdSet = new Set<Id>();
        List<Id> prtnCnIdlist = new List<Id>(); 

        Map<ID, Contact> prtnConMap1 = new Map<ID, Contact>();  
        //Map<ID, String> prtnConMap2 = new Map<ID, String>();    
        Set<Id> prmRefCnIdSet = new Set<Id>();
        List<Id> prmRefCnIdList = new List<Id>();  
          Set<Id> Spdset = new Set<Id>();
        List<Id> Spdlist = new List<Id>();  
        Set<Id> Spset = new Set<Id>();
        List<Id> Splist = new List<Id>(); 
        Map<ID, Account> SpdaccMap = new Map<ID, Account>();
        Map<ID, Account> SpaccMap = new Map<ID, Account>();   
        
        for (Opportunity opp : triggerNew){
        
           if (String.isNotBlank(opp.Sell_Through_Partner__c)) 
                Spdset.add(opp.Sell_Through_Partner__c);
                            
           if (String.isNotBlank(opp.Second_Partner__c)) 
                Spset.add(opp.Second_Partner__c);         

            if(String.isNotBlank(opp.PRM_Referring_Contact__c))
                prmRefCnIdSet.add(opp.PRM_Referring_Contact__c);
        }
     
        prtnCnIdlist.addAll(prtnCnIdSet);
        prmRefCnIdList.addAll(prmRefCnIdSet);
        
        if(!prtnCnIdlist.isEmpty()){
            for(Contact con : [Select Id, Account.name FROM Contact WHERE Id IN:prtnCnIdlist ]){
                prtnConMap.put(con.Id,con.Account.name);
            }
        }
        if(!prmRefCnIdList.isEmpty()){   
            for(Contact con : [Select Id, Account.name , Account.Partner_Type__c FROM Contact WHERE Id IN:prmRefCnIdList]){
                prtnConMap1.put(con.Id, con);
                //prtnConMap2.put(con.Id,con.Account.Partner_Type__c);
            }  
        }  
        
   /*  Kumar Navneet - commented PAEX-72 related changes
   
       if(!Spdset.isEmpty()){
            Spdlist.addAll(Spdset);
            if(!Spdlist.isEmpty()){            
                for(Account Accc : [Select Id, Partner_Type__c ,Program_Version__c FROM Account WHERE Id IN:Spdlist ]){
                    SpdaccMap.put(Accc.Id, Accc );
                }
            }
        }
        if(!Spset.isEmpty()){
            Splist.addAll(Spset);
            if(!Splist.isEmpty()){            
                for(Account Accc : [Select Id, Partner_Type__c, Program_Version__c FROM Account WHERE Id IN:Splist ]){
                    SpaccMap.put(Accc.Id, Accc );
                }
            }
        }
       */
                    
        for (Opportunity opp : triggerNew){  
        
  /* Kumar Navneet - commented PAEX-72 related changes
     if(opp.Revenue_Type__c != null && !SpdaccMap.isEmpty() && opp.Revenue_Type__c.contains('Reseller') ){                 
                if(SpdaccMap.containsKey(opp.Sell_Through_Partner__c)){                   
                    if((SpdaccMap.get(opp.Sell_Through_Partner__c).Partner_Type__c == 'SP - Elite' || SpdaccMap.get(opp.Sell_Through_Partner__c).Partner_Type__c == 'SP - Solution Provider') && (SpdaccMap.get(opp.Sell_Through_Partner__c).Program_Version__c == 'The Partner Program (new)') ) {
                        opp.Partner_Bonus__c = True ;
                    }else
                    {
                    opp.Partner_Bonus__c = False ;
                    } 
                 }else
                  {
                   opp.Partner_Bonus__c = False ;
                  } 
  
          }
          else {
              if(opp.Revenue_Type__c != null && !SpaccMap.isEmpty()  && opp.Revenue_Type__c.contains('Distributor')){
                 if(SpaccMap.containsKey(opp.Second_Partner__c)){                   
                    if((SpaccMap.get(opp.Second_Partner__c).Partner_Type__c == 'SP - Elite' || SpaccMap.get(opp.Second_Partner__c).Partner_Type__c == 'SP - Solution Provider' ) && (SpaccMap.get(opp.Second_Partner__c).Program_Version__c == 'The Partner Program (new)') ) {
                        opp.Partner_Bonus__c = True ;
                        opp.Partner_Bonus__c = True ;
                    }else
                    {
                    opp.Partner_Bonus__c = False ;
                    } 
                 }else
                  {
                   opp.Partner_Bonus__c = False ;
                  } 
                 } 
            else
                {
                   opp.Partner_Bonus__c = False ;
                }             
            }           
     */
                  

            if(!prtnConMap1.isEmpty()){ 
                if(prtnConMap1.containsKey(opp.PRM_Referring_Contact__c)){
                    opp.PRM_Referring_Partner__c = prtnConMap1.get(opp.PRM_Referring_Contact__c).account.name;
                    //opp.Partner_Fee_Level__c = prtnConMap1.get(opp.PRM_Referring_Contact__c).Account.Partner_Type__c;
                    opp.Partner_Fee_Type__c= 'Finder’s Fee';
                    //opp.Standard_Referral_Margin__c='5';
                    
                }
            }            
        }     
        return true;
    }
     
    public static Boolean Updatehandler(List<Opportunity> triggerNew , Map<Id, Opportunity> triggerNewMap ,  Map<Id, Opportunity> triggerOldMap) {        
        Map<ID, String> prtnConMap = new Map<ID, String>();
        Set<Id> prtnCnIdSet = new Set<Id>();
        List<Id> prtnCnIdlist = new List<Id>(); 

        Map<ID, Contact> prtnConMap1 = new Map<ID, Contact>();  
        Set<Id> prtnCnIdSet1 = new Set<Id>();
        List<Id> prtnCnIdlist1 = new List<Id>();    
       Set<Id> Spdset = new Set<Id>();
        List<Id> Spdlist = new List<Id>();  
        Set<Id> Spset = new Set<Id>();
        List<Id> Splist = new List<Id>(); 
        Map<ID, Account> SpdaccMap = new Map<ID, Account>();
        Map<ID, Account> SpaccMap = new Map<ID, Account>();  
        
        for (Opportunity opp : triggerNew){
        
           if (String.isNotBlank(opp.Sell_Through_Partner__c)) 
                Spdset.add(opp.Sell_Through_Partner__c);
                            
           if (String.isNotBlank(opp.Second_Partner__c)) 
                Spset.add(opp.Second_Partner__c);         


            if (String.isNotBlank(opp.PRM_Referring_Contact__c) &&
                    triggerNewMap.get(opp.Id).PRM_Referring_Contact__c != 
                        triggerOldMap.get(opp.Id).PRM_Referring_Contact__c)
                prtnCnIdSet1.add(opp.PRM_Referring_Contact__c);
        }
        if(!prtnCnIdSet.isEmpty() || !prtnCnIdSet1.isEmpty()){
            prtnCnIdlist.addAll(prtnCnIdSet);
            prtnCnIdlist1.addAll(prtnCnIdSet1);
            if(!prtnCnIdlist.isEmpty()){
                for(Contact con : [Select Id, Account.name FROM Contact WHERE Id IN:prtnCnIdlist ]){
                    prtnConMap.put(con.Id,con.Account.name);
                }
            }
            if(!prtnCnIdlist1.isEmpty()){   
                for(Contact con : [Select Id, Account.name , accountId, Account.Partner_Type__c FROM Contact WHERE Id IN:prtnCnIdlist1 ]){
                    prtnConMap1.put(con.Id, con);
                }
            }
        } 
        
         
      /* Kumar Navneet - commented PAEX-72 related changes
         if(!Spdset.isEmpty()){
            Spdlist.addAll(Spdset);
            if(!Spdlist.isEmpty()){            
                for(Account Accc : [Select Id, Partner_Type__c ,Program_Version__c FROM Account WHERE Id IN:Spdlist ]){
                    SpdaccMap.put(Accc.Id, Accc );
                }
            }
        }
        if(!Spset.isEmpty()){
            Splist.addAll(Spset);
            if(!Splist.isEmpty()){            
                for(Account Accc : [Select Id, Partner_Type__c, Program_Version__c FROM Account WHERE Id IN:Splist ]){
                    SpaccMap.put(Accc.Id, Accc );
                }
            }
        }  */

        
                    
        for (Opportunity opp : triggerNew){  
        
      /*  Kumar Navneet - commented PAEX-72 related changes
        if(opp.Revenue_Type__c != null && !SpdaccMap.isEmpty() && opp.Revenue_Type__c.contains('Reseller') ){                 
                if(SpdaccMap.containsKey(opp.Sell_Through_Partner__c)){                   
                    if((SpdaccMap.get(opp.Sell_Through_Partner__c).Partner_Type__c == 'SP - Elite' || SpdaccMap.get(opp.Sell_Through_Partner__c).Partner_Type__c == 'SP - Solution Provider') && (SpdaccMap.get(opp.Sell_Through_Partner__c).Program_Version__c == 'The Partner Program (new)') ) {
                        opp.Partner_Bonus__c = True ;
                    }else
                    {
                    opp.Partner_Bonus__c = False ;
                    } 
                 }else
                  {
                   opp.Partner_Bonus__c = False ;
                  } 
  
          }
          else {
              if(opp.Revenue_Type__c != null && !SpaccMap.isEmpty()  && opp.Revenue_Type__c.contains('Distributor')){
                 if(SpaccMap.containsKey(opp.Second_Partner__c)){                   
                    if((SpaccMap.get(opp.Second_Partner__c).Partner_Type__c == 'SP - Elite' || SpaccMap.get(opp.Second_Partner__c).Partner_Type__c == 'SP - Solution Provider' ) && (SpaccMap.get(opp.Second_Partner__c).Program_Version__c == 'The Partner Program (new)') ) {
                        opp.Partner_Bonus__c = True ;
                        opp.Partner_Bonus__c = True ;
                    }else
                    {
                    opp.Partner_Bonus__c = False ;
                    } 
                 }else
                  {
                   opp.Partner_Bonus__c = False ;
                  } 
                 } 
            else
                {
                   opp.Partner_Bonus__c = False ;
                }             
            }           

         */
        

            if (!prtnConMap1.isEmpty() && triggerNewMap.get(opp.Id).PRM_Referring_Contact__c != triggerOldMap.get(opp.Id).PRM_Referring_Contact__c){
                if(prtnConMap1.containsKey(opp.PRM_Referring_Contact__c)){
                    opp.PRM_Referring_Partner__c = prtnConMap1.get(opp.PRM_Referring_Contact__c).Account.Name;
                    //opp.Partner_Fee_Level__c = prtnConMap1.get(opp.PRM_Referring_Contact__c).Account.Partner_Type__c;
                    opp.Partner_Fee_Type__c='Finder’s Fee';
                    //opp.Standard_Referral_Margin__c='5';
                    
                }  
            }
            /*if(!prtnConMap2.isEmpty() && prtnConMap2.containsKey(opp.PRM_Referring_Contact__c)){                  
                opp.Partner_Fee_Partner_Level__c = prtnConMap2.get(opp.PRM_Referring_Contact__c);
            } */
            if(String.isBlank(opp.PRM_Referring_Contact__c)){
                opp.PRM_Referring_Partner__c = '';
                opp.Partner_Fee_Level__c = '';
               // opp.Partner_Fee_Type__c='';
                //opp.Standard_Referral_Margin__c='';
                
            }
        }  
        return true;
        //}
    }
}