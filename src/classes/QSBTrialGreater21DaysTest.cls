/*************************************************************************************************************
 Name: QSBTrialGreater21DaysTest
 Author: CCE
 Purpose: This is test class for QSBTrialGreater21Days
  
 Log History:
 2020-03-11 CHG0037726 DGM-55 FUR Revamp 2020 Initial Development
*************************************************************************************************************/
@isTest
private class QSBTrialGreater21DaysTest {
	
	static testmethod void QSBTrialGreater21Days_TestLead()
    {
        Test.startTest();
        QSBTrialGreater21Days.scheduleMe();
        QSBTrialGreater21Days qsbt = new QSBTrialGreater21Days();

        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(mockSysAdmin)
        {
            Lead l = New Lead (
                FirstName = 'Chris',
                LastName = 'QSBTrialGreater21DaysBatchLead',
                Company = 'Test',
                Country = 'Sweden',
                Email = 'asd@ad.com',
                QSB_Trial__c = true
            );  
            insert l;
            Lead retLead = [SELECT Id, QSB_Trial_Start_Date__c FROM Lead WHERE Id=:l.id];
            System.Debug('QSBTrialGreater21DaysTest:QSBTrialGreater21Days_TestLead: Date = ' + retLead.QSB_Trial_Start_Date__c);

            //This test requires that the QSB_Trial__c field has been true for more than 21 days.
            // As we can't wait that long in this test :) we will fake it by setting the QSB_Trial_Start_Date__c 
            // date field 23 days into the past.

            l.QSB_Trial_Start_Date__c = System.today() - 23;
            update l;
            retLead = [SELECT Id, QSB_Trial_Start_Date__c, QSB_Trial__c, Trial_QSB_Trial_greater_21__c FROM Lead WHERE Id=:l.id];
            System.Debug('QSBTrialGreater21DaysTest:QSBTrialGreater21Days_TestLead: Date (after updating) = ' + retLead.QSB_Trial_Start_Date__c);
        
            qsbt.execute(null);            
            Test.stopTest();
            retLead = [SELECT Id, QSB_Trial__c, Trial_QSB_Trial_greater_21__c FROM Lead WHERE Id=:l.id];
            System.Debug('QSBTrialGreater21DaysTest:QSBTrialGreater21Days_TestLead: lead = ' + retLead);
            system.assertEquals(true, retLead.Trial_QSB_Trial_greater_21__c);
            system.assertEquals(false, retLead.QSB_Trial__c);
        }      
    }
    static testmethod void QSBTrialGreater21Days_TestContact()
    {
        Test.startTest();
        QSBTrialGreater21Days qsbt = new QSBTrialGreater21Days();

        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(mockSysAdmin)
        {
            Account acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
            system.assert(acct.Id != null);
            //Contact ct = QTTestUtils.createMockContact(acct.Id);
            Contact ct = new Contact();
            ct.FirstName = 'Chris';
            ct.LastName = 'QSBTrialGreater21DaysBatchContact';
            ct.AccountId = acct.Id;
            insert ct;
            system.assert(ct.Id != null);            
            ct.QSB_Trial__c = true; //start the Trial
            update ct;
            Contact retCon = [SELECT Id, QSB_Trial_Start_Date__c FROM Contact WHERE Id=:ct.id];
            System.Debug('QSBTrialGreater21DaysTest:QSBTrialGreater21Days_TestContact: Date = ' + retCon.QSB_Trial_Start_Date__c);

            //This test requires that the QSB_Trial__c field has been true for more than 21 days.
            // As we can't wait that long in this test :) we will fake it by setting the QSB_Trial_Start_Date__c 
            // date field 23 days into the past.

            ct.QSB_Trial_Start_Date__c = System.today() - 23;
            update ct;
            retCon = [SELECT Id, QSB_Trial_Start_Date__c, QSB_Trial__c, Trial_QSB_Trial_greater_21__c FROM Contact WHERE Id=:ct.id];
            System.Debug('QSBTrialGreater21DaysTest:QSBTrialGreater21Days_TestContact: Date (after updating) = ' + retCon.QSB_Trial_Start_Date__c);
        
            qsbt.execute(null);            
            Test.stopTest();
            retCon = [SELECT Id, QSB_Trial__c, Trial_QSB_Trial_greater_21__c FROM Contact WHERE Id=:ct.id];
            System.Debug('QSBTrialGreater21DaysTest:QSBTrialGreater21Days_TestContact: contact = ' + retCon);
            system.assertEquals(true, retCon.Trial_QSB_Trial_greater_21__c);
            system.assertEquals(false, retCon.QSB_Trial__c);
        }
    }
	
}