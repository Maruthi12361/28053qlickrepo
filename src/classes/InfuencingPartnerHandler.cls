/******************************************************
Name:  Kumar Navneet 

Purpose:
-------
    Updating currency
======================================================

   Date : 2017/04/20   
OTQ-346 2020/3/18 - Shubham Gupta modifying invokeApprovalProcessHandler to update email to oppty owner approval manager's
******************************************************/

public without sharing class InfuencingPartnerHandler {
  

    public InfuencingPartnerHandler () {    
    }

    public static Boolean Inserthandler(List<Influencing_Partner__c> triggerNew) { 
  
        Map<ID, String> OppMap = new Map<ID, String>();      
        Set<Id> InfluenceSet = new Set<Id>();     
        
        for (Influencing_Partner__c Ipc : triggerNew){
            if(String.isNotBlank(Ipc.Opportunity__c))
                InfluenceSet.add(Ipc.Opportunity__c);         
        }
     
        for(Opportunity opp : [Select Id, CurrencyIsoCode FROM Opportunity WHERE Id IN:InfluenceSet ]){
            OppMap.put(opp.Id,opp.CurrencyIsoCode);
        }   
      
        for (Influencing_Partner__c Ipc : triggerNew){
            if(OppMap.containsKey(Ipc.Opportunity__c)){
                Ipc.currencyisocode = OppMap.get(Ipc.Opportunity__c);
            }
        }     
        
        updatePartnerAccountHandler(triggerNew);
        
        return true;
    }
    
    public static void updatePartnerAccountHandler(List<Influencing_Partner__c> triggerNew){
        List<String> Ids = new List<String>();
        for (Influencing_Partner__c ip:triggerNew)
        {
            if (ip.Influencing_Partner_Contact__c !=null)
            {
                ids.add(ip.Influencing_Partner_Contact__c);
            }
        }
        
        Map<Id, Contact> contactsMap = New Map<Id, Contact>([Select Account.OwnerId, AccountId FROM Contact WHERE Id In :Ids]);
        
        for (Influencing_Partner__c ip:triggerNew)
        {           
            if (ip.Influencing_Partner_Contact__c != null)
            {
                system.debug('InfuencingPartnerContactClass: Infuencing Partner contact Id = ' + ip.Influencing_Partner_Contact__c);
                
                Contact pContact = contactsMap.get(ip.Influencing_Partner_Contact__c);
                
                system.debug('InfuencingPartnerContactClass: Partner contact is ' + pContact);
                
                ip.Influencing_PSM_QM__c = pContact.Account.OwnerId;
                ip.Influencing_Partner_Account__c = pContact.AccountId;
            }           
        }
    }
    
    public static void invokeApprovalProcessHandler(List<Influencing_Partner__c> triggerNew, Map<Id,Influencing_Partner__c> triggerOldMap){
        List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
        Set<Id> oppIds = new Set<Id>();
        Map<Id, Id> oppToOwnerManager = new Map<Id,Id>();
         Map<Id, String> ownerManagerToEmail = new Map<Id,String>();
        
        for(Influencing_Partner__c ip: triggerNew){
            if(ip.Referral__c && ((triggerOldMap != null && triggerOldMap.get(ip.Id).Referral__c == false) || triggerOldMap == NULL))
                oppIds.add(ip.opportunity__c);
        }
        system.debug('@@@oppownercame'+oppIds);
        
        if(oppIds.size() > 0){
            for(Opportunity opty: [select id,owner.Approval_Manager__c from opportunity where id In:oppIds]){
                oppToOwnerManager.put(opty.Id, opty.owner.Approval_Manager__c);
            }
            
            Set<id> appovalManager = new Set<id>();
            appovalManager.addAll(oppToOwnerManager.values());
            system.debug('@@@oppownermapcame'+oppToOwnerManager);
            
            For(User u:[select id,email from User where id IN:appovalManager]){
                ownerManagerToEmail.put(u.id,u.email);
            }
            List<Influencing_Partner__c> approverEmailUpdateList = new List<Influencing_Partner__c>();
            
            for(Influencing_Partner__c ip: triggerNew){
                if(ip.Referral__c){
                    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
                    req.setComments('Submitted for approval. Please approve.');
                    req.setObjectId(ip.Id);
                    approvalReqList.add(req);  
                    Influencing_Partner__c infPartner = new Influencing_Partner__c(id=ip.Id, Approval_email__c=ownerManagerToEmail.get(oppToOwnerManager.get(ip.Opportunity__c)));
                    approverEmailUpdateList.add(infPartner);
                }

                if(approverEmailUpdateList.size() > 0)
                    update approverEmailUpdateList;
                // submit the approval request for processing     
                if(approvalReqList.size() > 0){
                    List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
                    // display if the reqeust was successful
                    for(Approval.ProcessResult result: resultList ){        
                        System.debug('Submitted for approval successfully: '+result.isSuccess());      
                    }
                }   
            }
        }
    }
     
     
  /*  public static Boolean Updatehandler(List<Influencing_Partner__c> triggerNew , Map<Id, Influencing_Partner__c> triggerNewMap ,  Map<Id, Influencing_Partner__c> triggerOldMap) {        
      
        Map<ID, String> OppMap = new Map<ID, String>();      
        Set<Id> InfluenceSet = new Set<Id>();     
        
        for (Influencing_Partner__c Ipc : triggerNew)
      {
         InfluenceSet.add(Ipc.Opportunity__c);         
      }
     
      for(Opportunity opp : [Select Id, CurrencyIsoCode FROM Opportunity WHERE Id IN:InfluenceSet ]){
         OppMap.put(opp.Id,opp.CurrencyIsoCode);
       }   
      
       for (Influencing_Partner__c Ipc : triggerNew)
       {
        if (triggerNewMap.get(Ipc.Id).currencyisocode != triggerOldMap.get(Ipc.Id).currencyisocode){
          if(OppMap.containsKey(Ipc.Opportunity__c))
          {
           Ipc.currencyisocode = OppMap.get(Ipc.Opportunity__c);
          }
         }
      
       }     
        
       
      return true;
     }
     */
}