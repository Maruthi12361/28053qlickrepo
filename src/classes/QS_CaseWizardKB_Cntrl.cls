/*******************************************
*
*   2017-09-07 AIN Added support for articlenumber parameter, article number can also be specified in the id parameter
*   2018-01-29 ext_bad  Add params to initialize live chat.
*   2018-12-11 ext_bad  Add article feedback saving.
*   2019-02-26 ext_bad  Remove 'articleId' param from the Article (added by Salesforce to smart links).
*   2019-03-19 ext_vos - CHG0035417: add variable for canonical URL (for correct indexing)
*   2019-04-17 extbad   Change current article ID to article title for smart links 
*   2019-05-05 AIN  IT-1597 Updated for Support Portal Redesign
*   2019-05-13 extbad  IT-1685 Add Article_Feedback Record Type for QS_DataCapture
*   2019-06-21 ext_ciz CHG0036302 Add "LastModifiedDate" field in select request
*   2019-11-21 AIN IT-2320 Added filter expression for Coveo
*   2020-02-06 extcqb IT-2418 New error message 'Article not found'
*   2020-03-05 extcqb IT-2537 Use article Id instead of version Id
*******************************************/
public class QS_CaseWizardKB_Cntrl {

    public Basic__kav basicArticleType { get; set; }
    public Diagnostic__kav diagnosticArticleType { get; set; }
    public KnowledgeArticleVersion knowledgeArticleVerRecord { get; set; }
    public String caseDetailUrl { get; set; }
    public String commentText { get; set; }
    public Boolean isAttachment1 { get; set; }
    public Boolean isAttachment2 { get; set; }
    public Boolean isAttachment3 { get; set; }
    public Boolean showException { get; set; }
    public Boolean showArticleNotFoundError { get; set; }
    public Boolean unauthenticatedUser { get; set; }
    public Boolean guestUser { get; set; }
    public Boolean invokeCaseWizard { get; set; }
    public Boolean isCaseExists { get; set; }
    public Boolean isOpenCase { get; set; }
    public Boolean hideFeedback { get; set; }
    public Boolean showComment { get; set; }
    public Boolean showThankYou { get; set; }
    public Boolean showCommentError { get; set; }
    public Boolean emailSent;
    public Case caseObj;
    public string CoveoFilterExpression { get; set; }

    private Id recTypeId;
    private Boolean helped;
    private User userRec;
    private String userContactId;
    private String userAccountId;
    private String articleId;
    private String articleNumber;
    private QTCustomSettingsHier__c qtSettings;
    private String dataCaptureRecordTypeId;

    // Constants used in assigning case origin
    private static final String DATA_CAPTURE_RECORD_TYPE = 'Article Feedback';
    private static final String PARTNER_PORTAL_ORIGIN = 'Partner Portal';
    private static final String CUSTOMER_PORTAL_ORIGIN = 'Customer Portal';
    private static final String STATUS_CLOSED = 'Closed';
    private static final String STATUS_COMPLETED = 'Completed';
    private static final String STATUS_RESOLVED = 'Resolved';
    private static final String KNOWLEDGE_MANAGERS_GROUP = 'KnowledgeManagers';
    private static final String CASE_ORIGIN_SUPPORT_PORTAL = 'Customer Support Portal';
    private static final String ARTICLE_RECORD_TYPE = 'Article Feedback';
    private static final String ARTICLE_FEEDBACK_TEMPLATE = 'Article_Feedback_Email_Template';
    private static final String ARTICLE_ID_PARAMETER = 'articleId';
    private static final String ID_PARAMETER = 'id';
    private static final String ARTICLE_NUMBER_PARAMETER = 'articlenumber';
    private static final String CASE_ID_PARAMETER = 'caseId';
    private static final String UNAUTHENTICATED_PARAMETER = 'unauthenticated';
    private static final String FROM_CASE_WIZARD_PARAMETER = 'fromCaseWizard';
    private static final String RECORD_TYPE_NAME_PARAMETER = 'recordTypeName';
    private static final String CATEGORY_ID_PARAMETER = 'categoryId';
    private static final String AREA_PARAMETER = 'area';
    private static final String ISSUE_PARAMETER = 'issue';
    private static final String SUBJECT_PARAMETER = 'subject';
    private static final String SEVERITY_PARAMETER = 'severity';
    private static final String DESCRIPTION_PARAMETER = 'description';
    private static final String PORTAL_GUEST_USER_ID = '005D0000004wTRs';
    private static final String UNASSIGNED_QUEUE_NAME = 'Unassigned';
    private static final String CASE_DEFLECTED_CODE = 'Case Deflected';
    private static final String ERROR_MESSAGE = 'Something went wrong.. Please try again!!';
    private static final String UTF8 = 'UTF-8';
    private static final String NEWLINE_TO_REPLACE = '_newline_';
    private static final String QUESTION_MARK_TO_REPLACE = '_questionmark_';
    private static final String ARTICLE_BASIC_TAG_START = '<a href="/articles/Basic/';
    private static final String ARTICLE_DIAGNOSTIC_TAG_START = '<a href="/articles/Diagnostic/';
    private static final String ARTICLE_BASIC_TAG_MODIFIED_START = '<a href="_MODIFIED_/articles/Basic/';
    private static final String ARTICLE_DIAGNOSTIC_TAG_MODIFIED_START = '<a href="_MODIFIED_/articles/Diagnostic/';
    private static final String ARTICLE_MODIFIED_TO_REMOVE = '_MODIFIED_';

    private static final String DATE_SUBMITTED = 'DATE_SUBMITTED';
    private static final String CASE_LINK = 'CASE_LINK';
    private static final String CASE_SUBJECT = 'CASE_SUBJECT';
    private static final String CASE_DESCRIPTION = 'CASE_DESCRIPTION';
    private static final String USER_NAME = 'USER_NAME';
    private static final String USER_ID = 'USER_ID';
    private static final String USER_LINK = 'USER_LINK';
    private static final String CONTACT_LINK = 'CONTACT_LINK';
    private static final String CONTACT_NAME = 'CONTACT_NAME';
    private static final String CONTACT_EMAIL = 'CONTACT_EMAIL';
    private static final String ARTICLE_LINK = 'ARTICLE_LINK';
    private static final String ARTICLE_NAME = 'ARTICLE_NAME';
    private static final String ARTICLE_NUMBER = 'ARTICLE_NUMBER';


    private static final String PARTIAL_ARTICLE_URL = '/articles/';

    //Fields on the object will not show in VF when logged in as guest, using workaround
    public string description {
        get {
            return basicArticleTypeDescription != null ? basicArticleTypeDescription : diagnosticArticleTypeDescription != null ? diagnosticArticleTypeDescription : null;
        }
    }
    public string cause {
        get {
            return basicArticleTypeCause != null ? basicArticleTypeCause : diagnosticArticleTypeCause != null ? diagnosticArticleTypeCause : null;
        }
    }
    public string resolution {
        get {
            return basicArticleTypeResolution != null ? basicArticleTypeResolution : diagnosticArticleTypeResolution != null ? diagnosticArticleTypeResolution : null;
        }
    }
    public string knowledgeArticleId {
        get {
            return basicArticleTypeKnowledgeArticleId != null ? basicArticleTypeKnowledgeArticleId : diagnosticArticleTypeKnowledgeArticleId != null ? diagnosticArticleTypeKnowledgeArticleId : null;
        }
    }
    public String basicArticleTypeDescription {
        get {
            return basicArticleType != null ? basicArticleType.Description__c : null;
        }
    }
    public String basicArticleTypeCause {
        get {
            return basicArticleType != null ? basicArticleType.Cause__c : null;
        }
    }
    public String basicArticleTypeResolution {
        get {
            return basicArticleType != null ? basicArticleType.Resolution__c : null;
        }
    }
    public String basicArticleTypeKnowledgeArticleId {
        get {
            return basicArticleType != null ? basicArticleType.KnowledgeArticleId : null;
        }
    }
    public String diagnosticArticleTypeDescription {
        get {
            return diagnosticArticleType != null ? diagnosticArticleType.Description__c : null;
        }
    }
    public String diagnosticArticleTypeCause {
        get {
            return diagnosticArticleType != null ? diagnosticArticleType.Cause__c : null;
        }
    }
    public String diagnosticArticleTypeResolution {
        get {
            return diagnosticArticleType != null ? diagnosticArticleType.Resolution__c : null;
        }
    }
    public String diagnosticArticleTypeKnowledgeArticleId {
        get {
            return diagnosticArticleType != null ? diagnosticArticleType.KnowledgeArticleId : null;
        }
    }

    public String articleUrl {
        get {
            return knowledgeArticleVerRecord != null ? PARTIAL_ARTICLE_URL + knowledgeArticleVerRecord.ArticleNumber : null;
        }
    }     
        
    public QS_CaseWizardKB_Cntrl() {
        init();
    }

    private void init() {
        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        CoveoFilterExpression  = partnerPortalURLs.Coveo_Filter_Expression__c;
        
        qtSettings = QTCustomSettingsHier__c.getInstance();

        if (UserInfo.getUserId() != null) {
            userRec = [
                    SELECT Id, Name, ContactId, Contact.AccountId, Contact.Name, Contact.Account.Name, Contact.Account.IsPartner,
                            Contact.Account.IsCustomerPortal, Contact.Account.QT_Designated_Support_Contact__c,
                            Contact.Account.Partner_Support_Contact__c
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
                    LIMIT 1
            ];
            userContactId = userRec.ContactId;
            userAccountId = userRec.Contact.AccountId;
        }
        //Get articleId from Knowledge Base Article
        if (ApexPages.currentPage().getParameters().get(ARTICLE_ID_PARAMETER) != null) {
            articleId = ApexPages.currentPage().getParameters().get(ARTICLE_ID_PARAMETER);
        } else if (ApexPages.currentPage().getParameters().get(ID_PARAMETER) != null) {
            articleId = ApexPages.currentPage().getParameters().get(ID_PARAMETER);
        }
        if (ApexPages.currentPage().getParameters().get(ARTICLE_NUMBER_PARAMETER) != null) {
            articleNumber = ApexPages.currentPage().getParameters().get(ARTICLE_NUMBER_PARAMETER);
        } else {
            articleNumber = '999999';
        }
        if (articleId != null && articleId.length() < 15) {
            articleNumber = articleId;
        }

        dataCaptureRecordTypeId = Schema.SObjectType.QS_DataCapture__c.getRecordTypeInfosByName()
                .get(DATA_CAPTURE_RECORD_TYPE).getRecordTypeId();

        helped = false;
        isCaseExists = false;
        isOpenCase = false;
        emailSent = false;
        String caseId = ApexPages.currentPage().getParameters().get(CASE_ID_PARAMETER);
        if (String.isNotBlank(caseId)) {
            if (caseId.contains('/')) {
                caseId = caseId.remove('/');
            }

            caseObj = [
                    SELECT Id, Subject, CaseNumber, RecordTypeId, OwnerId, AccountId, ContactId, Status,Closure_Code__c
                    FROM Case
                    WHERE Id = :caseId
                    LIMIT 1
            ];
            isOpenCase = caseObj != null && caseObj.Status != null && caseObj.Status != STATUS_COMPLETED && caseObj.Status != STATUS_RESOLVED
                    && caseObj.Status != STATUS_CLOSED && caseObj.Status != System.Label.QS_CloseCase;
            isCaseExists = caseObj != null && caseObj.Id != null;
        }

        guestUser = false;
        if (ApexPages.currentPage().getParameters().get(UNAUTHENTICATED_PARAMETER) != null) {
            String unauthenticatedParam = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get(UNAUTHENTICATED_PARAMETER), UTF8);
            unauthenticatedUser = unauthenticatedParam == 'true';
        } else {
            guestUser = userRec != null && userRec.Id == PORTAL_GUEST_USER_ID;
            unauthenticatedUser = UserInfo.getUserId() == null || userRec == null;
        }

        if (ApexPages.currentPage().getParameters().get(FROM_CASE_WIZARD_PARAMETER) != null) {
            String fromCaseWizardParam = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get(FROM_CASE_WIZARD_PARAMETER), UTF8);
            invokeCaseWizard = fromCaseWizardParam == 'true';
        }

        if (String.isNotBlank(articleId) || (String.isNotBlank(articleNumber))) {
            List<KnowledgeArticleVersion> versions = [
                    SELECT Id, VersionNumber, UrlName, Title, Summary, SourceId, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language,
                            IsLatestVersion, ArticleType, ArticleNumber, LastModifiedDate
                    FROM KnowledgeArticleVersion
                    WHERE IsLatestVersion = TRUE AND Language = 'en_US' AND PublishStatus = 'Online'
                    AND (Id = :articleId OR KnowledgeArticleId = :articleId OR ArticleNumber = :articleNumber OR UrlName = :articleId)
                    ORDER BY LastPublishedDate DESC
                    LIMIT 1
            ];
            if (versions.size() > 0) {
                knowledgeArticleVerRecord = versions[0];
            } else {
                showArticleNotFoundError = true;
                return;
            }
        }
        String query = '';

        if (knowledgeArticleVerRecord != null && knowledgeArticleVerRecord.KnowledgeArticleId != null && knowledgeArticleVerRecord.ArticleType != null) {
            query += 'SELECT Id, Summary, Description__c, Title, Resolution__c, Cause__c, Attachment_1__Name__s, Attachment_1__ContentType__s,'
                    + ' Attachment_1__Length__s, Attachment_1__Body__s, IsLatestVersion, ArticleNumber, ArticleType, knowledgeArticleId,'
                    + ' Attachment_2__Name__s, Attachment_2__ContentType__s , Attachment_2__Length__s, Attachment_2__Body__s,'
                    + ' Attachment_3__Name__s, Attachment_3__ContentType__s , Attachment_3__Length__s, Attachment_3__Body__s'
                    + ' FROM ' + knowledgeArticleVerRecord.ArticleType
                    + ' WHERE PublishStatus = \'Online\' AND Language = \'en_US\' AND ArticleNumber = \''
                    + knowledgeArticleVerRecord.ArticleNumber + '\' LIMIT 1';
            System.debug('Query: ' + query);

            if (knowledgeArticleVerRecord.ArticleType.toLowerCase() == 'basic__kav') {
                basicArticleType = Database.query(query);
                basicArticleType.Description__c = processBasicAndDiagnosticSmartLinks(basicArticleType.Description__c);
                basicArticleType.Resolution__c = processBasicAndDiagnosticSmartLinks(basicArticleType.Resolution__c);
                basicArticleType.Cause__c = processBasicAndDiagnosticSmartLinks(basicArticleType.Cause__c);
            } else if (knowledgeArticleVerRecord.ArticleType.toLowerCase() == 'diagnostic__kav') {
                diagnosticArticleType = Database.query(query);
                diagnosticArticleType.Description__c = processBasicAndDiagnosticSmartLinks(diagnosticArticleType.Description__c);
                diagnosticArticleType.Resolution__c = processBasicAndDiagnosticSmartLinks(diagnosticArticleType.Resolution__c);
                diagnosticArticleType.Cause__c = processBasicAndDiagnosticSmartLinks(diagnosticArticleType.Cause__c);
            }

            isAttachment1 = (basicArticleType != null && basicArticleType.Attachment_1__Name__s != null)
                    || (diagnosticArticleType != null && diagnosticArticleType.Attachment_1__Name__s != null);
            isAttachment2 = (basicArticleType != null && basicArticleType.Attachment_2__Name__s != null)
                    || (diagnosticArticleType != null && diagnosticArticleType.Attachment_2__Name__s != null);
            isAttachment3 = (basicArticleType != null && basicArticleType.Attachment_3__Name__s != null)
                    || (diagnosticArticleType != null && diagnosticArticleType.Attachment_3__Name__s != null);
        }

    }

    private String processBasicAndDiagnosticSmartLinks(String field) {
        field = processSmartLinks(field, ARTICLE_BASIC_TAG_START, ARTICLE_BASIC_TAG_MODIFIED_START);
        return processSmartLinks(field, ARTICLE_DIAGNOSTIC_TAG_START, ARTICLE_DIAGNOSTIC_TAG_MODIFIED_START);
    }

    private String processSmartLinks(String field, String articleTagStart, String modifiedArticleTagStart) {
        // smart links on the page have all parameters copied from the current url
        // change current article ID in these links to the title from the smart link

        String articleIdStr = ARTICLE_ID_PARAMETER + '=' + articleId;
        if (Test.isRunningTest()) {
            articleIdStr = ARTICLE_ID_PARAMETER + '=' + '987654';
        }

        if (field != null) {
            while (field.contains(articleIdStr) && field.indexOf(articleTagStart) != -1) {
                Integer indexOfArticleLinkStart = field.indexOf(articleTagStart);
                Integer charactersCountBeforeTitle = articleTagStart.length();

                String smartLink = field.substring(indexOfArticleLinkStart); //remove text before article link tag

                Integer indexOfSecondQuotes = smartLink.indexOf('" ');
                smartLink = smartLink.substring(0, indexOfSecondQuotes); // remove text after " closed <a> tag
                // smartLink example: <a href="/articles/Basic/test-1?_ga=2.29523583.1791008920.1555423660-1337716353.1555423660&amp;articleId=000071906

                String articleTitle = smartLink.substring(charactersCountBeforeTitle, smartLink.indexOf('?'));
                String smartLinkWithTitle = smartLink.replace(articleIdStr, ARTICLE_ID_PARAMETER + '=' + articleTitle);
                String smartLinkWithTitleAndModified = smartLinkWithTitle.replace(articleTagStart, modifiedArticleTagStart);
                // Add _MODIFIED_ to current link to mark it as already processed
                // smartLinkWithTitleAndModified example: <a href="_MODIFIED_/articles/Basic/test-1?_ga=2.29523583.1791008920.1555423660-1337716353.1555423660&amp;articleId=test-1

                field = field.replace(smartLink, smartLinkWithTitleAndModified);
            }
            field = field.replaceAll(ARTICLE_MODIFIED_TO_REMOVE, '');
        }
        return field;
    }

    public void setKBArticleReference() {
        if (knowledgeArticleVerRecord != null && knowledgeArticleVerRecord.ArticleNumber != null) {
            caseObj.Knowledge_Article__c = knowledgeArticleVerRecord.ArticleNumber;
        }

        // Mark the content unitilization as 'Yes'
        caseObj.Performance_Content_Utilization__c = 'Yes';
        // Mark the other performance fields as 'N/A'
        caseObj.Performance_Support_Entitlement__c = 'N/A';
        caseObj.Performance_Communication__c = 'N/A';
        caseObj.Performance_Case_Quality__c = 'N/A';
        caseObj.Performance_Troubleshooting__c = 'N/A';
        // Text to be confirmed with Staffan
        //  caseObj.Performance_Comments__c = 'The KB Article with Article Number - '+ caseObj.Knowledge_Article__c +' was helpful to the user.';
        caseObj.Performance_Comments__c = caseObj.Knowledge_Article__c ;

        if (isCaseExists != true && caseObj != null && caseObj.Id == null) {
            // Set the User's ContactId and AccountId as the Case's ContactId and AccountId
            if (userRec != null && userRec.ContactId != null) {
                caseObj.ContactId = userRec.ContactId;

                if (userRec.Contact.AccountId != null) {
                    caseObj.AccountId = userRec.Contact.AccountId;

                    if (userRec.Contact.Account.IsPartner) {
                        caseObj.Origin = PARTNER_PORTAL_ORIGIN;
                    } else if (userRec.Contact.Account.IsCustomerPortal) {
                        caseObj.Origin = CUSTOMER_PORTAL_ORIGIN;
                    }

                    caseObj.Designated_Support_Engineer__c = caseObj.Contact.Account.QT_Designated_Support_Contact__c;
                    caseObj.Partner_Support_Contact__c = caseObj.Contact.Account.Partner_Support_Contact__c;
                }
            }
            if (caseObj.OwnerId == null) {
                assignCaseOwnerFromCategory();
            }
        }

        if (caseObj != null) {
            caseObj.Status = System.Label.QS_CaseClosed;
            caseObj.Closure_Code__c = CASE_DEFLECTED_CODE;

            if (!isCaseExists && caseObj.Id == null) {
                caseObj.Case_submitted__c = System.now();

                if (String.isNotBlank(ApexPages.currentPage().getParameters().get(AREA_PARAMETER))) {
                    caseObj.Area__c = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get(AREA_PARAMETER), UTF8);
                }

                if (String.isNotBlank(ApexPages.currentPage().getParameters().get(ISSUE_PARAMETER))) {
                    caseObj.Issue__c = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get(ISSUE_PARAMETER), UTF8);
                }

                if (String.isNotBlank(ApexPages.currentPage().getParameters().get(SUBJECT_PARAMETER))) {
                    caseObj.Subject = processCaseParam(ApexPages.currentPage().getParameters().get(SUBJECT_PARAMETER));
                }
                String selectedSeverityLevel = ApexPages.currentPage().getParameters().get(SEVERITY_PARAMETER);
                if (String.isNotBlank(selectedSeverityLevel)) {
                    caseObj.Severity__c = (selectedSeverityLevel.contains('3') ? '3' : selectedSeverityLevel);
                }
                if (String.isNotBlank(ApexPages.currentPage().getParameters().get(DESCRIPTION_PARAMETER))) {
                    caseObj.Description = processCaseParam(ApexPages.currentPage().getParameters().get(DESCRIPTION_PARAMETER));
                }
                if (String.isNotBlank(ApexPages.currentPage().getParameters().get(RECORD_TYPE_NAME_PARAMETER))) {
                    String recordTypeNameParam = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get(RECORD_TYPE_NAME_PARAMETER), UTF8);
                    caseObj.RecordTypeId = getCaseRecordTypeId(recordTypeNameParam);
                }
            }

            if (String.isBlank(caseObj.Subject)) {
                caseObj.Subject = 'CASE_DEFLECTED';
            } else {
                caseObj.Subject = 'CASE_DEFLECTED - ' + (caseObj.Subject.length() > 238 ? caseObj.Subject.substring(0, 238) : caseObj.Subject);
            }
            caseObj.Resolution__c = caseObj.Performance_Comments__c;

            try {
                upsert caseObj;
            } catch (Exception ex) {
                showException = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ERROR_MESSAGE));
            }

            if (caseObj.Id != null) {
                try {
                    System.debug('QS_CaseWizardKB_Cntrl Trying to insert CaseArticle');
                    CaseArticle caseArticleObj = new CaseArticle(CaseId = caseObj.Id, KnowledgeArticleId = knowledgeArticleVerRecord.KnowledgeArticleId,
                            ArticleLanguage = knowledgeArticleVerRecord.Language, ArticleVersionNumber = knowledgeArticleVerRecord.VersionNumber);
                    insert caseArticleObj;
                    System.debug('QS_CaseWizardKB_Cntrl After insert CaseArticle');
                } catch (Exception ex) {
                    showException = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ERROR_MESSAGE));
                    System.debug('QS_CaseWizardKB_Cntrl Error when inserting CaseArticle: ' + ex.getMessage());
                }
            }
        }
    }

    private String processCaseParam(String fieldParam) {
        String field = EncodingUtil.urlDecode(fieldParam, UTF8);

        if (String.isNotBlank(field)) {
            if (field.contains(NEWLINE_TO_REPLACE)) {
                field = field.replace(NEWLINE_TO_REPLACE, '\n');
            }
            if (field.contains(QUESTION_MARK_TO_REPLACE)) {
                field = field.replace(QUESTION_MARK_TO_REPLACE, '?');
            }
        }
        return field;
    }

    // Assign an owner to the case based on the category selected
    private void assignCaseOwnerFromCategory() {
        String categoryId = ApexPages.currentPage().getParameters().get(CATEGORY_ID_PARAMETER);
        if (String.isNotBlank(categoryId)) {

            Category_Lookup__c categoryLookupSelectedRecord = [
                    SELECT Id, Category_Level__c, Name, Case_Owner__c
                    FROM Category_Lookup__c
                    WHERE Id = :categoryId
                    LIMIT 1
            ];
            // Assign an owner to the case based on the category selected
            if (categoryLookupSelectedRecord != null && String.isNotBlank(categoryLookupSelectedRecord.Case_Owner__c)) {
                caseObj.Queue_Name_Owner__c = categoryLookupSelectedRecord.Case_Owner__c;
                caseObj.Type = categoryLookupSelectedRecord.Case_Owner__c == 'Team Qoncierge' ? 'Service Request' : 'Incident';
                caseObj.Description = 'Selected Category Level - ' + categoryLookupSelectedRecord.Name + ' \nSelected Level 2-'
                        + categoryLookupSelectedRecord.Category_Level__c;
            } else {
                caseObj.Queue_Name_Owner__c = '';
            }
            caseObj.OwnerId = UserInfo.getUserId();

        }
    }
    // Method gets called when the user clicks on "Close Case" button  
    public void closeCase() {
        if (caseObj != null && caseObj.Id != null) {
            setKBArticleReference();
        }

        articleHelped();
    }

    // Get the Record Type for the case
    private Id getCaseRecordTypeId(String recordTypeName) {
        if (recTypeId == null) {
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        return recTypeId;
    }

    public void articleHelped() {
        helped = true;
        saveDataCapture();
    }

    public void articleDismissed() {
        helped = false;
        saveDataCapture();
    }

    public void submitComment() {
        showCommentError = false;

        if (!helped && String.isBlank(commentText)) {
            showCommentError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Comment can not be empty!'));
        } else {
            showComment = false;
            showThankYou = true;
            Case cs = createArticleFeedbackCase();
            sendEmailToKnowledgeGroup(cs);
        }
    }

    private void saveDataCapture() {
        hideFeedback = true;
        showComment = true;

        QS_DataCapture__c visit = new QS_DataCapture__c();
        visit.Time_of_closure__c = Datetime.now();
        visit.Article_helped__c = helped;
        visit.Article_number__c = knowledgeArticleVerRecord.ArticleNumber;
        visit.RecordTypeId = dataCaptureRecordTypeId;
        insert visit;
    }

    private Case createArticleFeedbackCase() {
        Case articleCase = new Case();
        RecordTypeInfo articleRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ARTICLE_RECORD_TYPE);
        if (articleRecordType != null) {
            articleCase.RecordTypeId = articleRecordType.recordTypeId;
        }
        articleCase.ContactId = userContactId;
        articleCase.AccountId = userAccountId;
        articleCase.Submitted_by__c = UserInfo.getUserId();
        articleCase.Description = commentText;
        articleCase.Subject = helped ? 'Yes' : 'No';
        articleCase.Origin = CASE_ORIGIN_SUPPORT_PORTAL;
        articleCase.Priority = 'High';
        articleCase.Knowledge_Article__c = knowledgeArticleVerRecord.KnowledgeArticleId;
        articleCase.Title__c = knowledgeArticleVerRecord.Title;

        Group gr = [SELECT Id FROM Group WHERE Name = :UNASSIGNED_QUEUE_NAME];
        articleCase.OwnerId = gr.Id;

        insert articleCase;
        return articleCase;
    }

    private void sendEmailToKnowledgeGroup(Case cs) {
        Group knowledgeGroup = [SELECT Id FROM Group WHERE DeveloperName = :KNOWLEDGE_MANAGERS_GROUP];
        List<GroupMember> members = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :knowledgeGroup.Id];
        List<String> userIds = new List<String>();
        for (GroupMember member : members) {
            userIds.add(member.UserOrGroupId);
        }
        List<User> users = [SELECT Id, Email FROM User WHERE Id IN :userIds];
        List<String> emails = new List<String>();
        for (User us : users) {
            emails.add(us.Email);
        }

        EmailTemplate templ = [SELECT Id, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = :ARTICLE_FEEDBACK_TEMPLATE];
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        emailMessage.setToAddresses(emails);
        emailMessage.setSubject(templ.Subject);
        emailMessage.setWhatId(cs.Id);

        String baseUrl = qtSettings.Base_URL__c;

        String htmlText = templ.HtmlValue.replace(DATE_SUBMITTED, Datetime.now().format());
        htmlText = htmlText.replace(CASE_LINK, cs != null ? baseUrl + cs.Id : '');
        htmlText = htmlText.replace(CASE_SUBJECT, cs != null && cs.Subject != null ? cs.Subject : '');
        htmlText = htmlText.replace(CASE_DESCRIPTION, cs != null && cs.Description != null ? cs.Description : '');

        htmlText = htmlText.replace(USER_NAME, userRec != null && userRec.Name != null ? userRec.Name : '');
        htmlText = htmlText.replace(USER_ID, userRec != null ? userRec.Id : '');
        htmlText = htmlText.replace(USER_LINK, userRec != null ? baseUrl + userRec.Id : '');

        List<Contact> contactRec = [SELECT Id, Name, Email FROM Contact WHERE Id = :userContactId];
        htmlText = htmlText.replace(CONTACT_LINK, contactRec.size() > 0 ? baseUrl + contactRec[0].Id : '');
        htmlText = htmlText.replace(CONTACT_NAME, contactRec.size() > 0 && contactRec[0].Name != null ? contactRec[0].Name : 'guest');
        htmlText = htmlText.replace(CONTACT_EMAIL, contactRec.size() > 0 && contactRec[0].Email != null ? '(' + contactRec[0].Email + ')' : '');

        htmlText = htmlText.replace(ARTICLE_LINK, knowledgeArticleVerRecord != null ? baseUrl + knowledgeArticleVerRecord.Id : '');
        htmlText = htmlText.replace(ARTICLE_NAME, knowledgeArticleVerRecord != null && knowledgeArticleVerRecord.Title != null ? knowledgeArticleVerRecord.Title : '');
        htmlText = htmlText.replace(ARTICLE_NUMBER, knowledgeArticleVerRecord != null && knowledgeArticleVerRecord.ArticleNumber != null ? knowledgeArticleVerRecord.ArticleNumber : '');

        emailMessage.setHtmlBody(htmlText);

        QTCustomSettings__c defSettings = QTCustomSettings__c.getValues('Default');
        String qlikNoreply = '';

        if (defSettings != null) {
            qlikNoreply = defSettings.QlikNoReplyEmailAddress__c;
            OrgWideEmailAddress[] orgwideaddress = [
                    SELECT Id
                    FROM OrgWideEmailAddress
                    WHERE DisplayName = :qlikNoreply
                    LIMIT 1
            ];
            if (!orgwideaddress.isEmpty()) {
                emailMessage.setOrgWideEmailAddressId(orgwideaddress[0].Id);
            }
        }

        try {
            List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(new List<Messaging.Email>{
                    emailMessage
            });
            for (Messaging.SendEmailResult res : sendRes) {
                System.debug(LoggingLevel.DEBUG, '[QS_CaseWizardKB_Cntrl] Email Result: ' + res);
            }
            emailSent = true;
        } catch (System.EmailException ex) {
            System.debug(LoggingLevel.DEBUG, '[QS_CaseWizardKB_Cntrl] Error when sending mail: ' + ex.getMessage());
        }
    }

}