/***********************************************
*
* 2019-03-05 AIN Test class fixes
*
***********************************************/


@isTest
public class TimecardControllerTest_QC {
	@isTest
    public static void getBaseURLForCommunityTest(){
        String PARTNER_ACCOUNT_RT = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
        /* Set up partner user - START*/
        Id p = [SELECT Id FROM Profile WHERE Name='PRM - Sales Dependent Territory + QlikBuy'].Id;
        Id apId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        
        Account acc = new Account(name ='Test Partner Account', RecordTypeId=PARTNER_ACCOUNT_RT) ;
        /* Test End User Account - START */
        
        insert acc;
            
        /* Test End User Account - END */
        
        Contact con = new Contact(LastName ='Test Partner Contact',AccountId = acc.Id,
                                  SFDCAccessGranted__c='LeadsOppsQuotes');
        
        insert con;  
        
        User partner = new User(alias = 'test123', email='asstest123223@asnmoemail1.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='12breanne@12tarth3.com');
        insert partner;
        System.runAs(partner){
            try{
                String baseURL = TimecardController_QC.getBaseURLForCommunity();
                System.assert(baseURL.startsWith('https://'));
            }catch(Exception ex){}
        }
    }
}