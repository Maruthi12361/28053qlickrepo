/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_PageTemplateControllerTEst {

    private static Id networkId;
    private static Id recTypeId;

    private static Case surveyToDelete;
    private static Attachment attachment;

    @isTest static void testProperties() {
        string STATUS_PENDING_CONTACT_RESPONSE = QS_PageTemplateController.STATUS_PENDING_CONTACT_RESPONSE;
        string STATUS_CONTACT_RESPONSE_RECEIVED = QS_PageTemplateController.STATUS_CONTACT_RESPONSE_RECEIVED;
        string STATUS_SOLUTION_SUGGESTED = QS_PageTemplateController.STATUS_SOLUTION_SUGGESTED;
        string STATUS_CLOSED = QS_PageTemplateController.STATUS_CLOSED;
        string STATUS_COMPLETED = QS_PageTemplateController.STATUS_COMPLETED;
        string STATUS_RESOLVED = QS_PageTemplateController.STATUS_RESOLVED;

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if(searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Announcement_Cookie_Expire_Time__c = 7;
            ss.Welcome_Screen_Cookie_Expire_Time__c = 7;
            insert ss;
        }

        PageReference pageRef = Page.QS_Home_Page;
        Test.setCurrentPage(pageRef);

        system.debug('Current URL according to test: ' +ApexPages.currentPage().getURL());

        

        QS_PageTemplateController controller = new QS_PageTemplateController();
        string UserName = controller.UserName;
        string UserId = controller.UserId;

        //boolean noEmail4me = controller.noEmail4me;
        string profileImageUrl= controller.profileImageUrl;

        List<Case> caselist = controller.caselist;
        Boolean showBugPopUp = controller.showBugPopUp;
        List<Case> bugCaseList = controller.bugCaseList;
        List<Case> caseSurveyList = controller.caseSurveyList;

        string surveyIdChosen = controller.surveyIdChosen;
        //String[] SelectedPersona = controller.SelectedPersona;
        string firstname = controller.FirstName;
        boolean enabledChat  = controller.enabledChat;

        string size = controller.caseListSize;
        string size2 = controller.caseListSize2;
        string bodyTags = controller.BodyTags;
        string ClassTags = controller.ClassTags;
        
    }
    @isTest static void testHomepage() {
        Semaphores.SetAllSemaphoresToTrue();

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if(searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Announcement_Cookie_Expire_Time__c = 7;
            ss.Welcome_Screen_Cookie_Expire_Time__c = 7;
            insert ss;
        }

        User testUser = IsnsertTestData('Application Developer;Server Administrator',2);
        PageReference pageRef = Page.QS_Home_Page;
        Test.setCurrentPage(pageRef);

        //QS_JiveSearchHTTPMock mock = QS_JiveSearchHTTPMock.CreateHTTPMock(15); 
        Semaphores.SetAllSemaphoresToFalse();

        test.startTest();
        system.runas(testUser)
        {
            //Test.setMock(HttpCalloutMock.class, mock);

            QS_PageTemplateController controller = new QS_PageTemplateController();
            
            controller.setMyEmailOption();
            
            controller.getCaseList();
            controller.surveyIdChosen = surveyToDelete.Id;
            controller.deleteSurveyStatus();
        }
        test.stopTest();
    }
    @isTest static void testNotHomepage() {
        Semaphores.SetAllSemaphoresToTrue();
        User testUser = IsnsertTestData('', 1);

        PageReference pageRef = Page.QS_CaseListPage;
        Test.setCurrentPage(pageRef);

        //QS_JiveSearchHTTPMock mock = QS_JiveSearchHTTPMock.CreateHTTPMock(15); 
        Semaphores.SetAllSemaphoresToFalse();

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if(searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Announcement_Cookie_Expire_Time__c = 7;
            ss.Welcome_Screen_Cookie_Expire_Time__c = 7;
            insert ss;
        }

        test.startTest();
        system.runas(testUser)
        {
            //Test.setMock(HttpCalloutMock.class, mock);
            QS_PageTemplateController controller = new QS_PageTemplateController();

            controller.getCaseList();
        }
        test.stopTest();
    }
    private static User IsnsertTestData(string persona, integer numberOfAnnouncements)
    {
        QS_Search_Setting__c ss1 = new QS_Search_Setting__c();
        ss1.Name = 'Jive_Search';
        ss1.Active__c = true;
        insert ss1;

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = persona;
        testContact.LeadSource = 'leadSource';
        insert testContact;

        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;

        Bugs__c bug = new Bugs__c();
        bug.Status__c ='Status 1';
        Insert bug;

        bug = [select Status__c from Bugs__c where id = :bug.id];
        System.debug('Bug.Status: ' + bug.Status__c);
        bug.Status__c = 'Status 2';
        update bug;

        bug = [select Status__c from Bugs__c where id = :bug.id];
        System.debug('Bug.Status: ' + bug.Status__c);

        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        caseObj.Bug__c = bug.Id;
        //caseObj.Status = QS_Controller.STATUS_SOLUTION_SUGGESTED;

        insert caseObj;

        surveyToDelete = TestDataFactory.createCase('Test Subject Survey','Test description survey', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        
        surveyToDelete.Survey_Status_Response__c = 'Later';
        surveyToDelete.Performance_Comments__c = '';
        surveyToDelete.Performance_Communication__c  = '';
        surveyToDelete.Performance_Support_Entitlement__c  = '';
        surveyToDelete.Performance_Content_Utilization__c  = '';
        surveyToDelete.Performance_Case_Quality__c  = '';
        surveyToDelete.Performance_Troubleshooting__c  = '';

        insert surveyToDelete;

        List<Support_Announcement__c> sas = new List<Support_Announcement__c>();
        for(integer i = 0;i < numberOfAnnouncements;i++)
        {
            Support_Announcement__c sa = new Support_Announcement__c();
            sa.Start_Date__c = Date.Today()-30;
            sa.End_Date__c = Date.Today()+30;
            sa.Active__c = true;
            sa.Announcement_Header__c = 'SA Test Header';
            sa.Announcement_Text__c = 'SA Test Text';
            sas.add(sa);
        }

        insert sas;

        List<Bugs__History> bugHistoryList = [Select ParentId, Parent.Status__c, OldValue, NewValue, Id, Field, CreatedDate, CreatedById 
            From Bugs__History 
            Where Field = 'Status__c' ];
            //AND ParentId IN (Select Bug__c From Case 
            //  where Status != :QS_Controller.STATUS_CLOSED AND Status != :QS_Controller.STATUS_RESOLVED AND 
            //  Status != :QS_Controller.STATUS_COMPLETED AND Status != :System.Label.QS_CloseCase)];
        


        system.debug('bugHistoryList.size(): ' + bugHistoryList.size());

        for(Bugs__History bh : bugHistoryList)
        {
            system.debug('bh.OldValue: ' + bh.OldValue);
            system.debug('bh.NewValue: ' + bh.NewValue);
        }
        return communityUser;
    }
    
    private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
                User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];  
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
    @isTest static void testUploadPhoto() {

        User testUser = InsertTestData('Application Developer;Server Administrator',2);
        PageReference pageRef = Page.QS_Home_Page;
        Test.setCurrentPage(pageRef);

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if(searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Announcement_Cookie_Expire_Time__c = 7;
            ss.Welcome_Screen_Cookie_Expire_Time__c = 7;
            insert ss;
        }

        test.startTest();
        system.runas(testUser)
        {
            QS_PageTemplateController controller = new QS_PageTemplateController();
            
            controller.setMyEmailOption();
            //system.debug('controller.getUserPersona(): ' + controller.getUserPersona());
            //system.assert(controller.getUserPersona() == 'Application Developer;Server Administrator');

            //controller.SaveBut();
            string username = controller.UserName;

            attachment = new Attachment();
            attachment.Name = 'Attach';
            attachment.body = Blob.valueof('Some random String');
            controller.attachment = attachment;

            //controller.UploadUserProfilePic();
            //QS_PageTemplateController.BlobToHex(Blob.valueof('Some random String'));
            controller.getUserprofilePic();
        }
        test.stopTest();
    }
    private static User InsertTestData(string persona, integer numberOfAnnouncements)
    {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = persona;
        testContact.LeadSource = 'leadSource';
        insert testContact;

        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        return communityUser;
    }
}