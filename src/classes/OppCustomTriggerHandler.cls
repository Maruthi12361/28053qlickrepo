/*
    Change Log
    2017-06-02 MTM Set QCW-1633 OEM Subscription deals
	2017-06-29 Rodion Vakulovskyi
 */
public class OppCustomTriggerHandler { 
    private static final String INTNFRCONST = 'Internal_NFR_Quote';
    private static final String PARTNFRCONST = 'Partner_Not_For_Resale';
    private static final String SPACECONST = ' ';
    public static Boolean FLAGTOCHECK = false;

    public static void onBeforeUpdate(List<Opportunity> inputList) {
        accountLookupNamePopulate(inputList);
    }

    private static void accountLookupNamePopulate(List<Opportunity> inputList) {
        Map<String, Account> mapOfAccounts = new Map<String, Account>();
        List<String> listOfAccountIds = new List<String>();
        Map<String, Id> mapOfRecordTypes = new Map<String, Id>();
        for(RecordType item : [select id , developerName From RecordType]) {
            mapOfRecordTypes.put(item.developerName, item.id);
        }
        for(Opportunity item : inputList) {
            listOfAccountIds.add(item.AccountId);
        }
        for(Account itemAccount : [Select id, Name From Account where id IN : listOfAccountIds]) {
            mapOfAccounts.put(itemAccount.Id,itemAccount);
        }
        for(Opportunity item : inputList) {
            if(mapOfAccounts.containsKey(item.AccountId)) {
                if(mapOfRecordTypes.containsKey(INTNFRCONST) && mapOfRecordTypes.get(INTNFRCONST) == item.RecordTypeId && mapOfRecordTypes.containsKey(PARTNFRCONST)) {
                    item.Name = mapOfAccounts.get(item.AccountId).Name +SPACECONST+ item.Revenue_Type__c +SPACECONST+ String.valueOf(Date.today());
                } else {
                    item.Name = mapOfAccounts.get(item.AccountId).Name +SPACECONST+ item.Short_Description__c;
                }
            }
        }
    }
    //begin of QCW-1467 Rodion Vakulovskyi
    public static void processBillingSchedule(List<Opportunity> inputList, Map<Id, Opportunity> inputOldMap) {
        if(!FLAGTOCHECK) {
            FLAGTOCHECK = true;
            List<String> oppIdsToQuery = new List<String>();

            for(Opportunity item : inputList) {
                if(inputOldMap.get(item.id).CloseDate != null && item.CloseDate != inputOldMap.get(item.id).CloseDate) {
                    oppIdsToQuery.add(item.id);
                }
            }
            List<OpportunityLineItem> listToProcess = new List<OpportunityLineItem>();
            for(OpportunityLineItem item : [Select id, OpportunityId, Net_Price__c, TotalPrice, Product2Id From OpportunityLineItem where OpportunityId IN : oppIdsToQuery]) {
                listToProcess.add(item);
            }
            if(!listToProcess.isEmpty()) {
                Set<String> setOfOppsid = new Set<String>();
        		for(OpportunityLineItem oppLineItem : listToProcess) {
            		setOfOppsid.add(oppLineItem.OpportunityId);
        		}
        		Map<String, SBQQ__Quote__c> mapQuoteFromOpp = OpportunityScheduleCreationHandler.queryQuoteFromOpp(setOfOppsid);
                OpportunityScheduleCreationHandler.deleteOldSchedules(listToProcess, mapQuoteFromOpp); 
                OpportunityScheduleCreationHandler.processScheduleSub(listToProcess, mapQuoteFromOpp);
            }
        }
    }
    //end of QCW-1467 Rodion Vakulovskyi

}