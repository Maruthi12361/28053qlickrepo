public class SendMeQuoteDetails {
    
    private static final String TEMPLATE_ID = '00XD0000002BUpr';
    private static final String IN_REPLY_TO = 'noreply@qlikview.com';

    @AuraEnabled
    public static Boolean isPartnerContactNull(String oppId){
        Boolean retVal = false;
        List<Opportunity> opp = [SELECT Id, Partner_Contact__c FROM Opportunity WHERE Id =:oppId];
        if(opp != null  && opp.size() > 0){
            retVal = (opp.get(0).Partner_Contact__c == null);
        }
        return retVal;
    }
    
    @AuraEnabled
    public static String sendEmail(String oppId){
        String retVal;
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setTemplateId(TEMPLATE_ID);
        message.setInReplyTo(IN_REPLY_TO);
        message.setWhatId(oppId);
        message.setTargetObjectId(UserInfo.getUserId());
        message.setSaveAsActivity(false);
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            retVal = Label.Quote_Email_Successful;
        } else   retVal = Label.Quote_Email_Error + results[0].errors[0].message;
         
        return retVal;
    }
}