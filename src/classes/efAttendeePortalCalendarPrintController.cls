//*********************************************************/
// Author: Mark Cane&
// Creation date: 24/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efAttendeePortalCalendarPrintController{
    private User user;
    private efRegistrationManager regManager;    	
    private String message='';
    private PageReference pr;    
    private String eventBannerUrl;
    private string contactId;
       
    public efAttendeePortalCalendarPrintController(){
    	if(user==null){
            try{
                initialize();
            } catch(Exception e){
            //    efUtility.updateDebugLog('Error in loading Portal User'+userId,'E1009','Error');
            }
        }
    }
    
	public void initialize(){
    	// Comment & : validate input registration Id.
    	String registrationId = ApexPages.currentPage().getParameters().get('registrationId');
    	contactId = efUtility.getContactIdFromRegistration(registrationId);
    	pr = checkInitialState();
    	if (pr!=null) return;
    	
    	// Comment& : efRegistrationManager creates and holds 
    	// efRegistrationWrapper, efContactWrapper instances.     	
        regManager = new efRegistrationManager(contactId, registrationId);
	}
	
	public PageReference checkInitialState(){
    	try {
    		String regId = ApexPages.currentPage().getParameters().get('registrationId');
    		if (regId==null || regId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    		if (contactId==null || contactId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);    		
    		return efRegistrationManager.checkRegistrationIdPortal(regId);
    	} catch (Exception e){
    		return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    	}
    }    
    
    public PageReference checkState(){
        if (pr!=null){ // Comment & : no registration manager instance = invalid state.
        	message = 'An error has occurred loading the Calendar. Please close this window and contact your System Administrator.';
        	return null;
        } else{
        	return null;
        }
    }
    
	// Page furniture methods.	
    public String getEventBannerUrl(){    	
    	if (eventBannerUrl==null || eventBannerUrl.length()==0){
    		eventBannerUrl = getDocumentUrl(regManager.getEventWrapper().getEvent().Banner_Image_Document__c);
    	}
    	return eventBannerUrl;
    }
    
    private String getDocumentUrl(String documentUniqueName){
    	Document doc = [Select d.Id From Document d Where d.DeveloperName=:documentUniqueName Limit 1];
    	String imageid = doc.id; 
    	imageid = imageid.substring(0,15);
    	return '/servlet/servlet.FileDownload?file=' + imageid;
    }
    
    public String getEventName(){
    	return regManager.getEventWrapper().getEventName();
    }   
    
    public Boolean getHasMessage(){
    	return (message.length()>0);
    }

    public String getRegistrationId(){
        return regManager.getRegistrationWrapper().getRegistration().Id;
    }

    public List<efClassWrapper> getClassesByGroup(){
    	return efActivityUtil.getClassesByGroup(
    	           efActivityUtil.joinTrainingAndActivities(
    	               null,
    	               efActivityUtil.getScheduledActivities(regManager.getRegistrationWrapper().getRegistration().Id)));
    }
    
    public String getMessage(){   
        return message;
    }    
}