public with sharing class Test_SEPForUser {

    //Global 
    public static List<Sales_Enablement_Program__c> SEPList = new List<Sales_Enablement_Program__c>();
    public static User gUser;

    static User createMockUserForProfile(String profileName, Id ContactId) {
		Profile p = [SELECT Id FROM profile WHERE name like :profileName];
		return createMockUser(p.id, ContactId);
	}
	 
	static User createMockUser(Id profileId, Id ContactId) {
		User u = new User(alias = 'newUser', email='newuser@tgerm.com.test',
		emailencodingkey='UTF-8', lastname='Testing', 
		languagelocalekey='en_US', localesidkey='en_US', profileid = profileId, ContactId = ContactId,
		timezonesidkey='America/Los_Angeles', username= System.now().millisecond() + 'newuser@tgerm.com.test');
		insert u;
		return  u; 
	}  
    
	static testMethod void CreateTestData() {
	 	 
	  // create test data        
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
        Account acc = new Account(Name='TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
	    insert acc;
	        
        Contact c = new Contact();
    	c.FirstName = 'Tao';
    	c.LastName = 'Handy';
    	c.accountId = acc.Id;
    	c.Email = 'ccetestmockcontact@test.com';	    	
    	insert c;         
        
	  	User lUser = createMockUserForProfile('PRM - Base', c.Id);
        gUser = lUser;
               	  	
	  	System.runAs(lUser)
        {
        	Sales_Enablement_Program__c SEP0 = new Sales_Enablement_Program__c(
        	Sales_Enablement_Program__c = 'System & Tool Training',
			Schedule_Date__c = Date.today(),
			Completed_Date__c = Date.today()+10,
			Comments__c = 'Test',
			User_Id__c = lUser.Id  		);
			
			SEPList.add(SEP0);
		
			Sales_Enablement_Program__c SEP1 = new Sales_Enablement_Program__c(
			Sales_Enablement_Program__c = 'QlikAcademy',
			Schedule_Date__c = Date.today()+5,
			Completed_Date__c = Date.today()+15,
			Comments__c = 'Test',
			User_Id__c = lUser.Id	);
			
			SEPList.add(SEP1);
		
			Sales_Enablement_Program__c SEP2 = new Sales_Enablement_Program__c(
			Sales_Enablement_Program__c = 'Sales Boot Camp',
			Schedule_Date__c = Date.today()+10,
			Completed_Date__c = Date.today()+15,
			Comments__c = 'Test',
			User_Id__c = lUser.Id	);
			
			SEPList.add(SEP2);
			
			insert SEPList;
        }
        
	 }
	 
	  static testMethod void TestAddingNewRecord() {  	
			
			CreateTestData();// First insert test data
			System.debug('TestAddingNewRecord Starting ');
			
			test.startTest();
			
			PageReference pageRef = new PageReference('/apex/SEPMultiple_Edit?User_Id__c=' + gUser.Id);
			Test.setCurrentPage(pageRef);
			
			ApexPages.StandardController StandardController = new ApexPages.StandardController(gUser);
			SEPForUser_Controller controller = new SEPForUser_Controller(StandardController);
			
			List<Sales_Enablement_Program__c> SEPListTemp = controller.getSalesEnablementProgramList();
			
			System.assert(SEPListTemp != null);
			
			//we should get our original 3 records plus the new blank one
			System.assert(SEPListTemp.size() == 4);	
			
			//Next we will modify an existing record
			Sales_Enablement_Program__c existingSEP = SEPList[1];
			existingSEP.Sales_Enablement_Program__c = 'CCS Workshop';
			
			// and edit the new blank record
			Sales_Enablement_Program__c newSEP = SEPList[2];
			newSEP.Schedule_Date__c = Date.today().adddays(24);
			newSEP.Sales_Enablement_Program__c = 'QlikAcademy';
			controller.QuickSaveSet();
			
			List<Sales_Enablement_Program__c> newSEPList = controller.getReturnSEPList();
			System.assert(newSEPList != null);
			
			//we should get our original 3 records plus the new one 
			System.assert(newSEPList.size() == 4);
			//test we modified an existing record
			
			Sales_Enablement_Program__c testSEPModify = SEPList[1];
			System.assert(testSEPModify.Sales_Enablement_Program__c == 'CCS Workshop');
			
			test.stopTest();			
            System.debug('TestAddingNewRecord Stop ');
	  }
	  
	   static testMethod void TestCancelingNewRecord() {
	   	
	   	CreateTestData();// First insert test data
			
		System.debug('TestCancelingNewRecord Starting ');
		test.startTest();
			
		PageReference pageRef = new PageReference('/apex/SEPMultiple_Edit?User_Id__c=' + gUser.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(gUser);
		SEPForUser_Controller controller = new SEPForUser_Controller(StandardController);

		List<Sales_Enablement_Program__c> SEPListTemp = controller.getSalesEnablementProgramList();
		System.assert(SEPListTemp != null);
		//we should get our original 3 records plus the new blank one
		System.assert(SEPListTemp.size() == 4);	
		
		//Edit the new blank record and then Cancel it
		Sales_Enablement_Program__c newSEP = SEPListTemp[3];
		newSEP.Schedule_Date__c = Date.today().adddays(4);
		newSEP.Sales_Enablement_Program__c = 'QlikAcademy';
		//do the Cancel
		controller.CancelSet();	
		
		List<Sales_Enablement_Program__c> newSEPList = controller.getSalesEnablementProgramList();
		System.assert(newSEPList != null);
		//we should get our original 3 records plus a new blank one
		System.assert(newSEPList.size() == 4);	
		test.stopTest();	
		System.debug('TestCancelingNewRecord Stop ');
	   }
	   
	   static testMethod void TestSaveWithoutAddingNewRecord() {
	   	
	   	CreateTestData();// First insert test data
			
		System.debug('TestSaveWithoutAddingNewRecord Starting ');
		test.startTest();
		
		PageReference pageRef = new PageReference('/apex/SEPMultiple_Edit?User_Id__c=' + gUser.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(gUser);
		SEPForUser_Controller controller = new SEPForUser_Controller(StandardController);

		List<Sales_Enablement_Program__c> SEPListTemp = controller.getSalesEnablementProgramList();
		System.assert(SEPListTemp != null);
		//we should get our original 3 records plus the new blank one
		System.assert(SEPListTemp.size() == 4);	
		
		//Next we will modify an existing record
		Sales_Enablement_Program__c existingSEP = SEPListTemp[1];
		existingSEP.Sales_Enablement_Program__c = 'QlikAcademy';
		//and Save the change
		controller.SaveSet();
		
		List<Sales_Enablement_Program__c> newSEPList = controller.getReturnSEPList();
		System.assert(newSEPList != null);
		//we should get our original 3 records plus a new blank one
		System.assert(newSEPList.size() == 4);	
		
		//test we modified an existing record	
		Sales_Enablement_Program__c testSEPModify = SEPListTemp[1];
		System.assert(testSEPModify.Sales_Enablement_Program__c == 'QlikAcademy');	
		
		test.stopTest();	
		System.debug('TestSaveWithoutAddingNewRecord Stop ');
	   }
	 
	  static testMethod void TestErrorWhileSaving() {
	  	
	  	CreateTestData();// First insert test data
			
		System.debug('TestErrorWhileSaving Starting ');
		test.startTest();
		
		PageReference pageRef = new PageReference('/apex/SEPMultiple_Edit?User_Id__c=' + gUser.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(gUser);
		SEPForUser_Controller controller = new SEPForUser_Controller(StandardController);

		List<Sales_Enablement_Program__c> SEPListTemp = controller.getSalesEnablementProgramList();
		System.assert(SEPListTemp != null);
		//we should get our original 3 records plus the new blank one
		System.assert(SEPListTemp.size() == 4);	
		
		//Edit the new blank record but do not fill in all the Required fields
		// (a validation rule checks we have filled in the Required fields, as
		// we have not we will not be able to Save the page so should still have 4 records)
		Sales_Enablement_Program__c newSEP = SEPListTemp[2];
		newSEP.Schedule_Date__c = Date.today().adddays(4);
		
		//try and do the Save
		controller.SaveSet();
		
		//we cannot, so we should still have only four records
		List<Sales_Enablement_Program__c> ErrSEPList = controller.getReturnSEPList();
		System.assert(ErrSEPList != null);
		System.assert(ErrSEPList.size() == 4);
		
		test.stopTest();	
		System.debug('TestErrorWhileSaving Stop ');
	  }
	   
	  static testMethod void TestDeleteARecord(){
	   	
	   	CreateTestData();// First insert test data
			
		System.debug('TestDeleteARecord Starting ');
		test.startTest();
		
		PageReference pageRef = new PageReference('/apex/SEPForUser?User_Id__c=' + gUser.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(gUser);
		SEPForUser_Controller controller = new SEPForUser_Controller(StandardController);

		List<Sales_Enablement_Program__c> SEPListTemp = controller.getSalesEnablementProgramList();
		System.assert(SEPListTemp != null);
		//we should get our original 3 records plus the new blank one
		System.assert(SEPListTemp.size() == 4);	
		
		controller.DeleteSEP();
		List<Sales_Enablement_Program__c> SEPListTemp1 = controller.getReturnSEPList();
		System.assert(SEPListTemp1 != null);
		
		//we should get our original 3 records plus the new blank one
		System.assert(SEPListTemp1.size() == 3);	
		
		
		test.stopTest();	
		System.debug('TestDeleteARecord Stop ');
		
	  }
}