/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_SearchComponentController {
	public QS_Search search {get; set;}
	public string searchString { get; set; }
    public string searchType {get; set;}
    public integer numberResults {get; set;}
    public boolean personasBased {get; set;}
	public QS_Search getSearchResultSet() {

		if (searchType != null && searchType.toUpperCase() == 'JIVE') {
            return new QS_JiveSearch(searchString, numberResults, personasBased);
        }
        else if (searchType != null && searchType.toUpperCase() == 'LITHIUM') {
            return new QS_LithiumSearch(searchString, numberResults, personasBased);
        }
        else {
            return new QS_PartnerLibrarySearch(searchString, numberResults);
        }
	}
}