/**
*AccUpdatePartnerLevelStatusHandlerTest
*Rodion Vakulovskyi 
*2017-07-05
*unit test for AccUpdatePartnerLevelStatusHandler
*19/07/2017 Updated by Pramod Kumar V:Paex 71
*23/08/2017 Updated by Pramod Kumar V:Paex 227
*29/08/2017 Updated by Pramod Kumar V:Paex 240
*04/09/2017   Rodion Vakulovskyi  Added test methods for PAEX 240
***/
@isTest(seeAlldata=false)
public class AccUpdatePartnerLevelStatusHandlerTest {
    private static final String PARTNERACC = '01220000000DOFzAAO';
    private static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    private static testmethod void testTriggerScenarios() {
       
       Id rtId2= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();         
        
        Account accForTest = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=false);
        insert accForTest ;
        
        Test.startTest();
            Partner_Category_Status__c partnerCatStatusTest = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c = 'Active',
                                                                                            Partner_Category__c = 'Influence',
                                                                                            Partner_Level__c = 'Authorized',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
            insert partnerCatStatusTest;
          
            System.assertEquals(true, String.isEmpty(accForTest.Detailed_Partner_Levels__c), 'There should be empty picklist as partner category status record is inactive');
            System.assertEquals(true, String.isEmpty(accForTest.PartnerType__c), 'There should be empty picklist as partner category status record is inactive');
            partnerCatStatusTest.Partner_Level_Status__c = 'Inactive';
            update partnerCatStatusTest;
           
            delete partnerCatStatusTest;
                   

        Account accForTest1 = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=false);
        //insert accForTest1 ;
        Partner_Category_Status__c partnerCatStatusTest1 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c = 'Active',
                                                                                            Partner_Category__c = 'MSP',
                                                                                            Partner_Level__c = 'Elite',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest1;
        partnerCatStatusTest1.Partner_Level_Status__c = 'Inactive';
        update partnerCatStatusTest1;
        
        Account accForTest2 = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=false);
        //insert accForTest2 ;
        Partner_Category_Status__c partnerCatStatusTest2 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c = 'Active',
                                                                                            Partner_Category__c = 'MSP',
                                                                                            Partner_Level__c = 'Select',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest2;
      
        partnerCatStatusTest2.Partner_Level_Status__c = 'Inactive';
        update partnerCatStatusTest2;

        
        Account accForTest3 = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=false);
        //insert accForTest3 ;
        Partner_Category_Status__c partnerCatStatusTest3 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c ='Active',
                                                                                            Partner_Category__c = 'Resell',
                                                                                            Partner_Level__c = 'Authorized Reseller',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest3;
        partnerCatStatusTest3.Partner_Level_Status__c = 'Inactive';
        update partnerCatStatusTest3;
        
        Account accForTest4 = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=true);
        //insert accForTest4 ;
        
        Partner_Category_Status__c partnerCatStatusTest4 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c ='Active',
                                                                                            Partner_Category__c = 'Resell',
                                                                                            Partner_Level__c = 'Elite',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest4;
        partnerCatStatusTest4.Partner_Level_Status__c = 'Inactive';
        //update partnerCatStatusTest4;
        
        Account accForTest5 = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=true);
        //insert accForTest5 ;
        
        Partner_Category_Status__c partnerCatStatusTest5 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c ='Active',
                                                                                            Partner_Category__c = 'Influence',
                                                                                            Partner_Level__c = 'Elite',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest5;
        partnerCatStatusTest5.Partner_Level_Status__c = 'Inactive';
        update partnerCatStatusTest5;
        
        Account accForTest6 = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=true);
        //insert accForTest6 ;
        
        Partner_Category_Status__c partnerCatStatusTest6 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c ='Active',
                                                                                            Partner_Category__c = 'Resell',
                                                                                            Partner_Level__c = 'Select',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest6 ;
        //partnerCatStatusTest6.Partner_Level_Status__c = 'Inactive';
        //update partnerCatStatusTest6 ;
        
        Partner_Category_Status__c partnerCatStatusTest7 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c ='Active',
                                                                                            Partner_Category__c = 'Influence',
                                                                                            Partner_Level__c = 'Select',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest7;
        //partnerCatStatusTest7.Partner_Level_Status__c = 'Inactive';
        //update partnerCatStatusTest7;
        
      Partner_Category_Status__c partnerCatStatusTest8 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c ='Active',
                                                                                            Partner_Category__c = 'Influence',
                                                                                            Partner_Level__c = 'Authorized',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
        insert partnerCatStatusTest8 ;
        //partnerCatStatusTest8.Partner_Level_Status__c = 'Inactive';
        //update partnerCatStatusTest8;

        Test.stopTest();  
    }

	private static testmethod void testQppPartnerLevel() {
		Id rtId2= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();         
		Account accForTest = new Account(name='Test AccountName',recordtypeid=rtId2,PartnerFeeOpportunity__c=false);
		insert accForTest ;
		Test.startTest();
		Partner_Category_Status__c partnerCatStatusTest = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c = 'Active',
                                                                                            Partner_Category__c = 'Influence',
                                                                                            Partner_Level__c = 'Authorized',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
		insert partnerCatStatusTest;
		Account testAccont = [SELECT Id, Overall_Partner_Level__c FROM Account WHERE Id = :accForTest.Id];
		System.assertEquals(testAccont.Overall_Partner_Level__c, partnerCatStatusTest.Partner_Level__c, 'Overall partner level field on Account should be ' + partnerCatStatusTest.Partner_Level__c + ' and it is ' + testAccont.Overall_Partner_Level__c);
		Partner_Category_Status__c partnerCatStatusTest2 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c = 'Active',
                                                                                            Partner_Category__c = 'Distributor',
                                                                                            Partner_Level__c = 'Distributor',
                                                                                            Program_Version__c = 'Distributor'
                                                                                            );
		insert partnerCatStatusTest2;
		System.assertEquals(testAccont.Overall_Partner_Level__c, partnerCatStatusTest.Partner_Level__c, 'Overall partner level field on Account should be ' + partnerCatStatusTest.Partner_Level__c + ' and it is ' + testAccont.Overall_Partner_Level__c);
		Partner_Category_Status__c partnerCatStatusTest3 = new Partner_Category_Status__c(Partner_Account__c = accForTest.Id, 
                                                                                            Partner_Level_Status__c = 'Active',
                                                                                            Partner_Category__c = 'Influence',
                                                                                            Partner_Level__c = 'Select',
                                                                                            Program_Version__c = 'QPP'
                                                                                            );
		insert partnerCatStatusTest3;
		testAccont = [SELECT Id, Overall_Partner_Level__c FROM Account WHERE Id = :accForTest.Id];
		System.assertEquals(testAccont.Overall_Partner_Level__c, partnerCatStatusTest3.Partner_Level__c, 'Overall partner level field on Account should be ' + partnerCatStatusTest3.Partner_Level__c + ' and it is ' + testAccont.Overall_Partner_Level__c);
		partnerCatStatusTest3.Partner_Level__c = 'Elite';
		update partnerCatStatusTest3;
		testAccont = [SELECT Id, Overall_Partner_Level__c FROM Account WHERE Id = :accForTest.Id];
		System.assertEquals(testAccont.Overall_Partner_Level__c, partnerCatStatusTest3.Partner_Level__c, 'Overall partner level field on Account should be ' + partnerCatStatusTest3.Partner_Level__c + ' and it is ' + testAccont.Overall_Partner_Level__c);
		delete partnerCatStatusTest3;
		testAccont = [SELECT Id, Overall_Partner_Level__c FROM Account WHERE Id = :accForTest.Id];
		System.assertEquals(testAccont.Overall_Partner_Level__c, partnerCatStatusTest.Partner_Level__c, 'Overall partner level field on Account should be ' + partnerCatStatusTest.Partner_Level__c + ' and it is ' + testAccont.Overall_Partner_Level__c);
		partnerCatStatusTest.Partner_Level_Status__c = 'Inactive';
		update partnerCatStatusTest;
		testAccont = [SELECT Id, Overall_Partner_Level__c FROM Account WHERE Id = :accForTest.Id];
		System.assert(String.isBlank(testAccont.Overall_Partner_Level__c), 'Overall partner level field on Account should be blank and it is ' + testAccont.Overall_Partner_Level__c);
		Test.stopTest();
	}
}