/**
  2019_11_07 : CCE BMW-1829
**/
global class EmailNotificationForQSBTrials Implements Schedulable
{
    public static String schedDaily = '0 00 04 * * ?';  //Daily 4am
    // used for scheduling a repeating job
    global static String scheduleMeDaily() {
        EmailNotificationForQSBTrials job = new EmailNotificationForQSBTrials(); 
        if (test.isRunningTest()) return null;
        else return System.schedule('EmailNotificationForQSBTrials', schedDaily, job);
    }
    global void execute(SchedulableContext sc)
    {
        SendEmailNotificationForQSBTrials();
    }

    public void SendEmailNotificationForQSBTrials()
    {
        Map<String, String> mapEmailsToSend  = new Map<String, String>();             
        Map<String, String> mapEmailToAccountOwnerName = new Map<String, String>();             
        string sActiveUser = '';
        string sInactiveUser = '';
        string sLeadText = '';
        string sConText = '';
        string sBodyAll = '';
        List<String> lstSegmentVals = new List<String>{'Strategic - Global','Strategic - Key','Enterprise - Priority','Enterprise - Target','Commercial - Mid-Market'};
        datetime dt = System.now();
        datetime dtminus1 = dt-1;

        List<string> lCampaignNames = EmailNotificationForQSBTrials.getCampaignNames();
        System.debug('listOfCampaigns lCampaignNames = ' + lCampaignNames);
    
        List<Lead> lds = [SELECT Id, Name, Last_Campaign_Response_Name__c, Last_Campaign_Response_Date__c, LeanData__Reporting_Matched_Account__c, LeanData__Reporting_Matched_Account__r.Name, Owner_of_the_Linked_Account__c, Owner_of_the_Linked_Account__r.IsActive, Owner_of_the_Linked_Account__r.Name, Owner_of_the_Linked_Account__r.Email, OwnerId, Owner.Name, Owner.IsActive, Owner.Email FROM Lead WHERE SystemModstamp > :dtminus1 AND Segment__c IN :lstSegmentVals AND Last_Campaign_Response_Name__c IN :lCampaignNames AND Last_Campaign_Response_Date__c > :dtminus1 AND LeanData__Reporting_Matched_Account__c != null AND Last_Campaign_Response_Date__c != null];
        System.debug('listOfCampaigns lds = ' + lds);
        List<Contact> cons = [SELECT Id, Name, AccountId, Account.Name, Last_Campaign_Response_Name__c, Last_Campaign_Response_Date__c, Account_Owner__c, Account_Owner__r.IsActive, Account_Owner__r.Name, Account_Owner__r.Email, OwnerId, Owner.Name, Owner.IsActive, Owner.Email FROM Contact WHERE SystemModstamp > :dtminus1 AND Segment__c IN :lstSegmentVals AND Last_Campaign_Response_Name__c IN :lCampaignNames AND Last_Campaign_Response_Date__c > :dtminus1 AND Last_Campaign_Response_Date__c != null];
        System.debug('listOfCampaigns cons = ' + cons);

        if (lds.size() > 0) { 
            Map<String, List<Lead>> mapAccOwnerIdToLead  = new Map<String, List<Lead>>();
            for(Lead l : lds)
            {
                if (mapAccOwnerIdToLead.containsKey(l.Owner_of_the_Linked_Account__c)) {
                    mapAccOwnerIdToLead.get(l.Owner_of_the_Linked_Account__c).add(l);
                }
                else {
                    mapAccOwnerIdToLead.put(l.Owner_of_the_Linked_Account__c, new List<Lead>{l});
                }
            }
            System.debug('listOfCampaigns mapAccOwnerIdToLead = ' + mapAccOwnerIdToLead);

            if (mapAccOwnerIdToLead.size() > 0) {
                for (String s : mapAccOwnerIdToLead.keyset()) {
                    System.debug('listOfCampaigns s = ' + s);
                    
                    sActiveUser = '';
                    sBodyAll = '';
                    string sTmp = '';
                    String sAccountOwnerName = '';
                    for (Lead l : mapAccOwnerIdToLead.get(s)) {
                        if (l.Owner_of_the_Linked_Account__r.IsActive) {
                            sAccountOwnerName = l.Owner_of_the_Linked_Account__r.Name;
                            //toAddr.add(l.Owner_of_the_Linked_Account__r.Email);
                            sTmp = URL.getSalesforceBaseUrl().toExternalForm()+ '/'+l.Id;                   
                            sLeadText = '<br><b>Name:</b> <a href="'+sTmp+'">'+l.Name+'.</a>';
                            sTmp = URL.getSalesforceBaseUrl().toExternalForm()+ '/'+l.LeanData__Reporting_Matched_Account__c;                   
                            sLeadText += '<br><b>Account:</b> <a href="'+sTmp+'">'+l.LeanData__Reporting_Matched_Account__r.Name+'.</a>';
                            
                            sTmp = '<br><b>Date Registered for trial:</b> ' + l.Last_Campaign_Response_Date__c.format('dd-MMMMM-yyyy') + '<br>';
                            sLeadText += sTmp;
                            sBodyAll += sLeadText;
                            
                            mapEmailsToSend.put(l.Owner_of_the_Linked_Account__r.Email, sBodyAll);                            
                            mapEmailToAccountOwnerName.put(l.Owner_of_the_Linked_Account__r.Email, l.Owner_of_the_Linked_Account__r.Name);
                        }
                    }
                }
            }
        }

        if (cons.size() > 0) 
        {            
            System.debug('listOfCampaigns cons = ' + cons);        
            for (Contact c : cons) {
                sBodyAll = '';
                string sTmp = '';
                String sAccountOwnerName = '';
                if (c.Account_Owner__r.IsActive) {
                    sAccountOwnerName = c.Account_Owner__r.Name;
                    sTmp = URL.getSalesforceBaseUrl().toExternalForm()+ '/'+c.Id;                   
                    sConText = '<br><b>Name:</b> <a href="'+sTmp+'">'+c.Name+'.</a>';
                    sTmp = URL.getSalesforceBaseUrl().toExternalForm()+ '/'+c.AccountId;                   
                    sConText += '<br><b>Account:</b> <a href="'+sTmp+'">'+c.Account.Name+'.</a>';
                    sTmp = '<br><b>Date Registered for trial:</b> ' + c.Last_Campaign_Response_Date__c.format('dd-MMMMM-yyyy') + '<br>';
                    sConText += sTmp;
                    sBodyAll += sConText;
            
                    if (mapEmailsToSend.containsKey(c.Account_Owner__r.Email)) {
                        String sCTmp = mapEmailsToSend.get(c.Account_Owner__r.Email);
                        sCTmp += sBodyAll;
                        mapEmailsToSend.put(c.Account_Owner__r.Email, sCTmp); 
                    }
                    else {
                        mapEmailsToSend.put(c.Account_Owner__r.Email, sBodyAll);                            
                        mapEmailToAccountOwnerName.put(c.Account_Owner__r.Email, c.Account_Owner__r.Name);
                    }   
                }
            } 
        }

        //Send the emails
        if (mapEmailsToSend.size() > 0) {
            List<Messaging.SingleEmailMessage> lstMails = new List<Messaging.SingleEmailMessage>();
            for (String s : mapEmailsToSend.keyset()) {                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String sBody;
                String[] toAddresses = new String[] {};
                //String[] toAddresses = new String[] {'cce@qlik.com'};
                    
                mail.setSubject('QSB Trials registered for the last 24 hours');                
                sBody = 'Dear ' + mapEmailToAccountOwnerName.get(s) + ',<br><br>You are receiving this email because someone at one or more of your accounts has registered for a Qlik Sense Business trial.  This is an opportunity for you to engage with your team and the account to support them in the evaluation process.<br><br>Here are the people who have registered<br>' + mapEmailsToSend.get(s);
                sBody += '<br>You should connect with your account team to agree on the best follow up approach.';
                toAddresses.add(s);
                mail.setToAddresses(toAddresses);
                mail.setHtmlBody(sBody);
                lstMails.add(mail);
            }
            Messaging.sendEmail(lstMails);
        }
        


        // if (lds.size() > 0) {        
        //     sActiveUser = '';
        //     sInactiveUser = '';
        //     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //     String[] toAddresses = new String[] {

        //         'cce@qlik.com'
        //     };
        //     mail.setToAddresses(toAddresses);
        //     mail.setSubject('QSB Trials downloaded for the last 24 hours');
        //     for (Integer i = 0; i < lds.size(); i++) {
        //         System.debug('listOfCampaigns lds'+'['+i+']'+'='+ lds[i]);
           
        //         sBody = '';
        //         sId = lds[i].OwnerId;
        //         if (sId.left(3) == '00G') continue; //do not add Leads where the Lead Owner is a Queue
        //         if (lds[i].Owner_of_the_Linked_Account__r.IsActive == false) {
        //             sInactiveUser += '\r\nLead Name: ' + lds[i].Name;
        //             sInactiveUser += ' Owner of the Linked Account is an Inactive user.';
        //             sInactiveUser += '\r\nLink: '+ URL.getSalesforceBaseUrl().toExternalForm()+ '/'+lds[i].Id;
        //             continue;
        //         }
        //         if (lds[i].Owner.IsActive == false) {
        //             sInactiveUser += '\r\nLead Name: ' + lds[i].Name;
        //             sInactiveUser += ' Owner of the Lead is an Inactive user.';
        //             sInactiveUser += '\r\nLink: '+ URL.getSalesforceBaseUrl().toExternalForm()+ '/'+lds[i].Id;
        //             continue;
        //         }
        //         sBody = '\r\nLead Name: ' + lds[i].Name + ' Campaign: ' + lds[i].Last_Campaign_Response_Name__c + '\r\nLink: '+ URL.getSalesforceBaseUrl().toExternalForm()+ '/'+lds[i].Id;
        //         sActiveUser += sBody;
        //     }
        //     String mailBody = 'Downloads with Inactive Account/Owner' + sInactiveUser + '\r\n' + sActiveUser;
        //     mail.setPlainTextBody(mailBody);
        //     Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        // }

    }

    private static List<string> getCampaignNames() {
        List<string> listOfCampaigns = new List<String>();
        
        Lead_Email_Notification_Settings__c Settings = Lead_Email_Notification_Settings__c.getOrgDefaults();
        List<string> cmps = Settings.QSB_Trial_Campaign_Name_1__c != null ? Settings.QSB_Trial_Campaign_Name_1__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_2__c != null ? Settings.QSB_Trial_Campaign_Name_2__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_3__c != null ? Settings.QSB_Trial_Campaign_Name_3__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_4__c != null ? Settings.QSB_Trial_Campaign_Name_4__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_5__c != null ? Settings.QSB_Trial_Campaign_Name_5__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_6__c != null ? Settings.QSB_Trial_Campaign_Name_6__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_7__c != null ? Settings.QSB_Trial_Campaign_Name_7__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_8__c != null ? Settings.QSB_Trial_Campaign_Name_8__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_9__c != null ? Settings.QSB_Trial_Campaign_Name_9__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        cmps = Settings.QSB_Trial_Campaign_Name_10__c != null ? Settings.QSB_Trial_Campaign_Name_10__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < cmps.size(); i++) {   
            listOfCampaigns.add(cmps[i]);
        }
        System.debug('listOfCampaigns = '+ listOfCampaigns);

        return listOfCampaigns;
    }
}