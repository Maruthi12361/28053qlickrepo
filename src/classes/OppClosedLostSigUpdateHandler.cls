/*

Created 2020-06-11 - DKF
Created for DTR-600 - Updates Accounts to clear out the Signature Success fields when a Signature Success renewal is lost

*/

public class OppClosedLostSigUpdateHandler {
    public static void OppClosedLostSignatureUpdate(List<Opportunity> oppList, Map<Id, Opportunity> oppNewMap, Map<Id, Opportunity> oppOldMap) {
        Set<Id> setClosedOppIds = new Set<Id>();
        Set<Id> setZuoraQuoteIds = new Set<Id>();
        Set<Id> setZuoraOppSigSuccess = new Set<Id>();
        List<Account> accountUpdateList = new List<Account>();
        Id oppRecordTypeRenewal = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription Renewal').getRecordTypeId();
        Id oppRecordTypeZuora = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS Zuora Subscription').getRecordTypeId();

        for(Opportunity opp: oppList){
            if(opp.StageName != oppOldMap.get(opp.Id).StageName && opp.IsClosed == TRUE && opp.IsWon == FALSE && (opp.RecordTypeId == oppRecordTypeRenewal || opp.RecordTypeId == oppRecordTypeZuora)){
                setClosedOppIds.add(opp.Id);
            }                
        }
        
        if(setClosedOppIds.size() > 0){
            for(zqu__QuoteChargeDetail__c qcd: [SELECT Name, zqu__Quote__c, zqu__Quote__r.zqu__Opportunity__c 
                                                FROM zqu__QuoteChargeDetail__c 
                                                WHERE Name LIKE 'SS%'
                                                AND zqu__Quote__r.zqu__SubscriptionType__c = 'Renew Subscription' 
                                                AND zqu__Quote__r.zqu__Primary__c = TRUE 
                                                AND zqu__Quote__r.zqu__Opportunity__c IN :setClosedOppIds]){
                setZuoraOppSigSuccess.add(qcd.zqu__Quote__r.zqu__Opportunity__c);
            }
        
            for(Id oppId: setZuoraOppSigSuccess){
                accountUpdateList.add(new Account(id = oppNewMap.get(oppId).AccountId,  
                                            Signature_Success_Entitled__c = FALSE, 
                                            Signature_Success_End_Date__c = null));
            }

            System.debug('These are the accounts being updated in the Signature Success process: ' + accountUpdateList);

            if(accountUpdateList.size() > 0)
                update accountUpdateList;
        }
    }
}