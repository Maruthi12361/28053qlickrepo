@isTest

/***************************************************************************************************************************************

    Changelog:
        2012-02-15  CCE     Added QlikTech_Company__c to Account creation due to required QlikTech Company field on Account
        2013-04-25  SLH     Removed the last two updates as this caused SOQL query limit in CaseTrigger
                            
****************************************************************************************************************************************/

private class TestGenericHistory 
{
    //Method which requeries and returns the test Case 
    private static Case requeryTestCase(Id testEndUserAcntId)
    {
        return [SELECT Id, AccountId, RecordTypeId, ContactId, OwnerId, Origin, Description, Status, Type FROM Case WHERE AccountId =: testEndUserAcntId];
    }
    
    static testMethod void testGenericHistory() 
    {	
	 Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();      
        QlikTech_Company__c Q = new QlikTech_Company__c(
            Name = 'USA',
            Country_Name__c = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = testSubs1.id );
        insert Q;

        //Create a Map of Account RecordTypes
        Map<String,Id> acntRecordTypes = new Map<String,Id>();
        for(RecordType rt:[SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND IsActive=true])
        {
            acntRecordTypes.put(rt.Name, rt.Id);
        }
        
        //Create a Map of Case RecordTypes
        Map<String,Id> caseRecordTypes = new Map<String,Id>();
        for(RecordType rt:[SELECT Id, Name FROM RecordType WHERE SObjectType = 'Case' AND IsActive=true])
        {
            caseRecordTypes.put(rt.Name, rt.Id);
        }
   
        //Create a Map of Country Codes
        Map<String, Id> countryCodes = new Map<String, Id>();
        for(QlikTech_Company__c qtc:[SELECT Id, Country_Name__c FROM QlikTech_Company__c])
        {
            countryCodes.put(qtc.Country_Name__c, qtc.Id);
        }
        
        //Added QlikTech_Company__c to Account creation
        //QlikTech_Company__c Q = [SELECT Id, QlikTech_Company_Name__c FROM QlikTech_Company__c WHERE Name = 'USA'];
        
        //Create test data
        //Create test Account
        Account testEndUserAcnt = new Account(Name='Test1 Account1', RecordTypeId=acntRecordTypes.get('End User Account'), Billing_Country_Code__c=countryCodes.get('USA'), QlikTech_Company__c = Q.QlikTech_Company_Name__c);
        insert testEndUserAcnt;
        
        //Create test Contact
        Contact testContact = new Contact(FirstName='Test1', LastName='Contact1', AccountId=testEndUserAcnt.Id, OwnerId=UserInfo.getUserId());
        insert testContact;
        
        //Create test Case
        Case testCase = new Case(AccountId = testEndUserAcnt.Id, RecordTypeId=caseRecordTypes.get('QT Support Customer Portal Record Type'), ContactId=testContact.Id, Status='New', OwnerId=UserInfo.getUserId(), Origin='Customer Portal');
        insert testCase;
        
        //Requery inserted test Case so that we can now modify fields on it which will generate the Case History
        testCase = requeryTestCase(testEndUserAcnt.Id);
        
        //Editing fields on Case to generate the Case History       
        //Add Description, Status and Type to the test Case             
        testCase.Description = 'Test Description';
        testCase.Status = 'New';
        testCase.Type = 'Incident';
        update testCase;
        /*
        //Requery inserted test Case so that we can now modify fields on it which will generate the Case History
        testCase = requeryTestCase(testEndUserAcnt.Id);
        
        //Edit and change Description, Status and Type on the test Case
        testCase.Description = 'Changed the Test Description';
        testCase.Status = 'In Process';
        testCase.Type = 'Problem';
        update testCase;
        */
        /*
        //Requery inserted test Case so that we can now modify fields on it which will generate the Case History
        testCase = requeryTestCase(testEndUserAcnt.Id);
        
        //Delete the Description, Status and Type field values from the test Case
        testCase.Description = '';
        testCase.Status = '';
        testCase.Type = '';
        update testCase;
        */
        Test.startTest();
        
        Test.stopTest();        
    }    
}