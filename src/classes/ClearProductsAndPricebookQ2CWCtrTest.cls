/******* Change Log ***************************************************
* 2017-07-18 1.1 Srinivasan PR- Changes for QCW-2955
*11-1-2018 Shubham Added bypass rule true to pass validation error 
************************************************************************/
@isTest
public class ClearProductsAndPricebookQ2CWCtrTest{
static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';

    static testMethod void doTheClearTest(){
        QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator']; 
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, 
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.runAs(u) 
        {

            Test.startTest();
            Account  testAcc = QTTestUtils.createMockAccount('TestCompany', u, true);
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAcc;
            
            QTTestUtils.SetupNSPriceBook('GBP');
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
            Date closeDate = Date.today();
            closeDate.addMonths(2);
            //Create test Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp',
                Name = 'TestOpp',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = rt.Id,
                CurrencyIsoCode = 'GBP',
                AccountId = testAcc.Id,
                Signature_Type__c = 'Digital Signature',
                ForecastCategoryName = 'Omitted'
                //Closed_date_for_conga__c = closeDate
            );
            insert testNewOpp;
            system.debug('stage is '+testNewOpp.stageName);
            Test.stopTest();
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;         
            insert LineItem; 

            
            OpportunityLineItem oli = new OpportunityLineItem();            
            oli.OpportunityId = testNewOpp.Id;
            oli.Quantity = 1;
            oli.PricebookEntryId = PB;  
            oli.TotalPrice = 10;      
            insert oli; 
            testNewOpp.Pricebook2Id = null;
            update testNewOpp;
            ApexPages.StandardController StandardController = new ApexPages.StandardController(testNewOpp);
            ClearProductsAndPricebookQ2CWController controller = new ClearProductsAndPricebookQ2CWController(StandardController);
            controller.doTheClear();
            Opportunity opportunityResult = [Select Id, Closed_date_for_conga__c from Opportunity Where Id =: testNewOpp.Id limit 1];
            System.assertEquals(null, opportunityResult.Closed_date_for_conga__c);
        }
    }
   /********** 1.1 starts ************/
    static testMethod void nullifyLicRecipientForOEM(){
        QuoteTestHelper.createCustomSettings();
        if(Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Oem_order_OppRecordType__c==null){
             Steelbrick_Settings__c cSettings=new Steelbrick_Settings__c(id=Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').id);
             cSettings.Oem_order_OppRecordType__c='01220000000DXq5AAG';
             update cSettings;
        }
        Profile p = [select id from profile where name='System Administrator']; 
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, 
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.runAs(u){
            
            Account  testAcc = QTTestUtils.createMockAccount('TestCompany', u, true);
            recordType accRT=[SELECT id FROM RecordType WHERE sObjectType='Account' and  name='OEM Partner Account'];
            testAcc.RecordtypeId = accRT.id;
            update testAcc;
            
            List<Contact>contactList=QTTestUtils.CreateMockContacts(testAcc.id, 1);//insert contacts;
            
            QTTestUtils.SetupNSPriceBook('GBP');
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            RecordType rt = [SELECT Id FROm RecordType where sObjectType='Opportunity' and DeveloperName='OEM_Order' LIMIT 1];
            Date closeDate = Date.today();
            closeDate.addMonths(2);
            //Create test Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp',
                Name = 'TestOpp',
                Type = 'New Customer',
                Revenue_Type__c = 'OEM',
                CloseDate = Date.today(),
                StageName = 'OEM - Identified by Partner',
                RecordTypeId = rt.Id,
                CurrencyIsoCode = 'GBP',
                AccountId = testAcc.Id,
                Signature_Type__c = 'Digital Signature',
                ForecastCategoryName = 'Omitted',
                //Closed_date_for_conga__c = closeDate,
                License_Recipient__c=contactList[0].id,
                Solution_Type__c='Hosted',
                End_User_Account_del__c=testAcc.Id
            );
            insert testNewOpp; 
            testNewOpp.Pricebook2Id = null;
            update testNewOpp;
            Test.Starttest();
            ApexPages.StandardController StandardController = new ApexPages.StandardController(testNewOpp);
            ClearProductsAndPricebookQ2CWController controller = new ClearProductsAndPricebookQ2CWController(StandardController);
            controller.doTheClear();
            Test.Stoptest();
        }
    }
   /********** 1.1 Ends **************/

}