/**
    21.09.2012 Fluido CR#5226 Test, that triggers are skipped for Academic Program Opportunity
    2015-11-20 NAD    Replaced opportunity stage name 'Academic Program - Open' with 'AP - Open' per CR 66252
 */
 
@isTest
private class TestAcademicProgramOpportunityTriggers {

    static testMethod void testTriggerSkipping() {
        //Test newAccount if Record Type is "Academic Program Account", QlikTech Company should be "QlikTech International AB"
        List<RecordType> academicRecTypeFound = [select Id, DeveloperName from RecordType where SobjectType ='Account' and DeveloperName = 'Academic_Program_Account'];
        RecordType oppRecordType = [select Id from RecordType where SobjectType ='Opportunity' and DeveloperName = 'Academic_Program_Opportunity' LIMIT 1];
        String recType = String.valueOf(academicRecTypeFound[0].Id).substring(0,15);
        Academic_Program_Settings__c settingsFound = Academic_Program_Settings__c.getInstance('AcademicProgramSetting');
        Boolean found = true;
        String notFoundFieldId = 'TestingFieldNameId';
        if(settingsFound == null){
            found = false;
            settingsFound = new Academic_Program_Settings__c(Name='AcademicProgramSetting',
                                                             AccountRecordTypeId__c = recType, 
                                                             OpportunityRecordTypeId__c = oppRecordType.Id,
                                                             QlikTech_Company__c = 'QlikTech International AB',
                                                             QlikTech_Company_field_Id__c = notFoundFieldId);
            insert settingsFound;
        }
        QTCustomSettings__c Settings = new QTCustomSettings__c();
        Settings.Name = 'Default';
        Settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        Settings.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        Settings.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        Settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert Settings;
        QuoteTestHelper.createCustomSettings();
        Account testAcc = new Account (
            Name= 'TestsAcademicProgramOpportunityTriggersAccount1',
            QlikTech_Company__c = settingsFound.QlikTech_Company__c
        );  

        insert testAcc;
        Opportunity o = new Opportunity(Name = 'TestsAcademicProgramOpportunityTriggersOpp1', 
                                        AccountId = testAcc.Id, 
                                        CloseDate = Date.today() +30,
                                        RecordTypeId = oppRecordType.Id,
                                        Signature_Type__c = 'Digital Signature',
                                        StageName = 'AP - Open');
        
        Test.startTest();
        insert o;
        Test.stopTest();
        
    }
    
    // Commented out as the trigger Opportunity_SetDefaultPriceBook is no longer being used 
    /* 
    static testMethod void testDefaultPricebookSetting() {
        //Test newAccount if Record Type is "Academic Program Account", QlikTech Company should be "QlikTech International AB"
        List<RecordType> academicRecTypeFound = [select Id, DeveloperName from RecordType where SobjectType ='Account' and DeveloperName = 'Academic_Program_Account'];
        RecordType oppRecordType = [select Id from RecordType where SobjectType ='Opportunity' and DeveloperName = 'Academic_Program_Opportunity' LIMIT 1];
        Pricebook2 pb = [Select Name, IsActive, Id From Pricebook2 where Name='USDM' and IsActive = true];
        String recType = String.valueOf(academicRecTypeFound[0].Id).substring(0,15);
        Academic_Program_Settings__c settingsFound = Academic_Program_Settings__c.getInstance('AcademicProgramSetting');
        Boolean found = true;
        String notFoundFieldId = 'TestingFieldNameId';
        if(settingsFound == null){
            found = false;
            settingsFound = new Academic_Program_Settings__c(Name='AcademicProgramSetting',
                                                             AccountRecordTypeId__c = recType, 
                                                             OpportunityRecordTypeId__c = oppRecordType.Id,
                                                             QlikTech_Company__c = 'QlikTech International AB',
                                                             QlikTech_Company_field_Id__c = notFoundFieldId,
                                                             DefaultPriceBook__c = pb.Id);
            insert settingsFound;
        }
        Account testAcc = new Account (
            Name= 'TestsAcademicProgramOpportunityTriggersAccount1',
            QlikTech_Company__c = settingsFound.QlikTech_Company__c
        );  

        insert testAcc;
        
        Opportunity o = new Opportunity(Name = 'TestsAcademicProgramOpportunityTriggersOpp1', 
                                        AccountId = testAcc.Id, 
                                        CloseDate = Date.today() +30,
                                        RecordTypeId = oppRecordType.Id,
                                        StageName = 'AP - Open');
        
        Test.startTest();
        insert o;
        Test.stopTest();
        Opportunity oppFound = [select Id, Pricebook2Id from Opportunity where Id =: o.Id];
        system.assertEquals(pb.Id, oppFound.Pricebook2Id);
    }*/
}