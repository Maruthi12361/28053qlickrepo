/****************************************************************************************************
   
   CaseContentDocumentLinkPurge
   In some cases attachments are coming from ContentDocument, so to purge these document these are all residing in ContentDocumentLink
   So, if we delete these from ContentDocumentLink Object the attachments will not be shown
   IT-79 : Class to purge case ContenetDocumnetlink Object which are closed and older than 90 days
   AIN      2018-01-12 - Initial implementation
   IT-467 :Class Updated to work in partial success scenarios and also send emails with error Id's only when there are any- CRW 12/04/2018
   IT-606 : Add Database.Stateful to see variable changed in Execute phase in Finish - ext_bad  2018-05-16
          : Update job's behavior: - ext_vos 2018-05-25 
                - job handles attachments from Cases with several Record Types;
                - attachment will be deleted if it's linked with outdated cases and users only;
                - attachment will be ignored if it's also linked with "still actual" cases;
                - notification with attachment details will be sent if attachment is created by Customer\Partner user and is also linked with any entities besides users and cases.
    IT-2347 : Added Closure Code 'License Delivery' to exceptions - ext_bad 2020-01-29
****************************************************************************************************/
global without sharing class CaseContentDocumentLinkPurge implements Database.Batchable<SObject>, Database.Stateful {
    global final String query;
    global Integer successIdCount;
    global List<Id> errorIds = new List<Id>();
    global List<String> sendEmailsMultipleLinks = new List<String>();

    global final List<String> CASE_RECORD_TYPES = new List<String>{'QlikTech Master Support Record Type', 'QT Support Customer Portal Record Type',
            'QT Support Partner Portal Record Type', 'QT Problem Case Record Type'};
    global final String CASE_RECORD_TYPE_LIVE_CHAT = 'Live Chat Support Record Type';
    global final List<String> ACCOUNT_TYPES = new List<String>{'Partner', 'Customer'};
    global final String CLOSED_STATUS_CASE = 'Closed';
    global final String CLOSED_STATUS_CASE_LIVE_CHAT = 'Chat Resolved';
    global final String CLOSURE_CODE_LICENSE_DELIVERY = 'License Delivery';

    global static Id TEST_ERROR_ID;
    global static Boolean TEST_EMAIL_SENDING = false;

    global CaseContentDocumentLinkPurge() {
        List<ContentDocumentLink> contentDocuments;  
        if ([SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox || Test.isRunningTest()) {
            contentDocuments = [SELECT ContentDocumentId
                                    FROM ContentDocumentLink
                                    WHERE LinkedEntityId IN (SELECT Id
                                                                FROM Case
                                                                WHERE 
                                                                    (RecordType.Name IN :CASE_RECORD_TYPES AND Status = :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                                    OR 
                                                                    (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status = :CLOSED_STATUS_CASE_LIVE_CHAT)
                                                            )
                                    LIMIT 1000];                                          
        } else {
            contentDocuments = [SELECT ContentDocumentId
                                    FROM ContentDocumentLink
                                    WHERE LinkedEntityId IN (SELECT Id
                                                                FROM Case
                                                                WHERE LastModifiedDate < LAST_N_DAYS:90 
                                                                    AND (
                                                                        (RecordType.Name IN :CASE_RECORD_TYPES AND Status = :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                                        OR 
                                                                        (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status = :CLOSED_STATUS_CASE_LIVE_CHAT)
                                                                    )
                                                            )
                                    LIMIT 1000];                                                           
        }
        String idString = '';          
        for (ContentDocumentLink i : contentDocuments) {
            if (idString == '') {
                idString += '\'' + i.ContentDocumentId + '\'';
            } else {
                idString += ',\'' + i.ContentDocumentId + '\'';
            }
        }
        if (idString != '') {
            query = 'SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Id in (' + idString + ')';
        } else {
            query = 'SELECT Id FROM ContentDocument WHERE Title = \'asdadqweraesqaewqweasaaaaaasdczxcxzc\'';
        }

        System.debug('Myquery ' + query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ContentDocument> records) {
        Set<Id> attachments = new Set<Id>();
        Set<Id> docIds = new Set<Id>();
        for (ContentDocument rec : records) {
            attachments.add(rec.Id);
        }
        // For sandboxes: need to check if file (ContentVersion instance) exists.
        // Feed attachment objects structure can be corrupted during of refresh full copy sandboxes. 
        if ([SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox) {
            Map<Id,Id> versionMap = new Map<Id,Id>();
            for (ContentDocument rec : records) {
                versionMap.put(rec.Id, rec.LatestPublishedVersionId);
            }
            List<Id> versionIds = versionMap.values();
            // Add any custom field to query to get list of existed objects. 
            // In case query with "standard only" fields the resut will include also broken ContentVersion objects (that were deleted incorrectly)
            Map<Id,ContentVersion> versions = new Map<Id,ContentVersion>([SELECT Id, Created_By__c FROM ContentVersion WHERE Id IN :versionIds]);
            Set<Id> existedVersions = versions.keySet();
            for (Id attDocId : versionMap.keySet()) {
                if (existedVersions.contains(versionMap.get(attDocId))) {
                    docIds.add(attDocId);
                }
            }
        } else {
            docIds.addAll(attachments);
        }        
        if (!docIds.isEmpty()) {
            // list of related objects
            List<ContentDocumentLink> relatedLinks = [SELECT ContentDocumentId, LinkedEntityId
                                                        FROM ContentDocumentLink
                                                        WHERE ContentDocumentId IN: docIds];
            Set<Id> allEntityIds = new Set<Id>();      
            Map<Id,Set<Id>> attachAndEntityIds = new Map<Id,Set<Id>>();
            for (ContentDocumentLink link : relatedLinks) {
                // fill set of entities Id
                allEntityIds.add(link.LinkedEntityId);
                // fill map of "attachmentId + all linked entity Ids"
                Set<Id> temp = attachAndEntityIds.get(link.ContentDocumentId);            
                if (temp == null) {
                    temp = new Set<Id>();
                }
                temp.add(link.LinkedEntityId);            
                attachAndEntityIds.put(link.ContentDocumentId, temp);
            }
            if (!attachAndEntityIds.isEmpty()) {
                Map<Id,Case> outDateCases;
                Map<Id,Case> actualCases; 
                if ([SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox || Test.isRunningTest()) {
                    outDateCases = new Map <Id,Case>([SELECT Id FROM Case
                                                        WHERE Id IN: allEntityIds  
                                                            AND (
                                                                (RecordType.Name IN :CASE_RECORD_TYPES AND Status = :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                                OR 
                                                                (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status = :CLOSED_STATUS_CASE_LIVE_CHAT)
                                                            )
                                                    ]);
                    
                    actualCases = new Map <Id,Case>([SELECT Id FROM Case 
                                                        WHERE Id IN: allEntityIds 
                                                            AND (
                                                                (RecordType.Name IN :CASE_RECORD_TYPES AND Status != :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                                OR
                                                                (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status != :CLOSED_STATUS_CASE_LIVE_CHAT)
                                                            )
                                                    ]);
                } else {
                    outDateCases = new Map <Id,Case>([SELECT Id FROM Case
                                                        WHERE Id IN: allEntityIds 
                                                            AND LastModifiedDate < LAST_N_DAYS:90 
                                                            AND (
                                                                (RecordType.Name IN :CASE_RECORD_TYPES AND Status = :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                                OR 
                                                                (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status = :CLOSED_STATUS_CASE_LIVE_CHAT)
                                                            )
                                                    ]);
                    
                    actualCases = new Map <Id,Case>([SELECT Id FROM Case 
                                                        WHERE Id IN: allEntityIds
                                                            AND (
                                                                (RecordType.Name IN :CASE_RECORD_TYPES AND (Status != :CLOSED_STATUS_CASE
                                                                    OR (Status = :CLOSED_STATUS_CASE AND LastModifiedDate >= LAST_N_DAYS:90 AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)))
                                                                OR 
                                                                (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND (Status != :CLOSED_STATUS_CASE_LIVE_CHAT OR (Status = :CLOSED_STATUS_CASE_LIVE_CHAT AND LastModifiedDate >= LAST_N_DAYS:90)))
                                                            ) 
                                                    ]);
                }
                Map<Id,User> users = new Map<Id,User>([SELECT Id FROM User WHERE Id IN: allEntityIds]);

                // set includes linked entities Ids besides all outdated and actual cases and users
                Set<Id> otherEntities = new Set<Id>();
                otherEntities.addAll(allEntityIds);
                otherEntities.removeAll(outDateCases.keySet());
                otherEntities.removeAll(actualCases.keySet());
                otherEntities.removeAll(users.keySet());
                
                // make decision about attachment
                List<ContentDocument> toDelete = new List<ContentDocument>();
                       
                for (ContentDocument att : records) {
                    Set<Id> currentEntityIds = attachAndEntityIds.get(att.Id);
                    if (currentEntityIds != null) {
                        Integer fullCount = currentEntityIds.size();
                        // try to remove "other" entities from all linked entities
                        currentEntityIds.removeAll(otherEntities);
                        if (fullCount != currentEntityIds.size()) {
                            // attachment is linked to any "other" entities -> needs the notification
                            sendEmailsMultipleLinks.add(att.Id);
                        } else {
                            // try to remove "actual case" entities from all linked entities
                            currentEntityIds.removeAll(actualCases.keySet());
                            if (fullCount == currentEntityIds.size()) {
                                // attachment is linked to outdated cases (only) and users -> TBD
                                toDelete.add(att);
                            }
                        }
                    }                
                }
                //Added by CRW for IT-467
                successIdCount = 0;
                Database.DeleteResult[] crList = Database.delete(toDelete, false);
                for (Database.DeleteResult cr : crList) {
                    if (cr.isSuccess()) {
                        successIdCount++;
                    } else {
                        errorIds.add(cr.getId());
                    }
                }
                //Added by CRW for IT-467
            }
        }
    }

    global void finish(Database.BatchableContext BC) {
        if (Test.isRunningTest() && TEST_ERROR_ID != null) {
            errorIds.add(TEST_ERROR_ID);
        }
        if (!errorIds.isEmpty() || !sendEmailsMultipleLinks.isEmpty()) {
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
            
            List<String> parts = getToAddresses();                             
            String qlikNoreply = '';
            if (QTCustomSettings__c.getInstance('Default') != null) {
                qlikNoreply = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
            }
            OrgWideEmailAddress[] orgwideaddress = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :qlikNoreply LIMIT 1];
            if (!orgwideaddress.isEmpty()) {
                mail.setOrgWideEmailAddressId(orgwideaddress[0].Id);
            }
            mail.setToAddresses(parts);
            mail.setSubject('Feed Attachments Failed to Delete');
            String plainTextBody = '';
            
            // add info about skipped files
            if (!sendEmailsMultipleLinks.isEmpty()) {
                List<ContentDocumentLink> docLinksEmails; 
                if (Test.isRunningTest()) {
                    docLinksEmails = [SELECT ContentDocumentId, LinkedEntityId, ContentDocument.Title
                                        FROM ContentDocumentLink
                                        WHERE LinkedEntityId IN (
                                                SELECT Id
                                                FROM Case
                                                WHERE 
                                                    (RecordType.Name IN :CASE_RECORD_TYPES AND Status = :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                    OR 
                                                    (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status = :CLOSED_STATUS_CASE_LIVE_CHAT)
                                        )
                                        AND ContentDocumentId IN :sendEmailsMultipleLinks];
                } else {
                    docLinksEmails = [SELECT ContentDocumentId, LinkedEntityId, ContentDocument.Title
                                        FROM ContentDocumentLink
                                        WHERE LinkedEntityId IN (
                                                SELECT Id
                                                FROM Case
                                                WHERE 
                                                    (RecordType.Name IN :CASE_RECORD_TYPES AND Status = :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                    OR 
                                                    (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status = :CLOSED_STATUS_CASE_LIVE_CHAT) 
                                        )
                                        AND ContentDocumentId IN :sendEmailsMultipleLinks
                                        AND ContentDocument.CreatedBy.Contact.Account_Type__c IN :ACCOUNT_TYPES];
                }
                if (!docLinksEmails.isEmpty()) {
                     plainTextBody += 'Batch Process has completed. \r\n\nNumber of attachments skipped to delete: ' + docLinksEmails.size() + '\r\n';

                    for (ContentDocumentLink docLink : docLinksEmails) {
                        plainTextBody += '\r\n\nSkipped to delete \'' + docLink.ContentDocument.Title + '\' '
                                        + ' for Case ' + Url.getSalesforceBaseUrl().toExternalForm() + '/' + docLink.LinkedEntityId;
                    }
                }
            }

            // add info about corrupted files
            if (!errorIds.isEmpty()) {
                List<ContentDocumentLink> errorDocLinksEmails = [SELECT ContentDocumentId, LinkedEntityId, ContentDocument.Title
                                                                FROM ContentDocumentLink
                                                                WHERE LinkedEntityId IN (
                                                                        SELECT Id
                                                                        FROM Case
                                                                        WHERE 
                                                                            (RecordType.Name IN :CASE_RECORD_TYPES AND Status = :CLOSED_STATUS_CASE AND Closure_Code__c != :CLOSURE_CODE_LICENSE_DELIVERY)
                                                                            OR 
                                                                            (RecordType.Name = :CASE_RECORD_TYPE_LIVE_CHAT AND Status = :CLOSED_STATUS_CASE_LIVE_CHAT)
                                                                )
                                                                AND ContentDocumentId IN :errorIds];
                if (!errorDocLinksEmails.isEmpty()) {
                     plainTextBody += 'Batch Process has completed. \r\n\nNumber of attachments failed to delete: ' + errorDocLinksEmails.size() + '\r\n';

                    for (ContentDocumentLink docLink2 : errorDocLinksEmails) {
                        plainTextBody += '\r\n\nFailed \'' + docLink2.ContentDocument.Title + '\' '
                                        + ' for Case ' + Url.getSalesforceBaseUrl().toExternalForm() + '/' + docLink2.LinkedEntityId;
                    }
                }
            }
            if (!String.isEmpty(plainTextBody)) {
                mail.setPlainTextBody(plainTextBody);            
                mails.add(mail);
                if (Test.isRunningTest()) {
                    TEST_EMAIL_SENDING = true;
                }
                Messaging.sendEmail(mails);                
            }            
        }
    }

    private List<String> getToAddresses() {
        List<String> listTo = new List<String>();
        QTCustomSettings__c allEmailAddress = QTCustomSettings__c.getValues('Default');
        String emailAddress = allEmailAddress.CaseAttachmentPurgeEmail__c;
        if (!String.isEmpty(emailAddress)) {
            listTo = emailAddress.split(',');
        }
        System.debug('Email To >>' + listTo);
        return listTo;
    }
}