/************************************
* Class: StatementsController
* Modified: xx/xx/xxxx MHG  Initial Code CR#409
*           22/04/2011 RDZ  Issue line 152 to retrive LastTransactionDate: over sfdc limit 50001 records
*           20/11/2012 RDZ  CR# 3958 #dev Change to Customer Autostatement email template
*                           https://eu1.salesforce.com/a0CD000000G0AsV Add information about invoices to write it back to navision
*           21/03/2013 RDZ  Set limit set to true so we only get a query with a limit.
*                           Check issue with QTEntity. Case# 00159495 https://eu1.salesforce.com/500D000000N8fqG
*                           CR# 7742 https://eu1.salesforce.com/a0CD000000XOktl
*           25/03/2013 RDZ  Set limit on max number of emails sent at the time, for now its using magic number on code and its set to 10 emails.
*                           CR# 7742 https://eu1.salesforce.com/a0CD000000XOktl fixing bug for CR.
*           28/9/2015  SLH  Added CreatedDate to query on line 115/222 to avoid unconstrained query issue 
*
**************************************/
public with sharing class StatementsController 
{
    //This will set or not Limits on Soql queries see GetLimitSOQL() method
    public boolean setLimit = true;
    
    public StatementsController()
    {
        if (System.currentPagereference().getParameters().containsKey('AID'))
        {
            AID = System.currentPagereference().getParameters().get('AID');
        }   
        if (System.currentPagereference().getParameters().containsKey('DDP'))
        {
            DDP = System.currentPagereference().getParameters().get('DDP');
        }   
        System.Debug('-------------------------------------');
        System.Debug('QTEntity: ' + QTEntity);
        if (QTEntity == '' || QTEntity == null)
        {
            User u = [select Id, QlikTech_Company__c from User where Id = :Userinfo.getUserId() LIMIT 1]; 
            
            QTEntity = u.QlikTech_Company__c;
        }
        
        if (Template == null)
        {
            Template = 'StatementPreview_1';
        }
    }
    
    /*
    Stuff used for the preview page
    */
    public string DueDate { get; set; } 
    public string QTEntity { get; set; }
    public string Template { get; set; }
    
    public boolean getisTest() 
    {
        String orgId = UserInfo.getOrganizationId();
        
        System.debug('Trying to assign isTest');
        //Live Env Org Id = 00D20000000IGPX
        return orgId.startsWith('00D20000000IGPX')? false: true;    
            
    }
    
   
    private string AID;
    public string getAID() 
    {
        if (AID == null || AID == '')
        {
            System.debug('Trying to assign AID');
            if (System.currentPagereference().getParameters().containsKey('AID'))
            {
                AID = System.currentPagereference().getParameters().get('AID');
            }           
        }
        return AID;
    }
    public void setAID(string AA)
    {
        AID = AA;
    }
    
    private string DDP;
    public string getDDP()
    {       
        if (DDP == null || DDP == '')
        {
            System.debug('Trying to assign DDP');
            if (System.currentPagereference().getParameters().containsKey('DDP'))
            {
                DDP = System.currentPagereference().getParameters().get('DDP');
            }               
        }       
        return DDP;
    }
    public void setDDP(string DD)
    {
        DDP = DD;
    }
    
    private StatementListItem Item;
    public StatementListItem getItem()
    {
        if (Item != null)
        {
            System.debug('Item was not null!');
            return Item;
        }
        
        if (getDDP() != '')
        {
            DueDate = getDDP();
        }
        
        Item = null;
        
        string soql = 'select Account__r.Id, PurchaseNo__c, Account__r.QlikTech_Company__c, Account__r.Navision_Customer_Number__c, Account__r.Name, DocumentNo__c, Posting_Date__c, Due_Date__c, Total_Amount__c, Remaining_Amount__c, Account__r.Billing_Contact__r.Name, Account__r.Billing_Contact__r.Email, Account__r.Billing_Contact__r.Id, Account__r.Billing_Contact__r.Email_Has_Bounced__c, Emailed__c, Navision_Comment__c,  Navision_Full_Comment__c, Rapidi_Transfer_Field__c from StatementHeader__c where Account__r.Id = :AID and Emailed__c = false and Remaining_Amount__c > 0 and createddate > 2014-01-01T00:00:00Z'; 
        if (DueDateSOQL() != '')
        {
            soql += DueDateSOQL();
        }
        //RDZ: Fix bug CR# 7742 https://eu1.salesforce.com/a0CD000000XOktl, we may need to uncomment it when customer statements can be only sent per Finance Department in different QTCompanies.
        //Select form at the top of the list_Statements.page needs to be filterd to display just companies for the current user.
        //if (QTEntitySOQL() != '')
        //{
        //    soql += QTEntitySOQL();         
        //}       
        string slimit = '';
        slimit = GetLimitSOQL();
        if (slimit != '')
        {
            soql += slimit;
        }
        
        System.Debug('------------------------------');
        System.Debug('Soql on getItem: ' + soql);
        System.Debug('------------------------------');
        

        for (StatementHeader__c SH : database.query(soql))
        {
            if (Item == null)
            {
                Item = new StatementListItem();
            
                Item.AccountId = SH.Account__r.Id;
                Item.AccountName = SH.Account__r.Name;
                Item.BillingContactId = SH.Account__r.Billing_Contact__r.Id;
                Item.BillingContactName = SH.Account__r.Billing_Contact__r.Name;
                Item.BillingContactEmail = SH.Account__r.Billing_Contact__r.Email;
                Item.NavisionCustomerNbr = SH.Account__r.Navision_Customer_Number__c;
                Item.QlikTechEntity = SH.Account__r.QlikTech_Company__c;
                Date d = Date.today().addDays(15);
                Item.HashDate = d.year() + '-' + d.month() + '-' + d.day();
                Item.Selected = false;
                Item.Emailed = SH.Emailed__c;
                Item.HasBouncedEmail = SH.Account__r.Billing_Contact__r.Email_Has_Bounced__c==1?true:false;                        
                Item.NavisionComment = SH.Navision_Comment__c;
                Item.NavisionFullComment = SH.Navision_Full_Comment__c;
                Item.RapidiTransferField = SH.Rapidi_Transfer_Field__c; 
                
            }
                        
            StatementListSubItem SubItem = new StatementListSubItem();      
            SubItem.StatementId = SH.Id;
            SubItem.InvoiceNbr = SH.DocumentNo__c;
            SubItem.PostDate = SH.Posting_Date__c;
            SubItem.DueDate = SH.Due_Date__c;
            SubItem.Amount = SH.Total_Amount__c;
            SubItem.PO = SH.PurchaseNo__c;
            SubItem.RemainingAmount = SH.Remaining_Amount__c;
            SubItem.Hash = GetHash(Item.NavisionCustomerNbr, SubItem.InvoiceNbr, Item.QlikTechEntity, Item.HashDate);
            Item.AddStatement(SubItem);
        }       
        
        return Item;        
    }
    
    private User Me;    
    public User getUser()
    {
        if (Me != null)
        {
            return Me;
        }
        
        Me = [select Id, Name, Email, Title, Phone, Fax, MobilePhone from User where Id = :UserInfo.getUserId() LIMIT 1];
        
        return Me;
    }

    
    /*
     Stuff used for the list page
     */
    private Date LastTransactionDate = null;
    public Date getLastTransactionDate()
    {
        if (LastTransactionDate == null)
        {           
            string soql = 'select Transaction_Date__c from StatementHeader__c ORDER BY Transaction_Date__c DESC LIMIT 1';
            for (StatementHeader__c SH : database.query(soql))
            {
                LastTransactionDate = (Date) SH.Transaction_Date__c;
            }    
        }
                
    
        return LastTransactionDate;
    }
         
    private transient List<StatementListItem> StatementList;  
    public List<StatementListItem> getStatementList()
    {
         
        if (StatementList != null)
        {
            return StatementList;
        }
        
        StatementList = new List<StatementListItem>();
        Map<Id, StatementListItem> AccountToStatementListItemMap = new Map<Id, StatementListItem>();

        string soql = 'select Id, Account__r.Id, Account__r.QlikTech_Company__c, Account__r.Navision_Customer_Number__c, PurchaseNo__c, Account__r.Name, DocumentNo__c, Posting_Date__c, Due_Date__c, Total_Amount__c, Remaining_Amount__c,  Account__r.Billing_Contact__r.Name, Account__r.Billing_Contact__r.Email, Account__r.Billing_Contact__r.Id, Account__r.Billing_Contact__r.Email_Has_Bounced__c, Emailed__c, Navision_Comment__c,  Navision_Full_Comment__c, Rapidi_Transfer_Field__c from StatementHeader__c where Emailed__c = false AND Remaining_Amount__c > 0 and CreatedDate > 2014-01-01T00:00:00Z'; //Emailed__c = false and
        if (DueDateSOQL() != '')
        {
            soql += DueDateSOQL();
        }
        if (QTEntitySOQL() != '')
        {
            soql += QTEntitySOQL();         
        }

        string slimit = '';
        slimit = GetLimitSOQL();
        if (slimit != '')
        {
            soql += slimit;
        }
        
        System.Debug('------------------------------');
        System.Debug('Soql on getItem: ' + soql);
        System.Debug('------------------------------');
        
        
        for (StatementHeader__c SH : database.query(soql))
        {
            StatementListItem ThisItem;
            if (AccountToStatementListItemMap.containsKey(SH.Account__r.Id))
            {
                ThisItem = AccountToStatementListItemMap.get(SH.Account__r.Id); 
            }
            else
            {
                ThisItem = new StatementListItem();
                AccountToStatementListItemMap.put(SH.Account__r.Id, ThisItem);
                StatementList.add(ThisItem);

                ThisItem.AccountId = SH.Account__r.Id;
                ThisItem.AccountName = SH.Account__r.Name;
                ThisItem.BillingContactId = SH.Account__r.Billing_Contact__r.Id;
                ThisItem.BillingContactName = SH.Account__r.Billing_Contact__r.Name;
                ThisItem.BillingContactEmail = SH.Account__r.Billing_Contact__r.Email;
                ThisItem.NavisionCustomerNbr = SH.Account__r.Navision_Customer_Number__c;
                ThisItem.QlikTechEntity = SH.Account__r.QlikTech_Company__c;
                Date d = Date.today().addDays(15);
                ThisItem.HashDate = d.year() + '-' + d.month() + '-' + d.day();
                ThisItem.Selected = false;  
                ThisItem.Emailed = SH.Emailed__c;
                ThisItem.HasBouncedEmail = SH.Account__r.Billing_Contact__r.Email_Has_Bounced__c==1?true:false;                        
                ThisItem.NavisionComment = SH.Navision_Comment__c;
                ThisItem.NavisionFullComment = SH.Navision_Full_Comment__c;
                ThisItem.RapidiTransferField = SH.Rapidi_Transfer_Field__c;              
            }
            

            StatementListSubItem Item = new StatementListSubItem();
            Item.StatementId = SH.Id;
            Item.InvoiceNbr = SH.DocumentNo__c;
            Item.PostDate = SH.Posting_Date__c;
            Item.DueDate = SH.Due_Date__c;
            Item.Amount = SH.Total_Amount__c;
            Item.RemainingAmount = SH.Remaining_Amount__c;
            Item.PO = SH.PurchaseNo__c;
            Item.Hash = GetHash(ThisItem.NavisionCustomerNbr, Item.InvoiceNbr, ThisItem.QlikTechEntity, ThisItem.HashDate);
            
            ThisItem.AddStatement(Item);    
        }
        
        return StatementList;
    }
    
    private transient List<StatementListItem> EmailedStatementList;  
    public List<StatementListItem> getEmailedStatementList()
    {
         
        if (EmailedStatementList != null)
        {
            return EmailedStatementList;
        }
                
        string query = 'select Id, Account__r.Id, Account__r.QlikTech_Company__c, Account__r.Navision_Customer_Number__c, PurchaseNo__c, Account__r.Name, DocumentNo__c, Posting_Date__c, Due_Date__c, Total_Amount__c, Remaining_Amount__c,  Account__r.Billing_Contact__r.Name, Account__r.Billing_Contact__r.Email, Account__r.Billing_Contact__r.Id, Account__r.Billing_Contact__r.Email_Has_Bounced__c, Emailed__c, Navision_Comment__c,  Navision_Full_Comment__c, Rapidi_Transfer_Field__c from StatementHeader__c where Emailed__c = true ';       
        EmailedStatementList = getStatementItemListByQuery(query);
        
        return EmailedStatementList;
    }

    /***************************************************************
    * Private method 
    * Args: String Query, initial query to retrive StatementList,
    *       from query a full soql query will be generated to add date and limit depending on custom setttings
    * Does, from query will return a List<StatementListItem>.
    * Return: a List<StatementListItems> populated will all results from soql query.
    ****************************************************************/
    private List<StatementListItem> getStatementItemListByQuery(string query)
    {
        if (string.isEmpty(query) || string.isBlank(query))
        {
            return null;
        }
        
        
        string soql = buildStatementSoqlQuery(query);
        
        System.Debug('------------------------------');
        System.Debug('Soql on getItem: ' + soql);
        System.Debug('------------------------------');
        return getStatementItemListFromSoql(soql);
        
    }
    
    private string buildStatementSoqlQuery(string query)
    {
        string soql = query; //'select Id, Account__r.Id, Account__r.QlikTech_Company__c, Account__r.Navision_Customer_Number__c, PurchaseNo__c, Account__r.Name, DocumentNo__c, Posting_Date__c, Due_Date__c, Total_Amount__c, Remaining_Amount__c,  Account__r.Billing_Contact__r.Name, Account__r.Billing_Contact__r.Email, Account__r.Billing_Contact__r.Id, Account__r.Billing_Contact__r.Email_Has_Bounced__c, Emailed__c from StatementHeader__c where Remaining_Amount__c > 0 '; //Emailed__c = false and
        if (DueDateSOQL() != '')
        {
            soql += DueDateSOQL();
        }
        if (QTEntitySOQL() != '')
        {
            soql += QTEntitySOQL();         
        }
        
        string slimit = '';
        slimit = GetLimitSOQL();
        if (slimit != '')
        {
            soql += slimit;
        }
        return soql;
    }
    
    private List<StatementListItem> getStatementItemListFromSoql(string soql)
    {
        Map<Id, StatementListItem> AccountToStatementListItemMap = new Map<Id, StatementListItem>();
        List<StatementListItem> lStatement = new List<StatementListItem>();
        for (StatementHeader__c SH : database.query(soql))
        {
            StatementListItem sItem;
            if (AccountToStatementListItemMap.containsKey(SH.Account__r.Id))
            {
                sItem = AccountToStatementListItemMap.get(SH.Account__r.Id); 
            }
            else
            {
                //sItem = StatementItem (Header)
                sItem = new StatementListItem();
                AccountToStatementListItemMap.put(SH.Account__r.Id, sItem);
                lStatement.add(sItem);

                sItem.AccountId = SH.Account__r.Id;
                sItem.AccountName = SH.Account__r.Name;
                sItem.BillingContactId = SH.Account__r.Billing_Contact__r.Id;
                sItem.BillingContactName = SH.Account__r.Billing_Contact__r.Name;
                sItem.BillingContactEmail = SH.Account__r.Billing_Contact__r.Email;
                sItem.NavisionCustomerNbr = SH.Account__r.Navision_Customer_Number__c;
                sItem.QlikTechEntity = SH.Account__r.QlikTech_Company__c;
                Date d = Date.today().addDays(15);
                sItem.HashDate = d.year() + '-' + d.month() + '-' + d.day();
                sItem.Selected = false;  
                sItem.Emailed = SH.Emailed__c;
                sItem.HasBouncedEmail = SH.Account__r.Billing_Contact__r.Email_Has_Bounced__c==1?true:false;                        
                sItem.NavisionComment = SH.Navision_Comment__c;
                sItem.NavisionFullComment = SH.Navision_Full_Comment__c;
                sItem.RapidiTransferField = SH.Rapidi_Transfer_Field__c;
            
            }
            
            //Item = Statement Sub Item (Invoice)
            StatementListSubItem Item = new StatementListSubItem();
            Item.StatementId = SH.Id;
            Item.InvoiceNbr = SH.DocumentNo__c;
            Item.PostDate = SH.Posting_Date__c;
            Item.DueDate = SH.Due_Date__c;
            Item.Amount = SH.Total_Amount__c;
            Item.RemainingAmount = SH.Remaining_Amount__c;
            Item.PO = SH.PurchaseNo__c;
            Item.Hash = GetHash(sItem.NavisionCustomerNbr, Item.InvoiceNbr, sItem.QlikTechEntity, sItem.HashDate);
            
            sItem.AddStatement(Item);    
        }
        
        return lStatement;
    }
    
    public Set<Id> getAccountSetofSelectedStatement()
    {
        Set<Id> AccountSet = new Set<Id>();
         
        if (StatementList != null)
        {
            for (StatementListItem SLI: StatementList)
            {
                if (SLI.Selected)
                {
                    AccountSet.add(SLI.AccountId);
                }
            }
        }
        System.Debug('------------------------------------------');
        System.Debug('Selected Accounts to email: ' + AccountSet);
        System.Debug('------------------------------------------');
        
        return AccountSet;
    }
    
    
   
   
    public PageReference SendEmails()
    {
        
        System.Debug('------------------------------------');
        //integer myStatementLimit = Limits.getScriptStatements();
        //System.Debug('myDMLLimit = Limits.getScriptStatements(); ' + myStatementLimit);
        System.Debug('------------------------------------');
        List<Messaging.SingleEmailMessage> Msgs = new List<Messaging.SingleEmailMessage>();
        string OrigAid = AID;
        integer nselected = 0;
        
        PageReference currentpage = ApexPages.currentPage();
        
        
        StatementList = getStatementList();
        EmailedStatementList = getEmailedStatementList();
        
        for(StatementListItem SLI : StatementList)
        {
            if (SLI.Selected)
            {
                nselected++;
                System.debug('///////////////////////////////');
                
            }
        }
        System.debug('Number of account selected are: '+ nselected );
        if(nselected > 10)
        {
            //To avoid error: 
            /*SendEmail failed. First exception on row 16; first error: LIMIT_EXCEEDED, System.LimitException: Too many SOQL queries: 101: []
            Error is in expression '{!SendEmails}' in component <apex:page> in page list_statements*/
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Max number of emails is 10, please select less than 10 Acc to send statements to.'));
            return currentpage;
        }
        
        for (StatementListItem SLI : StatementList)
        {           
            
            //System.Debug('myStatementLimit = Limits.getScriptStatements(); ' + myStatementLimit);
            
            if (SLI.BillingContactId == '' || SLI.BillingContactId == null || !SLI.Selected)
            {
                continue;
            }
            
            
            PageReference EmailPreview;
            if (Template == 'StatementPreview_2')
            {
                EmailPreview = Page.StatementPreview_2;
            }
            else
            {
                EmailPreview = Page.StatementPreview_1;
            }
            System.Debug('AID:' + SLI.AccountId);
            System.Debug('DDP:' + DueDate);
            EmailPreview.getParameters().put('AID', SLI.AccountId);
            EmailPreview.getParameters().put('DDP', DueDate);
            Blob MessageBody;
            
            try
            {
                MessageBody = EmailPreview.getContent();
            }
            catch (VisualforceException e)
            {
                MessageBody = Blob.valueOf('Error generating customer Statement');
            }
            
            Messaging.SingleEmailMessage Msg = new Messaging.SingleEmailMessage();
            Msg.setUseSignature(false);
            Msg.setTargetObjectId(SLI.BillingContactId);
            Msg.setSaveAsActivity(true);
            
            Msg.setSubject(getisTest()? 'Sandbox Test:' + SLI.AccountName + ': Accounts Receivable' : SLI.AccountName + ': Accounts Receivable');
            Msg.setHtmlBody(MessageBody.toString());
            List<string> CCAddresses = new List<string>();
            List<string> BCCAddresses = new List<string>();
            if (getisTest())
            {
                BCCAddresses.Add('rdz@qlikview.com'); //Set your email for testing
            }
            else{
                //Get email from Custom Settings (Email_List__c) if not use default AccountsReceivableNA@qlikview.com
                CCAddresses = GetEmailListCustomSettings();
                BCCAddresses.Add('rdz@qlikview.com'); //This will be remove after acceptance period in live           
            }           
                       
            
            Msg.setCcAddresses(CCAddresses);
            Msg.setBccAddresses(BCCAddresses);       
            
            Msgs.add(Msg);  
        }
        
        if (Msgs.size() > 0)
        {
            try {
                Messaging.sendEmail(Msgs);      
            }
            catch (system.EmailException ex) {
                system.debug('sendEmail returned an error: ' + ex.getMessage());
            }
        }

        
        AID = OrigAid;
        
        //myStatementLimit = Limits.getScriptStatements();
        //System.Debug('myStatementLimit = Limits.getScriptStatements(); ' + myStatementLimit);
            
        
        return SetStatementsAsEmailed(true);
    }
    
    
    
    public PageReference SetStatementsAsEmailed()
    {
        return SetStatementsAsEmailed(false);
    }
    
    //Modified by RDZ to get a String with all invoce numbers sent in the email
    //CR# 3958 #dev Change to Customer Autostatement email template
    //Modify write back to navision to include all invoice numbers with the following format
    //[Date: YYYYMMDD] Autostatement sent to [Recipient email] at [Company name] for invoices: [include all invoice numbers in Autostatement.]
    public PageReference SetStatementsAsEmailed(Boolean SaveComment)
    {
        //integer myStatementLimit = Limit.getScriptStatements();
        Set<Id> AccountSet = getAccountSetofSelectedStatement();
        String sComment = '';
        System.Debug('AccountSet: ' + AccountSet);
        string soql = 'select Id, Account__r.Id, Account__r.Name, DocumentNo__c, Navision_Comment__c, Posting_Date__c, Due_Date__c, Total_Amount__c, Remaining_Amount__c,  Account__r.Billing_Contact__r.Name, Account__r.Billing_Contact__r.Email, Account__r.Billing_Contact__r.Id from StatementHeader__c where Account__r.Id in :AccountSet and Emailed__c = false and Remaining_Amount__c > 0 ';
        if (DueDateSOQL() != '')
        {
            soql += DueDateSOQL();
        }
        if (QTEntitySOQL() != '')
        {
            soql += QTEntitySOQL();         
        }
        
        string slimit = '';
        slimit = GetLimitSOQL();
        if (slimit != '')
        {
            soql += slimit;
        }
        
        System.Debug('------------------------------');
        System.Debug('Soql on SetStatementsAsEmailed: ' + soql);
        //System.Debug('myDMLLimit = Limits.getScriptStatements(); ' + myStatementLimit);
        System.Debug('------------------------------');
        List<String> sArgList = new List<String>();

        List<StatementHeader__c> ToUpdate = new List<StatementHeader__c>();

        for (StatementHeader__c SH : database.query(soql))
        {
            for (StatementListItem SLI : StatementList)
            {
                if (SLI.AccountId == SH.Account__r.Id && SLI.Selected)
                {
                    SH.Emailed__c = true;
                    sComment = DateTime.Now().format('yyyyMMdd') + ' Autostatement sent to ' + SLI.BillingcontactName + ' (' + SLI.BillingcontactEmail + ') at ' + SLI.AccountName+ ' for invoices: ' + SLI.getInvoiceNumbersString();
                                               
                    //if sComment is more than 255 we have to truncate it. Send email to notify the issue.
                    if (sComment.length()>255)
                    {
                        sArgList.add(SLI.AccountId);
                        sArgList.add(sComment);
                        SH.Navision_Full_Comment__c = String.format('Navision Comment too long, has been truncated: (AccId:{0}) - {1}' , sArgList);
                        sArgList.clear();
                    }
                    
                    SH.Navision_Comment__c = sComment.length()>255? sComment.substring(0, 250)+'...':sComment;//Add Invoice numbers to Navision_Comment__c
                    ToUpdate.Add(SH);
                    break;
                }
            }
        }
        
        if (ToUpdate.size() > 0)
        {
            update ToUpdate;
        }
        
        StatementList = null;
        EmailedStatementList = null;
        return null;    
    }
    
    public PageReference Click()
    {
        StatementList = null;
        EmailedStatementList = null;
        return null;        
    }
    
    private string DueDateSOQL()
    {
        Date ToDay = Date.today();  
    
        if (DueDate == '15')
        {       
            ToDay = ToDay.addDays(-15);
        }
        else if (DueDate == '30')
        {
            ToDay = ToDay.addDays(-30);
        }
        else if (DueDate == '60')
        {
            ToDay = ToDay.addDays(-60);
        }
        
        return 'and Due_Date__c < ' +ToDay.year() + '-' + (ToDay.month() < 10 ? '0' + ToDay.month() : '' + ToDay.month()) + '-' + (ToDay.day() < 10 ? '0' + ToDay.day() : '' + ToDay.day()) + ' ';
    }
 
    private string QTEntitySOQL()
    {
        if (QTEntity != 'All' && QTEntity != null)
        {
            return 'and Account__r.QlikTech_Company__c = \'' + QTEntity + '\' ';
        }
        return '';
    }
    
    public string GetLimitSOQL()
    {
        integer ilimit = 1000; //If set limit = true and no value in Custom Settings will take 1000 as limit for the soql query 'LIMIT 1000'
        
        Customer_Statement_Custom_Settings__c MySetting = Customer_Statement_Custom_Settings__c.getValues('Default');
        if (MySetting == null)
        {
            System.Debug('No Customer_Statement_Custom_Settings__c found');
        }   
        else
        {
            ilimit = MySetting.Statement_Query_Limit__c.intValue(); //Getting limit from Custom Settings if exists.
        }
        
        if (setLimit)
        {
            return ' Limit '+ ilimit;
        }
        //No Limit
        return ''; //If Set limit = false, no limit will be added to the soql query.
    }
    
    private List<string> GetEmailListCustomSettings()
    {
        string sEmailList; //If email list set return email list if not return default settings
        List<string> EmailList = new List<string>();
        
        Customer_Statement_Custom_Settings__c MySetting = Customer_Statement_Custom_Settings__c.getValues('Default');
        if (MySetting == null)
        {
            System.Debug('No Customer_Statement_Custom_Settings__c found');
        }   
        else
        {
            sEmailList = MySetting.Email_List__c; //Getting email list from Custom Settings if exists.
        }
        
        if (String.isNotEmpty(sEmailList) && String.isNotBlank(sEmailList))
        {
            return  sEmailList.split(';', 20); //Will accept a list of at least 20 email address, semi-colon separated
        }
        //If not return empty list
        return EmailList;       
    }
 
    public class StatementListItem
    {
        private List<StatementListSubItem> Statements = new List<StatementListSubItem>();
        
        public ID AccountId { get; set; }
        public string AccountName { get; set; } 
        public Boolean Selected { get; set; }   
        public string BillingContactId { get; set; }
        public string BillingContactName { get; set; }
        public string BillingContactEmail { get; set; }
        public string NavisionCustomerNbr { get; set; }
        public string QlikTechEntity { get; set; }
        public string HashDate { get; set; }
        public Boolean Emailed { get; set; }  
        public Boolean HasBouncedEmail {get; set;}
        public string NavisionComment{get;set;}
        public string NavisionFullComment{get;set;}
        public datetime RapidiTransferField{get;set;}
        
        //to add to query Navision_Comment__c,  Navision_Full_Comment__c
        
        public List<StatementListSubItem> getStatements()
        {
            return Statements;
        }
        
        public void AddStatement(StatementListSubItem Item)
        {
            Statements.Add(Item);
        }

        //Added by RDZ to get a String with all invoce numbers sent in the email
        //CR# 3958 #dev Change to Customer Autostatement email template
        //Modify write back to navision to include all invoice numbers with the following format
        //[Date:YYYYMMDD]Autostatement sent to [Recipient email] at [Company name] for invoices: [include all invoice numbers in Autostatement.]
        public String getInvoiceNumbersString()
        {
            String InvoicesString = '';
            for (StatementListSubItem item:Statements)
            {
                InvoicesString += item.InvoiceNbr + ';';
            }
            
            return InvoicesString;
        }
        
    }
    
    private string GetHash(string CNBR, string INBR, string QENT, string VALIDTODATE)
    {
        return EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(CNBR + INBR + '7!akncAFN23uayfhb/asd2' + QENT + VALIDTODATE))).toUpperCase();
    }
    
    public class StatementListSubItem
    {
        public string StatementId { get; set; } 
        public string InvoiceNbr { get; set; }
        public date PostDate { get; set; }      
        public date DueDate { get; set; }
        public decimal Amount { get; set; }
        public decimal RemainingAmount { get; set; }
        public string PO { get; set; }
        public string Hash { get; set; }         
    }
}