/**
*	Class Name: AccountSharingHandler
*	2018-04-01 | AYS | BMW-739 | Created 
*/
public class AccountSharingHandler {
	
	public static void onBeforeUpdate(Account[] oldRecords, Account[] updatedRecords, Map<ID, Account> recordMap, Map<ID, Account> oldRecordMap) {
		
		Account_Settings__c cs = Account_Settings__c.getInstance();

		Id profileId = UserInfo.getProfileId();
		String profilesIds  = !Test.isRunningTest() ? cs.Rerun_Sharing_Profiles__c : UserInfo.getProfileId();

		if(profilesIds == null){
			return;
		}
		List<Id> l_profileIds = profilesIds.split(',');

		//run logic only to profiles which have FLS to read/edit Rerun_Sharing_Rules_Limit__c
		if(l_profileIds.contains(profileId)){

			Integer accountsLimit  = (Integer)cs.Rerun_Sharing_Rules_Limit__c;

			Set<Id> s_partnerId = new Set<Id>();
			//List<Account> l_accToUncheck = new List<Account>();
			
	 		for(Account acc : updatedRecords) {
	 			System.debug('debug_acc.Rerun_Sharing_Rules__c ' + acc.Rerun_Sharing_Rules__c);

				if(acc.Rerun_Sharing_Rules__c && !oldRecordMap.get(acc.id).Rerun_Sharing_Rules__c){
					//set ids to take end users
					s_partnerId.add(acc.Id);
					//process to uncheck checkbox
					acc.Rerun_Sharing_Rules__c = false;
					//l_accToUncheck.add(acc);
				}
			}

			//collect End Users
			if(!s_partnerId.isEmpty()){
				
				Set<Id> s_endUserIds = new Set<Id>();
				for(Responsible_Partner__c pr : [SELECT id, End_User__c 
												FROM Responsible_Partner__c 
												WHERE Partner__c IN : s_partnerId]) {
					s_endUserIds.add(pr.End_User__c);
				}
				

				if(s_endUserIds.isEmpty()){		
					throw new CustomException(Label.BMW739_error_emtly_list);
				}	
				else if(s_endUserIds.size() > accountsLimit){	
					throw new CustomException(Label.BMW739_to_mach_end_users_to_re_share);
				}
				else{
					//run normal batch process
					List<Id> l_endUserIds = new List<Id>();
					l_endUserIds.addAll(s_endUserIds);
					Database.executeBatch(new AccountSharingBatch(l_endUserIds), 50);
				}
				
			}
			

 		} 

	}

}