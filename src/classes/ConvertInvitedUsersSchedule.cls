/********************************************************
* CLASS: ConvertInvitedUsersSchedule
* DESCRIPTION: Schedule class for ConvertInvitedUsers
*
*
* CHANGELOG:
*   2019-10-17 - BAD - BMW-1792 - Added Initial logic
*********************************************************/
global class ConvertInvitedUsersSchedule implements Schedulable {
	global static void execute (SchedulableContext sc)
	{
		ConvertInvitedUsers.convertLeads();
	}
}