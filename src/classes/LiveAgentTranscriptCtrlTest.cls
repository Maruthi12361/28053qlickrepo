// CR# 11434 - Test case to test LiveAgentTranscriptCtrl apex class
// Changelog:
// March 13, 2014 - Madhav Kakani - Fluido Oy - Initial Implementation
// LCE-35 2018-01-30 - AIN - Changes to the post chat survey
// LCE-53 2018-02-04 - AIN - Changes to the post chat survey
// IT-1897  29.05.2019 - extbad  Update 'Close Chat Survey Id'
@isTest
public class LiveAgentTranscriptCtrlTest {
    static testMethod void runTest() {
        Boolean isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;

        QTTestUtils.GlobalSetUp();
        Case c = new Case();
        insert c;

        // Test the controller for the Live_Agent_Post_Chat visualforce page    
        // Create a JSON string with chatdetails
        String cd = '{' + '"userid":"005x0000001JGgr",' + '"agentName":"Emily W"' + '}';
        String ck = '12345'; // Create a chat key 
        String t = 'Test Chat Transcript'; // Create a transcript
        
        PageReference pageRef = Page.Live_Agent_Post_Chat;
        pageRef.getParameters().put('transcript', t);
        pageRef.getParameters().put('chatDetails', cd);
        pageRef.getParameters().put('chatkey', ck);

        Test.setCurrentPageReference(pageRef);

        Test.startTest();
        LiveAgentTranscriptCtrl latc = new LiveAgentTranscriptCtrl();
        latc.saveTranscript();
        // Check that the transcript record is created
        List<Live_Agent_Transcript__c> lstLat = [SELECT Id, Transcript__c FROM Live_Agent_Transcript__c LIMIT 1];
        system.assert(lstLat.size() == 1);
        system.assert(lstLat[0].Transcript__c.contains(t));

        // Test the controller for the Live_Agent_Transcript visualforce page      
        PageReference pageRef2 = Page.Live_Agent_Transcript;
        pageRef2.getParameters().put('chatkey', ck);
        
        Test.setCurrentPageReference(pageRef2);
        
        latc = new LiveAgentTranscriptCtrl();
        latc.saveTranscript(); // this will retrieve the chat transcript record based on the chatkey
        
        // Check that the transcript record is accessible
        system.assert(latc.lat.Transcript__c.contains(t));

        pageRef2 = Page.Live_Agent_Transcript;
        pageRef2.getParameters().put('chatkey', ck);
        pageRef2.getParameters().put('attachedRecords', '{"CaseId":"'+c.Id+'"');
        
        Test.setCurrentPageReference(pageRef2);
        
        latc = new LiveAgentTranscriptCtrl();
        
        Test.stopTest();

        QTCustomSettingsHier__c qtCustomSettingsHier = QTCustomSettingsHier__c.getInstance();
        System.assertEquals(isSandbox? qtCustomSettingsHier.Close_Chat_Survey_Id_QA__c : qtCustomSettingsHier.Close_Chat_Survey_Id__c,
                latc.surveyId);
    }
}