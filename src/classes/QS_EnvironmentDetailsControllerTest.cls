/**
* Change Log: 
*
* 2018-11-01    ext_vos INC0148657: add test fot filtered Licenses.
* 2019-05-05    AIN IT-1597 Updated for Support Portal Redesign
* 17.02.2020  ext_bad IT-2497   Added Legacy Enterprise Support Entitlement Process
*/
@isTest
public class QS_EnvironmentDetailsControllerTest {
    private static string environmentId {get; set;}
    private static EnvironmentProduct__c environmentProduct {get; set;}
    private static Id networkId;
    private static Id recTypeId;
    public static testMethod void TestWithEnvironment() {
        InsertTestData();
        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', environmentId);
        QS_EnvironmentDetailsController controller = new QS_EnvironmentDetailsController();
        controller.GetEnvironmentProducts();
        system.debug('environmentProduct: ' + environmentProduct);
        controller.selectedProductId = environmentProduct.Id;
        controller.SaveEnvironment();
        controller.EditEnvironment();
        controller.CancelEnvironment();
        controller.redirectToListPage();
        controller.EditProduct();
        controller.SaveProduct();
        controller.CancelProduct();
        controller.DeleteProduct();
    }
    
    public static testMethod void TestWithEnvironmentAndLicenses() {
        InsertTestDataWithLicenses();

        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', environmentId);
        QS_EnvironmentDetailsController controller = new QS_EnvironmentDetailsController();
        controller.selectedProductId = environmentProduct.Id;
        controller.EditEnvironment();
        // 100 licenses + none value
        System.assertEquals(101, controller.LicenseOptionListFiltered.size());
        controller.CancelEnvironment();

        // create more Entitlement to exceed the limit
        List<Account_License__c> accLicenses = [select Id from Account_License__c where Name = '11InsertTestDataWithLicenses' limit 1];
        List<SlaProcess> slaProcesses = [select Id from SlaProcess where isActive = true limit 1];

        Entitlement ent = new Entitlement();
        ent.Name = 'Product License_newone_ent';
        ent.AccountId = controller.environment.Account__c;
        ent.Account_license__c = accLicenses[0].Id;
        ent.SlaProcessid = slaProcesses[0].Id;
        ent.StartDate = Date.today().addDays(-15);
        ent.EndDate = Date.today().addDays(15);
        insert ent;

        controller.EditEnvironment();
        // limit is exceeded -> 2 options: exists license + message about limit
        System.assertEquals(2, controller.LicenseOptionListFiltered.size()); 

        controller.refreshLicenseList();
        // limit is exceeded -> after filter without value -> 1 option (message about limit)
        System.assertEquals(1, controller.LicenseOptionListFiltered.size());
        System.assert(controller.LicenseOptionListFiltered[0].getLabel().contains('Too many licenses'));

        // limit is exceeded -> after filter with incorrect value -> 1 option (message about no suitable licenses)
        controller.licenseFilterText = 'teeeeeeeeeest';
        controller.refreshLicenseList();
        System.assertEquals(1, controller.LicenseOptionListFiltered.size());
        System.assert(controller.LicenseOptionListFiltered[0].getLabel().contains('No licenses found'));
    }

    public static testMethod void TestWithoutEnvironment() {
        InsertTestData();
        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPage(pageRef);
        QS_EnvironmentDetailsController controller = new QS_EnvironmentDetailsController();
        controller.SaveEnvironment();
        controller.GetEnvironmentProducts();
    }
    /*public static testMethod void TestEnvironmentNotFound() {
        InsertTestData('environmentName');
        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPage(pageRef);
        Environment__c environ = new Environment__c();
        QS_EnvironmentDetailsController controller = new QS_EnvironmentDetailsController();
        controller.SaveEnvironment();
    }*/
    public static testMethod void TestWithBlankEnvironment() {
        InsertTestData();
        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', environmentId);
        QS_EnvironmentDetailsController controller = new QS_EnvironmentDetailsController();
        controller.environment.Name = '';
        controller.SaveEnvironment();
        controller.GetEnvironmentProducts();
    }

    private static void InsertTestDataWithLicenses() {
        InsertTestData();
        List<SlaProcess> slaProcesses = [select Id from SlaProcess where isActive = true limit 1];
        List<Environment__c> env = [select Id, Account__c from Environment__c where Name = 'environmentName' limit 1];
        
        Entitlement_Settings__c settings4 = new Entitlement_Settings__c();
        settings4.X24x7_Business_Hours_Name__c = '24x7';
        settings4.Premium_Entitlement_Process_Name__c = 'Legacy Enterprise Support SLA';
        settings4.Enterprise_Entitlement_Process_Name__c = 'Enterprise Support SLA';
        settings4.Premium_Support_Level__c = 'Enterprise';
        settings4.Standard_Entitlement_Process_Name__c = 'Basic Support SLA';
        settings4.Standard_Support_Level__c = 'Basic';
        settings4.Signature_Entitlement_Process_Name__c = 'Signature Support SLA';
        settings4.Hold_Account_Name__c = 'UnMatched License Hold Account';
        settings4.Email_Message_Phrase_Communication__c = 'CUSTOMER COMMUNICATION';
        settings4.Email_Message_Phrase_First_Response__c = 'FIRST RESPONSE';
        insert settings4;

        Account_License__c accLic = new Account_License__c();
        accLic.Name = '11InsertTestDataWithLicenses';
        accLic.Account__c = env[0].Account__c;
        accLic.Support_From__c = Date.today().addDays(-15);
        accLic.Support_To__c = Date.today().addDays(15);
        acclic.INT_NetSuite_Support_Level__c = 'Basic';
        insert accLic;

        List<Entitlement> entitlements = new List<Entitlement>();
        for(Integer i = 0; i < 98; i++) {
            Entitlement ent = new Entitlement();
            ent.Name = 'Product License_' +  String.valueOf(i) + 'ent';
            ent.AccountId = env[0].Account__c;
            ent.Account_license__c = accLic.Id;
            ent.SlaProcessid = slaProcesses[0].Id;
            ent.StartDate = Date.today().addDays(-15);
            ent.EndDate = Date.today().addDays(15);
            entitlements.add(ent);
        }
        insert entitlements;
    }

    private static User InsertTestData()
    {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.Navision_Status__c = 'Customer';
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.LeadSource = 'leadSource';
        insert testContact;

        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        environmentId = environment.Id;

        environmentProduct = new EnvironmentProduct__c();
        environmentProduct.Name = 'test';
        environmentProduct.Environment__c = environment.Id;
        insert environmentProduct;

        Bugs__c bug = new Bugs__c();
        bug.Status__c ='Status 1';
        Insert bug;

        bug = [select Status__c from Bugs__c where id = :bug.id];
        System.debug('Bug.Status: ' + bug.Status__c);
        bug.Status__c = 'Status 2';
        update bug;

        bug = [select Status__c from Bugs__c where id = :bug.id];
        System.debug('Bug.Status: ' + bug.Status__c);

        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        caseObj.Bug__c = bug.Id;
        //caseObj.Status = QS_Controller.STATUS_SOLUTION_SUGGESTED;

        insert caseObj;

        return communityUser;
    }
    private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
                User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];  
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}