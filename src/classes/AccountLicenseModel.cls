public with sharing class AccountLicenseModel 
{
    //Constructor
    public AccountLicenseModel(Account_License__c al)
    {
        record = al; 
        selected = false;
    }
    
    //Properties
    public Account_License__c record
    {
        get
        {
            if(record == null)
                record = new Account_License__c();
            return record;
        }
        set;
    }
    
    public Boolean selected {get; set;}
}