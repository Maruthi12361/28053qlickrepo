/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_PopupTemplateController {
	public string CSSBaseURL {get;set;}
	public QS_PopupTemplateController() {
		QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        CSSBaseURL = partnerPortalURLs.Support_Portal_CSS_Base__c;
	}
}