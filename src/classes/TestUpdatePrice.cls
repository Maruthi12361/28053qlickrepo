@isTest

/***************************************************************************************************************************************

    Changelog:
        2012-02-16  CCE     Added a test Account to set QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
        2016-06-09  roman@4front    test_SetListPrice method added     
        2017-01-04  Pramod Kumar V  Test class coverage
		2020-04-14  EXT_CDX Updating Opp's record type. Direct / Reseller - Std Sales Process was deactivated - OTQ-480      
****************************************************************************************************************************************/

private class TestUpdatePrice 
{
    static testMethod void test_UpdatePrice()
    {
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
                
        //Pricebook2 s = [select id from Pricebook2 where IsStandard = true limit 1];  
         
        // create the product
        /*Product2 p1 = new Product2(
            name='Test Product 1',
            IsActive=true,
            Description='My Product',
            CurrencyIsoCode = 'GBP',
            ProductCode='12345',
             Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
             Income_Account__c = '26000 Deferred Revenue'
        );
        insert p1;       
      
        Product2 p2 = new Product2(
            name='Test Product 2',
            IsActive=true,
            Description='My Product2',
            CurrencyIsoCode = 'GBP',
            ProductCode='12345',
             Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
             Income_Account__c = '26000 Deferred Revenue'
        );
        insert p2; 
        // create the pricebookentry
        PricebookEntry pbe1 = new PricebookEntry(
            Pricebook2Id= s.Id,
            Product2Id= p1.id,
            UnitPrice= 900.00,
            IsActive=true,
            UseStandardPrice=false,
            CurrencyIsoCode = 'GBP'
        ); 
        insert pbe1;*/

        PricebookEntry pbe1 = [select id from PricebookEntry limit 1];

        
        //CCE  Adding an account with BillingCountryCode and Qliktech Company
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
            Country_Name__c = 'United Kingdom',
            Subsidiary__c = testSubs1.id                        
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
            
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;
            
        // create the opportunity
        Test.startTest();
        Opportunity opp1 = new Opportunity(
            name='Test Opp 1',
            recordtypeid='012D0000000khWZ',
            StageName = 'Marketing Qualified',
            CurrencyIsoCode = 'GBP',
            CloseDate = Date.newInstance(2009,01,01), 
            Signature_Type__c = 'Digital Signature',       
            AccountId = acc.Id          
        );
        insert opp1;
        
        // add the line item
        List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.Quantity = 1;
        oli.UnitPrice = 1000;
        oli.PricebookEntryId = pbe1.id;
        oli.OpportunityId = opp1.id; 
        oli.Partner_Margin__c = 40;
        oli.Discounted_Price__c = 900;
        oppList.add(oli);
     
        
        OpportunityLineItem oli2 = new OpportunityLineItem();
        oli2.Quantity = 1;
        oli2.UnitPrice = 2000;
        oli2.PricebookEntryId = pbe1.id;
        oli2.OpportunityId = opp1.id;
        oppList.add(oli2); 

        
        OpportunityLineItem oli3 = new OpportunityLineItem(Partner_Margin__c = null);
        oli3.Quantity = 1;
        oli3.UnitPrice = 3000;
        oli3.PricebookEntryId = pbe1.id;
        oli3.OpportunityId = opp1.id;
        oli3.Discount__c = 10; 
        oppList.add(oli3);
        insert oppList;  
        Test.stopTest();
    }

    static testMethod void test_SetListPrice()
    {
        Test.startTest();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
                
        // create the product
        /*Product2 p1 = new Product2(
            name='Test Product 1',
            IsActive=true,
            Description='My Product',
            CurrencyIsoCode = 'GBP',
            ProductCode='12345',
             Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
             Income_Account__c = '26000 Deferred Revenue'
        );
        insert p1;       
      
        Product2 p2 = new Product2(
            name='Test Product 2',
            IsActive=true,
            Description='My Product2',
            CurrencyIsoCode = 'GBP',
            ProductCode='12345', 
            Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
            Income_Account__c = '26000 Deferred Revenue'
        );
        insert p2; 
        // create the pricebookentry
        PricebookEntry pbe1 = new PricebookEntry(
            Pricebook2Id= s.Id,
            Product2Id= p1.id,
            UnitPrice= 900.00,
            IsActive=true,
            UseStandardPrice=false,
            CurrencyIsoCode = 'GBP'
        ); 
        insert pbe1;*/
        PricebookEntry pbe1 = [select id from PricebookEntry limit 1];

        //CCE  Adding an account with BillingCountryCode and Qliktech Company
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
            
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        // create the opportunity
        Opportunity opp1 = new Opportunity(
            name='Test Opp 1',
            recordtypeid='012D0000000khWZ',
            StageName = 'Marketing Qualified',
            CurrencyIsoCode = 'GBP',
            CloseDate = Date.newInstance(2009,01,01), 
            Signature_Type__c = 'Digital Signature',       
            AccountId = acc.Id          
        );
        insert opp1;
        
        // add the line item
        List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.Quantity = 1;
        oli.UnitPrice = 1000;
        oli.PricebookEntryId = pbe1.id;
        oli.OpportunityId = opp1.id; 
        oli.Partner_Margin__c = 40;
        oli.Discounted_Price__c = 900;
        oli.CPQ_List_Price__c = 50;
        oppList.add(oli);
        insert oli;
        
        OpportunityLineItem oli2 = new OpportunityLineItem();
        oli2.Quantity = 1;
        oli2.UnitPrice = 2000;
        oli2.PricebookEntryId = pbe1.id;
        oli2.OpportunityId = opp1.id;
        oppList.add(oli2); 
        insert oli2;
        
        OpportunityLineItem oli3 = new OpportunityLineItem();
        oli3.Quantity = 1;
        oli3.UnitPrice = 3000;
        oli3.PricebookEntryId = pbe1.id;
        oli3.OpportunityId = opp1.id;
        oli3.Discount__c = 10;
        oli3.CPQ_List_Price__c = -25;
        oppList.add(oli3);
        //insert oli3;
        Test.stopTest();
        System.assertNotEquals(null, oli2.Id);
        //insert oppList; 
        OpportunityLineItem tempOli = [Select Id, CPQ_List_Price__c From OpportunityLineItem Where Id =: oli2.Id];
        System.assertEquals(null, tempOli.CPQ_List_Price__c); 
        Boolean isInsert=true;
        List<OpportunityLineItem> triggerNew=new List<OpportunityLineItem>();
        triggerNew.add(oli);
        UpdatePriceHandler.handle(triggerNew,isInsert);

    }
}