//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//			
// Change History
// --------------
//	2013-01-08		SAN		Adding changes for partner paying by invoice						
//*********************************************************/
public class efRegistrationController{
    public Boolean isTest=false;
    private User user;
    private efRegistrationManager regManager;
    private efBillingManager billingManager;    
    private efOpportunityManager oppManager;
    private efAppNavigator navigator;
    private efRegistrationValidator regValidator;
   	private efTrainingUtil trainingUtil; // Comment & : utility classes handle specific functional aspects.
   	private efPromoCodeUtil promoUtil;
    private efProductUtil prodUtil;
    private String eventBannerUrl;        
    private boolean displayThanx = false;        
    private Map<String,String> configMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);                 
    private boolean chkTermsAccepted = false;
    private boolean paymentTermsAccepted = false;   
    private boolean invalidPaymentUrl = false;
    private boolean validatedOnRegisterPage = false;    
    private boolean validatedOnConfirmPage = false;
    private boolean validatedOnTrainingPage = false;
    private String message='';
    private efProductWrapper[] pis;
    private String formInEdition = '';
    //private String paymentOption = 'Pay by Credit Card';
    
    
    // Initialisation.
    
    public efRegistrationController(){        
        if(user==null){
            try{
                initialize();
            } catch(Exception e){
                efUtility.updateDebugLog('Error in loading Portal User'+UserInfo.getUserId(),'E1009','Error');
            }
        }
    }

    public void initialize(){
    	// Comment & : validate input registration Id.
		if (checkInitialState()!=null) return;
    	String registrationId = ApexPages.currentPage().getParameters().get('registrationId'); 
    	string contactId;
    	if (!ApexPages.currentPage().getParameters().containsKey('contactId')) {
    		contactId = efUtility.getContactIdFromRegistration(registrationId);
    	} else {   
    		contactId = ApexPages.currentPage().getParameters().get('contactId');
    	}
    	// Comment& : efRegistrationManager creates and holds 
    	// efRegistrationWrapper, efContactWrapper instances.     	
        regManager = new efRegistrationManager(contactId, registrationId);        
        
        // Comment& : efAppNavigator encapsulates page navigation logic.
        navigator = new efAppNavigator(regManager.getEventWrapper(), registrationId, (regManager.getAttendeeType()=='Employee'));
        navigator.setCurrentPage(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE);
        
        // Comment& : efRegistrationValidator encapsulated data validation logic.
        regValidator = new efRegistrationValidator(regManager);
        
      	trainingUtil = new efTrainingUtil(regManager);
      	prodUtil = new efProductUtil(regManager);
      	promoUtil = new efPromoCodeUtil(regManager); 
        
        oppManager = new efOpportunityManager(regManager);            

        billingManager = new efBillingManager(getRegistrationProducts(), 
        										getTrainingOfInterest(), 
        										promoUtil.getPromoCodeListObject(),
        										getAddOnsOfInterest());   
        billingManager.generateBill();
        
        paymentOption = 'Pay by Credit Card';
    }

	// & in use
	public boolean getDisplayThanx(){ 
		// Comment& : called from finish page. 
        return this.displayThanx;
    }

	// & in use
    public pageReference checkStatusForTransition(){
    	System.debug('&&&& : '+ApexPages.currentPage().getUrl());
    	System.debug('&&&& : '+UserInfo.getUserName());
    	// Comment& : called as the action method from finish page. 
   		try{ 
   			
   			String regId = ApexPages.currentPage().getParameters().get('registrationId');
    		if (regId==null || regId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    		String contactId;	
    		if (ApexPages.currentPage().getParameters().containsKey('contactId')) {
    				contactId = ApexPages.currentPage().getParameters().get('contactId');
    		} else {
					contactId = efUtility.getContactIdFromRegistration(regId);    			
    		}
			if (contactId == null || contactId.length()==0) {
    			 return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    		}    		
    		    
    		System.debug('&&&& : '+regId);		
    		PageReference pr = efRegistrationManager.checkRegistrationIdPortal(regId);
   			
			if (pr!=null) return pr;
			
			regManager = new efRegistrationManager(contactId, regId);
	        Payment_Details__c[] currentPayment = [Select p.Return_Code__c, 
            										p.Id, 
                                                    p.CreatedDate,
                                                    p.RegistrationId__c
                                                    From Payment_Details__c p 
                                                    Where  p.RegistrationId__c=:regId
                                                    Order By p.CreatedDate Desc Limit 1];	
          	if(currentPayment.size()>0 && currentPayment[0].Return_Code__c == '1'){
    			System.debug('&&&& :  display thankx');		
				displayThanx = true;
           	}        
        } catch(Exception e){
            efUtility.updateDebugLog('Login to Registration Wizard failed '+e.getMessage(),'E1011','Error');      
        }    
        return null;   
    }
    
    // & in use
    public PageReference checkInitialState(){
    	try {
    		String regId = ApexPages.currentPage().getParameters().get('registrationId');
    		if (regId==null || regId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
     		String contactId;
     		if (ApexPages.currentPage().getParameters().containsKey('contactId')) {
     			contactId = ApexPages.currentPage().getParameters().get('contactId');
     		} else {
     			contactId = efUtility.getContactIdFromRegistration(regId);
     		}
    		if (contactId==null || contactId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);    		   		    		
    		return efRegistrationManager.checkRegistrationId(regId);
    	} catch (Exception e){
    		return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    	}
    }    
    
    public PageReference checkState(){
        if (regManager==null) // Comment & : no registration manager instance = invalid state.
        	return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
        else
        	return null;
    }
    
    // & in use
    public PageReference checkStatus(){
        try{
            regManager.refreshRegStatus();
            return navigator.resolveFromRegistration(regManager);            
        }
        catch(Exception e){
            efUtility.updateDebugLog('Login to Registration Wizard failed '+e.getMessage(),'E1011','Error');
            return null;
        }
        return null;
    }

	
	// Navigation methods. all & in use	
    public PageReference goTraining(){        
        return goPage(efAppNavigator.REGISTRATION_TRAINING_PAGE);                       
    }
           
    public PageReference goAttendeeInfo(){ 
        return goPage(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE);
    }
    
   	public PageReference goSummary(){
		PageReference pr = goPage(efAppNavigator.REGISTRATION_SUMMARY_PAGE);
		promoUtil.reInitialise(); // Comment & : Flush old promo codes.
		initialiseBilling();  // Comment & : Recalc the bill.
		return pr;
    }

    public PageReference goNext(){
    	regManager.refreshRegStatus();        
        if (regManager.getIsRegisteredUser()) return goAttendeePortal(); 
        
        // Comment& : check addons.
        if (navigator.getCurrentPage()==efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE){
        	checkAddOns();
        }
        
        PageReference pr = validateFormData(navigator.getCurrentPage());     
       	if (pr!=null) return pr; // Comment& : validation failed, stay on the page.
        
        goSaveForLater(); // Comment& : use to persist state, but ignore return val.        
        pr = navigator.getNextPageReference(navigator.getCurrentPage());
        
        if (navigator.getCurrentPage()==efAppNavigator.REGISTRATION_SUMMARY_PAGE){
        	promoUtil.reInitialise(); // Comment & : Flush old promo codes.
			initialiseBilling();  // Comment & : Recalc the bill.        	
        }
        return pr;
    }
    
    public PageReference goBack(){
    	regManager.refreshRegStatus();        
        if (regManager.getIsRegisteredUser()) return goAttendeePortal();        
        
        PageReference pr = validateFormData(navigator.getCurrentPage());     
       	if (pr!=null) return pr; // Comment& : validation failed, stay on the page.
        
        goSaveForLater(); // Comment& : use to persist state, but ignore return val.        
        pr = navigator.getBackPageReference(navigator.getCurrentPage());                      
        return pr;
    }
    
    private PageReference goPage(String pageName){
    	regManager.refreshRegStatus();        
        if (regManager.getIsRegisteredUser()) return goAttendeePortal();        
        
        PageReference pr = validateFormData(navigator.getCurrentPage());     
       	if (pr!=null) return pr; // Comment& : validation failed, stay on the page.
       	
       	// Comment& : check addons.
        if (navigator.getCurrentPage()==efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE){
        	checkAddOns();
        }
        
        goSaveForLater(); // Comment& : use to persist state, but ignore return val.        
        pr = navigator.getPageReference(pageName);                      
        return pr;
    }
    
    private void checkAddOns(){    	
		if (regManager.getEventWrapper().getExtendedStayProductId()!=null || regManager.getEventWrapper().getSpouseOfferProductId()!=null){    			
			efProductWrapper p = prodUtil.getAddOnProductById(regManager.getEventWrapper().getExtendedStayProductId());
			efProductWrapper p2 = prodUtil.getAddOnProductById(regManager.getEventWrapper().getSpouseOfferProductId());
			if (p!=null){  	
				p.setIsSelected(regManager.getRegistrationWrapper().getExtendedStay());
			}
			if (p2!=null){
				p2.setIsSelected(regManager.getRegistrationWrapper().getSpouseAddition());
			}			
			initialiseBilling();
			oppManager.cancelOldOpportunityAndLineItems(); // Comment& : just in case.
		}
    }
          
    public PageReference goFinish(){
    	invalidPaymentUrl = false;
    	chkTermsAccepted = false;
        if(!getPaymentTermsAccepted()){
            chkTermsAccepted = true;
            return null;
        }   
    	  
    	PageReference pr;

    	regManager.refreshRegStatus();        
        if (regManager.getIsRegisteredUser()) return goAttendeePortal();        
                            
        if (formInEdition != '') { cancelConfirm(); } // Comment & : cancel inline editing.
                
        resetValidation();
        this.setPaymentTermsAccepted(true);

        if (!promoUtil.validatePromoCodes()){
            regenerateBilling();
            return null;
        }

        if (!trainingUtil.validateTrainingInventory()){
            promoUtil.removeEducationPromoCode();
            regenerateBilling();
            return null;
        }
        
        if(getShowPaymentPage()){            
            //String paymentUrl = getPaymentURL(regManager.getRegistrationWrapper().getId());
            System.debug('&&&& : '+regManager.getRegistrationWrapper().getRegistration());
            System.debug('&&&& : '+regManager.getRegistrationWrapper().getPaymentDetails());
            String paymentUrl = getPaymentURL(regManager.getRegistrationWrapper().getPaymentDetails().Id);
            
            if (paymentUrl==null || paymentUrl.length()==0){
            	invalidPaymentUrl = true;
            	return null;
            } else {             	
            	//paymentUrl = EncodingUtil.urlDecode(paymentURL, 'utf-8');
            	invalidPaymentUrl = false; 
            }
                
            pr = new PageReference(paymentUrl);
            pr.setRedirect(true);
        } else{
        	if(paymentOption == 'Pay by Invoice')
        	{
        		regManager.getRegistrationWrapper().setStatus(efConstants.REG_STATUS_INVOICED);
		        oppManager.cancelOldOpportunityAndLineItems();
		        oppManager.createRegistrationOpportunity();
        		
        		
	        	// Comment & : no payment due.
	            oppManager.createRegOLI(getRegistrationProducts(),
	                                    promoUtil.getPromoCodeListObject(),
	                                    false);
	                                    
	            if (getAddOnsOfInterest().size()>0){
	                oppManager.createRegOLI(getAddOnsOfInterest(), null, false);
	            }
	            
	            if(getIsTrainingSelected() && navigator.getEventHasTraining()){
	            	oppManager.createEduOLI(getTrainingOfInterest(), 
	            							promoUtil,
	            							false);
	            }       
            
	                               
	            promoUtil.updatePromoProducts();
        		
	                                               
	            //& update the pending -1 record to paid at zero amount.
	            updateDummyPaymentDetails();
	            //Create Zero Dollar Payment Details Record 
	            //createDummyPaymentDetails();
	            
	            goFinal();
	        

	            
	            pr = navigator.getPageReference(efAppNavigator.REGISTRATION_FINISH_PAGE);
	            pr.getParameters().put('registrationId', regManager.getRegistrationWrapper().getId());            
	            pr.setRedirect(true);   
	            
	            return pr;     		
        		
        	}
        	else
        	{
        		regManager.getRegistrationWrapper().setStatus(efConstants.REG_STATUS_REGISTERED);
        	}
        }

        oppManager.cancelOldOpportunityAndLineItems();
        oppManager.createRegistrationOpportunity();
        
        // CR& : all OLI on a single Opp.
        /*if (getIsTrainingSelected() && navigator.getEventHasTraining()){
            oppManager.createEducationOpportunity();
        }*/
        
        if (!getShowPaymentPage()){
        	// Comment & : no payment due.
            oppManager.createRegOLI(getRegistrationProducts(),
                                    promoUtil.getPromoCodeListObject(),
                                    false);
                                    
            if (getAddOnsOfInterest().size()>0){
                oppManager.createRegOLI(getAddOnsOfInterest(), null, false);
            }
            
            if(getIsTrainingSelected() && navigator.getEventHasTraining()){
            	oppManager.createEduOLI(getTrainingOfInterest(), 
            							promoUtil,
            							false);
            }                            
            promoUtil.updatePromoProducts();
                                               
            //& update the pending -1 record to paid at zero amount.
            updateDummyPaymentDetails();
            //Create Zero Dollar Payment Details Record 
            //createDummyPaymentDetails();
            
            goFinal();
            
            pr = navigator.getPageReference(efAppNavigator.REGISTRATION_FINISH_PAGE);
            pr.getParameters().put('registrationId', regManager.getRegistrationWrapper().getId());            
            pr.setRedirect(true);
        } else{
        	// Comment & : payment due via RBS so setup pending Opp/OLI.
            oppManager.createRegOLI(getRegistrationProducts(),
                                    promoUtil.getPromoCodeListObject(),
                                    true);

            if (getAddOnsOfInterest().size()>0){
                oppManager.createRegOLI(getAddOnsOfInterest(), null, true);
            }
            
            if(getIsTrainingSelected() && navigator.getEventHasTraining()){
                oppManager.createEduOLI(getTrainingOfInterest(),
                						promoUtil,
                						true);
            }
            promoUtil.updatePromoProducts();
        }    
        return pr;
    }
    
    public PageReference goAttendeePortal(){
    	return navigator.returnAttendeePortalPageReference(regManager);	
    }
    
    public PageReference cancelConfirm(){
    	// Comment & : called from cancel button on in-place edit on summary page.
        formInEdition = '';
        validatedOnRegisterPage = false;
        regManager.reloadRegistration(); // Comment & : overwrite changes by reloading the registration object.
        return null;
    }
    
    public PageReference saveConfirm(){
    	// Comment & : called from save button on in-place edit on summary page.
        if (!isValidRegisterData())
            return null;
        
        //setPromoCodeExecuted(false);
        navigator.setCurrentPage(efAppNavigator.REGISTRATION_SUMMARY_PAGE);
        formInEdition = '';
        validatedOnRegisterPage = false;
        goSaveForLater();
        return null;
    }
    
    public PageReference goSaveForLater(){
        regManager.getRegistrationWrapper().setStatus(efConstants.REG_STATUS_SAVED_FOR_LATER);
        
        goFinal(); // Comment& : use to persist the registration data but ignore the return val.
        PageReference p = new PageReference('/secur/logout.jsp');
        return p;
    }
    
    public PageReference goFinal(){
        try{
        	regManager.getRegistrationWrapper().setWizardPageSaved(navigator.getCurrentPage());
            regManager.flushRegistration();
        } catch(Exception e){
            efUtility.updateDebugLog('Error in Final Stage of Registration Process :: '+e.getMessage(),'E1020','Error');
        }
        
        return navigator.returnPageReference(efAppNavigator.REGISTRATION_FINISH_PAGE);
   	}
	// End navigation methods.


    // Helper methods.
	private String getPaymentURL(String orderCode){
    	efQTPaymentGateway.ServiceSoap stub = new efQTPaymentGateway.ServiceSoap();    	
    	stub.timeout_x = 60000;
        
        efQTPaymentGateway.Order ord = new efQTPaymentGateway.Order(); 
        
        Registration__c registration = regManager.getRegistrationWrapper().getRegistration();
        
        ord.OrderCode = orderCode;
        ord.Description = registration.Name;
        ord.CurrencyCode = 'USD'; // Comment& : need to address this for multi-currency.        
        ord.Vat = 0;
        ord.TotalAmount = Double.valueOf(getTotalPaymentAmount());
        ord.Phone = registration.Mobile_Phone__c;
        
        Contact registrant = regManager.getContactWrapper().getContact();
        ord.Email = registrant.Email;
        ord.Company = getBusinessName();
        ord.FirstName = registrant.FirstName;
        ord.LastName = registrant.LastName;
        ord.Street = registrant.mailingStreet;      
        ord.HouseNbr = parseHouseNumber(registrant.mailingStreet);                
        ord.Zip = registrant.mailingPostalCode;
        ord.City = registrant.mailingCity;
        ord.Country = registrant.mailingCountry;                
                      
        ord.CaptureType = 'EventForce';
        ord.Status = 'New';        
        ord.QlikTechCompany = 'QlikTech UK Ltd';        
        ord.CreatedDate = DateTime.now();
        ord.LastModificationDate = DateTime.now();
        ord.SuccessURL = '';
        ord.FailureURL = '';
        
        String s = isTest ? 'http://test' : stub.GetPaymentURL(ord);
        System.debug('&&&& url : '+s);
        return s;
    }	
    
    private String parseHouseNumber(String streetString){
    	try{    
			for (String s : streetString.split(' ')){
				if (Pattern.matches('^[0-9]+$', s)){
					return s;
				}
			}
    	} catch (Exception ex){
    		return '1';
    	}
    	return '1';	
    }
    
    /*private void createDummyPaymentDetails(){
        Payment_Details__c p= new  Payment_Details__c(Amount__c = 0.0,
                                                      Transaction_Date__c=system.today(),
                                                      Return_Code__c ='1', //For Success
                                                      CurrencyIsoCode='USD',
                                                      RegistrationId__c =  regManager.getRegistrationWrapper().getRegistration().Id);
        insert p;
    }*/   
    
    private void updateDummyPaymentDetails(){
        Payment_Details__c p = new  Payment_Details__c(Id=regManager.getRegistrationWrapper().getPaymentDetails().Id);
        p.Return_Code__c = '1';
        update p;
    }    
    
    // End helper methods.
   
 
    // Validation methods.
    
    public boolean getValidatedOnTrainingPage(){
        return this.validatedOnTrainingPage;
    }

    public boolean getValidatedOnRegisterPage(){ 
        return this.validatedOnRegisterPage;
    }
    
    public Boolean getValidatedOnConfirmPage(){
        return validatedOnConfirmPage;
    }
    
    public void resetValidation(){
        message = '';
        validatedOnRegisterPage = false;
        validatedOnTrainingPage = false;
        validatedOnConfirmPage = false;
    }   

    public String getMessage(){
        return message;
    }
    
    public Boolean getSelectedTrainingInvalidated(){
    	if (trainingUtil==null)
    		return false;
    	else
    		return trainingUtil.selectedTrainingInvalidated;     	
    }
    
    public efRegistrationValidator getRegValidator(){
        return regValidator;
    }
    
    private Boolean isValidConfirmData(){
        return true;
    }
    
	public Boolean isValidTrainingData(){
        efProductWrapper[] selectedTraining = getTrainingOfInterest();
 
        message = '';
        
        // Comment & : check for overlapping classes.
        for (Integer i = 0; i < selectedTraining.size(); i++){
            for (Integer j = 0; j < selectedTraining.size(); j++){
                if (i == j)
                    continue;
                
                if (selectedTraining[i].getProduct().Family == selectedTraining[j].getProduct().Family){
                    if (!(selectedTraining[i].getStartDateTime() >= selectedTraining[j].getEndDateTime() ||
                        selectedTraining[i].getEndDateTime() <= selectedTraining[j].getStartDateTime())){
                        message = '\'' + selectedTraining[i].getName() + '\' and \'' + selectedTraining[j].getName() + '\' trainings overlap in the time. Please modify your selection by selecting items which do not overlap.';                     
                        return false;
                    }
                }
            }
        }
        validatedOnTrainingPage = false;    
        message = '';
        return true;
    }
    
    public boolean isValidRegisterData(){
        if (!regValidator.getIsValidRegistrationData()){
            validatedOnRegisterPage = true;
            return false;
        }
        else
            return true;
    }
    
    public Boolean getIsValidExtendedStay(){
    	return regValidator.getIsValidExtendedStay();
    }   
    
    public PageReference validateFormData(String pageName){
        if(!efUtility.isNull(pageName)){            
            if(pageName.equals(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE) && !isValidRegisterData()){   
                return navigator.returnPageReference(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE);
            } else if(pageName.equals(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE)){
                validatedOnRegisterPage=false;
            }
            
            if(pageName.equals(efAppNavigator.REGISTRATION_SUMMARY_PAGE) && !isValidConfirmData()){
                return navigator.returnPageReference(efAppNavigator.REGISTRATION_SUMMARY_PAGE);
            } else if(pageName.equals(efAppNavigator.REGISTRATION_SUMMARY_PAGE)){
                validatedonConfirmPage=false;
            }
            
            if (pageName.equals(efAppNavigator.REGISTRATION_TRAINING_PAGE) && !isValidTrainingData())
                return navigator.returnPageReference(efAppNavigator.REGISTRATION_TRAINING_PAGE);
        }
        return null;
    }

    public PageReference validateConfirm(){
        PageReference p = validateFormData(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE);
        if (p!=null){ validatedOnRegisterPage = true ;} return navigator.returnPageReference(efAppNavigator.REGISTRATION_SUMMARY_PAGE);
    }

    public Boolean getCanConfirmRegistration()
    {
    	// Comment& : training is valid.
      	Boolean canConfirm = !((getShowTrainingForAttendee() && !isValidTrainingData()));
		
		if (message.length()>0) return false;		

        return canConfirm;
    }
    
    public Boolean getInValidPaymentUrl(){
    	return invalidPaymentUrl;
    }    
    // End validation methods.


    // Attendee Info page methods.

    public String getEventBannerUrl(){    	
    	if (eventBannerUrl==null || eventBannerUrl.length()==0){
    		eventBannerUrl = getDocumentUrl(regManager.getEventWrapper().getEvent().Banner_Image_Document__c);
    	}
    	return eventBannerUrl;
    }
    
    private String getDocumentUrl(String documentUniqueName){
    	Document doc = [Select d.Id From Document d Where d.DeveloperName=:documentUniqueName Limit 1];
    	String imageid = doc.id; 
    	imageid = imageid.substring(0,15);
    	return '/servlet/servlet.FileDownload?file=' + imageid;
    }

    public String getEventName(){
    	return regManager.getEventWrapper().getEventName();
    }
    
    public String getRegistrationId(){
    	if (regManager==null)
    		return '';
        else
	        return regManager.getRegistrationWrapper().getId();
    }
            
    public efRegistrationWrapper getAttendeeRegistration(){
    	if (regManager==null)
    		return null;
        else
	        return regManager.getRegistrationWrapper();
    }
    
    public efEventWrapper getEventWrapper(){
    	if (regManager==null)
    		return null;
    	else
    		return regManager.getEventWrapper();
    }
    
    public efContactWrapper getAttendeeContact(){
    	if (regManager==null) 
    		return null;
    	else
        	return regManager.getContactWrapper();
    }
    
    public efEventWrapper getEvent(){
    	if (regManager==null)
    		return null;
    	else
        	return regManager.getEventWrapper();
    }
    
    public String getAttendeeType(){
        return regManager.getAttendeeType(); 
    }   

    public String getRegStatus(){
        return regManager.getRegistrationWrapper().getStatus();
    }

    public boolean isAttendeeType(string stype){   
        return efUtility.isAttendeeType(stype, regManager.getRegistrationWrapper().getRegistration());
    }
     
    public boolean getIsEmployee(){   
        return efUtility.isAttendeeType(efConstants.ATTENDEE_TYPE_EMPLOYEE,
                                      regManager.getRegistrationWrapper().getRegistration());
    }

    private Account getAccount(){
        return regManager.getAccount();
    }

    public String getCompanySize(){
        return efUtility.giveValue(regManager.getAccount().company_Size__c);
    }
    
    public void setCompanySize(String cs){
        regManager.getAccount().Company_Size__c = cs;
    }
    
    public String getIndustry(){
        return efUtility.giveValue(regManager.getAccount().Industry);
    }
    
    public void setIndustry(String i){
        regManager.getAccount().Industry = i;
    }

    public String getBusinessName(){
        return efUtility.giveValue(regManager.getAccount().Name);
    }
    
    public void setBusinessName(String bName){
        regManager.getAccount().Name = bName;
    }
	// End Attendee Info page methods.


    // Training page methods.
    
    public void setClassesByGroup(efClassWrapper[] c){ 
        trainingUtil.setClassesByGroup(c);
    }
    
    public List<efProductWrapper> getClasses(){
    	if (regManager==null)
    		return null;
        else 
        	return trainingUtil.getClasses();
    }
    
    public List<efClassWrapper> getClassesByGroup(){
    	if (regManager==null)
    		return null;
        else 
        	return trainingUtil.getClassesByGroup();
    }
    
    public List<efProductWrapper> getTrainingOfInterest(){
        List<efProductWrapper> trainingOfInterest = trainingUtil.getTrainingOfInterest();        
        return trainingOfInterest;
    }
       
    public boolean getIsTrainingSelected(){        
        return trainingUtil.getIsTrainingSelected();
    }
    
    // End Training page methods.
    
    
    // Payment related methods.

    public String getSubTotalPaymentAmount(){
    	// Comment & : called from summary page.
        return efUtility.formatDecimals(String.valueOf(billingManager.getSubTotalPaymentAmount()));
    }

    public String getTotalPaymentAmount(){
    	// Comment & : called from summary page.
        return efUtility.formatDecimals(String.valueOf(billingManager.getTotalPaymentAmount()));
    }

    public String getSummaryPaymentAmount(){
    	// Comment & : called from summary page.
        return efUtility.formatDecimals(String.valueOf(billingManager.getTotalPaymentAmount()));
    }

    public Boolean getPaymentTermsAccepted(){
    	// Comment & : called from summary page.        
        return this.paymentTermsAccepted;
    }
    
  	public Boolean getChkTermsAccepted(){
        return this.chkTermsAccepted;
    }

    public void setPaymentTermsAccepted(Boolean paymentTermsAccepted){
    	// Comment & : called from summary page.        
        this.paymentTermsAccepted = paymentTermsAccepted;
    }
        
    public efBillingManager getBillingManager(){
        return billingManager;
    }
    
    public void regenerateBilling(){
        initialiseBilling();
        regValidator = new efRegistrationValidator(regManager);
    }
    
    private void initialiseBilling(){
        billingManager.reset(getRegistrationProducts(),
                             getTrainingOfInterest(),
                             promoUtil.getPromoCodeListObject(),
                             getAddOnsOfInterest());                                                                         
        billingManager.generateBill();
    }
    // End payment related methods.


	// Promo code related methods.
	
    public String getPromoCode(){
    	// Comment & : called from summary page.
    	return promoUtil==null ? '' : promoUtil.getPromoCode(); 
    }
    
    public void setPromoCode(String promoCode){
    	// Comment & : called from summary page.    	 
    	if (promoCode!=null)
    		promoCode=promoCode.trim();    			
   			
        promoUtil.setPromoCode(promoCode);
    }
    
    public Boolean getAnyPromoCodeAdded(){
    	// Comment & : called from summary page.
    	return promoUtil==null ? false : promoUtil.getAnyPromoCodeAdded();
    }
    
    public String getPromoMessage(){
    	// Comment & : called from summary page.
    	return promoUtil==null ? '': promoUtil.getPromoMessage();        
    }
    
    public List<efProductWrapper> getPromoCodeList(){
        List<efProductWrapper> p = promoUtil.getPromoCodeList(billingmanager.getTotalRegistrationAmount(),
                                                       billingmanager.getTotalClassesAmount(),
                                                       getIsTrainingSelected());
        initialiseBilling();
        return p;
    }
    
    public List<efProductWrapper> getPromoCodes(){
        return promoUtil.getPromoCodeListObject();
    }    
    
    public PageReference updatePromoCodes(){
    	// Comment & : called from Add promo code button summary page.
        getPromoCodeList();
        return null;
    }
    
    public boolean getPromoCodeExecuted(){
        // TODO & : what is this?
        return promoUtil.getPromoCodeExecuted();
    }
    
    public void setPromoCodeExecuted(Boolean val){
        // TODO & : what is this?
        promoUtil.setPromoCodeExecuted(val);
    }
    
    public void removePromoCode(){
        String pid = System.currentPageReference().getParameters().get('pid');
        promoUtil.removePromoCode(pid);
        initialiseBilling();
    }
    // End promo code related methods.
    
    
    // Dynamic display methods.
       
    public boolean getShowTrainingForAttendee(){    		
    	if (navigator==null) return false;	
    	
    	return navigator.getEventHasTraining();
    }
    
    public boolean getShowPromoCode(){
    	return true;
    }
    
    public Boolean getIsInvoiced(){
		if(getRegStatus() == 'Invoiced')
    	{
    		return true;
    	}   
    	else
    		return false; 	
    }
        
    public Boolean getShowPaymentPage(){
    	// Comment & : is payment due?
    	if(paymentOption == 'Pay by Invoice')
    	{
    		return false;
    	}
    	else
    	{
	    	if (billingManager==null)
	    		return true;
	    	else	
	    		return (billingManager.getTotalPaymentAmount()>0);    
    	}    
    }
    
    
	// Product related methods.
	    
    private List<efProductWrapper> getRegistrationProducts(){
        return prodUtil.getRegistrationProducts();
    }
    
    public String getRegistrationProductName(){
        efProductWrapper[] reg = getRegistrationProducts();        
        if(reg!=null && reg.size()>0){
            return reg[0].getProduct().Name;     
        }        
        return '';  
    }

    public efOpportunityManager getOpportunityManager(){
        return oppManager;
    }
    
    public List<efProductWrapper> getAddOnsOfInterest(){
        List<efProductWrapper> addOnsOfInterest = prodUtil.getSelectedAddOns();
        
        return addOnsOfInterest;
    }
	// End product related methods.    
    
    
    // Event Label methods.
    
    // Comment & : reading the labels directly via the wrapper object doesn't 
   	//				work in this instance, to follow up.
    public String getRegistrationLabelTrainingHeader(){
    	return regManager.getEventWrapper().getRegistrationLabelTrainingHeader();
    }
    
    public String getRegistrationLabelTrainingSubHeader(){
    	return regManager.getEventWrapper().getRegistrationLabelTrainingSubHeader();
    }
    
    public String getRegistrationLabelSummaryFinancialSummary(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryFinancialSummary();
    }

    public String getRegistrationLabelSummaryPayment(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryPayment();
    }
    
    public String getRegistrationLabelSummaryGroupDiscounts(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryGroupDiscounts();
    }
    
    public String getRegistrationLabelSummaryGroup5Plus(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryGroup5Plus();
    }
    
    public String getRegistrationLabelSummaryGroup10Plus(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryGroup10Plus();
    }
                        
   	public String getRegistrationLabelSummaryMessageTrainingInvalidated(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryMessageTrainingInvalidated();
    }
    
   	public String getRegistrationLabelSummaryTermsAndConditions(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryTermsAndConditions();
    }
    
   	public String getRegistrationLabelSummaryPaymentRedirect(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryPaymentRedirect();
    }

   	public String getRegistrationLabelSummaryInvoice(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryInvoice();
    }
    
   	public String getRegistrationLabelSummaryInvoiceConf(){
    	return regManager.getEventWrapper().getRegistrationLabelSummaryInvoiceConf();
    }
            
    public String getRegistrationLabelFinishError(){
    	return regManager.getEventWrapper().getRegistrationLabelFinishError();
    }
    
   	public String getRegistrationLabelFinishGoEvent(){
    	return regManager.getEventWrapper().getRegistrationLabelFinishGoEvent();
    }
    
    public String getRegistrationLabelFinishPaymentConfirmed(){
    	return regManager.getEventWrapper().getRegistrationLabelFinishPaymentConfirmed();
    }
    
  	public String getRegistrationLabelFinishGoAttendeePortal(){
    	return regManager.getEventWrapper().getRegistrationLabelFinishGoAttendeePortal();
    }
    // End event label methods.
    
    
    // Other methods.
    // & in use
    public efPickLists getPicklist(){
        efPickLists p = new efPickLists();
        return p;
    }
    // & in use
    public String getFormInEdition(){
        return formInEdition;
    }
    // & in use
    public void editForm(){
        formInEdition = System.currentPageReference().getParameters().get('editForm');
    }
    
    public List<SelectOption> getExtendedStayArrivalList(){
    	return getPicklist().getEventExtendedStayArrivalList(regManager.getEventWrapper().getEvent().Extended_Stay_Start_Date__c, 
    															regManager.getEventWrapper().getEvent().Extended_Stay_End_Date__c);
    }
    
    public List<SelectOption> getExtendedStayDepartureList(){
    	return getPicklist().getEventExtendedStayDepartureList(regManager.getEventWrapper().getEvent().Extended_Stay_Start_Date__c,
    															regManager.getEventWrapper().getEvent().Extended_Stay_End_Date__c);
    }
        
    // End Other methods.
    
        public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Pay by Credit Card','Pay by Credit Card')); 
        options.add(new SelectOption('Pay by Invoice','Pay by Invoice')); 
        return options; 
    }
    
    public String paymentOption { get; set; }
	{
   		paymentOption = 'Pay by Credit Card';
	}

}