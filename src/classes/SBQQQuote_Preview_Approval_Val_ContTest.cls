/*************************************************
*
* 2017-06-21 AIN Test class for SBQQQuote_Preview_Approval_Val_Cont
* 2017-08-10 MTM commenting out test method as this is blocking QA deployment
* 2017-09-26 AIN Fixed test class for QCW-2381 deployment
*
*************************************************/
@isTest
private class SBQQQuote_Preview_Approval_Val_ContTest {

    @isTest static void successTest1() {

        QTTestUtils.GlobalSetUp();


        List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'System Administrator' LIMIT 1];
    
        if (plist.size() != 1) {
            throw new QTException('Profile does not exist: System Administrator');
        }
        Profile p = plist[0];
        Id profileId = p.Id;

        User user1 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user1.Username = 'CEO@CEO.com.AINTEST';
        user1.Email = 'CEO@CEO.com.AINTEST';

        User user2 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user2.Username = 'PU@PU.com.AINTEST';
        user2.Email = 'PU@PU.com.AINTEST';

        User user3 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user3.Username = 'NAM@NAM.com.AINTEST';
        user3.Email = 'NAM@NAM.com.AINTEST';

        User user4 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user4.Username = 'VP@VP.com.AINTEST';
        user4.Email = 'VP@VP.com.AINTEST';

        List<User> users = new List<User>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        insert users;

        user1.Approval_Manager__c = null;
        user1.Quote_Approval_Role__c = 'CEO';
        user2.Approval_Manager__c = user1.Id;
        user2.Quote_Approval_Role__c = 'PU';
        user3.Approval_Manager__c = user2.Id;
        user3.Quote_Approval_Role__c = 'NAM';
        user4.Approval_Manager__c = user3.Id;
        user4.Quote_Approval_Role__c = 'VP Education Services';

        update users;

        QlikTech_Company__c qtComp_FRA = QTTestUtils.createMockQTCompany('QlikTech France Pty Ltd', 'FRA', 'France','EMEA');
        qtComp_FRA.Education_Quote_Approver__c = user2.id;
        qtComp_FRA.Consulting_Quote_Approver__c = user1.id;
        update qtComp_FRA;

        Account acc;

        system.runas(user4) {

            acc = new Account();
            acc.Name = 'Test account';
            acc.QlikTech_Company__c = 'QlikTech France Pty Ltd';
            acc.Billing_Country_Code__c = qtComp_FRA.Id;
            acc.Territory_Country__c = 'France';
            insert acc;
        }

        Contact con = new Contact(firstName='RDZTestContact',
            lastName='RDZTestLastName',
            Email='RDZTest@test.com.sandbox',
            HasOptedOutOfEmail=false);
        con.AccountId = acc.id;
        insert con;

        List<Address__c> addresses = new List<Address__c> ();
        Address__c address = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Billing');
        address.Valid_Address__c = true;
        addresses.Add(address);

        Address__c address1 = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Shipping');
        address1.Valid_Address__c = true;
        addresses.Add(address1);
        insert addresses;

        test.startTest();


        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__Account__c = acc.Id;
        insert quote;

        ApexPages.currentPage().getParameters().put('id', quote.id);
        SBQQQuote_Preview_Approval_Val_Cont controller = new SBQQQuote_Preview_Approval_Val_Cont();

        List<SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus> approverStatuses = controller.approverStatuses;

        for(SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus approverStatus : approverStatuses)
            system.debug('Status: ' + approverStatus.Status);

        system.assert(controller.ValidationSucceeded);

        test.stopTest();

    }

    @isTest static void failTest1() {

        QTTestUtils.GlobalSetUp();


        List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'System Administrator' LIMIT 1];
    
        if (plist.size() != 1) {
            throw new QTException('Profile does not exist: System Administrator');
        }
        Profile p = plist[0];
        Id profileId = p.Id;

        User user1 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user1.Username = 'PUPU@PUPU.com.AINTEST';
        user1.Email = 'PUPU@PUPU.com.AINTEST';
        user1.isactive = false;

        User user2 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user2.Username = 'PU@PU.com.AINTEST';
        user2.Email = 'PU@PU.com.AINTEST';
        user2.isactive = true;

        User user3 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user3.Username = 'NAM@NAM.com.AINTEST';
        user3.Email = 'NAM@NAM.com.AINTEST';
        user3.isactive = true;

        User user4 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user4.Username = 'VP@VP.com.AINTEST';
        user4.Email = 'VP@VP.com.AINTEST';
        user4.isactive = true;

        List<User> users = new List<User>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        insert users;

        user1.Approval_Manager__c = null;
        user1.Quote_Approval_Role__c = 'PU';
        user2.Approval_Manager__c = user1.Id;
        user2.Quote_Approval_Role__c = 'PU';
        user3.Approval_Manager__c = user2.Id;
        user3.Quote_Approval_Role__c = 'NAM';
        user4.Approval_Manager__c = user3.Id;
        user4.Quote_Approval_Role__c = 'VP Education Services';

        update users;

        QlikTech_Company__c qtComp_Fra = QTTestUtils.createMockQTCompany('QlikTech France Pty Ltd', 'FRA', 'France','EMEA');
        qtComp_Fra.Education_Quote_Approver__c = user2.id;
        qtComp_Fra.Consulting_Quote_Approver__c = user1.id;
        update qtComp_Fra;

        Account acc;

        system.runas(user4) {

            acc = new Account();
            acc.Name = 'Test account';
            acc.QlikTech_Company__c = 'QlikTech France Pty Ltd';
            acc.Billing_Country_Code__c = qtComp_FRA.Id;
            acc.Territory_Country__c = 'France';
            insert acc;
        }

        Contact con = new Contact(firstName='RDZTestContact',
            lastName='RDZTestLastName',
            Email='RDZTest@test.com.sandbox',
            HasOptedOutOfEmail=false);
        con.AccountId = acc.id;
        insert con;

        List<Address__c> addresses = new List<Address__c> ();
        Address__c address = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Billing');
        address.Valid_Address__c = true;
        addresses.Add(address);

        Address__c address1 = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Shipping');
        address1.Valid_Address__c = true;
        addresses.Add(address1);
        insert addresses;

        test.startTest();

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__Account__c = acc.Id;
        insert quote;

        quote = [Select id,SBQQ__SalesRep__c,SBQQ__Account__c,
                                                       Sales_Approver_1_Role__c,Sales_Approver_1__c,
                                                       Sales_Approver_2_Role__c,Sales_Approver_2__c,
                                                       Sales_Approver_3_Role__c,Sales_Approver_3__c,
                                                       Sales_Approver_4_Role__c,Sales_Approver_4__c,
                                                       Sales_Approver_5_Role__c,Sales_Approver_5__c,
                                                       Consulting_Approver_1__c,
                                                       Consulting_Approver_2__c,
                                                       Consulting_Approver_3__c,
                                                       Education_Approver_1__c,
                                                       Education_Approver_2__c,
                                                       Education_Approver_3__c,
                                                       Consulting_Approver_1_Role__c,
                                                       Consulting_Approver_2_Role__c,
                                                       Consulting_Approver_3_Role__c,
                                                       Education_Approver_1_Role__c,
                                                       Education_Approver_2_Role__c,
                                                       Education_Approver_3_Role__c
                                                       from SBQQ__Quote__c where id = :quote.Id ];

        system.debug('quote.Sales_Approver_1__c: ' + quote.Sales_Approver_1__c);
        system.debug('quote.Sales_Approver_2__c: ' + quote.Sales_Approver_2__c);
        system.debug('quote.Sales_Approver_3__c: ' + quote.Sales_Approver_3__c);
        system.debug('quote.Sales_Approver_4__c: ' + quote.Sales_Approver_4__c);
        system.debug('quote.Sales_Approver_5__c: ' + quote.Sales_Approver_5__c);

        system.debug('quote.Consulting_Approver_1__c: ' + quote.Consulting_Approver_1__c);
        system.debug('quote.Consulting_Approver_2__c: ' + quote.Consulting_Approver_2__c);
        system.debug('quote.Consulting_Approver_3__c: ' + quote.Consulting_Approver_3__c);

        system.debug('quote.Education_Approver_1__c: ' + quote.Education_Approver_1__c);
        system.debug('quote.Education_Approver_2__c: ' + quote.Education_Approver_2__c);
        system.debug('quote.Education_Approver_3__c: ' + quote.Education_Approver_3__c);


        //Should fail as the last approver is not the CEO so the chain shouldn't have stopped

        ApexPages.currentPage().getParameters().put('id', quote.id);
        SBQQQuote_Preview_Approval_Val_Cont controller = new SBQQQuote_Preview_Approval_Val_Cont();

        List<SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus> approverStatuses = controller.approverStatuses;

        SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus approverStatus;

        for(integer i = 0; i< approverStatuses.Size(); i++) {
            approverStatus = approverStatuses[i];
            system.debug('debug i: ' + i);

            //Salesapprover 1-3, consulting approver 1 and education approver 1-2 should be populated
            if(i==3 || i==4 || i==6 || i==7 || i==10)
            {
                
                system.assertNotEquals(approverStatus.Status, '');
                system.assert(approverStatus.Status.Contains('field is not populated!'));
            }
            if(i==5){
                system.assertNotEquals(approverStatus.Status, '');
                system.assert(approverStatus.Status.Contains('active'));
            }

            if(i==0 || i==1)
                system.assertEquals(approverStatus.Status, '');
            if(i==2)
                system.assertEquals(approverStatus.Status, getAppStatus('Sales', i+1, false, user1)); 
            if(i == 3 || i == 4){
                system.assertEquals(approverStatus.Status, getAppStatus('Sales', i+1, false, null)); 
            }
            if(i == 5)
                system.assertEquals(approverStatus.Status, getAppStatus('Consulting', i-4, false, user1));
            if(i == 6 || i == 7)
                system.assertEquals(approverStatus.Status, getAppStatus('Consulting', i-4, false, null));

            if(i == 8)
                system.assertEquals(approverStatus.Status, '');
            if(i == 9)
                system.assertEquals(approverStatus.Status, getAppStatus('Education', i-7, false, user1));
            if(i == 10)
                system.assertEquals(approverStatus.Status, getAppStatus('Education', i-7, false, null));
            system.debug('Status: ' + approverStatus.Status);
        }

        system.assert(!controller.ValidationSucceeded);

        test.stopTest();

    }

    @isTest static void failTest2() {

        QTTestUtils.GlobalSetUp();


        List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'System Administrator' LIMIT 1];
    
        if (plist.size() != 1) {
            throw new QTException('Profile does not exist: System Administrator');
        }
        Profile p = plist[0];
        Id profileId = p.Id;

        User user1 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user1.Username = 'CEO@CEO.com.AINTEST';
        user1.Email = 'CEO@CEO.com.AINTEST';
        user1.isactive = true;

        User user2 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user2.Username = 'PU@PU.com.AINTEST';
        user2.Email = 'PU@PU.com.AINTEST';
        user2.isactive = true;

        User user3 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user3.Username = 'NAM@NAM.com.AINTEST';
        user3.Email = 'NAM@NAM.com.AINTEST';
        user3.isactive = true;

        User user4 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user4.Username = 'VP@VP.com.AINTEST';
        user4.Email = 'VP@VP.com.AINTEST';
        user4.isactive = true;

        List<User> users = new List<User>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        insert users;

        user1.Approval_Manager__c = null;
        user1.Quote_Approval_Role__c = 'CEO';
        user2.Approval_Manager__c = user1.Id;
        user2.Quote_Approval_Role__c = null;
        user3.Approval_Manager__c = user2.Id;
        user3.Quote_Approval_Role__c = null;
        user4.Approval_Manager__c = user3.Id;
        user4.Quote_Approval_Role__c = 'VP Education Services';

        update users;

        QlikTech_Company__c qtComp_Fra = QTTestUtils.createMockQTCompany('QlikTech France Pty Ltd', 'FRA', 'France','EMEA');
        qtComp_Fra.Education_Quote_Approver__c = user2.id;
        qtComp_Fra.Consulting_Quote_Approver__c = user1.id;
        update qtComp_Fra;

        Account acc;

        system.runas(user4) {

            acc = new Account();
            acc.Name = 'Test account';
            acc.QlikTech_Company__c = 'QlikTech France Pty Ltd';
            acc.Billing_Country_Code__c = qtComp_FRA.Id;
            acc.Territory_Country__c = 'France';
            insert acc;
        }

        Contact con = new Contact(firstName='RDZTestContact',
            lastName='RDZTestLastName',
            Email='RDZTest@test.com.sandbox',
            HasOptedOutOfEmail=false);
        con.AccountId = acc.id;
        insert con;

        List<Address__c> addresses = new List<Address__c> ();
        Address__c address = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Billing');
        address.Valid_Address__c = true;
        addresses.Add(address);

        Address__c address1 = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Shipping');
        address1.Valid_Address__c = true;
        addresses.Add(address1);
        insert addresses;

        test.startTest();

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__Account__c = acc.Id;
        insert quote;

        //Should fail as approver 2 & 3 have no approval role
        //For education and consulting nothing gets populated

        ApexPages.currentPage().getParameters().put('id', quote.id);
        SBQQQuote_Preview_Approval_Val_Cont controller = new SBQQQuote_Preview_Approval_Val_Cont();

        List<SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus> approverStatuses = controller.approverStatuses;

        SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus approverStatus;

        quote = [Select id,SBQQ__SalesRep__c,SBQQ__Account__c,
                                                       Sales_Approver_1_Role__c,Sales_Approver_1__c,
                                                       Sales_Approver_2_Role__c,Sales_Approver_2__c,
                                                       Sales_Approver_3_Role__c,Sales_Approver_3__c,
                                                       Sales_Approver_4_Role__c,Sales_Approver_4__c,
                                                       Sales_Approver_5_Role__c,Sales_Approver_5__c,
                                                       Consulting_Approver_1__c,
                                                       Consulting_Approver_2__c,
                                                       Consulting_Approver_3__c,
                                                       Education_Approver_1__c,
                                                       Education_Approver_2__c,
                                                       Education_Approver_3__c,
                                                       Consulting_Approver_1_Role__c,
                                                       Consulting_Approver_2_Role__c,
                                                       Consulting_Approver_3_Role__c,
                                                       Education_Approver_1_Role__c,
                                                       Education_Approver_2_Role__c,
                                                       Education_Approver_3_Role__c
                                                       from SBQQ__Quote__c where id = :quote.Id ];

        system.debug('quote.Sales_Approver_1__c: ' + quote.Sales_Approver_1__c);
        system.debug('quote.Sales_Approver_2__c: ' + quote.Sales_Approver_2__c);
        system.debug('quote.Sales_Approver_3__c: ' + quote.Sales_Approver_3__c);
        system.debug('quote.Sales_Approver_4__c: ' + quote.Sales_Approver_4__c);
        system.debug('quote.Sales_Approver_5__c: ' + quote.Sales_Approver_5__c);

        system.debug('quote.Consulting_Approver_1__c: ' + quote.Consulting_Approver_1__c);
        system.debug('quote.Consulting_Approver_2__c: ' + quote.Consulting_Approver_2__c);
        system.debug('quote.Consulting_Approver_3__c: ' + quote.Consulting_Approver_3__c);

        system.debug('quote.Education_Approver_1__c: ' + quote.Education_Approver_1__c);
        system.debug('quote.Education_Approver_2__c: ' + quote.Education_Approver_2__c);
        system.debug('quote.Education_Approver_3__c: ' + quote.Education_Approver_3__c);

        for(integer i = 0; i< approverStatuses.Size(); i++) {
            approverStatus = approverStatuses[i];

            if(i >= 0 && i <= 4){
                system.assertEquals(approverStatus.Status, getAppStatus('Sales', i+1, false, null)); 
            }
            if(i >= 5 && i <= 7)
                system.assertEquals(approverStatus.Status, getAppStatus('Consulting', i-4, false, null));

            if(i >= 8 && i <= 10)
                system.assertEquals(approverStatus.Status, getAppStatus('Education', i-7, false, null));
            system.debug('Status: ' + approverStatus.Status);
        }

        system.assert(!controller.ValidationSucceeded);

        test.stopTest();
    }
    
    @isTest static void failTest3() {

        QTTestUtils.GlobalSetUp();


        List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'System Administrator' LIMIT 1];
    
        if (plist.size() != 1) {
            throw new QTException('Profile does not exist: System Administrator');
        }
        Profile p = plist[0];
        Id profileId = p.Id;

        User user1 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user1.Username = 'CEO@CEO.com.AINTEST';
        user1.Email = 'CEO@CEO.com.AINTEST';
        user1.isactive = true;

        User user2 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user2.Username = 'PU@PU.com.AINTEST';
        user2.Email = 'PU@PU.com.AINTEST';
        user2.isactive = true;

        User user3 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user3.Username = 'NAM@NAM.com.AINTEST';
        user3.Email = 'NAM@NAM.com.AINTEST';
        user3.isactive = true;

        User user4 = new User(alias = 'newUser',
            Emailencodingkey='UTF-8', lastname='Testing', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profileId,
            Timezonesidkey='America/Los_Angeles');
        
        user4.Username = 'VP@VP.com.AINTEST';
        user4.Email = 'VP@VP.com.AINTEST';
        user4.isactive = true;

        List<User> users = new List<User>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        insert users;

        user1.Approval_Manager__c = null;
        user1.Quote_Approval_Role__c = 'CEO';
        user2.Approval_Manager__c = null;
        user2.Quote_Approval_Role__c = 'PU';
        user3.Approval_Manager__c = null;
        user3.Quote_Approval_Role__c = 'Legal';
        user4.Approval_Manager__c = user3.id;
        user4.Quote_Approval_Role__c = 'VP Education Services';

        update users;

        QlikTech_Company__c qtComp_Fra = QTTestUtils.createMockQTCompany('QlikTech France Pty Ltd', 'FRA', 'France','EMEA');
        qtComp_Fra.Education_Quote_Approver__c = user2.id;
        qtComp_Fra.Consulting_Quote_Approver__c = user1.id;
        update qtComp_Fra;

        Account acc;

        system.runas(user4) {

            acc = new Account();
            acc.Name = 'Test account';
            acc.QlikTech_Company__c = 'QlikTech France Pty Ltd';
            acc.Billing_Country_Code__c = qtComp_FRA.Id;
            acc.Territory_Country__c = 'France';
            insert acc;
        }

        Contact con = new Contact(firstName='RDZTestContact',
            lastName='RDZTestLastName',
            Email='RDZTest@test.com.sandbox',
            HasOptedOutOfEmail=false);
        con.AccountId = acc.id;
        insert con;

        List<Address__c> addresses = new List<Address__c> ();
        Address__c address = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Billing');
        address.Valid_Address__c = true;
        addresses.Add(address);

        Address__c address1 = QuoteTestHelper.createAddress(acc.Id,con.Id, 'Shipping');
        address1.Valid_Address__c = true;
        addresses.Add(address1);
        insert addresses;

        test.startTest();

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__Account__c = acc.Id;
        insert quote;

        quote = [Select id,SBQQ__SalesRep__c,SBQQ__Account__c,
                                                       Sales_Approver_1_Role__c,Sales_Approver_1__c,
                                                       Sales_Approver_2_Role__c,Sales_Approver_2__c,
                                                       Sales_Approver_3_Role__c,Sales_Approver_3__c,
                                                       Sales_Approver_4_Role__c,Sales_Approver_4__c,
                                                       Sales_Approver_5_Role__c,Sales_Approver_5__c,
                                                       Consulting_Approver_1__c,
                                                       Consulting_Approver_2__c,
                                                       Consulting_Approver_3__c,
                                                       Education_Approver_1__c,
                                                       Education_Approver_2__c,
                                                       Education_Approver_3__c,
                                                       Consulting_Approver_1_Role__c,
                                                       Consulting_Approver_2_Role__c,
                                                       Consulting_Approver_3_Role__c,
                                                       Education_Approver_1_Role__c,
                                                       Education_Approver_2_Role__c,
                                                       Education_Approver_3_Role__c
                                                       from SBQQ__Quote__c where id = :quote.Id ];

        system.debug('quote.Sales_Approver_1__c: ' + quote.Sales_Approver_1__c);
        system.debug('quote.Sales_Approver_2__c: ' + quote.Sales_Approver_2__c);
        system.debug('quote.Sales_Approver_3__c: ' + quote.Sales_Approver_3__c);
        system.debug('quote.Sales_Approver_4__c: ' + quote.Sales_Approver_4__c);
        system.debug('quote.Sales_Approver_5__c: ' + quote.Sales_Approver_5__c);

        system.debug('quote.Consulting_Approver_1__c: ' + quote.Consulting_Approver_1__c);
        system.debug('quote.Consulting_Approver_2__c: ' + quote.Consulting_Approver_2__c);
        system.debug('quote.Consulting_Approver_3__c: ' + quote.Consulting_Approver_3__c);

        system.debug('quote.Education_Approver_1__c: ' + quote.Education_Approver_1__c);
        system.debug('quote.Education_Approver_2__c: ' + quote.Education_Approver_2__c);
        system.debug('quote.Education_Approver_3__c: ' + quote.Education_Approver_3__c);

        //Should fail as approver 2 & 3 have no approval manager

        ApexPages.currentPage().getParameters().put('id', quote.id);
        SBQQQuote_Preview_Approval_Val_Cont controller = new SBQQQuote_Preview_Approval_Val_Cont();

        List<SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus> approverStatuses = controller.approverStatuses;

        SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus approverStatus;

        for(integer i = 0; i< approverStatuses.Size(); i++) {
            approverStatus = approverStatuses[i];

            system.debug('failtest3 debug i: ' + i);
            system.debug('Status: ' + approverStatus.Status);

            if(i==0)
                system.assertEquals(approverStatus.Status, getAppStatus('Sales', i+1, false, user3));
            if(i>=1 && i<=4)
                system.assertEquals(approverStatus.Status, getAppStatus('Sales', i+1, false, null));
            else if(i>=5 && i<=7)
                system.assertEquals(approverStatus.Status, '');
            else if(i==8)
                system.assertEquals(approverStatus.Status, getAppStatus('Education', (i-7), false, user2));
            else if(i>=9)
                system.assertEquals(approverStatus.Status, getAppStatus('Education',(i-7),false, null));
        }

        system.assert(!controller.ValidationSucceeded);

        test.stopTest();

    }
    @isTest static void coverageTest1() {

        QTTestUtils.GlobalSetUp();


        List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'System Administrator' LIMIT 1];
    
        if (plist.size() != 1) {
            throw new QTException('Profile does not exist: System Administrator');
        }
        Profile p = plist[0];
        Id profileId = p.Id;

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        insert quote;

        quote.Sales_Approver_1__c = null;
        quote.Sales_Approver_2__c = null;
        quote.Sales_Approver_3__c = null;
        quote.Sales_Approver_4__c = null;
        quote.Sales_Approver_5__c = null;
        quote.Consulting_Approver_1__c = null;
        quote.Consulting_Approver_2__c = null;
        quote.Consulting_Approver_3__c = null;
        quote.Education_Approver_1__c = null;
        quote.Education_Approver_2__c = null;
        quote.Education_Approver_3__c = null;
        update quote;

        //Should fail as approver 2 & 3 have no approval manager

        SBQQQuote_Preview_Approval_Val_Cont controller = new SBQQQuote_Preview_Approval_Val_Cont();

        ApexPages.currentPage().getParameters().put('id', profileId);
        controller = new SBQQQuote_Preview_Approval_Val_Cont();
        ApexPages.currentPage().getParameters().put('id', quote.Id);
        controller = new SBQQQuote_Preview_Approval_Val_Cont();
        controller.redirectToApproval();
        controller.ValidationSucceeded = true;
        controller.redirectToApproval();
        controller.redirectToQuote();
    }

    public static string getAppStatus(string type, integer appnumber, boolean ceoReached, User user){
        if(user == null)
            return new SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus(type, appnumber, ceoReached, null, null, null, null, null).Status;
        else
            return new SBQQQuote_Preview_Approval_Val_Cont.ApproverStatus(type, appnumber, ceoReached, user.Id, user.IsActive, User.Lastname, user.Quote_Approval_Role__c, user.Approval_Manager__c).Status;
    }
}