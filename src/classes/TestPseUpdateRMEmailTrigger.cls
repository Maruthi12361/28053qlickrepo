@isTest

/***************************************************************************************************************************************

	Changelog:
		2012-02-15  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
							to TestPseUpdateRMEmailTrigger()
        2017-11-09  ext_vos CR#CHG0031444 Add required fields for pse__Proj__c creation.
****************************************************************************************************************************************/

private class TestPseUpdateRMEmailTrigger {

    static testMethod void testUpdateRM() {
        // setup account
         Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert testAccount;

        // setup resources
        List<Contact> resources = new List<Contact>();
        Contact resource = new Contact(LastName = 'Test Resource', pse__Is_Resource__c = true, 
            pse__Is_Resource_Active__c = true, AccountId = testAccount.Id, 
            Email = 'testresource@qlikview.com.appdev');
        resources.add(resource);
        Contact resource2 = new Contact(LastName = 'Test Resource 2', pse__Is_Resource__c = true, 
            pse__Is_Resource_Active__c = true, AccountId = testAccount.Id, 
            Email = 'otherresource@qlikview.com.appdev');
        resources.add(resource2);
        Contact pm = new Contact(LastName = 'Test PM', pse__Is_Resource__c = true, 
            pse__Is_Resource_Active__c = true, AccountId = testAccount.Id, 
            Email = 'testpm@qlikview.com.appdev');
        resources.add(pm);
        insert resources;

        pse__Region__c hq;
        List<pse__Region__c> regions = [SELECT Id FROM pse__Region__c WHERE pse__Hierarchy_Depth__c = 0];
        if (regions == null || regions.size() == 0) {
            hq = new pse__Region__c(Name = 'Test Region 0');
            insert hq;
        } else {
            hq = regions[0];
        }
        pse__Region__c region = new pse__Region__c(Name = 'Test Region 1', pse__Parent_Region__c = hq.Id,
            pse__Region_Head__c = resource.Id);
        insert region;

        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;
        // setup Projects
        List<pse__Proj__c> projects = new List<pse__Proj__c>();
        for (Integer i = 0; i < 20; i++) {
            pse__Proj__c project = new pse__Proj__c(
                Name = 'Test Project ' + i, 
                pse__Is_Active__c = true,
                pse__Allow_Timecards_Without_Assignment__c = true,
                pse__Is_Billable__c = false, 
                pse__Account__c = testAccount.Id, 
                pse__Project_Manager__c = pm.Id,
                pse__Region__c = region.Id,
                pse__Stage__c = 'In Progress',
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                pse__Practice__c = practice.Id,
                Invoicing_Type__c = 'Deferred');
            projects.add(project);
        }
        insert projects;

        // Assert before test
        List<pse__Proj__c> testProjects = [SELECT Id, Name, Regional_Manager_Email__c 
            FROM pse__Proj__c WHERE Id in :projects];
        for (pse__Proj__c project : testProjects) {
            System.assert(project.Regional_Manager_Email__c == 'testresource@qlikview.com.appdev');
        }

        // change region head
        Test.startTest();
        region.pse__Region_Head__c = resource2.Id;
        update region;
        Test.stopTest();

        // Assert after test
        List<pse__Proj__c> afterProjects = [SELECT Id, Name, Regional_Manager_Email__c 
            FROM pse__Proj__c WHERE Id in :projects];
        for (pse__Proj__c project : afterProjects) {
            System.assert(project.Regional_Manager_Email__c == 'otherresource@qlikview.com.appdev');
        }
    }
}