//Copyright (c) 2008, Peter Aubrey, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
//
// History
//
// Version      Date            Author          Comments
// 1.0.0        02-03-2009      Peter Aubrey    Initial version

public with sharing class newsPageCtrl {

    public List<News_Item__c> lstNewsItem; 
    public List<News_Item__c> lstNewsItemCol1 = new List<News_Item__c>();
    public List<News_Item__c> lstNewsItemCol2 = new List<News_Item__c>();
    public List<News_Item__c> lstNewsItemCol3 = new List<News_Item__c>();
	public Integer lstNewsItemCount = 0;
	public Integer lstNewsItemCol1Count = 0;
	public Integer lstNewsItemCol2Count = 0;
	public Integer lstNewsItemCol3Count = 0;
	public Integer lstNewsItemCol1TotalCount = 0;
	public Integer lstNewsItemCol2TotalCount = 0;
	public Integer lstNewsItemCol3TotalCount = 0;
	public Integer newsListLimit = 5;
			

    public Boolean showNewsItemColOne{
        get { return showNewsItemColOne; }
        set { showNewsItemColOne = value; }   
    }

    public Boolean showNewsItemColTwo{
        get { return showNewsItemColTwo; }
        set { showNewsItemColTwo = value; }   
    }

    public Boolean showNewsItemColThree{
        get { return showNewsItemColThree; }
        set { showNewsItemColThree = value; }   
    }


    // Constructor initializes page and determines whether to show datatable or error message
    public newsPageCtrl() {
        
        initNewsItems();
		 
    }	

    public void initNewsItems() {
    
       	lstNewsItem = [select Id, Name, Title__c, Column__c, CreatedDate, Abstract__c, Image_URL__c, Link__c, News_Item_Text__c, Link_Target__c from News_Item__c where (Visible_From__c <= TODAY and Visible_Until__c >= TODAY) and Make_Public__c = true order by Visible_From__c desc limit 50];
		lstNewsItemCount = lstNewsItem.Size();
		
		if( lstNewsItem.isEmpty() ){
			
			// Do nothing...
			// showNewsItemColOne = false;
			// newsItemColMessage = 'No news items found.';
			
		} else {
			
			for ( Integer i = 0; i < lstNewsItem.size(); i++ ){
				
				if ( lstNewsItem.get(i).Column__c == 1 ){
					
					lstNewsItemCol1TotalCount++;
					
					if( lstNewsItemCol1Count < newsListLimit ){
						lstNewsItemCol1.add( lstNewsItem.get(i) );
						lstNewsItemCol1Count++;
					}
					
				} else if ( lstNewsItem.get(i).Column__c == 2 ){
					
					lstNewsItemCol2TotalCount++;
					
					if( lstNewsItemCol2Count < newsListLimit ){
						lstNewsItemCol2.add( lstNewsItem.get(i) );
						lstNewsItemCol2Count++;
					}
					
				} else if ( lstNewsItem.get(i).Column__c == 3 ){

					lstNewsItemCol3TotalCount++;
					
					if( lstNewsItemCol3Count < newsListLimit ){
						lstNewsItemCol3.add( lstNewsItem.get(i) );
						lstNewsItemCol3Count++;
					}

				}

			}

		}
		

		if ( lstNewsItemCol1.Size() > 0 ){
			showNewsItemColOne = true;
		} else {
			showNewsItemColOne = false;
		}
		
		if ( lstNewsItemCol2.Size() > 0 ){
			showNewsItemColTwo = true;
		} else {
			showNewsItemColTwo = false;
		}

		if ( lstNewsItemCol3.Size() > 0 ){
			showNewsItemColThree = true;
		} else {
			showNewsItemColThree = false;
		}
		
    }
    
    public list<News_Item__c> getNewsItemsColOne() {
        return lstNewsItemCol1;
    }

    public list<News_Item__c> getNewsItemsColTwo() {
        return lstNewsItemCol2;
    }

    public list<News_Item__c> getNewsItemsColThree() {
        return lstNewsItemCol3;
    }




    public Integer getlstNewsItemCount() {
        return lstNewsItemCount;
    }
    public Integer getlstNewsItemCol1Count() {
        return lstNewsItemCol1Count;
    }
    public Integer getlstNewsItemCol2Count() {
        return lstNewsItemCol2Count;
    }
    public Integer getlstNewsItemCol3Count() {
        return lstNewsItemCol3Count;
    }
    
    public Integer getlstNewsItemCol1TotalCount() {
        return lstNewsItemCol1TotalCount;
    }
    public Integer getlstNewsItemCol2TotalCount() {
        return lstNewsItemCol2TotalCount;
    }
    public Integer getlstNewsItemCol3TotalCount() {
        return lstNewsItemCol3TotalCount;
    }

	public static testMethod void NewsListTest()
	{
		
		List<News_Item__c> News = new List<News_Item__c>();
		
		News_Item__c Item = new News_Item__c();
		Item.Title__c = 'News Item I';
		Item.Column__c = 1.0;
		Item.Make_public__c = true;
		Item.Visible_From__c = Date.Today();	
		Item.Visible_Until__c = Date.Today() + 1;	
		News.add(Item);
		
		Item = new News_Item__c();
		Item.Title__c = 'News Item II';
		Item.Column__c = 2.0;
		Item.Make_public__c = true;
		Item.Visible_From__c = Date.Today();	
		Item.Visible_Until__c = Date.Today() + 1;
		News.add(Item);

		Item = new News_Item__c();
		Item.Title__c = 'News Item III';
		Item.Column__c = 3.0;
		Item.Make_public__c = true;
		Item.Visible_From__c = Date.Today();	
		Item.Visible_Until__c = Date.Today() + 1;		
		News.add(Item);
		
		insert News;
		
		test.startTest();
		
		newsPageCtrl controller = new newsPageCtrl();
		
		System.assert(controller.getlstNewsItemCount() >= 3);
		System.assert(controller.getlstNewsItemCol1Count() >= 1);
		System.assert(controller.getlstNewsItemCol2Count() >= 1);
		System.assert(controller.getlstNewsItemCol3Count() >= 1);
		System.assert(controller.getlstNewsItemCol1TotalCount() >= 1);
		System.assert(controller.getlstNewsItemCol2TotalCount() >= 1);
		System.assert(controller.getlstNewsItemCol3TotalCount() >= 1);
		
		System.assertEquals(true, controller.showNewsItemColOne);
		System.assertEquals(true, controller.showNewsItemColTwo);
		System.assertEquals(true, controller.showNewsItemColThree);
			
		Boolean hasFound = false;	
		for (News_Item__c NI : controller.getNewsItemsColOne())
		{
			System.debug('Martin: ' + NI.Title__c);
			if (NI.Title__c == 'News Item I')
			{
				hasFound = true;
			}
		}	
		System.assertEquals(true, hasFound);			

		hasFound = false;	
		for (News_Item__c NI : controller.getNewsItemsColTwo())
		{
			if (NI.Title__c == 'News Item II')
			{
				hasFound = true;
			}
		}	
		System.assertEquals(true, hasFound);			

		hasFound = false;	
		for (News_Item__c NI : controller.getNewsItemsColThree())
		{
			if (NI.Title__c == 'News Item III')
			{
				hasFound = true;
			}
		}	
		System.assertEquals(true, hasFound);			

				
		test.stopTest();
	}

}