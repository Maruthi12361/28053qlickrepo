/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing  virtual class QS_Search {
    
    public List<QS_SearchWrapper> searchResults {get; set;}
    public integer startIndex {get; set;}
    public integer pageSize {get; set;}
    public integer totalPages {get; set;}
    public integer totalSize {get; set;}
    public string  searchTerms {get; set;}

    public virtual void doSearch(){}
    
    public virtual PageReference previous() { 
        startIndex -= pageSize;
        doSearch();
        return null;
    }

    public boolean getShowEnd() {
        if (totalSize > 0) {
            return true;
        }
        return false;
    }
    
    public virtual PageReference next() { 
        startIndex += pageSize;
        doSearch();
        return null;
    }
    
    public virtual Integer getTotalPages() {
        try {
                if (math.mod(totalSize, pageSize) > 0) {
                    return totalSize/pageSize + 1;
                } 
                else {
                    return (totalSize/pageSize + 1);
                }
            }
        catch(Exception e){return 1;}
    }
    
    public virtual Integer getPageNumber() {
        return startIndex/pageSize + 1;
    }
    
    public virtual PageReference beginning() { 
        startIndex = 0;
        doSearch();
        return null;
    }
    
    public virtual PageReference end() {
        startIndex = totalSize - math.mod(totalSize, pageSize);
        doSearch();
        return null;
    }
    
    public virtual Boolean getDisablePrevious() { 
        if (startIndex > 0) return false; else return true;
    }
    
    public virtual Boolean getDisableNext() { 
        if (startIndex + pageSize < totalSize) return false; else return true;
    }
}