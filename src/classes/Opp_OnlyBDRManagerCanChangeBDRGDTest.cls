/****************************************************** 

Class: Opp_OnlyBDRManagerCanChangeBDRGDTest 

This class tests the Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery trigger 

Changelog: 
2014-11-03 CCE Initial creation for CR# 11817
2015-03-03 CCE set opp.GI_Acceptance_Status__c = 'GI Rejected', required to be added due to CR 16194 - validation rule OP034
               set opp.GI_Rejected_Reason__c = 'Created in Error', required to be added due to CR 16194 - validation rule OP035
2016-06-08 CCE Andrew Lokotosh Comented Forecast Amount fields 249
24.03.2017 Rodion Vakulovskyi
2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring 
2020-07-09 Leonardo Grauer - Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
		"Deployment Preference" is required in Goal Confirmed stage   
******************************************************/ 
@isTest
private class Opp_OnlyBDRManagerCanChangeBDRGDTest {

    static final String DEBUGPREFIX = 'Opp_OnlyBDRManagerCanChangeBDRGDTest.cls: ';
    private static ID ID_RecordType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;//Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Qlikbuy CCS Standard II').getRecordTypeId();
    private static Account acc = new Account();
    private static Opportunity opp = new Opportunity();
    private static User BDRUser = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Global Marketing Std User');
    
    //Test that a BDR can move the Opportunity to stage "Goal Rejected" but cannot move the Opportunity Stage from "Goal Rejected"
    @isTest static void CreateOppAsBDR()
    {
        QTTestUtils.GlobalSetUp();
        String DEBUGPREFIX2 = 'CreateOppAsBDR: ';
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Starting');
        
        setup();
        System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries());
        
        //Have a BDR User create an Opportunity
        System.runAs(BDRUser)
        {
            //Create Opportunity
            opp.Short_Description__c = 'TestOpp - delete me';
            opp.Name = 'TestOpp - DeleteMe';
            opp.Type = 'New Customer';
            opp.Revenue_Type__c = 'Direct';
            opp.CloseDate = Date.today();
            opp.StageName = 'Goal Discovery';
            opp.RecordTypeId = ID_RecordType;
            opp.CurrencyIsoCode = 'GBP';
            opp.AccountId = acc.Id;
            opp.Signature_Type__c = 'Digital Signature';
            opp.Amount = 500.00;    //so that we don't need to add a Sphere Of Influence (SoI)
            insert opp;

            System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries()); 
            Test.startTest();
		
            opp.StageName = 'Goal Rejected';
            opp.Primary_reason_lost__c = 'Created in Error';
            opp.GI_Acceptance_Status__c = 'GI Rejected';    //CCE - needed to be added due to CR 16194 - validation rule OP034
            opp.GI_Rejected_Reason__c = 'Created in Error'; //CCE - needed to be added due to CR 16194 - validation rule OP035
            opp.Included_Products__c = 'Qlik Sense';
            update opp;

            //reset the trigger semaphore flag so the trigger will run again
            Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery', 1);
        
            //Have the user try to move the Stage from 'Goal Rejected' (as a BDR they should not be able to do this and we should get an exception thrown)
            boolean failed = false;
            string exMsg = '';
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            try {
                opp.StageName = 'Goal Discovery';
                update opp;
            }
            catch (Exception ex) {
                System.Debug(ex.getMessage());
                exMsg = ex.getMessage();
                System.debug(DEBUGPREFIX + DEBUGPREFIX2 + 'exMsg: ' + exMsg); 
                failed = true;
            }
            
            System.assertEquals(true, failed);
            System.assert(exMsg.contains('Only BDR Managers can move the Opp out of Goal Rejected stage'), 'Unexpected exception thrown');

            Test.stopTest();
        }
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Finishing');     
    }


    //Test that a BDR can move the Opportunity to stage "Goal Identified" and back to "Goal Discovery"
    @isTest static void BDRMovesOppToGI()
    {
        QTTestUtils.GlobalSetUp();
        String DEBUGPREFIX2 = 'BDRMovesOppToGI: ';
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Starting');
        
        setup();
        System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries());
        
        //Have a BDR User create an Opportunity
        System.runAs(BDRUser)
        {
            //Create Opportunity
            opp.Short_Description__c = 'TestOpp - delete me';
            opp.Name = 'TestOpp - DeleteMe';
            opp.Type = 'New Customer';
            opp.Revenue_Type__c = 'Direct';
            opp.CloseDate = Date.today();
            opp.StageName = 'Goal Discovery';
            opp.RecordTypeId = ID_RecordType;
            opp.CurrencyIsoCode = 'GBP';
            opp.AccountId = acc.Id;
            opp.Signature_Type__c = 'Digital Signature';
            opp.Amount = 500.00;    //so that we don't need to add a Sphere Of Influence (SoI)
            insert opp;

            System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries()); 
            Test.startTest();

            opp.StageName = 'Goal Identified';
            update opp;
            
            //reset the trigger semaphore flag so the trigger will run again
            Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery', 1);
        
            //Have the user try to move the Stage from 'Goal Identified' back to 'Goal Discovery' (they should be able to do this)
            boolean failed = false;
            string exMsg = '';
            try {
                opp.StageName = 'Goal Discovery';
                update opp;
            }
            catch (Exception ex) {
                System.Debug(ex.getMessage());
                exMsg = ex.getMessage();
                System.debug(DEBUGPREFIX + DEBUGPREFIX2 + 'exMsg: ' + exMsg); 
                failed = true;
            }
            
            System.assertEquals(false, failed); //there should have been no exception thrown
            Opportunity retopp = [select Id, StageName from Opportunity where Id = :opp.Id];
            System.assertEquals('Goal Discovery', retopp.StageName);

            Test.stopTest();
        }
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Finishing');     
    }

    //Test that a user with the BDR Manager permission set can move the Opportunity to stage "Goal Discovery" from "Goal Rejected"

    @isTest static void BDRManagerMovesOppToGD()
    {
        QTTestUtils.GlobalSetUp();
        String DEBUGPREFIX2 = 'BDRManagerMovesOppToGD: ';
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Starting');
        
        setup();
        System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries());
        
        //Have a BDR User create an Opportunity
        System.runAs(BDRUser)
        {
            //Create Opportunity
            opp.Short_Description__c = 'TestOpp - delete me';
            opp.Name = 'TestOpp - DeleteMe';
            opp.Type = 'New Customer';
            opp.Revenue_Type__c = 'Direct';
            opp.CloseDate = Date.today();
            opp.StageName = 'Goal Discovery';
            opp.RecordTypeId = ID_RecordType;
            opp.CurrencyIsoCode = 'GBP';
            opp.AccountId = acc.Id;
            opp.Signature_Type__c = 'Digital Signature';
            opp.Amount = 500.00;    //so that we don't need to add a Sphere Of Influence (SoI)
            insert opp;

            System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries()); 
            
            opp.StageName = 'Goal Rejected';
            opp.Primary_reason_lost__c = 'Created in Error';
            opp.GI_Acceptance_Status__c = 'GI Rejected';    //CCE - needed to be added due to CR 16194 - validation rule OP034
            opp.GI_Rejected_Reason__c = 'Created in Error'; //CCE - needed to be added due to CR 16194 - validation rule OP035
            opp.Included_Products__c = 'Qlik Sense';
            update opp;

            //reset the trigger semaphore flag so the trigger will run again
            Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery', 1);
        
            Test.startTest();

            //Get BDR_Managers permission set Id and assign BDR_Managers permission set to User
            PermissionSet ps = [select Id from PermissionSet where Name = 'BDR_Managers' limit 1];
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.Id, AssigneeId = BDRUser.Id);
            insert psa;

            boolean failed = false;
            string exMsg = '';
            try {
                opp.StageName = 'Goal Discovery';
                update opp;
            }
            catch (Exception ex) {
                System.Debug(ex.getMessage());
                exMsg = ex.getMessage();
                System.debug(DEBUGPREFIX + DEBUGPREFIX2 + 'exMsg: ' + exMsg); 
                failed = true;
            }
            
            System.assertEquals(false, failed); //there should have been no exception thrown
            Opportunity retopp = [select Id, StageName from Opportunity where Id = :opp.Id];
            System.assertEquals('Goal Discovery', retopp.StageName);

            Test.stopTest();
        }
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Finishing');     
    }

    //Test that a BDR cannot move the Opportunity to stage "Goal Confirmed"

    @isTest static void BDRCannotMoveOppToGC()
    {
        QTTestUtils.GlobalSetUp();
        String DEBUGPREFIX2 = 'BDRCannotMoveOppToGC: ';
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Starting');
        
        setup();
        System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries());
        
        //Have a BDR User create an Opportunity
        System.runAs(BDRUser)
        {
            //Create Opportunity
            opp.Short_Description__c = 'TestOpp - delete me';
            opp.Name = 'TestOpp - DeleteMe';
            opp.Type = 'New Customer';
            opp.Revenue_Type__c = 'Direct';
            opp.CloseDate = Date.today();
            opp.StageName = 'Goal Discovery';
            opp.RecordTypeId = ID_RecordType;
            opp.CurrencyIsoCode = 'GBP';
			opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            opp.AccountId = acc.Id;
            opp.Signature_Type__c = 'Digital Signature';
            insert opp;

            System.debug(DEBUGPREFIX + DEBUGPREFIX2 + '#Queries used in this apex code so far: ' + Limits.getQueries()); 
            Test.startTest();

            //reset the trigger semaphore flag so the trigger will run again
            Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery', 1);      
            
            //Have the user try to move the Stage from 'Goal Discovery' to 'Goal Confirmed' (they should not be able to do this)
            boolean failed = false;
            string exMsg = '';
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            try {
                //opp.License_Forecast_Amount__c = 5.00;
                opp.StageName = 'Goal Confirmed';
                update opp;
            }
            catch (Exception ex) {
                System.Debug(ex.getMessage());
                exMsg = ex.getMessage();
                System.debug(DEBUGPREFIX + DEBUGPREFIX2 + 'exMsg: ' + exMsg); 
                failed = true;
            }
            
            System.assertEquals(true, failed);  //there should have been an exception thrown
            System.assert(exMsg.contains('BDRs can only move the Opp to Goal Identified or Goal Rejected stage'), 'Unexpected exception thrown');

            Test.stopTest();
        }
        System.Debug(DEBUGPREFIX + DEBUGPREFIX2 + 'Finishing');     
    }

    private static void Setup()
    {
        BDRUser = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Global Marketing Std User');
        //make the user a BDR
        BDRUser.Function__c = 'iBDR';   //the function field is what we are using to define a BDR role
        update BDRUser;    
        
        //Create test Account
        acc =  QTTestUtils.createMockAccount('TestAcc', BDRUser);
        acc.Pending_Validation__c = false;  //needed due to OP010_Cant_Create_4_EndUserAc_PendingVal validation rule
        update acc;
    }
    
}