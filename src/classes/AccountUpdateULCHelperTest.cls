/*
    2018-01-23  AIN     Initial implementation for CHG0032866
    2018-03-06  CRW     Modified ulc username to avoid new validation error.BMW-508
*/
@isTest
private class AccountUpdateULCHelperTest {
    
    @isTest static void test_method_one() {
        Semaphores.SetAllSemaphoresToTrue();
        Account a = new Account();
        a.QCC_Entitled__c = false;
        a.Name = 'Test account';
        Insert a;
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Testersson';
        c.AccountId = a.Id;
        insert c;

        ULC_Details__c ulc = new ULC_Details__c();
        ulc.ULCStatus__c = 'Active';
        ulc.ContactId__c = c.Id;
        ulc.Toggle_dirty__c = false;
        ulc.ULCName__c = 'ulculculc';
        insert ulc;

        Semaphores.SetAllSemaphoresToFalse();

        test.startTest();
        a.QCC_Entitled__c = true;
        update a;
        test.stopTest();

        ulc = [select id, Toggle_dirty__c from ULC_Details__c where id = :ulc.Id];

        system.assert(ulc.Toggle_dirty__c == true);
    }
}