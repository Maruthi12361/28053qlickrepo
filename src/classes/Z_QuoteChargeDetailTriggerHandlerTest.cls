@isTest(seeAllData=True)
private class Z_QuoteChargeDetailTriggerHandlerTest {
    /*
	private static Account account;
    private static Contact contact;
    private static Zuora__CustomerAccount__c customerAccount; 
    private static Opportunity opp;
    private static zqu__Quote__c quote;
    private static Product2 product;



    private static zqu__ProductRatePlan__c productRatePlan;
    private static zqu__QuoteAmendment__c quoteAmendment;
    private static zqu__QuoteRatePlan__c quoteRatePlan;
    private static zqu__QuoteChargeDetail__c  quoteDetail;
    private static zqu__QuoteAmendment__c quoteAmendment2, quoteAmendment3,quoteAmendment4,quoteAmendment5,quoteAmendment6;
    private static zqu__QuoteRatePlan__c quoteRatePlan2,quoteRatePlan3,quoteRatePlan4,quoteRatePlan5,quoteRatePlan6;

 	private static void set_up_data() {
 		account =Z_TestFactory.makeAccount();
        insert account;
        account.Pending_Validation__c =FALSE;
        update account;

        
        contact = Z_TestFactory.makeContact(account,'aaaa1789','bbbb134534','AA1@gtytyt.com','United Kingdom','','80011');
        contact.MailingStreet = 'hhhh';
        contact.MailingCity = 'sdfsdfsd';
        contact.phone = '404-464-4784';
        insert contact;

        opp = Z_TestFactory.makeOpportunity(account, contact);
        opp.Revenue_Type__c='Direct';
        insert opp;

        quote = Z_TestFactory.makeQuote2(opp, account);
        quote.zqu__BillToContact__c = contact.id;
        quote.zqu__SoldToContact__c = contact.id;
        insert quote;

		// Create Annual - Standard
		quoteDetail = New zqu__QuoteChargeDetail__c();
		quoteDetail.Name = 'Marketing Essentials - Annual Service Fee';
		quoteDetail.zqu__Quote__c = quote.Id;

		insert quoteDetail;
	} */
	 @isTest static void test_updateQuoteChargeDetail() {
        //set_up_data();
        
        //Create sample Catalog
        Product2 prod = Z_TestFactory.makeProduct();
        zqu__ProductRatePlan__c prp = Z_TestFactory.makeProductRatePlan(String.valueOf(prod.Id), 'prp');
        zqu__ProductRatePlanCharge__c prpc = Z_TestFactory.makeProductRatePlanCharge(String.valueOf(prp.Id), 'Recurring', 'Flat Fee', 'Test');
         
        //Create Account/Opportunity/Quote
        Account acc = Z_TestFactory.makePartnerAccount('country', 'cName', 'nState', 'zipcode');
		acc.Pending_Validation__c = false;
         insert acc;

        Contact contact = Z_TestFactory.makeContact(acc);
        insert contact;

        Opportunity opp = Z_TestFactory.buildOpportunity(acc);
        opp.Revenue_Type__c='Direct';
         insert opp;
        zqu__Quote__c quote = Z_TestFactory.makeQuote(opp, acc);
        quote.zqu__BillToContact__c = contact.id;
        quote.zqu__SoldToContact__c = contact.id;
        quote.Billing_Frequency__c = 'Prepaid';
        insert quote;
        
        zqu__QuoteAmendment__c qa = Z_TestFactory.makeQuoteAmendment(quote);
         insert qa;
        zqu__QuoteRatePlan__c qrp = Z_TestFactory.makeQRP(quote, qa, prp);
         insert qrp;
        zqu__QuoteRatePlanCharge__c qrpc = Z_TestFactory.makeQRPC(quote, qrp, prpc, 'Test');
         insert qrpc;
        zqu__QuoteChargeSummary__c qcs = Z_TestFactory.makeQCS(quote, qrp, prpc, 'Test');
         insert qcs;
        zqu__QuoteChargeDetail__c qcd = Z_TestFactory.makeQCD(quote, 'Test');
        
        Test.startTest();
         insert qcd;
        //update quoteDetail;
        
    
		//updateChargeSummaryMetrics(quoteDetail);

		/*zqu__QuoteChargeDetail__c zQuote = [SELECT PAR__c,
												   FL_Commission__c,
												   Setup_Fees__c
											  FROM zqu__QuoteChargeDetail__c
											 WHERE Id = :quoteAnnualStandard.Id];

		system.assertEquals(240,zQuote.PAR__c);
		system.assertEquals(240,zQuote.FL_Commission__c);
		system.assertEquals(null,zQuote.Setup_Fees__c);*/

		Test.stopTest();
		
	}
}