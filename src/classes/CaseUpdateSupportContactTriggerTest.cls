// Test case to test the trigger CaseUpdateSupportContact
//    Changelog:
//        2013-04-03  Madhav Kakani - Fluido Oy Initial development for CR# 7356
//        2014-03-18  CCE Added to start/stop calls to stop "Too many SOQL queries: 101" error
//		  2014-08-01  TJG Resolved error
//						aused by: TriggerHandler.TriggerHandlerException: Maximum loop count of 30 reached in ContactTriggerHandler
//						rigger.CaseUpdateSupportContact: line 62, column 1: [] stack Class.CaseUpdateSupportContactTriggerTest.runTest: line 34, column 1
@isTest
private class CaseUpdateSupportContactTriggerTest {
    static testMethod void runTest() {
        Semaphores.SetAllSemaphoresToTrue();
        Account act = new Account(name = 'Test Account');
        act.QlikTech_Company__c = 'Test Company';
        insert act;
        List<Contact> cList = new List<Contact>();

        Contact ct = new Contact(AccountId = act.Id, lastname = 'Contact1', firstname = 'Test');
        //insert ct;
        cList.Add(ct);
        Contact ct2 = new Contact(AccountId = act.Id, lastname = 'Contact2', firstname = 'Test');
        //insert ct2;
        cList.Add(ct2);
        insert cList;

        List<Case> caseList = new List<case>();

        Case c1 = new Case(AccountId = act.Id, ContactId = ct.Id, Status = 'New');
        c1.Severity__c = '3';
        c1.Origin = 'Email';
        c1.Subject = 'Test';
        c1.Description = 'Testing';
        c1.type = 'Incident';
        //insert c1;
        caseList.Add(c1);

        Case c2 = new Case(AccountId = act.Id, ContactId = ct2.Id, Status = 'New');
        c2.Severity__c = '3';
        c2.Origin = 'Email';
        c2.Subject = 'Test';
        c2.Description = 'Service Request';
        c2.type = 'Service Request';
        //insert c2;
        caseList.Add(c2);
        Semaphores.SetAllSemaphoresToFalse();
        insert caseList;

        Test.startTest();   //CCE 2014-02-07 Added to stop "Too many SOQL queries: 101" error

        Contact x = [Select Support_Contact__c FROM Contact WHERE Id = :ct.Id];
        system.assert(x.Support_Contact__c == 'Yes');

        x = [Select Qoncierge_Contact__c FROM Contact WHERE Id = :ct2.Id];
        system.assert(x.Qoncierge_Contact__c == 'Yes');
        Test.stopTest();    //CCE 2014-02-07 Added to stop "Too many SOQL queries: 101" error
    }
}