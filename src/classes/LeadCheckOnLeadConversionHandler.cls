/**********************************************************
* Class: LeadCheckOnLeadConversionHandler
*
* Changelog:
*	2016-08-23 : roman@4front : Migrated from LeadCheckOnLeadConversion.trigger.
*
*************************************************************/

public class LeadCheckOnLeadConversionHandler {
	public LeadCheckOnLeadConversionHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld, Boolean isUpdate) {
		String msg = '';
	    Boolean isValid = true;
	    
	    //Check if its a OEM Lead and all data has been provided if not display an error on the Conversion Page.
	    for (integer i = 0; i < triggerNew.size(); i++)
	    {
	        msg = 'Cannot convert OEM lead. Required data: ';
	        
	        Lead NewLead = triggerNew[i];
	         
	        if (isUpdate)
	        {
	            Lead OldLead = triggerOld[i];
	            
	            if ((OldLead.isConverted == false) && (NewLead.isConverted == true))
	            {
	                //Check that Software Segment, Target Industry, Deployment Model, Solution Name, No. Of Customers
	                //are filled before conversion if Lead is a oem record type.
	                if (NewLead.OEM__c == true)
	                {
	                	System.debug('OEM Settings!!!!');
	                	System.debug('Software_Segment__c: ' + NewLead.Software_Segment__c);
	                	System.debug('Target_Industry__c: ' + NewLead.Target_Industry__c);
	                	System.debug('Deployment_Model__c: ' + NewLead.Deployment_Model__c);
	                	System.debug('Solution_Name__c: ' + NewLead.Solution_Name__c);
	                	System.debug('No_of_Customers__c: ' + NewLead.No_of_Customers__c);                	
	                    if (NewLead.Software_Segment__c == '' || NewLead.Software_Segment__c == null)
	                    {
	                        msg+='Software Segment; ';
	                        isValid = false;
	                    }
	                    if (NewLead.Target_Industry__c == '' || NewLead.Target_Industry__c == null)
	                    {
	                        msg+='Target Industry; ';
	                        isValid = false;
	                    }
	                    if (NewLead.Deployment_Model__c == '' || NewLead.Deployment_Model__c == null)
	                    {
	                        msg+='Deployment Model; ';
	                        isValid = false;
	                    }
	                    if (NewLead.Solution_Name__c == '' || NewLead.Solution_Name__c == null)
	                    {
	                        msg+='Solution Name; ';
	                        isValid = false;
	                    }
	                    if (NewLead.No_of_Customers__c == '' || NewLead.No_of_Customers__c == null)
	                    {
	                        msg+='No. Of Customers; ';
	                        isValid = false;
	                    }
	                    if (!isValid)
	                    {
	                    	NewLead.addError(msg);                      
	                    }               
	                }
	            }
	        }
	    }    
	}
}