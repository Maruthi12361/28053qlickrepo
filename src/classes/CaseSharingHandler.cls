/*
 File Name: CaseSharingHandler
 Moved from trigger Case_Sharing on Case (after insert, after update)

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.

*/

public with sharing class CaseSharingHandler {

    public static void handlerCaseSharing(List<Case> newCases, List<Case> oldCases) {

        List<Id> caseIds = new List<Id>();

        if (oldCases != null) { // Trigger.isUpdate
            for (integer i = 0; i < newCases.Size(); i++) {
                if (newCases[i].OwnerId != oldCases[i].OwnerId
                        || newCases[i].Account_Origin__c != oldCases[i].Account_Origin__c || newCases[i].severity__c != oldCases[i].severity__c) {
                    caseIds.Add(newCases[i].Id);
                }
            }
            system.debug('its an update');
        } else { //Trigger.isInsert
            for (integer i = 0; i < newCases.Size(); i++) {
                if (newCases[i].Account_Origin__c != null) {
                    caseIds.Add(newCases[i].Id);
                }
            }
            system.debug('its an insert');
        }

        if (caseIds.size() > 0) {
            system.debug('case sharing handler updating these cases '+caseIds);
            if (!Test.isRunningTest()) {
                ApexSharingRules.UpdateCaseSharing(caseIds);
            } else if (ApexSharingRules.TestingParnershare) {
                ApexSharingRules.UpdateCaseSharingForTest(caseIds);
            }
        }
    }
}