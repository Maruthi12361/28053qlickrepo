/****************************************************************
*
*  OppGIAcceptanceCtrlHandler
*
*  2016-05-19 TJG : Migrated from the following trigger
					OppGIAcceptanceCtr 
*  2017-06-22 MTM  QCW-2711 remove sharing option
*****************************************************************/
public class OppGIAcceptanceCtrlHandler {
	// to be called on both events before Insert and before Update
	public static void handle(Map<Id, Opportunity> triggerOldMap, List<Opportunity> triggerNew) {

	    set<string> SomeOppStageValues = new set<string>
	    {
	    	'Alignment Meeting', 'Demo Mini SiB', 'Goal Shared', 'Goal Confirmed', 'OEM - Identified by Partner', 'Champion', 'Prove Value', 
            'SiB', 'Trial', 'OEM - Deal Commitment to Partner', 'ER - Quote', 'Negotiating', 'Present Contracts', 'Quote', 'ER - Present Contracts', 
            'OEM - Run Rate Best Case', 'OEM - Run Rate Commit', 'OEM Pre-Pay Quote', 'Closed Lost', 'ER - Closed Lost', 'OEM - Closed Lost', 
            'Closed Won', 'Deal Split-closed Won', 'ER - Closed Won', 'OEM - Closed Won'
        }; 
	    //set<string> SomeOppStageValues = new set<string>{'Goal Confirmed','Champion','Prove Value','Negotiating','OEM - Vender Selected','OEM - Deal Commitment to Partner','OEM - Closed Won','OEM - Closed Lost','Closed Lost','Closed Won'}; 
	    string GD = 'Goal Discovery';
	    string GI = 'Goal Identified';
	    Opportunity oldOpp = new Opportunity();
	    Date todaysDate = Date.today();

	    for (Opportunity o: triggerNew) {
	        if (Trigger.isUpdate) {
	            oldOpp = triggerOldMap.get(o.Id);
	        }
	        
	        //most of the fields are updated due to the StageName value changing
	        if (Trigger.isInsert || (oldOpp.StageName != o.StageName)) {
	            //if we are at stage GD then we clear various GI Acceptance fields
	            if (o.StageName == GD) {
	                o.GI_Acceptance_Status__c = null;
	                o.GI_Rejected_Reason__c = null;
	                o.GI_Disqualified_Reason__c = null;
	                o.GI_Acceptance_Start_Date__c = null;
	                o.GI_Acceptance_Stop_Date__c = null;
	                o.GI_Qualification_Start_Date__c = null;
	                o.GI_Qualification_Stop_Date__c = null;
	            }
	            else if (o.StageName == GI) {
	                o.GI_Acceptance_Start_Date__c = todaysDate;
	                o.GI_Acceptance_Status__c = null;
	                o.GI_Rejected_Reason__c = null;
	                o.GI_Disqualified_Reason__c = null;
	                o.GI_Acceptance_Stop_Date__c = null;
	                o.GI_Qualification_Start_Date__c = null;
	                o.GI_Qualification_Stop_Date__c = null;
	            }
	            else if (SomeOppStageValues.contains(o.StageName)) {
	                if (o.GI_Acceptance_Start_Date__c == null) { o.GI_Acceptance_Start_Date__c = todaysDate; }
	                if (o.GI_Acceptance_Stop_Date__c == null) { o.GI_Acceptance_Stop_Date__c = todaysDate; }
	                if ((o.GI_Acceptance_Status__c == null) || (o.GI_Acceptance_Status__c == 'GI Rejected') || (o.GI_Acceptance_Status__c == 'GI Disqualified')) {
	                    o.GI_Acceptance_Status__c = 'GI Accepted';
	                    o.GI_Rejected_Reason__c = null;
	                    o.GI_Disqualified_Reason__c = null;
	                }
	                //becasue we are setting 'GI Accepted' we should also set 'GI_Qualification_Start_Date__c' and 'GI_Qualification_Stop_Date__c'
	                if (o.GI_Qualification_Start_Date__c == null) { o.GI_Qualification_Start_Date__c = todaysDate; }
	                if (o.GI_Qualification_Stop_Date__c == null) { o.GI_Qualification_Stop_Date__c = todaysDate; }
	            }
	        }
	            
	        //but some can also be changed by the 'GI_Acceptance_Status__c' field changing
	        if ((Trigger.isInsert || (oldOpp.GI_Acceptance_Status__c != o.GI_Acceptance_Status__c)) && o.StageName == GI) {
	            if (o.GI_Acceptance_Status__c == 'GI Rejected') { o.GI_Acceptance_Stop_Date__c = todaysDate; }            
	            if (o.GI_Acceptance_Status__c == 'GI Accepted') { o.GI_Acceptance_Stop_Date__c = todaysDate; o.GI_Qualification_Start_Date__c = todaysDate; }
	            if (o.GI_Acceptance_Status__c == 'GI Disqualified') { o.GI_Acceptance_Stop_Date__c = todaysDate; o.GI_Qualification_Start_Date__c = todaysDate; o.GI_Qualification_Stop_Date__c = todaysDate; }
	        }
	    }		
	}
}