/*
	InstanceDetailsComponentController
	
	Author:
		Martin Haagen, mhg@qlikview.com
		
	Changes:
		2010-11-26 MHG	Created function.
						CR# 835: Remove Static endpoint references
						https://emea.salesforce.com/a0C2000000C9mxw

 */
public class InstanceDetailsComponentController 
{
	public string getHostName()
	{
		return ApexPages.currentPage().getHeaders().get('Host');
	}
	
	static testMethod void TEST_LeadCountryISOUpdate() 
	{
		
		test.startTest();
		
		InstanceDetailsComponentController cnt = new InstanceDetailsComponentController();
		string TestStr = cnt.getHostName();
				
		test.stopTest();	
	}
}