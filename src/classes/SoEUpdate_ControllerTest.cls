// Changelog:
// 2020-07-09 Leonardo Grauer Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
//				"Deployment Preference" is required in Goal Confirmed stage 
@isTest
private class SoEUpdate_ControllerTest {
	
	static testMethod void Test_SoEUpdateController() 
    {

        //CCE  Adding an account with BillingCountryCode and Qliktech Company
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

        QTCustomSettings__c Settings = new QTCustomSettings__c();
        Settings.Name = 'Default';
        Settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        Settings.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        Settings.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        Settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert Settings;
        QuoteTestHelper.createCustomSettings();
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test Opp 1';
		Opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        Opp.StageName = 'Goal Confirmed';   
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        Opp.CloseDate = Date.today();
        Opp.AccountId = acc.Id;
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other'; 
        Opp.Included_Products__c = 'Qlik Sense';
        insert Opp;
        
        
        test.startTest();
        ApexPages.StandardController StandardController = new ApexPages.StandardController(Opp);
        SoEUpdate_Controller controller = new SoEUpdate_Controller(StandardController);

        Sequence_of_Event__c SOE = controller.NewSoE;
        SOE.S_o_E_Date__c = Date.today();
        SOE.Proposed_Event__c = 'Other';
        controller.NewSoE = SOE;
        controller.CreateNew();
        
        List<Sequence_of_Event__c> Events = controller.getSoERelatedList();
        System.assert(Events != null);
        System.assert(Events.size() == 1);
        
        Sequence_of_Event__c SOELoaded = Events[0];
        
        System.currentPagereference().getParameters().put('SoEid', ''); 
        controller.EditSoE();   // Should not go through because of the parameter SoEid's not set
        controller.DeleteSoE(); //                      - " - 
                
        System.currentPageReference().getParameters().clear();      
        System.currentPagereference().getParameters().put('SoEid', SOELoaded.Id);                           
        System.debug('Test_SoEUpdateController - Edit & Delete');       
        controller.EditSoE();
        controller.DeleteSoE();             
                
        test.stopTest();
    }
	
}