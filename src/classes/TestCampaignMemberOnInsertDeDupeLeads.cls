/*********************************************************************************************************
* Name: TestCampaignMemberOnInsertDeDupeLeads
* Does: Unit tests for the trigger CampaignMemberOnInsertDeDupeLeads
* 														
* Log Changes:
*
* 11/10/2012	TJG	Reduce the number of database operations to cope with the SFDC limits
*
* 17/08/2012	TJG PO179 
*				https://projects.qliktech.com/sites/projects/MarketingasaServiceforPartners/default.aspx
*
*********************************************************************************************************/
@isTest
public class TestCampaignMemberOnInsertDeDupeLeads {
	static final String DEBUG_PREFIX = 'TJ-.-JT: ';
	static final String LEADSOURCE = 'PART - Partner';
	static final String WA_SOURCE = 'Qonnect Marketing Services';
	static final String CAMPAIGN_STATUS = 'Sent';
	// Lead Record Type - 'Partner Opp Reg'
	static final String RECORD_TYPE = '012D0000000JsWAIA0'; // on PartnerDev sandbox 
	static final String SERVICEOWNERID = '00520000000zCfxAAE';
	
    public static testMethod void testDeDupeLeads() {
		Subsidiary__c tesForSubs = TestQuoteUtil.createSubsidiary();
		Campaign newCamp = new Campaign(
			Name = 'MaasTestCampaign1',
			Planned_Opportunity_Value__c = 10000
		);
		insert(newCamp);
		
		Campaign newCamp1 = new Campaign(
			Name = 'MaasTestCampaign2',
			Planned_Opportunity_Value__c = 100000
		);
		insert(newCamp1);

		Campaign newCamp2 = new Campaign(
			Name = 'MaasTestCampaign3',
			Planned_Opportunity_Value__c = 100000
		);
		insert(newCamp2);
		
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'USA',
			Country_Name__c = 'USA',
			QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = tesForSubs.id		
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];	
		
		Lead lead0 = new Lead(
			FirstName = 'Tom',
			LastName = 'Jones',
			Email = 'po179@test.com',
			Company = 'Courtly Ltd',
			Phone = '7776213421',
			City = 'Norwich',
			State = 'Norfolk',
			Street = 'New Road',
			PostalCode = 'NR4 4TJ',
			Country = 'UK',
			Industry = 'Telecom',
			Job_Title__c = 'Director',
			HasOptedOutOfEmail = false,
			LeadSource = LEADSOURCE,			
			RecordTypeId = RECORD_TYPE,
			QMS_Original_Partner_OwnerId__c = SERVICEOWNERID,
			Web_Activity_Source__c = WA_SOURCE
		);
		
		insert(lead0);		
		System.Debug(DEBUG_PREFIX + 'Lead0 Id = '+ lead0.Id);
		
		Lead newLead = new Lead(
			FirstName = 'Joe',
			LastName = 'Blob',
			Email = 'po179@test.com',
			Company = 'Test Company 1',
			Phone = '7776213421',
			City = 'Norwich',
			State = 'Norfolk',
			Street = 'New Road',
			PostalCode = 'NR4 4TJ',
			Country = 'UK',
			Industry = 'Media',
			Job_Title__c = 'Director',
			HasOptedOutOfEmail = false,
			RecordTypeId = RECORD_TYPE,
			QMS_Original_Partner_OwnerId__c = SERVICEOWNERID,	
			Web_Activity_Source__c = WA_SOURCE,
			LeadSource = LEADSOURCE			
		);
		insert(newLead);
		System.Debug(DEBUG_PREFIX + 'newLead Id = '+ newLead.Id);
		
		CampaignMember cm1 = new CampaignMember(
			LeadId = newLead.Id,
			CampaignId = newCamp.Id
		);

		CampaignMember cm1a = new CampaignMember(
			LeadId = newLead.Id,
			CampaignId = newCamp1.Id
		);
		
		List<CampaignMember> cmList = new List<CampaignMember>();

		cmList.Add(cm1);
		cmList.Add(cm1a);
		
		Lead lead2 = new Lead(
			FirstName = 'James',
			LastName = 'Bond',
			Email = 'po179@test.com',
			Company = 'Courtly Ltd',
			Phone = '7776213421',
			City = 'Norwich',
			State = 'Norfolk',
			Street = 'New Road',
			PostalCode = 'NR4 4TJ',
			Country = 'UK',
			Industry = 'Media',
			Job_Title__c = 'Director',
			HasOptedOutOfEmail = false,
			LeadSource = LEADSOURCE,			
			RecordTypeId = RECORD_TYPE,
			QMS_Original_Partner_OwnerId__c = SERVICEOWNERID,
			Web_Activity_Source__c = WA_SOURCE
		);
		
		insert(lead2);
		System.Debug(DEBUG_PREFIX + 'lead2 Id = '+ lead2.Id);
		
		CampaignMember cm2 = new CampaignMember(
			LeadId = lead2.Id,
			CampaignId = newCamp.Id,
			Status = CAMPAIGN_STATUS
		);

		Lead lead2b = new Lead(
			FirstName = 'Jack',
			LastName = 'Hunt',
			Email = 'po179@test.com',
			Company = 'Trinity Ltd',
			Phone = '7776213421',
			City = 'Norwich',
			State = 'Norfolk',
			Street = 'New Road',
			PostalCode = 'NR4 4TJ',
			Country = 'UK',
			Industry = 'Media',
			Job_Title__c = 'Director',
			HasOptedOutOfEmail = false,
			LeadSource = LEADSOURCE,			
			RecordTypeId = RECORD_TYPE,
			QMS_Original_Partner_OwnerId__c = SERVICEOWNERID,
			Web_Activity_Source__c = WA_SOURCE
		);
		insert (lead2b);

		CampaignMember cm2b = new CampaignMember(
			LeadId = lead2b.Id,
			CampaignId = newCamp2.Id
		);

		cmList.Add(cm2);
		cmList.Add(cm2b);
		insert(cmList);

		Account newAccount = new Account(
			Name='MaasTestAccount',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id
		);				
		
		insert(newAccount);
		
		Contact newContact = new Contact(
			AccountId = newAccount.Id,
			FirstName = 'MaasTest',
			LastName = 'Test'
		);
		
		insert(newContact);
		CampaignMember cm0 = new CampaignMember(
			ContactId = newContact.Id,
			CampaignId = newCamp.Id
		);
			
		insert(cm0);

		System.Debug(DEBUG_PREFIX + 'new lead Id = ' + newLead.Id + ', first name = ' + newLead.FirstName);
		
		List<Lead> ldList = [select Id, FirstName from Lead where Id = :newLead.Id limit 1];
		If (ldList.size() > 0)
		{
			Lead lead3 = ldList.get(0);
			// verify the lead has been updated
			System.assertEquals('James', lead3.FirstName);
		}
		System.Debug(DEBUG_PREFIX + 'lead2.Id = ' + lead2.Id);
		// new lead should have been deleted
		List<Lead> newleads = [select Id, FirstName from Lead where Id = :lead2.Id];
		System.assertEquals(0, newLeads.size());
		System.Debug(DEBUG_PREFIX + 'newleads = ' + newleads);

		Lead lead2a = new Lead(
			FirstName = 'James',
			LastName = 'Bond',
			Email = 'po-179@test.com',
			Company = 'Courtly Ltd',
			Phone = '7776213421',
			City = 'Norwich',
			State = 'Norfolk',
			Street = 'New Road',
			PostalCode = 'NR4 4TJ',
			Country = 'UK',
			Industry = 'Media',
			Job_Title__c = 'Director',
			HasOptedOutOfEmail = false,
			LeadSource = LEADSOURCE,			
			RecordTypeId = RECORD_TYPE,
			QMS_Original_Partner_OwnerId__c = SERVICEOWNERID,
			Web_Activity_Source__c = WA_SOURCE
		);
		
		insert(lead2a);
		CampaignMember cm2a = new CampaignMember(
			LeadId = lead2a.Id,
			CampaignId = newCamp1.Id,
			Status = CAMPAIGN_STATUS
		);
		//insert(cm2a);
    }
}