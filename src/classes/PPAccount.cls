/*************************************************************************************************************
 Name:PPAccount
 Author: Santoshi Mishra
 Purpose: This class redirects to user contact page on partner portal
 Created Date : March 2012
*************************************************************************************************************/
public with sharing class PPAccount { 
    public User currUser {get;set;}// Stores logged in User info
    public Pagereference redirectUser()// When My Profile link is clicked on Partner Portal.It redirects to User's contact info.
    {
         if (UserInfo.getUserType().equals('PowerPartner')) // Determines if the user is Partner User
         {
            currUser=[select AccountId,ContactId,FirstName,Id from User where id=:System.UserInfo.getUserId()];
            //return(new Pagereference('/apex/MyAccountLandingPage')); //+currUser.ContactId
            return(new Pagereference('/'+currUser.ContactId));
         }
         
         return null;
    }
}