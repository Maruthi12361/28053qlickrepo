/*    Copyright (c) 2014 Zuora, Inc.
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy of
 *   this software and associated documentation files (the "Software"), to use copy,
 *   modify, merge, publish the Software and to distribute, and sublicense copies of
 *   the Software, provided no fee is charged for the Software.  In addition the
 *   rights specified above are conditioned upon the following:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   Zuora, Inc. or any other trademarks of Zuora, Inc.  may not be used to endorse
 *   or promote products derived from this Software without specific prior written
 *   permission from Zuora, Inc.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 *   ZUORA, INC. BE LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *   IN THE EVENT YOU ARE AN EXISTING ZUORA CUSTOMER, USE OF THIS SOFTWARE IS GOVERNED
 *
 *   BY THIS AGREEMENT AND NOT YOUR MASTER SUBSCRIPTION AGREEMENT WITH ZUORA.
 
 *  April 16 2020 - DOB - DTR-761 - To preview the calculations before sending to zbilling.
 */
public class Z_PreviewBeforeSendToZBilling implements Queueable, Database.AllowsCallouts  {

    public final Set<String> quoteIds;
    
    public Z_PreviewBeforeSendToZBilling(Set<String> ids){
        quoteIds = ids;
    }
    
    /**
    *This method instatiates the Z_SendToZBiling class after previewing
    *@param ids         Set of quote ids
    *@return void
    **/
    public void execute(QueueableContext context){
        if(quoteIds.size() > 0){
            String quoteId = new List<String> (quoteIds).get(0);
            system.debug('quoteId - ' + quoteId);
            zqu__Quote__c quote = [SELECT zqu__Is_Charge_Expired__c FROM zqu__Quote__c WHERE Id=:quoteId];
            if(quote.zqu__Is_Charge_Expired__c){
                zqu.zQuoteUtil.ZBillingResult previewResult = zqu.QuoteRecalculateController.JR_recalculate(quoteId);
                system.debug('previewResult  ' + previewResult);
                if(previewResult.success){
                    Z_SendToZBillingInstantiator.send(quoteIds);
                }
                addSystemMessage(previewResult, quoteId);
            }else{
                Z_SendToZBillingInstantiator.send(quoteIds);
            }
        }
    }
    
    private void addSystemMessage(zqu.zQuoteUtil.ZBillingResult previewResult, String quoteId){
        if(String.isNotEmpty(previewResult.message) || !previewResult.success || Test.isRunningTest()){
            System_Message__c systemMessage = new System_Message__c();
            systemMessage.Zuora_Quote__c = quoteId;
            systemMessage.Error_Message__c = previewResult.message;
            systemMessage.Error_Type__c = 'ZUORA PREVIEW INFO';
            insert systemMessage;
        }
    }

}