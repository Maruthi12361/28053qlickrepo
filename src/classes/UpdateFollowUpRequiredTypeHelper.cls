/*****************************************************************************************************************
Change Log:

20150106    CCE     Initial development CR# 16194 Update Lead Lifecycle Process - https://eu1.salesforce.com/a0CD000000kld2k
                    Create trigger:
                    "If Overall Follow-Up Required is true, then update lead status to Follow-Up Required
                    If lead status was Follow-Up Rejected or Follow-Up Disqualified, then update Follow-Up Required Type to Revived
                    Otherwise update Follow-Up Required Type to New"
                    NOTE: Do not add a semaphore to this trigger as it will stop it working. The trigger tests for
                    the "Overall: Follow-up Required" changing and Salesforce rerunning the triggers (see Salesforce Order of Execution document)
20150720    CCE     CR# 49405 - https://eu1.salesforce.com/a0CD000000vX8rp Clear Follow Up Rejected and Disqualified Reasons
20160422    CCE     CR# 82611 - https://eu1.salesforce.com/a0CD000000xSrSI Velocify Lead Lifecycle Changes - removing "Not Follow-Up Required Reason" field.
2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
20180611    CCE     CHG0034173 Remove "with sharing"
20200312    CCE     To be deactivated/removed as part of CHG0037726 DGM-55 FUR Revamp 2020
******************************************************************************************************************/
public class UpdateFollowUpRequiredTypeHelper {
	
/*	public static void UpdateFollowUpRequiredType(List<Contact> triggernew, List<Contact> triggerold, Boolean isInsert, Boolean isUpdate){
		//Create a Set of Contact Id's that appear on Closed Won opps and a Set of Contact Id's that appear on Closed Lost opps, note that we
        // do not need a count of how many times they are Closed Won or Lost, just that they are.
        
        Set<ID> conIds = new Set<Id>();
        for (Contact c: triggernew) {
            conIds.add(c.Id);
        }
        System.debug('UpdateFollowUpRequiredTypeForContact: conIds.size() = ' + conIds.size());
        
        Set<ID> conIdsClosedWon = new Set<Id>(); //This will contain a list of Contact Id's that appear in the SoI of Closed Won Opportunities
        Set<ID> conIdsClosedLost = new Set<Id>(); //This will contain a list of Contact Id's that appear in the SoI of Closed Lost Opportunities
        Boolean populateClosedLists = false;
                
        for (Integer i = 0; i < triggernew.size(); i++)
        {
            System.debug('UpdateFollowUpRequiredTypeForContact: isInsert = ' + isInsert);
            //Don't do any of the following if the Contact is being created from a Lead conversion - values will map over instead.
            if (!(isInsert && triggernew[i].IsNotConvertedFromLead__c == false)) {
                System.debug('UpdateFollowUpRequiredTypeForContact: Not from Lead conversion');
                System.debug('UpdateFollowUpRequiredTypeForContact: isInsert = ' + isInsert);
                System.debug('UpdateFollowUpRequiredTypeForContact: triggernew[i].Event_Follow_Up_Required__c = ' + triggernew[i].Event_Follow_Up_Required__c);
                System.debug('UpdateFollowUpRequiredTypeForContact: triggernew[i].Existing_Follow_Up_Required__c = ' + triggernew[i].Existing_Follow_Up_Required__c);
                System.debug('UpdateFollowUpRequiredTypeForContact: triggernew[i].New_Follow_Up_Required__c = ' + triggernew[i].New_Follow_Up_Required__c);
                if (((isInsert || triggerold[i].Event_Follow_Up_Required__c != true) && triggernew[i].Event_Follow_Up_Required__c == true) ||
                   ((isInsert || triggerold[i].Existing_Follow_Up_Required__c != true) && triggernew[i].Existing_Follow_Up_Required__c == true) ||
                   ((isInsert || triggerold[i].New_Follow_Up_Required__c != true) && triggernew[i].New_Follow_Up_Required__c == true))
                {
                    System.debug('UpdateFollowUpRequiredTypeForContact: triggernew[i].Contact_Status__c = ' + triggernew[i].Contact_Status__c);
                    if ((triggernew[i].Contact_Status__c == 'Follow-Up Rejected') || (triggernew[i].Contact_Status__c == 'Follow-Up Disqualified'))
                    {
                        triggernew[i].Follow_Up_Required_Type__c = 'Revived';
                    }
                    else
                    {
                        //populate the ClosedWon, ClosedLost lists only if we have not already done so (calculated here so we avoid doing the Select unnecessarily)
                        if (!populateClosedLists)
                        {
                            for (Sphere_of_Influence__c soi : [select Contact__c, Opportunity__r.StageName from Sphere_of_Influence__c where Contact__c IN :conIds])
                            {
                                System.debug('UpdateFollowUpRequiredTypeForContact: Id='+soi.Contact__c+' StageName='+soi.Opportunity__r.StageName);
                                if (soi.Opportunity__r.StageName == 'Closed Won') {
                                    conIdsClosedWon.add(soi.Contact__c);
                                }
                                else if (soi.Opportunity__r.StageName == 'Closed Lost') {
                                    conIdsClosedLost.add(soi.Contact__c);
                                }
                            }
                            System.debug('UpdateFollowUpRequiredTypeForContact: conIdsClosedWon size = ' + conIdsClosedWon.size());
                            System.debug('UpdateFollowUpRequiredTypeForContact: conIdsClosedLost size = ' + conIdsClosedLost.size());
                            populateClosedLists = true; //so we only populate the Sets once
                        }
                        
                        if (conIdsClosedWon.contains(triggernew[i].Id))
                        {
                            triggernew[i].Follow_Up_Required_Type__c = 'Won';
                        }
                        else if (conIdsClosedLost.contains(triggernew[i].Id))
                        {
                            triggernew[i].Follow_Up_Required_Type__c = 'Lost';
                        }
                        else
                        {
                            triggernew[i].Follow_Up_Required_Type__c = 'New';
                        }
                    }
                    triggernew[i].Follow_Up_Rejected_Reason__c = null; //as we're changing the Contact_Status__c to 'Follow-Up Required' then we don't want this set
                    triggernew[i].Follow_Up_Disqualified_Reason__c = null; //as we're changing the Contact_Status__c to 'Follow-Up Required' then we don't want this set
                    triggernew[i].Not_Follow_Up_Required_Reason__c = null; //if we're changing the Contact_Status__c to 'Follow-Up Required' then we don't want this set
                    triggernew[i].Contact_Status__c = 'Follow-Up Required';            
                }
                if (isUpdate) {
                    if ((triggerold[i].Contact_Status__c == 'Follow-Up Rejected') && (triggernew[i].Contact_Status__c == 'Follow-Up Rejected') && (triggerold[i].Follow_Up_Rejected_Reason__c == 'Not Follow-Up Required') && (triggernew[i].Follow_Up_Rejected_Reason__c != 'Not Follow-Up Required')) {
                        triggernew[i].Not_Follow_Up_Required_Reason__c = null;
                    }
                    else if ((triggerold[i].Contact_Status__c == 'Follow-Up Rejected') && (triggernew[i].Contact_Status__c != 'Follow-Up Rejected')) {
                        triggernew[i].Follow_Up_Rejected_Reason__c = null;
                        triggernew[i].Not_Follow_Up_Required_Reason__c = null;
                    }
                    else if ((triggerold[i].Contact_Status__c == 'Follow-Up Disqualified') && (triggernew[i].Contact_Status__c != 'Follow-Up Disqualified')) {
                        triggernew[i].Follow_Up_Disqualified_Reason__c = null;
                    }
                }
            }
        }
	}*/
}