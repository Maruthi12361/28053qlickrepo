//
// Temporary user data access fro mheroku to create Cookie, before we move to Token
// for backward compatibility during ULC upgrade/transition
//
// SAN 26/04/2015
//

@RestResource(urlMapping='/HerokuLoginREST/*')
global class HerokuCookieLogin {

	@HttpGet
    global static RESTHerokuCookieData doGet() {


    	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String userName = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

        ULC_Details__c result = [SELECT Id,
        								ULCName__c, 
        								ContactId__r.FirstName,
        								ContactId__r.LastName,
        								ContactId__r.Email,
        								ContactId__c,
        								ContactId__r.AccountId,
        								ContactId__r.Id,
        								ContactId__r.Account_Type__c,
        								ContactId__r.Account.Adopted_Navision_Status__c,
        								ContactId__r.Account.Partner_Type__c,        								
        								ContactId__r.Account.Parent.ParentId,
        								ContactId__r.MailingCity, 
        								ContactId__r.MailingCountry, 
        								ContactId__r.MailingPostalCode, 
        								ContactId__r.MailingState, 
        								ContactId__r.MailingStreet, 
        								ContactId__r.Country_Code__r.Name,
                                 		ContactId__r.Phone, 
        								LeadId__c,
        								LeadId__r.FirstName,
        								LeadId__r.LastName,
        								LeadId__r.Email, 
        								LeadId__r.City, 
        								LeadId__r.Street, 
        								LeadId__r.Country, 
        								LeadId__r.PostalCode, 
        								LeadId__r.State,
        								LeadId__r.Country_Code__r.Name,
                                 		LeadId__r.Phone

        								FROM ULC_Details__c WHERE ULCName__c = :userName];
        return new RESTHerokuCookieData(result);

    }

	global class RESTHerokuCookieData { 
	        
	  global String USERNAME;
	  global String FIRSTNAME;
	  global String LASTNAME;
	  global String EMAIL;
	  global String CONTACTID;
	  global String ACCOUNTID;
	  global String ULCLEVELS;
	  global String TIMELIMIT;
	  global String ORGID;
	  global String SECRET;

	  global String LEADID;
	  global String CITY;
	  global String COUNTRY;
	  global String COUNTRYCODE;
	  global String STATE; 
	  global String STREET;
	  global String POSTALCODE;
	  global String ULCID; 
      global String PHONE;


	        
    global RESTHerokuCookieData (ULC_Details__c ULCData) {   

			QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');

      USERNAME = ULCData.ULCName__c;
      if (ULCData.ContactId__c == null)
      {
      	EMAIL = ULCData.LeadId__r.Email;
        FIRSTNAME = ULCData.LeadId__r.FirstName;
        LASTNAME = ULCData.LeadId__r.LastName;
        CONTACTID = '';
        ACCOUNTID = '';
        ULCLEVELS = 'Base';
        LEADID = ULCData.LeadId__c;
        CITY = ULCData.LeadId__r.City;
        COUNTRY = ULCData.LeadId__r.Country;
        COUNTRYCODE = ULCData.LeadId__r.Country_Code__r.Name;
        STATE = ULCData.LeadId__r.State;
        STREET = ULCData.LeadId__r.Street;
        POSTALCODE = ULCData.LeadId__r.PostalCode;
        PHONE = ULCData.LeadId__r.Phone;
     	}
      else
      {
      	LEADID= '';
      	EMAIL = ULCData.ContactId__r.Email;
        FIRSTNAME = ULCData.ContactId__r.FirstName;
        LASTNAME = ULCData.ContactId__r.LastName;
        CONTACTID = ULCData.ContactId__r.Id;
        ACCOUNTID = ULCData.ContactId__r.AccountId;
        ULCLEVELS = GetULCLevel(ULCData);
        CITY = ULCData.ContactId__r.MailingCity;
        COUNTRY = ULCData.ContactId__r.MailingCountry;
        COUNTRYCODE = ULCData.ContactId__r.Country_Code__r.Name;
        STATE = ULCData.ContactId__r.MailingState;
        STREET = ULCData.ContactId__r.MailingStreet;
        POSTALCODE = ULCData.ContactId__r.MailingPostalCode;
        PHONE = ULCData.ContactId__r.Phone;
      }
      
      ORGID = UserInfo.getOrganizationId();
      TIMELIMIT = String.valueOf(DateTime.now().getTime());
      SECRET = (Settings.ULC_Crypto_Secret__c != null ? Settings.ULC_Crypto_Secret__c : 'secret');
      ULCID = ULCData.Id;
	    
    }

	        @TestVisible
	        String GetULCLevel(ULC_Details__c u){
				string ULCLevels = '';
		        if (u.ContactId__c != null) {
		            string NavisionStatus = '';
		            string act = u.ContactId__r.Account_Type__c;
		            if (act == 'Customer' || u.ContactId__r.Account.Adopted_Navision_Status__c == 'Customer') {
		                NavisionStatus = 'Customer';
		            } else if (act == 'Partner' || u.ContactId__r.Account.Adopted_Navision_Status__c == 'Partner') {
		                NavisionStatus = 'Partner';
		                if (u.ContactId__r.Account.Partner_Type__c == 'MasterReseller') {
		                    ULCLevels += 'MasterReseller';
		                }
		            } else if (act == 'Pending Partner' || act == 'Pending Customer') {
		                NavisionStatus = 'Download';
		            } else if (act == 'Employee') {
		                NavisionStatus = 'Employee';
		            } else {
		                if (u.ContactId__r.Account.ParentId != null) {
		                    string AdditionalULCLevel = GetParentNavisionStatus(u.ContactId__r.Account.ParentId, 0);
		                    if (AdditionalULCLevel != null) {
		                        ULCLevels += AdditionalULCLevel;
		                    }
		                }
		                if (HasActiveLicense(u.ContactId__r.AccountId)) {
		                    if (ULCLevels != '') {
		                        ULCLevels += ',';
		                    }
		                    ULCLevels += 'Download';
		                }
		            }

		            if (NavisionStatus != '') {
		                if (ULCLevels != '') {
		                    ULCLevels += ',';
		                }
		                ULCLevels += NavisionStatus;
		            }
		            string additionalLevels = LoadAdditionalLevels(u.ContactId__c);
		            if (additionalLevels != '') {
		                if (ULCLevels != '') {
		                    ULCLevels += ',';
		                }
		                ULCLevels += additionalLevels;
		            }
		        }

		        if (ULCLevels != '') {
		            ULCLevels += ',';
		        }
		        ULCLevels += 'Base';

	        	return ULCLevels;

	        }

			@TestVisible
		    private string GetParentNavisionStatus(string ParentId, Integer RecursionLevel) {
		        List<Account> accounts = [SELECT Adopted_Navision_Status__c, Navision_Status__c, ParentId FROM Account WHERE Id = :ParentId];
		        if (accounts.size() == 0) {
		            return null;
		        }
		        Account a = accounts.get(0);
		        if (a.Navision_Status__c == 'Customer' || a.Adopted_Navision_Status__c == 'Customer') {
		            return 'Customer';
		        } else if (a.Navision_Status__c == 'Partner' || a.Adopted_Navision_Status__c == 'Partner') {
		            return 'Partner';
		        } else if (a.ParentId != null) {
		            if (RecursionLevel == 3) {
		                return null;
		            }
		            return GetParentNavisionStatus(a.ParentId, RecursionLevel++);
		        }
		        return null;
		    }

		    @TestVisible
		    private Boolean HasActiveLicense(string AccountId) {
		        List<AggregateResult> res = [SELECT count(Time_Limit__c) FROM Account_License__c WHERE Time_Limit__c >= TODAY AND Account__c = :AccountId];
		        if (res.size() == 0) {
		            return false;
		        }
		        AggregateResult a = res.get(0);
		        if (a.get('expr0') != 0) {
		            return true;
		        }
		        return false;
		    }

		    @TestVisible
		    private string LoadAdditionalLevels(string ContactId) {
		        List<Assigned_ULC_Level__c> levels = [SELECT ULCLevelId__r.Name FROM Assigned_ULC_Level__c WHERE ContactId__c = :ContactId AND Status__c = 'Approved'];
		        string ULCLevels = '';
		        for (Assigned_ULC_Level__c a : levels) {
		            if (ULCLevels != '') {
		                ULCLevels += ',';
		            }
		            ULCLevels += a.ULCLevelId__r.Name;
		        }
		        return ULCLevels;
		    }
    
	}


}