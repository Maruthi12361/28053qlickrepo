/**************************************************************** 
* 
* TOSApprovedCallHandler.class
* 
* 2018-02-01 AYS : Class created. IT-439 
* 2018-08-09 extbad : set udf_varchar_1 to '""'. IT-899
* 2019-06-11 extbad : set udf_varchar_1 to ''. IT-1885
*****************************************************************/ 
public class TOSApprovedCallHandler {

  final static String APPROVED_STATUS = 'Legal Approval Granted';

  public static void handle(List<TOS_Registration__c> triggerNew, Map<Id,TOS_Registration__c> triggerOldMap){
    
    Set<Id> s_TOSIdToCall = new Set<Id>();
    
    for(TOS_Registration__c tOS: triggerNew){
      if(tOS.TOS_Legal_Approval_Status__c == APPROVED_STATUS 
          && triggerOldMap.get(tOS.id).TOS_Legal_Approval_Status__c != APPROVED_STATUS){
        s_TOSIdToCall.add(tOS.id);
      }
    }
    //Prevented ivocation in test in order to split future call and DML operation
    if(!Test.isRunningTest() && !s_TOSIdToCall.isEmpty()){
      TOSApprovedCallHandler.call(s_TOSIdToCall);
    }
    
  }

  @future(callout=true)
  public static void call(Set<ID> s_TOSIdToCall){

    TOS_registration_settings__mdt trsettings = getSettings();

    List<TOS_Registration__c> l_TOStoCall = new List<TOS_Registration__c>([
      SELECT id, Name
      FROM TOS_Registration__c
      WHERE Id IN: s_TOSIdToCall
    ]);
    
    String xmlStrind = generateXML();
    String bodyString = generateBody(new Map<String,String>{
          'username'              => trsettings.Username__c,
          'Xpassword'             => trsettings.Password__c,
          'company_login_id'      => 'qlik',
          'product_id'            => '1',
          'sendRegistrationComms' => '0',
          'apiFunction'           => 'PostXmlRegistrationUpdate',
          'sxml'                  => xmlStrind
    });

    for(TOS_Registration__c tOSrecord : l_TOStoCall) {
      
      Http http = new Http();
      HttpRequest request = new HttpRequest();
        request.setEndpoint(trsettings.Endpoint__c);
        request.setMethod('POST');
        request.setTimeout(120000);
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('charset', 'utf-8');

        bodyString = bodyString.replace('tOSrecord_Name', meetingNumber(tOSrecord.Name));

        request.setBody(bodyString);

      System.debug('debug_Body: ' + request.getBody());

      HttpResponse response;

      try{
        response = http.send(request);
        tOSrecord.Litmos_Response__c = response.getStatusCode() + ': ' +
          response.getStatus() + ': ' + response.getBody();
      }catch(exception e){
        tOSrecord.Litmos_Response__c = e.getMessage();
      }    
      //tOSrecord.Litmos_Response__c += '\n\nREQUEST WAS:\n ' + request.getBody();
    }
    update l_TOStoCall;
  }

  private static String meetingNumber(String tOSrecord_Name){
    List<String> l_NameParts = tOSrecord_Name.split('-');
    return l_NameParts.get(l_NameParts.size()-1);
  }
  

  private static String generateBody(Map<String,String> m_Params){
    String result = '';
    List<String> l_joinedParams = new List<String>();
    
    for(String key : m_Params.keySet()){
      l_joinedParams.add(EncodingUtil.urlEncode(key, 'UTF-8') + '=' + EncodingUtil.urlEncode(m_Params.get(key), 'UTF-8'));
    }
    
    result = String.join(l_joinedParams, '&');
    //result = EncodingUtil.urlEncode(result, 'UTF-8');

    return result;
  }

  private static String generateXML(){
    Xmlstreamwriter xmlW = new Xmlstreamwriter();

    //stream
    xmlW.writeStartDocument('utf-8','1.0');
    xmlW.writeStartElement(null, 'viewcentral_exchange', null); 
      xmlW.writeStartElement(null, 'post_registration_info', null); 
        xmlW.writeStartElement(null, 'cid', null); 
          xmlW.writeCharacters('qlik');
        xmlW.writeEndElement();
        xmlW.writeStartElement(null, 'pid', null); 
          xmlW.writeCharacters('1');
        xmlW.writeEndElement(); 
        xmlW.writeStartElement(null, 'attendee', null); 
        xmlW.writeEndElement();  
        xmlW.writeStartElement(null, 'registration', null); 
          xmlW.writeStartElement(null, 'events_attended_id', null); 
            xmlW.writeCharacters('tOSrecord_Name');
          xmlW.writeEndElement();
          xmlW.writeStartElement(null, 'current_status', null); 
            xmlW.writeCharacters('Confirmed');
          xmlW.writeEndElement();
          xmlW.writeStartElement(null, 'udf_varchar_1', null); 
            xmlW.writeCharacters('');
          xmlW.writeEndElement();
        xmlW.writeEndElement(); 
      xmlW.writeEndElement(); 
    xmlW.writeEndElement();
    //end stream

    System.debug('The XML :'+xmlW.getXmlString());
    String xmlStrind = xmlW.getXmlString();
    xmlW.close();
    return xmlStrind;
  }
  
  private static TOS_registration_settings__mdt getSettings(){

    TOS_registration_settings__mdt trsettings;
    Boolean isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;

    if(isSandbox){
      trsettings = [SELECT MasterLabel, QualifiedApiName, Username__c, Endpoint__c, Password__c 
        FROM TOS_registration_settings__mdt WHERE QualifiedApiName = 'Sandbox'];
    }else{
      trsettings = [SELECT MasterLabel, QualifiedApiName, Username__c, Endpoint__c, Password__c 
        FROM TOS_registration_settings__mdt WHERE QualifiedApiName = 'Production'];
    }

    return trsettings;
  }

}