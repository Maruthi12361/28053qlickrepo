/********
* NAME : OppExecDealsVisibilityHandlerTest
* Description: Test class to validate the OppExecDealsVisibilityHandlerTest
*
*
*Change Log:
20.02.2020 : OTQ-8 PB error handling

******/

@isTest
private class OppExecDealsVisibilityHandlerTest {

    
    private static testMethod void testHandle(){

        QTTestUtils.GlobalSetUp();

        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        insert settings;
        /*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc'           
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
        */
        QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        Id partnerAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();

        
        Account a = new Account(Name ='Partner', recordtypeId = partnerAccRecordTypeId);
        insert a;
        
    	Opportunity o = new Opportunity(Name='Test Opp', AccountId=a.id, StageName = 'Goal Discovery', CloseDate = System.today().addDays(30));
      	insert o; 
                
        Competitor_Information__c ci = new Competitor_Information__c(Opportunity__c = o.Id, Primary_Competitor__c = 'Test Comp');
        insert ci; 
        
        Influencing_Partner__c ip = new Influencing_Partner__c(Opportunity__c = o.Id, Influencing_Partner_Account__c = a.Id);
        insert ip; 
        
        Competitor_Information__c[] info = [select Opportunity__c, Primary_Competitor__c from Competitor_Information__c where Opportunity__c =: o.Id];
        System.assertEquals(1, info.size());
        
        o = [SELECT Id, Influencing_Partners_Count__c, Competitors__c FROM Opportunity WHERE Id =: o.Id];
        
        System.assertEquals(1, o.Influencing_Partners_Count__c);
        System.assertEquals(1, o.Competitors__c);
        
        
        Semaphores.SetAllSemaphoresToFalse();
        OpportunityExecDealsVisibilityHandler.reset();
        
        
        Test.startTest(); 
        OpportunityExecDealsVisibilityHandler.handle(new List<Opportunity>{o}, false, true);
        Test.stopTest();

        
    }
    
    
    
    
    
    
}