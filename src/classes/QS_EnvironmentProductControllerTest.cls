/************************************************************************
 * @author  AIN
 * 2016-04-11 AIN First log entry, increased test coverage
 *  
 * Test class for controller QS_EnvironmentProductController
 *
 *Modification log
 * 07.02.2018 : Reshma - Fix for deployment error
 * 11.07.2018  ext_bjd  ITRM-229 Added logic which defines the current sandbox is prod or qa and run the test
 * 20.08.2018  ext_bjd  ITRM-229 Added logic which created community user. In this context, removed previous part of logic for defines the current sandbox.
 * 18.02.2019 AIN       Added more unique naming convention
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 * 2019-11-25 extcqb    Changed profile to fix tests
 *
 *************************************************************************/
@isTest
private class QS_EnvironmentProductControllerTest {

    private static Entitlement selectedLicense;
    private static EnvironmentProduct__c selectedEnvironmentProduct;
    private static Environment__c selectedEnvironment;
    private static Account selectedAccount;
    private static User userAdmin = QTTestUtils.createMockSystemAdministrator();

    @isTest static void TestNewEnvironmentFail() {

        InsertTestData();

        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPageReference(pageRef);


        QS_EnvironmentProductController controller = new QS_EnvironmentProductController();

        string mode = controller.Mode;
        controller.Mode = 'NewEnvironment';
        mode = controller.Mode;

        controller.Create();
        controller.CreateNewClicked();
        EnvironmentProduct__c ep = controller.EnvironmentProduct;
    }
    @isTest static void TestNewEnvironmentSuccessWithLicense() {

        InsertTestData();

        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPageReference(pageRef);


        QS_EnvironmentProductController controller = new QS_EnvironmentProductController();

        Entitlement license = [select id from Entitlement limit 1];

        string mode = controller.Mode;
        controller.Mode = 'NewEnvironment';
        mode = controller.Mode;

        controller.envProduct = selectedEnvironmentProduct;

        controller.selectedLicense = license.Id;

        controller.Create();
        controller.CreateNewClicked();
        EnvironmentProduct__c ep = controller.EnvironmentProduct;
    }
    @isTest static void TestNewEnvironmentSuccessNoLicense() {

        InsertTestData();

        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPageReference(pageRef);


        QS_EnvironmentProductController controller = new QS_EnvironmentProductController();

        Entitlement license = [select id from Entitlement limit 1];

        string mode = controller.Mode;
        controller.Mode = 'NewEnvironment';
        mode = controller.Mode;

        controller.envProduct = selectedEnvironmentProduct;

        controller.Create();
        controller.CreateNewClicked();
        EnvironmentProduct__c ep = controller.EnvironmentProduct;
    }
    @isTest static void TestNewEnvironmentException() {

        System.runAs (userAdmin) {
            InsertTestData();
        }

        PageReference pageRef = Page.QS_EnvironmentDetails;
        Test.setCurrentPageReference(pageRef);


        QS_EnvironmentProductController controller = new QS_EnvironmentProductController();

        Entitlement license = [select id from Entitlement limit 1];

        string mode = controller.Mode;
        controller.Mode = 'NewEnvironment';
        mode = controller.Mode;

        insert selectedEnvironmentProduct;

        controller.envProduct = selectedEnvironmentProduct;

        controller.Create();
        controller.CreateNewClicked();
        EnvironmentProduct__c ep = controller.EnvironmentProduct;
    }
    @isTest static void TestExistingEnvironment() {

        System.runAs (userAdmin) {
            InsertTestData();
        }

        PageReference pageRef = Page.QS_EnvironmentDetails;
        pageRef.getParameters().put('Id', Id.valueOf(selectedEnvironment.Id));
        Test.setCurrentPageReference(pageRef);

        QS_EnvironmentProductController controller = new QS_EnvironmentProductController();
        insert selectedEnvironmentProduct;
        controller.selectedId = selectedEnvironmentProduct.Id;
        controller.Initiate();
        controller.EditClicked();
        controller.Cancel();
        QS_EnvironmentProductController.getLicenseList(selectedAccount.Name);
    }
    @isTest static void TestRefreshLicenseList() {

        InsertTestData();

        PageReference pageRef = Page.QS_EnvironmentDetails;
        pageRef.getParameters().put('Id', Id.valueOf(selectedEnvironment.Id));
        Test.setCurrentPageReference(pageRef);

        QS_EnvironmentProductController controller = new QS_EnvironmentProductController();
        controller.refreshLicenseList();
    }

    @isTest
    private static void InsertTestData() {

        Account portalAccount;
        //Create portal account owner
        UserRole portalRole = [SELECT Id FROM UserRole WHERE PortalType = 'None' LIMIT 1];
        Profile profile1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User portalAccountOwner = new User(
                UserRoleId = portalRole.Id,
                ProfileId = profile1.Id,
                Username = System.now().millisecond() + 'test298791657@test.com',
                Alias = 'batman',
                Email = 'bruce.wayne@testwayneenterprises.com',
                EmailEncodingKey = 'UTF-8',
                FirstName = 'BruceTest',
                LastName = 'TestWayne',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = 'America/Chicago'
        );
        Database.insert(portalAccountOwner);

        System.runAs(portalAccountOwner) {

            //creates Billing Address to be used on Account record
            QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
            insert qtc;

            //Create account
            Account portalAccount1 = new Account(
                    Name = 'TestAccount',
                    OwnerId = portalAccountOwner.Id,
                    QlikTech_Company__c = qtc.QlikTech_Company_Name__c,
                    Billing_Country_Code__c = qtc.Id,
                    BillingStreet = 'Street 123',
                    BillingCity = 'City 123',
                    BillingState = 'S 123',
                    BillingCountry = qtc.Country_Name__c,

                    ShippingStreet = 'Street 123',
                    ShippingCity = 'City 123',
                    ShippingState = 'S 123',
                    ShippingCountry = qtc.Country_Name__c
            );
            Database.insert(portalAccount1);
            portalAccount = portalAccount1;

            selectedAccount = portalAccount;

            //Create contact
            Contact contact1 = new Contact(
                    FirstName = 'Test',
                    LastName = 'McTesty',
                    AccountId = portalAccount1.Id,
                    Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);

            //Create Community user
            Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Case Logging Access' LIMIT 1];
            User user1 = new User(
                    Username = System.now().millisecond() + 'test12345@test.com',
                    ContactId = contact1.Id,
                    ProfileId = portalProfile.Id,
                    Alias = 'test123',
                    Email = 'test12345@test.com',
                    EmailEncodingKey = 'UTF-8',
                    LastName = 'McTesty',
                    CommunityNickname = 'test12345',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    LocaleSidKey = 'en_US',
                    LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
        }

        List<User> communityUserList = [
                SELECT Id, Name, ContactId, Contact.Persona__c, AccountId, ProfileId, (SELECT MemberId, NetworkId FROM NetworkMemberUsers WHERE Network.Status = 'Live' LIMIT 1)
                FROM User
                WHERE UserType != 'Standard' AND ProfileId != null
                AND IsPortalEnabled = true AND IsActive = true
                AND ContactId != null AND AccountId != null
                AND Id IN (SELECT MemberId FROM NetworkMember where Network.Status = 'Live')
                LIMIT 1
        ];

        System.assertEquals(false, communityUserList.isEmpty(), 'No community users found.');
        User communityUser = communityUserList[0];
        update communityUser;

        User thisUserOpAdmin = QTTestUtils.createMockOperationsAdministrator();
        System.runAs (thisUserOpAdmin) {
            //Create Contact
            Contact testContact2 = TestDataFactory.createContact('test_Thomas', 'test_Jones_Jr', 'testSandboxTJ2@qlikTech.com', '+44-8888888888', communityUser.AccountId);
            testContact2.Persona__c = 'Business Users';
            testContact2.LeadSource = 'Qlikmarket';
            insert testContact2;

            //User communityUser2 = communityUserList[1];

            if (communityUser.ContactId != null && communityUser.Contact.Persona__c == null) {
                Contact conUpdate = new Contact(Persona__c = 'Business User', Id = communityUser.ContactId);
                update conUpdate;
            }

            //Create Entitlement
            Entitlement productLicense = TestDataFactory.createLicense(portalAccount.Id, 'licenseReference', '5398347387423');
            insert productLicense;

            System.debug('productLicense.Id:' + productLicense.Id);
            selectedLicense = productLicense;

            //Create Environment
            Environment__c environment = TestDataFactory.createEnvironment(portalAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'Type', 'version');
            insert environment;

            selectedEnvironment = environment;

            EnvironmentProduct__c environmentProduct = new EnvironmentProduct__c();
            environmentProduct.Environment__c = environment.Id;
            environmentProduct.License__c = productLicense.Id;
            environmentProduct.Operating_System__c = 'Windows 95';
            environmentProduct.Product__c = 'SAP Connector';
            environmentProduct.Version__c = '5.80.2108';

            selectedEnvironmentProduct = environmentProduct;
        }
    }
}