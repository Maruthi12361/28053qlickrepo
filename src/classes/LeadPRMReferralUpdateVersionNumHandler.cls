/****************************************************************
*
*  LeadPRMReferralUpdateVersionNumHandler
*
*  Class to update the T&C Version field from the T&C Version number Custom field
*  We have to use a trigger for this as Workflow Rules only allow a heirachical 
*  Custom field to be used in formulas and we have a list type.	
*	
*  Changelog:
*
*   CR 7097 https://eu1.salesforce.com/a0CD000000U8WRR
*           Partner Lead sharing after submssion
*   2013-03-15  TJG Created
*   2013-03-26  TJG Addressed feedback about when ownership changes from queue to user
*                   the sharing needs to recreated.
*   2014-11-06  TJG Recreated from backup.
*   2017-10-25 AYS BMW-402 : Migrated from Lead_PRMReferral_UpdateVersionNum.trigger.
* 
*****************************************************************/

public class LeadPRMReferralUpdateVersionNumHandler {
	public LeadPRMReferralUpdateVersionNumHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld, Boolean isInsert) {
		System.Debug('Starting Lead_PRMReferral_UpdateVersionNum');
	    for (integer i = 0; i < triggerNew.size(); i++)
	    {
	    	if (isInsert)
	    	{
	    		if (triggerNew[i].Accepted_T_and_C__c && (triggerNew[i].RecordTypeId == '012D0000000K781IAC'))	//qttest=012L00000004JyJ live=012D0000000K781IAC
		        {   
				    QTCustomSettings__c MySetting = QTCustomSettings__c.getValues('Default');
			        if (MySetting == null) {System.Debug('No QTCustomSettings__c found'); continue; }   
			        triggerNew[i].PRM_Referral_T_C_Version__c = MySetting.PRM_Referral_TandC_Version_Num__c;
			        System.Debug('Lead_PRMReferral_UpdateVersionNum i= ' + i + ' Ver#= ' + MySetting.PRM_Referral_TandC_Version_Num__c);    
		        }
	    	}
	    	else
	    	{
		    	if (triggerNew[i].Accepted_T_and_C__c && !triggerOld[i].Accepted_T_and_C__c && (triggerNew[i].RecordTypeId == '012D0000000K781IAC'))	//qttest=012L00000004JyJ live=012D0000000K781IAC
		        {   
				    QTCustomSettings__c MySetting = QTCustomSettings__c.getValues('Default');
			        if (MySetting == null) {System.Debug('No QTCustomSettings__c found'); continue; }   
			        triggerNew[i].PRM_Referral_T_C_Version__c = MySetting.PRM_Referral_TandC_Version_Num__c;
			        System.Debug('Lead_PRMReferral_UpdateVersionNum i= ' + i + ' Ver#= ' + MySetting.PRM_Referral_TandC_Version_Num__c);    
		        } 
	    	}   
	    }
	    System.Debug('Finishing Lead_PRMReferral_UpdateVersionNum');
	}
}