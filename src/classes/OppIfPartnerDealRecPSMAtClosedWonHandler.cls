/*
Name:  OppIfPartnerDealRecPSMAtClosedWonHandler class

Purpose:
-------
CR# 6188 - Cleanup Opportunity Workflows
======================================================

History
------- 
VERSION AUTHOR        DATE        DETAIL
1.0     Roman@4front  2016-05-20  Migrated from OpportunityIfPartnerDealRecordPSMAtClosedWon.trigger.

  Records the Qonnect Manager ID at Closed Won
  There was a Workflow Rule called "if partner deal record PSM at Closed Won" which this trigger intends
  to duplicate so we can deactivate the workflow and thus free up some Opp Object references as we
  have run out.
  Original Workflow:
  Rule Name:        if partner deal record PSM at Closed Won
  Object:          Opportunity
  Evaluation Criteria:  Evaluate the rule when a record is created, and any time it’s edited to subsequently meet criteria
  Description:      records the Qonnect Manager ID at closed won
  Rule Ctriteria:      (Opportunity: StageEQUALSDeal Split-closed Won,OEM - Closed Won,ER - Closed Won,Closed Won,QSG Closed Won,OEM Pre-Pay Closed Won) AND (Opportunity: Qonnect Manager at closeEQUALSnull)
  
  Field Update:
  Name:          Opp: Add QM ID to QM at close field
  Unique Name:      Opp_Add_QM_ID_to_QM_at_close_field
  Description:      records the Qonnect Manager ID at closed won
  Object:          Opportunity
  Field to Update:    Opportunity: Qonnect Manager at close
  Field Data Type:    Text
  Re-eval after change:  Yes
  Formula Value:      BLANKVALUE(Sell_Through_Partner__r.Qonnect_Manager__r.Id, Referring_Contact__r.Account.Qonnect_Manager__c)
  
  Field Update:
  Name:          Opp: Add Qonnect Manager Name
  Unique Name:      Opp_Add_Qonnect_Manager_Name
  Description:      Since the Qonnect Manager at close provides just the user ID, this will show the users name.
  Object:          Opportunity
  Field to Update:    Opportunity: Qonnect Manager Name at Closed Won
  Field Data Type:    Text Area
  Re-eval after change:  No
  Formula Value:      IF( Sell_Through_Partner__c <> "",
              Sell_Through_Partner__r.Qonnect_Manager__r.FirstName & " " & Sell_Through_Partner__r.Qonnect_Manager__r.LastName,
              Referring_Contact__r.Account.Qonnect_Manager__r.FirstName & " " & Referring_Contact__r.Account.Qonnect_Manager__r.LastName)
  *  2017-06-14 Rodion Vakulvoskyi added static flag to get rid of soql query limit
  *  2017-06-22 MTM  QCW-2711 remove sharing option
*/

public class OppIfPartnerDealRecPSMAtClosedWonHandler {
    public static Boolean FlagForInsert = true;
	public OppIfPartnerDealRecPSMAtClosedWonHandler() {
		
	}

	public static void handle(List<Opportunity> triggerNew, List<Opportunity> triggerOld, Map<Id, Opportunity> triggerOldMap, Boolean isInsert, Boolean isUpdate) {
    System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon trigger: Starting');
      
    /* KMH- Commented and rewritten below  
      //CR# 7841 
      Set<Id> accountIds = new Set<Id>();
      for(Opportunity op : Trigger.new) accountIds.add(op.AccountId);
      
      Map<Id, Account> acctMap = new Map<Id, Account>(
        [SELECT Id, Account_EIS_New__c
         FROM Account WHERE Id in :accountIds ]);
        
      Set<Account> accountsToUpdate = new Set<Account>();  
      for(Opportunity op : Trigger.new) {
        Boolean updatingOp = Trigger.isUpdate && op.Active_EIS__c != Trigger.oldMap.get(op.Id).Active_EIS__c;    
        Account acc = acctMap.get(op.AccountId);      
        if(acc != null && acc.Account_EIS_New__c != null) {
          op.Active_EIS__c = acc.Account_EIS_New__c;
        } else if(acc != null) {
          acc.Account_EIS_New__c = op.Active_EIS__c ; 
          accountsToUpdate.add(acc);
        }
      }
      List<Account> accountsList = new List<Account>();
      accountsList.AddAll(accountsToUpdate);
      update accountsList;
      */
        for (Opportunity opp : triggerNew)  
        {
            if (Opp.Split_Opportunity__c == true && ((opp.StageName == 'Closed Won') || (opp.StageName == 'OEM - Closed Won') || (opp.StageName == 'OEM Pre-Pay Closed Won') || (opp.StageName == 'Deal Split-closed Won') || (opp.StageName == 'ER - Closed Won') || (opp.StageName == 'QSG Closed Won') )) 
            {
                Opp.ForecastCategoryName = 'Omitted';
            }
        }   
          
          //CR# 7841 
        Set<Id> accountIds = new Set<Id>();
        //for(Opportunity op : Trigger.new) accountIds.add(op.AccountId);
        
        for(Integer i = 0; i<triggerNew.size(); i++)
        {
            if (isInsert)
            {
              accountIds.add(triggerNew[i].AccountId);
            }
            if(isUpdate)
            {
              if( (triggerNew[i].Active_EIS__c != triggerOld[i].Active_EIS__c ) || (triggerNew[i].Active_EIS__c ==null)||
              (triggerNew[i].Account_ISER__c ==null))
              {
                accountIds.add(triggerNew[i].AccountId);
              }
            }
        }
        if (accountIds.size() > 0)
        {
            Map<Id, Account> acctMap = new Map<Id, Account>(
            [SELECT Id, Account_EIS_New__c, Account_ISER__c
             FROM Account WHERE Id in :accountIds ]);
         
            for(Opportunity op : triggerNew) {
            Boolean updatingOp = isUpdate && op.Active_EIS__c != triggerOldMap.get(op.Id).Active_EIS__c;       
            Account acc = acctMap.get(op.AccountId);            
            if(acc != null && acc.Account_EIS_New__c != null) 
            {
                op.Active_EIS__c = acc.Account_EIS_New__c;        
            }
            
            if(acc != null && acc.Account_ISER__c != null) 
            {
               op.Account_ISER__c = acc.Account_ISER__c;
            }
            
           }       
        }
        
      
      Set<Opportunity> OpportunityIds = new Set<Opportunity>();
      for (Opportunity opp : triggerNew)
      {
        if ((opp.Qonnect_Manager_at_close__c == null) && ((opp.StageName == 'Closed Won') || (opp.StageName == 'OEM - Closed Won') || (opp.StageName == 'OEM Pre-Pay Closed Won') || (opp.StageName == 'Deal Split-closed Won') || (opp.StageName == 'ER - Closed Won') || (opp.StageName == 'QSG Closed Won') ))
        {
          OpportunityIds.Add(opp);  //Add the Opp we want to check
        }
      }
      
      
      if (OpportunityIds.size() > 0)
      {
        //If we get here we have at least one Opp that needs updating    
        //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon trigger: we have something to update');
        
        List<string> AccIDs = new List<string>();  //List of Account Id's
         List<Id> ConIDs = new List<Id>();  //List of Contact Id's
         for (Opportunity Opp : OpportunityIds)
         {
           //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon Opp= ' + Opp.Id);
          //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon Opp.Sell_Through_Partner__c= ' + Opp.Sell_Through_Partner__c);
          //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon Opp.Referring_Contact__c= ' + Opp.Referring_Contact__c);
        
           AccIDs.Add(Opp.Sell_Through_Partner__c);   //Add the Sell Through Partner Account Id
           ConIDs.Add(Opp.Referring_Contact__c);   //Add the Contact Id  
         }
         //Create and populate a Map with the Account information we will need  
        Map<Id, Account> OppToAcc = New Map<Id, Account>([Select Id, Qonnect_Manager__c, Qonnect_Manager__r.Name FROM Account WHERE Id In :AccIDs]);
            //Create and populate a Map with the Contact information we will need  
        Map<Id, Contact> ConToAcc = New Map<Id, Contact>();
        if (ConIDs.size() > 0)
        {
          for (Contact c : [Select c.Id, c.AccountId, c.Account.Qonnect_Manager__c FROM Contact c WHERE Id In :ConIDs])
          {
            ConToAcc.put(c.Id, c);
          }
        }
            
            //Create and populate a Map with the User Fullname from a Qonnect Manager Id    
            Map<Id, Id> QMIdToAQM = New Map<Id, Id>();
            Map<Id, string> UsrIdToName = New Map<Id, string>();
            if (ConToAcc.size() > 0)
            {
              for (Id coId : ConToAcc.keySet())
           {
                //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon QMIdToAQM= ' + ConToAcc.get(coId).Id);
            //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon QMIdToAQM= ' + ConToAcc.get(coId).Account.Qonnect_Manager__c);
            QMIdToAQM.put(ConToAcc.get(coId).Id, ConToAcc.get(coId).Account.Qonnect_Manager__c);
              }  
              
              for (User u : [select u.Id, u.Name from User u where Id in:QMIdToAQM.values()]){          
                //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon UsrIdToName= ' + u.Id);
            //System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon UsrIdToName= ' + u.Name);
            UsrIdToName.put(u.Id, u.Name);
              }          
            }
            
            
            //We need to update the 'Qonnect Manager at close' field
        //If 'Sell_Through_Partner__r.Qonnect_Manager__r.Id' is not blank then we use that value otherwise we 
        //use the value in 'Referring_Contact__r.Account.Qonnect_Manager__c'
        
        for (Opportunity Opp : OpportunityIds)
         {
           if ((Opp.Sell_Through_Partner__c != null) && (OppToAcc.get(Opp.Sell_Through_Partner__c).Qonnect_Manager__c != null))
          {
            Opp.Qonnect_Manager_at_close__c = OppToAcc.get(Opp.Sell_Through_Partner__c).Qonnect_Manager__c;
            //Opp.Test_Id__c = OppToAcc.get(Opp.Sell_Through_Partner__c).Qonnect_Manager__c;  //for testing only        
          }
          else
          {
            if (Opp.Referring_Contact__c != null)
            {
              Opp.Qonnect_Manager_at_close__c = ConToAcc.get(Opp.Referring_Contact__c).Account.Qonnect_Manager__c;
              //Opp.Test_Id__c = ConToAcc.get(Opp.Referring_Contact__c).Account.Qonnect_Manager__c;  //for testing only        
            }
          }
         }
      
        //We also need to update 'Qonnect Manager Name at Closed Won' field. 
        //If 'Sell_Through_Partner__c' is not blank then then we use 'Sell_Through_Partner__r.Qonnect_Manager__r.FirstName' and
        //'Sell_Through_Partner__r.Qonnect_Manager__r.LastName' otherwise we use 'Referring_Contact__r.Account.Qonnect_Manager__r.FirstName'
        //and 'Referring_Contact__r.Account.Qonnect_Manager__r.LastName'
        
        for (Opportunity Opp : OpportunityIds)
         {
           if (Opp.Sell_Through_Partner__c != null)
          {
            Opp.Qonnect_Manager_Name_at_Closed_Won__c = OppToAcc.get(Opp.Sell_Through_Partner__c).Qonnect_Manager__r.Name;
            //Opp.Test__c = OppToAcc.get(Opp.Sell_Through_Partner__c).Qonnect_Manager__r.Name;  //for testing only        
          }
          else
          {
            if (Opp.Referring_Contact__c != null)
            {
              Opp.Qonnect_Manager_Name_at_Closed_Won__c = UsrIdToName.get(QMIdToAQM.get(Opp.Referring_Contact__c));
              //Opp.Test__c = UsrIdToName.get(QMIdToAQM.get(Opp.Referring_Contact__c));  //for testing only          
            }
          }
         }
      }
      
    System.debug('OpportunityIfPartnerDealRecordPSMAtClosedWon trigger: Finishing');
	}
}