/**
* Change log:
* 
* 2019-03-04    ext_vos CHG0035184: update article link. Add attached objects from Coveo: articles and YT and Community links.
* 2019-05-05    AIN IT-1597 Updated for Support Portal Redesign
*/
public without sharing class QS_CaseArticleListController {

    public static final String ARTICLE_URL = 'articles/';
    public static final String ARTICLE_ID_INDEX = '/kA';
    public static final List<String> ARTICLE_SOURCES = new List<String>{'KB', 'Salesforce Knowledge'};
    public static final List<String> LINK_SOURCES = new List<String>{'Lithium Community', 'Qlik Support Youtube', 'Qlik Help'};
    public static final String ERROR_MESSAGE = 'Something went wrong in retrieving the related content. Please refresh the page and try again.';

    public Id contentDocumentParentId {get; set;}
    public Boolean showException {get; set;}
    public String errorMessageText {get; set;}
    public List<AttachedResultWrapper> attachResults;
    
    public QS_CaseArticleListController() {    
    }

    public List<AttachedResultWrapper> getAttachResults() {
        attachResults = new List<AttachedResultWrapper>(); 
        attachResults.addAll(getCaseArticleList());
        attachResults.addAll(getCaseAttachedResultList());
        return attachResults;
    }
    
    public List<AttachedResultWrapper> getCaseArticleList() {
        List<AttachedResultWrapper> articles = new List<AttachedResultWrapper>();
        if (contentDocumentParentId != null) {
            try {
                List<CaseArticle> caseArticles = [select KnowledgeArticleId from CaseArticle where CaseId = :contentDocumentParentId];
                List<Id> caseArticleIds = new List<Id>();
                for (CaseArticle ca : caseArticles) {
                    caseArticleIds.Add(ca.KnowledgeArticleId);
                }
                List<KnowledgeArticleVersion> caseArticleList = [select Id, Title, Summary, LastPublishedDate, IsVisibleInPkb, ArticleNumber 
                                                                    from KnowledgeArticleVersion 
                                                                    where PublishStatus = 'Online' and Language ='en_US' and KnowledgeArticleId in :caseArticleIds];
                for (KnowledgeArticleVersion art : caseArticleList) {
                    articles.add(new AttachedResultWrapper(art.Title, ARTICLE_URL + art.ArticleNumber));
                }
            } catch (Exception ex) {
                showException = true;
                errorMessageText = ERROR_MESSAGE;
            }
        }
        return articles;   
    }

    private List<AttachedResultWrapper> getCaseAttachedResultList() {
        List<AttachedResultWrapper> links = new List<AttachedResultWrapper>();        
        if (contentDocumentParentId != null) {            
            try {
                List<CoveoV2__CoveoCaseAttachedResult__c> attach = [select CoveoV2__ResultUrl__c, CoveoV2__Source__c, CoveoV2__Title__c 
                                                                    from CoveoV2__CoveoCaseAttachedResult__c 
                                                                    where CoveoV2__case__c = :contentDocumentParentId];
                List<Id> kaIds = new List<Id>();
                for (CoveoV2__CoveoCaseAttachedResult__c att : attach) {
                    if (LINK_SOURCES.contains(att.CoveoV2__Source__c)) {
                        links.add(new AttachedResultWrapper(att.CoveoV2__Title__c, att.CoveoV2__ResultUrl__c));
                    } else if (ARTICLE_SOURCES.contains(att.CoveoV2__Source__c)) {
                        String url = att.CoveoV2__ResultUrl__c;
                        Integer ind = url.indexOf(ARTICLE_ID_INDEX);
                        if (ind != -1) {
                            String kaId = url.substring(ind + 1, url.length());
                            if (String.isNotEmpty(kaId)) {
                                kaIds.add(kaId);
                            }
                        }
                    }
                }
                List<KnowledgeArticleVersion> caseArticleList = [select Id, Title, Summary, LastPublishedDate, IsVisibleInPkb, ArticleNumber 
                                                                    from KnowledgeArticleVersion 
                                                                    where PublishStatus = 'Online' and Language ='en_US' and KnowledgeArticleId in :kaIds];
                String existedArticles = '';
                for (AttachedResultWrapper att : attachResults) {
                    existedArticles += att.url + ',';
                }
                for (KnowledgeArticleVersion art : caseArticleList) {
                    if (!existedArticles.contains(art.ArticleNumber)) {
                        links.add(new AttachedResultWrapper(art.Title, ARTICLE_URL + art.ArticleNumber));
                    }
                }
            } catch (Exception ex) {
                showException = true;
                errorMessageText = ERROR_MESSAGE;
            }
        }
        return links;
    }

    public class AttachedResultWrapper {
        public String id {get;set;}
        public String title {get;set;}
        public String url {get;set;}

        public AttachedResultWrapper(String title, String url) {
            this.title = title;
            this.url = url;
        }
    }
}