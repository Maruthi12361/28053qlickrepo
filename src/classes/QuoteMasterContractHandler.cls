/****************************************************************
*
*  QuoteMasterContractHandler
*
*  2016-08-04 IRN: Q2CW, handler for setting correct master contract on a quote.
*  2017-02-13 IRN: Q2CW, rewrote this class due to new requirement
*  2017-09-27 UIN: Introduced new method for async schedule deletion and opp line update
*  2017-09-27 UIN: Commented out old unused method
*  2018-04-10 DKF: BSL-1860. Commented out the previous class and update with the class originally written on QCW-4155 to work with the CPQ upgrade. 
*****************************************************************/
@RestResource(urlMapping='/Q2CWCalculator/*')
global class QuoteMasterContractHandler {

    @HttpPost
    global static void processRequest() {
        try {
            RestRequest req = RestContext.request;
            String uriTail = req.requestURI.substringAfter('/Q2CWCalculator/');
            String method = uriTail.substringBefore('/');
            uriTail = uriTail.substringAfter('/');
            IPostMethod ipm = sPostMethods.get(method);
            if (null == ipm) {
                throw new CPQException('Unknown method: ' + method);
            }
            ipm.ExecuteREST(uriTail);
        } catch (Exception e) {
            System.debug(logginglevel.ERROR,'Line: ' + String.valueOf(e.getLineNumber())+ '\nStack:' + e.getStackTraceString()+ '\nMessage: ' + e.getMessage());
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object>{'ErrorText' => e.getMessage()}));
            RestContext.response.statusCode = 500;
        }
    }

    global virtual class CPQException extends Exception {}

    global class ReturnData {
        public String ErrorText;
        public String RevenueType;
        public String SellThroughPartner;
        public String PaymentTerms;
        public String BillingAddress;
        public String ShippingAddress;
        public String MasterContract;
        public String MaintenanceLevel;
        public String MaintenancePercentage;
        public String Support_ProvidedByQlik;
        public String SubscriptionTerm;
        public Date StartDate;
        public String ShortDescription;
        public String PartnerContact;
        public String Distributor;
        public String OppUrl;
    }

    //Viktor@front QCW-4155 - this method can be used anywhere from Apex directly
    //the logic inside was not modified
    global static ReturnData OnBeforeCalculate(String allAssetIds, String quoteId) {
        ReturnData returnData = new ReturnData();

        if (allAssetIds != null && allAssetIds != '') {
            String [] assetIds = allAssetIds.split(',');

            List<Sales_Channel__c> saleschannels = [SELECT Id, Contract_End_Date__c, Latest_Contract__c, Revenue_Type__c, Sell_Through_Partner__c, Support_Level__c, Support_Percentage__c, Support_Provided_by__c, Support_Provided_By_Type__c FROM Sales_Channel__c WHERE IsDeleted = false AND Id IN (SELECT Sales_Channel__c FROM Asset WHERE Id IN :assetIds)];
            List<SBQQ__Quote__c> quotes = [SELECT Id, Payment_Terms_Days_from_Account__c, SBQQ__Opportunity2__r.Id, SBQQ__Opportunity2__r.Short_Description__c, SBQQ__Opportunity2__r.Partner_Contact__c, SBQQ__Opportunity2__r.Second_Partner__c, SBQQ__Opportunity2__r.Revenue_Type__c FROM SBQQ__Quote__c WHERE Id = :quoteId];

            System.debug('saleschannels ' + saleschannels);

            if (saleschannels.size() == 1) {
                Sales_Channel__c s = saleschannels[0];
                if (s.Revenue_Type__c != quotes[0].SBQQ__Opportunity2__r.Revenue_Type__c) {
                    returnData.ErrorText = 'The Revenue Type on the Opportunity does not match the selected Asset(s). You need to return to the Opportunity to correct the Revenue Type or select different Asset(s). Please click the OK button below, this will take you back to the Opportunity.';
                    returnData.OppUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + quotes[0].SBQQ__Opportunity2__r.Id;
                    //delete quotes[0];
                } else {
                    returnData.ErrorText = 'Ok';
                }

                if (s.Contract_End_Date__c != Null) {
                    Integer monthsBetween = Date.today().monthsBetween(s.Contract_End_Date__c);
                    returnData.SubscriptionTerm = monthsBetween + '';
                }

                returnData.RevenueType = s.Revenue_Type__c;
                returnData.SellThroughPartner = s.Sell_Through_Partner__c;
                returnData.Support_ProvidedByQlik = s.Support_Provided_By_Type__c;
                returnData.MasterContract = s.Latest_Contract__c;
                returnData.MaintenancePercentage = integer.valueof(s.Support_Percentage__c) + '%';
                returnData.MaintenanceLevel = s.Support_Level__c;

                if (quotes.size() > 0) {
                    returnData.PaymentTerms = String.valueOf(quotes[0].Payment_Terms_Days_from_Account__c);
                    returnData.ShortDescription = quotes[0].SBQQ__Opportunity2__r.Short_Description__c;
                    returnData.PartnerContact = quotes[0].SBQQ__Opportunity2__r.Partner_Contact__c;
                    returnData.Distributor = quotes[0].SBQQ__Opportunity2__r.Second_Partner__c;
                }

                List<Asset> assets = [Select AccountId from Asset where Id in:assetIds];
                Asset asset;
                if (assets.size() > 0) {
                    asset = assets[0];
                }

                if (asset != null) {
                    Set<String> revenueTypeSellThroughPartner = new Set<String>{
                        'Reseller', 'Fulfillment', 'Distributor', 'Partner Purchase', 'Not For Resale', 'Reseller Services-Deferred', 'Reseller QSG'
                    };
                    if (revenueTypeSellThroughPartner.contains(quotes[0].SBQQ__Opportunity2__r.Revenue_Type__c)) {
                        setDefautAddresses(returnData, s.Sell_Through_Partner__c);
                    } else {
                        setDefautAddresses(returnData, asset.AccountId);
                    }
                }
                Date startDate = Date.today();
                startDate = startDate.toStartOfMonth().AddMonths(1);
                returnData.StartDate = startDate;
                return returnData;
            } else if (saleschannels.size() > 1) {
                returnData.ErrorText = 'Too many saleschannels';
                return returnData;
            }
        }
        return returnData;
    }

    //Viktor@front QCW-4155 - moved from OnInit
    //the logic inside was not modified
    global static ReturnData OnAfterCalculate(String quoteId, Double netAmount) {
        ReturnData returnData = new ReturnData();
        List<OpportunityLineItemSchedule> itemSchedToDelete = new List<OpportunityLineItemSchedule>();

        //Get OppId from Quote
        List<SBQQ__Quote__c> quotes = new List<SBQQ__Quote__c>();
        quotes = [SELECT Id, SBQQ__NetAmount__c, SBQQ__Type__c, SBQQ__Opportunity2__r.Id FROM SBQQ__Quote__c WHERE Id = :quoteId];

        //Get Line Items Using oppId
        Boolean deleteLines = false;
        //UIN NEW CHANGES
        List<OpportunityLineItem> dbLineItems = new List<OpportunityLineItem>();

        //UIN NEW CHANGES
        if (!quotes.isEmpty()) {
            dbLineItems = [SELECT Id,Quantity,ProductCode,product2.SBQQ__SubscriptionType__c,Test_Opp_Schedule_Deleted__c From OpportunityLineItem WHERE OpportunityId = :quotes[0].SBQQ__Opportunity2__r.Id];
            if ((quotes[0].SBQQ__NetAmount__c).format() != netAmount.format() || quotes[0].SBQQ__Type__c == 'Amendment') {
                deleteLines = true;
            }
            if (deleteLines) {
                //Get OppSchedules
                for (OpportunityLineItemSchedule scheduleItem : [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId IN :dbLineItems]) {
                    itemSchedToDelete.add(scheduleItem);
                }

                if (itemSchedToDelete.size() > 0) {
                    delete itemSchedToDelete;
                }

                //UIN NEW CHANGES
                List<OpportunityLineItem> updLineItems = new List<OpportunityLineItem>();
                for (OpportunityLineItem lItem : dbLineItems) {
                    if (lItem.product2.SBQQ__SubscriptionType__c == 'Renewable' && !lItem.Test_Opp_Schedule_Deleted__c) {
                        lItem.Test_Opp_Schedule_Deleted__c = true;
                        updLineItems.add(lItem);
                    }
                }

                if (!updLineItems.isEmpty()) {
                    update updLineItems;
                }
                //UIN NEW CHANGES
            }
        }
        return returnData;
    }

    public static void setDefautAddresses(ReturnData returnData, String accountId) {
        Map<String, Address__c> mapOfAddresses = new Map<String, Address__c>();
        for (Address__c itemAddress : [SELECT Id, Name, Account__c, Address_Type__c FROM Address__c WHERE Default_Address__c = true AND Account__c = :accountId]) {
            if (itemAddress.Address_Type__c == 'Billing') {
                returnData.BillingAddress = itemAddress.Id;
            }
            if (itemAddress.Address_Type__c == 'Shipping') {
                returnData.ShippingAddress = itemAddress.Id;
            }
        }
    }

    ////////////////////////////////// PRIVATE /////////////////////////////////////

    private interface IPostMethod {
        void ExecuteREST(String inUriTail);
    }

    private class CBeforeCalculate implements IPostMethod {
        public void ExecuteREST(String inUriTail) {
            Map<String, Object> inData = (Map<String, Object>) JSON.deserializeUntyped(RestContext.request.requestBody.toString());
            String quoteId = (String) inData.get('quoteId');
            String allAssetIds = (String) inData.get('allAssetIds');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(OnBeforeCalculate(allAssetIds, quoteId)));
            RestContext.response.statusCode = 200;
        }
    }

    private class CAfterCalculate implements IPostMethod {
        public void ExecuteREST(String inUriTail) {
            Map<String, Object> inData = (Map<String, Object>) JSON.deserializeUntyped(RestContext.request.requestBody.toString());
            String quoteId = (String) inData.get('quoteId');
            Double netAmount = (Double) inData.get('netAmount');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(OnAfterCalculate(quoteId, netAmount)));
            RestContext.response.statusCode = 200;
        }
    }

    private static Map<String, IPostMethod> sPostMethods {
        set;
        get {
            if (null == sPostMethods) {
                sPostMethods = new Map<String, IPostMethod> {'beforeCalculate' => new CBeforeCalculate(), 'afterCalculate' => new CAfterCalculate()};
            }
            return sPostMethods;
        }
    }
}