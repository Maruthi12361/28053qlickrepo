/****************************************************************
*
*  OppOnlyBdrManagerCanChangeBdrGdHandler
*
*   This class prevents Users without the 'BDR_Managers' permission set from changing the Stage
*   from 'Goal Discovery' on Opps that have been created by BDRs (as defined by the Opportunity field: "Original Created by Function")
*
*	2016-05-19 TJG : Migrated from the following trigger
					Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery
*  2017-06-22 MTM  QCW-2711 remove sharing option
*****************************************************************/
public class OppOnlyBdrManagerCanChangeBdrGdHandler {
	static final String DEBUGPREFIX = 'OppOnlyBdrManagerCanChangeBdrGdHandler: ';
	// to be called on event before Update
	public static void handle(List<Opportunity> triggerOld, List<Opportunity> triggerNew) {
	    
	    System.Debug(DEBUGPREFIX + 'Starting');
	    
	    if (!Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscovery'))
	    {
	        
	        User currentUserFunction = UserHelper.getExtendedUser();
	        string cSF = currentUserFunction.Function__c;

	        //List<PermissionSetAssignment> psAss = [select AssigneeId, PermissionSetId from PermissionSetAssignment where PermissionSetId = '0PSD0000000X842OAC']; //BDR_Managers
	        boolean NoBDRManagers = false;

	        //Get a Set of Users that have been assigned the BDR_Managers permission set
	        PermissionSet ps = [SELECT Id, Name, (SELECT AssigneeId FROM Assignments) FROM PermissionSet WHERE Name='BDR_Managers' Limit 1];
	        List<PermissionSetAssignment> psAss = ps.Assignments;       
	        if (psAss.size() == 0) {
	            NoBDRManagers = true;
	            System.Debug(DEBUGPREFIX + 'No Users have been assigned the BDR_Managers permission set.');
	        }
	        
	        Set<Id> BDRManagers = new Set<Id>();
	        for (PermissionSetAssignment p : psAss)
	        {
	            BDRManagers.add(p.AssigneeId);
	        }
	        System.Debug(DEBUGPREFIX + 'BDRManagers.size() = ' + BDRManagers.size() + ' BDRManagers = ' + BDRManagers);
	        for(integer i=0; i< triggerNew.size(); i++)
	        {
	            //Check if the Opp was created by a BDR or iBDR (as defined in the "Original Created by Function" field) - the following is only relevant if it was
	            System.Debug(DEBUGPREFIX + 'Current User Function = ' + cSF);
	            //System.Debug(DEBUGPREFIX + 'Original_Created_by_Function__c = ' + triggerNew[i].Original_Created_by_Function__c);
	            if (String.isNotBlank(cSF) && cSF.contains('BDR')) //test for BDR also covers iBDR
	            //if (String.isNotBlank(triggerNew[i].Original_Created_by_Function__c) && triggerNew[i].Original_Created_by_Function__c.contains('BDR')) //test for BDR also covers iBDR
	            {
	                if (NoBDRManagers)
	                {
	                    triggerNew[i].addError(DEBUGPREFIX + 'There are no BDR_Managers permission sets assigned - Please contact your Systems Administrator.');
	                }
	                else
	                {
	                    System.Debug(DEBUGPREFIX + ': triggerOld[i].StageName = ' + triggerOld[i].StageName + ' triggerNew[i].StageName = ' + triggerNew[i].StageName);
	                    //check if the StageName has changed from "Goal Discovery" to a non-allowed value
	                    if (((triggerOld[i].StageName == 'Goal Discovery') && (triggerNew[i].StageName != 'Goal Discovery') || 
	                        (triggerOld[i].StageName == 'Goal Identified') && (triggerNew[i].StageName != 'Goal Identified')) &&
	                        (!(triggerNew[i].StageName == 'Goal Discovery' || triggerNew[i].StageName == 'Goal Identified' || triggerNew[i].StageName == 'Goal Rejected')))
	                    {
	                        //it has, so now check if the user is allowed to change it - do they have the BDR_Managers permission set assigned to them?
	                        if (!BDRManagers.contains(UserInfo.getUserId()))
	                        {
	                            triggerNew[i].addError(DEBUGPREFIX + 'BDRs can only move the Opp to Goal Identified or Goal Rejected stage.');
	                        }
	                        System.Debug(DEBUGPREFIX + ': 1');
	                    }
	                    //check if the StageName has changed from Goal Rejected
	                    else if (triggerOld[i].StageName == 'Goal Rejected' && triggerNew[i].StageName != 'Goal Rejected')
	                    {
	                        //it has, so now check if the user is allowed to change it - do they have the BDR_Managers permission set assigned to them?
	                        if (!BDRManagers.contains(UserInfo.getUserId()))
	                        {
	                            triggerNew[i].addError(DEBUGPREFIX + 'Only BDR Managers can move the Opp out of Goal Rejected stage.');
	                        }
	                        System.Debug(DEBUGPREFIX + ': 2');
	                    }
	                }
	            }           
	        }       
	    }
	    System.Debug(DEBUGPREFIX + 'Finishing');		
	}
}