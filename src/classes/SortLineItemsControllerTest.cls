/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 * Log of Changes:
 * 2012-02-15	RDZ		Adding to the opp an Account that has Billing Country code and Qliktech company
 *						as the validation rule makes this test to fail.
 * 07.02.2017   RVA :   changing methods 
 */
@isTest
private class SortLineItemsControllerTest {

    static testMethod void myUnitTest() {
        System.debug('SortLineItemsControllerTest.myUnitTest: Starting tests'); 
        
        //RDZ  ~Adding and account with BillingCountryCode and Qliktech Company
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id			
		);
		insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account acc = new Account(Name = 'Test',
		// Needs a Billing and Shippin Addr
		QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
		Billing_Country_Code__c = QTComp.Id
		);
		
		insert acc;

        QTCustomSettings__c Settings = new QTCustomSettings__c();
        Settings.Name = 'Default';
        Settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        Settings.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        Settings.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        Settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert Settings;
        QuoteTestHelper.createCustomSettings();
        Opportunity opp = new Opportunity(
        	name = 'Test Opportunity',
        	StageName = 'Goal Shared',
            ForecastCategoryName = 'Omitted',   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        	CloseDate = Date.today().addDays(30),
        	AccountId = acc.Id	
        );
        insert opp;
        
        System.debug('SortLineItemsControllerTest.myUnitTest: Created opportunity');
        
        List<Sequence_of_Event__c> listSoE = new List<Sequence_of_Event__c>();
        for (integer i=0;i<10;i++) {
        	
        	Sequence_of_Event__c SoE = new Sequence_of_Event__c(
        			Type__c = 'Customer', 
        			S_o_E_Date__c = Date.today().addDays(-i), 
        			Proposed_Event__c = 'Other',
        			Responsibility__c = 'Responsibility ' + i,
        			Opportunity_ID__c = opp.Id
        			        			 
        		);
        	
        	listSoE.add(SoE);
        }
        insert listSoE;

		System.debug(opp);         
        System.debug('SortLineItemsControllerTest.myUnitTest: Created related Sequence of Events');
        
        try {
        	test.startTest();
        
        	SortLineItemsController slic = new SortLineItemsController();
        	slic.opportunity = opp;
        	slic.Sequence_of_Event = listSoE;
			System.debug('SortLineItemsControllerTest.myUnitTest: Opportunity id = ' +  slic.opportunity.ID);
        	System.debug('SortLineItemsControllerTest.myUnitTest: Has ' +  slic.opportunity.Sequence_of_events__r.size() + ' records in Opportunity.SequenceOfEvents list');
        	System.debug('SortLineItemsControllerTest.myUnitTest: Has ' +  slic.Sequence_of_Event.size() + ' records in SequenceOfEvent list');
        	List<Sequence_of_Event__c> returns = slic.getSorted();
        	System.debug('SortLineItemsControllerTest.myUnitTest: Got ' + returns.size() + ' sequences in return from getSorted()');
        	
        	integer i = 9;
        	for (Sequence_of_Event__c SoE: returns) {
        		System.debug('SortLineItemsControllerTest.myUnitTest:  Comparing ' + SoE.S_o_E_Date__c + ' with ' + Date.today().addDays(-i));
        		System.assertEquals(SoE.S_o_E_Date__c, Date.today().addDays(-i));
        		
        		i--;
        	}
        
        } catch (System.Exception e) {
        	System.debug('SortLineItemsControllerTest.myUnitTest: Cought exception ' + e.getDmlMessage(0));
        }
        
        test.stopTest();
        System.debug('SortLineItemsControllerTest.myUnitTest: Tests done');
    }
}