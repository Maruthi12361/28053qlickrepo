/**
* Class: Test class for SBQQQuoteSetUpdIndSectorFuncTest.
*
* Changelog:
*	 2017-02-08 : Ramakrishna Kini: Initial development.
*    2017-03-29 : Roman Dovbush  : duplicated createCustomSettings() deleted. We call it from QTTestUtils.GlobalSetUp();
*   2017-08-09 : Aslam Kamal QCW-2934 Code failure fix
*   02.09.2017 : Srinivasan PR- fix for query error
*   12-09-2017 : Linus Löfberg Q2CW-2953 setting quote recipient to conform with added validation conditions.
*   18-04-2018 : AIN BSL-10 fix
*   14-02-2019 : AIN Fix soql to use like to get unique records.
*/

@isTest
private class SBQQQuoteSetUpdIndSectorFuncTest {

	static final String AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id; //'01220000000DOFu';

	@testSetup static void testSetup() {
		QTTestUtils.GlobalSetUp();
		QTTestUtils.SetupNSPriceBook('USD');
		//QuoteTestHelper.createCustomSettings(); //duplicate

		Profile p = [select id from profile where name='System Administrator'];
		User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com',
        INT_NetSuite_InternalID__c = '5307');

		User u1 = new User(alias = 'test', email='standardtestuser@testorg.com',
        emailencodingkey='UTF-8', lastname='TestingNSId', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standardtestuser@hourserr.com');
        insert u1;

        System.runAs(u) {
        	SIC_Code__c code = new SIC_Code__c(Name ='8732:Commercial Economic, Sociological, and Educational Research', Industry__c='Services', QlikTech_Sector__c='Retail & Services');
			insert code;

			SIC_Code__c code2 = new SIC_Code__c(Name ='6022:State Commercial Banks', Industry__c='Banking', QlikTech_Sector__c='Financial Services');
			insert code2;

			Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
			testAccount.Name = 'Test Val Rule 1';
	        testAccount.SIC_Code_Name__c = code.Id;
	        update testAccount;

	        Account  testAccount2 = QTTestUtils.createMockAccount('TestCompany2', u, true);
	        testAccount2.RecordtypeId = AccRecordTypeId_EndUserAccount;
			testAccount2.Name = 'Test Val Rule 2';
			testAccount2.SIC_Code_Name__c = code2.Id;
	        update testAccount2;

			List<QlikTech_Company__c> companyQT = [Select Id, QlikTech_Company_Name__c, Country_Name__c From QlikTech_Company__c Where Country_Name__c = 'France'];
			System.assertEquals('France', companyQT[0].Country_Name__c);

	        Contact testContact = QTTestUtils.createMockContact(); // partner contact, lastname = 'RDZTestLastName'
	       	Contact testContact1 = new Contact(); // typical account contact;
	       	testContact1.FirstName = 'Roman';
	       	testContact1.Lastname = 'Dovbush';
	       	testContact1.Email = 'test@test.com';
	       	testContact1.HasOptedOutOfEmail = false;
	       	testContact1.AccountId = testAccount.Id;
	       	insert testContact1;

			Contact testContact2 = new Contact(); // typical account contact;
            testContact2.FirstName = 'Roman';
            testContact2.Lastname = 'Dovbush2';
            testContact2.Email = 'test@test2.com';
            testContact2.HasOptedOutOfEmail = false;
            testContact2.AccountId = testAccount2.Id;
            insert testContact2;

	        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
												where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error

	        Account testPartnerAccount = QuoteTestHelper.createAccount(companyQT[0], rTypeAcc2, 'Partner'); // acc name = 'PartnerAcc'
	        testPartnerAccount.Partner_Margin__c = 12;
	        testPartnerAccount.Navision_Status__c = 'Partner';
	        testPartnerAccount.Territory_Country__c = 'France';
			insert	testPartnerAccount;
			testContact.AccountId = testPartnerAccount.id;
			update testContact;

			/******* changes for qcw-2934 start *********/

			insert QuoteTestHelper.createPCS(testPartnerAccount);
			/******* changes for qcw-2934 end *********/

			Address__c address = new Address__c();
	        address.Account__c = testPartnerAccount.Id;
	        address.Address_Contact__c = testContact.Id;
	        address.Valid_Address__c = true;
	        address.Address_Type__c = 'Billing';
	        address.Country__c = 'France';
	        address.Address_1__c = '10 Road Island';
	        address.City__c  = 'Paris';
	        insert address;
	        RecordType opprecTYpe= [select id from Recordtype where DeveloperName='Sales_QCCS' limit 1];
	        system.debug('qqqq'+opprecTYpe);
	        Account testacc2 = [select id from account where name ='Test Val Rule 1' limit 1];
	        system.debug('qqqq'+testacc2);
	        Opportunity opp = new Opportunity(CurrencyIsoCode= 'USD', name = 'asasa', Short_Description__c ='frff',
	        							StageName = 'Goal Identified', Revenue_Type__c= 'Reseller', CloseDate = Date.Today() +10,
	        							AccountId = testacc2.ID, RecordtypeId= opprecTYpe.Id, Partner_Contact__c = testContact.Id);
	        insert opp;
	        system.debug('qqqq'+opp);
	    }
	}

	static testMethod void quoteInsertScenario() {
		User u = [Select Id, Lastname, Email, INT_NetSuite_InternalID__c From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];

		RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, Name,SIC_Code_Name__c,sector__c,Industry_from_SIC_Code_Lookup__c From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name like 'PartnerAcc%'];
        Contact testContact = [Select Id, Name From Contact Where Lastname like 'RDZTestLastName%'];
		Contact testContact1 = [Select Id, Name From Contact Where Lastname = 'Dovbush'];

   		System.runAs(u) {
			Test.startTest();
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
			quoteForTest.Quote_Recipient__c = testContact1.Id;
			insert quoteForTest;
            System.assertNotEquals(null, quoteForTest.Id);
			System.debug('Test quote: ' + quoteForTest);
   			Test.stopTest();
		}
	}

	@isTest static void quoteUpdScenario() {
		User u = [Select Id, Lastname, Email, INT_NetSuite_InternalID__c From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];

		RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name like 'PartnerAcc%'];
        Contact testContact = [Select Id, Name From Contact Where Lastname like 'RDZTestLastName%'];
		Contact testContact1 = [Select Id, Name From Contact Where Lastname = 'Dovbush'];
        Contact testContact2 = [Select Id, Name From Contact Where Lastname = 'Dovbush2'];

   		System.runAs(u) {
			Test.startTest();
			Opportunity opp = [select id from opportunity limit 1];
			Account testAccount2 = [Select Id, Name From Account Where Name = 'Test Val Rule 2'];
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
            quoteForTest.SBQQ__Opportunity2__c = opp.Id;
            quoteForTest.Quote_Recipient__c = testContact1.Id;
            insert quoteForTest;
            System.assertNotEquals(null, quoteForTest.Id);
            update quoteForTest;
         Test.stopTest();
		}
	}

}