/**
*  06.04.2017 : Rodion Vakulvsokyi QCW-1976
*  10.04.2017 : Rodion Vakulvsokyi QCW-1994
**/
public class AccountNDACountryCheck {
    public  Boolean isRedCountry{
                                    get
                                    {
                                        //return customHelper.IsinRedCountryList(Country);
										return false;
                                    }
                                    set;
                                }
    public transient Account account {get; set;} 

    public String formedURL{get {	
									QTCustomSettingsHier__c qtCSHier = getQTCustomSettingsHier();
        							formedURL = 'https://'+qtCSHier.SpringCM_Environment__c+'.springcm.com/atlas/doclauncher/eos/';
									formedURL+= account.Subsidiary__r.NDA__c;
                					formedURL+='?aid='+qtCSHier.SpringCM_Account_Id__c+'&eos[0].Id='+account.id+'&eos[0].System=Salesforce&eos[0].Type=Account&eos[0].Name='+account.id+'&eos[0].ScmPath=/Salesforce/Accounts';
        							return formedURL;
                                }
                            set;}
    public String Country {get; set;}
    private transient ECustomsHelper customHelper;
    public ApexPages.StandardController controller;
    public AccountNDACountryCheck(ApexPages.StandardController stdController) {
        Id acctId = stdController.getId();
        controller =stdController;
        customHelper = new ECustomsHelper();
        account = [SELECT id, Territory_Country__c, Subsidiary__c, Subsidiary__r.NDA__c FROM Account WHERE Id =: acctId limit 1];        
		Country = String.IsBlank(account.Territory_Country__c)? '': account.Territory_Country__c ;
	}

	public QTCustomSettingsHier__c getQTCustomSettingsHier() {
		QTCustomSettingsHier__c qtCSHier = QTCustomSettingsHier__c.getInstance();
		return qtCSHier;
	}
}