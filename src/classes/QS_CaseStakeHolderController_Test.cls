/************************************************************************
 * @author  SAN
 * 2014-10-30 customer portal project
 *  
 * Test Class for QS_CaseStakeHolderController_Test.
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 *
 *************************************************************************/

@isTest
private class QS_CaseStakeHolderController_Test
{

	    private static Id recTypeId;

	@isTest
	static void test_Constructor() 
	{

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

		        Test.startTest();
       		// Running as the current user for code coverage only

		Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);

		insert caseObj;

       		ApexPages.currentPage().getParameters().put('id', caseObj.Id);
            	QS_CaseStakeHolderController caseStakeHolderController = new QS_CaseStakeHolderController();
	            //System.assertEquals(caseStakeHolderController.stakeholderToBe,caseObj.Id);

	            caseStakeHolderController.addStakeholder();
	            caseStakeHolderController.delStakeholder();

          
	            
    		
        Test.stopTest();
	//}

	}


	@isTest static void test_method_delStakeholder() {
		        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

		        Test.startTest();
       		// Running as the current user for code coverage only

		Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);

		insert caseObj;

       		ApexPages.currentPage().getParameters().put('id', caseObj.Id);
            	QS_CaseStakeHolderController caseStakeHolderController = new QS_CaseStakeHolderController();
			caseStakeHolderController.cshIdChosen=caseObj.Id;
			caseStakeHolderController.delStakeholder();
			System.assertEquals(caseStakeHolderController.caseStakeholders.size(),0);


	}



// Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }

}