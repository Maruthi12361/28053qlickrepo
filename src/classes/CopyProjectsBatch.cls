// Batch program to copy Projects to Project_SE__c custom objects
// NOTE: USE IT ONLY ONCE
// Initial Implementation - November 11, 2013 - Madhav Kakani
global class CopyProjectsBatch implements Database.Batchable<sObject> {
 
    global CopyProjectsBatch() {
    }
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, Name, pse__Account__c, pse__Is_Active__c, pse__Start_Date__c, pse__End_Date__c, Primary_Contact__c, pse__Project_Manager__c, pse__Region__c FROM pse__Proj__c';
        if(Test.isRunningTest()) query += ' LIMIT 1';
        return Database.getQueryLocator(query); 
    }
 
    global void execute(Database.BatchableContext BC, List<sObject> scope){ 
        List<pse__Proj__c> lstProj = scope;
     
        List<Project_SE__c> lstprojSE = new List<Project_SE__c>(); // list of project SE objects to insert      

        for(pse__Proj__c project : lstProj) { // copy the project
            Project_SE__c projSE = new Project_SE__c();
            projSE.Name = project.Name;
            projSE.Master_Project_Id__c = project.Id;
            projSE.Account__c = project.pse__Account__c;
            projSE.Is_Active__c = project.pse__Is_Active__c;
            projSE.Start_Date__c = project.pse__Start_Date__c;
            projSE.End_Date__c = project.pse__End_Date__c;
            projSE.Primary_Contact__c = project.Primary_Contact__c;
            projSE.Project_Manager__c = project.pse__Project_Manager__c;
            projSE.Region__c = project.pse__Region__c;
            
            lstprojSE.add(projSE);
        }
        if((lstprojSE != null) && (lstprojSE.size() > 0)) database.insert(lstprojSE, false);
    }
 
    global void finish(Database.BatchableContext BC){
    }
 
}