/*
2017-09-23 Shubham Gupta QCW-2849 Created A handler which will check if Deal split opportunity is created as a child of some
            parent opportunity, this will create dummy products on Child opportunity accumulating sum of net total from parent opportunity
            products. It classifies Renwable and non renwable products and creates separate dummy product for each of License,Education
            Consultancy,support(Renwable) and License,Education,Consultancy,support(Non Renwable).
*/


public class OppDealSplitScheduleHandler {

        private static List<String> ProductTypes = new List<String> {'Consulting', 'Licenses', 'Subscription', 'Education', 'Support Services', 'Misc', 'Licenses Renewable','Consulting Renewable','Support Services Renewable','Education Renewable'};

            static Set<Id> parentOppIds = new Set<Id>();
            static List<OpportunityLineItem> lineToInsert = new List<OpportunityLineItem>();
            static List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
            static List<OpportunityLineItem> splitoppLineItems = new List<OpportunityLineItem>();
            static Map<id, List<OpportunityLineItem>> oppToLineItm = new Map<id, List<OpportunityLineItem>>();
            static List<OpportunityLineItem> childlineItem = new List<OpportunityLineItem>();
            static set<id> splitopp = new Set<id>();
            static boolean isDeleted = false;
            static set<id> childopp = new set<id>();

        public static void onAfterInsert(List<Opportunity> inputList, Map<id, Opportunity> inputMap){


            for(Opportunity opp: inputList){

                if(String.isNotBlank(opp.Deal_Split_Parent_Opportunity__c))
                    parentOppIds.add(opp.Deal_Split_Parent_Opportunity__c);
            }
            system.debug('Parent Opp on deal split is '+parentOppIds);
            if(!parentOppIds.isEmpty()){
                createSplitProduct(inputList,parentOppIds);
                }
            }

    public static void onAfterUpdate(Map<id, Opportunity> inputMap){


            System.debug('******* Going in split oppty*****');
            system.debug('aplit oppty came are '+inputMap);

            if(!inputMap.isEmpty()){
                system.debug('split queable is called');
                List<Opportunity> opptytosend = inputMap.values();
                system.debug('split Limits of jobs' + Limits.getQueueableJobs());
                for(Opportunity opp:opptytosend){
                    childopp.add(opp.id);
                }
                if(!childopp.isEmpty()){
                createUpdatedLines(childopp);
            }
        }

    }
        @future
        public static void createUpdatedLines(set<id> childoppids){
            Set<id> parentopptyid = new Set<id>();
            List<OpportunityLineItem> childlines = new List<OpportunityLineItem>();

            
            List<opportunity> childopps = [select id,CurrencyIsoCode,Deal_Split_Parent_Opportunity__c,Deal_Split_Percentage__c,(select id from opportunityLineItems) from opportunity where id IN:childoppids];
            system.debug('child opp came are '+childopps);  
            
            for(Opportunity chopp:childopps){
                parentopptyid.add(chopp.Deal_Split_Parent_Opportunity__c);
                childlines.addAll(chopp.opportunityLineItems);
            }
            system.debug('parent opp id is '+parentopptyid);
            system.debug('childs to delete are '+childlines);

            deleteExisting(childlines);
            createSplitProduct(childopps,parentopptyid);

        }

        public static void deleteExisting(List<OpportunityLineItem> lineItemExist){
            try{
        delete lineItemExist;
            }
            Catch(Exception e){
                system.debug(e);
            }
    }

    public static void createSplitProduct(List<Opportunity> inputList,Set<Id> parentOppIds){
            if(!parentOppIds.isEmpty()){
                List<Opportunity> oppties = [select id,name,Pricebook2Id,(select id,Product2.id,product2.SBQQ__SubscriptionType__c,product2.family,productcode,Product_Family__c,PricebookEntryId,Net_Price__c,UnitPrice from OpportunityLineItems) from opportunity where id IN:parentOppIds];
                for(Opportunity oppty:oppties){
                    oppToLineItm.put(oppty.id, oppty.opportunityLineItems);
                }
            }
            String priceBookId = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Standard_PriceBook__c;
            List<PricebookEntry> listOfPriceBookEntry = [SELECT Id, CurrencyIsoCode, IsActive, Name, Pricebook2Id, Product2Id, ProductCode, UnitPrice, UseStandardPrice FROM PricebookEntry WHERE IsActive = TRUE AND Pricebook2Id = :priceBookId and ProductCode in :ProductTypes];

            Decimal SumLicNonRen;
            Decimal SumSubNonRen;
            Decimal SumConsulNonRen;
            Decimal SumEduNonRen;
            Decimal SumSuppNonRen;
            Decimal SumLicRen;
            Decimal SumSubRen;
            Decimal SumConsulRen;
            Decimal SumEduRen;
            Decimal SumSuppRen;

            if(!oppToLineItm.isEmpty()){
                system.debug('Map of opportunity to lineitems is '+oppToLineItm);
                for(opportunity opp:inputList){
                    SumLicNonRen= 0;
                    SumSubNonRen= 0;
                    SumConsulNonRen= 0;
                    SumEduNonRen = 0;
                    SumSuppNonRen = 0;
                    SumLicRen = 0;
                    SumSubRen = 0;
                    SumConsulRen= 0;
                    SumEduRen = 0;
                    SumSuppRen = 0;

                    oppLineItems = oppToLineItm.get(opp.Deal_Split_Parent_Opportunity__c);
                    system.debug('OppLineItemCame '+oppLineItems);
                    if(oppLineItems != Null){
                    for(OpportunityLineItem oppline:oppLineItems){
                        if(oppline.product2.SBQQ__SubscriptionType__c == 'Renewable'){
                            if(oppline.product2.family == 'Licenses'){
                            system.debug('Renewable License product '+oppline.Net_Price__c);
                            SumLicRen += oppline.Net_Price__c;
                           }
                           if(oppline.product2.family == 'Subscription'){
                            system.debug('Renewable License product '+oppline.Net_Price__c);
                            SumSubRen += oppline.Net_Price__c;
                           }
                           if(oppline.product2.family == 'Consulting'){
                            system.debug('Renewable License product '+oppline.Net_Price__c);
                            SumConsulRen += oppline.Net_Price__c;
                           }
                           if(oppline.product2.family == 'Education'){
                            system.debug('Renewable License product '+oppline.Net_Price__c);
                            SumEduRen += oppline.Net_Price__c;
                           }
                           if(oppline.product2.family == 'Support Services'){
                            system.debug('Renewable License product '+oppline.Net_Price__c);
                            SumSuppRen += oppline.Net_Price__c;
                           }
                        }
                        if(string.isEmpty(oppline.product2.SBQQ__SubscriptionType__c) || oppline.product2.SBQQ__SubscriptionType__c == null){
                            if(oppline.product2.family == 'Licenses'){
                            system.debug('Non Renewable Liscense product '+oppline.Net_Price__c);
                            SumLicNonRen += oppline.Net_Price__c;
                            }
                            if(oppline.product2.family == 'Subscription'){
                            system.debug('Non Renewable Liscense product '+oppline.Net_Price__c);
                            SumSubNonRen += oppline.Net_Price__c;
                            }
                           if(oppline.product2.family == 'Consulting'){
                            system.debug('Non Renewable consulting product '+oppline.Net_Price__c);
                            SumConsulNonRen += oppline.Net_Price__c;
                            }
                            if(oppline.product2.family == 'Education'){
                            system.debug('Non Renewable Education product '+oppline.Net_Price__c);
                            SumEduNonRen += oppline.Net_Price__c;
                            }
                            if(oppline.product2.family == 'Support Services'){
                            system.debug('Non Renewable Support product '+oppline.Net_Price__c);
                            SumSuppNonRen += oppline.Net_Price__c;
                            }
                        }
                        }
                    }
                    if(SumLicRen > 0){
                        lineToInsert.add(createProduct(opp,'Licenses Renewable',SumLicRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                    if(SumSubRen > 0){
                        lineToInsert.add(createProduct(opp,'Subscription Renewable',SumSubRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                     if(SumConsulRen > 0){
                        lineToInsert.add(createProduct(opp,'Consulting Renewable',SumConsulRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                     if(SumEduRen > 0){
                        lineToInsert.add(createProduct(opp,'Education Renewable',SumEduRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                     if(SumSuppRen > 0){
                        lineToInsert.add(createProduct(opp,'Support Services Renewable',SumSuppRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                    if(SumLicNonRen > 0){
                        lineToInsert.add(createProduct(opp,'Licenses',SumLicNonRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                    if(SumSubNonRen > 0){
                        lineToInsert.add(createProduct(opp,'Subscription',SumSubNonRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                    if(SumConsulNonRen > 0){
                        lineToInsert.add(createProduct(opp,'Consulting',SumConsulNonRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                    if(SumEduNonRen > 0){
                        lineToInsert.add(createProduct(opp,'Education',SumEduNonRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                    if(SumSuppNonRen > 0){
                        lineToInsert.add(createProduct(opp,'Support Services',SumSuppNonRen,listOfPriceBookEntry,opp.Deal_Split_Percentage__c));
                    }
                }
            }
        try{
            upsert lineToInsert;
        }
        Catch(Exception e){
            system.debug(e);
        }
        }


         public static OpportunityLineItem CreateProduct(Opportunity Opp, string ProductType, decimal Val, list<PricebookEntry> ProductList, Decimal splitPercent){

             System.Debug('ProductList.size() = ' + ProductList.size());
            //Search the ProductList for the PricebookEntry to use
            PricebookEntry pbeToUse = new PricebookEntry();
            for (PricebookEntry pbe : ProductList)
            {
                if ((pbe.Name == ProductType) && (pbe.CurrencyIsoCode == Opp.CurrencyIsoCode))
                {
                    pbeToUse = pbe;
                }
            }
            System.Debug('pbeToUse= ' + pbeToUse);
            if (pbeToUse.Name == null)
            {
                System.Debug('Pricebook to use: No PricebookEntry found');
            }
            else
            {
                val = val*(splitPercent/100);
                system.debug('value new is '+val);
                OpportunityLineItem oli = new OpportunityLineItem
                (
                    OpportunityId = Opp.Id,
                    PricebookEntryId = pbeToUse.Id,
                    Quantity = 1,
                    UnitPrice = Val
                );
                System.Debug('Opportunity_ManageForecastProductsHelper: Create: oli = ' + oli);
                return oli;
            }

            return null;
         }
    }