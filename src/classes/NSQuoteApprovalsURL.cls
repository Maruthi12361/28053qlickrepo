/*
* File NSQuoteApprovalsURL
    * @description : 
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       09.01.2018   Pramod Kumar V      Created Class 
  
*/
public class NSQuoteApprovalsURL{

public String NSQuoteAppr {get;set;}
public String gSandboxName;

public NSQuoteApprovalsURL() {
//NSQuoteAppr= 'https://sfdcnsjumpsite.qliktech.com:1024/Default.aspx?SessionId={!$Api.Session_ID}&CallbackURL={!URLENCODE($Api.Enterprise_Server_URL_160)}&TargetURL=https%3A%2F%2Fsystem.netsuite.com%2Fc.679333%2FQlikTech-Inc-%2FQT-Quote-Utils%2Findex.ssp';
 } 
public NSQuoteApprovalsURL(ApexPages.StandardController acon) {
     String lSandBoxName = UserInfo.getUserName();
     String gSandboxName;   
     String defaultKeyValue = 'DefaultValues';
     RefreshSandboxData__mdt rs = null;
     Map<String, RefreshSandboxData__mdt> RefreshSandboxItem=new Map<String, RefreshSandboxData__mdt>();
     
     if(lSandBoxName.contains('com.')){
                    List <String> lSandBoxSplit = lSandBoxName.split('com.',2);  
                    gSandboxName= lSandBoxSplit[1];
     }
     else{
                gSandboxName = 'test';
     }
     
           for(RefreshSandboxData__mdt item :  [SELECT Id,MasterLabel,Support_Portal_Live_Agent_API_Endpoint__c,Support_Portal_Login_Page_Url__c,Support_Portal_Login_Url__c,
                                                                                                 Support_Portal_Url__c,Support_Portal_Url_Base__c,Support_Portal_Logout_Page_Url__c,Support_Portal_CSS_Base__c,SpringAccountPrefix__c,CMInstanceUrl__c,QlikMarket_Website__c, BoomiBaseURL__c, NSQuoteApprovals__c,ULC_Base_URL__c FROM RefreshSandboxData__mdt]) {
            RefreshSandboxItem.put(item.MasterLabel,item);
        }
        if(RefreshSandboxItem.get(gSandboxName.toUppercase())!=NULL){
            system.debug('sand '+RefreshSandboxItem.get(gSandboxName));
            rs=RefreshSandboxItem.get(gSandboxName.toUppercase());
        }
       else if(RefreshSandboxItem.containsKey(defaultKeyValue)){
            rs=RefreshSandboxItem.get(defaultKeyValue);
        }
        if(rs!=NULL){
             NSQuoteAppr=rs.NSQuoteApprovals__c; 
            }   
         }
    }