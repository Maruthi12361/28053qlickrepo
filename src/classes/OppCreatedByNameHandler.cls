/****************************************************************
*
*  OppCreatedByNameHandler
*
*  2016-05-19 TJG : Migrated from the following trigger
					OppCreatedByName
*  2017-06-22 MTM  QCW-2711 remove sharing option
*****************************************************************/
public class OppCreatedByNameHandler {
	// to be called on both events before Insert and before Update 
	public static void handle(List<Opportunity> triggerOld, List<Opportunity> triggerNew) {
		Set<Id> OppId= new Set<Id>();

		for(integer i = 0; i < triggerNew.size(); i++)
		{
			if ((triggerNew[i].CreatedById == null) ||
		 		(triggerNew[i].Original_Created_by_Function__c != triggerOld[i].Original_Created_by_Function__c) )
			{
		    	OppId.add(triggerNew[i].Id);
			}  
		}
		if(OppId.size() > 0)
		{
			Map <ID,Opportunity> OppList = new Map<ID,Opportunity>(
		  		[Select Academic_Contact_Name__r.Id,Id,Name,CreatedBy.FirstName,CreatedBy.LastName from Opportunity where Id = :OppId]
		  	);

			for(Opportunity opp : triggerNew)
			{
		    	if( OppList.containsKey(opp.Id) )
		    	{       
		             
		        	if( opp.Original_Created_by_Function__c == 'BDR;' ||
		            	opp.Original_Created_by_Function__c == 'iBDR;'||
		            	opp.Original_Created_by_Function__c == 'EIS;'
		            )
		        	{
		            	Opportunity lOpp = OppList.get(opp.Id);
		            	opp.Opp_CreatedBy_Copy__c = lOpp.CreatedBy.FirstName+ ' ' + lOpp.CreatedBy.LastName;     
		        	}
		        	else
		        	{
			          opp.Opp_CreatedBy_Copy__c = null;		            
		    		}
		    	}      
		    }   
		} 		
	}
}