/**
*
* Change log:
*
*   2019-08-28 ext_bad  CHG0036677: Initial implementation.
*/
public with sharing class SkillsAssessmentHandler {
    public static void onBeforeInsert(List<Skills_Assessment__c> newItems) {
        List<Skills_Assessment__c> contactAssessments = new List<Skills_Assessment__c>();
        List<Skills_Assessment__c> leadAssessments = new List<Skills_Assessment__c>();
        List<String> contactIds = new List<String>();
        List<String> leadIds = new List<String>();
        for (Skills_Assessment__c sa : newItems) {
            if (sa.Contact__c != null) {
                contactIds.add(sa.Contact__c);
                contactAssessments.add(sa);
            } else if (sa.Lead__c != null) {
                leadIds.add(sa.Lead__c);
                leadAssessments.add(sa);
            }
        }
        if (!leadAssessments.isEmpty()) {
            processLeadAssessments(leadAssessments, leadIds);
        }

        if (!contactAssessments.isEmpty()) {
            processContactAssessments(contactAssessments, contactIds);
        }
    }

    private static void processLeadAssessments(List<Skills_Assessment__c> items, List<String> leadIds) {
        List<ULC_Details__c> ulcDetails = [
                SELECT Id, ULCName__c, LeadId__c
                FROM ULC_Details__c
                WHERE LeadId__c IN :leadIds
        ];
        Map<String, ULC_Details__c> ulcMap = new Map<String, ULC_Details__c>();
        for (ULC_Details__c ulc : ulcDetails) {
            ulcMap.put(ulc.LeadId__c, ulc);
        }

        Map<String, Lead> leads = new Map<String, Lead>([
                SELECT Id, Email, FirstName, LastName
                FROM Lead
                WHERE Id IN :leadIds
        ]);

        for (Skills_Assessment__c sa : items) {
            Lead lead = leads.get(sa.Lead__c);
            sa.Email__c = lead.Email;
            sa.First_Name__c = lead.FirstName;
            sa.Last_Name__c = lead.LastName;
            sa.ULC__c = ulcMap.get(sa.Lead__c) != null ? ulcMap.get(sa.Lead__c).ULCName__c : '';
        }
    }

    private static void processContactAssessments(List<Skills_Assessment__c> items, List<String> contactIds) {
        List<ULC_Details__c> ulcDetails = [
                SELECT Id, ULCName__c, ContactId__c
                FROM ULC_Details__c
                WHERE ContactId__c IN :contactIds
        ];
        Map<String, ULC_Details__c> ulcMap = new Map<String, ULC_Details__c>();
        for (ULC_Details__c ulc : ulcDetails) {
            ulcMap.put(ulc.ContactId__c, ulc);
        }

        Map<String, Contact> contacts = new Map<String, Contact>([
                SELECT Id, Email, FirstName, LastName
                FROM Contact
                WHERE Id IN :contactIds
        ]);

        for (Skills_Assessment__c sa : items) {
            Contact contact = contacts.get(sa.Contact__c);
            sa.Email__c = contact.Email;
            sa.First_Name__c = contact.FirstName;
            sa.Last_Name__c = contact.LastName;
            sa.ULC__c = ulcMap.get(sa.Contact__c) != null ? ulcMap.get(sa.Contact__c).ULCName__c : '';
        }
    }
}