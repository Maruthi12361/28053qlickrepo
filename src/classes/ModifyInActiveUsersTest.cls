/*
* File ModifyInActiveUsersTest
    * @description : Unit test for ModifyInActiveUsers
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       05.08.2018   ext_bjd             Created Class
    2       11.06.2019   ext_bjd             Added fix to avoid System.LimitException: Too many query rows: 50001 from
    *                                        CustomUserTriggerHandler.processUserAssignment
    3       30.03.2020   ext_bjd             ITRM-409 Activate SPRING CM Users, code refactoring
    4       30.04.2020   ext_bjd             code refactoring
    5       04.05.2020   ext_bjd             ITRM-463 code refactoring to decrease long-running tests
    6       01.07.2020   ext_bjd             ITRM-487 Initial Development
*/

@IsTest
public class ModifyInActiveUsersTest {

    @TestSetup
    static void setup() {
        CustomUserTriggerHandler.ENABLE_USER_TRIGGER = false;
        List<User> testUsers = new List<User>();
        List<User> updatedTestUsers = new List<User>();

        testUsers.add(QTTestUtils.createMockUserForProfile('PRM - Base', true));

        User thisUserAdmin = QTTestUtils.createMockSystemAdministrator();
        System.runAs (thisUserAdmin) {
            for (User us : testUsers) {
                us.Country = 'Sweden';
                us.IsActive = false;
                us.SpringCMEos__SpringCM_User__c = false;
                us.Email = us.Email + '.invalid';
                us.activeOnlyInSandboxes__c = true;
                us.Last_Profile__c = 'PRM - Base (READ ONLY)';
                updatedTestUsers.add(us);
            }
            update updatedTestUsers;
        }
    }

    @IsTest
    public static void inActiveUserTest() {

        SandboxNameFinder snf = new SandboxNameFinder();
        String sandboxName = snf.SandboxNameHelper();

        Test.startTest();
        ModifyInActiveUsers.runUserUpdatesWithLicenses(sandboxName);
        Test.stopTest();

        List<User> restUsrWithSweden = [
                SELECT Id, Name, Country, Email, IsActive, activeOnlyInSandboxes__c, Profile.Name, SpringCMEos__SpringCM_User__c
                FROM User
                WHERE Country = 'Sweden' AND activeOnlyInSandboxes__c = TRUE AND Last_Profile__c = 'PRM - Base (READ ONLY)'
        ];
        for (User us : restUsrWithSweden) {
            System.assertEquals(true, us.IsActive);
            System.assertEquals(true, us.SpringCMEos__SpringCM_User__c);
            System.assertEquals('Sweden', us.Country);
            System.assertEquals('PRM - Base (READ ONLY)', us.Profile.Name);
            System.assertEquals(false, us.Email.contains('.invalid'));
        }
    }
}