/**     * File Name: PartnerAccountTerminationHandler
        * Description : Trigger handler to manage partner account terminations.
        * @author : Linus Löfberg
        * Modification Log ===============================================================
        Ver     Date         Author         Modification
        1.0     2018-01-31   ext_lfb        Class initiated.
*/
public with sharing class PartnerAccountTerminationHandler {

    public static void TerminatePartnerAccounts(Account[] oldRecords, Account[] updatedRecords, Map<ID, Account> recordMap, Map<ID, Account> oldRecordMap) {

        // First look for Accounts in scope to avoid unnecessary SOQLS.
        List<Account> accountsToProcess = new List<Account>();
        for (Account acc : updatedRecords) {
            if (acc.Terminated__c && !oldRecordMap.get(acc.Id).Terminated__c) {
                accountsToProcess.add(acc);
            }
        }

        if (!accountsToProcess.isEmpty()) {
            HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
            String userProfileName = [SELECT Id, Profile__c FROM User WHERE Id = :UserInfo.getUserId()].Profile__c;
            Boolean allowTermination = false;
            Boolean freeSlotInFlexQueueAvailable = false;
            Set<Id> accountIdsForBatch = new Set<Id>();

            if (settings.Ops_Admin_Profile_Id__c.contains(UserInfo.getProfileId()) ||
            settings.Sys_Admin_ProfileId__c.contains(UserInfo.getProfileId()) ||
            userProfileName.equals('Custom: Api Only User')) {
                allowTermination = true;
            }

            // Check flex queue size
            Integer flexQueueSize = [SELECT COUNT() FROM AsyncApexJob WHERE Status = 'Holding'];
            if (flexQueueSize < 100) {
                freeSlotInFlexQueueAvailable = true;
            }

            // Retrieve portal user counts
            Map<Id, Integer> accountUserCountMap = new Map<Id, Integer>();
            List<User> portalUsers = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Profile.Name LIKE '%PRM%' AND Contact.AccountId IN :recordMap.keySet()];

            for (User u : portalUsers) {
                if (accountUserCountMap.containsKey(u.Contact.AccountId)) {
                    accountUserCountMap.put(u.Contact.AccountId, accountUserCountMap.get(u.Contact.AccountId) + 1);
                } else {
                    accountUserCountMap.put(u.Contact.AccountId, 1);
                }
            }

            for (Account acc : accountsToProcess) {
                if (allowTermination || acc.OwnerId.equals(UserInfo.getUserId())) {
                    if (freeSlotInFlexQueueAvailable) {
                        // Check contact count before continued processing
                        if (accountUserCountMap.containsKey(acc.Id) && accountUserCountMap.get(acc.Id) > 15) {
                            acc.addError('Partner Account Termination is not available for accounts with 15+ contacts. Please manually de-activate Contacts and try again.');
                        } else {
                            acc.Partner_Status__c = 'Terminated';
                            acc.Status_from_Date__c = DATE.TODAY();
                            acc.Partner_Status_Change_Reason__c = 'Termination';
                            acc.Navision_Status__c = 'Terminated';
                            acc.Publish_To_Directory__c = false;

                            if (acc.Partner_Status_Change_Comment__c == null || acc.Partner_Status_Change_Comment__c.equals(oldRecordMap.get(acc.Id).Partner_Status_Change_Comment__c)) {
                                acc.Partner_Status_Change_Comment__c = 'Deactivated';
                            }
                            accountIdsForBatch.add(acc.Id);
                        }
                    } else {
                        acc.addError('No available slots in Apex Flex Queue. Try again later.');
                    }
                } else {
                    acc.addError('Partner Account Termination is not authorized for this user.');
                }
            }

            // Add the batch to flex queue
            if (!accountIdsForBatch.isEmpty()) {
                Database.executeBatch(new PartnerAccountTerminationBatch(accountIdsForBatch), 1);
            }
        }
    }
}