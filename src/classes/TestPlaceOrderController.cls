/****************************************************************
*
*  TestPlaceOrderController
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*  17.03.2017 : Rodion Vakulvsokyi
*  23.03.2017 : Rodion Vakulvsokyi
*  20.05.2017 : Muneer  QCW-1633
*  02.09.2017 : Srinivasan PR- fix for query error
*  30.01.2018 : Linus Löfberg Test optimizations as part of reverting QCW-4421
*  2018-06-04 MTM BSL-490 New visual compliance implementations
*  2019-02-14 AIN Added more unique names for test accounts to avoid duplicate rule
*  2020-04-10 UIN Added Logic for oem
*****************************************************************/
@isTest
public class TestPlaceOrderController {
    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    static final String AccRecordTypeId_OEMEndUserAccount = '012D0000000KBYx';
    static final String AccRecordTypeId_OEMPArtnerAccount = '01220000000DOG4';
    static final String OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
    static final String OEMOppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('OEM_Order').id;

    @testSetup
    public static void testSetup() {
        Test.startTest();
        QuoteTestHelper.createCustomSettings();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        Test.stopTest();
    }
    
    @isTest
    public static void PlaceOrderOEM() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {
            Account testAccount = QTTestUtils.createMockAccount('OEM Test company with a unique name', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_OEMEndUserAccount;
            testAccount.ECUSTOMS__RPS_Status__c = 'No Matches';
            testAccount.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            update testAccount;
            
            Account testAccount1 = QTTestUtils.createMockAccount('OEM Part company with a unique name', testUser, true);
            testAccount1.RecordtypeId = AccRecordTypeId_OEMPArtnerAccount;
            testAccount1.ECUSTOMS__RPS_Status__c = 'No Matches';
            testAccount1.ECUSTOMS__RPS_RiskCountry_Status__c = 'N/A';
            testAccount1.Territory_Country__c = 'United Kingdom';
            update testAccount1;

            QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

            Contact testContact = QuoteTestHelper.createContact(testAccount1.id);
            insert testContact;

            Test.startTest();
            Address__c billAddress = QuoteTestHelper.createAddress(testAccount1.id, testContact.id, 'Billing');
            billAddress.Valid_Address__c = true;
            billAddress.Default_Address__c = true;
            billAddress.Legal_Approval_Status__c = 'Granted';
            billAddress.Country__c = 'United Kingdom';
            insert billAddress;

            Address__c shippAddress = QuoteTestHelper.createAddress(testAccount1.id, testContact.id, 'Shipping');
            shippAddress.Valid_Address__c = true;
            shippAddress.Default_Address__c = true;
            shippAddress.Legal_Approval_Status__c  = 'Granted';
            insert shippAddress;
            
            RecordType rTypeOpp = [Select id From RecordType Where developerName = 'OEM_Order'];
            Opportunity opp = QuoteTestHelper.createOpportunity(testAccount1, '', rTypeOpp);
            opp.End_User_Account_del__c  = testAccount.id;
            insert opp;
            
            RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount1, '', 'OEM', 'Open', 'Quote', false, opp.Id);
            quoteForTest.Select_Billing_Address__c = billAddress.id;
            quoteForTest.Shipping_Address__c = shippAddress.id;

            insert quoteForTest;

            quoteForTest = [Select id, 
                            Revenue_Type__c, 
                            SBQQ__Account__c,
                            SBQQ__Opportunity2__c,
                            Select_Billing_Address__r.Legal_Approval_Status__c,
                            Shipping_Address__r.Legal_Approval_Status__c,
                            SBQQ__Account__r.ECUSTOMS__RPS_Status__c,
                            SBQQ__Account__r.ECUSTOMS__IM_Status__c,
                            Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c, 
                            Sell_Through_Partner__r.ECUSTOMS__IM_Status__c,
                            SBQQ__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
                            Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c
                            From SBQQ__Quote__c 
                            where id =: quoteForTest.id];

            

            ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
            PlaceOrderController placeOrderCntr = new PlaceOrderController(sc);
            placeOrderCntr.quote = quoteForTest;
            System.assertEquals(true, placeOrderCntr.status);
            System.assertEquals(true, placeOrderCntr.VCstatus);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void PlaceOrderDirect() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {
            Account testAccount = QTTestUtils.createMockAccount('Test company with a unique name', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;

            QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

            Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

            Test.startTest();
            Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
            billAddress.Valid_Address__c = true;
            billAddress.Default_Address__c = true;
            billAddress.Legal_Approval_Status__c = 'Granted';
            billAddress.Country__c = 'United Kingdom';
            insert billAddress;

            Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            shippAddress.Valid_Address__c = true;
            shippAddress.Default_Address__c = true;
            shippAddress.Legal_Approval_Status__c  = 'Granted';
            insert shippAddress;

            RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
            quoteForTest.Select_Billing_Address__c = billAddress.id;
            quoteForTest.Shipping_Address__c = shippAddress.id;

            insert quoteForTest;

            quoteForTest = [Select id, Revenue_Type__c, SBQQ__Account__c, SBQQ__Opportunity2__c, Select_Billing_Address__r.Legal_Approval_Status__c, Shipping_Address__r.Legal_Approval_Status__c From SBQQ__Quote__c where id =: quoteForTest.id];

            Test.stopTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
            PlaceOrderController placeOrderCntr = new PlaceOrderController(sc);
            placeOrderCntr.quote = quoteForTest;
            System.assertEquals(true, placeOrderCntr.status);
        }
    }

    @isTest
    public static void PlaceOrderReseller() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {
            Account testAccount = QTTestUtils.createMockAccount('Test company with a unique name', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;

            RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and sobjecttype='Account' ];
            QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

            Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            testPartnerAccount.Territory_Country__c = 'France';
            insert  testPartnerAccount;

            insert QuoteTestHelper.createPCS(testPartnerAccount);

            Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

            Contact testPartnerContact = QuoteTestHelper.createContact(testPartnerAccount.id);
            insert testPartnerContact;

            Test.startTest();

            Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
            billAddress.Valid_Address__c = true;
            billAddress.Default_Address__c = true;
            billAddress.Legal_Approval_Status__c = 'Granted';
            billAddress.Country__c = 'United Kingdom';
            insert billAddress;

            Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            shippAddress.Valid_Address__c = true;
            shippAddress.Default_Address__c = true;
            shippAddress.Legal_Approval_Status__c  = 'Granted';
            insert shippAddress;

            Address__c billAddress2 = QuoteTestHelper.createAddress(testPartnerAccount.id, testPartnerContact.id, 'Billing');
            billAddress2.Valid_Address__c = true;
            billAddress2.Default_Address__c = true;
            billAddress2.Legal_Approval_Status__c = 'Granted';
            billAddress.Country__c = 'United Kingdom';
            insert billAddress2;

            Address__c shippAddress2 = QuoteTestHelper.createAddress(testPartnerAccount.id, testPartnerContact.id, 'Shipping');
            shippAddress2.Valid_Address__c = true;
            shippAddress2.Default_Address__c = true;
            shippAddress2.Legal_Approval_Status__c  = 'Granted';
            insert shippAddress2;

            RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Reseller', 'Open', 'Quote', false, '');
            quoteForTest.Select_Billing_Address__c = billAddress2.id;
            quoteForTest.Shipping_Address__c = shippAddress2.id;
            quoteForTest.Partner_Contact__c = testPartnerContact.Id;
            quoteForTest.Sell_Through_Partner__c = testPartnerAccount.Id;
            quoteForTest.Quote_Recipient__c = testContact.Id;
            insert quoteForTest;

            quoteForTest = [Select id, Revenue_Type__c, SBQQ__Account__c, SBQQ__Opportunity2__c, Select_Billing_Address__r.Legal_Approval_Status__c, Shipping_Address__r.Legal_Approval_Status__c From SBQQ__Quote__c where id =: quoteForTest.id];

            Test.stopTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
            PlaceOrderController placeOrderCntr = new PlaceOrderController(sc);
            placeOrderCntr.quote = quoteForTest;
            System.assertEquals(true, placeOrderCntr.status);
        }
    }

    @isTest
    public static void VCResellerTest() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {
            Account testAccount = QTTestUtils.createMockAccount('TestCompany1', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAccount.ECUSTOMS__RPS_Status__c = 'No Matches';
            update testAccount;

            RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and sobjecttype='Account' ];
            QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

            Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            testPartnerAccount.Territory_Country__c = 'France';
            testPartnerAccount.ECUSTOMS__RPS_Status__c = 'No Matches';
            insert  testPartnerAccount;

            insert QuoteTestHelper.createPCS(testPartnerAccount);

            Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

            Contact testPartnerContact = QuoteTestHelper.createContact(testPartnerAccount.id);
            insert testPartnerContact;

            Test.startTest();
            PageReference pageReference = Page.PlaceOrder;
            Test.setCurrentPage(pageReference);
            List<Address__c> addresses = new List<Address__c>();
            Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
            billAddress.Valid_Address__c = true;
            billAddress.Default_Address__c = true;
            //billAddress.Legal_Approval_Status__c = 'Granted';
            billAddress.Country__c = 'United Kingdom';
            addresses.Add(billAddress);

            Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            shippAddress.Valid_Address__c = true;
            shippAddress.Default_Address__c = true;
            shippAddress.Legal_Approval_Status__c  = 'Granted';
            addresses.Add(shippAddress);

            Address__c billAddress2 = QuoteTestHelper.createAddress(testPartnerAccount.id, testPartnerContact.id, 'Billing');
            billAddress2.Valid_Address__c = true;
            billAddress2.Default_Address__c = true;
            billAddress2.Legal_Approval_Status__c = 'Granted';
            billAddress.Country__c = 'United Kingdom';
            addresses.Add(billAddress2);

            Address__c shippAddress2 = QuoteTestHelper.createAddress(testPartnerAccount.id, testPartnerContact.id, 'Shipping');
            shippAddress2.Valid_Address__c = true;
            shippAddress2.Default_Address__c = true;
            shippAddress2.Legal_Approval_Status__c  = 'Granted';
            addresses.Add(shippAddress2);
            insert(addresses);

            RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Reseller', 'Open', 'Quote', false, '');
            quoteForTest.Select_Billing_Address__c = billAddress2.id;
            quoteForTest.Shipping_Address__c = shippAddress2.id;
            quoteForTest.Partner_Contact__c = testPartnerContact.Id;
            quoteForTest.Sell_Through_Partner__c = testPartnerAccount.Id;
            quoteForTest.Quote_Recipient__c = testContact.Id;
            insert quoteForTest;

            quoteForTest = [Select id, 
                            Revenue_Type__c, 
                            SBQQ__Account__c,
                            SBQQ__Opportunity2__c,
                            Select_Billing_Address__r.Legal_Approval_Status__c,
                            Shipping_Address__r.Legal_Approval_Status__c,
                            SBQQ__Account__r.ECUSTOMS__RPS_Status__c,
                            SBQQ__Account__r.ECUSTOMS__IM_Status__c,
                            Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c, 
                            Sell_Through_Partner__r.ECUSTOMS__IM_Status__c,
                            SBQQ__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
                            Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c
                            From SBQQ__Quote__c 
                            where id =: quoteForTest.id];



            ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
            PlaceOrderController placeOrderCntr = new PlaceOrderController(sc);
            //aceOrderCntr.quote = quoteForTest;
            System.assertEquals(true, placeOrderCntr.status);
            System.assertEquals(true, placeOrderCntr.VCstatus);
            Test.stopTest();
        }
    }
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
        Country_Code_Two_Letter__c = countryAbbr,
        Country_Name__c = countryName,
        Subsidiary__c = subsId);
        return qlikTechIns;
    }
}