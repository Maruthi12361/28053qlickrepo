/********************************************************
* 2018-07-10	ext_bad		CHG0034165
* 2020-05-05	ain		    IT-2767
*********************************************************/
@isTest
private class CaseEscalationHandlerTest {
    
    public static List<Decimal> bugWeightsInEscalations;
    public static Support_Bug_Weight_Calculations__mdt bugWeightMdt = null;
    
    static testMethod void testFieldsPopulating() {
        User testUsr = [
                SELECT Id
                FROM User
                WHERE IsActive = TRUE AND Id != :UserInfo.getUserId()
                AND ProfileId = :UserInfo.getProfileId()
                LIMIT 1
        ];
        System.runAs(testUsr) {
            List<EmailTemplate> etlist = [
                    SELECT id, Body, Subject, HtmlValue
                    FROM EmailTemplate
                    WHERE DeveloperName = :'New_Case_Escalation'
            ];
            if (etlist.size() == 0) {
                EmailTemplate et = new EmailTemplate(
                        DeveloperName = 'New_Case_Escalation',
                        Name = 'New Case Escalation',
                        FolderId = UserInfo.getOrganizationId(),
                        TemplateType = 'Custom',
                        Body = 'test',
                        Subject = 'test',
                        HtmlValue = 'test'
                );
                insert et;
            }
        }

        List<Account> accounts = new List<Account>();
        Account acc = new Account(Name = 'Test Account');
        Account acc2 = new Account(Name = 'Test Account');
        accounts.add(acc);
        accounts.add(acc2);
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        Contact c = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title');
        Contact c2 = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title 2');
        contacts.add(c);
        contacts.add(c2);
        insert contacts;
        Case cs = new Case();
        cs.Account_Origin__c = acc.Id;
        cs.ContactId = cs.Id;
        cs.Status = 'New';
        cs.Subject = 'New';
        cs.Severity__c = '1';
        cs.OwnerId = UserInfo.getUserId();
        insert cs;
        List<Qlikview_Support_Escalation__c> escs = new List<Qlikview_Support_Escalation__c>();

        Qlikview_Support_Escalation__c esc = new Qlikview_Support_Escalation__c();
        esc.Case__c = cs.Id;
        esc.ContactName__c = c.Id;
        esc.Description__c = 'test';
        esc.Business_Case__c = 'test';
        esc.Distribution_List__c = 'test';
        esc.Qlik_Management_Approver__c = UserInfo.getUserId();

        Qlikview_Support_Escalation__c esc2 = new Qlikview_Support_Escalation__c();
        esc2.Case__c = cs.Id;
        esc2.ContactName__c = c.Id;
        esc2.Contact_Role__c = 'new Job Title';
        esc2.Account_Origin_Lookup__c = acc2.Id;
        esc2.Description__c = 'test';
        esc2.Business_Case__c = 'test';
        esc2.Distribution_List__c = 'test';
        esc2.Qlik_Management_Approver__c = UserInfo.getUserId();

        escs.add(esc);
        escs.add(esc2);

        Test.startTest();
        insert escs;
        Test.stopTest();

        esc = [
                SELECT Id, Status__c, Account_Origin_Lookup__c, Contact_Role__c, CaseOwner__c
                FROM Qlikview_Support_Escalation__c
                WHERE ID = :esc.Id
        ];

        esc2 = [
                SELECT Id, Status__c, Account_Origin_Lookup__c, Contact_Role__c, CaseOwner__c
                FROM Qlikview_Support_Escalation__c
                WHERE ID = :esc2.Id
        ];

        System.assertEquals(cs.Account_Origin__c, esc.Account_Origin_Lookup__c);
        System.assertEquals(c.Job_Title__c, esc.Contact_Role__c);
        System.assertEquals('New', esc.Status__c);
        System.assertEquals(cs.OwnerId, esc.CaseOwner__c);

        System.assertNotEquals(cs.Account_Origin__c, esc2.Account_Origin_Lookup__c);
        System.assertNotEquals(c.Job_Title__c, esc2.Contact_Role__c);
        System.assertEquals('New', esc2.Status__c);
        System.assertEquals(cs.OwnerId, esc2.CaseOwner__c);

        esc.ContactName__c = c2.Id;
        esc.Contact_Role__c = 'test';
        update esc;

        esc = [
                SELECT Id, Contact_Role__c
                FROM Qlikview_Support_Escalation__c
                WHERE ID = :esc.Id
        ];
        System.assertEquals(c2.Job_Title__c, esc.Contact_Role__c);
    }
    static testMethod void testUpdateBugWeightInsert() {
        
        bugWeightsInEscalations = new List<Decimal>();
    
        List<Support_Bug_Weight_Calculations__mdt> bugWeightMdts = [select Case_Escalation_Proactive_Red__c, Case_Escalation_Proactive_Yellow__c, Case_Escalation_Reactive_Red__c, Case_Escalation_Reactive_Yellow__c from Support_Bug_Weight_Calculations__mdt where MasterLabel = 'Master'];
        system.assertEquals(bugWeightMdts.Size(), 1);
        
        bugWeightMdt = bugWeightMdts[0];
        
        List<Account> accounts = new List<Account>();
        Account acc = new Account(Name = 'Test Account');
        Account acc2 = new Account(Name = 'Test Account');
        accounts.add(acc);
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        Contact c = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title');
        contacts.add(c);
        insert contacts;
        
        Bugs__c bug = new Bugs__c();
        insert bug;
        
        Case cs = new Case();
        cs.Account_Origin__c = acc.Id;
        cs.ContactId = cs.Id;
        cs.Status = 'New';
        cs.Subject = 'New';
        cs.Severity__c = '1';
        cs.OwnerId = UserInfo.getUserId();
        cs.Bug__c = bug.Id;
        insert cs;
        
        Qlikview_Support_Escalation__c esc = new Qlikview_Support_Escalation__c();
        esc.Case__c = cs.Id;
        esc.ContactName__c = c.Id;
        esc.Description__c = 'Reactive Red';
        esc.Business_Case__c = 'test';
        esc.Distribution_List__c = 'test';
        //Value 1000
        esc.Rapid_Response_Escalation__c = false; 
        esc.Temperature__c = 'Red';

        Qlikview_Support_Escalation__c esc2 = new Qlikview_Support_Escalation__c();
        esc2.Case__c = cs.Id;
        esc2.ContactName__c = c.Id;
        esc2.Contact_Role__c = 'new Job Title';
        esc2.Account_Origin_Lookup__c = acc2.Id;
        esc2.Description__c = 'Reactive Yellow';
        esc2.Business_Case__c = 'test';
        esc2.Distribution_List__c = 'test';
        //Value 600
        esc2.Rapid_Response_Escalation__c = false; 
        esc2.Temperature__c = 'Yellow';
        
        Qlikview_Support_Escalation__c esc3 = new Qlikview_Support_Escalation__c();
        esc3.Case__c = cs.Id;
        esc3.ContactName__c = c.Id;
        esc3.Contact_Role__c = 'new Job Title';
        esc3.Account_Origin_Lookup__c = acc2.Id;
        esc3.Description__c = 'Proactive Red';
        esc3.Business_Case__c = 'test';
        esc3.Distribution_List__c = 'test';
        //Value 500
        esc3.Rapid_Response_Escalation__c = true; 
        esc3.Temperature__c = 'Red';
        
        Qlikview_Support_Escalation__c esc4 = new Qlikview_Support_Escalation__c();
        esc4.Case__c = cs.Id;
        esc4.ContactName__c = c.Id;
        esc4.Contact_Role__c = 'new Job Title';
        esc4.Account_Origin_Lookup__c = acc2.Id;
        esc4.Description__c = 'Proactive Yellow';
        esc4.Business_Case__c = 'test';
        esc4.Distribution_List__c = 'test';
        //Value 250
        esc4.Rapid_Response_Escalation__c = true; 
        esc4.Temperature__c = 'Yellow';
        
        Test.startTest();
        
        //250
        TestCaseEscalation(esc4, 'insert');
        //500
        TestCaseEscalation(esc3, 'insert');
        //600
        TestCaseEscalation(esc2, 'insert');
        //1000
        TestCaseEscalation(esc, 'insert');

        Test.stopTest();
    }
    static testMethod void testUpdateBugWeightUpdate() {
        
        bugWeightsInEscalations = new List<Decimal>();
    
        List<Support_Bug_Weight_Calculations__mdt> bugWeightMdts = [select Case_Escalation_Proactive_Red__c, Case_Escalation_Proactive_Yellow__c, Case_Escalation_Reactive_Red__c, Case_Escalation_Reactive_Yellow__c from Support_Bug_Weight_Calculations__mdt where MasterLabel = 'Master'];
        system.assertEquals(bugWeightMdts.Size(), 1);
        
        bugWeightMdt = bugWeightMdts[0];
        
        List<Account> accounts = new List<Account>();
        Account acc = new Account(Name = 'Test Account');
        Account acc2 = new Account(Name = 'Test Account');
        accounts.add(acc);
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        Contact c = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title');
        contacts.add(c);
        insert contacts;
        
        Bugs__c bug = new Bugs__c();
        insert bug;
        
        Case cs = new Case();
        cs.Account_Origin__c = acc.Id;
        cs.ContactId = cs.Id;
        cs.Status = 'New';
        cs.Subject = 'New';
        cs.Severity__c = '1';
        cs.OwnerId = UserInfo.getUserId();
        cs.Bug__c = bug.Id;
        insert cs;
        
        Qlikview_Support_Escalation__c esc = new Qlikview_Support_Escalation__c();
        esc.Case__c = cs.Id;
        esc.ContactName__c = c.Id;
        esc.Description__c = 'Reactive Red';
        esc.Business_Case__c = 'test';
        esc.Distribution_List__c = 'test';
        //Value 1000
        esc.Rapid_Response_Escalation__c = false; 
        esc.Temperature__c = 'Red';

        Qlikview_Support_Escalation__c esc2 = new Qlikview_Support_Escalation__c();
        esc2.Case__c = cs.Id;
        esc2.ContactName__c = c.Id;
        esc2.Contact_Role__c = 'new Job Title';
        esc2.Account_Origin_Lookup__c = acc2.Id;
        esc2.Description__c = 'Reactive Yellow';
        esc2.Business_Case__c = 'test';
        esc2.Distribution_List__c = 'test';
        //Value 600
        esc2.Rapid_Response_Escalation__c = false; 
        esc2.Temperature__c = 'Yellow';
        
        Qlikview_Support_Escalation__c esc3 = new Qlikview_Support_Escalation__c();
        esc3.Case__c = cs.Id;
        esc3.ContactName__c = c.Id;
        esc3.Contact_Role__c = 'new Job Title';
        esc3.Account_Origin_Lookup__c = acc2.Id;
        esc3.Description__c = 'Proactive Red';
        esc3.Business_Case__c = 'test';
        esc3.Distribution_List__c = 'test';
        //Value 500
        esc3.Rapid_Response_Escalation__c = true; 
        esc3.Temperature__c = 'Red';
        
        Qlikview_Support_Escalation__c esc4 = new Qlikview_Support_Escalation__c();
        esc4.Case__c = cs.Id;
        esc4.ContactName__c = c.Id;
        esc4.Contact_Role__c = 'new Job Title';
        esc4.Account_Origin_Lookup__c = acc2.Id;
        esc4.Description__c = 'Proactive Yellow';
        esc4.Business_Case__c = 'test';
        esc4.Distribution_List__c = 'test';
        //Value 250
        esc4.Rapid_Response_Escalation__c = true; 
        esc4.Temperature__c = 'Yellow';
        
        Test.startTest();
        
        Semaphores.CustomCaseTriggerBeforeInsert = true;
        Semaphores.CustomCaseTriggerAfterInsert = true;
        Semaphores.CustomCaseTriggerBeforeUpdate = true;
        Semaphores.CustomCaseTriggerAfterUpdate = true;
        
        list<Qlikview_Support_Escalation__c> escalations = new List<Qlikview_Support_Escalation__c>();
        escalations.add(esc2);
        escalations.add(esc3);
        escalations.add(esc4);
        insert escalations;
        
        bugWeightsInEscalations.add(getBugWeight(esc2));
        bugWeightsInEscalations.add(getBugWeight(esc3));
        
        
        esc4.Rapid_Response_Escalation__c = false; 
        esc4.Temperature__c = 'Red';
        
        
        TestCaseEscalation(esc4, 'update');

        Test.stopTest();
    }
    
    static testMethod void testUpdateBugWeightDelete() {
        
        bugWeightsInEscalations = new List<Decimal>();
    
        List<Support_Bug_Weight_Calculations__mdt> bugWeightMdts = [select Case_Escalation_Proactive_Red__c, Case_Escalation_Proactive_Yellow__c, Case_Escalation_Reactive_Red__c, Case_Escalation_Reactive_Yellow__c from Support_Bug_Weight_Calculations__mdt where MasterLabel = 'Master'];
        system.assertEquals(bugWeightMdts.Size(), 1);
        
        bugWeightMdt = bugWeightMdts[0];
        
        List<Account> accounts = new List<Account>();
        Account acc = new Account(Name = 'Test Account');
        Account acc2 = new Account(Name = 'Test Account');
        accounts.add(acc);
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        Contact c = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title');
        contacts.add(c);
        insert contacts;
        
        Bugs__c bug = new Bugs__c();
        insert bug;
        
        Case cs = new Case();
        cs.Account_Origin__c = acc.Id;
        cs.ContactId = cs.Id;
        cs.Status = 'New';
        cs.Subject = 'New';
        cs.Severity__c = '1';
        cs.OwnerId = UserInfo.getUserId();
        cs.Bug__c = bug.Id;
        insert cs;
        
        Qlikview_Support_Escalation__c esc = new Qlikview_Support_Escalation__c();
        esc.Case__c = cs.Id;
        esc.ContactName__c = c.Id;
        esc.Description__c = 'Reactive Red';
        esc.Business_Case__c = 'test';
        esc.Distribution_List__c = 'test';
        //Value 1000
        esc.Rapid_Response_Escalation__c = false; 
        esc.Temperature__c = 'Red';

        Qlikview_Support_Escalation__c esc2 = new Qlikview_Support_Escalation__c();
        esc2.Case__c = cs.Id;
        esc2.ContactName__c = c.Id;
        esc2.Contact_Role__c = 'new Job Title';
        esc2.Account_Origin_Lookup__c = acc2.Id;
        esc2.Description__c = 'Reactive Yellow';
        esc2.Business_Case__c = 'test';
        esc2.Distribution_List__c = 'test';
        //Value 600
        esc2.Rapid_Response_Escalation__c = false; 
        esc2.Temperature__c = 'Yellow';
        
        Qlikview_Support_Escalation__c esc3 = new Qlikview_Support_Escalation__c();
        esc3.Case__c = cs.Id;
        esc3.ContactName__c = c.Id;
        esc3.Contact_Role__c = 'new Job Title';
        esc3.Account_Origin_Lookup__c = acc2.Id;
        esc3.Description__c = 'Proactive Red';
        esc3.Business_Case__c = 'test';
        esc3.Distribution_List__c = 'test';
        //Value 500
        esc3.Rapid_Response_Escalation__c = true; 
        esc3.Temperature__c = 'Red';
        
        Qlikview_Support_Escalation__c esc4 = new Qlikview_Support_Escalation__c();
        esc4.Case__c = cs.Id;
        esc4.ContactName__c = c.Id;
        esc4.Contact_Role__c = 'new Job Title';
        esc4.Account_Origin_Lookup__c = acc2.Id;
        esc4.Description__c = 'Proactive Yellow';
        esc4.Business_Case__c = 'test';
        esc4.Distribution_List__c = 'test';
        //Value 250
        esc4.Rapid_Response_Escalation__c = true; 
        esc4.Temperature__c = 'Yellow';
        
        Test.startTest();
        
        Semaphores.CustomCaseTriggerBeforeInsert = true;
        Semaphores.CustomCaseTriggerAfterInsert = true;
        Semaphores.CustomCaseTriggerBeforeUpdate = true;
        Semaphores.CustomCaseTriggerAfterUpdate = true;
        
        list<Qlikview_Support_Escalation__c> escalations = new List<Qlikview_Support_Escalation__c>();
        escalations.add(esc);
        escalations.add(esc2);
        escalations.add(esc3);
        escalations.add(esc4);
        insert escalations;
        
        bugWeightsInEscalations.add(getBugWeight(esc));
        bugWeightsInEscalations.add(getBugWeight(esc2));
        bugWeightsInEscalations.add(getBugWeight(esc3));
        bugWeightsInEscalations.add(getBugWeight(esc4));
        
        TestCaseEscalation(esc2, 'delete');
        TestCaseEscalation(esc, 'delete');
        TestCaseEscalation(esc3, 'delete');
        TestCaseEscalation(esc4, 'delete');

        Test.stopTest();
    }
    public static Decimal getBugWeight(Qlikview_Support_Escalation__c escalation){
        if(escalation.Rapid_Response_Escalation__c == true && escalation.Temperature__c == 'Yellow')
            return bugWeightMdt.Case_Escalation_Proactive_Yellow__c;
        else if(escalation.Rapid_Response_Escalation__c == true && escalation.Temperature__c == 'Red')
            return bugWeightMdt.Case_Escalation_Proactive_Red__c;
        else if(escalation.Rapid_Response_Escalation__c == false && escalation.Temperature__c == 'Yellow')
            return bugWeightMdt.Case_Escalation_Reactive_Yellow__c;
        else if(escalation.Rapid_Response_Escalation__c == false && escalation.Temperature__c == 'Red')
            return bugWeightMdt.Case_Escalation_Reactive_Red__c;
        else 
            return 0;
            
    }
    public static Decimal getHighestBugWeight(){
        Decimal highestValue = 0;
        for(Decimal i : bugWeightsInEscalations){
            if(highestValue < i)
                highestValue = i;
        }
        return highestValue;
    }
    public static void TestCaseEscalation(Qlikview_Support_Escalation__c escalation, string operation){
        CaseBugWeightRecalculator.bugsToUpdate = new Map<Id, Bugs__c>();
        Semaphores.CustomCaseTriggerBeforeInsert = false;
        Semaphores.CustomCaseTriggerAfterInsert = false;
        Semaphores.CustomCaseTriggerBeforeUpdate = false;
        Semaphores.CustomCaseTriggerAfterUpdate = false;
        
        if(operation == 'insert') {
            insert escalation;
            bugWeightsInEscalations.add(getBugWeight(escalation));
        }
        if(operation == 'update') {
            update escalation;
            bugWeightsInEscalations.add(getBugWeight(escalation));
        }
        else if(operation == 'delete'){
            bugWeightsInEscalations.remove(bugWeightsInEscalations.indexOf(getBugWeight(escalation)));
            delete escalation;
        }
        
        Case cs = [select id, Bug_Weight_Case_Escalation__c from Case where id = :escalation.Case__c];
        system.debug('');
        system.debug(escalation.Description__c);
        system.debug('Operation: ' + operation);
        system.debug('Bug_Weight_Case_Escalation__c: ' + cs.Bug_Weight_Case_Escalation__c);
        system.debug('getHighestBugWeight: ' + getHighestBugWeight());
    }
}