/****************************************************** 

    Class: Test_Opportunity_ManageForecastProducts 

    This class tests the Opportunity_ManageForecastProducts trigger. 

    Changelog: 
    2013-03-15  CCE Initial Development 
    2014-05-07  SAN Based on SLH 2103-12-10 commented out - shoul drun for all revenue types (now OEM is included)
    2014-05-07  SND this unit test is obsolete
    2014-06-23  MHG Fixed build/validation errors for QTNext GoLive
    2014-12-01  TJG Add test method TestOEMPrepayCategoryIsOmitted for
                    CR# 17441 https://eu1.salesforce.com/a0CD000000lViDU
    Andrew Lokotosh 2016-06-08 Comented Forecast_Amount fields line 119-122,140,175-178,293-296,334-337,357-360,424                
    2017-24-03  Roman Dovbush (4front) Changed hardcoded record types to SOQL.
    2017-07-18  Aslam QCW-1697 test method update
	2020-04-20   Leonardo Grauer - OTQ 399 - Record types deactivation
    
******************************************************/ 
@isTest
private class Test_Opportunity_ManageForecastProducts {

    //Opportunity record types
    static Id OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id;//QTNS '012f0000000Cugl';  //NetSuite - old- 012D0000000KEKO, old 012M00000000LdR
    static Id OEMRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('OEM_Order').Id;  //OEM - Order
    //Account record types
    static Id AccRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id;  //End User Account
    
    //Create an Opp - this should throw an exception until we set a forecast amount

    /*static void SetupData()
    {
        //QTTestUtils.GlobalSetUp();

        Pricebook2 nspb = new Pricebook2();
        nspb.Name = 'NS Pricebook';
        insert nspb;

        Id standardPriceBookId = Test.getStandardPricebookId();

        List<Product2> products = new List<Product2>();
        Product2 p1 = new Product2(Name='Licenses', isActive=true, Family='Licenses');
        products.add(p1);
        Product2 p2 = new Product2(Name='Consulting', isActive=true, Family='Consulting');
        products.add(p2);
        Product2 p3 = new Product2(Name='Education', isActive=true, Family='Education');
        products.add(p3);
        Product2 p4 = new Product2(Name='Support Services', isActive=true, Family='Support Services');
        products.add(p4);
        insert products;

        
        
        PriceBook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];
            
        // set up PricebookEntry and Verify that the results are as expected.
        List<PricebookEntry> entries = new List<PricebookEntry>();
        PricebookEntry pbe1 = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p1, 'USD');
        PricebookEntry pbe2 = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p2, 'USD');
        PricebookEntry pbe3 = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p3, 'USD');
        PricebookEntry pbe4 = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p4, 'USD');


        
        //PricebookEntry pbe2 = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p3, 'GBP');

        NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = nspb.Id;
        settings.UserRecordTypeIdsToExclude__c = '005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
        insert settings;

        QTCustomSettings__c settings2 = new QTCustomSettings__c();
        settings2.Name = 'Default';
        settings2.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        settings2.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        settings2.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert settings2;
    }*/

    static testMethod void TestOppCreation() {
        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: Starting');

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('USD');
      /*   
        PriceBook2 pb2NS = [select Id from Pricebook2 where Name = 'SteelBrick Price book'];

        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.Exclude_From_OppUpdatePrice_Trigger__c = pb2Ns.Id;
        upsert settings Name;

        ////  This may be deleted after QTNext GoLive  -- End
        
*/
        // Adding an account with BillingCountryCode and Qliktech Company
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
        insert QTComp;
        
        Account acc = new Account(
            Name = 'Test',
            RecordTypeId = AccRecordTypeId, //End User Account
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        test.startTest();
        Opportunity Opp = new Opportunity(
            Name = 'Test Opp PF',
            StageName = 'Goal Identified',
            ForecastCategoryName = 'Pipeline',
            CloseDate = Date.today(),
            AccountId = acc.Id,
            RecordTypeId = OppRecordTypeId,
            Type = 'Existing Customer',
            Revenue_Type__c = 'Direct',
          //  License_Forecast_Amount__c = null,
           // Consultancy_Forecast_Amount__c = null,
           // Education_Forecast_Amount__c = null,
            //Support_Forecast_Amount__c = null,
            Signature_Type__c = 'Digital Signature',
            CurrencyIsoCode = 'USD'
        );
        
        boolean CaughtException = false;
        try {
            //we should get an exception now as we have not added any value to any of the Forecast amount fields
            System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: inserting Opp');
            insert Opp;
        } catch (System.Exception e) {
            CaughtException = true;
            System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: Caught exception: ' + e.getDmlMessage(0));
        }
        //System.assertEquals(true, CaughtException, 'We should have got an exception');
        
        //insert the Opp as it wasn't inserted previously due to the exception    

        //Opp.License_Forecast_Amount__c = 1000.0;
        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: insert Opp');
        System.Debug('Test_Opportunity_ManageForecastProducts: Semaphores.Opportunity_ManageForecastProductsHasRun_Before=' + Semaphores.Opportunity_ManageForecastProductsHasRun_Before);
        System.Debug('Test_Opportunity_ManageForecastProducts: Semaphores.Opportunity_ManageForecastProductsHasRun_After=' + Semaphores.Opportunity_ManageForecastProductsHasRun_After);
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
        //insert Opp;
                
        //As we added a License amount we should get a License product added so test the 'Licenses' product was added      
        //OpportunityLineItem resOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :Opp.Id];                
        //System.assertEquals('Licenses', resOli.Name__c, 'Opportunity Line Item was not created');
                
        //we now have to add an SOI to the Opp or we are stopped by a validation rule
        Contact cont = new Contact();
        cont.FirstName = 'test';
        cont.LastName = 'test2';
        Insert cont;
        //Contact cont = [select Id, Name from Contact LIMIT 1];
        
        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: Update the SOI');
        Sphere_of_Influence__c sph = new Sphere_of_Influence__c(
            Opportunity__c = Opp.Id, 
            Contact__c = cont.Id,
            Role__c = 'Decision Maker;User');
        insert sph;
        
        //reset the Semaphore flag - we have to do this manually while testing otherwise we will skip the 'after update'
        System.Debug('Test_Opportunity_ManageForecastProducts: Semaphores.Opportunity_ManageForecastProductsHasRun_Before=' + Semaphores.Opportunity_ManageForecastProductsHasRun_Before);
        System.Debug('Test_Opportunity_ManageForecastProducts: Semaphores.Opportunity_ManageForecastProductsHasRun_After=' + Semaphores.Opportunity_ManageForecastProductsHasRun_After);
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
            
        //now update the Amount on the Licenses oli and create a new 'Support' oli
        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: Update the Opp');
    //    Opp.License_Forecast_Amount__c = 100.00;
     //   Opp.Consultancy_Forecast_Amount__c = 123.00;
      //  Opp.Education_Forecast_Amount__c = 456.00;
      //  Opp.Support_Forecast_Amount__c = 99.00;
        update Opp;
        
        //test the Amount field 
        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: Check the Amount');
        Opportunity testOpp2 = [SELECT Id, Amount, License_Forecast_Amount__c, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c FROM Opportunity WHERE Id = :Opp.Id];
        
        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: testOpp2 = ' + testOpp2);  
        //System.assertEquals(778.00, testOpp2.Amount, 'Wrong Opp Amount');
        
        //test we now have 4 OpportunityLineItems on the Opp
        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: Update the oli list');
        list<OpportunityLineItem> listOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :testOpp2.Id];
        //System.assertEquals(4, listOli.size(), 'Wrong number of oli');
        
        test.stopTest();

        System.debug('Test_Opportunity_ManageForecastProducts>TestOppCreation: Finished');  
    }
    
    //Create an Opp with an OEM record type - we should skip out of the trigger before forcing the user to add forecast amounts
    //This for code coverage.
    // SAN 2014-05-07 Based on SLH 2103-12-10 commented out - shoul drun for all revenue types (now OEM is included)
    // SAN 2014-05-07 this unit test is obsolete

    //static testMethod void TestSkipWhenOppCreatedWithWrongRecordType() {
    //    System.debug('Test_Opportunity_ManageForecastProducts>TestSkipWhenOppCreatedWithWrongRecordType: Starting');
         
    //    // Adding an account with BillingCountryCode and Qliktech Company
    //    QlikTech_Company__c QTComp = new QlikTech_Company__c(
    //        Name = 'GBR',
    //        QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
    //    );
    //    insert QTComp;
    //    QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
    //    Account acc = new Account(
    //        Name = 'Test',
    //        RecordTypeId = AccRecordTypeId, //End User Account
    //        QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
    //        Billing_Country_Code__c = QTComp.Id);
    //    insert acc;
        
    //    test.startTest();
    //    Opportunity Opp = new Opportunity(
    //        Name = 'Test Opp PF',
    //        Short_Description__c = 'TestOpp - delete me',
    //        StageName = 'OEM - Identified by Partner',
    //        CloseDate = Date.today(),
    //        AccountId = acc.Id,
    //        RecordTypeId = OEMRecordTypeId,
    //        Type = 'New Customer',
    //        Revenue_Type__c = 'OEM'     
    //    );
    //    insert Opp;        
    //    System.assertEquals(Opp.Short_Description__c, 'TestOpp - delete me');
        
    //    test.stopTest();

    //    System.debug('Test_Opportunity_ManageForecastProducts>TestSkipWhenOppCreatedWithWrongRecordType: Finished');    
    //}
    
    //Create an Reseller Opp - add forecast amounts and check products are create, remove forecast amount and check products are removed
    static testMethod void TestAddingAndRemovingProductsWhenOppCreatedReseller() {
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Starting');

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('USD');         
        
        ////  This may be deleted after QTNext GoLive  -- Start        
        //QTCustomSettings__c settings = new QTCustomSettings__c();
        //settings.Name = 'Default';
        //settings.Exclude_From_OppUpdatePrice_Trigger__c = '01sD0000000JOeZ';
        //upsert settings Name;
        ////  This may be deleted after QTNext GoLive  -- End

        PriceBook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];

        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.Exclude_From_OppUpdatePrice_Trigger__c = pb2Ns.Id;
        upsert settings Name;
        
         
        // Adding an account with BillingCountryCode and Qliktech Company
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
        insert QTComp;
        
        Account acc = new Account(
            Name = 'Test',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = acc.Id
        );
        insert PartnerContact;
            
        Opportunity Opp = new Opportunity(
            Name = 'Test Opp PF',
            StageName = 'Goal Identified',
            ForecastCategoryName = 'Pipeline',
            CloseDate = Date.today().addDays(30),
            AccountId = PartnerContact.AccountId,
            RecordTypeId = OppRecordTypeId,
            Type = 'Existing Customer',
            Revenue_Type__c = 'Reseller',
            Sell_Through_Partner__c = PartnerContact.AccountId,
            Partner_Contact__c = PartnerContact.Id,
          //  License_Forecast_Amount__c = 100.00,
          //  Consultancy_Forecast_Amount__c = 69.00,
          //  Education_Forecast_Amount__c = 1.00,
          //  Support_Forecast_Amount__c = 30.00,
            Signature_Type__c = 'Digital Signature',
            CurrencyIsoCode = 'USD',            
            Next_step_custom__c = 'Test Comment',
            Manager_Comments__c = 'Manager test comments'
        );
        insert Opp;        
        
        test.startTest();        
        //test the Amount field 
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Check the Amount');
        Opportunity testOpp2 = [SELECT Id, Amount, License_Forecast_Amount__c, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c FROM Opportunity WHERE Id = :Opp.Id];
        
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: testOpp2 = ' + testOpp2);  
        //System.assertEquals(200.00, testOpp2.Amount, 'Wrong Opp Amount');
        
        //test we now have 4 OpportunityLineItems on the Opp
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Update the oli list');
        list<OpportunityLineItem> listOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :testOpp2.Id];
        //System.assertEquals(4, listOli.size(), 'Wrong number of oli');
        
        //test the default 'Licenses' product was added      
        //OpportunityLineItem resOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :Opp.Id];                
        //System.assertEquals('Licenses', resOli.Name__c, 'Opportunity Line Item was not created');
                
        //we now have to add an SOI to the Opp or we are stopped by a validation rule
        //Contact cont = [select Id, Name from Contact LIMIT 1];
        
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Update the SOI');
        /*Sphere_of_Influence__c sph = new Sphere_of_Influence__c(
            Opportunity__c = Opp.Id, 
            Contact__c = PartnerContact.Id,
            Role__c = 'Decision Maker;User');
        insert sph;*/
        
        //reset the Semaphore flag - we have to do this manually while testing otherwise we will skip the 'after update'
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
        
        //now update the forecast amounts - this should update the products
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Update the products');
        //Opp.License_Forecast_Amount__c = 111.00;
        //Opp.Consultancy_Forecast_Amount__c = 222.00;
        //Opp.Education_Forecast_Amount__c = 333.00;
        //Opp.Support_Forecast_Amount__c = 444.00;
        update Opp;
        
        //test the Amount field has been updated
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Check the Amount');
        Opportunity testOpp3 = [SELECT Id, Amount, License_Forecast_Amount__c, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c FROM Opportunity WHERE Id = :Opp.Id];
        
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: testOpp3 = ' + testOpp3);  
        //System.assertEquals(1110.00, testOpp3.Amount, 'Wrong Opp Amount');
        
        //test we now have 4 OpportunityLineItems on the Opp
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Update the oli list');
        list<OpportunityLineItem> listOli3 = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :testOpp3.Id];
        //System.assertEquals(4, listOli3.size(), 'Wrong number of oli');
            
        //reset the Semaphore flag - we have to do this manually while testing otherwise we will skip the 'after update'
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
        
        //now clear the forecast amounts - this should remove the products
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Update the Opp');
        //Opp.License_Forecast_Amount__c = null;
        //Opp.Consultancy_Forecast_Amount__c = null;
        //Opp.Education_Forecast_Amount__c = 0.00;
        //Opp.Support_Forecast_Amount__c = null;
        update Opp;
        
        //test the Amount field 
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Check for 0 products');
        Opportunity testOpp4 = [SELECT Id, Amount, License_Forecast_Amount__c, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c FROM Opportunity WHERE Id = :Opp.Id];
        
        //test we now have 0 OpportunityLineItems on the Opp
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Update the oli list');
        list<OpportunityLineItem> listOli4 = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :testOpp4.Id];
        //System.assertEquals(0, listOli4.size(), 'Wrong number of oli');
        
        //having set the forecast amounts to zero we should not now be able to re-save it with the amounts still set to zero
        
        //reset the Semaphore flag - we have to do this manually while testing otherwise we will skip the 'after update'
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
        
        //now clear the forecast amounts - this should remove the products
        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Re-save the Opp');
        
        boolean CaughtException = false;
        try {
            //we should get an exception now as we Forecast amount fields have previously been set to zero
            update Opp;
        } catch (System.Exception e) {
            CaughtException = true;
            System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Caught exception: ' + e.getDmlMessage(0));
        }
        //System.assertEquals(true, CaughtException, 'We should have got an exception');
        
        test.stopTest();

        System.debug('Test_Opportunity_ManageForecastProducts>TestAddingAndRemovingProductsWhenOppCreatedReseller: Finished'); 
    }


    // Make sure the Forecast Category does not include any other value than Omitted for OEM Prepay opportunities
    
	//2020-04-20 - OTQ 399 - Record types deactivation - OEM Prepay
	/*static testMethod void TestOEMPrepayCategoryIsOmitted() {

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('USD');

        User userStd = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Sales Std User');
        Account testAcnt = QTTestUtils.createMockAccount('Imperial Venture', userStd, true);
        // OEM Prepay recordtype ID 01220000000DNwZAAW
        
        //Opportunity prepayOpp = QTTestUtils.createMockOpportunity('Test for OEM Prepay', 'Test OEM Prepay Opp - Delte me', 
        //    'New Customer', 'Direct', 'Alignment Meeting', '01220000000DNwZAAW', 'USD', userStd, true);
        

        RecordType rt = [select Id from recordtype where Name like '%OEM Prepay%' limit 1];
        
        Opportunity prepayOpp = New Opportunity (
            Short_Description__c = 'Test OEM Prepay Opp',
            Name = 'Test for OEM Prepay',
            Type = 'New Customer',
            Revenue_Type__c = 'Direct',
            CloseDate = Date.today().addDays(7),
            StageName = 'Goal Identify', 
            RecordTypeId = rt.Id,    //  '01220000000DNwZAAW', 
            ForecastCategoryName = 'Pipeline',
            AccountId = testAcnt.Id,
            License_Amount_USD__c = 1000, 
           // License_Forecast_Amount__c = 1000,
            Signature_Type__c = 'Digital Signature',
            CurrencyIsoCode = 'USD',
            Next_step_custom__c = 'Test Comment',
            Manager_Comments__c = 'Manager test comments'
            ); 

        Test.startTest();
        
        insert prepayOpp;

        System.debug('TestOEMPrepayCategoryIsOmitted ---- ' + prepayOpp.ForecastCategoryName);

        prepayOpp = [select Id, ForecastCategoryName from Opportunity where Id = :prepayOpp.Id];

        System.assertEquals('Pipeline', prepayOpp.ForecastCategoryName);

        prepayOpp.ForecastCategoryName = 'Closed';

        update prepayOpp;

        prepayOpp = [select Id, ForecastCategoryName from Opportunity where Id = :prepayOpp.Id];

        System.debug('TestOEMPrepayCategoryIsOmitted ---- ' + prepayOpp.ForecastCategoryName);

        System.assertEquals('Closed', prepayOpp.ForecastCategoryName);

        Test.stopTest();
    }*/
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*@isTest (SeeAllData=true)
private class Test_Opportunity_ManageForecastProducts {

    //Opportunity record types
    static Id OppRecordTypeId = '01220000000J1KR';  //Qlikbuy CCS Standard - this will change for NetSuite
    static Id OEMRecordTypeId = '01220000000DXqj';  //OEM - Standard Sales Process 
    //Account record types
    static Id AccRecordTypeId = '01220000000DOFu';  //End User Account
    
    //Create an Opp - this will create a default product on it (Licenses) with UnitPrice as defined by custom settings in the Org.
    static testMethod void TestAutomaticallyAddingProductsWhenOppCreated() {
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: Starting');
         
        // Adding an account with BillingCountryCode and Qliktech Company
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(
            Name = 'Test',
            RecordTypeId = AccRecordTypeId, //End User Account
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        test.startTest();
        Opportunity Opp = new Opportunity(
            Name = 'Test Opp PF',
            StageName = 'Goal Identified',
            CloseDate = Date.today(),
            AccountId = acc.Id,
            RecordTypeId = OppRecordTypeId, //Qlikbuy CCS Standard - this will change for NetSuite
            Type = 'Existing Customer',
            Revenue_Type__c = 'Direct',
            License_Forecast_Amount__c = null,
            Consultancy_Forecast_Amount__c = null,
            Education_Forecast_Amount__c = null,
            Support_Forecast_Amount__c = null       
        );
        insert Opp;        
        
        //test the default 'Licenses' product was added      
        OpportunityLineItem resOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :Opp.Id];                
        System.assertEquals('Licenses', resOli.Name__c, 'Opportunity Line Item was not created');
                
        //we now have to add an SOI to the Opp or we are stopped by a validation rule
        Contact cont = [select Id, Name from Contact LIMIT 1];
        
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: Update the SOI');
        Sphere_of_Influence__c sph = new Sphere_of_Influence__c(
            Opportunity__c = Opp.Id, 
            Contact__c = cont.Id,
            Role__c = 'Decision Maker;User');
        insert sph;
        
        //reset the Semaphore flag - we have to do this manually while testing otherwise we will skip the 'after update'
        Semaphores.Opportunity_ManageForecastProductsHasRun = false;
            
        //now update the Amount on the Licenses oli and create a new 'Support' oli
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: Update the Opp');
        Opp.License_Forecast_Amount__c = 100.00;
        Opp.Consultancy_Forecast_Amount__c = 123.00;
        Opp.Education_Forecast_Amount__c = 456.00;
        Opp.Support_Forecast_Amount__c = 99.00;
        update Opp;
        
        //test the Amount field 
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: Check the Amount');
        Opportunity testOpp2 = [SELECT Id, Amount, License_Forecast_Amount__c, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c FROM Opportunity WHERE Id = :Opp.Id];
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: testOpp2 = ' + testOpp2);  
        System.assertEquals(778.00, testOpp2.Amount, 'Wrong Opp Amount');
        
        //test we now have 4 OpportunityLineItems on the Opp
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: Update the oli list');
        list<OpportunityLineItem> listOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :testOpp2.Id];
        System.assertEquals(4, listOli.size(), 'Wrong number of oli');
        
        test.stopTest();

        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: Finished');    
    }
    
    //Create an Opp with an OEM record type - we should skip out of the trigger before creating any products
    //This for code coverage.
    static testMethod void TestSkipWhenOppCreatedWithWrongRecordType() {
        System.debug('Test_Opportunity_ManageForecastProducts>TestSkipWhenOppCreatedWithWrongRecordType: Starting');
         
        // Adding an account with BillingCountryCode and Qliktech Company
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(
            Name = 'Test',
            RecordTypeId = AccRecordTypeId, //End User Account
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        test.startTest();
        Opportunity Opp = new Opportunity(
            Name = 'Test Opp PF',
            StageName = 'OEM - Identified by Partner',
            CloseDate = Date.today(),
            AccountId = acc.Id,
            RecordTypeId = OEMRecordTypeId, //OEM - Standard Sales Process
            Type = 'New Customer',
            Revenue_Type__c = 'OEM'     
        );
        insert Opp;        
        
        //test the default 'Licenses' product was not added by catching the 'no-rows' exception     
        Boolean CaughtException = false;
        try
        {
            OpportunityLineItem resOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :Opp.Id];
        }
        catch (System.Exception Ex)
        {
            CaughtException = true;
            System.debug('CaughtException: ' + Ex.getMessage());
        }
        System.assertEquals(true, CaughtException);
                    
        test.stopTest();

        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreated: Finished');    
    }
    
    //Create an Reseller Opp - this will create a default product on it (Licenses) with UnitPrice as defined by custom settings in the Org.
    static testMethod void TestAutomaticallyAddingProductsWhenOppCreatedReseller() {
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Starting');
         
        // Adding an account with BillingCountryCode and Qliktech Company
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(
            Name = 'Test',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = acc.Id
        );
        insert PartnerContact;
            
        test.startTest();
        Opportunity Opp = new Opportunity(
            Name = 'Test Opp PF',
            StageName = 'Goal Identified',
            CloseDate = Date.today().addDays(30),
            AccountId = PartnerContact.AccountId,
            RecordTypeId = OppRecordTypeId, //Qlikbuy CCS Standard - this will change for NetSuite
            Type = 'Existing Customer',
            Revenue_Type__c = 'Reseller',
            Sell_Through_Partner__c = PartnerContact.AccountId,
            Partner_Contact__c = PartnerContact.Id,
            License_Forecast_Amount__c = null,
            Consultancy_Forecast_Amount__c = null,
            Education_Forecast_Amount__c = null,
            Support_Forecast_Amount__c = null       
        );
        insert Opp;        
        
        //test the default 'Licenses' product was added      
        OpportunityLineItem resOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :Opp.Id];                
        System.assertEquals('Licenses', resOli.Name__c, 'Opportunity Line Item was not created');
                
        //we now have to add an SOI to the Opp or we are stopped by a validation rule
        Contact cont = [select Id, Name from Contact LIMIT 1];
        
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Update the SOI');
        Sphere_of_Influence__c sph = new Sphere_of_Influence__c(
            Opportunity__c = Opp.Id, 
            Contact__c = cont.Id,
            Role__c = 'Decision Maker;User');
        insert sph;
        
        //reset the Semaphore flag - we have to do this manually while testing otherwise we will skip the 'after update'
        Semaphores.Opportunity_ManageForecastProductsHasRun = false;
            
        //now update the Amount on the Licenses oli and create a new 'Support' oli
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Update the Opp');
        Opp.License_Forecast_Amount__c = 100.00;
        Opp.Support_Forecast_Amount__c = 123.00;
        update Opp;
        
        //test the Amount field 
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Check the Amount');
        Opportunity testOpp2 = [SELECT Id, Amount, License_Forecast_Amount__c, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, Support_Forecast_Amount__c FROM Opportunity WHERE Id = :Opp.Id];
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: testOpp2 = ' + testOpp2);  
        System.assertEquals(223.00, testOpp2.Amount, 'Wrong Opp Amount');
        
        //test we now have 4 OpportunityLineItems on the Opp
        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Update the oli list');
        list<OpportunityLineItem> listOli = [SELECT Id, OpportunityId, Name__c, UnitPrice from OpportunityLineItem WHERE OpportunityId = :testOpp2.Id];
        System.assertEquals(4, listOli.size(), 'Wrong number of oli');
        
        test.stopTest();

        System.debug('Test_Opportunity_ManageForecastProducts>TestAutomaticallyAddingProductsWhenOppCreatedReseller: Finished');    
    }
    
}*/