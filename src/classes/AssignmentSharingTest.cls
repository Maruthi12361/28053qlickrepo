/********
* NAME :AssignmentSharingTest
* Description: Tests for Assignmment Sharing 
* 
*
*Change Log:
    TJG     2016-01-06   Partner sharing project, Created test class
    ext_vos     2017-11-09   CR#CHG0031444 Add required fields for pse__Proj__c creation.
******/
@isTest
public class AssignmentSharingTest {
	static final integer NBR_OF_ACCOUNTS = 10;
	static final Id PortalRoleId = '00E20000000vrJSEAY';
	static final Id AdminProfId = '00e20000000yyUzAAI';

    private static testMethod void testUpdateSharing(){
        ApexSharingRules.TestingParnershare = true;
        //QTTestUtils.GlobalSetUp();
        Test.startTest();
        QTCustomSettings__c setting = new QTCustomSettings__c(
        Name = 'Default', 
        eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO',
        OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
        Insert setting;

        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        User me = [ select Id from User where Id = :UserInfo.getUserId() ];    
        User ownerUser = createSysAdminUser(AdminProfId, PortalRoleId, me);
        List<Contact> ctList = new List<Contact>();

        List<Account> lsAccounts = createPartnerAccounts(ctList, ownerUser, qtComp);
        /*
		Account myAccount  = createPartnerAccount('MyTestAccount', ownerUser, qtComp, false);
        Account partnerAcc = createPartnerAccount('MyPartnerAcc',  ownerUser, qtComp, false);
        List<Account> pAccounts = new List<Account>();
        pAccounts.add(myAccount);
        pAccounts.add(partnerAcc);
        Insert pAccounts;

        Contact ct  = new Contact(
                AccountId = partnerAcc.Id,
                lastname = 'Contact1',
                firstname = 'Test',
                email = 'ops.test@test.com.sandbox',
                pse__Is_Resource__c = true,
                pse__Is_Resource_Active__c = true
            );
        Contact ct0 = new Contact(
                AccountId = myAccount.Id, 
                lastname = 'Contact0',
                firstname = 'SysAdmin',
                email = 'sys.admin@test.com.sandbox',
                pse__Is_Resource__c = true,
                pse__Is_Resource_Active__c = true
            );
 
        ctList.add(ct0);
        ctlist.add(ct);
        Insert ctList;
        */
        //List<Account> testAccounts = createAccounts(ownerUser);

        List<User> ppUsers = createUserFromProfileId('00e20000001OyLwAAK', ctList, me); 

        /////User myUser = createMockPatnerUser();
        //User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<Account> myAccounts = new List<Account>();
        List<Contact> myContacts = createTestContacts(ownerUser, qtComp, myAccounts); /////createTestContacts(myUser, qtComp, myAccounts);       
  
        List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();

        List<pse__Proj__c> lsProj = new List<pse__Proj__c>();
        List<pse__Permission_Control__c> lsPermset = new List<pse__Permission_Control__c>();
        pse__Practice__c practice = new pse__Practice__c(Name = 'test');
        insert practice;

        Integer iLen = myContacts.size();
        for(Integer i = 0; i < iLen; i++)
        {
			pse__Proj__c project = new pse__Proj__c(
                Name = 'Test Project'+i, 
                pse__Stage__c = 'Open', 
                pse__Account__c = myAccounts[i].Id,
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                pse__Practice__c = practice.Id,
                Invoicing_Type__c = 'Deferred');
            lsProj.add(project);
    
            pse__Permission_Control__c pc = new pse__Permission_Control__c(pse__User__c = UserInfo.getUserId(),
                pse__Resource__c = ctList[i].Id, pse__Cascading_Permission__c = true, pse__Staffing__c = true);
            lsPermset.add(pc);
        }
        Insert lsProj;

        for(Integer i = 0; i<iLen; i++)
        {
            pse__Permission_Control__c pc = new pse__Permission_Control__c(pse__User__c = UserInfo.getUserId(),
                pse__Project__c = lsProj[i].Id, pse__Cascading_Permission__c = true, pse__Staffing__c = true);
            lsPermset.add(pc);
        }
        Insert lsPermset;
    
        // setup assignments
        for (Integer i = 0; i < iLen; i ++) {
            pse__Assignment__c assignment = new pse__Assignment__c(Name = 'Test Assignment ' + i, 
                pse__Project__c = lsProj[i].Id, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                pse__Resource__c = ctList[i].Id);
            assignments.add(assignment);
        }        	

        Insert assignments;
        
        //Set<pse__Assignment__c> asmSet = new Set<pse__Assignment__c>();
        //for (pse__Assignment__c assig : assignments)
        //{
        //	asmSet.add(assig);
        //}

        //AssignmentSharing.UpdateAssignmentSharing(asmSet);
        Test.stopTest();
        List<Id> asmIds = new List<Id>();
        for (pse__Assignment__c assignm : assignments)
        {
        	asmIds.add(assignm.Id);
        }

        List<pse__Assignment__Share> asmShares = [select Id, UserOrGroupId, ParentId, AccessLevel
        	from pse__Assignment__Share where RowCause = 'Manual' and ParentId in : asmIds];
        System.debug('asmShares ' + asmShares);
        System.assertNotEquals(0, asmShares.size());

        for (Integer i = 2; i < iLen; i++)
        {
            if (lsPermset[i].pse__Resource__c != null)
                lsPermset[i].pse__Resource__c = ctList[i-2].Id;
        }

        Update lsPermset;

        for (Integer i = 2; i < iLen; i++)
        {
            assignments[i].pse__Resource__c = ctList[i-2].Id;
        }

        //Update assignments;

        
    }

    private static List<Account> createAccounts(User user)
    {
        List<Account> nuAccounts = new List<Account>();
        for (Integer i=0; i< NBR_OF_ACCOUNTS; i++) {
            Account acc = QTTestUtils.createMockAccount('AsmSharing Test Inc [' + (i+1) + ']', user, false);
            nuAccounts.add(acc);
        }
        Insert nuAccounts;
        return nuAccounts;
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me) {
        User u = new User(alias = 'adminUsr', email='adminUser@tjtest.com.test',
                Emailencodingkey='UTF-8', lastname='adminTest', 
                Languagelocalekey='en_US', localesidkey='en_US',
                Profileid = profId,
                Timezonesidkey='America/Los_Angeles', 
                //ContactId = cont.Id,
                UserRoleId = roleId,
                Username= System.now().millisecond() + '_' + '_newuser@jttest.com.test');
        System.runAs(me)
        {
            insert u;
        }
        return u;
    }

    private static User createMockPatnerUser(User me){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'Partner'and PortalRole = 'Executive' Limit 1];
        System.debug('portalRole ' + portalRole);
        User aUser;
        System.runAs(me)
        {

            aUser = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');
            aUser.IsPortalEnabled = true;
            update aUser;
        }
        return aUser;
    }

    private static List<Contact> createTestContacts(User aUser, QlikTech_Company__c qtComp, List<Account> lsAccounts)
    {
        List<Contact> con = new List<Contact>();
        User user1 = createMockPatnerUser(aUser);
        Boolean oddIndex = false;
        System.runAs(aUser){
            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                //Create account

                Account pAccount = new Account(Name = 'partneracc' + i,
		            OwnerId = aUser.Id,
		            Billing_Country_Code__c = qtComp.Id,
		            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
		            Navision_Customer_Number__c = '12345', 
		            RecordTypeId = '01220000000DOFzAAO',  // Partner Account
		            Legal_Approval_Status__c = 'Legal Approval Granted', 
		            BillingStreet = '31 Hight Street',
		            BillingState ='Berks',
		            BillingPostalCode = 'RG2 5ST',
		            BillingCountry = 'United Kingdom',
		            BillingCity = 'Reading',           
		            Shipping_Country_Code__c = QTComp.Id,
		            ShippingStreet = '31 Hight Street',
		            ShippingState ='Berks',
		            ShippingPostalCode = 'RG2 5ST',
		            ShippingCountry = 'United Kingdom',
		            ShippingCity = 'Reading',
		            From_Lead_Conversion__c = false
                );
                if (oddIndex)
                {
                    pAccount.Owner = user1;
                }
                oddIndex = !oddIndex;
                lsAccounts.add(pAccount);  
            }
            Insert lsAccounts;

            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Contact contact = new Contact( FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = lsAccounts[i].Id,
                pse__Is_Resource__c = true,
                pse__Is_Resource_Active__c = true,
                Email = 'tc' + i + '@test.com');
                con.add(contact);  
            }
            insert con;
        } 
        return con;
    }

    public static List<User> createUserFromProfileId(Id profId, List<Contact> cont, User me) {
        List<User> usrs = new List<User>();
        System.runAs ( me ) {
            integer ndx = 1;
            for (Contact ct : cont)
            {
                User u = new User(alias = 'newUser', email='puser'+ ndx + '@tjtest.com.test',
                    Emailencodingkey='UTF-8', lastname='Testing', 
                    Languagelocalekey='en_US', localesidkey='en_US',
                    Profileid = profId,
                    Timezonesidkey='America/Los_Angeles',
                    ContactId = ct.Id,
                    Username= 'asuser' + ndx + '@tjtest.com.test');
                usrs.add(u);
                ndx++;
            }
            insert usrs;
            for (integer i=0; i<usrs.size(); i++)
            {
                usrs[i].IsPortalEnabled = true;                        
            }
            update usrs;
        }
        return usrs;
    } 

    public static List<Account> createPartnerAccounts(List<Contact> ctList, User user, QlikTech_Company__c qtComp)
    {
        List<Account> nuAccounts = new List<Account>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++)
        {
            Account acc = new Account(  
                Name = 'AsmTest' + i, 
                OwnerId = user.Id,
                Billing_Country_Code__c = qtComp.Id,
                QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                Navision_Customer_Number__c = '12345', 
                RecordTypeId = '01220000000DOFzAAO',  // Partner Account
                Legal_Approval_Status__c = 'Legal Approval Granted', 
                BillingStreet = '31 Hight Street',
                BillingState ='Berks',
                BillingPostalCode = 'RG2 5ST',
                BillingCountry = 'United Kingdom',
                BillingCity = 'Reading',           
                Shipping_Country_Code__c = QTComp.Id,
                ShippingStreet = '31 Hight Street',
                ShippingState ='Berks',
                ShippingPostalCode = 'RG2 5ST',
                ShippingCountry = 'United Kingdom',
                ShippingCity = 'Reading',
                From_Lead_Conversion__c = false
            );
            nuAccounts.add(acc);
        }
        Insert nuAccounts;

        for(Integer i = 0; i < NBR_OF_ACCOUNTS; i++)
        {
            Contact contact = new Contact( 
                FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = nuAccounts[i].Id,
                pse__Is_Resource__c = true,
                pse__Is_Resource_Active__c = true,
                Email = 'Asm' + i + '@test.com');
            ctList.add(contact);  
        }
        insert ctList;
        return nuAccounts;
    }
}