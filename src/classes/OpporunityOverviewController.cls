/*
 *  OpporunityOverviewController
 *
 *	Controller for the different Page that handles the different
 *  opportunity Aging Views.
 *
 *	Changelog:
 *		2010-03-18	MHG		First version developed
 *		2010-10-04	MHG		CR# 431 - Goal Confirmed aging view (https://emea.salesforce.com/a0C2000000C83OP)
 *		2011-02-18	MHG		CR# 1468 - Home Page View (https://eu1.salesforce.com/a0CD000000CAv5u)
 *		2011-03-07	MHG		CR# 823 - Home page change (https://eu1.salesforce.com/a0C2000000C9k7o)
 *      1013-11-12	CCE		Changed User alias from knd (now inactive) to ori
 */
public with sharing class OpporunityOverviewController 
{
	// string[] PRMandCPProfiles = new string[] {'Gold Partner User', 'PRM - QlickBuy Sales Dependent Territory', 'PRM - Sales Dependent Territory', 'PRM - Sales Dependent Territory + QlikBuy', 'PRM - Sales QT Territory', 'PRM - Sales QT Territory + QlikBuy', 'Custom: Alliances Manager', 'Customer Portal Base Access', 'Customer Portal Base Access + QlikBuy', 'Customer Portal Case Logging Access', 'Customer Portal Case Logging Access + QlikBuy', 'Customer Portal Case Viewing Access', 'Customer Portal Case Viewing Access + QlikBuy', 'Limited Customer Portal Manager Custom', 'Limited Customer Portal Manager Standard', 'PRM - Independent Territory', 'PRM - Independent Territory + QlikBuy', 'PRM - Technical', 'PRM - Technical + QlickBuy'};
	string[] MarketingViewProfiles = new string[] {'Custom: BDR with Positions and inside view', 'Custom: BDR with Positions and inside view and leads', 'Custom: Corporate Marketing', 'Custom: Global Marketing Manager', 'Custom: Global Marketing Std User', 'Custom: Marketing Champ (Non Worldwide Visibility)', 'Custom: Marketing Champ (Non Worldwide Visibility) with Read Only Change Control', 'Custom: Marketing Champ EventForce View All', 'Custom: Marketing Champion', 'Custom: Marketing Director', 'Custom: Marketing Manager', 'Custom: Marketing Std User', 'Custom: Marketing Std User Inside View', 'Custom: QlikBuy BDR with Positions and inside view and leads', 'Custom: QlikBuy Corporate Marketing', 'Custom: QlikBuy Global Marketing Std User', 'Custom: QlikBuy Marketing Champ (Non Worldwide Visibility)', 'Custom: QlikBuy Marketing Champ (Non Worldwide Visibility) Read Only Change Cont', 'Custom: Support Std User with Change Control - Marketing', 'Custom: Support Std User with Change Control - marketing/leads/campaign', 'Marketing User', 'Sys Admin Profile for Marketing'};
	string[] SalesViewProfiles = new string[] {'Custom: Alliances Manager with CR', 'Custom: Alliances Manager Xactly', 'Custom: Alliances NetExam Std User', 'Custom: Alliances NetExam Std User with Merge', 'Custom: Alliances Std User', 'Custom: QlikBuy Alliances Manager', 'Custom: QlikBuy Alliances Manager with CR', 'Custom: QlikBuy Alliances Manager Xactly', 'Custom: QlikBuy Alliances NetExam Std User', 'Custom: QlikBuy Alliances NetExam Std User with Merge', 'Custom: QlikBuy Alliances Std User', 'Custom: QlikBuy Alliances Std User Inside View', 'Custom: QlikBuy EE, IM & LatAM Reseller Mgn’s', 'Custom: QlikBuy EE, IM & LatAM Reseller Mgn’s Xactly', 'Custom: Alliances Std User Inside View', 'Custom: Alliances Std User Xactly', 'Custom: Inside Sales Std User Inside View Americas (Clone)', 'Custom: Inside Sales Std User Inside View Asia (Clone)', 'Custom: OEM and Direct Sales Std User', 'Custom: OEM Sales Std User', 'Custom: OEM Sales Std User Inside View', 'Custom: QlikBuy Inside Sales Std User Inside View Americas', 'Custom: QlikBuy Inside Sales Std User Inside View Asia', 'Custom: QlikBuy OEM and Direct Sales Std User', 'Custom: QlikBuy OEM Sales Std User', 'Custom: QlikBuy OEM Sales Std User Inside View', 'Custom: QlikBuy Qlikview Master Partner Sales Manager', 'Custom: QlikBuy Qlikview Master Partner Sales Manager Merge Accounts & Leads', 'Custom: QlikBuy Sales Admin', 'Custom: QlikBuy Sales Admin Insideview', 'Custom: QlikBuy Sales Manager', 'Custom: QlikBuy Sales Manager - Inside View', 'Custom: QlikBuy Sales Manager (q4 ad sign off)', 'Custom: QlikBuy Sales Manager (with account access)', 'Custom: QlikBuy Sales Std User', 'Custom: QlikBuy Sales Std User + estimator test Xactly', 'Custom: QlikBuy Sales Std User Inside View', 'Custom: QlikBuy Sales Std User with Change Control', 'Custom: QlikBuy Sales Std User with D+BP Change', 'Custom: QlikBuy Sales Std User with OEM', 'Custom: QlikBuy Sales Std User Xactly', 'Custom: QlikBuy Sales+full Case access and Net Exam', 'Custom: Qlikview Master Partner Sales Manager', 'Custom: Qlikview Master Partner Sales Manager Merge Accounts & Leads', 'Custom: Sales Admin Insideview', 'Custom: Sales Admin Insideview (with Allow Portal access)', 'Custom: Sales Assistant ET', 'Custom: Sales Manager', 'Custom: Sales Manager - Inside View', 'Custom: Sales Manager (q4 ad sign off)', 'Custom: Sales Manager (with account access)', 'Custom: Sales Manager Xactly', 'Custom: Sales Std User', 'Custom: Sales Std User (OEM)', 'Custom: Sales Std User + estimator test Xactly', 'Custom: Sales Std User Inside View', 'Custom: Sales Std User Inside View AND ESTIMATOR TEST', 'Custom: Sales Std User with Change Control', 'Custom: Sales Std User with D+BP Change', 'Custom: Sales Std User with OEM', 'Custom: Sales Std User Xactly', 'Custom: Sales+full Case access and Net Exam', 'Sales / Marketing Admin extended role', 'Standard User'};
	string[] PRMandCPProfiles = new string[] {'Gold Partner User', 'Customer Portal Base Access', 'Customer Portal Base Access + QlikBuy', 'Customer Portal Case Logging Access', 'Customer Portal Case Logging Access + QlikBuy', 'Customer Portal Case Viewing Access', 'Customer Portal Case Viewing Access + QlikBuy', 'Limited Customer Portal Manager Custom', 'Limited Customer Portal Manager Standard', 'PRM - Technical', 'PRM - Technical + QlickBuy'};
	string[] PRMSalesProfiles = new string[] {'PRM - QlickBuy Sales Dependent Territory', 'PRM - Sales Dependent Territory', 'PRM - Sales Dependent Territory + QlikBuy', 'PRM - Sales QT Territory', 'PRM - Sales QT Territory + QlikBuy', 'PRM - Independent Territory', 'PRM - Independent Territory + QlikBuy', 'PRM - Technical + QlickBuy'};

	public integer myLimit = 400;

	public class SalesViewObject
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public integer Cnt { get; set; }		
	}
	
	public class ManagerViewObject
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public integer Cnt10 { get; set; }
		public integer CntConfirmed21 { get; set; }		
	}	
	
	public class MarketingViewObject
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public integer Cnt10 { get; set; }
		public integer Cnt7 { get; set; }
		public integer Cnt { get; set; }
	}
	
	public class SummaryViewObject
	{
		public string Name { get; set; }
		public string OppName { get; set; }
		public string OppId { get; set; }
		public date CreatedDate { get; set; }
		public integer Age { get; set; }
		public string CreatedBy { get; set; }
	}

	public List<string> getUserIDsFromManagerHierarchy()
	{
		boolean FirstPass = true;
		Set<string> UserIDs = new Set<string>();
		Set<string> LoopIDs = new Set<string>();		
		
		
		for (integer i=0;i<5;i++)
		{
			List<User> newIDs = new List<User>();
			
			System.debug('myLimit before: ' + myLimit);
			
			if (FirstPass)
			{
				FirstPass = false;
				newIDs = [select Id from User where Profile.Name not in :PRMandCPProfiles and IsActive = true and ManagerId = :UserInfo.getUserId() LIMIT :myLimit];
				UserIDs.add(UserInfo.getUserId());				
			}
			else
			{				
				System.debug('Nbr of LoopIDs: ' + LoopIDs.size());
				newIDs = [select Id from User where Profile.Name not in :PRMandCPProfiles and IsActive = true and ManagerId in :LoopIDs LIMIT :myLimit];
				LoopIDs.clear();
			} 
			
			myLimit -= newIDs.size();
			System.debug('myLimit after: ' + myLimit);				
			
			if (newIDs.size() == 0)
			{
				break;
			}
			
			for (User user : newIDs)
			{
				if (!UserIDs.contains(user.Id))
				{
					UserIDs.add(user.Id);
					LoopIDs.add(user.Id);		
				}					
			}	
			
			if (LoopIDs.size() == 0 || myLimit <= 0)
			{
				break;
			}
				
		}

		myLimit += 99;
		
		List<string> ReturnValue = new List<string>();
		ReturnValue.addAll(UserIDs);		
		
		return ReturnValue;
	}

	public List<SalesViewObject> getSalesView()
	{
		system.debug('getSalesView start');
		List<string> Users = getUserIDsFromManagerHierarchy();
		List<SalesViewObject> SalesViewObjects = new List<SalesViewObject>();
		
		Datetime SearchDate = Datetime.now();
		SearchDate = SearchDate.addDays(-10);		
		
		System.debug('getSalesView: myLimit - ' + myLimit);
		System.debug('getSalesView: users - ' + users);

		System.debug('getSalesView: SearchDate - ' + SearchDate);
		
		//Set<string> SalesViewProfileSet = new Set<String>();
		//SalesViewProfileSet.AddAll(SalesViewProfiles);

		//System.debug('getSalesView: SalesViewProfiles contains our profile - ' + SalesViewProfileSet.Contais(Owner.Profile.Name));

		
		for (AggregateResult ar : [select Owner.Name, Owner.Id, count(Id) from Opportunity where OwnerId in :Users and StageName = 'Goal Identified' and CreatedDate <= :SearchDate and Owner.Profile.Name in :SalesViewProfiles GROUP BY Owner.Name, Owner.Id LIMIT :myLimit])  //  CreatedBy.Profile.Name in :MarketingViewProfiles and
		{
				SalesViewObject SVobj = new SalesViewObject();
				
				SVobj.Name = '' + ar.get('Name');
				SVobj.Id = '' + ar.get('Id');
				SVobj.Cnt = integer.valueOf(ar.get('expr0'));
				SalesViewObjects.add(SVobj);
		}
		system.debug('SalesViewObjects.Size(): ' + SalesViewObjects.Size());
		
        // Sort the list using Bubble sort
        boolean Swap;        
        do {
        	Swap = false;
        	
        	for (integer i=0;i<SalesViewObjects.size() - 1;i++) {
        		SalesViewObject SVO1 = SalesViewObjects.get(i);
        		SalesViewObject SVO2 = SalesViewObjects.get(i+1);
        		
        		if (SVO1.Cnt < SVO2.Cnt) {
        			SalesViewObjects.set(i, SVO2);
        			SalesViewObjects.set(i+1, SVO1);
        			System.debug('getSalesView sorting: SVO1.Cnt (' + SVO1.Cnt +') > SVO2.Cnt (' + SVO2.Cnt + ') - Swapping!');
        			Swap = true;
        		}
        		
        	} 
        	
        } while(Swap);
        
        System.debug('getSalesView sorting: Finnished');
        		
		return SalesViewObjects; 	
	}

	public List<MarketingViewObject> getMarketingView()
	{
		List<string> Users = getUserIDsFromManagerHierarchy();		
		Map<string, MarketingViewObject> MarketingViewObjects = new Map<string, MarketingViewObject>();

		system.debug('Users: ' + Users);

		for (AggregateResult ar : [select CreatedBy.Name, CreatedBy.Id, count(Id) from Opportunity where CreatedById in :Users and StageName = 'Goal Identified' and CreatedBy.Profile.Name in :MarketingViewProfiles and Owner.Profile.Name in :SalesViewProfiles GROUP BY CreatedBy.Name, CreatedBy.Id])
		{
				MarketingViewObject MVobj = new MarketingViewObject();
				
				MVobj.Name = '' + ar.get('Name');
				MVobj.Id = '' + ar.get('Id');
				MVobj.Cnt = integer.valueOf(ar.get('expr0'));
				
				MarketingViewObjects.put(MVobj.Name, MVobj);
		}

		Datetime SearchDate = Datetime.now();
		SearchDate = SearchDate.addDays(-10);		
		for (AggregateResult ar : [select CreatedBy.Name, CreatedBy.Id, count(Id) from Opportunity where CreatedById in :Users and StageName = 'Goal Identified' and CreatedDate <= :SearchDate and CreatedBy.Profile.Name in :MarketingViewProfiles and Owner.Profile.Name in :SalesViewProfiles GROUP BY CreatedBy.Name, CreatedBy.Id])
		{
				MarketingViewObject MVobj = MarketingViewObjects.get('' + ar.get('Name'));
				MVobj.Cnt10 = integer.valueOf(ar.get('expr0'));
				
		}

		SearchDate = SearchDate.addDays(3); 
		for (AggregateResult ar : [select CreatedBy.Name, CreatedBy.Id, count(Id) from Opportunity where CreatedById in :Users and StageName = 'Goal Identified' and CreatedDate <= :SearchDate and CreatedBy.Profile.Name in :MarketingViewProfiles and Owner.Profile.Name in :SalesViewProfiles GROUP BY CreatedBy.Name, CreatedBy.Id])
		{
				MarketingViewObject MVobj = MarketingViewObjects.get('' + ar.get('Name'));
				MVobj.Cnt7 = integer.valueOf(ar.get('expr0'));
		}
		
		List<MarketingViewObject> Result = MarketingViewObjects.values();	
			
        // Sort the list using Bubble sort
        system.debug('MarketingViewObjects.Size(): ' + MarketingViewObjects.Size());

        boolean Swap;        
        do {
        	Swap = false;
        	
        	for (integer i=0;i<Result.size() - 1;i++) {
        		MarketingViewObject MVO1 = Result.get(i);
        		MarketingViewObject MVO2 = Result.get(i+1);
        		
        		if (MVO1.Cnt10 < MVO2.Cnt10) {
        			Result.set(i, MVO2);
        			Result.set(i+1, MVO1);
        			System.debug('getMarketingView sorting: SVO1.Cnt10 (' + MVO1.Cnt10 +') > SVO2.Cnt10 (' + MVO2.Cnt10 + ') - Swapping!');
        			Swap = true;
        		}
        		
        	} 
        	
        } while(Swap);
        
        System.debug('getMarketingView sorting: Finnished');			
			
		return Result;
	}
	
	public List<ManagerViewObject> getManagerView()
	{
		List<string> Users = getUserIDsFromManagerHierarchy();
		Map<string, ManagerViewObject> ManagerViewObjects = new Map<string, ManagerViewObject>(); 		
		
		List<string> CompiledManagerHierarchy = new List<string>();
		CompiledManagerHierarchy.addAll(PRMSalesProfiles);
		CompiledManagerHierarchy.addAll(SalesViewProfiles);
		
		Datetime SearchDate = Datetime.now();
		SearchDate = SearchDate.addDays(-10);

		system.debug('Users: ' + Users);
		
		for (AggregateResult ar : [select Owner.Name, Owner.Id, count(Id) from Opportunity where OwnerId in :Users and StageName = 'Goal Identified' and CreatedDate <= :SearchDate and Owner.Profile.Name in :CompiledManagerHierarchy GROUP BY Owner.Name, Owner.Id LIMIT :myLimit])
		{
			ManagerViewObject Mobj = new ManagerViewObject();
				
			Mobj.Name = '' + ar.get('Name');
			Mobj.Id = '' + ar.get('Id');
			Mobj.Cnt10 = integer.valueOf(ar.get('expr0'));
			Mobj.CntConfirmed21 = 0;
			ManagerViewObjects.put(Mobj.Name, Mobj);
		}

		SearchDate = SearchDate.addDays(-21);
		for (AggregateResult ar : [select Owner.Name, Owner.Id, count(Id) from Opportunity where OwnerId in :Users and StageName = 'Goal Confirmed' and CreatedDate <= :SearchDate and Owner.Profile.Name in :CompiledManagerHierarchy GROUP BY Owner.Name, Owner.Id LIMIT :myLimit])
		{
			if (ManagerViewObjects.containsKey('' + ar.get('Name')))
			{		
				ManagerViewObject Mobj = ManagerViewObjects.get('' + ar.get('Name'));
				Mobj.CntConfirmed21 = integer.valueOf(ar.get('expr0'));
			}
			else
			{
				ManagerViewObject Mobj = new ManagerViewObject();
				
				Mobj.Name = '' + ar.get('Name');
				Mobj.Id = '' + ar.get('Id');
				Mobj.Cnt10 = 0;
				Mobj.CntConfirmed21 = integer.valueOf(ar.get('expr0'));
				ManagerViewObjects.put(Mobj.Name, Mobj);			
			}
		}

		List<ManagerViewObject> Result = ManagerViewObjects.values();						
					
        // Sort the list using Bubble sort
        boolean Swap;        
        do {
        	Swap = false;
        	
        	for (integer i=0;i<Result.size() - 1;i++) {
        		ManagerViewObject SVO1 = Result.get(i);
        		ManagerViewObject SVO2 = Result.get(i+1);
        		
        		if (SVO1.Cnt10 < SVO2.Cnt10) {
        			Result.set(i, SVO2);
        			Result.set(i+1, SVO1);
        			System.debug('getManagerView sorting: SVO1.Cnt (' + SVO1.Cnt10 +') > SVO2.Cnt (' + SVO2.Cnt10 + ') - Swapping!');
        			Swap = true;
        		}
        		
        	} 
        	
        } while(Swap);
        
        System.debug('getManagerView sorting: Finnished');
		
		
		return Result; 			
	}	
	
	public List<SummaryViewObject> getSummaryView()
	{
		List<string> Users = getUserIDsFromManagerHierarchy();
		List<SummaryViewObject> SummaryViewObjects = new List<SummaryViewObject>();
		
		Id UserId = System.currentPagereference().getParameters().get('Uid');
		Integer LT = integer.valueOf(System.currentPagereference().getParameters().get('LT'));
		Integer IsManagerView = integer.valueOf(System.currentPagereference().getParameters().get('IMV'));
		Integer IsMarketingUserView = integer.valueOf(System.currentPagereference().getParameters().get('IMUV'));
		Integer IsGoalConfirmed = 0;
		if (System.currentPageReference().getParameters().containsKey('C'))
		{
			IsGoalConfirmed = integer.valueOf(System.currentPagereference().getParameters().get('C'));
		}
		
		Datetime SearchDate = Datetime.now();
		
		if (LT == 0)
		{
			SearchDate = Datetime.now(); //datetime.newInstance(1979, 03, 01, 0, 0, 0);
		}
		else
		{
			SearchDate = SearchDate.addDays(-LT);
		}
		
		if (IsManagerView == 0)
		{
			if (IsMarketingUserView == 0)
			{
				for (Opportunity Opp : [select Id, Owner.Name, CreatedBy.Name, Name, CreatedDate from Opportunity where OwnerId = :UserId and StageName = 'Goal Identified' and CreatedDate <= :SearchDate and Owner.Profile.Name in :SalesViewProfiles])  //  and CreatedBy.Profile.Name in :MarketingViewProfiles
				{
					SummaryViewObject SVobj = new SummaryViewObject();
					
					SVobj.Name = Opp.Owner.Name;
					SVobj.OppName = Opp.Name;
					SVobj.OppId = Opp.Id;
					SVobj.CreatedDate = Opp.CreatedDate.date();
					SVobj.Age = SVobj.CreatedDate.daysBetween(date.today());
					SVobj.CreatedBy = Opp.CreatedBy.Name;
					
					SummaryViewObjects.add(SVobj);
				}
			}
			else
			{			
				for (Opportunity Opp : [select Id, Owner.Name, CreatedBy.Name, Name, CreatedDate from Opportunity where CreatedById = :UserId and StageName = 'Goal Identified' and CreatedBy.Profile.Name in :MarketingViewProfiles and CreatedDate <= :SearchDate and Owner.Profile.Name in :SalesViewProfiles]) 
				{
					SummaryViewObject SVobj = new SummaryViewObject();
					
					SVobj.Name = Opp.Owner.Name;
					SVobj.OppName = Opp.Name;
					SVobj.OppId = Opp.Id;
					SVobj.CreatedDate = Opp.CreatedDate.date();
					SVobj.Age = SVobj.CreatedDate.daysBetween(date.today());
					SVobj.CreatedBy = Opp.CreatedBy.Name;
					
					SummaryViewObjects.add(SVobj);
				}			
			}
		}
		else
		{
			List<string> CompiledManagerHierarchy = new List<string>();
			CompiledManagerHierarchy.addAll(PRMSalesProfiles);
			CompiledManagerHierarchy.addAll(SalesViewProfiles);
			
			if (IsGoalConfirmed == 0)
			{
				system.debug('Inside IsManagerView!=0 && IsGoalConfirmed = 0');
				for (Opportunity Opp : [select Id, Owner.Name, CreatedBy.Name, Name, CreatedDate from Opportunity where OwnerId = :UserId and StageName = 'Goal Identified' and CreatedDate <= :SearchDate and Owner.Profile.Name in :CompiledManagerHierarchy ])
				{
					SummaryViewObject SVobj = new SummaryViewObject();
					
					SVobj.Name = Opp.Owner.Name;
					SVobj.OppName = Opp.Name;
					SVobj.OppId = Opp.Id;
					SVobj.CreatedDate = Opp.CreatedDate.date();
					SVobj.Age = SVobj.CreatedDate.daysBetween(date.today());
					SVobj.CreatedBy = Opp.CreatedBy.Name;
					
					SummaryViewObjects.add(SVobj);
				}
			}
			else
			{	
				for (Opportunity Opp : [select Id, Owner.Name, CreatedBy.Name, Name, CreatedDate from Opportunity where OwnerId = :UserId and StageName = 'Goal Confirmed' and CreatedDate <= :SearchDate and Owner.Profile.Name in :CompiledManagerHierarchy ])
				{
					SummaryViewObject SVobj = new SummaryViewObject();
					
					SVobj.Name = Opp.Owner.Name;
					SVobj.OppName = Opp.Name;
					SVobj.OppId = Opp.Id;
					SVobj.CreatedDate = Opp.CreatedDate.date();
					SVobj.Age = SVobj.CreatedDate.daysBetween(date.today());
					SVobj.CreatedBy = Opp.CreatedBy.Name;
					
					SummaryViewObjects.add(SVobj);		
				}
			}
		}
		
		return SummaryViewObjects;
	}


}