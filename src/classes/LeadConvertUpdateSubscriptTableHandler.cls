/***************************************************
Object: Lead
Description: For eCommerce Project. Story QE-233. There is two ID fields on the Subscription table.
             One field holds the lead ID, if the lead is converted the ID needs to be deleted 
             and the contact ID be placed instead. 
Change Log:
 20151027    CCE Initial development
 2017-10-25 AYS BMW-402 : Migrated from UpdateSubscriptionTableOnLeadConvert.trigger
******************************************************/

public class LeadConvertUpdateSubscriptTableHandler {
	public LeadConvertUpdateSubscriptTableHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
		System.debug('UpdateSubscriptionTableOnLeadConvert: starting');
	    if (!Semaphores.TriggerHasRun('UpdateSubscriptionTableOnLeadConvert'))
	    {
	        System.debug('UpdateSubscriptionTableOnLeadConvert: passed semaphore.');
	        List<Subscription__c> SubscriptionsToBeUpdated = new List<Subscription__c>();
	        Map<Id, Id> LeadToContact = new Map<Id, Id>();

	        for (integer i = 0; i < triggerNew.size(); i++)
	        {
	            if (triggerNew[i].IsConverted == true && triggerOld[i].IsConverted == false && triggerNew[i].ConvertedContactId != null)
	            {
	                LeadToContact.put(triggerNew[i].Id, triggerNew[i].ConvertedContactId);
	            }               
	        }
	        System.debug('UpdateSubscriptionTableOnLeadConvert: LeadToContact = ' + LeadToContact);    

	        if (!LeadToContact.isEmpty()) { //only continue if we have something to work with
	            for (Subscription__c s : [select Id, Lead__c, Contact__c from Subscription__c where Lead__c in :LeadToContact.keySet()])
	            {
	                s.Contact__c = LeadToContact.get(s.Lead__c);
	                s.Lead__c = null;
	                SubscriptionsToBeUpdated.Add(s);
	            }
	            if (SubscriptionsToBeUpdated.size() > 0)
	            {
	                update SubscriptionsToBeUpdated;     
	            }    
	        }

	    }
	    System.debug('UpdateSubscriptionTableOnLeadConvert: finishing');
	}
}