/* 
Purpose: Partner Expertise Solution Profile Controller test
Created Date: 6 Jan 2016
Initial delvelpment 6 Jan 2016
Change log:
06.02.2017  RVA :   changing CreateAcounts methods
**/
@isTest(seeAllData=false)

private class EP_SolutionProfileController_Test {
							 static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
                             Static Account account { get; set; }
                             Static Partner_Expertise_Area__c ExpertiseArea {get; set;}
                             Static Solution_Profiles__c SolProfile {get; set;}
                             Static Solution_Profiles__c sol1 {get; set;}
                             Static Solution_Profiles__c sol2 {get; set;}
                             Static Solution_Profiles__c sol3 {get; set;}
                             Static Solution_Profiles__c sol4 {get; set;}
                             Static String StepNumber {get; set;}
                             Static String CurrentPageURL {get; set;}
                             Static User testUser {get; set;}
                             Static Partner_Expertise_Application_Lookup__c ExpertLookup {get; set;} 
                             
    private static testMethod void test_EP_SolutionProfileController() 
    {
        Setup(); 
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        System.currentPageReference().getParameters().put('stepNumber',String.valueOf(2));
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        System.assertEquals(controller.ExpertiseId, ExpertiseArea.Id);  
        System.assertEquals(controller.SolProfileID, sol1.Id);  
        System.assertEquals(controller.StepNumber,2);
        SolProfile=controller.getNewOrExistingProfile(controller.SolProfileID);
        System.assertEquals(controller.SolProfileID, SolProfile.Id);   
        SolProfile=controller.GetIncompleteSolutionProfile();
        System.assertEquals(sol1.Id, SolProfile.Id);    
        SolProfile=controller.getNewOrExistingProfileByStep(1);
        System.assertEquals(sol1.Id, SolProfile.Id);  
        SolProfile=controller.getNewOrExistingProfileByStep(2);
        System.assertEquals(sol2.Id, SolProfile.Id); 
        Test.stopTest();
    }
    Static testmethod void test_IsProfileLocked() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol4.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        Boolean isLocked=controller.IsProfileLocked;
        System.assertEquals(false,isLocked);
        Test.stopTest();
    }
    Static testmethod void test_ShowOpportunity() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        //System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        //System.currentPageReference().getParameters().put('solprofid',sol4.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        account.Partner_Category__c  = 'Technology Partner';
        upsert account;
        Boolean isShowOpportunity=controller.ShowOpportunity;
        System.assertEquals(false,isShowOpportunity);
        Test.stopTest();
    }

    Static testmethod void test_Expertise() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        Partner_Expertise_Area__c ExpertiseAreaObj = controller.Expertise;
        System.assertEquals(ExpertiseArea.Id, ExpertiseAreaObj.Id);
        Test.stopTest();
    }


    Static testmethod void test_ListOpportunities() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        System.assertEquals(controller.ListOpportunities.size(),0);
        Test.stopTest();
    }
    Static testmethod void test_Opportunities() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        System.assertEquals(controller.Opportunities.size(),1);
        Test.stopTest();
    }

    Static testmethod void test_SolutionOwners() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        List<selectOption> SolOwnerUsers=controller.SolutionOwners;
        System.assert(SolOwnerUsers.size()>=1);
        Test.stopTest();
    }

    Static testmethod void test_GetLookupObject() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        Partner_Expertise_Application_Lookup__c lookupobj=controller.GetLookupObject();
        System.assertEquals(lookupobj.Id, ExpertLookup.Id);
        System.assertEquals(ExpertLookup.Solution_Profile_1_Name__c,sol1.Id);
        delete lookupobj;
        lookupobj=controller.GetLookupObject();
        System.assertEquals(lookupobj.Id, null);
        Test.stopTest(); 
    }

    Static testmethod void test_getNewOrExistingProfile() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        Solution_Profiles__c sp=controller.getNewOrExistingProfile(sol1.Id);
        System.assertEquals(controller.SolProfileID, sp.Id);    
        Solution_Profiles__c sp1=controller.getNewOrExistingProfile(null);
        System.assertEquals(sp1.Id,null);    
        Test.stopTest();
    }

    Static testmethod void test_getNewOrExistingProfileByStep() 
    {
        Setup();
        Test.startTest();
        PageReference pg = Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        Solution_Profiles__c sp=controller.getNewOrExistingProfileByStep(1);
        System.assertEquals(sp.Id,sol1.Id);
        Solution_Profiles__c sp1=controller.getNewOrExistingProfileByStep(2);
        System.assertEquals(sp1.Id,sol2.Id);
        Solution_Profiles__c sp2=controller.getNewOrExistingProfileByStep(3);
        System.assertEquals(sp2.Id,sol3.Id);
        Solution_Profiles__c sp3=controller.getNewOrExistingProfileByStep(4);
        System.assertEquals(sp3.Id,sol4.Id);
        Test.stopTest();
    }

    Static testmethod void test_GetIncompleteSolutionProfile()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol2.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        sol1.Status__c='Approved';
        upsert sol1;
        Solution_Profiles__c sp2=controller.GetIncompleteSolutionProfile();
        System.assertEquals(sp2.Id,sol2.Id);
        sol2.Status__c='Approved';
        upsert sol2;
        Solution_Profiles__c sp3=controller.GetIncompleteSolutionProfile();
        System.assertEquals(sp3.Id,sol3.Id);
        sol3.Status__c='Approved';
        upsert sol3;
        Solution_Profiles__c sp4=controller.GetIncompleteSolutionProfile();
        System.assertEquals(sp4.Id,sol4.Id);
        sol4.Status__c='Approved';
        upsert sol4;
        Solution_Profiles__c sp5=controller.GetIncompleteSolutionProfile();
        System.assertEquals(sp5.Id,sol1.Id);
        Test.stopTest();
    }

    Static testmethod void test_linkSolutionProfile()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        System.currentPageReference().getParameters().put('stepnumber','1');
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        controller.linkSolutionProfile(sol1);
        System.assertEquals(controller.Lookup.Solution_Profile_1_Name__c,sol1.Id);
        controller.StepNumber=2;
        controller.linkSolutionProfile(sol2);
        System.assertEquals(controller.Lookup.Solution_Profile_2_Name__c,sol2.Id);
        controller.StepNumber=3;
        controller.linkSolutionProfile(sol3);
        System.assertEquals(controller.Lookup.Solution_Profile_3_Name__c,sol3.Id);
        Test.stopTest();
    }

    Static testmethod void test_updateSolProfileStatus()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        String solProfStatus = controller.updateSolProfileStatus();
        System.assertEquals(solProfStatus,'In Process');
        controller.SolProfile.Sector__c='HealthCare';
        controller.SolProfile.Industry__c='HealthCare';
        controller.SolProfile.Business_Issues_Goals__c='Test';
        controller.SolProfile.IT_Issues_Goals__c='Test';
        controller.SolProfile.Business_Value_Qualitative__c='Test';
        controller.SolProfile.IT_Value_Qualitative__c='Test';
        controller.SolProfile.No_of_Deployed_Users__c = 1;
        controller.SolProfile.No_of_Potential_Users__c =1;
        controller.SolProfile.Data_Source_Systems__c= 'Test';
        controller.SolProfile.Description_of_Data__c= 'Test';
        controller.SolProfile.Description_of_Users__c= 'Test';
        controller.SolProfile.Go_Live_Date__c =Date.today().adddays(10);
        controller.SolProfile.Time_to_Develop_Deploy__c = 1;
        controller.SolProfile.Data_Volume_Records__c = 1;
        controller.SolProfile.Solution_Description__c='Test';
        solProfStatus = controller.updateSolProfileStatus();
        System.assertEquals(solProfStatus,'Ready for Review');
        Test.stopTest();
    }
    Static testmethod void test_StatusEditable()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        SolProfile=controller.getNewOrExistingProfileByStep(1);
        Boolean IsStatusEditable=controller.StatusEditable(SolProfile.Status__c);
        System.assertEquals(true,IsStatusEditable);
        Test.stopTest();
    }
    Static testmethod void test_SaveProfile()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        controller.SolProfile.Name='Solution11';
        controller.SaveProfile();
        System.assertEquals(controller.SolProfile.Name,'Solution11');
        Test.stopTest();
    }
    Static testmethod void test_SaveSolProfile()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol4.Id);
        System.currentPageReference().getParameters().put('stepnumber','4');
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        PageReference pg1 = controller.saveSolProfile('Next');
        String pageref=String.valueOf(pg1);
        System.assert(pageref.contains('QlikExpertiseProgram'));
        Test.stopTest();
    }

    Static testmethod void test_SaveAndRedirect()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        System.currentPageReference().getParameters().put('stepnumber','1');
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        controller.RedirectStepNumber=2;
        PageReference pg1 = controller.SaveAndRedirect();
        String pageref=String.valueOf(pg1);
        System.assert(pageref.contains('stepNumber=2'));
        /*PageReference pg2 = controller.NextPage();
        String pageref2=String.valueOf(pg2);
        System.assert(pageref2.contains('stepNumber=3'));*/
        Test.stopTest();
    }
    Static testmethod void test_NextPage()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        System.currentPageReference().getParameters().put('stepnumber','1');
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        /*controller.RedirectStepNumber=2;
        PageReference pg1 = controller.SaveAndRedirect();
        String pageref=String.valueOf(pg1);
        System.assert(pageref.contains('stepNumber=2'));*/
        PageReference pg2 = controller.NextPage();
        String pageref2=String.valueOf(pg2);
        System.assert(pageref2.contains('stepNumber=2'));
        Test.stopTest();
    }
    Static testmethod void test_Cancel()
    {
        Setup();
        Test.startTest();
        PageReference pg=Page.EP_SolutionProfilePage;
        Test.setCurrentPage(pg);
        System.currentPageReference().getParameters().put('acc', account.Id);
        System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
        System.currentPageReference().getParameters().put('solprofid',sol1.Id);
        EP_SolutionProfileController controller = new EP_SolutionProfileController();
        PageReference pg2 = controller.Cancel();
        String pageref2=String.valueOf(pg2);
        System.assert(pageref2.contains('QlikExpertiseProgram'));
        controller.SolProfile.Name='TestName';
        PageReference pg1 = controller.Cancel();
        System.assertEquals(pg1,null);
        Test.stopTest();
    }

    public Static void Setup()
    {    
		/*
        Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c qtc = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert qtc;
        //Create Account
        account = TestDataFactory.createAccount('Test AccountName', qtc);
        insert account;
        */
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		account = QTTestUtils.createMockAccount('TestCompany', thisUser, true);
	        account.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update account;
        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', account.Id);
        insert testContact;

        // Retrieve Customer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'PRM - Sales Dependent Territory + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        // Create Users     
        testUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert testUser;
        
        //Create ExpertiseArea
        ExpertiseArea = EP_TestDataSetup.CreatExpertiseArea(account.Id, 'Communcation', testContact);      

        sol1 = EP_TestDataSetup.CreateSolutionProfile('Solution1', account.Id, ExpertiseArea.Id);
        sol2 = EP_TestDataSetup.CreateSolutionProfile('Solution2', account.Id, ExpertiseArea.Id);
        sol3 = EP_TestDataSetup.CreateSolutionProfile('Solution3', account.Id, ExpertiseArea.Id);
        sol4 = EP_TestDataSetup.CreateSolutionProfile('Solution4', account.Id, ExpertiseArea.Id);

        //Create ExpertiseArea lookup object

        ExpertLookup = new Partner_Expertise_Application_Lookup__c(
        Account_Name__c = account.Id,
        Expertise_Area__c = ExpertiseArea.Id);
        //Assign Solution_Profiles to lookup object        
        ExpertLookup.Solution_Profile_1_Name__c = sol1.Id;
        ExpertLookup.Solution_Profile_2_Name__c = sol2.Id;
        ExpertLookup.Solution_Profile_3_Name__c = sol3.Id;
        ExpertLookup.Solution_Profile_4_Name__c = sol4.Id;  
        insert ExpertLookup;

        QTCustomSettings__c setting = new QTCustomSettings__c(
        Name = 'Default', 
        eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO',
        OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
        Insert setting;
        QuoteTestHelper.createCustomSettings();
        Opportunity op = New Opportunity (
        Short_Description__c = 'TestOpp - delete me',
        Name = 'TestOpp - DeleteMe',
        CloseDate = Date.today().adddays(10),
        StageName = 'Closed Lost',
        Sell_Through_Partner__c=account.Id,
        Amount=100,
        AccountId = account.Id,
        ForecastCategoryName = 'Omitted'
        );                    
            
        insert op;
        system.debug('oppId -'+op.Id);
        
    }    
}