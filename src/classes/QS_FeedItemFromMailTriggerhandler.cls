/**
 * @author  Navneet
 * @date    26/01/2016
 *  
 * Trigger Handler class for QS_emailMessageforchatterTrigger trigger.
 * ext_bad	    2018-04-18  Add check for ParentId
 * AIN          2019-05-05 IT-1597 Updated for Support Portal Redesign
 *
 * 
 */

public class QS_FeedItemFromMailTriggerhandler {

    public static void feeditemChatterCase(List<EmailMessage> EmailMessageChatter) {

        for (EmailMessage message : EmailMessageChatter) {

            if (message.ParentId != null) {
                string CEmail = [select Contact.Email from Case where Id = :message.ParentId].Contact.Email;
                //  Id recordTypeId = [Select Id ,name From RecordType where name = 'QlikTech Master Support Record Type'].Id;
                string recordtypename = [select recordtype.name from Case where Id = :message.ParentId].recordtype.name;
                system.debug('CEmail ' + CEmail);
            }
            System.debug('%%%%BccAddress: ' + message.BccAddress);
            System.debug('%%%%CcAddress: ' + message.CcAddress);
            System.debug('%%%%ToAddress: ' + message.ToAddress);
            string Address = message.BccAddress + message.CcAddress + message.ToAddress;
            System.debug('%%%%Address: ' + Address);

            /*   if((CEmail != null && CEmail != '') && (recordtypename=='QlikTech Master Support Record Type')) {
                   if(Address.contains(CEmail))  {
                       string cbody = 'Email received on case: '+message.TextBody;
                       if(cbody.Length() > 5000)
                           cbody = cbody.substring(0,4995) + '...';
                       FeedItem post = new FeedItem();
                       post.Visibility = 'AllUsers';
                       post.ParentId  = message.ParentId;
                       post.Body = cbody;
                       insert post;
                   }
               }   */
        }
    }
}