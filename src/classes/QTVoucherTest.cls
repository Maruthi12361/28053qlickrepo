/*
*   20140624    RDZ Creating QTVoucher Test Class, To improve test coverage 
*   20150618    TJG Add yet another dummy test method for urgent deployment
*   20180327    AIN Added test coverage for new WS calls
*/
@isTest(SeeAllData=true)
class QTVoucherTest
{  
    static testMethod void QTVoucherCodeCoverageTestMethod()
    {
        WebServiceMockDispatcher dispatcher = new WebServiceMockDispatcher();
        test.setMock(WebServiceMock.Class, dispatcher);

    	//public static void CallCreateVoucherFromNLRP(List<string> NLRPToUpdate)
	   	QTVoucher.CallCreateVoucherFromNLRP(new List<string>());

	   	//public static void CallCreateVoucherFromOpportunity(List<string> OppIDsWithNLRP)
    	QTVoucher.CallCreateVoucherFromOpportunity(new List<string>());

    	//public static void CallCreateVouchersFromOpportunities(List<string> OppIDsWithEducationOLIs)
    	QTVoucher.CallCreateVouchersFromOpportunities(new List<string>());

    	//public static void CallActivateVouchersFromOpportunity(List<string> OppIDsWithInactiveVouchers)
    	QTVoucher.CallActivateVouchersFromOpportunity(new List<string>());

    	//public static void CallActivateVouchersFromOpportunities(List<string> OppIDsWithEducationOLIs)
    	QTVoucher.CallActivateVouchersFromOpportunities(new List<string>());

    	//public static void CallCreateAndActivateVouchersFromOpportunities(List<string> OppIDsWithEducationOLIs)
    	QTVoucher.CallCreateAndActivateVouchersFromOpportunities(new List<string>());

        QTVoucher.CallReactivateVouchersOnOpportunities(new List<string>());
        QTVoucher.CallDeactivateVouchersOnOpportunities(new List<string>());




        
    }
    static testMethod void qtvoucherQliktechComCoverageTest2()
    {
        WebServiceMockDispatcher dispatcher = new WebServiceMockDispatcher();
        test.setMock(WebServiceMock.Class, dispatcher);

        qtvoucherQliktechCom.CheckVoucherResponse_element checkVoucherResponseElement = null;
        qtvoucherQliktechCom.CreateVoucherResponse_element createVoucherResponseElement = null;
        qtvoucherQliktechCom.ConsumeVoucherResponse_element ConsumeVoucherResponseElement = null;
        qtvoucherQliktechCom.GeteLearningSubscriptionResponse_element GeteLearningSubscriptionResponseElement = null;
        qtvoucherQliktechCom.CreateVoucherFromOppLineItemsResponse_element CreateVoucherFromOppLineItemsResponseElement = null;
        qtvoucherQliktechCom.CheckCancellationResponse_element CheckCancellationResponseElement = null;
        qtvoucherQliktechCom.CreateVoucherFromSFDCResponse_element CreateVoucherFromSFDCResponseElement = null;
        qtvoucherQliktechCom.ExpireVouchersResponse_element ExpireVouchersResponseElement = null;
        boolean ExpireVouchersResponseElementBool = false;
        qtvoucherQliktechCom.ActiveElearningSubscriptionResponse_element ActiveElearningSubscriptionResponseElement = null;
        qtvoucherQliktechCom.CancelVoucherResponse_element CancelVoucherResponseElement = null;
        qtvoucherQliktechCom.TryVoucherCreationAndActivationResponse_element TryVoucherCreationAndActivationResponseElement = null;
        qtvoucherQliktechCom.ActivateVouchersFromOpportunitiesResponse_element ActivateVouchersFromOpportunitiesResponseElement = null;
        qtvoucherQliktechCom.CreateVouchersFromOpportunitiesResponse_element CreateVouchersFromOpportunitiesResponseElement = null;
        qtvoucherQliktechCom.ActivateVouchersFromOpportunityResponse_element ActivateVouchersFromOpportunityResponseElement = null;
        qtvoucherQliktechCom.CreateVoucherFromNLRPResponse_element  CreateVoucherFromNLRPResponseElement  = null;
        qtvoucherQliktechCom.CreateVoucherFromOpportunityResponse_element CreateVoucherFromOpportunityResponseElement = null;
        qtvoucherQliktechCom.CreateVoucherFromImportFileResponse_element CreateVoucherFromImportFileResponseElement = null;
        qtvoucherQliktechCom.DeactivateVouchersOnOpportunities_element DeactivateVouchersOnOpportunitiesElement = null;
        qtvoucherQliktechCom.SetVoucherCreationToPendingResponse_element SetVoucherCreationToPendingResponseElement = null;
        qtvoucherQliktechCom.ReactivateVouchersOnOpportunitiesResponse_element ReactivateVouchersOnOpportunitiesResponseElement = null;
        qtvoucherQliktechCom.SetVoucherCreationToPending_element SetVoucherCreationToPendingElement = null;
        qtvoucherQliktechCom.CreateVoucherFromImportFile_element CreateVoucherFromImportFileElement = null;
        qtvoucherQliktechCom.DeactivateVouchersOnOpportunitiesResponse_element DeactivateVouchersOnOpportunitiesResponseElement = null;
        qtvoucherQliktechCom.ReactivateVouchersOnOpportunities_element ReactivateVouchersOnOpportunitiesElement = null;

        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
        test.startTest();
        checkVoucherResponseElement = service.CheckVoucher('voucherCode', 'username',10,'relatesTo','Region', 'ReturnText');
        createVoucherResponseElement  = service.CreateVoucher('AccountId','UserName','ContactId', 'OpportunityId', 'RelatesTo', 'vType','Status',10,'Region','ReturnText');
        ConsumeVoucherResponseElement = service.ConsumeVoucher('VoucherCode', 'UserName', 10, 'RelatesTo', Datetime.Now(), 'ExternalId', 'Region', 'ReturnText');
        GeteLearningSubscriptionResponseElement = service.GeteLearningSubscription('Username', 'Password', 'ReturnText');
        qtvoucherQliktechCom.ArrayOfString stringArray = new qtvoucherQliktechCom.ArrayOfString();
        stringArray.string_x = new string[1];
        stringArray.string_x[0] = 'test';
        CreateVoucherFromOppLineItemsResponseElement = service.CreateVoucherFromOppLineItems(stringArray, 'returnText');
        CheckCancellationResponseElement = service.CheckCancellation('Username', 'ContactId', 'ExternalId', 'EventDate', 'ReturnText');
        CreateVoucherFromSFDCResponseElement = service.CreateVoucherFromSFDC('AccountId', 'UserName', 'ContactId', 'OpportunityId', 'RelatesTo', 'vType', 'Status', 10, 'Region', 'NonLicRelProdId', 'ReturnText');
        ExpireVouchersResponseElementBool = service.ExpireVouchers();
        ActiveElearningSubscriptionResponseElement = service.ActiveElearningSubscription('Username', 'Password', 'VoucherNumber', 'ReturnText');
        CancelVoucherResponseElement = service.CancelVoucher('VoucherCode', 'Username', 'ContactId', 'ExternalId', 'EventDate', 'ReturnText');
        TryVoucherCreationAndActivationResponseElement = service.TryVoucherCreationAndActivation(stringArray, 'ReturnText');
        ActivateVouchersFromOpportunitiesResponseElement = service.ActivateVouchersFromOpportunities(stringArray, 'ReturnText');
        CreateVouchersFromOpportunitiesResponseElement = service.CreateVouchersFromOpportunities(stringArray, 'ReturnText');
        ActivateVouchersFromOpportunityResponseElement = service.ActivateVouchersFromOpportunity('OpportunityId', 'ReturnText');
        CreateVoucherFromNLRPResponseElement = service.CreateVoucherFromNLRP('NonLicRelProdId', 'ReturnText');
        CreateVoucherFromOpportunityResponseElement = service.CreateVoucherFromOpportunity('OpportunityId', 'ReturnText');

        CreateVoucherFromImportFileResponseElement = service.CreateVoucherFromImportFile('monetaryKey','companyName','contactEmail','contactName','uniqueIdentifier','country','purchasePrice','currencyCode','customText1','customText2','customText3',
            'numberOfCredits','discount','expirationDate','extCompanyId','startDate','customText4','ReturnText');

        SetVoucherCreationToPendingResponseElement = service.SetVoucherCreationToPending(stringArray, 'ReturnText');
        ReactivateVouchersOnOpportunitiesResponseElement = service.ReactivateVouchersOnOpportunities(stringArray, 'ReturnText');
        DeactivateVouchersOnOpportunitiesResponseElement = service.DeactivateVouchersOnOpportunities(stringArray, 'ReturnText');

        test.stopTest();
    }
    // 2015-06-18 tjg       dummy test method so that we can get urgnet deployment moving
    static testMethod void qtvoucherQliktechComCoverageTest()
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        string ReturnText = '';
        List<string> NLRPToUpdate = new List<string>();
        NLRPToUpdate.add('Dummy1');
        NLRPToUpdate.add('Dummy2');
        service.timeout_x = 60000;

        qtvoucherQliktechCom.ArrayOfString OppIds = new qtvoucherQliktechCom.ArrayOfString();
        OppIds.string_x = NLRPToUpdate;

        try
        {
            service.CreateVouchersFromOpportunities(OppIds, ReturnText);
        }
        catch(Exception ex)
        {
            System.Debug(ex.getMessage());
        }


        if (ReturnText != null && ReturnText != '')
        {
            system.Debug('Error Creating Vouchers from Opp Ids: ' + ReturnText);
        }

        try 
        {
            service.ActivateVouchersFromOpportunities(OppIds, ReturnText);
        }
        catch(Exception ex)
        {
            System.Debug(ex.getMessage());
        }

        try
        {
            service.TryVoucherCreationAndActivation(OppIds, ReturnText);
        }
        catch(Exception ex)
        {
            System.Debug(ex.getMessage());
        }
        
        for (integer i = 0; i < NLRPToUpdate.size(); i++ )
        {
            try
            {
                service.CreateVoucherFromNLRP(NLRPToUpdate[i], ReturnText);
                system.Debug('Error Creating Voucher from NLRP Id [' +NLRPToUpdate[i]+']: ' + ReturnText);
            }
            catch(Exception ex)
            {
                System.Debug(ex.getMessage());
            }
            try
            {
                service.CreateVoucherFromOpportunity(NLRPToUpdate[i], ReturnText);
            }
            catch(Exception ex)
            {
                System.Debug(ex.getMessage());
            }

            try
            {

                service.ActivateVouchersFromOpportunity(NLRPToUpdate[i], ReturnText);
            }
            catch(Exception ex)
            {
                System.Debug(ex.getMessage());
            }

        }
    }
}