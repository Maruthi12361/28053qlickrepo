/**     * File Name: PartnerAccountTerminationHandlerTest
        * Description : This test class for PartnerAccountTerminationHandler
        * @author : Linus Löfberg
        * Modification Log ===============================================================
        Ver     Date         Author         Modification
        1.0     2018-01-31   ext_lfb        Class initiated.
        29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta
*/
@isTest
private class PartnerAccountTerminationHandlerTest {

    @testSetup
    public static void Setup() {
        QuoteTestHelper.createCustomSettings();
    }

    @isTest
    public static void testPartnerAccountTerminationAsAdmin() {

        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, isActive = true,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com');
        insert u;

        System.runAs(u) {
            Test.startTest();

            RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];

            Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
            QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);

            Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
            accPartner.Navision_Status__c = 'Partner';
            accPartner.Payment_Terms__c = '75 days';
            insert accPartner;
            insert QuoteTestHelper.createPCS(accPartner);
            Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
            insert conPartner;

            accPartner = [SELECT Id, Name,Terminated__c,Partner_Status__c,Status_from_Date__c,Partner_Status_Change_Reason__c,Navision_Status__c,Publish_To_Directory__c FROM Account WHERE Id = :accPartner.Id];

            System.assertEquals(false, accPartner.Terminated__c);

            accPartner.Terminated__c = true;

            Semaphores.CustomAccountTriggerBeforeUpdate = false;
            update accPartner;

            accPartner = [SELECT Id, Name,Terminated__c,Partner_Status__c,Status_from_Date__c,Partner_Status_Change_Reason__c,Navision_Status__c,Publish_To_Directory__c FROM Account WHERE Id = :accPartner.Id];

            Test.stopTest();

            System.assertEquals(true, accPartner.Terminated__c);
            System.assertEquals('Terminated', accPartner.Partner_Status__c);
            System.assertEquals(DATE.TODAY(), accPartner.Status_from_Date__c);
            System.assertEquals('Termination', accPartner.Partner_Status_Change_Reason__c);
            System.assertEquals('Terminated', accPartner.Navision_Status__c);
            System.assertEquals(false, accPartner.Publish_To_Directory__c);
        }
    }

    @isTest
    public static void testPartnerAccountTerminationAsAccountOwner() {

        Profile p = [select id from profile where name='Custom: Sales Std User'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, isActive = true,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com');
        insert u;

        Test.startTest();


        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];

        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);

        System.runAs(u) {

            Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
            accPartner.Navision_Status__c = 'Partner';
            accPartner.Payment_Terms__c = '75 days';
            insert accPartner;
            insert QuoteTestHelper.createPCS(accPartner);
            Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
            insert conPartner;

            accPartner = [SELECT Id, Name,Terminated__c,Partner_Status__c,Status_from_Date__c,Partner_Status_Change_Reason__c,Navision_Status__c,Publish_To_Directory__c FROM Account WHERE Id = :accPartner.Id];
            System.assertEquals(false, accPartner.Terminated__c);

            accPartner.Terminated__c = true;
            Semaphores.CustomAccountTriggerBeforeUpdate = false;
            update accPartner;

            accPartner = [SELECT Id, Name,Terminated__c,Partner_Status__c,Status_from_Date__c,Partner_Status_Change_Reason__c,Navision_Status__c,Publish_To_Directory__c FROM Account WHERE Id = :accPartner.Id];

            System.assertEquals(true, accPartner.Terminated__c);
            System.assertEquals('Terminated', accPartner.Partner_Status__c);
            System.assertEquals(DATE.TODAY(), accPartner.Status_from_Date__c);
            System.assertEquals('Termination', accPartner.Partner_Status_Change_Reason__c);
            System.assertEquals('Terminated', accPartner.Navision_Status__c);
            System.assertEquals(false, accPartner.Publish_To_Directory__c);
        }

        Test.stopTest();
    }
}