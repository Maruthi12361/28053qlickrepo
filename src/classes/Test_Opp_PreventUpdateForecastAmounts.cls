/****************************************************** 

    Class: Test_Opp_PreventUpdateForecastAmounts 

    This class tests the Opportunity_PreventUpdateForecastAmounts trigger. 

    Changelog: 
    2013-03-26  CCE Initial development CR# 7524 - Product Family Forecasting Opportunity change
                https://eu1.salesforce.com/a0CD000000XN6U6 

******************************************************/ 
@isTest
private class Test_Opp_PreventUpdateForecastAmounts {

    static User createMockUserForProfile(String profileName, Id ContactId) {
      Profile p = [SELECT Id FROM profile WHERE name like :profileName];
      return createMockUser(p.id, ContactId);
    }
     
    static User createMockUser(Id profileId, Id ContactId) {
      User u = new User(alias = 'newUser', email='newuser@tgerm.com.test',
      emailencodingkey='UTF-8', lastname='Testing', 
      languagelocalekey='en_US', localesidkey='en_US', profileid = profileId, ContactId = ContactId,
      timezonesidkey='America/Los_Angeles', username= System.now().millisecond() + 'newuser@tgerm.com.test');
      insert u;
      return  u; 
    }
    
    static testMethod void TestPreventUpdateForecastAmounts() {

        QTTestUtils.GlobalSetUp();
        //Create the test Account
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
        insert QTComp;
        
        Account TestAccount = new Account(
            Name = 'PUFA Test Ltd.',
            Navision_Status__c = 'Customer',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id                     
        );
        insert TestAccount;
        
        // Run as an Opps Admin as they should not be able to update a forecast value after a Quote ha been added to the Opp        
        System.runAs(createMockUserForProfile('Operations Administrator', null))
        {
            //Add an Opportunity    
            Opportunity Opp = new Opportunity();
            Opp.AccountId = TestAccount.Id;
            Opp.Short_Description__c = 'My Test Description';
            Opp.Name = '.';
            Opp.Amount = 0;
            Opp.CurrencyIsoCode = 'USD';
            Opp.CloseDate = Date.today().addDays(30);
            Opp.StageName = 'Goal Identified';
            insert Opp; 
            
            //Can we update a Forecast amount - we should be able to
            try {
           //     Opp.Education_Forecast_Amount__c = 15.00;
                upsert Opp;
            } catch (Exception Ex) {
                System.debug('Test_Opp_PreventUpdateForecastAmounts: Got Exception (1) = ' + Ex.getMessage());
            }
            
            //Add a Quote to the Opp
            Boolean GotException = false;
            try {
                NS_Quote__c Q = new NS_Quote__c (Opportunity__c = Opp.Id);
                insert Q;   
            } catch (Exception Ex) {
                GotException = true;
                System.debug('Test_Opp_PreventUpdateForecastAmounts: Got Exception (2) = ' + Ex.getMessage());
            }
            //System.assert(GotException == false);
            
            //the Opp should now have one Quote on it           
            Opportunity resOpp = [SELECT Id, (SELECT Quote_Id__c FROM NS_Quotes__r) FROM Opportunity WHERE Id = :Opp.Id];                
            list<NS_Quote__c> resQ = resOpp.NS_Quotes__r;
            System.debug('Test_Opp_PreventUpdateForecastAmounts: resQ.size() = ' + resQ.size());        
            //System.assert(resQ.size() == 1);
            
            test.startTest();
            
            //Can we now update a Forecast amount - we should not be able to
            GotException = false;
            try {
                //Opp.Education_Forecast_Amount__c = 20.00;
                upsert Opp;
            } catch (Exception Ex) {
                GotException = true;
                System.debug('Test_Opp_PreventUpdateForecastAmounts: Got Exception (3) = ' + Ex.getMessage());
            }
            //System.assert(GotException == true);
            test.stopTest();
        }
    }

}