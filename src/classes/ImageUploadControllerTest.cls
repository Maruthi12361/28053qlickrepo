/****************************************************************************************************
   ImageUploadControllerTest
   2019-03-05 IT-1390 : test for ImageUploadController
****************************************************************************************************/
@isTest
public with sharing class ImageUploadControllerTest {

    @TestSetup
    static void setupData() {
        QTCustomSettingsHier__c qtSettings = new QTCustomSettingsHier__c();
        qtSettings.Base_Content_URL__c = 'https://qlik--qa--c.cs89.content.force.com';
        insert qtSettings;
    }

    static testMethod void testUploadImages() {
        Test.setCurrentPageReference(new PageReference('Page.ImageUpload'));
        String namesParam = 'name_one' + '!,!' + 'name_two' + '!,!';
        String idsParam = '11111111' + '!,!' + '2222222' + '!,!';
        String errorNamesParam = 'name_error_one' + '!,!' + 'name_error_two' + '!,!';
        String errorsParam = 'error_one' + '!,!' + 'error_two' + '!,!';

        System.currentPageReference().getParameters().put('namesParam', namesParam);
        System.currentPageReference().getParameters().put('idsParam', idsParam);
        System.currentPageReference().getParameters().put('errorNamesParam', errorNamesParam);
        System.currentPageReference().getParameters().put('errorsParam', errorsParam);

        Test.startTest();
        ImageUploadController controller = new ImageUploadController();
        controller.addImages();
        Test.stopTest();

        System.assert(controller.showErrors);
        System.assert(controller.showImages);
        System.assertEquals(2, controller.imagesErrorsMap.size());
        System.assertEquals(2, controller.imagesMap.size());
        System.assertEquals('error_one', controller.imagesErrorsMap.get('name_error_one'));
        System.assert(controller.imagesMap.get('name_one').contains('11111111'));
        System.assert(controller.imagesMap.get('name_one').contains('https://qlik--qa--c.cs89.content.force.com'));

    }
}