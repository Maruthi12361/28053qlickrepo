public class ConvertToPerpetualController {
    Id oppId;
    public ConvertToPerpetualController(ApexPages.StandardController controller){
        oppId= (Id)controller.getRecord().Id;
    }
    public pageReference updateOpp(){
        //deleting Zuora quotes related to opportunity
        list<zqu__Quote__c> listZQuotes = [select Id from zqu__Quote__c where zqu__Opportunity__c = :oppId];
        if(listZQuotes.size()>0) Delete listZQuotes;
        
        Id SalesQCCSRecordtypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS').getRecordTypeId();   
        //updating opp
        Opportunity opp = new Opportunity();
        opp.Id = oppId;
        opp.RecordTypeId =SalesQCCSRecordtypeId;
        opp.Subscription_Forecast_Amount__c =0;
        Update opp;  
        return new pageReference('/'+oppId);     
    }
}