/**************************************************
* This is a stress Test class for Process Builder and Flow below
* Process Builder: Create CM to Campaign when Task Type equal to TK - Inbound Call
* Flow: Check Campaign Member Flow
*
* Change Log:
* 2016-02-12 BTN (reviewed by CCE) CR# 73334 - Create Trigger for Adding Leads/Contacts to Campaign.
* 2017-03-07 BTN (reviewed by CCE) CR# 106553 - #PRJ - New Process Builder to handle B+S Dailer.
* 2017-04-08 RDZ CR# 106553 Adding try and catch to identify Flow being inactive for QueryException: List has not row for assigment object
* 2018-06-21 EXTBJD  ITRM-228 In test added the creation of a particular Campaign object and Parent Campaign object.
* 2019-01-24 EXTBJD  Changed method createCampaign on runCreationCampaign
**************************************************/

@IsTest

private class TaskAddLeadContactToCampaignTest {

    private final static Integer TEST_TIMES = 100;
    private final static String GLOBAL_CAMPAIGN_NAME = 'Global-TM-Inbound Call';

    @TestSetup static void setupTestData() {
        SandboxRefresh_Controller.runCreationCampaign();
    }

    // Testing creating Task from Lead
    static testMethod void TestLead() {
        Campaign campaign = [SELECT Id FROM Campaign WHERE Name = :GLOBAL_CAMPAIGN_NAME];
        // Creating User and Account using QTTestUtils Mock method
        User u = QTTestUtils.createMockSystemAdministrator();
        Account acc = QTTestUtils.createMockAccount('TestAccount', u);

        Test.startTest();
        List<Lead> testLeads = new List<Lead>();
        for (Integer i = 0; i < TEST_TIMES; i++) {
            Lead l = new Lead(
                    Firstname = 'TestFirstname ' + i,
                    Lastname = 'TestLastname BTN',
                    Company = 'TestQTCompany',
                    Country = 'United Kingdom',
                    LeadSource = 'Part - Partner',
                    Phone = '12345'
            );
            testLeads.add(l);
        }
        insert testLeads;

        List<Lead> retl = [SELECT Id FROM Lead WHERE Lastname = :'TestLastname BTN'];
        List<Task> testTasks = new List<Task>();
        for (Integer i = 0; i < TEST_TIMES; i++) {
            Task T = new Task(
                    WhoId = retl[i].Id,
                    Subject = 'TestSubject ' + i,
                    CallType = 'Inbound',
                    Type = 'TK - Call (Inbound)'
            );
            testTasks.add(T);
        }
        insert testTasks;

        for (Task t : testTasks) {
            t.CallDurationInSeconds = 121;
        }
        update testTasks;

        Lead l_id = [select id from Lead where Firstname = :'TestFirstname 0'];

        // Select the Campaign Member created by the PB/Flow
        CampaignMember campMember;
        try {
            campMember = [SELECT Id, LeadId, ContactId, CampaignId, Status FROM CampaignMember WHERE LeadId = :l_id.Id];
        } catch (QueryException ex) {
            System.debug('Exception: ' + ex.getMessage());
            //No campaing member found, trigger must be inactive, and should be activated.
            campMember = null;
        }

        if (campMember == null) {

            System.assert(false, 'PB/Flow is inactive : Check Campaign Member Flow');
        } else {

            // Testing CampaignMember has the same Lead ID.
            System.assertEquals(campMember.LeadId, l_id.id);

            System.debug('TaskAddLeadContactToCampaign TestLead: campMember.CampaignId  = ' + campMember.CampaignId);
            System.assertEquals(campMember.CampaignId, campaign.Id);

            // Testing CampaignMember has the same Status.
            System.debug('TaskAddLeadContactToCampaign TestLead: campMember.Status  = ' + campMember.Status);
            System.assertEquals(campMember.Status, 'Reached - Respond to Offer');
        }
        Test.stopTest();
    }

    // Testing creating Task from Contact
    static testMethod void TestContact() {
        Campaign campaign = [SELECT Id FROM Campaign WHERE Name = :GLOBAL_CAMPAIGN_NAME];
        // Creating User, Account and Contact using QTTestUtils Mock method
        User u = QTTestUtils.createMockSystemAdministrator();
        Account acc = QTTestUtils.createMockAccount('TestAccount', u);

        Test.startTest();
        List<Contact> testContacts = new List<Contact>();
        for (Integer i = 0; i < TEST_TIMES; i++) {
            Contact c = new Contact(
                    Firstname = 'TestFirstname ' + i,
                    Lastname = 'TestLastname BTN',
                    AccountId = acc.Id,
                    Phone = '12345'
            );
            testContacts.add(c);
        }
        insert testContacts;

        List<Contact> retl = [SELECT Id FROM Contact WHERE LastName = :'TestLastname BTN'];
        List<Task> testTasks = new List<Task>();
        for (Integer i = 0; i < TEST_TIMES; i++) {
            Task T = new Task(
                    WhoId = retl[i].Id,
                    Subject = 'TestSubject ' + i,
                    CallType = 'Inbound',
                    Type = 'TK - Call (Inbound)'
            );
            testTasks.add(T);
        }
        insert testTasks;

        for (Task t : testTasks) {
            t.CallDurationInSeconds = 121;
            System.debug('TaskAddLeadContactToCampaign TestContact: UpdateT = ' + t);
        }
        update testTasks;

        Contact c_id = [SELECT Id FROM Contact WHERE FirstName = :'TestFirstname 0'];

        // Select the Campaign Member created by the PB/Flow
        CampaignMember campMember;
        try {
            campMember = [SELECT Id, LeadId, ContactId, CampaignId, Status FROM CampaignMember WHERE ContactId = :c_id.Id];
        } catch (QueryException ex) {
            //No campaing member found, trigger must be inactive, and should be activated.
            System.debug('Exception: ' + ex.getMessage());
            campMember = null;
        }

        if (campMember == null) {
            //System.assert(false, 'PB/Flow is inactive : Check Campaign Member Flow');
            System.assert(false, 'PB/Flow is inactive : Check Campaign Member Flow');
        } else {
            // Testing CampaignMember has the same Contact ID.
            System.debug('TaskAddLeadContactToCampaign TestContact: campMember.ContactId = ' + campMember.ContactId + ', c_id.Id  = ' + c_id.Id);
            System.assertEquals(campMember.ContactId, c_id.Id);

            System.debug('TaskAddLeadContactToCampaign TestContact: campMember.CampaignId  = ' + campMember.CampaignId);
            System.assertEquals(campMember.CampaignId, campaign.Id);

            // Testing CampaignMember has the same Status.
            System.debug('TaskAddLeadContactToCampaign TestContact: campMember.Status  = ' + campMember.Status);
            System.assertEquals('Reached - Respond to Offer', campMember.Status);
        }

        Test.stopTest();
    }
}