/****************************************************************************************************

   SCC_Case_ExclamationExtension

   IT-662 : ext_bad : Add message for Special_SLA_customer__c
   27-05-2019   ext_vos CHG0036251: update help text and condition; refactoring.
   20-04-2020   ext_cqb IT-2706: Retire Strategic Account notification on Cases and introduce Signature.

****************************************************************************************************/

public with sharing class SCC_Case_ExclamationExtension {
    private static final String LINE_BREAK = '---Linebreak---';
    private static final String SLA_HELP_TEXT = 'Special SLA customer: Customer has special SLA around resolution times, please contact the Escalation team';
    private static final String ACCOUNT_LABEL = 'Account Support Information: ';
    private static final String END_USER_LABEL = 'End User - Account Support Information: ';

    private Case caseObj;
    public String accountInfoHelpText;
    public Boolean isAccountInfo;
    public Boolean caseHasEscalation = false;

    public SCC_Case_ExclamationExtension(ApexPages.StandardController stdController) {
        this.caseObj = (Case)stdController.getRecord();
        caseObj = [select id, 
                        Account_Origin__r.Red_Account__c, Account_Origin__r.Customer_Success_Manager__c,
                        Account_Origin__r.Special_SLA_customer__c,
                        Account.Account_Support_Information__c, 
                        Account_Origin__r.Account_Support_Information__c 
                    from Case where Id = :caseObj.Id];
        caseHasEscalation = hasEscalation(); 
    }
    
    public Boolean getcaseHasEscalation() {
        return caseHasEscalation;
    }

    public Boolean hasEscalation() {
        List<Qlikview_Support_Escalation__c> escalations = [select Id 
                                                            from Qlikview_Support_Escalation__c 
                                                            where Case__c = :caseObj.Id and Status__c != 'Resolved' limit 1];
        return escalations.size() > 0;
    }

    public Boolean getIsAccountInfo() {
        return caseObj.Account_Origin__r.Special_SLA_customer__c || caseObj.Account_Origin__r.Account_Support_Information__c != null
                || caseObj.Account.Account_Support_Information__c != null;
    }
    
    public String getAccountInfoHelpText() {
        accountInfoHelpText = '';
        Boolean accountSupportInformationIsShown = false;
        Boolean needsLineBreak = false;
        if (caseObj.Account_Origin__r.Special_SLA_customer__c) {
            if (needsLineBreak) {
                accountInfoHelpText += LINE_BREAK;
            }
            accountInfoHelpText += SLA_HELP_TEXT;
            needsLineBreak = true;
        }
        if (caseObj.Account.Account_Support_Information__c != null) {
            if (needsLineBreak) {
                accountInfoHelpText += LINE_BREAK;
            }
            accountInfoHelpText += ACCOUNT_LABEL + caseObj.Account.Account_Support_Information__c;
            accountSupportInformationIsShown = true;
            needsLineBreak = true;
        }
        if (caseObj.Account_Origin__r.Account_Support_Information__c != null && (caseObj.AccountId != caseObj.Account_Origin__c || !accountSupportInformationIsShown)) {
            if (needsLineBreak) {
                accountInfoHelpText += LINE_BREAK;
            }
            accountInfoHelpText += END_USER_LABEL + caseObj.Account_Origin__r.Account_Support_Information__c;
        }
        return accountInfoHelpText;
    }
}