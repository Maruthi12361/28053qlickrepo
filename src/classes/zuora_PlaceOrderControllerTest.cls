@isTest
private class zuora_PlaceOrderControllerTest {
     //private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';


    //List of accounts to be processed in test methods
    public Static List<Account> accounts = new List<Account>();


   @testSetup
    private static void testSetup() {
        QuoteTestHelper.createCustomSettings();
    }
    @isTest static void test_methodNonOEM() {
        Subsidiary__c sub = new Subsidiary__c(name='Umpa Lumpa',Legal_Entity_Name__c='Umpa Lumpa',CurrencyIsoCode='USD');
        insert sub;

       Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'No Matches';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        endUserAccount.Subsidiary__c = sub.Id;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'No Matches';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';
        partner.Subsidiary__c = sub.Id;
        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'No Matches';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;
        
        Test.startTest();
        //Constructer initilization
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        zquote.Quote_Status__c = 'Approved';
        //zquote.Subsidiary__c  = 'Umpa lumpa';
        insert zquote;
        
        zqu__Quote__c zquote1 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, Doc_Launcher_Config__c, zqu__Number__c, 
            zqu__Opportunity__r.OppNumber__c, zqu__Account__r.AccNumber__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(zquote1);
            zuora_PlaceOrderController placeOrderCntr = new zuora_PlaceOrderController(sc);
            placeOrderCntr.quote = zquote1;
            System.assertEquals(true, placeOrderCntr.status);
            System.assertEquals(true, placeOrderCntr.VCstatus);
            
            Account testaccount1 = placeOrderCntr.enduseraccount;
            ACcount testacc2 = placeOrderCntr.secondpartner;
            Account testacc3 =placeOrderCntr.sellthroughpartner;
            placeOrderCntr.sellthroughpartner = [select id, po_Required__c from account where id = :partner.id];
            Boolean po = placeOrderCntr.poRequired;
            Test.stopTest();

    }
    
    @isTest static void test_methodOEM() {
        Subsidiary__c sub = new Subsidiary__c(name='Umpa Lumpa',Legal_Entity_Name__c='Umpa Lumpa',CurrencyIsoCode='USD');
        insert sub;

       Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'No Matches';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        endUserAccount.Subsidiary__c = sub.Id;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'No Matches';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';
        partner.Subsidiary__c = sub.Id;
        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'No Matches';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;
        
        Account partner2 = Z_TestFactory.makePartnerAccount('United States', 'Test Account 2249', '','65000-480');
        partner2.ECUSTOMS__RPS_Status__c= 'No Matches';
        partner2.ECUSTOMS__RPS_Date__c= Date.Today();
        partner2.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner2.BillingStreet='hhhh';
        partner2.BillingCity='iiiii';
        partner2.Subsidiary__c = sub.Id;
        insert partner2;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.End_User_Account_del__c = partner2.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'OEM';
        insert testOpp;
        
        Test.startTest();
        //Constructer initilization
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        zquote.Quote_Status__c = 'Approved';
        //zquote.Subsidiary__c  = 'Umpa lumpa';
        insert zquote;
        
        zqu__Quote__c zquote1 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, Doc_Launcher_Config__c, zqu__Number__c, 
            zqu__Opportunity__r.OppNumber__c, zqu__Account__r.AccNumber__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(zquote1);
            zuora_PlaceOrderController placeOrderCntr = new zuora_PlaceOrderController(sc);
            placeOrderCntr.quote = zquote1;
            System.assertEquals(true, placeOrderCntr.status);
            System.assertEquals(true, placeOrderCntr.VCstatus);
            
            Account testaccount1 = placeOrderCntr.enduseraccount;
            ACcount testacc2 = placeOrderCntr.secondpartner;
            Account testacc3 =placeOrderCntr.sellthroughpartner;

            Test.stopTest();
    }
    
}