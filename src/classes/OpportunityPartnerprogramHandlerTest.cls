/******************************************************

        OpportunityPartnerprogramHandlerTest
        Kumar Navneet
        Date - 03/08/17

        Changelog:
        16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors

        Changelog:
        23.03.2017 : Kumar Navneet : changes for coverage
        25.07.2017 : Pramod Kumar :Paex 71 added logics to increase code coverage
        04.03.2019 : AIN Fixed test class issue with record type missing in insert.
    ******************************************************/

@isTest(seealldata=false)
private class OpportunityPartnerprogramHandlerTest {

    public static TestMethod void TEST_OpportunityPartnerprogramHandler()
    {
        QTTestUtils.GlobalSetUp();

        QTCustomSettings__c settings = new QTCustomSettings__c();
    settings.Name = 'Default';
    settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        insert settings;

        Subsidiary__c  sub = new Subsidiary__c (
            Name = 'test',
            Legal_Entity_Name__c = 'Test'

        );
         insert sub;
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc' ,
            Subsidiary__c = sub.id

        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        Id AccRecordTypeId_OEMEndUserAccount = '012D0000000KBYx'; //OEM End User
        
        List<RecordType> partnerAccountRecordTypes = [select id from Recordtype where developername= 'Partner_Account' and sobjecttype = 'Account'];
        RecordType partnerAccountRecordType = null;
        system.assert(partnerAccountRecordTypes.size() > 0, 'Partner Account record type not found');
        partnerAccountRecordType = partnerAccountRecordTypes[0];
        
        Account TestAccount = new Account(
            Name = 'ULC Test Ltd.',
            Navision_Status__c = 'Customer',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Partner_Type__c='SP - Elite',
            Program_Version__c='The Partner Program (new)',
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Commercial - Lower SMB'
        );
        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Partner_Type__c='SP - Elite',
            Program_Version__c='The Partner Program (new)',
            Segment_New__c = 'Commercial - Lower SMB',
            RecordTypeId = partnerAccountRecordType.Id
        );
        List<Account> AccToInsert = new List<Account>();
        AccToInsert.Add(TestAccount);
        AccToInsert.Add(TestPartnerAccount);
        insert AccToInsert;

        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partnerr',
            LastName = 'PPr',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact;

        Contact PartnerContact1 = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact1;

         Contact PartnerContact2 = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'test',
            AccountId = TestAccount .Id
        );
        insert PartnerContact2;

        test.startTest();

        Opportunity Opp = new Opportunity();
        Opp.AccountId = TestAccount.Id;
        Opp.Sell_Through_Partner__c=TestAccount.Id;
        Opp.Second_Partner__c=TestPartnerAccount.Id;
        Opp.PRM_Referring_Contact__c = PartnerContact1.Id;


        opp.Partner_Fee_Type__c='Influence Fee';
        opp.Standard_Referral_Margin__c='10';
        Opp.Short_Description__c = 'My Test Description';
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'USD';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Identified';
        Opp.Partner_Contact__c = PartnerContact.Id;
        Opp.Revenue_Type__c='Distributor';
        insert Opp;

        Opportunity Opp1 = new Opportunity();
        Opp1.AccountId = TestAccount.Id;
        Opp1.Sell_Through_Partner__c=TestAccount.Id;
        Opp1.Second_Partner__c=TestPartnerAccount.Id;

        Opp1.Partner_Fee_Type__c='Influence Fee';
        Opp1.Standard_Referral_Margin__c='10';
        Opp1.Short_Description__c = 'My Test Description';
        Opp1.Name = '.';
        Opp1.Amount = 0;
        Opp1.CurrencyIsoCode = 'USD';
        Opp1.CloseDate = Date.today().addDays(30);
        Opp1.StageName = 'Goal Identified';
        Opp1.Partner_Contact__c = PartnerContact.Id;
        Opp1.Revenue_Type__c='Reseller';
        insert Opp1;

        Opp.PRM_Referring_Partner__c= null;
        Opp.Standard_Referral_Margin__c = '5';
        Opp.Partner_Fee_Type__c =  'Finder’s Fee';
        Opp.Revenue_Type__c='Reseller';
        update Opp;
        List<Opportunity> oppList=new List<Opportunity>();
        oppList.add(Opp);


        //Opp1.PRM_Referring_Contact__c= PartnerContact1.Id;
        Opp1.Standard_Referral_Margin__c = '5';
        Opp1.Partner_Fee_Type__c =  'Finder’s Fee';
        update Opp1;

        Opp1.PRM_Referring_Contact__c= null;
        Opp1.Partner_Fee_Level__c = '';
        Opp1.PRM_Referring_Partner__c= '';

        update Opp1;
        oppList.add(Opp1);
        OpportunityPartnerprogramHandler.Inserthandler(oppList);

        test.stopTest();

    }



}