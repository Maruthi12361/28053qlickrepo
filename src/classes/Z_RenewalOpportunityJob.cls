/**     
 * File Name : Z_RenewalOpportunityJob
 * Description : This is a schedulable job to create renewal opportunities, which triggers the Z_RenewalOpportunityBatchable job.
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

global class Z_RenewalOpportunityJob implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        run();
    }

    global void run() {
        Renewal_Automation_Settings__c settings = Renewal_Automation_Settings__c.getOrgDefaults();
        Integer batchSize = (null == settings.OpportunityBatchSize__c) ? null : (Integer) settings.OpportunityBatchSize__c;
        Set<String> renewalOpportunityIdsReference = new Set<String>(),
                    opportunitiesToExclude = new Set<String>();

        for (Apex_Debug_Log__c apexDebugLog : [SELECT Record_Id__c FROM Apex_Debug_Log__c
                                               WHERE Apex_Class__c = 'Z_RenewalOpportunityJob'
                                               AND Message__c = 'Processing'
                                               AND Method__c = 'createRenewalOpportunities'
                                               AND Stack_Trace__c = NULL
                                               AND Type__c = 'Information']) {
            renewalOpportunityIdsReference.add(apexDebugLog.Record_Id__c);
        }

        for (Opportunity opptyToExclude: [SELECT Id, Previous_Opportunity__c FROM Opportunity WHERE Id IN :renewalOpportunityIdsReference]) {
            opportunitiesToExclude.add(opptyToExclude.Previous_Opportunity__c);
        }

        Map<String, Map<String, Object>> subscriptionFieldsMap = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();
        Z_RenewalOpportunityBatchable batchable = new Z_RenewalOpportunityBatchable(subscriptionFieldsMap, opportunitiesToExclude);

        if (null != batchSize) {
            Database.executeBatch(batchable, batchSize);
        } else {
            Database.executeBatch(batchable);
        } 
    }
}