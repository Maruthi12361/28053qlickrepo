/**************************************************************************************************** 
   UserDeactiveInactiveCustomerUsers
   IT-739 : Class to deactivate users that have not logged in for 365 days, they will be automatically reactivated if they try to loggin to the support portal
****************************************************************************************************/

global class UserDeactiveInactiveCustomerUsers implements Database.Batchable<sObject>, Database.stateful {

    global String query;
    global UserDeactiveInactiveCustomerUsers(integer limitUsers) {
        //Query to get the users records for users that have not logged in for 365 days
        query = 'select Id, LastLoginDate, isActive from User ' 
                    + ' where isActive = true ' 
                        + ' and ((LastLoginDate = null and CreatedDate < last_n_days:365) or LastLoginDate < last_n_days:1065) ' 
                        + ' and (Profile.Name = \'Customer Portal Case Logging Access\' or Profile.Name = \'Customer Portal Case Viewing Access\') ' 
                        + ' and (Contact.Account.Navision_Status__c = \'Customer\' or Contact.Account.Navision_Status__c = \'Partner\') ' 
                    + ' order by LastLoginDate asc limit ' + limitUsers;
        if (test.isRunningTest()) {
            query = 'select Id, LastLoginDate, isActive from User where Firstname = \'Andreas\' and LastName = \'UserDeactiveInactiveCustomerUsers\'';
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Database.QueryLocator ql = Database.getQueryLocator(query);
        return ql;
    }

    global void execute(Database.BatchableContext BC, List<USer> scope) {
        for (User  u : scope) {
            u.isActive = false;
        }
        Database.update(scope, false);
    }

    global void finish(Database.BatchableContext BC) {
    }
}