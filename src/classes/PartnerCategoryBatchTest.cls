/**     * File Name: PartnerCategoryBatchTest
        * Description : This apex class is unit test for PartnerCategoryBatchHandler
        * @author : Rodion Vakulovskyi
        * Jira Id : QCW-3903
        * Modification Log :

        Ver     Date         Author
        1      18/09/2017  Rodion Vakulovskyi
        2      18/04/2018  ain for BSL-10
        3      09/07/2018  ext_axf BSL-370
*/
@isTest
private class PartnerCategoryBatchTest {
    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    private static testmethod void testPartnerCategoryQPP() {
        QuoteTestHelper.createCustomSettings();
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
        RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        insert accEndUser;
        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;
        Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        insert accPartner;
        insert QuoteTestHelper.createPCS(accPartner);
        createAdditionalPartnerCats(accPartner);
        Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
        insert conPartner;
        Test.startTest();
            SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
            Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accEndUser, accPartner.Id, [Select id From RecordType where DeveloperName ='Sales_QCCS']);
            oppCreatedForUpdate.Revenue_Type__c = 'Reseller';
            oppCreatedForUpdate.Partner_Contact__c = conPartner.Id;
            oppCreatedForUpdate.CurrencyIsoCode = 'USD';
            insert oppCreatedForUpdate;
            SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, accPartner.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote.Quote_Recipient__c = contEndUser.Id;
            quote.Sell_Through_Partner__c = accPartner.Id;
            insert quote;
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            PartnerCategoryBatch batchPartnerCategory = new PartnerCategoryBatch();
            database.executebatch(batchPartnerCategory, 25);
            Partner_Category_Status__c pcs2 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'QPP',
                                                                            Partner_Level__c = 'Elite',
                                                                            Partner_Category__c = 'Resell',
                                                                            Partner_Level_Status__c='Active');
            insert pcs2;
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            database.executebatch(batchPartnerCategory, 25);
            delete [SELECT Id FROM Partner_Category_Status__c WHERE Program_Version__c = 'QPP' AND Partner_Category__c = 'Resell'];
            database.executebatch(batchPartnerCategory, 25);
            Partner_Category_Status__c pcs3 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'Partner Program 4.1',
                                                                            Partner_Level__c = 'Solution Provider',
                                                                            Partner_Category__c = 'Solution Provider',
                                                                            Partner_Level_Status__c='Active');
            insert pcs3;
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            database.executebatch(batchPartnerCategory, 25);
            delete [SELECT Id FROM Partner_Category_Status__c WHERE Program_Version__c = 'Partner Program 4.1'];
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            database.executebatch(batchPartnerCategory, 25);
            Test.stopTest();
    }

    private static testmethod void testPartnerCategorySecondPartner() {
        QuoteTestHelper.createCustomSettings();
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
        RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        insert accEndUser;
        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;
        Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        insert accPartner;
        Account accPartner2 = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAccsecond');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        insert accPartner2;
        insert QuoteTestHelper.createPCS(accPartner);
        createAdditionalPartnerCats(accPartner);
        Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
        insert conPartner;
        Test.startTest();
            SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
            Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accEndUser, accPartner.Id, [Select id From RecordType where DeveloperName ='Sales_QCCS']);
            oppCreatedForUpdate.CurrencyIsoCode = 'USD';
            oppCreatedForUpdate.Partner_Contact__c = conPartner.Id;
            oppCreatedForUpdate.Revenue_Type__c = 'Reseller';
            insert oppCreatedForUpdate;
            SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, accPartner.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote.Quote_Recipient__c = contEndUser.Id;
            quote.Second_Partner__c = accPartner2.Id;
            insert quote;
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            PartnerCategoryBatch batchPartnerCategory = new PartnerCategoryBatch();
            database.executebatch(batchPartnerCategory, 25);
            Partner_Category_Status__c pcs2 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'QPP',
                                                                            Partner_Level__c = 'Elite',
                                                                            Partner_Category__c = 'Resell',
                                                                            Partner_Level_Status__c='Active');
            insert pcs2;
            database.executebatch(batchPartnerCategory, 25);
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            delete [SELECT Id FROM Partner_Category_Status__c WHERE Program_Version__c = 'QPP' AND Partner_Category__c = 'Resell'];
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            database.executebatch(batchPartnerCategory, 25);
            Partner_Category_Status__c pcs3 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'Partner Program 4.1',
                                                                            Partner_Level__c = 'Solution Provider',
                                                                            Partner_Category__c = 'Solution Provider',
                                                                            Partner_Level_Status__c='Active');
            insert pcs3;
            database.executebatch(batchPartnerCategory, 25);
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            delete [SELECT Id FROM Partner_Category_Status__c WHERE Program_Version__c = 'Partner Program 4.1'];
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote});
            database.executebatch(batchPartnerCategory, 25);
            Test.stopTest();
    }

    private static testmethod void distributorTest() {
        QuoteTestHelper.createCustomSettings();
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
        RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        insert accEndUser;
        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;
        Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        insert accPartner;
        insert QuoteTestHelper.createPCS(accPartner);
        createAdditionalPartnerCats(accPartner);
        Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
        insert conPartner;
        Test.startTest();
            SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
            Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accEndUser, accPartner.Id, [Select id From RecordType where DeveloperName ='Sales_QCCS']);
            oppCreatedForUpdate.CurrencyIsoCode = 'USD';
            oppCreatedForUpdate.Partner_Contact__c = conPartner.Id;
            oppCreatedForUpdate.Second_Partner__c = accPartner.Id;
            oppCreatedForUpdate.Revenue_Type__c = 'Distributor';
            insert oppCreatedForUpdate;
            SBQQ__Quote__c quote2 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, accPartner.Id, 'Distributor', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote2.Quote_Recipient__c = contEndUser.Id;
            quote2.Second_Partner__c = accPartner.Id;
            insert quote2;
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote2});
            PartnerCategoryBatch batchPartnerCategory = new PartnerCategoryBatch();
            database.executebatch(batchPartnerCategory, 25);
            SBQQ__Quote__c quote3 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, accPartner.Id, 'Distributor', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote3.Quote_Recipient__c = contEndUser.Id;
            quote3.Sell_Through_Partner__c = accPartner.Id;
            quote3.Second_Partner__c = accPartner.Id;
            insert quote3;
            Partner_Category_Status__c pcs2 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'QPP',
                                                                            Partner_Level__c = 'Elite',
                                                                            Partner_Category__c = 'Resell',
                                                                            Partner_Level_Status__c='Active');
            insert pcs2;
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote2, quote3});
            database.executebatch(batchPartnerCategory, 25);
            delete [SELECT Id FROM Partner_Category_Status__c WHERE Program_Version__c = 'QPP' AND Partner_Category__c = 'Resell'];
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote2, quote3});
            database.executebatch(batchPartnerCategory, 25);
            Partner_Category_Status__c pcs3 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'Partner Program 4.1',
                                                                            Partner_Level__c = 'Solution Provider',
                                                                            Partner_Category__c = 'Solution Provider',
                                                                            Partner_Level_Status__c='Active');
            insert pcs3;
            database.executebatch(batchPartnerCategory, 25);
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote2, quote3});
            delete [SELECT Id FROM Partner_Category_Status__c WHERE Program_Version__c = 'Partner Program 4.1'];
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote2, quote3});
            database.executebatch(batchPartnerCategory, 25);
        Test.stopTest();
    }

    private static testmethod void mspTest() {
        QuoteTestHelper.createCustomSettings();
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
        RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        accEndUser.Territory_Country__c = 'France';
        insert accEndUser;
        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;
        Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        accPartner.Territory_Country__c = 'France';
        insert accPartner;
        insert QuoteTestHelper.createPCS(accPartner);
        createAdditionalPartnerCats(accPartner);
        Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
        insert conPartner;
        List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Billing'),
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Shipping'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Billing'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Shipping')
         };
        insert addrs;

            Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accPartner, accPartner.Id, [Select id From RecordType where DeveloperName ='Sales_QCCS']);
            oppCreatedForUpdate.CurrencyIsoCode = 'USD';
            oppCreatedForUpdate.Partner_Contact__c = conPartner.Id;
            oppCreatedForUpdate.Revenue_Type__c = 'MSP';
            insert oppCreatedForUpdate;
            SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
            Test.startTest();
            SBQQ__Quote__c quote5 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accPartner, accPartner.Id, 'MSP', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote5.Quote_Recipient__c = conPartner.Id;
            quote5.Second_Partner__c = null;
            quote5.Second_Partner_Contact__c = null;
            quote5.Resell_SPD_Discount__c = null;
            quote5.SBQQ__Account__c = accPartner.Id;
            insert quote5;
            PartnerCategoryBatch batchPartnerCategory = new PartnerCategoryBatch();
            database.executebatch(batchPartnerCategory, 25);
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote5});
        Test.stopTest();
    }

    private static testmethod void oemTest() {
        QuoteTestHelper.createCustomSettings();
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
        RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        accEndUser.Territory_Country__c = 'France';
        insert accEndUser;
        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;
        Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        accPartner.Territory_Country__c = 'France';
        insert accPartner;
        insert QuoteTestHelper.createPCS(accPartner);
        createAdditionalPartnerCats(accPartner);
        Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
        insert conPartner;
        List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Billing'),
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Shipping'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Billing'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Shipping')
         };
        insert addrs;

        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accEndUser, accPartner.Id, [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        oppCreatedForUpdate.Revenue_Type__c = 'OEM';
        oppCreatedForUpdate.CurrencyIsoCode = 'USD';
        oppCreatedForUpdate.Sell_Through_Partner__c = accPartner.Id;
        oppCreatedForUpdate.Partner_Contact__c = conPartner.Id;
        oppCreatedForUpdate.End_User_Account_del__c = accEndUser.Id;
        insert oppCreatedForUpdate;
        Test.startTest();
        SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
        SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
        SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
        SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
        SBQQ__Quote__c quote4 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, accPartner.Id, 'OEM', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
        quote4.Quote_Recipient__c = contEndUser.Id;
        quote4.Bypass_Rules__c = true;
        quote4.CurrencyIsoCode = 'USD';
        insert quote4;
        Test.stopTest();
        PartnerCategoryBatch batchPartnerCategory = new PartnerCategoryBatch();
        database.executebatch(batchPartnerCategory, 25);

        //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote4});
    }

    private static testmethod void accTest() {
        QuoteTestHelper.createCustomSettings();
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
        RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        accEndUser.Territory_Country__c = 'France';
        insert accEndUser;
        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;
        Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        accPartner.Territory_Country__c = 'France';
        insert accPartner;
        insert QuoteTestHelper.createPCS(accPartner);
        createAdditionalPartnerCats(accPartner);
        Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
        insert conPartner;
        List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Billing'),
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Shipping'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Billing'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Shipping')
         };
        insert addrs;
        Test.startTest();
            SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
            Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accEndUser, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
            oppCreatedForUpdate.CurrencyIsoCode = 'USD';
            oppCreatedForUpdate.Revenue_Type__c = 'Direct';
            insert oppCreatedForUpdate;
            SBQQ__Quote__c quote5 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, '', 'Direct', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote5.Quote_Recipient__c = contEndUser.Id;
            insert quote5;
            PartnerCategoryBatch batchPartnerCategory = new PartnerCategoryBatch();
            database.executebatch(batchPartnerCategory, 25);
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote5});
        Test.stopTest();
    }

    private static testmethod void directTest() {
        QuoteTestHelper.createCustomSettings();
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
        RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        accEndUser.Territory_Country__c = 'France';
        insert accEndUser;
        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;
        Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
        accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
        accPartner.Territory_Country__c = 'France';
        insert accPartner;
        insert QuoteTestHelper.createPCS(accPartner);
        createAdditionalPartnerCats(accPartner);
        Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
        insert conPartner;
        List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Billing'),
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Shipping'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Billing'),
                QuoteTestHelper.createAddress(accPartner.Id, conPartner.Id, 'Shipping')
         };
        insert addrs;
        Test.startTest();
            SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
            Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accEndUser, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
            oppCreatedForUpdate.CurrencyIsoCode = 'USD';
            oppCreatedForUpdate.Revenue_Type__c = 'Direct';
            insert oppCreatedForUpdate;
            SBQQ__Quote__c quote6 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, '', 'Direct', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote6.Quote_Recipient__c = contEndUser.Id;
            quote6.Second_Partner__c = accPartner.Id;
            insert quote6;
            PartnerCategoryBatch batchPartnerCategory = new PartnerCategoryBatch();
            database.executebatch(batchPartnerCategory, 25);
            //PartnerCategoryBatchHandler.processBatch(new List<SBQQ__Quote__c>{quote6});
        Test.stopTest();
    }
    private static void createAdditionalPartnerCats(Account accPartner) {
        Partner_Category_Status__c pcs1 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'QPP',
                                                                            Partner_Level__c = 'Elite',
                                                                            Partner_Category__c = 'MSP',
                                                                            Partner_Level_Status__c ='Active');

        Partner_Category_Status__c pcs2 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'Master Reseller',
                                                                            Partner_Level__c = 'Master Reseller',
                                                                            Partner_Category__c = 'Master Reseller',
                                                                            Partner_Level_Status__c ='Active');
        Partner_Category_Status__c pcs3 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
                                                                            Program_Version__c = 'Partner Program 4.1',
                                                                            Partner_Level__c = 'Solution Provider',
                                                                            Partner_Category__c = 'Solution Provider',
                                                                            Partner_Level_Status__c='Active');
        List<Partner_Category_Status__c> listToInsert = new List<Partner_Category_Status__c>{pcs1, pcs2, pcs3};
        insert listToInsert;
    }
}