/******************************************************************
Name: TestOperationsRequestBeforeInsertTrigger
for P0212 - Operations Request Portal Enhancements Project
https://projects.qliktech.com/sites/projects/QSMRequestPortal/default.aspx
To test OperationsRequestBeforeInsertTrigger

2012-11-08  TJG Initial creation. Test default approval status of 'New' 
2013-12-12  BTN Need to comment 'Description__c' to change datatype, and after datatype change uncomment it CR# 10225.
*******************************************************************/
@isTest
private class TestOperationsRequestBeforeInsertTrigger {

    static testMethod void testOperationsRequestBeforeInsert() {
        Marketing_Request__c oRequest = new Marketing_Request__c(
            Subject__c = 'Test subject 0000',
            Requestor__c = '005D0000001sVDgIAM',
            Request_Type__c = 'Enhancement Request',
            Description__c = 'Test OR before insert trigger'
        );
        
        insert oRequest;
        oRequest = [select Id, Approval_Status_QSM__c from Marketing_Request__c where Id = :oRequest.Id];
        
        test.startTest();

        System.assertEquals('New', oRequest.Approval_Status_QSM__c);
        
        test.stopTest();    
    }
}