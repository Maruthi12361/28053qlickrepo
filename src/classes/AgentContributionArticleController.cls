/*
Name:  AgentContributionArticleController.cls
Copyright © 2011  salesforce.com consulting
======================================================
======================================================
Purpose:
-------
======================================================
======================================================
History
------- 
VERSION AUTHOR                 DATE        DETAIL
1.0     Mark Cane&             2011-10-13  Initial development.
2.0     Alberto de Diego       2012-06-05  CR# 4594
3.0     BTN (reviewed by CCE)  2013-10-03  CR# 9363
*/
public class AgentContributionArticleController {
 
    public AgentContributionArticleController(ApexPages.KnowledgeArticleVersionStandardController ctl) {
        
        //Diagnostic__kav article = (Diagnostic__kav)ctl.getRecord();   //this is the SObject for the new article.
        SObject article = ctl.getRecord();
        
        if (!(article instanceof Basic__kav)) {
            //throw new WrongArticleException('This Article type is no longer in use. Please select Basic or Diagnostic instead.');
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Article type is no longer in use. Please select Basic instead.'));          
        } else {
            String sourceId = ctl.getSourceId(); //this returns the id of the case that was closed.
            Case c = [select Subject, Description, Steps_to_reproduce__c, Resolution__c, Area__c, Product__c, Service_Request_Type__c, Type from Case where id=:sourceId];
            
            article.put('Title', c.subject);  //this overrides the default behavior of pre-filling the title of the article with the subject of the closed case. 
            
            if (String.isBlank(c.Steps_to_reproduce__c)) {
               article.put('Description__c', ifNullEmptyString(c.description));
            } else {
               article.put('Description__c', ifNullEmptyString(c.description) + '<br>' + 'Steps to Reproduce: ' + '<br>' + c.Steps_to_reproduce__c);
            }
            
            article.put('Resolution__c', ifNullEmptyString(c.Resolution__c));
            article.put('Tags__c',  ifNullEmptyString(c.Area__c));
           
            if (c.Type=='Incident') {
               if (c.Product__c==null) {
                  ctl.selectDataCategory('Product','Product');      
               } else {
                  String tempProduct = String.valueOf(c.Product__c);
                  if(tempProduct == 'Server/Publisher') {
                      tempProduct = 'Server_Publisher';
                  }
                  else {
                      tempProduct = tempProduct.replaceAll(' ','_');
                  }
                  ctl.selectDataCategory('Product', tempProduct);
               }
            }

            if (c.Type=='Service Request') {
               if (c.Service_Request_Type__c=='Other' || c.Service_Request_Type__c==null) {
                  ctl.selectDataCategory('Product','Qoncierge');        
               } else {
                  String tempService=String.valueOf(c.Service_Request_Type__c);
                  tempService=tempService.replaceAll(' ','_');
                  ctl.selectDataCategory('Product', tempService);  
               }
            }
            // if (c.Product__c!=null) ctl.selectDataCategory('Product', c.Product__c); //No longer used, replaced by above CR# 9363 /BTN
            if (c.Area__c!=null) ctl.selectDataCategory('Area', c.Area__c); //Only one category per category group can be specified.
        }
    }
    
//    public class WrongArticleException extends Exception {}
    
    //Alberto de Diego: old code no longer in use
/*
    private static void copyCaseFieldsToArticle(Basic__kav article, Case c) {
        article.Title = c.subject;  //this overrides the default behavior of pre-filling the title of the article with the subject of the closed case. 
        article.Description__c = ifNullEmptyString(c.description);
        //aaron: modified line below
        article.Resolution__c = ifNullEmptyString(c.Resolution__c) + '<br>' + 'Steps to Reproduce: ' + '<br>' + ifNullEmptyString(c.Steps_to_reproduce__c);
        article.Tags__c =  ifNullEmptyString(c.Area__c);
    }
*/    
    private static String ifNullEmptyString(String v){
        return v == null ? '' : v;
    }
    
}