/**     
 * File Name : Z_RenewalQuoteRulesEngineBatchable
 * Description : This is a batchable job to run the rules engine against the renewal quotes, triggered by 
 *               Z_RenewalQuoteBuildAndSaveBatchable batchable class.  It calls the following methods in this order:
 *               1. start - returns QueryLocator to pass a list of renewal quotes created by Z_RenewalQuoteBatchable, which already 
 *                          went through the Z_RenewalQuoteBuildAndSaveBatchable batchable class
 *               2. execute - retrieves the list of active rules and runs them against renewal quotes retrieved from step 1
 *               3. finish - terminates the job
 * @author : Inki Hong
 * Modification Log ======================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

global with sharing class Z_RenewalQuoteRulesEngineBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts {

    private List<Id> quoteIds;

    private final String CLASS_NAME = 'Z_RenewalQuoteRulesEngineBatchable',
                         DEBUG_PREFIX = '> $ ' + this.CLASS_NAME;

    global Z_RenewalQuoteRulesEngineBatchable(List<Id> quoteIds) {
        this.quoteIds = quoteIds;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String queryString = 'SELECT Id '
                           + 'FROM zqu__Quote__c '
                           + 'WHERE Id IN (\'' + String.join(this.quoteIds, '\',\'') + '\')';
        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext bc, List<zqu__Quote__c> quotes) {
        List<zqu__ZRule__c> rulesToRun = [SELECT Id, Name, zqu__Json__c, zqu__ActionJson__c FROM zqu__ZRule__c WHERE zqu__Active__c = TRUE];

        for (zqu__Quote__c quote : quotes) {
            Id quoteId = quote.Id;
            Set<zqu.CacheId> requiredIds = new Set <zqu.CacheId> ();
        
            requiredIds.add(new zqu.CacheId('zqu__Quote__c', quoteId));
            
            List<zqu.DataObject> dataChargeGroups = new List<zqu.DataObject>();
            List<zqu.DataObject> dataCharges = new List<zqu.DataObject>();
            List<zqu.ZChargeGroup> chargeGroups = zqu.zQuoteUtil.getChargeGroups(quoteId);
            
            for (zqu.ZChargeGroup chargeGroup : chargeGroups) {
                zqu.DataObject dataChargeGroup = (zqu.DataObject) new zqu.ZChargeGroupDataObject(chargeGroup);

                if (chargeGroup.groupType == 9) {
                    dataChargeGroups.add(dataChargeGroup);
                    dataCharges.addAll(dataChargeGroup.getChildren('zqu__QuoteRatePlanCharge__c'));
                }
            }
        
            Map<String, List<zqu.DataObject>> dataObjects = new Map<String, List<zqu.DataObject>> {
                'zqu__QuoteRatePlan__c' => dataChargeGroups,
                'zqu__QuoteRatePlanCharge__c' => dataCharges
            };
        
            zqu.RulesEngine re = new zqu.RulesEngine('zqu__Quote__c', rulesToRun);
            re.initialize(dataObjects, requiredIds);
        
            zqu.RulesEngineResponse response = re.run(zqu.RulesEngine.RuleType.ALL);
        
            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' ' + response);

            zqu.DataObject quoteDataObject = response.getMasterObject();

            List<zqu.DataObject> ratePlanDataObjects = quoteDataObject.getChildren('zqu__QuoteRatePlan__c');
            List<zqu.zChargeGroup> chargeGroupsForUpdate = new List<zqu.zChargeGroup>();

            for(zqu.DataObject rpDataObject : ratePlanDataObjects) {
                zqu.ZChargeGroupDataObject cgForUpdate = (zqu.ZChargeGroupDataObject) rpDataObject;
                chargeGroupsForUpdate.add( cgForUpdate.getChargeGroup() );
            }

            if (!chargeGroupsForUpdate.isEmpty()) {
                zqu.ZQuoteUtil.updateChargeGroups(chargeGroupsForUpdate);   
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()]; 

        System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' status: ' + a.Status);
        System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' job id: ' + a.Id);
        
        for (Id quoteId : this.quoteIds) {
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' quote id = ' + quoteId);
        }

        Database.executeBatch(new Z_RenewalQuoteRecalculateBatchable(this.quoteIds), 1);
    }
}