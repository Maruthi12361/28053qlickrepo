/**
* Change log:
*
*  2019-05-22  ext_vos  CHG0036145: add testCalculateRecentEndDate() and testBatch().
*  2019-07-11 ext-ciz CHG0036145: changed testCalculateRecentEndDate() and testBatch().
*
*****************************************************************/
@isTest
public class PseTimecardHeaderTriggerTest {

    static testmethod void testCalculateRecentEndDate() {
        User adminUser = QTTestUtils.createMockSystemAdministrator();
        adminUser.CurrencyIsoCode = 'USD';
        update adminUser;

        System.runAs(adminUser) {
            User projectmanagerUser1 = QTTestUtils.createMockSystemAdministrator();
            projectmanagerUser1.FirstName = 'PM User 1';
            update projectmanagerUser1;

            User projectmanagerUser2 = QTTestUtils.createMockSystemAdministrator();
            projectmanagerUser2.FirstName = 'PM User 2';
            update projectmanagerUser2;

            Account projectmanagerAccount1 = new Account();
            projectmanagerAccount1.Name = 'Project manager 1 account';

            Account projectmanagerAccount2 = new Account();
            projectmanagerAccount2.Name = 'Project manager 2 account';

            List<Account> accounts = new List<Account>();
            accounts.add(projectmanagerAccount1);
            accounts.add(projectmanagerAccount2);
            insert accounts;

            Contact projectManagerContact1 = new Contact();
            projectManagerContact1.accountId = projectmanagerAccount1.Id;
            projectManagerContact1.pse__External_Resource__c = true;
            projectManagerContact1.pse__Is_Resource__c = true;
            projectManagerContact1.pse__Is_Resource_Active__c = true;
            projectManagerContact1.pse__Salesforce_User__c = projectmanagerUser1.Id;
            projectManagerContact1.lastname = 'Project manager 1';
            projectManagerContact1.firstname = 'Test';

            Contact projectManagerContact2 = new Contact();
            projectManagerContact2.accountId = projectmanagerAccount2.Id;
            projectManagerContact2.pse__External_Resource__c = true;
            projectManagerContact2.pse__Is_Resource__c = true;
            projectManagerContact2.pse__Is_Resource_Active__c = true;
            projectManagerContact2.pse__Salesforce_User__c = projectmanagerUser2.Id;
            projectManagerContact2.lastname = 'Project manager 2';
            projectManagerContact2.firstname = 'Test';

            List<Contact> contacts = new List<Contact>();
            contacts.add(projectManagerContact1);
            contacts.add(projectManagerContact2);
            insert contacts;

            TestUtil.createBasicTestData();

            TestUtil.testProject.pse__Project_Manager__c = projectManagerContact1.Id;
            update TestUtil.testProject;

            pse__Timecard_Header__c timecard = new pse__Timecard_Header__c();
            timecard.pse__Project__c = TestUtil.testProject.Id;
            timecard.pse__Resource__c = TestUtil.testResource.Id;
            timecard.pse__Start_Date__c = Date.today().addDays(-16);
            timecard.pse__End_Date__c = Date.today().addDays(-10);
            timecard.pse__Assignment__c = TestUtil.testAssignment.Id;
            timecard.pse__Monday_Hours__c = 8;
            timecard.pse__Status__c = 'Approved';
            insert timecard;

            Map<Integer, Decimal> daysOfWeekforfirstTC = new Map<Integer, Decimal>();
            daysOfWeekforfirstTC.put(6, timecard.pse__Saturday_Hours__c);
            daysOfWeekforfirstTC.put(5, timecard.pse__Friday_Hours__c);
            daysOfWeekforfirstTC.put(4, timecard.pse__Thursday_Hours__c);
            daysOfWeekforfirstTC.put(3, timecard.pse__Wednesday_Hours__c);
            daysOfWeekforfirstTC.put(2, timecard.pse__Tuesday_Hours__c);
            daysOfWeekforfirstTC.put(1, timecard.pse__Monday_Hours__c);
            daysOfWeekforfirstTC.put(0, timecard.pse__Sunday_Hours__c);

            List<pse__Timecard_Header__c> cards = [
                    select Id, pse__End_Date__c, pse__Start_Date__c, pse__Total_Hours__c, pse__Sunday_Hours__c, pse__Saturday_Hours__c,
                            pse__Friday_Hours__c, pse__Thursday_Hours__c,pse__Wednesday_Hours__c, pse__Tuesday_Hours__c, pse__Monday_Hours__c
                    from pse__Timecard_Header__c
                    where pse__Project__c = :TestUtil.testProject.Id
            ];
            pse__Proj__c prTest = [select Id, Project_Ageing_Formula__c, Recent_Timecard_End_Date__c from pse__Proj__c where Id = :TestUtil.testProject.Id];

            System.assertEquals(cards.size(), 1);

            for (Integer i = 6; i >= 0; i--) {
                if (daysOfWeekforfirstTC.get(i) != null && daysOfWeekforfirstTC.get(i) > 0) {
                    System.assertEquals(timecard.pse__Start_Date__c.addDays(i), prTest.Recent_Timecard_End_Date__c);
                    break;
                }
            }

            Test.startTest();
            // create new timecard
            Semaphores.PseTimecardHeaderTriggerAfterInsert = false;
            pse__Timecard_Header__c newTimecard = new pse__Timecard_Header__c();
            newTimecard.pse__Project__c = TestUtil.testProject.Id;
            newTimecard.pse__Resource__c = TestUtil.testResource.Id;
            newTimecard.pse__Start_Date__c = Date.today().addDays(-11);
            newTimecard.pse__End_Date__c = Date.today().addDays(-5);
            newTimecard.pse__Assignment__c = TestUtil.testAssignment.Id;
            newTimecard.pse__Friday_Hours__c = 8;
            newTimecard.pse__Status__c = 'Approved';
            insert newTimecard;

            Map<Integer, Decimal> daysOfWeekforSecondTC = new Map<Integer, Decimal>();
            daysOfWeekforSecondTC.put(6, newTimecard.pse__Saturday_Hours__c);
            daysOfWeekforSecondTC.put(5, newTimecard.pse__Friday_Hours__c);
            daysOfWeekforSecondTC.put(4, newTimecard.pse__Thursday_Hours__c);
            daysOfWeekforSecondTC.put(3, newTimecard.pse__Wednesday_Hours__c);
            daysOfWeekforSecondTC.put(2, newTimecard.pse__Tuesday_Hours__c);
            daysOfWeekforSecondTC.put(1, newTimecard.pse__Monday_Hours__c);
            daysOfWeekforSecondTC.put(0, newTimecard.pse__Sunday_Hours__c);

            prTest = [select Id, Project_Ageing_Formula__c, Recent_Timecard_End_Date__c from pse__Proj__c where Id = :TestUtil.testProject.Id];

            for (Integer i = 6; i >= 0; i--) {
                if (daysOfWeekforSecondTC.get(i) != null && daysOfWeekforSecondTC.get(i) > 0) {
                    System.assertEquals(newTimecard.pse__Start_Date__c.addDays(i), prTest.Recent_Timecard_End_Date__c);
                    break;
                }
            }


            // create new timecard with earlier EndDate -> Project will not be updated
            Semaphores.PseTimecardHeaderTriggerAfterInsert = false;
            pse__Timecard_Header__c oldTimecard = new pse__Timecard_Header__c();
            oldTimecard.pse__Project__c = TestUtil.testProject.Id;
            oldTimecard.pse__Resource__c = TestUtil.testResource.Id;
            oldTimecard.pse__Start_Date__c = Date.today().addDays(-18);
            oldTimecard.pse__End_Date__c = Date.today().addDays(-12);
            oldTimecard.pse__Friday_Hours__c = 8;
            oldTimecard.pse__Assignment__c = TestUtil.testAssignment.Id;
            oldTimecard.pse__Status__c = 'Approved';
            insert oldTimecard;
            prTest = [select Id, Project_Ageing_Formula__c, Recent_Timecard_End_Date__c from pse__Proj__c where Id = :TestUtil.testProject.Id];
            System.assertNotEquals(oldTimecard.pse__End_Date__c, prTest.Recent_Timecard_End_Date__c);

            //delete timecard
            delete oldTimecard;

            prTest = [select Id, Project_Ageing_Formula__c, Recent_Timecard_End_Date__c from pse__Proj__c where Id = :TestUtil.testProject.Id];
            System.assertEquals(6, prTest.Project_Ageing_Formula__c);

            Test.stopTest();

            Database.executeBatch(new PseProjectAgeingUpdateBatchJob(), 50);
        }
    }

    static testMethod void testBatch() {
        User adminUser = QTTestUtils.createMockSystemAdministrator();
        adminUser.CurrencyIsoCode = 'USD';
        update adminUser;

        System.runAs(adminUser) {
            User projectmanagerUser1 = QTTestUtils.createMockSystemAdministrator();
            projectmanagerUser1.FirstName = 'PM User testBatch1';
            update projectmanagerUser1;

            Account projectmanagerAccount1 = new Account(Name = 'Project manager testBatch account');
            insert projectmanagerAccount1;

            Contact projectManagerContact1 = new Contact();
            projectManagerContact1.accountId = projectmanagerAccount1.Id;
            projectManagerContact1.pse__External_Resource__c = true;
            projectManagerContact1.pse__Is_Resource__c = true;
            projectManagerContact1.pse__Is_Resource_Active__c = true;
            projectManagerContact1.pse__Salesforce_User__c = projectmanagerUser1.Id;
            projectManagerContact1.lastname = 'Project manager testBatch';
            projectManagerContact1.firstname = 'TestBatch';
            insert projectManagerContact1;

            TestUtil.createBasicTestData();

            TestUtil.testProject.pse__Project_Manager__c = projectManagerContact1.Id;
            update TestUtil.testProject;

            pse__Timecard_Header__c timecard = new pse__Timecard_Header__c();
            timecard.pse__Project__c = TestUtil.testProject.Id;
            timecard.pse__Resource__c = TestUtil.testResource.Id;
            timecard.pse__Start_Date__c = Date.today().addDays(-16);
            timecard.pse__End_Date__c = Date.today().addDays(-10);
            timecard.pse__Assignment__c = TestUtil.testAssignment.Id;
            timecard.pse__Monday_Hours__c = 8;
            timecard.pse__Status__c = 'Approved';
            insert timecard;

            Map<Integer, Decimal> daysOfWeekforfirstTC = new Map<Integer, Decimal>();
            daysOfWeekforfirstTC.put(6, timecard.pse__Saturday_Hours__c);
            daysOfWeekforfirstTC.put(5, timecard.pse__Friday_Hours__c);
            daysOfWeekforfirstTC.put(4, timecard.pse__Thursday_Hours__c);
            daysOfWeekforfirstTC.put(3, timecard.pse__Wednesday_Hours__c);
            daysOfWeekforfirstTC.put(2, timecard.pse__Tuesday_Hours__c);
            daysOfWeekforfirstTC.put(1, timecard.pse__Monday_Hours__c);
            daysOfWeekforfirstTC.put(0, timecard.pse__Sunday_Hours__c);

            pse__Proj__c prTest = [select Id, Recent_Timecard_End_Date__c from pse__Proj__c where Id = :TestUtil.testProject.Id];
            for (Integer i = 6; i >= 0; i--) {
                if (daysOfWeekforfirstTC.get(i) != null && daysOfWeekforfirstTC.get(i) > 0) {
                    System.assertEquals(timecard.pse__Start_Date__c.addDays(i), prTest.Recent_Timecard_End_Date__c);
                    break;
                }
            }

            Test.startTest();
            TestUtil.testProject.Recent_Timecard_End_Date__c = null;
            update TestUtil.testProject;

            prTest = [select Id, Recent_Timecard_End_Date__c from pse__Proj__c where Id = :TestUtil.testProject.Id];
            System.assertEquals(null, prTest.Recent_Timecard_End_Date__c);

            Database.executeBatch(new PseProjectAgeingUpdateBatchJob());
            Test.stopTest();

            prTest = [select Id, Recent_Timecard_End_Date__c from pse__Proj__c where Id = :TestUtil.testProject.Id];
            for (Integer i = 6; i >= 0; i--) {
                if (daysOfWeekforfirstTC.get(i) != null && daysOfWeekforfirstTC.get(i) > 0) {
                    System.assertEquals(timecard.pse__Start_Date__c.addDays(i), prTest.Recent_Timecard_End_Date__c);
                    break;
                }
            }
        }
    }
}