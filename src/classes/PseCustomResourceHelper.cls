/********************************************************************************
Name            : PseCustomResourceTrigger
Author          : Jason Favors (Appirio)
Created Date    : September 8, 2010
Usage           : A trigger to handle updates/inserts on Contact records.  More 
                : specifically, checks for updates to the Account Super User flag
                : and handles Permission Control creation/deletion accordingly.
2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
*********************************************************************************/

public with sharing class PseCustomResourceHelper {

    public static void psePermissionControlOnInsert(List<Contact> triggernew){
    	// assemble list of resource records, related account IDs, and related user IDs
    Set<Id> accountIds = new Set<Id>();
    Set<Id> userIds = new Set<Id>();
    boolean atLeastOneResource = false;
    List<Contact> triggerResources = new List<Contact>();

    for (Contact contact : triggernew) {
        if (contact.pse__Is_Resource__c) {
            accountIds.add(contact.AccountId);
            userIds.add(contact.pse__Salesforce_User__c);
            triggerResources.add(contact);
            atLeastOneResource = true;
        }
    }

    // ignore if not updating/inserting non-resources 
    // (necessary to reduce # queries in other contact trigger tests)
    if (atLeastOneResource) {
    	Map<Id, Account> accounts = new Map<Id, Account>([SELECT Id, 
                (SELECT Id, AccountId, Account_Super_User__c, pse__Salesforce_User__c 
                    FROM Contacts WHERE pse__Is_Resource__c = true) 
                FROM Account WHERE Id in :accountIds]);

        // Prep list to store new PCs
        List<pse__Permission_Control__c> insertPcs = new List<pse__Permission_Control__c>();
        List<pse__Permission_Control__c> deletePcs = new List<pse__Permission_Control__c>();

        // Template PC with default settings
        pse__Permission_Control__c prototypePc = new pse__Permission_Control__c();
        prototypePc.pse__Skills_And_Certifications_Entry__c = true;
        prototypePc.pse__Skills_And_Certifications_View__c  = true;
        prototypePc.pse__Timecard_Entry__c                  = true;
        prototypePc.pse__Expense_Entry__c                   = true;
        System.debug('pseTrigger: New Resources');

            Account newResourceAccount;
            List<Contact> newResourceAccountResources;
            // ignore non-resources
            for (Contact newResource : triggerResources) {

                newResourceAccount = accounts.get(newResource.AccountId);
                if (newResourceAccount != null) {
                    newResourceAccountResources = newResourceAccount.Contacts;
                } else {
                    newResourceAccountResources = new List<Contact>();
                }
                for(Contact accountResource : newResourceAccountResources) {

                    // Add PCs for other Super Users of this Resource's account
                    if(accountResource.Account_Super_User__c && accountResource.Id != newResource.Id) {
                        //pse__Permission_Control__c pc = prototypePc;
                        pse__Permission_Control__c pc = new pse__Permission_Control__c();
                        pc.pse__Skills_And_Certifications_Entry__c = prototypePc.pse__Skills_And_Certifications_Entry__c;
                        pc.pse__Skills_And_Certifications_View__c = prototypePc.pse__Skills_And_Certifications_View__c;
                        pc.pse__Timecard_Entry__c = prototypePc.pse__Timecard_Entry__c;
                        pc.pse__Expense_Entry__c = prototypePc.pse__Expense_Entry__c;

                        pc.pse__User__c = accountResource.pse__Salesforce_User__c;
                        pc.pse__Resource__c = newResource.Id;

                        insertPcs.add(pc);
                    }

                    // Add PCs for each resource on the same account if Super User Flag set
                    if(newResource.Account_Super_User__c && accountResource.Id != newResource.Id) {
                        //pse__Permission_Control__c pc = prototypePc;
                        pse__Permission_Control__c pc = new pse__Permission_Control__c();
                        pc.pse__Skills_And_Certifications_Entry__c = prototypePc.pse__Skills_And_Certifications_Entry__c;
                        pc.pse__Skills_And_Certifications_View__c = prototypePc.pse__Skills_And_Certifications_View__c;
                        pc.pse__Timecard_Entry__c = prototypePc.pse__Timecard_Entry__c;
                        pc.pse__Expense_Entry__c = prototypePc.pse__Expense_Entry__c;

                        pc.pse__User__c = newResource.pse__Salesforce_User__c;
                        pc.pse__Resource__c = accountResource.Id;

                        insertPcs.add(pc);
                    }
                }
            }

            System.debug('pseTrigger: adding new PCs for new Resources: ' + insertPcs);
            // Bulk add new PCs
            insert insertPcs;
        }

    }

    public static void psePermissionControlOnUpdate(List<Contact> triggernew, Map<Id,Contact> triggeroldmap){
    	Set<Id> accountIds = new Set<Id>();
    	Set<Id> userIds = new Set<Id>();
    	boolean atLeastOneResource = false;
    	List<Contact> triggerResources = new List<Contact>();
    		for (Contact contact : triggernew) {
        		if (contact.pse__Is_Resource__c) {
            		accountIds.add(contact.AccountId);
            		userIds.add(contact.pse__Salesforce_User__c);
            		triggerResources.add(contact);
            		atLeastOneResource = true;
        		}
    		}

    		if (atLeastOneResource) {
    			System.debug('pseTrigger: Updated Resources');

    			Map<Id, Account> accounts = new Map<Id, Account>([SELECT Id, 
                (SELECT Id, AccountId, Account_Super_User__c, pse__Salesforce_User__c 
                    FROM Contacts WHERE pse__Is_Resource__c = true) 
                FROM Account WHERE Id in :accountIds]);

    			// Prep list to store new PCs
        	List<pse__Permission_Control__c> insertPcs = new List<pse__Permission_Control__c>();
        	List<pse__Permission_Control__c> deletePcs = new List<pse__Permission_Control__c>();

        	// Template PC with default settings
        pse__Permission_Control__c prototypePc = new pse__Permission_Control__c();
        prototypePc.pse__Skills_And_Certifications_Entry__c = true;
        prototypePc.pse__Skills_And_Certifications_View__c  = true;
        prototypePc.pse__Timecard_Entry__c                  = true;
        prototypePc.pse__Expense_Entry__c                   = true;

            // assemble mapping of trigger resource users with permission controls
            Map<Id, User> users = new Map<Id, User>([SELECT Id,
                    (SELECT Id, pse__Resource__r.AccountId FROM pse__Permission_Controls__r)
                    FROM User WHERE Id in :userIds]);

            Contact oldResource;
            for (Contact updatedResource : triggerResources) {

                oldResource = triggeroldmap.get(updatedResource.id);

                // Super User Flag changed; add/remove PCs accordingly
                if (oldResource.Account_Super_User__c != updatedResource.Account_Super_User__c) {
                    if (updatedResource.Account_Super_User__c) {
                        
                        System.debug('pseTrigger: updating super user.  account contacts: ' + accounts.get(updatedResource.AccountId).Contacts);
                        for (Contact accountResource : accounts.get(updatedResource.AccountId).Contacts) {
                            if(accountResource.Id != updatedResource.Id) {
                                // Add permission control for each resource on the same account
                                //pse__Permission_Control__c pc = prototypePc;
                                pse__Permission_Control__c pc = new pse__Permission_Control__c();
                                pc.pse__Skills_And_Certifications_Entry__c = prototypePc.pse__Skills_And_Certifications_Entry__c;
                                pc.pse__Skills_And_Certifications_View__c = prototypePc.pse__Skills_And_Certifications_View__c;
                                pc.pse__Timecard_Entry__c = prototypePc.pse__Timecard_Entry__c;
                                pc.pse__Expense_Entry__c = prototypePc.pse__Expense_Entry__c;

                                pc.pse__User__c = updatedResource.pse__Salesforce_User__c;
                                pc.pse__Resource__c = accountResource.Id;

                                System.debug('pseTrigger: adding insertPC: ' + pc);
                                insertPcs.add(pc);
                            }
                        }
                    } else {
                        // Delete all the current resources PCs where the PC resource has the same account
                        for (pse__Permission_Control__c pc : 
                                users.get(updatedResource.pse__Salesforce_User__c).pse__Permission_Controls__r) {
                            if (updatedResource.AccountId == pc.pse__Resource__r.AccountId) {
                                deletePcs.add(pc);
                            }
                        }
                    }
                }
            }

            System.debug('pseTrigger: adding new PCs for updated Resources: ' + insertPcs);
            // bulk add new PCs
            insert insertPcs;

            System.debug('pseTrigger: deleting old PCs for updated Resources: ' + deletePcs);
            // bulk delete existing PCs
            delete deletePcs;
    		}
    }

}