/*
@author: Anthony Victorio, Model Metrics
@date: 02/14/2012
@description: Controller for the QVM "product builder step 4" page

2015-09-29  |  BAD  |  CR# 57108 Changed Pageref in saveProduct() 
*/
public without sharing class QVM_ProductBuilderStep4 {
    
    public QVM_Product_Data__c product {get;set;}
    public Id pId {get;set;}
    
    public S3.AmazonS3 as3 { get; private set; }
    public S3.ListAllMyBucketsEntry[] allBucketList {get;set;}
    
    public String bucketOwnerId {get;set;}
    public String bucketName {get;set;}
    public String urlBase {get;set;}
    
    private String AWS_URL {get;set;}
    private String folder {get;set;}
    private String permission {get;set;}
    
    public QVM_ThumbnailGen thumbnailGen {get; private set;}
    
    public QVM_ProductBuilderStep4() {
        
        pId = ApexPages.currentPage().getParameters().get('id');
        
        product = [select Id, QVM_Partner__c, Product_Name__c, Overview__c, Short_Description__c, Type__c, Industry__c, Job_Function__c, Version__c, QlikView_Version__c, Referral_Link__c, Product_Download_Link__c, Image_1__c, Image_2__c, Image_3__c, Image_4__c, Image_5__c, Magento_Product_Id__c, Magento_Product_URL__c, Price__c, Weight__c, Product_Change__c, Edit_Wizard__c from QVM_Product_Data__c where Id = :pId];
        QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
        Datetime now = Datetime.now();
        bucketName = settings.S3_Bucket__c;
        AWS_URL = 'http://' + bucketName + '.s3.amazonaws.com/';
        if(Test.isRunningTest()) {
            bucketOwnerId = '123456';
        } else {
            as3 = new S3.AmazonS3(settings.S3_Key__c,settings.S3_Secret__c);
            S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));
            bucketOwnerId = allBuckets.Owner.Id;
        } String sdf;
        folder = 'draft/' + product.Id + '/images/';
        permission = 'public-read';
        urlBase = AWS_URL + folder;
        image1 = new fileWrapper();
        image2 = new fileWrapper();
        image3 = new fileWrapper();
        image4 = new fileWrapper();
        image5 = new fileWrapper();
        thumbnailGen = new QVM_ThumbnailGen(settings.S3_Bucket__c, 'draft', settings.Thumbnail_Width__c.intValue(), settings.Thumbnail_Height__c.intValue());
    }
    
    public class fileWrapper {
        public String fileName {get;set;}
        public Blob fileBody {get;set;}
        public Integer fileSize {get;set;}
        public String docBody() {
            return EncodingUtil.base64Encode(fileBody);
        }
    }
    
    public fileWrapper image1 {get;set;}
    public fileWrapper image2 {get;set;}
    public fileWrapper image3 {get;set;}
    public fileWrapper image4 {get;set;}
    public fileWrapper image5 {get;set;}
    public list<fileWrapper> images {get;set;}
    
    public void uploadImage1() {
        Datetime now = Datetime.now();
        String fileName = image1.fileName.toLowerCase();
        Boolean putObjResult = (Test.isRunningTest()) ? true : as3.PutObjectInline_ACL(bucketName,folder + fileName, null,image1.docBody(), image1.fileSize ,permission,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, bucketOwnerId);
        if(putObjResult) {
            thumbnailGen.generate(pId, fileName);
            product.Image_1__c = urlBase + fileName;
            update product;
            image1 = new fileWrapper();
        } else { throw new QVM.newException('An error has occured.'); }
    }
    public void uploadImage2() {
        Datetime now = Datetime.now();
        String fileName = image2.fileName.toLowerCase();
        Boolean putObjResult = (Test.isRunningTest()) ? true : as3.PutObjectInline_ACL(bucketName,folder + fileName, null,image2.docBody(), image2.fileSize ,permission,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, bucketOwnerId);
        if(putObjResult) {
            thumbnailGen.generate(pId, fileName);
            product.Image_2__c = urlBase + fileName;
            update product;
            image2 = new fileWrapper();
        } else { throw new QVM.newException('An error has occured.'); }
    }
    public void uploadImage3() {
        Datetime now = Datetime.now();
        String fileName = image3.fileName.toLowerCase();
        Boolean putObjResult = (Test.isRunningTest()) ? true : as3.PutObjectInline_ACL(bucketName,folder + fileName, null,image3.docBody(), image3.fileSize ,permission,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, bucketOwnerId);
        if(putObjResult) {
            thumbnailGen.generate(pId, fileName);
            product.Image_3__c = urlBase + fileName;
            update product;
            image3 = new fileWrapper();
        } else { throw new QVM.newException('An error has occured.'); }
    }
    public void uploadImage4() {
        Datetime now = Datetime.now();
        String fileName = image4.fileName.toLowerCase();
        Boolean putObjResult = (Test.isRunningTest()) ? true : as3.PutObjectInline_ACL(bucketName,folder + fileName, null,image4.docBody(), image4.fileSize ,permission,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, bucketOwnerId);
        if(putObjResult) {
            thumbnailGen.generate(pId, fileName);
            product.Image_4__c = urlBase + fileName;
            update product;
            image4 = new fileWrapper();
        } else { throw new QVM.newException('An error has occured.'); }
    }
    public void uploadImage5() {
        Datetime now = Datetime.now();
        String fileName = image5.fileName.toLowerCase();
        Boolean putObjResult = (Test.isRunningTest()) ? true : as3.PutObjectInline_ACL(bucketName,folder + fileName, null,image5.docBody(), image5.fileSize ,permission,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, bucketOwnerId);
        if(putObjResult) {
            thumbnailGen.generate(pId, fileName);
            product.Image_5__c = urlBase + fileName;
            update product;
            image5 = new fileWrapper();
        } else { throw new QVM.newException('An error has occured.'); }
    }
    public void removeImage1() {
        product.Image_1__c = null;
        update product;
    }
    public void removeImage2() {
        product.Image_2__c = null;
        update product;
    }
    public void removeImage3() {
        product.Image_3__c = null;
        update product;
    }
    public void removeImage4() {
        product.Image_4__c = null;
        update product;
    }
    public void removeImage5() {
        product.Image_5__c = null;
        update product;
    }
    public PageReference saveProduct() {
        try{
            product.Edit_Wizard__c = false;
            update product;
            PageReference next;

            next = Page.QVM_ProductBuilderStep4;
            return next;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }
    public PageReference backPage() {
        try{
            update product;
            PageReference back;
            back = Page.QVM_ProductBuilderStep3;
            back.getParameters().put('Id', product.Id);
            return back;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }
    public PageReference nextPage() {
        try{
            update product;
            PageReference next;
            next = Page.QVM_ProductPreview;
            next.getParameters().put('Id', product.Id);
            return next;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }
}