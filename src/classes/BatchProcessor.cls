/********
* NAME: BatchProcessor
* Description: Class for mass DML operations
*
*Change Log:
    extcqb  2020-02-28  IT-2488 Mass update of Cases
******/
public class BatchProcessor implements Database.batchable<SObject> {
    SObject[] source;

    public BatchProcessor(SObject[] source) {
        this.source = source;
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        BatchSObjectFeeder bf = new BatchSObjectFeeder(source);
        return bf;
    }


    public void execute(Database.BatchableContext bc, SObject[] scope) {
        Semaphores.SetAllSemaphoresToTrue();
        upsert source;
    }

    public void finish(Database.BatchableContext bc) {

    }
}