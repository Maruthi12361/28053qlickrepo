/********************************************************
* CLASS: ExtendProductTrialControllerTest
* DESCRIPTION: Test class for ExtendProductTrialController
*
* CHANGELOG:    
*   2019-05-07 - BAD - Added Initial logic
*********************************************************/
@isTest
public class ExtendProductTrialControllerTest {

    @isTest
    static void test_extendTrial(){
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        QTTestUtils.GlobalSetUp();        
        Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());

        System.RunAs(mockSysAdmin)
        {
            Product_Trial__c trial = new Product_Trial__c();
            trial.Trial_Status__c = 'Started';
            trial.Start_Date__c = Date.today();
            trial.End_Date__c = Date.today().addDays(15);
            trial.Product__c = 'Qlik Sense Business';
            trial.Trial_License_Key__c = 'key123456';
            trial.Of_Trial_Extensions__c = 3;
            
            insert trial;

            ApexPages.StandardController stc = new ApexPages.StandardController(trial);
            ExtendProductTrialController controller = new ExtendProductTrialController(stc);
            
            Test.startTest();
            controller.ExtendTrial();
            trial = [select Trial_Status__c from Product_Trial__c where Id = :trial.Id];        
            system.assertEquals('Pending Extension', trial.Trial_Status__c);
            Test.stopTest();
        }
    }
   
}