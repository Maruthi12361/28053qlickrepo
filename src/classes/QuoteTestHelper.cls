/**
*  22.03.2017 : Rodion Vakulovskyi : changes for Validation failures
*  24.03.2017 : Rodion Vakulovskyi : changes for Validation failures
*  28.03.2017 : Ramakrishna Kini : Added new custom settings creation
*  6.04.2017 : Rodion Vakulovskyi : Added new custom settings creation QCW-1976
*  20.04.2017 : MTM fix List null errors QCW-2118
*  25.08.2017 : Aslam Kamal : Added method for inserting Partner category Status QCW -2934
*  09.03.2017 : Bala : included ORDER BY in fetching record type query
*  10.09.2017 : Anjuna: Added subscription fields to newQuote method
*  02.06.2018 : Reshma Code udjustment for test failure QCW-4780
*  27.03.2018 : AIN Added some new values for custom settings
*  06.07.2018 : ext_vos Added some new values for custom settings
*  11.07.2018 : ext_bad Added creation of DL_Training_Notification_Emails__c custom settings
*  29.10.2018 : Linus Löfberg BSL-1085 - Optimize PCS code on Quoting and add test coverage
*  13.02.2019 : Andreas Nilsson fixed test class error related to new duplicate rules on account and contact
*  06.03.2019 : Andreas Nilsson fixed low test coverage by adding custom setting for QVM.
*  29.05.2019 : extbad IT-1864 Update survey link
*  29.05.2019 : extbad IT-1897 Add 'Close Chat Survey Id' for QTCustomSettingsHier
*  26.11.2019 : feh updated to remove RecordType queries
*  23.12.2019 : extbad IT-2363 Add Signature Process name for Entitlement_Settings__c
*  17.02.2020 : extbad IT-2497 Add Legacy Enterprise Process name for Entitlement_Settings__c
*  01.04.2020 : extcqb IT-2492 Academic Program: Validate ULC quantity assigned to Contact
**/

public class QuoteTestHelper {
    static final String OppRecordTypeId = '012D0000000KEKOIA4';
    public static integer NumCreated = 0;
    private static boolean customSettingSetup = false;


    public static Subsidiary__c createSubsidiary() {
        Subsidiary__c subs = new Subsidiary__c(
        Legal_Entity_Name__c = 'test',
        Netsuite_Id__c = 'test',
        Legal_Country__c = 'France'
        );
        return subs;
    }

    public static QlikTech_Company__c createQlickTechCompany(String subsId) {
        QlikTech_Company__c qTComp = new QlikTech_Company__c(
        Name = 'GBR',
        Country_Name__c = 'France',
        CurrencyIsoCode = 'GBP',
        Language__c = 'English',
        Subsidiary__c = subsId,
        QlikTech_Company_Name__c = 'QlikTech UK Ltd'
        );
        return qTComp;
    }

    public static Contact createContact(String accountId) {
        NumCreated++;
        Contact testContact = new Contact (
        AccountId = accountId,
        OwnerId = UserInfo.getUserId(),
        LastName = 'Wilson-Hemingway' + '_' +System.now().millisecond() + '_' + NumCreated,
        FirstName = 'Daryl' + '_' +System.now().millisecond() + '_' + NumCreated,
        Email = 'rodion.vakulovskyi' + '_' +System.now().millisecond() + '_' + NumCreated + '@gmail.com'
        );
        return testContact;
    }

    public static Account createAccount(QlikTech_Company__c qtComp, RecordType rType, String description) {
        NumCreated++;
        Account testPartnerAcc = new Account(
        OwnerId = UserInfo.getUserId(),
        Name = 'PartnerAcc' + '_' +System.now().millisecond() + '_' + NumCreated,
        Sub_Reseller__c = false,
        RecordTypeId = rType.id,
        QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
        Legal_Approval_Notes__c = 'Legal Approval Auto Approved',
        Billing_Country_Code__c = qtComp.Id,
        Payment_Terms__c = '30 days'
        );
        if(rType.DeveloperName == 'Partner_Account') {
            testPartnerAcc.Navision_Status__c = 'Partner';
        }

        return testPartnerAcc;
    }

    public static Address__c createAddress(String accId, String contactId, String typeOfAddress) {
        Address__c testAddress = new Address__c(
        Account__c = accId,
        Address_1__c = '86, RUE MONTORGUEIL',
        Address_Contact__c = contactId,
        Address_Type__c = typeOfAddress,
        City__c = 'PARIS',
        Country__c = 'France',
        Default_Address__c = true,
        Legal_Approval_Status__c = 'Auto Approved',
        State_Province__c = 'Paris',
        Valid_Address__c = false,
        Zip__c = '75002'
        );
        return testAddress;
    }

    public static Opportunity createOpportunity(Account simpleAccount, String sellTroughPartnerAccount, RecordType rType) {
        Opportunity testOppPartner = new Opportunity(
        RecordTypeId = rType.id,
        Name = 'TestOppPartner',
        CurrencyIsoCode = 'USD',
        Short_Description__c = 'Partner Reseller',
        StageName = 'Goal Identified',
        CloseDate = Date.today().addDays(7),
        Type = 'New Customer',
        AccountId = simpleAccount.Id,
        ForecastCategoryName = 'Omitted'
        );
        if(!String.isBlank(sellTroughPartnerAccount)) {
            testOppPartner.Sell_Through_Partner__c = sellTroughPartnerAccount;
        }
        return testOppPartner;
    }

    public static  SBQQ__Quote__c createQuote(RecordType rType, String inputContact, Account simpleAccount, String partnerAccount, String revenueType, String status, String typeOfQuote, Boolean primary, String oppId) {
        return createQuote(rType.Id, inputContact, simpleAccount, partnerAccount, revenueType, status, typeOfQuote, primary, oppId);
    }

    public static  SBQQ__Quote__c createQuote(Id recordTypeId, String inputContact, Account simpleAccount, String partnerAccount, String revenueType, String status, String typeOfQuote, Boolean primary, String oppId) {
        SBQQ__Quote__c quotePartner = new SBQQ__Quote__c(
                SBQQ__Account__c = simpleAccount.id,
                SBQQ__Status__c = status,//'Open',
                SBQQ__Type__c = typeOfQuote,//'Quote',
                Revenue_Type__c = revenueType,//'Direct',
                RecordTypeId = recordTypeId,
                Short_Description__c = 'test',
                Partner_Special_Term_Request__c = false,
                Legal_Approval_Triggered__c = true,
                TypeUnmanaged__c = 'Existing Customer',
                CurrencyIsoCode  = 'USD',
                SBQQ__BillingCountry__c = 'France',
                SBQQ__PrimaryContact__c = inputContact,
                SBQQ__Primary__c = primary,
                SBQQ__StartDate__c = date.today()
        );
        if(!String.isEmpty(partnerAccount)) {
            quotePartner.Sell_Through_Partner__c = partnerAccount;
            quotePartner.Partner_Contact__c =inputContact;
        }
        if(!String.isBlank(oppId)) {
            quotePartner.SBQQ__Opportunity2__c = oppId;
        }
        if(!String.isEmpty(inputContact) && (quotePartner.Revenue_Type__c == 'Direct' || quotePartner.Revenue_Type__c.contains('OEM'))) {
            quotePartner.Quote_Recipient__c = inputContact;
        }

        if(String.isNotBlank(revenueType) && revenueType=='OEM' || revenueType=='MSP') {
            quotePartner.No_of_Subscription_Units__c = String.Valueof(1);
            quotePartner.Subscription_Start_Date_Editable__c = Date.Today();
            //quotePartner.Subscription_Unit__c = 'Months';
        }
        return quotePartner;
    }

    public static void createCustomSettings() {

        Steelbrick_Settings__c cs_Steelbrick = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails');
        if(cs_Steelbrick == null) { //Checking Custom Settings creation not called more than once

        Steelbrick_Settings__c cs = new Steelbrick_Settings__c();

        cs.Name = 'SteelbrickSettingsDetails';
        cs.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        cs.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW,012D0000000KIknIAG,012D0000000KIkxIAG';
        cs.SBPricebookId__c = '01s20000000E0PWAA0';
        cs.ExcludedCountries__c = 'Algeria,Armenia,Azerbaijan,Bahrain,Cambodia,China,Comoros,Croatia,Djibouti,Egypt,Georgia,India,Jordan,Kazakhstan,Kuwait,Kyrgyzstan,Laos,Macau,Mauritania,Moldova,Mongolia,Morocco,New Caledonia,Oman,Qatar,Saudi Arabia,Sri Lanka,Tajikistan,Tunisia,Turkmenist';
        cs.Account__c = '00N8E000000dnlw';
        cs.Billing_Address__c = '00N8E000000mTaO';
        cs.Invoice_Address__c = '00N8E000000mWVb';
        cs.Opportunity__c = '00N8E000000dnmO';
        cs.Partner_Contact__c = '00N8E000000kT5v';
        cs.Revenue_Type__c = '00N8E000000kKRT';
        cs.Sell_Through_Partner__c = '00N8E000000kT5b';
        cs.Shipping_Address__c = '00N8E000000mWV7';
        cs.Type__c = '00N8E000000dnmm';
        cs.BoomiToken__c = 'BASIC c2FsZXNmb3JjZVFBQHFsaWt0ZWNoLVRTQjZSQzplMzY4OWNhOS1jZjgwLTQ4YTUtOTQ4ZC0xZGY3YWNmNTYwZmY=';
        cs.Short_Description__c = '00N8E000000mQOo';
        cs.BoomiBaseURL__c = 'https://devboomi.qliktech.com:9093/ws/simple/';
        cs.Standard_PriceBook__c = '01s20000000E0PW';
        cs.Sector__c='01s20000000E0PW';
        cs.Industry__c='01s20000000E0PW';
        cs.Primary__c = '00N26000000oqYO';
        cs.ECustomsManualApproval__c = 'Iraq,South Sudan,Sierra Leone,Ivory Coast,Lebanon,Russia,Ukraine,Belarus,Pakistan,Zimbabwe,Cuba,North Sudan,North Korea,Syria,Iran';
        cs.YellowCountry__c = 'Iraq,South Sudan,Russia,Ukraine,Cote D’Ivoire (Ivory Coast),Sierra Leone,Lebanon,Belarus,Pakistan,Zimbabwe';
        cs.RedCountry__c = 'Iran,Syria,North Korea,North Sudan,Cuba,The region of Crimea (not a country) (capital is Sevastopol)';
        cs.SpringAccountPrefix__c = '8154';
        cs.CMInstanceUrl__c = 'uatna11';
        cs.Sys_Admin_ProfileId__c = '00e20000000yyUz';
        cs.Ops_Admin_Profile_Id__c = '00e20000001ODnHAAW';
        cs.NewVisualComplianceEnabled__c = false;
        insert cs;

        NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
        settings.UserRecordTypeIdsToExclude__c = '005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
        insert settings;

        QTCustomSettings__c settings2 = new QTCustomSettings__c();
        settings2.Name = 'Default';
        settings2.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        settings2.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        settings2.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        settings2.Opp_Default_License_Amount_For_Direct__c = 25000;
        settings2.Opp_Default_License_Amount_For_Reseller__c = 8000;
        settings2.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        settings2.PRM_Referral_TandC_Version_Num__c = 1.00;
        settings2.QlikNoReplyEmailAddress__c = 'noreply@qlik.com';
        settings2.BoomiBaseURL__c = 'https://testboomi.qliktech.com:9093/ws/simple/';
        settings2.BoomiToken__c = 'BASIC c2FsZXNmb3JjZXRlc3RAcWxpa3RlY2gtVFNCNlJDOmUyNDRhYzg4LTExNTctNDJmMC04NzAyLWRhNjlhNzQzYjQ4Ng==';
        settings2.BAMEventTypes__c = '\'EV - Presales Activity\'';
        settings2.Users_can_edit_locked_Opps__c='005D0000002SU5X,005D0000001rW2q,00520000000z3Of,005D0000002TkRy';
        settings2.Users_can_edit_locked_Opps2__c='005D0000002SBno,005D0000002SOYI,00520000000z3Oi,005D0000004Bsiq';
        settings2.OLIAppNamesToCreateVouchers__c = 'GTC,ELEARNING,ELEARNING1,ELEARNING2,ELEARNING3,ELEARNING4,ELEARNING5,ELEARNING6,ELEARNING7';
        settings2.VoucherDeactivateReactivateEmail__c = 'ain@qlik.com';

        insert settings2;

        Academic_Program_Settings__c settings3 = new Academic_Program_Settings__c();
        settings3.Name = 'AcademicProgramSetting';
        settings3.OpportunityRecordTypeId__c = '012D0000000KB2N';
        settings3.AccountRecordTypeId__c = '012D0000000KB2M';
        settings3.QlikTech_Company_field_Id__c = '00N20000001bqcS';
        settings3.LeadRecordTypeId__c = '012D0000000KBYn';
        insert settings3;

        //UIN
        Entitlement_Settings__c settings4 = new Entitlement_Settings__c();
        settings4.X24x7_Business_Hours_Name__c = '24x7';
        settings4.Premium_Entitlement_Process_Name__c = 'Legacy Enterprise Support SLA';
        settings4.Premium_Support_Level__c = 'Enterprise';
        settings4.Standard_Entitlement_Process_Name__c = 'Basic Support SLA';
        settings4.Standard_Support_Level__c = 'Basic';
        settings4.Hold_Account_Name__c = 'UnMatched License Hold Account';
        settings4.Email_Message_Phrase_Communication__c = 'CUSTOMER COMMUNICATION';
        settings4.Email_Message_Phrase_First_Response__c = 'FIRST RESPONSE';
        settings4.Signature_Entitlement_Process_Name__c = 'Signature Support SLA';
        settings4.Enterprise_Entitlement_Process_Name__c = 'Enterprise Support SLA';
        insert settings4;

        HardcodedValuesQ2CW__c hSettings = new HardcodedValuesQ2CW__c();
        hSettings.Partner_Revenue_Types__c = 'Reseller, Fulfillment, Distributor, Partner Purchase, Reseller QSG, Reseller Services-Deferred';
        hSettings.Ops_Admin_Profile_Id__c = '00e20000001ODnH,00eD0000001P4DC';
        hSettings.Sys_Admin_ProfileId__c = '00e20000000yyUz,00e26000000HwcW';
        hSettings.Profile_Custom_API__c = '00e20000000zFfp';
        hSettings.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert hSettings;

        //QCW-4780
        Q2CWSettings__c hSettings1 = new Q2CWSettings__c();
        hSettings1.Max_Units_Consumed_Limit__c = 7000;
        hSettings1.Max_Units_Hit_Error_Messag__c = 'aaaaa';
        hSettings1.SalesQCCSZuoraSubscription_OppRecordType__c = '0122600000010lP';
        insert hSettings1;

        QTCustomSettingsHier__c qtCSHier = new QTCustomSettingsHier__c();
        qtCSHier.SpringCM_Account_Id__c = '8154';
        qtCSHier.SpringCM_Environment__c = 'uatna11';
        qtCSHier.Survey_Link__c = 'https://engage.clicktools.com/v2/lgfrfzk36wli';
        qtCSHier.Survey_Link_QA__c = 'https://engage.clicktools.com/v2/1y6hz2wpa479x';
        qtCSHier.Close_Chat_Survey_Id__c = 'mimg1b725kel';
        qtCSHier.Close_Chat_Survey_Id_QA__c = '3hftxnypuf8j1';
        insert qtCSHier;


        DL_Training_Notification_Emails__c dlEmails = new DL_Training_Notification_Emails__c();
        dlEmails.ITALY__c = 'DL-{Sandbox}TrainingNotificationITALY@qlikview.com';
        dlEmails.SPI__c = 'DL-{Sandbox}TrainingNotificationIBERIA@qlikview.com';
        dlEmails.AF__c = 'DL-{Sandbox}TrainingNotificationAF@qlikview.com';
        dlEmails.No_Region__c = '{Sandbox}No_QT_Region_In_Qliktech_Company@qlikview.com';
        insert dlEmails;

        QVM_Settings__c qvmSettings = new QVM_Settings__c();
        qvmSettings.Pagination_After_Row__c = 25;
    	qvmSettings.QlikMarket_Website__c = 'http://qlikmarket-dev.qlikview.com/';
        qvmSettings.Max_CP_Rows__c = 5;

        qvmSettings.Default_Lead_Record_Type__c = 'Partner Opp Reg';
		qvmSettings.Default_Lead_Source__c = 'PART - Partner';
		qvmSettings.Default_Lead_Status__c = 'Raw - Prospect';

        qvmSettings.htaccess_Required__c = true;
        qvmSettings.htaccess_username__c = 'qlikview';
        qvmSettings.htaccess_password__c = 'qlikview100';

        qvmSettings.Magento_Endpoint__c = 'qlikmarket-dev.qlikview.com/index.php/api/inchoo/rest';
        qvmSettings.Magento_API_Key__c = 'qlikKEY1';
        qvmSettings.Magento_Username__c = 'qlikapi';

        qvmSettings.S3_Endpoint__c = 'http://s3.amazonaws.com/doc/2006-03-01/';
        qvmSettings.S3_Bucket__c = 'qlikview-market-dev';
        qvmSettings.S3_Key__c = 'AKIAJZB45BFTFO4AMS5Q';
        qvmSettings.S3_Secret__c = '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';

        qvmSettings.SQS_Endpoint__c = 'sqs.us-east-1.amazonaws.com';
        qvmSettings.SQS_Thumbnail_Queue__c = 'QVMThumbnail_SBX';
        qvmSettings.Thumbnail_Height__c = 77;
        qvmSettings.Thumbnail_Width__c = 77;

		qvmSettings.Default_Case_Origin__c = 'QlikMarket';
		qvmSettings.Default_Case_Priority__c = 'Medium / Low';
		qvmSettings.Default_Case_Record_Type__c = 'QlikMarket Case Record Type';
		qvmSettings.Default_Case_Status__c = 'New';

        qvmSettings.MagentoCategoryMap_Function_1__c = '17,Executive,18,Finance & Accounting,19,General Administration,20,Human Resources,21,IT,22,Logistics & Supply Chain,23,Marketing,24,Operations / Production,25,Partner / Alliances';
        qvmSettings.MagentoCategoryMap_Function_2__c = '26,Product Management,27,Purchasing & Procurement,28,Quality & Change Management,29,Research & Development,30,Sales,31,Service & Support,32,Web,39,Non-Job Function Specific';
        qvmSettings.MagentoCategoryMap_Industry__c = '50,Communications,48,Energy & Utilities,13,Financial Services,12,Healthcare,49,Life Sciences,6,Manufacturing & High Tech,15,Public Sector,10,Retail & Services,40,Non-Industry Specific';
        qvmSettings.MagentoCategoryMap_QVVersion__c = '41,9.0,37,10.0,38,11.0,46,11.0 SR1,47,11.0 SR2,51,11.2,45,OEM,53,Qlik Sense 2.1,54,Qlik Sense 2.2,64,Qlik Sense 3,65,QlikView 12,66,Qlik Sense Sep 2017,67,Qlik Sense Nov 2017,68,Qlik Sense Feb 2018,69,Qlik Sense Apr 2018';
        qvmSettings.MagentoCategoryMap_QVVersion_2__c = '70,Qlik Sense Jun 2018';
        qvmSettings.MagentoCategoryMap_QVVersion_3__c = '71,QlikView 12.20';
        qvmSettings.MagentoCategoryMap_Type__c = '9,Application,8,Connector,7,Extension';

        insert qvmSettings;
        customSettingSetup = true;
        }
    }

    public static SBQQ__QuoteLine__c createQuoteLine(Id quoteId, Id productId, String serverSiteSetup, Integer quantity, String licenseCode, String groupText, String family, String currencyIsoCode, Integer price, Integer percent, String licenseId, String lefDetail) {
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c(
        SBQQ__Quantity__c = quantity,
        License_Code__c = licenseCode,
        SBQQ__Quote__c = quoteId,
        SBQQ__Product__c = productId,
        Product_Group_Text__c = groupText,
        Family__c = family,
        CurrencyIsoCode = currencyIsoCode,
        SBQQ__CustomerPrice__c = price,
        SBQQ__DefaultSubscriptionTerm__c = 12,
        SBQQ__ListPrice__c = price,
        SBQQ__StartDate__c = Date.today(),
        SBQQ__NetPrice__c = price,
        SBQQ__Number__c = 4,
        SBQQ__PartnerPrice__c = price,
        SBQQ__PricingMethod__c = 'List',
        SBQQ__SubscriptionPercent__c = percent,
        SBQQ__SubscriptionPricing__c = 'Percent Of Total',
        SBQQ__SubscriptionScope__c = 'Quote',
        Disable_AJAX__c = false,
        LicenseProductID__c = licenseId,
        LEFDetailID__c = lefDetail
        );
        return quoteline;
    }

    public static Product2 createProduct(String name, String family, Integer percent, String productGroup, String licenseCode){
        /*
        Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
        insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
        insert testQtCompany;

        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        */
        Product2 product = new Product2();
        product.Name=name;
        product.Accounts_Specific__c=FALSE;
        product.All_Countries_Visibility__c=FALSE;
        product.Application_Name__c='QVSERVER';
        product.CanUseRevenueSchedule=TRUE;
        product.Contest_Promo_Code__c=FALSE;
        product.CPQ_Product__c=TRUE;
        product.CurrencyIsoCode='USD';
        product.Deferred_Revenue_Account__c='26010 Deferred Revenue : Deferred revenue - license';
        product.Family=family;
        product.Generate_License__c=TRUE;
        product.Income_Account__c='40000 License revenue';
        product.IsActive=TRUE;
        product.LEF_Format_Text__c='PRODUCTLEVEL:10';
        product.License_Code__c=licenseCode;//'QVSERVER';
        product.Product_Group__c= productGroup;//'QlikView';
        product.Product_Level__c=10;
        product.ProductCode='4750';
        product.pse__IsServicesProduct__c=FALSE;
        product.SBQQ__AssetConversion__c='One per unit';
        product.SBQQ__BlockPricingField__c='Quantity';
        product.SBQQ__Component__c=FALSE;
        product.SBQQ__ConfigurationEvent__c='Always';
        product.SBQQ__ConfigurationType__c='Required';
        product.SBQQ__CostEditable__c=FALSE;
        product.SBQQ__CustomConfigurationRequired__c=FALSE;
        product.SBQQ__DefaultQuantity__c=1;
        product.SBQQ__DescriptionLocked__c=FALSE;
        product.SBQQ__ExcludeFromMaintenance__c=FALSE;
        product.SBQQ__ExcludeFromOpportunity__c=FALSE;
        product.SBQQ__ExternallyConfigurable__c=FALSE;
        product.SBQQ__HasConfigurationAttributes__c=TRUE;
        product.SBQQ__Hidden__c=FALSE;
        product.SBQQ__HidePriceInSearchResults__c=FALSE;
        product.SBQQ__IncludeInMaintenance__c=TRUE;
        product.SBQQ__NewQuoteGroup__c=FALSE;
        product.SBQQ__NonDiscountable__c=FALSE;
        product.SBQQ__NonPartnerDiscountable__c=FALSE;
        product.SBQQ__Optional__c=FALSE;
        product.SBQQ__OptionSelectionMethod__c='Click';
        product.SBQQ__PriceEditable__c=FALSE;
        product.SBQQ__PricingMethod__c='List';
        product.SBQQ__PricingMethodEditable__c=FALSE;
        product.SBQQ__QuantityEditable__c=TRUE;
        product.SBQQ__QuantityScale__c=0;
        product.SBQQ__ReconfigurationDisabled__c=FALSE;
        product.SBQQ__SubscriptionBase__c='List';
        product.SBQQ__SubscriptionType__c = 'Renewable';
        product.SBQQ__SubscriptionPercent__c = percent;
        product.SBQQ__Taxable__c=FALSE;
        product.Team_Discount__c=FALSE;
        product.Use_Price_Level__c=FALSE;
        product.WaitListCount__c=0;
        return product;
    }

    public static OpportunityLineItem createLineItem(String oppId, PricebookEntry pbEntry) {
        OpportunityLineItem oppLineToInsert = new OpportunityLineItem(
        OpportunityId = oppId,
        PricebookEntryId = pbEntry.id,
        Quantity = 5,
        TotalPrice = 5 * pbEntry.UnitPrice
        );
        return oppLineToInsert;
    }

    public static PricebookEntry createPriceBookEntry(String prodId, String priceBookId) {
        PricebookEntry pbEntry = new PricebookEntry(
        Pricebook2Id = priceBookId,
        Product2Id = prodId,
        UnitPrice = 100.00,
        IsActive = true
        );
        return pbEntry;
    }

    public static Competitor_Information__c createCompInfo(String oppId) {
        Competitor_Information__c testCompInfo = new Competitor_Information__c(
        Competitive_Status__c = '2 - Even with Competition',
        Primary_Competitor__c = 'Amazon QuickSight',
        Date_to_revisit__c = date.today(),
        Detail__c = 'detail',
        Current_System__c = 'Actuate - Actuate',
        Opportunity__c = oppId,
        Other__c = 'other'
        );
        return testCompInfo;
    }

    public static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
        Country_Code_Two_Letter__c = countryAbbr,
        Country_Name__c = countryName,
        Subsidiary__c = subsId);
        return qlikTechIns;
    }





    private static Map<String, Map<String, RecordType>> recordTypeMapByDevName = new Map<String, Map<String, RecordType>>();
    private static Map<String, Schema.SObjectType> gd;

    private static void loadRecordTypes(String sObjectName) {
        if(gd == null) {
            gd = Schema.getGlobalDescribe();
        }

        Map<String, Schema.RecordTypeInfo> recordTypeInfo = gd.get(sObjectName).getDescribe().getRecordTypeInfosByDeveloperName();
        Map<String, RecordType> recordTypes = new Map<String, RecordType>();
        for(String recordTypeDevName: recordTypeInfo.keySet()) {
            recordTypes.put(recordTypeDevName, new RecordType(Id = recordTypeInfo.get(recordTypeDevName).getRecordTypeId(), DeveloperName = recordTypeDevName));
        }
        recordTypeMapByDevName.put(sObjectName, recordTypes);
    }

    public static RecordType getRecordTypeByDevName(String sObjectName, String developerName) {
        if(recordTypeMapByDevName.get(sObjectName) == null) {
            loadRecordTypes(sObjectName);
        }

        return recordTypeMapByDevName.get(sObjectName).get(developerName);
    }


    public static RecordType getRecordTypebyDevName(String developerName) {
        return [Select id, DeveloperName From RecordType where DeveloperName =: developerName ORDER BY createddate limit 1];// fix for query error
    }

    /******* changes for qcw-2934 start *********/
    public static List<Partner_Category_Status__c> createPCS(Account accPartner){
        Partner_Category_Status__c pcs1 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
        Program_Version__c = 'Distributor',
        Partner_Level__c = 'Distributor',
        Partner_Category__c = 'Distributor',
        Partner_Level_Status__c='Active');

        Partner_Category_Status__c pcs2 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
        Program_Version__c = 'QPP',
        Partner_Level__c = 'Select',
        Partner_Category__c = 'Resell',
        Partner_Level_Status__c='Active');

        Recordtype r=[SELECT id FROM recordtype where name='OEM Partner Account'
        and sobjecttype='Partner_Category_Status__c'];

        Partner_Category_Status__c pcs3 = new Partner_Category_Status__c(Partner_Account__c = accPartner.Id,
        Program_Version__c = 'OEM',
        recordtypeid=r.id,
        Partner_Level_Status__c='Active',
        Partner_Category__c='OEM',
        Partner_Level__c='OEM');


        return new List<Partner_Category_Status__c>{pcs1,pcs2,pcs3};
    }
    /******* changes for qcw-2934 end *********/

    public static Partner_Category_Status__c createSpecificPCS(Id partnerAccountId, Id pcsRecordTypeId, String programVersion, String partnerLevel, String partnerCategory, String pcsStatus) {
        Partner_Category_Status__c pcs = new Partner_Category_Status__c(
        Partner_Account__c = partnerAccountId,
        Program_Version__c = programVersion,
        Partner_Level__c = partnerLevel,
        Partner_Category__c = partnerCategory,
        Partner_Level_Status__c = pcsStatus,
        RecordTypeId = pcsRecordTypeId);
        return pcs;
    }
}