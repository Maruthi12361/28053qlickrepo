/********************************************************
* CLASS: ExtendProductTrialController
* DESCRIPTION: Controller for Product Trial. 
*  Controller sets the field "Trial Status" to "Pending Extension"
*
* CHANGELOG:    
*   2019-05-07 - BAD - Added Initial logic
*   2019-06-20 - BAD - BMW-1587 - added extensionDays to WS call
*	2019-12-11 - CRW - BMW-1812 - Trial to be extended on Expired status as well and stop calculating end date.
*********************************************************/
public class ExtendProductTrialController {
    private static final String WSENDPOINT = 'executeSFTrialExtension';
    Id TrialId;
    
    public ExtendProductTrialController(ApexPages.StandardController controller){
        TrialId= (Id)controller.getRecord().Id;
    }
    
    public pageReference ExtendTrial(){
        String emailRecipient = '';
        Decimal noOfExtentions = 0;
        Datetime endDate = null;
        String trialKey = '';
        System.debug('Start Extend Trial');
        Integer extensionDays = 15;

        //Retrieve Trial record
        Product_Trial__c trial = [Select Id, Name, Trial_License_Key__c, Trial_Status__c, End_Date__c, Of_Trial_Extensions__c,  Contact_Account__r.Owner.Approval_Manager__r.Email, Contact__c, Lead__r.OwnerId From Product_Trial__c Where Id = :TrialId];
        if(trial.Trial_Status__c == 'Started' || trial.Trial_Status__c == 'Extended' || trial.Trial_Status__c == 'Expired')
        {
            extensionDays = String.isNotBlank(Label.Product_Trial_Extension_Days) ? integer.ValueOf(Label.Product_Trial_Extension_Days) : 15;
            trial.Trial_Status__c = 'Pending Extension';
            if(trial.Trial_License_Key__c != null) {trialKey = trial.Trial_License_Key__c;}
            //if(trial.End_Date__c != null) {endDate = trial.End_Date__c + extensionDays;}
            endDate = trial.End_Date__c;
            System.debug('Trial: ' + trial);

            //Send message to Boomi Web Service
            PostTrialExtension(trialKey, endDate, extensionDays);

            //Send email if the trial has been extended before
            if(trial.Of_Trial_Extensions__c != null && trial.Of_Trial_Extensions__c > 1){
                noOfExtentions = trial.Of_Trial_Extensions__c;
     
               //Set email recipient - Account owner' approval manager or Lead owner's approval manager
                if (trial.Contact__c != null){
                    if(trial.Contact_Account__r.Owner.Approval_Manager__r.Email != null){
                    	emailRecipient = trial.Contact_Account__r.Owner.Approval_Manager__r.Email;
                	}
                }
                else if (trial.Lead__c != null){
                    String lOwner = trial.Lead__r.OwnerId;
                    User u = [select Id, Approval_Manager__r.Email from user where id = :lOwner];
                    if(u.Approval_Manager__r.Email != null) {emailRecipient = u.Approval_Manager__r.Email;}
                }
                //Send mail
                SendMail(emailRecipient, noOfExtentions);
            }

            Update trial;          

        }

        return new pageReference('/' + TrialId);     
    }      


    public void SendMail(string emailRecipient, decimal noOfExtentions){
        System.debug('Send mail');

        noOfExtentions++;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] mailTo = new String[] {emailRecipient};
        mail.setToAddresses(mailTo);
        mail.setSenderDisplayName('Extend Trial');
        mail.setSubject('Extend Trial');
        mail.setPlainTextBody('This notification is to inform you that a trial extension for trial ' + URL.getOrgDomainUrl().toExternalForm() +'/'+ TrialId + ' has been initiated by ' + UserInfo.GetUserName() + '.\nThis is the second or more times this trial has been extended. The End Date will be extended and will increase the number of extensions to: ' + noOfExtentions + '. This number includes the user requested extensions.');        
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch(System.EmailException ex) {
            system.debug('Failed to send mail, error message: ' + ex.getMessage());
        }

    }


    //@future(callout = true)
    public void PostTrialExtension(string trialKey, Datetime endDate, integer extensionDays){
        QTCustomSettings__c custSettings = BoomiUtil.getCustomSettings();
        System.debug('custom settings ' + custSettings);        
        if (custSettings == null || custSettings.BoomiBaseURL__c == null) {
            system.debug('Cannot call boomi, settings are missing');
            return;
        }        

        String urlToCall = custSettings.BoomiBaseURL__c + WSENDPOINT;
        //String urlToCall = 'https://boomi-qa.qlik.com/ws/rest/sfdc/' + WSENDPOINT;
        String msgBody = '{"trialLicenseKey":"' + trialKey + '","endDate":"' + endDate + '","salesRepEmail":"' + UserInfo.getUserEmail() + '","extensionDays":"' + extensionDays + '"}';
        system.Debug('Calling BOOMI  ' + urlToCall + ' at '+ Datetime.now() +' with Message :: ' + msgBody);

        Http http = new Http();    
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(urlToCall);
        req.setHeader('Authorization',custSettings.BoomiToken__c); 
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(msgBody);

        try {
            HTTPResponse res = http.send(req);  
            System.debug(res.toString());
            System.debug('STATUS:' + res.getStatus());                  
            System.debug('STATUS_CODE:' + res.getStatusCode());
        } catch(System.CalloutException e) {
            System.debug('Error Calling BOOMI : ' + e);
        }

    }

}