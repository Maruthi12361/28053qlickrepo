/**
* PushKnowledgeArticlesToKPBTest
* 
* Description: Test covers PushKnowledgeArticlesToKPBHandler, PushBasicKnowledgeArticlesToKPB, PushDiagnosticKnowledgeArticlesToKPB
* Added: 11-07-2018 - ext_vos - CHG0034282
*
* Change log:
* 09-11-2018 - ext_vos - CHG0034844: add tests for triggers, update existed test methods, add test method for "manual publishing" logic.
* 14-03-2019 - ext_bad - CHG0035668 - update channels population.
* 28-10-2019 - AIN     - IT-2229 - Added test to exclude attunity product
* 13-07-2020 - ext_cqb - IT-2939 disable automation which sets IsVisibleInPKB
*/
@isTest
public class PushKnowledgeArticlesToKPBTest {

    @testSetup static void  createTestData() {
        QTCustomSettings__c settings1 = new QTCustomSettings__c();
        settings1.Name = 'Default';
        settings1.PushKnowledgeArticlesEmail__c = 'test@test.com';
        settings1.QlikNoReplyEmailAddress__c = 'no-reply@test.com';
        insert settings1;
        
        QS_Partner_Portal_Urls__c settings2 = new QS_Partner_Portal_Urls__c();
        settings2.ExcludeAttunityProductOnPublishToPKB__c = true;
        insert settings2;
    }
    
    static testMethod void testPushBasicKnowledgeArticlesToKPB() {
        // init data
        Account a = new Account(Name = 'testPushBasicKnowledgeArticlesToKPB Test');
        insert a;

        Contact con = new Contact(LastName = 'testcontact', AccountId = a.id);
        insert con;
        List<RecordType> entStdRecType = [SELECT Id FROM RecordType WHERE DeveloperName= 'QlikTech_Master_Support_Record_Type' AND SobjectType = 'Case'];
        
        if (entStdRecType.size() == 0) {
            System.assert(false, 'Missing record type!');
        }
        Case cse = new Case(Type='Electrical', AccountId = a.id, ContactId = con.id, Status = 'Closed',
                RecordTypeId = entStdRecType[0].Id);
        insert cse;
        Case cse2 = new Case(Type='Electrical2', AccountId = a.id, ContactId = con.id, Status = 'Closed',
                RecordTypeId = entStdRecType[0].Id);
        insert cse2;
        Case cse3 = new Case(Type='Electrical3', AccountId = a.id, ContactId = con.id, Status = 'In progress',
                RecordTypeId = entStdRecType[0].Id);
        insert cse3;
        Case cse4 = new Case(Type='Electrical3', AccountId = a.id, ContactId = con.id, Status = 'In progress',
                RecordTypeId = entStdRecType[0].Id);
        insert cse4;

        // create and publish article
        Id kaId = createBasicArticle();   
        Id kaId1 = createBasicArticle();
        Id kaId2 = createBasicArticleWithAttunity();

        // attach 'kaId' article to cases
        List<CaseArticle> caseArticles = new List<CaseArticle>();
        caseArticles.add(new CaseArticle(KnowledgeArticleId = kaId, CaseId = cse.Id));
        caseArticles.add(new CaseArticle(KnowledgeArticleId = kaId, CaseId = cse2.Id));
        caseArticles.add(new CaseArticle(KnowledgeArticleId = kaId, CaseId = cse3.Id));
        caseArticles.add(new CaseArticle(KnowledgeArticleId = kaId2, CaseId = cse4.Id));
        insert caseArticles;

        Basic__kav basicToTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Basic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId1];
        System.assertEquals(0, basicToTest.ArticleCaseAttachCount);
        System.assert(!basicToTest.IsVisibleInPkb);

        basicToTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Basic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId];
        System.assertEquals(3, basicToTest.ArticleCaseAttachCount);
        System.assert(!basicToTest.IsVisibleInPkb);

        PushKnowledgeArticlesToKPBHandler.TEST_EMAIL_SENDING = false;
        Test.startTest();
        Database.executeBatch(new PushBasicKnowledgeArticlesToKPB());
        Test.stopTest();
        System.assert(PushKnowledgeArticlesToKPBHandler.TEST_EMAIL_SENDING);

        // was handled - fits conditions
        basicToTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Basic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId];
        System.assert(basicToTest.IsVisibleInPkb);

        // wasn't handled - doesn't fit conditions
        basicToTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Basic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId1];
        System.assert(!basicToTest.IsVisibleInPkb);
        
        // Attunity, shouldn't have been handled
        basicToTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Basic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId2];
        System.assert(!basicToTest.IsVisibleInPkb);
    }

    static testMethod void testPushDiagnosticKnowledgeArticlesToKPB() {
        // init data
        Account a = new Account(Name = 'Cloud Environment Test');
        insert a;

        Contact con = new Contact(LastName = 'testcontact', AccountId = a.id);
        insert con;
        List<RecordType> entStdRecType = [SELECT Id FROM RecordType WHERE DeveloperName= 'QlikTech_Master_Support_Record_Type' AND SobjectType = 'Case'];
        
        if (entStdRecType.size() == 0) {
            System.assert(false, 'Missing record type!');
        }
        Case cse = new Case(Type='Electrical', AccountId = a.id, ContactId = con.id, Status = 'Closed',
                RecordTypeId = entStdRecType[0].Id);
        insert cse;
        Case cse2 = new Case(Type='Electrical2', AccountId = a.id, ContactId = con.id, Status = 'Closed',
                RecordTypeId = entStdRecType[0].Id);
        insert cse2;
        Case cse3 = new Case(Type='Electrical3', AccountId = a.id, ContactId = con.id, Status = 'In progress',
                RecordTypeId = entStdRecType[0].Id);
        insert cse3;

        // create and publish article
        Id kaId = createDiagnosticArticle();   
        Id kaId1 = createDiagnosticArticle();

        // attach 'kaId' article to cases
        List<CaseArticle> caseArticles = new List<CaseArticle>();
        caseArticles.add(new CaseArticle(KnowledgeArticleId = kaId, CaseId = cse.Id));
        caseArticles.add(new CaseArticle(KnowledgeArticleId = kaId, CaseId = cse2.Id));
        caseArticles.add(new CaseArticle(KnowledgeArticleId = kaId, CaseId = cse3.Id));
        insert caseArticles;

        Diagnostic__kav toTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Diagnostic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId1];
        System.assertEquals(0, toTest.ArticleCaseAttachCount);
        System.assert(!toTest.IsVisibleInPkb);

        toTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Diagnostic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId];
        System.assertEquals(3, toTest.ArticleCaseAttachCount);
        System.assert(!toTest.IsVisibleInPkb);

        Test.startTest();
        Database.executeBatch(new PushDiagnosticKnowledgeArticlesToKPB());
        Test.stopTest();

        // was handled - fits conditions
        toTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Diagnostic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId];
        System.assert(toTest.IsVisibleInPkb);

        // wasn't handled - doesn't fit conditions
        toTest = [select Id, ArticleCaseAttachCount, IsVisibleInPkb, KnowledgeArticleId from Diagnostic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: kaId1];
        System.assert(!toTest.IsVisibleInPkb);

        // just to cover 
        PushKnowledgeArticlesToKPBHandler.TEST_EMAIL_SENDING = false;
        PushKnowledgeArticlesToKPBHandler.generateReport('Basic__kav', '\'' + toTest.Id  + '\'', new Set<String>{'11111'});
        System.assert(PushKnowledgeArticlesToKPBHandler.TEST_EMAIL_SENDING);
    }

    static testMethod void testExceptionsForHandler() {
        Basic__kav basic = new Basic__kav(Title = 'test Exception article', UrlName = 'testURL' + System.now().millisecond(), Summary = 'test summary', Language = 'en_US');
        List<Basic__kav> articles = new List<Basic__kav>();
        articles.add(basic);
        Set<String> errorIds = PushKnowledgeArticlesToKPBHandler.publish(articles, 'Basic__kav');
        System.assertEquals(1, errorIds.size());

        Test.startTest();
        Database.executeBatch(new PushBasicKnowledgeArticlesToKPB());
        Database.executeBatch(new PushDiagnosticKnowledgeArticlesToKPB());
        Test.stopTest();

        PushKnowledgeArticlesToKPBHandler.TEST_EMAIL_SENDING = false;
        PushKnowledgeArticlesToKPBHandler.generateErrorReport('Basic__kav', 'SOME_ERROR_MESSAGE');
        System.assert(PushKnowledgeArticlesToKPBHandler.TEST_EMAIL_SENDING);
    }

    static testMethod void testRunBatch() {
        PushBasicKnowledgeArticles_RunBatch cshBasic = new PushBasicKnowledgeArticles_RunBatch();
        PushDiagnosticKnowledgeArticles_RunBatch cshDiagnostic = new PushDiagnosticKnowledgeArticles_RunBatch();

        Test.StartTest();
        cshBasic.execute();
        cshDiagnostic.execute();
        Test.stopTest();
    }

    static testMethod void testBasicArticleChannelsPopulation() {
        Basic__kav basicCspVisible = new Basic__kav(Title = 'test Basic article 1', UrlName = 'testURLB1' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);
        Basic__kav basicPkbVisible = new Basic__kav(Title = 'test Basic article 2', UrlName = 'testURLB2' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInPkb = true);
        Basic__kav basicPrmVisible = new Basic__kav(Title = 'test Basic article 3', UrlName = 'testURLB3' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInPrm = true);
        Basic__kav basicCspVisibleAttunity = new Basic__kav(Title = 'test Basic article 4', UrlName = 'testURLB4' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);
        
        
        

        List<Basic__kav> basics = new List<Basic__kav>();
        basics.add(basicCspVisible);
        basics.add(basicPkbVisible);
        basics.add(basicPrmVisible);
        basics.add(basicCspVisibleAttunity);
        Semaphores.BasicKnowledgeArticlesTriggerBeforeInsert = false;

        Test.startTest();
        //insert basics;
        Database.insert(basics);
        
        basicCspVisibleAttunity = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Basic__kav WHERE Id =: basicCspVisibleAttunity.Id];
        
        Basic__DataCategorySelection datCat = new Basic__DataCategorySelection();
        datCat.ParentId = basicCspVisibleAttunity.Id;
        datCat.DataCategoryGroupName = 'Attunity_Data_Categories';
        datCat.DataCategoryName = 'Attunity_Product';
        insert datCat;
        
        database.update(basics);
        
        basicCspVisibleAttunity = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Basic__kav WHERE Id =: basicCspVisibleAttunity.Id];
        System.assert(basicCspVisibleAttunity.IsVisibleInCsp);
        System.assert(!basicCspVisibleAttunity.IsVisibleInPkb);
        
        Test.stopTest();

        
        
        basicCspVisible = [SELECT Id, IsVisibleInPrm FROM Basic__kav WHERE Id =: basicCspVisible.Id];
        System.assert(basicCspVisible.IsVisibleInPrm);
        basicPkbVisible = [SELECT Id, IsVisibleInCsp, IsVisibleInPrm FROM Basic__kav WHERE Id =: basicPkbVisible.Id];
        System.assert(basicPkbVisible.IsVisibleInCsp);
        System.assert(basicPkbVisible.IsVisibleInPrm);
        basicPrmVisible = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Basic__kav WHERE Id =: basicPrmVisible.Id];
        System.assert(!basicPrmVisible.IsVisibleInCsp);
        System.assert(!basicPrmVisible.IsVisibleInPkb);
        
        basicCspVisibleAttunity = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Basic__kav WHERE Id =: basicCspVisibleAttunity.Id];
        System.assert(basicCspVisibleAttunity.IsVisibleInCsp);
        System.assert(!basicCspVisibleAttunity.IsVisibleInPkb);
    }

    static testMethod void testDiagnosticArticleChannelsPopulation() {
        Diagnostic__kav diagnosticCspVisible = new Diagnostic__kav(Title = 'test Diagnostic article 1', UrlName = 'testURLD1' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);
        Diagnostic__kav diagnosticPkbVisible = new Diagnostic__kav(Title = 'test Diagnostic article 2', UrlName = 'testURLD2' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInPkb = true);
        Diagnostic__kav diagnosticPrmVisible = new Diagnostic__kav(Title = 'test Diagnostic article 3', UrlName = 'testURLD3' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInPrm = true);
        Diagnostic__kav diagnosticCspVisibleAttunity = new Diagnostic__kav(Title = 'test Diagnostic article 4', UrlName = 'testURLD4' + System.now().millisecond(),
                Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);

        List<Diagnostic__kav> diagnostics = new List<Diagnostic__kav>();
        diagnostics.add(diagnosticCspVisible);
        diagnostics.add(diagnosticPkbVisible);
        diagnostics.add(diagnosticPrmVisible);
        diagnostics.add(diagnosticCspVisibleAttunity);
        Semaphores.DiagnosticKnowledgeArticlesTriggerBeforeUpdate = false;

        Test.startTest();
        insert diagnostics;
        
        diagnosticCspVisibleAttunity = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Diagnostic__kav WHERE Id =: diagnosticCspVisibleAttunity.Id];
        
        Diagnostic__DataCategorySelection datCat = new Diagnostic__DataCategorySelection();
        datCat.ParentId = diagnosticCspVisibleAttunity.Id;
        datCat.DataCategoryGroupName = 'Attunity_Data_Categories';
        datCat.DataCategoryName = 'Attunity_Product';
        insert datCat;
        
        update diagnostics;
        
        diagnosticCspVisibleAttunity = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Diagnostic__kav WHERE Id =: diagnosticCspVisibleAttunity.Id];
        System.assert(diagnosticCspVisibleAttunity.IsVisibleInCsp);
        System.assert(!diagnosticCspVisibleAttunity.IsVisibleInPkb);
        
        diagnosticCspVisibleAttunity.IsVisibleInCsp = true;
        diagnosticCspVisibleAttunity.IsVisibleInPkb = false;
        diagnosticCspVisibleAttunity.IsVisibleInPrm = false;
        update diagnosticCspVisibleAttunity;
        Test.stopTest();

        diagnosticCspVisible = [SELECT Id, IsVisibleInPrm FROM Diagnostic__kav WHERE Id =: diagnosticCspVisible.Id];
        System.assert(diagnosticCspVisible.IsVisibleInPrm);
        diagnosticPkbVisible = [SELECT Id, IsVisibleInCsp, IsVisibleInPrm FROM Diagnostic__kav WHERE Id =: diagnosticPkbVisible.Id];
        System.assert(diagnosticPkbVisible.IsVisibleInCsp);
        System.assert(diagnosticPkbVisible.IsVisibleInPrm);
        diagnosticPrmVisible = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Diagnostic__kav WHERE Id =: diagnosticPrmVisible.Id];
        System.assert(!diagnosticPrmVisible.IsVisibleInCsp);
        System.assert(!diagnosticPrmVisible.IsVisibleInPkb);
        
        diagnosticCspVisibleAttunity = [SELECT Id, IsVisibleInCsp, IsVisibleInPkb FROM Diagnostic__kav WHERE Id =: diagnosticCspVisibleAttunity.Id];
        System.assert(diagnosticCspVisibleAttunity.IsVisibleInCsp);
        System.assert(!diagnosticCspVisibleAttunity.IsVisibleInPkb);
    }

    static testMethod void testArticleManualFlow_Basic() {
        // create and publish article
        Basic__kav kaBasic = new Basic__kav(Title = 'test Basic testManualArticleFlow', UrlName = 'testManualArticleFlowB' + System.now().millisecond(), Summary = 'test summary', Language = 'en_US');   
        insert kaBasic;
        kaBasic = [select Id, ArticleNumber, KnowledgeArticleId from Basic__kav where Id =: kaBasic.Id];
        Id basicKnowledgeArticleId = kaBasic.KnowledgeArticleId;
        KbManagement.PublishingService.publishArticle(basicKnowledgeArticleId, true);

        // check
        Basic__kav toTest = [select Id, IsVisibleInPkb from Basic__kav where Id =: kaBasic.Id];
        System.assert(!toTest.IsVisibleInPkb);

        List<String> articleNumbers = new List<String>();
        articleNumbers.add(kaBasic.ArticleNumber);
        // run batch script by article number
        Test.startTest();
        Database.executeBatch(new PushBasicKnowledgeArticlesToKPB(articleNumbers));
        Test.stopTest();

        // check
        toTest = [select Id, IsVisibleInPkb from Basic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: basicKnowledgeArticleId];
        System.assert(toTest.IsVisibleInPkb);
    }

    static testMethod void testArticleManualFlow_Diagnostic() {
        // create and publish article
        Diagnostic__kav kaDiagnostic = new Diagnostic__kav(Title = 'test Diagnostic testArticleFlow', UrlName = 'testArticleManualFlowD' + System.now().millisecond(), Summary = 'test summary', Language = 'en_US'); 
        insert kaDiagnostic;
        kaDiagnostic = [select Id, ArticleNumber, KnowledgeArticleId from Diagnostic__kav where Id =: kaDiagnostic.Id];
        Id dKnowledgeArticleId = kaDiagnostic.KnowledgeArticleId;
        KbManagement.PublishingService.publishArticle(dKnowledgeArticleId, true);

        // check
        Diagnostic__kav toTest = [select Id, IsVisibleInPkb from Diagnostic__kav where Id =: kaDiagnostic.Id];
        System.assert(!toTest.IsVisibleInPkb);

        List<String> articleNumbers = new List<String>();
        articleNumbers.add(kaDiagnostic.ArticleNumber);
        // run batch script for flagged article
        Test.startTest();
        Database.executeBatch(new PushDiagnosticKnowledgeArticlesToKPB(articleNumbers));
        Test.stopTest();

        // check
        toTest = [select Id, IsVisibleInPkb from Diagnostic__kav where PublishStatus = 'Online' and KnowledgeArticleId =: dKnowledgeArticleId];
        System.assert(toTest.IsVisibleInPkb);
    }

    private static ID createBasicArticle() {
        Basic__kav basic = new Basic__kav(Title = 'test Basic article', UrlName = 'testURL' + System.now().millisecond(), Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);
        // need to simulate old created article
        Semaphores.BasicKnowledgeArticlesTriggerBeforeInsert = true;
        insert basic;
        basic = [select Id, KnowledgeArticleId from Basic__kav where Id =: basic.Id];

        KbManagement.PublishingService.publishArticle(basic.KnowledgeArticleId, true);
        return basic.KnowledgeArticleId;
    }

    private static ID createDiagnosticArticle() {
        Diagnostic__kav article = new Diagnostic__kav(Title = 'test Diagnostic article', UrlName = 'testURL' + System.now().millisecond(), Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);
        // need to simulate old created article
        Semaphores.DiagnosticKnowledgeArticlesTriggerBeforeInsert = true;
        insert article;
        article = [select Id, KnowledgeArticleId from Diagnostic__kav where Id =: article.Id];

        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        return article.KnowledgeArticleId;
    }
    private static ID createBasicArticleWithAttunity() {
        Basic__kav basic = new Basic__kav(Title = 'test Basic article', UrlName = 'testURL' + System.now().millisecond(), Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true);
        // need to simulate old created article
        Semaphores.BasicKnowledgeArticlesTriggerBeforeInsert = true;
        insert basic;
        basic = [select Id, KnowledgeArticleId from Basic__kav where Id =: basic.Id];
        
        Basic__DataCategorySelection datCat = new Basic__DataCategorySelection();
        datCat.ParentId = basic.Id;
        datCat.DataCategoryGroupName = 'Attunity_Data_Categories';
        datCat.DataCategoryName = 'Attunity_Product';
        insert datCat;

        KbManagement.PublishingService.publishArticle(basic.KnowledgeArticleId, true);
        return basic.KnowledgeArticleId;
    }
}