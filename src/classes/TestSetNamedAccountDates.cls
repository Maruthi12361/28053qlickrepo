@isTest

/***************************************************************************************************************************************

	Changelog:
		2012-02-15  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
		29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta
****************************************************************************************************************************************/

private class TestSetNamedAccountDates
{
    static testMethod void test_SetNamedAccountDates()
    {
        User testUser =[select Id, Name from User where isActive = true and UserType<>'PowerPartner' limit 1];
        List<Account> newList = new List<Account>();
        
        //Insert Account 
         Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'USA',
			QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = testSubs1.id			
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		// set Named Account to true
        Account newAccount1 = new Account (Name = 'Test Account1', OwnerId = testUser.Id, Named_Account__c = true, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        // Named Account = false
        Account newAccount2 = new Account (Name = 'Test Account2', OwnerId = testUser.Id, Named_Account__c = false, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        newList.add(newAccount1);
        newList.add(newAccount2);

        Semaphores.CustomAccountTriggerBeforeInsert = false;

        insert newList;
        
        //Update Account
        List<Account> accountList = new List<Account>();
        for(Account acct: newList)
        {
            if(acct.Named_Account__c)
            {
                acct.Named_Account__c = false;
            }
            else
            {
                acct.Named_Account__c = true;
            }
            accountList.add(acct);
        }
        Semaphores.CustomAccountTriggerBeforeUpdate = false;
        update accountList;
      
      //Update Account - Named Account set the second time
      List<Account> secondUpdateList = new List<Account>();
      for(Account acct: accountList)
      {
          if(acct.Named_Account__c == false) 
          {
              if(acct.End_Date_for_Named_Account__c != null)
              {
                 acct.End_Date_for_Named_Account__c = null;
              }
              acct.Start_Date_for_Named_Account__c = System.today();
              acct.Named_Account__c = true;
          }
          secondUpdateList.add(acct);
      
      }
      Semaphores.CustomAccountTriggerBeforeUpdate = false;
      update secondUpdateList;
   }
}