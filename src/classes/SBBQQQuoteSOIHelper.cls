/*	
	This class is used to create update SOI during quote save and edit.

    20161201 UIN Changes related to SOI insert/update for primary quotes
*/
public class SBBQQQuoteSOIHelper {

	private static final String CHAMPIONCONST = 'Champion';

	public SBBQQQuoteSOIHelper() {
		
	}

	/*** UIN Changes for SOI ****/
    /**************************************************************************************
    *
    *    Public  Methods
    ***************************************************************************************/
    /*  @Description :This common method is used to create SOI records.
    
        @parameter lSBQQQuotes: List of primary quote records
    */
    public static void createQuoteOppSOI(List<SBQQ__Quote__c> lSBQQQuotes, List<Sphere_of_Influence__c> listSOIToUpdate){
        List<Sphere_of_Influence__c> listSOIToInsert = new List<Sphere_of_Influence__c>();
        Sphere_of_Influence__c soiRec;
        for(SBQQ__Quote__c quote: lSBQQQuotes){
            soiRec = new Sphere_of_Influence__c(Opportunity__c = quote.SBQQ__Opportunity2__c,
                Role__c = CHAMPIONCONST, Contact__c = quote.Quote_Recipient__c, Quote_Recipient__c = true);
            listSOIToInsert.add(soiRec);
        }
        upsertSOI(listSOIToInsert, listSOIToUpdate);
    }

    public static  void upsertSOI(List<Sphere_of_Influence__c> listSOIToInsert, List<Sphere_of_Influence__c> listSOIToUpdate){
        List<Sphere_of_Influence__c> listUpsertSOI = new List<Sphere_of_Influence__c>();
        if(!listSOIToInsert.isEmpty()) {
            listUpsertSOI.addAll(listSOIToInsert);
        }if(!listSOIToUpdate.isEmpty()){
            listUpsertSOI.addAll(listSOIToUpdate);
        }
        if(!listUpsertSOI.isEmpty()) {
            upsert listUpsertSOI;
        }
    }


    /*  @Description :This common method is used to find all existing SOI records for a particular opportunity.
    
        @parameter oppIds: Set of opportunity ids for whoom the soi records must be fetched

        @return Map<Id, List<Sphere_of_Influence__c>>: Map of the quote recipient id as key and list of the SOI as values
    */
    public static  List<Sphere_of_Influence__c> fetchQuoteOppSOIs(Set<String> oppIds) {
        List<Sphere_of_Influence__c> listOfSoiId = new List<Sphere_of_Influence__c>();
        
        for(Sphere_of_Influence__c item : [Select id, Opportunity__c, Contact__c,Quote_Recipient__c From Sphere_of_Influence__c Where Opportunity__c IN : oppIds]) {
            listOfSoiId.add(item);
        }
        
        return listOfSoiId;
    }

    public static  Map<Id, List<Sphere_of_Influence__c>> createSoiMap( List<Sphere_of_Influence__c> listOfSoiId) {
        Map<Id, List<Sphere_of_Influence__c>> mRecipientSOI = new Map<Id, List<Sphere_of_Influence__c>>();
        if(!listOfSoiId.isEmpty()){
            for(Sphere_of_Influence__c soi : listOfSoiId) {
                if(mRecipientSOI.containsKey(soi.Contact__c)){
                    mRecipientSOI.get(soi.Contact__c).add(soi);
                }else 
                    mRecipientSOI.put(soi.Contact__c, new List<Sphere_of_Influence__c>{soi}); 
            }
        }

        return mRecipientSOI;
    }

    public static  Map<Id, List<Sphere_of_Influence__c>> createOppSoiMap( List<Sphere_of_Influence__c> listOfSoiId) {
        Map<Id, List<Sphere_of_Influence__c>> mOppSOI = new Map<Id, List<Sphere_of_Influence__c>>();
        if(!listOfSoiId.isEmpty()){
            for(Sphere_of_Influence__c soi : listOfSoiId) {
                if(mOppSOI.containsKey(soi.opportunity__c)){
                    mOppSOI.get(soi.opportunity__c).add(soi);
                }else 
                    mOppSOI.put(soi.opportunity__c, new List<Sphere_of_Influence__c>{soi}); 
            }
        }
        return mOppSOI;
    }

    /*  @Description :This common method is used to update the quote recipient flag on existing SOI records. i.e SOI record already exists and only flag needs to be updated
    
        @parameter setOfOppIds: Set of opportunity ids for whoom the soi records must be fetched
        @parameter lQuotesForUpd: List of quotes whose opp SOI must be updated
        @parameter mIdSOI: Map of all the existing opp SOI with quote recipient id as key
        @parameter lOldQuoteRecpIds: Set of quote recipient ids from old quote versions i,e from trigger.oldMap
    */
    public static  void updateQuoteOppSOI(List<SBQQ__Quote__c> lQuotesForUpd, Map<Id, List<Sphere_of_Influence__c>> mIdSOI, Set<String> lOldQuoteRecpIds, Set<String> setOfOppIds) {
        // List of SOI for final update
        List<Sphere_of_Influence__c> lFinalSOIForUpd = new List<Sphere_of_Influence__c>();
        //Map of all old SOI whose quote recipient flag must be reset
        Map<ID, Sphere_of_Influence__c> mOldIdSOI = new Map<ID, Sphere_of_Influence__c>([SELECT Id, Contact__c, Opportunity__c,Quote_Recipient__c FROM Sphere_of_Influence__c
                                                                        where /*Contact__c in :lOldQuoteRecpIds and */ Quote_Recipient__c = true and Opportunity__c in :setOfOppIds]);
        system.debug('step11'+lQuotesForUpd);
        system.debug('step12'+setOfOppIds);
        system.debug('step13'+lOldQuoteRecpIds);
        for(SBQQ__Quote__c quote: lQuotesForUpd){
            if(mIdSOI.containsKey(quote.Quote_Recipient__c)){
                for(Sphere_of_Influence__c SOI: mIdSOI.get(quote.Quote_Recipient__c)){
                    if((quote.Quote_Recipient__c == SOI.Contact__c) && (quote.SBQQ__Opportunity2__c == SOI.Opportunity__c) && !SOI.Quote_Recipient__c){
                        SOI.Quote_Recipient__c = true;
                        lFinalSOIForUpd.add(SOI);
                    }
                }
            }
        }
        //Reset Quote recipient flag for old SOI record whenever the quote recipient field is changed
        for(Sphere_of_Influence__c SOI: mOldIdSOI.values()){
            if(SOI.Quote_Recipient__c){
                SOI.Quote_Recipient__c = false;
                lFinalSOIForUpd.add(SOI);
            }
        }
        if(!lFinalSOIForUpd.isEmpty())
            update lFinalSOIForUpd;
    }

    /*  @Description :This common method is used to fetch only valud primary quote records for SOI insert/update
    
        @parameter updatedSBQQQuotes: Array of quote records which have been updated i.e Trigger.New
        @parameter SBQQQuoteOldMap: Map of old versions of quote records which have been changed i.e Trigger.OldMap

    */
    public static  void updQuoteOppSoi(SBQQ__Quote__c[] updatedSBQQQuotes, Map<ID, SBQQ__Quote__c> SBQQQuoteOldMap){
        //List of Quotes for which SOI must be created/updated
        List<SBQQ__Quote__c> lQuotesForSOI = new List<SBQQ__Quote__c>();
        Set<String> lOldQuoteRecipientIds = new Set<String>();
        Set<String> setOfOppIds = new Set<String>();
        Set<String> setOfContactIds = new Set<String>();
        for(SBQQ__Quote__c quote: updatedSBQQQuotes){
            system.debug('qqqqqq3'+quote.Quote_Recipient__c + SBQQQuoteOldMap.get(quote.id).Quote_Recipient__c);
            system.debug('qqqqqq4'+quote.SBQQ__Primary__c + SBQQQuoteOldMap.get(quote.id).SBQQ__Primary__c);
            if(((quote.SBQQ__Primary__c && !SBQQQuoteOldMap.get(quote.id).SBQQ__Primary__c) || ((quote.Quote_Recipient__c != SBQQQuoteOldMap.get(quote.id).Quote_Recipient__c) && quote.SBQQ__Primary__c)
                || ((quote.SBQQ__Opportunity2__c != SBQQQuoteOldMap.get(quote.id).SBQQ__Opportunity2__c) && quote.SBQQ__Primary__c)) 
                    && String.isNotBlank(quote.Quote_Recipient__c)
                        && String.isNotBlank(quote.SBQQ__Opportunity2__c)){ 
                system.debug('qqqqqq1'+quote); 
                lQuotesForSOI.add(quote);
                setOfOppIds.add(quote.SBQQ__Opportunity2__c);
                setOfContactIds.add(quote.Quote_Recipient__c);
                if(String.isNotBlank(SBQQQuoteOldMap.get(quote.id).Quote_Recipient__c))
                    lOldQuoteRecipientIds.add(SBQQQuoteOldMap.get(quote.id).Quote_Recipient__c);
            }  
        }
        system.debug('qqqqqq2'+lQuotesForSOI);
        if(!lQuotesForSOI.isEmpty()){
            callCreateUpdateSOI(lQuotesForSOI, setOfOppIds, lOldQuoteRecipientIds);
        }
    }

    /*  @Description :This common method is used to split changed quote records based on whether SOI records have to be inserted/updated or both
    
        @parameter lOldQuoteRecipientIds: Set of old quote recipient ids from valid changed q        
        @parameter setOfOppIds: Set of current quote opportunity ids from valid changed quotes
        @parameter lQuotesForSOI: Quotes which are eligible for SOI related record opera
    */
    public static  void callCreateUpdateSOI(List<SBQQ__Quote__c> lQuotesForSOI, Set<String> setOfOppIds, Set<String> lOldQuoteRecipientIds){
        List<Sphere_of_Influence__c> lSOI = fetchQuoteOppSOIs(setOfOppIds);
        system.debug('step1'+lSOI);
        Map<Id, List<Sphere_of_Influence__c>> mapOfRecipientIdSOI = createSoiMap(lSOI);
        Map<Id, List<Sphere_of_Influence__c>> mapOfOppIdSOI = createOppSoiMap(lSOI);
        system.debug('step2'+mapOfRecipientIdSOI);
        List<SBQQ__Quote__c> lQuotesForSOICreate = new List<SBQQ__Quote__c>();
        List<SBQQ__Quote__c> lQuotesForSOIUpd = new List<SBQQ__Quote__c>();
        List<Sphere_of_Influence__c> lSOIUpd = new List<Sphere_of_Influence__c>();
        for(SBQQ__Quote__c quote: lQuotesForSOI){
            system.debug('step3'+quote.SBQQ__Opportunity2__c);
            if(!mapOfRecipientIdSOI.containsKey(quote.Quote_Recipient__c) || mapOfRecipientIdSOI.isEmpty()){
                lQuotesForSOICreate.add(quote);
                system.debug('step4'+lQuotesForSOICreate);
                if(!mapOfOppIdSOI.isEmpty()){
                    system.debug('step5'+mapOfOppIdSOI);
                    for(Sphere_of_Influence__c soi: mapOfOppIdSOI.get(quote.SBQQ__Opportunity2__c)){
                        system.debug('step6'+soi.Opportunity__c+soi.Quote_Recipient__c);
                        if(soi.Opportunity__c == quote.SBQQ__Opportunity2__c && soi.Quote_Recipient__c){
                            soi.Quote_Recipient__c = false;
                            lSOIUpd.add(soi);
                        }
                    }
                }
            }
            else
                lQuotesForSOIUpd.add(quote);
        }
        system.debug('step8'+lSOIUpd);
        system.debug('step9'+lQuotesForSOICreate);
        system.debug('step10'+lOldQuoteRecipientIds);
        if(!lQuotesForSOICreate.isEmpty())
            createQuoteOppSOI(lQuotesForSOICreate, lSOIUpd);
        if(!lQuotesForSOIUpd.isEmpty())
            updateQuoteOppSOI(lQuotesForSOIUpd, mapOfRecipientIdSOI, lOldQuoteRecipientIds, setOfOppIds);
    }
}