/****************************************************************
*
*  OpportunityMandatorySoIOnUpdateHandler
*
*  Trigger will stop modifications of an Opportunity until a SoI
*  object has been created and linked.  
*
*  2016-05-18 TJG : Migrated from Opportunity_MandatorySoIOnUpdate trigger
*  2016-06-23 roman@4front Query for CurrencyType is replaced with CurrencyQueryHelper class where this query is moved to.
*  2017-06-14 Rodion Vakulvoskyi added static flag to get rid of soql query limit
*  2017-06-22 MTM  QCW-2711 remove sharing option
*****************************************************************/

public class OpportunityMandatorySoIOnUpdateHandler {
    public static Boolean FlagForInsert = true;
	public OpportunityMandatorySoIOnUpdateHandler() {
		
	}

	static public Boolean handle (List<Opportunity> triggerOld, List<Opportunity> triggerNew, Map<Id, Opportunity> triggerNewMap)
	{
		// only before update event is needed for original trigger
	    Set<string> Opps = new Set<string>();
	    Map<string, Boolean> OppHasSoI = new Map<string, Boolean>();
	    List<String> isoCodes = new List<String>();
	    List<CurrencyType> currencyList = new List<CurrencyType>();
	    
	    Map<string, decimal> IsoToConvert = new Map<string, decimal>();
	    //amountLimit is in USD - we'll use this as the default value if we get nothing from QTCustomSettings__c
	    decimal amountLimit = 5000.00;
	    
	    System.debug('Starting Opportunity_MandatorySoIOnUpdate');      
	    if (Semaphores.Opportunity_ManageForecastProductsIsInsert == false) //CCE 2013-03-12
	    {
	        QTCustomSettings__c MySetting = QTCustomSettings__c.getValues('Default');
	        if (MySetting != null)
	        {
	            string ProfileIDs  = MySetting.MandatorySoIOnUpdateExcludeProfiles__c;
	            if (ProfileIDs != null)
	            {
	                for (string ProfileID : ProfileIDs.split(','))
	                {
	                    if (UserInfo.getProfileId().startsWith(ProfileID))
	                    {
	                        System.debug('Finishing Opportunity_MandatorySoIOnUpdate not mandatory for this profile');
	                        return true;
	                    }       
	                }
	            }
	            
	            Decimal curLimit = MySetting.Value_Below_Which_SoI_Not_Mandatory__c;
	            System.debug('curLimit is: ' + curLimit);
	            if (CurLimit != null)
	            {
	                amountLimit = curLimit;                 
	            }
	            System.debug('amountLimit is: ' + amountLimit);     
	        }   
	        
	        ID IncludeID = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('CCS - Customer Centric Selling').getRecordTypeId();
	        System.debug('IncludeID Id is: ' + IncludeID);
	        ID IncludeID2 = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Qlikbuy CCS Standard').getRecordTypeId();
	        System.debug('IncludeID2 is: ' + IncludeID2);
	        ID IncludeID3 = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Qlikbuy CCS Standard II').getRecordTypeId();  // 2014-06-04
	        System.debug('IncludeID3 is: ' + IncludeID3);
	        ID ID_QlikbuyCCSAddon = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Qlikbuy CCS Add on').getRecordTypeId();
	        System.debug('ID_QlikbuyCCSAddon is: ' + ID_QlikbuyCCSAddon);
	        ID ID_CCSAddon = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('CCS - Add On').getRecordTypeId();
	        System.debug('ID_CCSAddon is: ' + ID_CCSAddon);
	        
	        // EDIT 19.09.2012 : Exclude Academic Program Opportunity records from this trigger, Id from Custom Setting
	        Id academidProgramOpportunityId;
	        if(Academic_Program_Settings__c.getInstance('AcademicProgramSetting') != null) {
	            academidProgramOpportunityId = Academic_Program_Settings__c.getInstance('AcademicProgramSetting').OpportunityRecordTypeId__c;
	        }
	        
	        for(Opportunity opp : triggerNew) {
	        	isoCodes.add(opp.CurrencyIsoCode);
	        }
	        //2016-06-23 roman@4front Query for CurrencyType is replaced with CurrencyQueryHelper class with the query inside it.
	        if(!isoCodes.isEmpty()) {
	        	currencyList = CurrencyQueryHelper.getCurrenciesByISOCodes(isoCodes);
	        }

	        for (integer i = 0; i < triggerNew.size(); i++)
	        {
	            if(academidProgramOpportunityId != null && triggerNew[i].RecordTypeId == academidProgramOpportunityId){
	                continue;
	            }
	    
	            // Don't check if this is a service opportunity
	            System.debug('triggerNew[i].Is_Services_Opportunity__c is: ' + triggerNew[i].Is_Services_Opportunity__c);
	            if (triggerNew[i].Is_Services_Opportunity__c)
	            {
	                continue;
	            }
	            
	            // Don't check when changing from Record Type Qlikbuy CCS Add On to Qlikbuy CCS Standard
	            // otherwise Catch 22 situation - can't save the new record as no SoI but can't add SoI
	            // as no SoI section on Qlikbuy CCS Add On record.
	            
	            //Changing at Josefine request - don't check for SoI if Qlikbuy CCS Add On or CCS - Add On record type
	            if ((triggerOld[i].RecordTypeId == ID_QlikbuyCCSAddon) || (triggerOld[i].RecordTypeId == ID_CCSAddon))
	            {
	                System.debug('Record type is Qlikbuy CCS Add On or CCS - Add On so skip');
	                continue;
	            }   
	            
	            //if ((triggerNew[i].RecordTypeId == IncludeID2) && (triggerOld[i].RecordTypeId == ID_QlikbuyCCSAddon))
	            //{
	            //  System.debug('Changing record type from Qlikbuy CCS Add On to Qlikbuy CCS Standard so continue');
	            //  continue;
	            //} 
	            
	            //Convert ammountLimit to correct currency for the opp
	            System.debug('triggerNew[i].CurrencyIsoCode is: ' + triggerNew[i].CurrencyIsoCode);
	            decimal amountLimitInOppCurrency = amountLimit; //this is in USD
	            if (triggerNew[i].CurrencyIsoCode != 'USD')
	            {
	                System.debug('not USD');
	                if (IsoToConvert.isEmpty())
	                {
	                	//2016-06-23 roman@4front Query for CurrencyType is replaced with CurrencyQueryHelper class with the query inside it.
	                    for (CurrencyType c : currencyList)
	                    	//[Select IsoCode, ConversionRate From CurrencyType])
	                    {
	                        IsoToConvert.put(c.IsoCode, c.ConversionRate);
	                    }
	                }   
	                    
	                if (IsoToConvert.containsKey(triggerNew[i].CurrencyIsoCode))
	                {
	                    System.debug('IsoToConvert.get(triggerNew[i].CurrencyIsoCode) is: ' + IsoToConvert.get(triggerNew[i].CurrencyIsoCode));
	                    amountLimitInOppCurrency = amountLimit * IsoToConvert.get(triggerNew[i].CurrencyIsoCode);
	                }
	            }
	            System.debug('amountLimitInOppCurrency is: ' + amountLimitInOppCurrency);
	            System.debug('triggerNew[i].Amount is: ' + triggerNew[i].Amount);
	            // if the opp Ammount field hasn't been filled in it will be null so set it to 0 for processing
	            if (triggerNew[i].Amount == null) {triggerNew[i].Amount = 0;}
	            // If the opp amount is less than the amountLimit then we don't want to process it.
	            if (triggerNew[i].Amount < amountLimitInOppCurrency)
	            {
	                System.debug('less than limit so continue');
	                continue;
	            }
	            System.debug('triggerNew[i].Amount is: ' + triggerNew[i].Amount);
	                    
	            if (triggerNew[i].IsNew__c)
	            {
	                System.debug('triggerNew[i].IsNew__c is: false');
	                triggerNew[i].IsNew__c = false;
	            } 
	            else
	            { 
	                System.debug('adding to Opps');
	                Opps.add(triggerNew[i].Id);
	                OppHasSoI.put(triggerNew[i].Id, false);
	                Semaphores.Opportunity_ManageForecastProductsIsInsert = true;//RDZ & MAW Fix soql query limit NS issue 2603  Case:https://eu1.salesforce.com/500D000000f3lNP
	            }
	        }
	        
	        if (Opps.size() == 0)
	        {
	            System.debug('Finishing Opportunity_MandatorySoIOnUpdate - (Opps.size() == 0'); 
	            return true;
	        }
	                
	        for (Sphere_of_Influence__c SoI : [select Id, Opportunity__c from Sphere_of_Influence__c where Opportunity__c in :Opps])
	        {
	            System.debug('Found an SoI for the Opp');
	            OppHasSoI.put(SoI.Opportunity__c, true);    
	        }
	        
	        for (string OppId : Opps)
	        {
	            Opportunity Opp = triggerNewMap.get(OppId);
	            System.debug('Opp.RecordTypeId is: ' + Opp.RecordTypeId);
	            //only perform the SoI check on the 'CCS - Customer Centric Selling' and 'QlikBuy CCS Standard' record types
	            if (!OppHasSoI.get(Opp.Id) && ((Opp.RecordTypeId == IncludeID) || (Opp.RecordTypeId == IncludeID2) || (Opp.RecordTypeId == IncludeID3)))
	            {
	                Opp.addError('You need to add a contact to the Sphere of Influence (SoI) on this opportunity before you can modify it. Click Cancel, Add relevant contacts to the SoI, save, then you will be able to update.');
	            }
	        }
	    }
	    
	    System.debug('Finishing Opportunity_MandatorySoIOnUpdate');
	    return true;
	}
}