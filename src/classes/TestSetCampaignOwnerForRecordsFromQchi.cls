/**
 
 Test for trigger SetCampaignOwnerForRecordsFromQchi
    For campaigns originated from Qchi CPT, we need to change the owner to the correct user ID
    that is saved in Qchi_ownerId__c
    27/06/2013  TJG First created CR# 8498   https://eu1.salesforce.com/a0CD000000ZAAjC
    20141216    BTN/CCE    CR# 19854 https://eu1.salesforce.com/a0CD000000nRqNR changes on picklist values, subtype.
 */
@isTest
private class TestSetCampaignOwnerForRecordsFromQchi {
    static final String qtwebId = '00520000000zCfxAAE';
    static testMethod void myUnitTest() {
        User opsuser = QTTestUtils.createMockOperationsAdministrator();

        List<Campaign> cmps = new List<Campaign>();
        for (integer index=0; index<23; index++) {
            Campaign cam = new Campaign(
                Name = 'Trigger Test Campaign'+index, 
                Planned_Opportunity_Value__c = 10000,
                Campaign_Sub_Type__c = 'INF - Other',
                Type = 'EV - Event',
                Publishable_Name__c = 'My Test' + index,
                LOB__c = 'Training',
                Qchi_OwnerId__c = opsuser.Id,
                StartDate = date.today(),
                EndDate = date.today().addDays(90),
                Status = 'Planned'          
            );
            cmps.Add(cam);
        }
        
        User u = [Select Id from user where Id= :qtwebId];
        System.runAs(u) {
            insert(cmps);
            ID cmpId = cmps.get(0).Id;
            Campaign camp = [select OwnerId from Campaign where Id= :cmpId limit 1];
            System.debug('Campaign 1 owner ID =' +  camp.OwnerId);
            
            update(cmps);
            camp = [select OwnerId from Campaign where Id= :cmpId limit 1];
            System.debug('Campaign 1 owner ID =' +  camp.OwnerId);
            System.System.assertEquals(opsuser.Id, camp.OwnerId);
        }
    }
}