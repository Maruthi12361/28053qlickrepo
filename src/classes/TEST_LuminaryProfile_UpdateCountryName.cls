/*********************************************
Test_LuminaryProfile_UpdateCountryName
Description: Test to populate Country Code and Copmany Name
             when the Nominee field is update with country 

Log History:
2013-08-22    KMH    Initial Development
2013-09-09    SLH    Commented out Name field as this is now auto-number
07.02.2017   RVA :   changing methods 
**********************************************/
@isTest
private class TEST_LuminaryProfile_UpdateCountryName {
    
    
     static testMethod void Test_LuminaryProfile_UpdateCountryName() {
        
        System.debug('Start of Test_LuminaryProfile_UpdateCountryName:');
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'SWE',
            Country_Name__c = 'Sweden',
            QlikTech_Company_Name__c = 'Sweden',
			Subsidiary__c = testSubs1.id
			);
        insert QTComp;

        QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'United Kingdom',
            QlikTech_Company_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id
			);
        insert QTComp;
        */
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('Sweden', 'SWE', 'United States');
			QTComp.Country_Name__c = 'Sweden';
		update QTComp; 

		QTComp = QTTestUtils.createMockQTCompany('United Kingdom', 'GBR', 'United States');
			QTComp.Country_Name__c = 'United Kingdom';
		update QTComp;
        Luminary_Profile__c TestLP = new Luminary_Profile__c (
        //Name= 'Luminary - Test ',
        Luminary_Type__c = 'Chatter Luminary',
        Status__c = 'New',
        //Owner = 'Stuart',
        Nominee_Country__c ='France',
        Luminary_Start_Date__c  = Date.today().addDays(10)
        );  
        
        insert TestLP;
        
        TestLP.Nominee_Country__c ='Sweden';
        TestLP.Nominator_First_Name__c ='Luminary';
        TestLP.Nominator_Last_Name__c ='Profile';
        
        update TestLP;
        
        test.startTest();
        
        TestLP = [select ID,Name,QlikTech_Company__c ,Nominee_Country__c,Country_Code__c from Luminary_Profile__c where Nominee_Country__c =: 'Sweden' LIMIT 1];
        
        System.debug(' Name '+ TestLP.Name);
        System.debug(' Nominee Country'+ TestLP.Nominee_Country__c);
        System.debug(' Country Code'+ TestLP.Country_Code__c);
        System.debug(' Nominee Company Name '+ TestLP.QlikTech_Company__c);
        
        System.debug('Stop of Test_LuminaryProfile_UpdateCountryName:');
        
        System.assertEquals(TestLP.Country_Code__c, 'SWE');
        
        //Update Nominee Country
        TestLP.Nominee_Country__c = 'United Kingdom';
        
        update TestLP;
        
        TestLP = [select ID,Name, Country_Code__c,QlikTech_Company__c from Luminary_Profile__c where Nominee_Country__c ='United Kingdom' LIMIT 1];
        
        
        System.debug(' Country Code'+ TestLP.Country_Code__c);
        //System.debug(' Nominee Company Name '+ TestLP.QlikTech_Company__c);
        
        
        
        System.assertEquals(TestLP.Country_Code__c, 'GBR');
        
        System.debug('Stop of Test_LuminaryProfile_UpdateCountryName:');
        test.stopTest();
     }
    
}