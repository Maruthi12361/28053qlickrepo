/**
  Rodion Vakulovskyi : 07.03.2017 : QCW-1159
  Ajay Santhosh : 25.01.2019 : BSL-1505 - Updated the code to handle Zuora subscription Quotes.
  Emily Duda: 12.07.2019: BSL-2324
  Emily Duda: 15.11.2019: BSL-2898
**/
public with sharing class QuoteApproverController {
    
  public List<sbaa__Approval__c> currentPendingApprovalsList {get; set;}
  public List<sbaa__Approval__c> futureApprovalslList {get; set;}
  public List<sbaa__Approval__c> contractedPriceList {get; set;}
  public List<sbaa__Approval__c> contractedPriceFutureList {get; set;}
  public String userThumbPhoto {get;set;}
  public List<sbaa__Approval__c> approvallis {get;set;}
  private final String STATUS_ASSIGNED ='Assigned';
  private final String STATUS_REQUESTED ='Requested';
  //private final String APPROVAL_STATUS_PENDING ='Pending';
  private final String userType = Schema.SObjectType.User.getKeyPrefix();
  private final String groupType = Schema.SObjectType.Group.getKeyPrefix();
    
    public QuoteApproverController(){
        User u = [Select u.SmallPhotoUrl, u.FullPhotoUrl From User u where u.id=:Userinfo.getUserid()];
        userThumbPhoto = u.FullPhotoUrl;
    }   

  public void getAssignedToApprovals(){     

    if (null==currentPendingApprovalsList && null==futureApprovalslList){
      currentPendingApprovalsList = new List<sbaa__Approval__c>();
      contractedPriceList = new List<sbaa__Approval__c>();
      contractedPriceFutureList = new List<sbaa__Approval__c>();
      futureApprovalslList = new List<sbaa__Approval__c>();
      if (Schema.sObjectType.sbaa__Approval__c.fields.Name.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__Status__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__Actions__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__Approver__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__Rule__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__DelegatedApprover__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__ApprovalChain__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__Rule__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.Quote__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.Zuora_Quote__c.isAccessible() &&
          Schema.sObjectType.sbaa__Approval__c.fields.sbaa__AssignedTo__c.isAccessible()  ){

    Set<Id> groupIds = new Set<Id>();
    Set<Id> groupMemberListItem = new Set<Id>();
    Map<id, Set<Id>> idToListMembers = new Map<id, Set<Id>>();
    approvallis = [Select id,Name, 
                   sbaa__Status__c,
                   sbaa__Actions__c,
                   sbaa__AssignedGroupId__c,
                   sbaa__Approver__r.Name,
                   sbaa__Approver__r.sbaa__GroupId__c,
                   sbaa__Rule__r.Name,
                   sbaa__DelegatedApprover__r.sbaa__User__r.Name, 
                   sbaa__ApprovalChain__r.Name,
                   Zuora_Quote__r.Name,
                   Zuora_Quote__r.Attunity_Quote__c,
                   Zuora_Quote__c,
                   Quote__r.Name,
                   Quote__r.Attunity_Quote__c,
                   Quote__c,
                   ContractedPrice__c,
                   Quote_Account__c,
                   Quote_Opportunity__c,
                   ContractedPrice__r.SBQQ__Account__r.Name,
                   ContractedPrice__r.Name,
                   ContractedPrice__r.SBQQ__Account__r.Id,
                   ContractedPrice__r.Contracted_Price_Type__c,
                   ContractedPrice__r.Hold_Type__c,
                   Owner.Name,
                   Quote_Revenue_Type__c,
                   Quote__r.SBQQ__Opportunity2__r.Id,
                   Quote__r.SBQQ__Account__r.Id,
                   Quote__r.ApprovalStatus__c,
                   Zuora_Quote__r.zqu__Opportunity__r.Id,
                   Zuora_Quote__r.zqu__Opportunity__r.Name,
                   Zuora_Quote__r.zqu__Account__r.Id,
                   Zuora_Quote__r.zqu__Account__r.Name,
                   Zuora_Quote__r.Revenue_Type__c,
                   Zuora_Quote__r.ApprovalStatus__c,
                   sbaa__AssignedTo__r.Name,
                   Quote_Sales_Rep__c
                   from sbaa__Approval__c
                   Where (sbaa__AssignedTo__c=:UserInfo.getUserId() OR sbaa__AssignedGroupId__c!=null)
                   AND (sbaa__Status__c = :STATUS_ASSIGNED OR sbaa__Status__c = :STATUS_REQUESTED)];
    for (sbaa__Approval__c approvalTtem : approvallis){
            groupIds.add(approvalTtem.sbaa__AssignedGroupId__c);     
        }

    List<GroupMember> groupMemberList= [Select id,GroupId,UserOrGroupId from GroupMember Where GroupId  IN :groupIds];
    Integer CodeStatementsStart = Limits.getCpuTime();

    for(Id groupItemId: groupIds){
        groupMemberListItem = new Set<Id>();
          for(GroupMember groupMemberItem: groupMemberList){
              if(groupMemberItem.GroupId==groupItemId){
                groupMemberListItem.add(groupMemberItem.UserOrGroupId);
              }
             } 
        idToListMembers.put(groupItemId, groupMemberListItem);
          }

    for (sbaa__Approval__c approvalTtem : approvallis){
              if(String.isNotEmpty(approvalTtem.sbaa__AssignedTo__c)){
                  if(String.valueOf(approvalTtem.sbaa__Status__c)==STATUS_REQUESTED  ){
                  //Rodion changes 07.03.2017
                  if((!String.isEmpty(approvalTtem.Quote__c) || !String.isEmpty(approvalTtem.Zuora_Quote__c)) && String.isEmpty(approvalTtem.ContractedPrice__c)) {
                    currentPendingApprovalsList.add(approvalTtem);
                  } else if(!String.isEmpty(approvalTtem.ContractedPrice__c) && (String.isEmpty(approvalTtem.Quote__c) || String.isEmpty(approvalTtem.Zuora_Quote__c))){
                    contractedPriceList.add(approvalTtem);
                  }
                  //end of changes
                  } else if(approvalTtem.sbaa__Status__c==STATUS_ASSIGNED  ){
                        //Rodion changes 07.03.2017
                        if((!String.isEmpty(approvalTtem.Quote__c) || !String.isEmpty(approvalTtem.Zuora_Quote__c)) && String.isEmpty(approvalTtem.ContractedPrice__c)) {
                          futureApprovalslList.add(approvalTtem);
                        }
                        if(!String.isEmpty(approvalTtem.ContractedPrice__c) && (String.isEmpty(approvalTtem.Quote__c) || String.isEmpty(approvalTtem.Zuora_Quote__c))){
                          contractedPriceFutureList.add(approvalTtem);
                        }
                        //end of changes
                }
              } else if (String.isEmpty(approvalTtem.sbaa__AssignedTo__c) && String.isNotEmpty(approvalTtem.sbaa__AssignedGroupId__c)){
                    for( Id userId :idToListMembers.get(approvalTtem.sbaa__AssignedGroupId__c)){
                      if(userId==UserInfo.getUserId()){
                        if(approvalTtem.sbaa__Status__c==STATUS_REQUESTED){
                            //Rodion changes 07.03.2017
                                          if((!String.isEmpty(approvalTtem.Quote__c) || !String.isEmpty(approvalTtem.Zuora_Quote__c)) && String.isEmpty(approvalTtem.ContractedPrice__c)) {
                              currentPendingApprovalsList.add(approvalTtem);
                            }
                            if(!String.isEmpty(approvalTtem.ContractedPrice__c) && (String.isEmpty(approvalTtem.Quote__c) || String.isEmpty(approvalTtem.Zuora_Quote__c)) ){
                              contractedPriceList.add(approvalTtem);
                            }
                            //end of changes
                        } else if(approvalTtem.sbaa__Status__c==STATUS_ASSIGNED  ){
                            //Rodion changes 07.03.2017
                                          if((!String.isEmpty(approvalTtem.Quote__c) || !String.isEmpty(approvalTtem.Zuora_Quote__c)) && String.isEmpty(approvalTtem.ContractedPrice__c)) {
                              futureApprovalslList.add(approvalTtem);
                            }
                            if(!String.isEmpty(approvalTtem.ContractedPrice__c) && (String.isEmpty(approvalTtem.Quote__c) || String.isEmpty(approvalTtem.Zuora_Quote__c))){
                              contractedPriceFutureList.add(approvalTtem);
                            }
                            //end of changes
                        }
                      }
                    }
              }
        }

        Integer CodeStatementsTotal = Limits.getCpuTime() - CodeStatementsStart;
        System.Debug('CodeStatementsTotal - '+CodeStatementsTotal + ', currentPendingApprovalsList = ' + currentPendingApprovalsList);
      } 
     }
    } 
    public List<String> getCurrentPendingTitle(){
      return new String[]{'Account','Opportunity', 'Quote','Revenue Type','Approval Rule',/*'Status','Approval Chain',*/'Requestor/Sales Rep','Approver','Delegated Approver','Attunity Quote'};
    }
    //Rodion changes 07.03.2017
    public List<String> getContractedPrice(){
      return new String[]{'Account','Contracted Price', 'Contracted Price Type', 'Hold Type', 'Approval Rule', 'Requestor/Sales Rep', 'Approver', 'Delegated Approver'};
    }

    public List<String> getFutureContractedPrice(){
      return new String[]{'Account','Contracted Price', 'Contracted Price Type', 'Hold Type', 'Approval Rule', 'Requestor/Sales Rep', 'Approver', 'Delegated Approver'};
    }
    //end of changes
        public List<String> getFutureApprovalTitle(){
      return new String[]{'Account','Opportunity', 'Quote','Revenue Type','Approval Rule',/*'Status','Approval Chain',*/'Requestor/Sales Rep','Approver','Delegated Approver', 'Attunity Quote'};
    }
        //    public static Set<id> GetUserIdsFromGroup(Set<id> mapKeySet){
        //    // store the results in a set so we don't get duplicates
        //    Set<Id> result=new Set<Id>();
        //    String userType = Schema.SObjectType.User.getKeyPrefix();
        //    String groupType = Schema.SObjectType.Group.getKeyPrefix();
            
        //    // Loop through all group members in a group
        //    for (GroupMember m : [Select Id, UserOrGroupId From GroupMember Where GroupId  IN :mapKeySet]){
        //        // If the user or group id is a user
        //        if (((String)m.UserOrGroupId).startsWith(userType)){
        //            result.add(m.UserOrGroupId);
        //        }
        //        // If the user or group id is a group
        //        // Note: there may be a problem with governor limits if this is called too many times
        //        else if (((String)m.UserOrGroupId).startsWith(groupType)){
        //            // Call this function again but pass in the group found within this group
        //            result.addAll(GetUSerIdsFromGroup(m.UserOrGroupId));
        //        }
        //    }
        //    return result;  
        //}
      
}