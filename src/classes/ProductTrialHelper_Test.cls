//-----------------------------------------------------------------------------------------------------------
// ProductTrialHelper_Test
// Description : A tester class for Production
// @author : Leonardo Grauer
//-----------------------------------------------------------------------------------------------------------

@isTest
private class ProductTrialHelper_Test  {

		static testMethod void testUpdateAccountERPStatus() {

		//Creating an End User Account
		Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo= cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get('End User Account').getRecordTypeId();
        Account acc=new Account(name ='Test Account',recordtypeid = rtId );
        insert acc;
		System.debug('###24 acc:' +acc);

		//Creating a Contact
		Contact ct = QTTestUtils.createMockContact(acc.Id);
		System.debug('###24 ct:' +ct);

		//Creating Product Trial
		Product_Trial__c prdTrial = new Product_Trial__c();
		prdTrial.Contact__c = ct.Id;
		prdTrial.Contact_Account__c = acc.Id;
		prdTrial.Trial_License_Key__c = '1234';
		prdTrial.Product__c = 'Qlik Sense Business';
		prdTrial.Start_Date__c = Date.today();
		prdTrial.End_Date__c = Date.today().addDays(5);
		insert prdTrial;
		System.debug('###24 prdTrial:' +prdTrial);
				
	}//function
}//class