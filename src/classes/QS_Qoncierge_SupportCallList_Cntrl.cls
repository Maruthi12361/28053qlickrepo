/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_Qoncierge_SupportCallList_Cntrl {
	
	public List<SelectOption> countryPhoneList {get; set;}
    public String selectedCountry {get; set;}
    private Map<String, QS_Support_Contact_Info__c> supportContactInfoMap;
    public QS_Support_Contact_Info__c supportDetailInfo {get; set;}
    
    public QS_Qoncierge_SupportCallList_Cntrl() {
    	
    	// populating dropdown values for support information related to the various countries/regions
        supportContactInfoMap = QS_Support_Contact_Info__c.getAll();
        if(supportContactInfoMap != null && supportContactInfoMap.keySet() != null && !supportContactInfoMap.keySet().isEmpty()) {
        	countryPhoneList = new List<SelectOption>();
        	for(String countryName: supportContactInfoMap.keySet()) {
        		countryPhoneList.add(new SelectOption(countryName, countryName));
        	}
        	if(countryPhoneList != null && (!countryPhoneList.isEmpty()))	
        		countryPhoneList.sort();
        }
    }
    
    // set the support detail information to contact for the current user's selected region/country
    public PageReference populateSupportDetailInfo() {
    	
    	if(selectedCountry != null && selectedCountry != '' 
    		&& supportContactInfoMap != null && supportContactInfoMap.keySet() != null && (!supportContactInfoMap.keySet().isEmpty())
    		&& supportContactInfoMap.values() != null && (!supportContactInfoMap.values().isEmpty()))	
	    		supportDetailInfo = supportContactInfoMap.get(selectedCountry);
    	else
    		supportDetailInfo = null;
    		
    	return null;
    		
    }
    
    
}