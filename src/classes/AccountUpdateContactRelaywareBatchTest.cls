/**************************************************
* CR# 97533 – Relayware triggers
* Change Log:
* 2016-10-31 BAD Initial creation of test class 
**************************************************/
@isTest
private class AccountUpdateContactRelaywareBatchTest {
	
	@isTest static void test_ContactRelaywareBatch() {
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        { 
			Account acc = QTTestUtils.createMockAccount('Test Account', mockSysAdmin, false);
			acc.Navision_Status__c = 'Partner';
			insert acc;
			system.assert(acc.Id != null);

			List<Id> AccountsApprovedList = new List<Id>();
			AccountsApprovedList.add(acc.Id);

			//Contact con = QTTestUtils.createMockContact(acc.Id);
  			Contact con1 = new Contact(
  					FirstName='Qtestbad1First', 
                   	LastName='Qtestbad1Last', 
                   	Email='qtestbad1@test.com.sandbox',
                   	AccountId = acc.Id,
                   	ActiveULC__c=true
                   	);			
			insert con1;
    		Contact con2 = new Contact(
  					FirstName='Qtestbad2First', 
                   	LastName='Qtestbad2Last', 
                   	Email='qtestbad2@test.com.sandbox',
					AccountId = acc.Id,
                   	ActiveULC__c=true
                   );
			insert con2;
    		Contact con3 = new Contact(
  					FirstName='Qtestbad3First', 
                   	LastName='Qtestbad3Last', 
                   	Email='qtestbad3@test.com.sandbox',
					AccountId = acc.Id,
                   	ActiveULC__c=true
                   );
			insert con3;

			system.assert(con1.Id != null);
			system.assert(con2.Id != null);
			system.assert(con3.Id != null);

	        Test.startTest();

			AccountUpdateContactRelaywareBatch objBatch = new AccountUpdateContactRelaywareBatch(AccountsApprovedList);
			ID batchprocessid = Database.executeBatch(objBatch);

	        Test.stopTest();
		}        

	}
	

	
}