/********************************************************
* CLASS: WebActivityHandlerTest
* DESCRIPTION: Test class for Web Activity handler class
*
* CHANGELOG:    
*	2018-10-15 - BAD - Added Initial logic
*   2019-03-05 - CRW/BAD - added w2l logic
*********************************************************/
@isTest
private class WebActivityHandlerTest
{

    @testSetup 
    static void setup() {

        Semaphores.SetAllSemaphoresToTrue();

        List<QlikTech_Company__c> QlikCompanies = new List<QlikTech_Company__c>();

        QlikTech_Company__c qc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
            qc.Country_Name__c = 'Sweden';
            qc.QlikTech_Region__c = 'NORDIC';
            qc.QlikTech_Sub_Region__c = 'NORDIC';
            qc.QlikTech_Operating_Region__c = 'N EUROPE';
            qc.Language__c = 'Swedish';
        QlikCompanies.add(qc);
        upsert QlikCompanies;

        Account a = new Account();
        a.Name = 'The Base Company';
        a.Navision_Status__c = '';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        insert a;

        QuoteTestHelper.createCustomSettings();
        QTCustomSettings__c setting = QTCustomSettings__c.getInstance('Default');
        setting.ULC_QT_SSO_Account__c = a.Id;
        update setting;

        WebActivitySettings__c csWebActivity = new WebActivitySettings__c();
        insert csWebActivity;

        Semaphores.SetAllSemaphoresToFalse();
    }

	
	@isTest
	static void test_beforeInsert()
	{
		WebActivityHandler tempObj = new WebActivityHandler();

		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        {
        	Lead lead = createLead('qtestbad1');
        	ULC_Details__c ulc = createULC('qtestbad1', lead.Id);

	        Web_Activity__c wa = new Web_Activity__c(
				Name = 'waBAD1',
				Sender__c = 'QlikID',
				Type__c = 'QSD_FirstLogin',
				Status__c = 'Pending',
				Content__c = '{"TimeStamp":"2018-10-04T11:11:11.916Z","ULCUsername":"qtestbad1"}'
	        	);
	        insert wa;
	        system.assert(wa.Id != null);	

	        Web_Activity__c wa2 = new Web_Activity__c(
				Name = 'waBAD2',
				Sender__c = 'QlikID',
				Type__c = 'QSD_FirstLogin',
				Status__c = 'Pending',
				Content__c = '{"TimeStamp""2018-10-04T11:11:11.916Z","ULCUsername":"qtestbad1"}'
	        	);
		    insert wa2;
			system.assert(wa2.Id != null);	

		    WebActivityUtils waUtils = new WebActivityUtils();
	    	WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();

	        Test.startTest();	        
				WAcontent = (WebActivityUtils.WebActivityContent) JSON.deserialize (wa.Content__c, WebActivityUtils.WebActivityContent.class);  
				wa.ULC__c = ulc.Id;
				system.assert(wa.ULC__c == ulc.Id);

				try{
					WAcontent = (WebActivityUtils.WebActivityContent) JSON.deserialize (wa2.Content__c, WebActivityUtils.WebActivityContent.class);  
	            }
	            catch (System.Exception ex)
	            {
					system.assertEquals(ex.getMessage(), ex.getMessage());
				}
	        Test.stopTest();	    	

		}
	}

	@isTest
	static void test_afterInsert()
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        {

        	Lead lead = createLead('qtestbad2');
        	ULC_Details__c ulc = createULC('qtestbad2', lead.Id);

	        Web_Activity__c wa = new Web_Activity__c(
				Name = 'waBAD3',
				Sender__c = 'QlikID',
				Type__c = 'QSC_FirstLogin',
				Status__c = 'Pending',
				Content__c = '{"TimeStamp":"2018-10-04T11:11:11.916Z","ULCUsername":"qtestbad2"}'
	        	);

	        Web_Activity__c wa2 = new Web_Activity__c(
				Name = 'waBAD4',
				Sender__c = 'QlikID',
				Type__c = 'QSD_FirstLogin',
				Status__c = 'Pending',
				Content__c = '{"TimeStamp":"2018-10-04T11:11:11.916Z","ULCUsername":"qtestbad2"}'
	        	);

	        Test.startTest();	        
		        insert wa;
		        system.assert(wa.Id != null);	
		        system.assert(wa.Type__c == 'QSC_FirstLogin');	

		        insert wa2;
		        system.assert(wa2.Id != null);	
		        system.assert(wa2.Type__c == 'QSD_FirstLogin');	
			Test.stopTest();	  

		}
	}

	@isTest
	static void test_LeadGenerationQlikID()
	{
		//Test case covers OnCreate_ULCDetails 
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        {
			HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
			setting.Name = 'Default';
			setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
			insert setting;        	

        	ULC_Details__c ulc = new ULC_Details__c(
				ULCName__c = 'ulcusername2',
				ULCStatus__c = 'Awaiting Activation',
				oUser__c = '{"Type":"SetUser","action":"SetUser","oUser":{"UserName":"ulcusername2","CountryCode":"SWE", "FirstName":"first","LastName":"last","EmailAddress":"first.last@test.com","Phone":"+46 123324","Account":"test","Country":"","JobTitle":"test","LeadID":"","ULCDetailsID":"","LeadSourceDetail":"","LeadSource":"WEB - Web Activity","ClaimedQTRel":"","QCloudID":"","EmailOptOut":false,"Feature":"DIRECT_ULC"},"additionalParams":{"SupplimentalData":"WEB_ACTIVITY_SOURCE~WA~SOURCEID2~Qlik Sense Desktop~SOURCEURL~https://qlik.qa.qws.qlik.com/us/try-or-buy/download-qlik-sense~INCENTIVE~Free Download~LEADSOURCEDETAILMIRROR~WEB - Download QlikSense~CAMPAIGNID~701D0000001PKdL~TCACCEPTED~True~REF~QSDW~","Asset":"Qlik Cloud Subscription","ZiftData":""}}'
				);

	        Test.startTest();	        
		        insert ulc;
		        List<Web_Activity__c> lstWa = [SELECT name from Web_Activity__c where ULC__c = :ulc.Id];
		        if (lstWa.size() > 0)
		        	system.assert(lstWa[0].Id != null);	
			Test.stopTest();	  

		}
	}
    @isTest
	static void test_LeadGenerationQlikCom()
	{
		//Test case covers OnCreate_ULCDetails 
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        {
			HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
			setting.Name = 'Default';
			setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
			insert setting;

        	ULC_Details__c ulc = new ULC_Details__c(
				ULCName__c = 'ulcusername22',
				ULCStatus__c = 'Awaiting Activation');
				//oUser__c = '{"Type":"SetUser","action":"SetUser","oUser":{"UserName":"ulcusername22","CountryCode":"SWE", "FirstName":"first","LastName":"last","EmailAddress":"first.last@test.com","Phone":"+46 123324","Account":"test","Country":"","JobTitle":"test","LeadID":"","ULCDetailsID":"","LeadSourceDetail":"","LeadSource":"WEB - Web Activity","ClaimedQTRel":"","QCloudID":"","EmailOptOut":false,"Feature":"DIRECT_ULC"},"additionalParams":{"SupplimentalData":"WEB_ACTIVITY_SOURCE~WA~SOURCEID2~Qlik Sense Desktop~SOURCEURL~https://qlik.qa.qws.qlik.com/us/try-or-buy/download-qlik-sense~INCENTIVE~Free Download~LEADSOURCEDETAILMIRROR~WEB - Download QlikSense~CAMPAIGNID~701D0000001PKdL~TCACCEPTED~True~REF~QSDW~","Asset":"Qlik Cloud Subscription","ZiftData":""}}'
			insert ulc;

            string json2 = '{"head":{"timestamp":"2018-11-09T07:14:10.3619957Z"},';
            json2 += '"body":{';
            json2 += '"user":{';
            json2 += '"ids": [';
            json2 += '{"systemName": "Marketo","systemType": "marketing automation","idType": "tracking","id": "id:497-BMK-910&token:_mch-qlik.com-1542095853734-38546"}]},';
            json2 += '"data":{"status":"executing","guid":"687d7498-c86a-4994-8030-7d04ee0cb091","timestamp":"2018-11-09T07:14:10.2160133Z","url":"https://qlik.dev.qws.qlik.com/us/try-or-buy/buy-now?sourcepartner=srcPartner&kw=keyword1&utm_source=utmsssssource",  "urlReferrer":"https://qlik.dev.qws.qlik.com/us/try-or-buy",';
            json2 += '"acknowledgements": [{"typeAck": "acknowledgement","name": "privacyPolicy","isAcceptanceEnabled": false,';
            json2 += '"isPreaccepted": true,"isAccepted": true,"isRequired": false,"text": "Please note that by submitting your personal data, you agree to receive marketing messages and other communications from Qlik. You may opt-out of receiving further communications at any time. For further information please see our <a target=newWin href=https://www.qlik.com/us/legal/cookies-and-privacy-policy>Privacy Policy</a>.",';
            json2 += '"contentAck": [{"type": "content reference","contentGUID": "0f8f96c1-22bd-11e8-8dbc-0abb13a835aa","contentLanguage": null,"contentVersion": "1.0",';
            json2 += '"contentType": "terms","guid": "055afb96-04cc-4852-8741-f6fe3f14d934","language": "en"}],"sortOrder": 0,"key": "privacyPolicyAcceptanceNotRequired","guid": "670bcaa8-a386-4598-9c16-256aa02e41e3","language": "en"}],';
            json2 += '"fields":{"incentive":"Incentives","sourceID2":"soource5656","campaignID":"7010D00000028Fy","ULCUsername":"ulcusername22","comments":"test devq1","company":"qlik","country":"IND","emailAddress":"qtestbad4@test.com.sandbox","firstName":"","lastName":"qtestbad4","jobTitle":"manager","postalCode":"90002","telephoneNumber":"440156456464"}}}}';
            
            Web_Activity__c wa = new Web_Activity__c();
			wa.name = 'waRecDowFromQlikCom';
			wa.Type__c = 'LeadGeneration';
			wa.Sender__c = 'qlik.com';
			wa.Content__c = json2;

	        Test.startTest();	        
		        insert wa;
		        List<Web_Activity__c> lstWa = [SELECT name,Status__c from Web_Activity__c where ULC__c = :ulc.Id];
		        if (lstWa.size() > 0)
	        		system.assertEquals('Completed', lstWa[0].Status__c);
			Test.stopTest();	  

		}
	}

	@isTest
	static void test_RecordDownload()
	{
		//Test case covers OnCreate_ULCDetails 
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        {
	        List<Campaign> Campaigns = new List<Campaign>();
	        Campaign cam = new Campaign();
	        cam.Name = 'My Campaign';
	        Campaigns.add(cam);
	        insert Campaigns;

        	Lead lead = createLead('qtestbad3');
        	ULC_Details__c ulc = createULC('qtestbad3', lead.Id);

			String json = '{"Type":"SetUser","action":"SetUser","oUser":{"UserName":"qtestbad3","ULCDetailsID":""},';
			json += '"additionalParams":{"SupplimentalData":"WEB_ACTIVITY_SOURCE~WA~SOURCEID2~Qlik Sense Desktop~SOURCEURL~https://qlik.qa.qws.qlik.com/us/try-or-buy/download-qlik-sense~INCENTIVE~Free Download~LEADSOURCEDETAILMIRROR~WEB - Download QlikSense~';
			json += 'CAMPAIGNID~' + cam.Id + '~TCACCEPTED~True~REF~QSDW~","Asset":"Qlik Cloud Subscription","ZiftData":""}}';        	

			Web_Activity__c wa = new Web_Activity__c();
			wa.name = 'waRecDow';
			wa.Type__c = 'RecordDownload';
			wa.Sender__c = 'QlikId';
			wa.Content__c = json;

	        Test.startTest();	        
	        	insert wa;
	        	List<Web_Activity__c> lstWa = [SELECT Status__c from Web_Activity__c where name = :wa.name];
	        	System.debug('BAD stat' + wa.Status__c);
	        	System.debug('BAD wa' + wa);
	        	if (lstWa.size() > 0)
	        		system.assertEquals('Completed', lstWa[0].Status__c);
			Test.stopTest();	  
		}
	}

	private static Lead createLead(String name){
        Lead lead = new Lead(
	        LastName = name,
	        Company = 'Company', 
	        IsUnreadByOwner = True,
	        Country='Sweden',
	        Email= name + '@test.com.sandbox',
	        Phone='3333'
        	);
        insert lead;        	
        return lead;
	}

	private static ULC_Details__c createULC(String name, Id leadID){
        ULC_Details__c ulc = new ULC_Details__c(
            LeadId__c = leadID,
            ULCName__c = String.isNotBlank(name) ? name : 'qtestbad',
            ULCStatus__c = 'Active'
        	);
        insert ulc;
        return ulc;
	}



}