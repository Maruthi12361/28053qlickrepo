/**     
 * File Name : Z_RenewalQuoteBuildAndSaveBatchable
 * Description : This is a batchable job to recalculate the quote metrics against the renewal quotes, triggered by 
 *               Z_RenewalQuoteBuildAndSaveBatchable batchable class.  It calls the following methods in this order:
 *               1. start - returns QueryLocator to pass a list of renewal quotes created by Z_RenewalQuoteBatchable, which already 
 *                          went through the Z_RenewalQuoteBuildAndSaveBatchable and Z_RenewalQuoteRulesEngineBatchable 
 *                          batchable classes
 *               2. execute - retrieves the list of active rules and runs them against renewal quotes retrieved from step 1
 *               3. finish - terminates the job
 * @author : Inki Hong
 * Modification Log ======================================================================
 * Ver     Date         Author         Modification
 * 1       05.19.2020   Inki Hong      Created new class (RTR-477)
 */

global with sharing class Z_RenewalQuoteRecalculateBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts {
    
    private List<Id> quoteIds;

    private final String CLASS_NAME = 'Z_RenewalQuoteRecalculateBatchable',
                         DEBUG_PREFIX = '> $ ' + this.CLASS_NAME;
    
    global Z_RenewalQuoteRecalculateBatchable(List<Id> quoteIds) {
        this.quoteIds = quoteIds;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' at the job start:');
        for (Id quoteId : this.quoteIds) {
            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' quote id = ' + quoteId);
        }

        String queryString = 'SELECT Id '
                           + 'FROM zqu__Quote__c '
                           + 'WHERE Id IN (\'' + String.join(this.quoteIds, '\',\'') + '\')';
        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext bc, List<zqu__Quote__c> quotes) {
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' at the job execute');
        for (zqu__Quote__c quote : quotes) {
            Id quoteId = quote.Id;
            
            try { 
                zqu.zQuoteUtil.ZBillingResult previewResult = zqu.QuoteRecalculateController.JR_recalculate(quoteId);
            } catch (Exception e) { 
                System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' exception caught');
                return; 
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        
    }
}