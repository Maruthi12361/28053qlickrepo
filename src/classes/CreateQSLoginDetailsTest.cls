/********************************************************
* CLASS: CreateQSLoginDetailsTest
* DESCRIPTION: Test class for class CreateQSLoginDetails
*
* CHANGELOG:    
*	2018-10-29 - BAD - Added Initial logic
*********************************************************/
@isTest
private class CreateQSLoginDetailsTest
{

	@isTest
	static void test_callBatch()
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        {
        	List<Web_Activity__c> lstWA = new List<Web_Activity__c>();

			Account acc = QTTestUtils.createMockAccount('Test Account', mockSysAdmin, false);
			acc.Navision_Status__c = 'Partner';
			insert acc;
			system.assert(acc.Id != null);

  			Contact con = new Contact(
				FirstName='BADTestContact', 
               	LastName='BADTestLastName', 
               	Email='BADTestContact@test.com.sandbox',
               	AccountId = acc.Id
               	);			
			insert con;			

	        Lead lead = new Lead(
		        LastName = 'LastName',
		        Company = 'Company', 
		        IsUnreadByOwner = True,
		        Country='Sweden',
		        Email='BADTestLead@test.com.sandbox',
		        Phone='3333'
	        	);
	        insert lead;
	        
	        ULC_Details__c ulc = new ULC_Details__c(
	            LeadId__c = lead.Id,
	            ULCName__c = 'qtestbad',
	            ULCStatus__c = 'Active'
	        	);
	        insert ulc;

	        ULC_Details__c ulc2 = new ULC_Details__c(
	            ContactId__c = con.Id,
	            ULCName__c = 'qtestbad2',
	            ULCStatus__c = 'Active'
	        	);
	        insert ulc2;

	        Campaign camp1 = new Campaign(Name = 'Test Campaign 1', IsActive = True);
	        insert camp1;

	        WebActivitySettings__c csWebActivity = new WebActivitySettings__c();
	        csWebActivity.QSDTaskSubject__c = 'Initial Log In – Qlik Sense Desktop;';
	        csWebActivity.QSCTaskSubject__c = 'Initial Log In – Qlik Sense Cloud;';
	        csWebActivity.QSDCreateCMOActive__c = true;
	        csWebActivity.QSCCreateCMOActive__c = false;
	        csWebActivity.QSDLoginCampaignID__c  = camp1.Id;
	        insert csWebActivity;

	        Web_Activity__c wa = new Web_Activity__c(
				Name = 'waBAD1',
				Sender__c = 'QlikID',
				Type__c = 'QSD_FirstLogin',
				Status__c = 'Pending',
				Content__c = '{"TimeStamp":"2018-10-04T11:11:11.916Z","ULCUsername":"qtestbad"}'
	        	);
	        insert wa;
	        lstWA.add(wa);

	        Web_Activity__c wa2 = new Web_Activity__c(
				Name = 'waBAD2',
				Sender__c = 'QlikID',
				Type__c = 'QSC_FirstLogin',
				Status__c = 'Pending',
				Content__c = '{"TimeStamp":"2018-10-04T11:11:11.916Z","ULCUsername":"qtestbad2"}'
	        	);
	        insert wa2;
	        lstWA.add(wa2);

	        Test.startTest();	        

			CreateQSLoginDetails updloadBatch = new CreateQSLoginDetails(lstWA, 'QSD_FirstLogin');
			Database.executeBatch(updloadBatch, 100);

	        Test.stopTest();
        }		


	}
}