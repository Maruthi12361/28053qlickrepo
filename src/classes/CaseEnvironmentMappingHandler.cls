/**     * File Name:CaseEnvironmentMappingHandler
        * Description : This handler class is used to set the environment details on the case everytime the 
        *               environment is set on case from a blank value 
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Oct 3rd 2016 RamakrishnaKini     Added new trigger logic.
*       2018-03-01 ext_bad LCE-62 populate environment for Live Chat Support RecordType cases
*       2019-12-19 ext_cqb IT-2279 set Product on Case when Environment.Product = 'QlikView'
*/
public with sharing class CaseEnvironmentMappingHandler {

	//Populate environment related fields on case opening.
    public static final String SERVER_PUBLISHER_CASE_PRODUCT = 'Server/Publisher';
    public static final String QLIK_VIEW_ENVIRONMENT_PRODUCT = 'QlikView';
    private static final String QLIK_TECH_MASTER_SUPPORT_RECORD_TYPE = 'QlikTech Master Support Record Type';
    private static final String LIVE_CHAT_SUPPORT_RECORD_TYPE = 'Live Chat Support Record Type';

    static List<Id> lEnvIds = new List<Id>();
	static List<Case> lCases = new List<Case>();
	//Map to store all record type ids and names
    static Map<Id,Schema.RecordTypeInfo> caseRecordTypeMap = Case.SObjectType.getDescribe().getRecordTypeInfosById();

	/*  @Description :This common method is used to map environmnet details on to a case during case insertion.
    
        @parameter inputList: Trigger.new Case list
   
    */
    public static void onBeforeInsert(List<Case> inputList) {
    	for(Case c: inputList){
			if((QLIK_TECH_MASTER_SUPPORT_RECORD_TYPE.equalsIgnoreCase(caseRecordTypeMap.get(c.RecordTypeId).getName())
                || LIVE_CHAT_SUPPORT_RECORD_TYPE.equalsIgnoreCase(caseRecordTypeMap.get(c.RecordTypeId).getName()))
                    && String.isNotBlank(c.EnvironmentOfCase__c)){
				lEnvIds.add(c.EnvironmentOfCase__c);
                lCases.add(c);
			}
		}
        if (!lEnvIds.isEmpty()) {
            updCaseEnvDetails();
        }
	}


	/*  @Description :This common method is used to update case environment details on to a case when the enivornment details
    	are updated on to a case from a blank value.

        @parameter inputList: Trigger.new Case list
        @parameter inputMap: Trigger.newMap 
        @parameter oInputList: Trigger.old Case list  
    */
    public static void onBeforeUpdate(List<Case> inputList, Map<Id, Case> inputMap, Map<Id, Case> oInputMap) {
    	for(Case c: inputList){
			if((QLIK_TECH_MASTER_SUPPORT_RECORD_TYPE.equalsIgnoreCase(caseRecordTypeMap.get(c.RecordTypeId).getName())
                    || LIVE_CHAT_SUPPORT_RECORD_TYPE.equalsIgnoreCase(caseRecordTypeMap.get(c.RecordTypeId).getName()))
						&& String.isNotBlank(c.EnvironmentOfCase__c) && String.isBlank(oInputMap.get(c.Id).EnvironmentOfCase__c)){
				lEnvIds.add(c.EnvironmentOfCase__c);
                lCases.add(c);
			}
		}
        if (!lEnvIds.isEmpty()) {
            updCaseEnvDetails();
        }
	}

	/**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/

    private static void updCaseEnvDetails(){
    	Map<Id, Environment__c> mIdEnvironments = new Map<Id, Environment__c>([SELECT Id, Architecture__c, Clustered__c, Customer_Patch_Version__c, Description__c,
                                                                                                    Operating_System__c, Product__c, Product_License__c, Product_License__r.Name, Type__c,
                                                                                                        Version__c, Virtual__c FROM Environment__c WHERE Id IN :lEnvIds]);
        Map<Id, List<EnvironmentProduct__c>> mEnvIdProducts = new  Map<Id, List<EnvironmentProduct__c>>();
        List<EnvironmentProduct__c> lEnvProds = [SELECT Id,Name,Operating_System__c,Product__c,Version__c,Environment__c FROM EnvironmentProduct__c WHERE Environment__c IN :lEnvIds];
        for(Id envId : lEnvIds){
            for(EnvironmentProduct__c envProd: lEnvProds){
                if(envId == envProd.Environment__c){
                    if(mEnvIdProducts.containsKey(envId)){
                       mEnvIdProducts.get(envId).add(envProd);     
                    }else{
                       mEnvIdProducts.put(envId, new List<EnvironmentProduct__c>{envProd}); 
                    }
                }
            }
        }
        if(!mIdEnvironments.isEmpty()){
            for(Case c: lCases){
                if(mIdEnvironments.containsKey(c.EnvironmentOfCase__c)){
                    String temp = '';
                    Environment__c environment = mIdEnvironments.get(c.EnvironmentOfCase__c);
                    c.Environment_Architecture__c = environment.Architecture__c;
                    c.Environment_Clustered__c = environment.Clustered__c;
                    c.Environment_Customer_Patch_Version__c = environment.Customer_Patch_Version__c;
                    c.Environment_Description__c = environment.Description__c;
                    c.Environment_Operating_System__c =  environment.Operating_System__c;
                    c.Environment_Product__c = environment.Product__c;
                    c.Environment_Version__c = environment.Version__c;
                    c.Environment_Virtual__c = environment.Virtual__c;
                    c.Environment_Product_License__c = environment.Product_License__c;
                    c.Environment_Type__c = environment.Type__c;
                    if(!mEnvIdProducts.isEmpty()){
                        for(EnvironmentProduct__c envProd: mEnvIdProducts.get(c.EnvironmentOfCase__c)) {
                            temp =  temp + envProd.Product__c + ' v'  + envProd.Version__c + ';';
                        }
                    }    
                    c.Environment_Product_Details__c = temp;
                    c.Product__c = environment.Product__c == QLIK_VIEW_ENVIRONMENT_PRODUCT ? SERVER_PUBLISHER_CASE_PRODUCT : c.Product__c;
                }    
            }
        }
    }
}