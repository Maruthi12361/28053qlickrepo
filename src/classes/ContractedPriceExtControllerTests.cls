@isTest
public class ContractedPriceExtControllerTests {

    private static final String PARTNERACC = '01220000000DOFzAAO';
    public static testMethod void testContractedPriceExtController() {
    
        User u = QTTestUtils.createMockSystemAdministrator();
        User newManageUser = QTTestUtils.createMockSystemAdministrator();
        
        u.User_Access__c='Admin';
        u.Quote_Approval_Role__c='NAM';
        u.Approval_Manager__c=newManageUser.id;

        update u;

        System.runAs(u) {
            
        Boolean checkError=false;
        Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
        QuoteTestHelper.createCustomSettings();
    /*
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c companyQT = TestQuoteUtil.createQCompany(sub.id);
    */
        Account testResseller = TestQuoteUtil.createReseller();
        Contact testContact = TestQuoteUtil.createContact(testResseller .id);
    /*
        Account  testAcc = TestQuoteUtil.createAccount(companyQT.id); 
    */
    Account  testAcc = QTTestUtils.createMockAccount('TestCompany', u, true);
        testAcc.RecordtypeId = PARTNERACC;
        update testAcc;
        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Evaluation_Quote'];
        

        Test.startTest();
      SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
        quoteForTest.ApprovalStatus__c='Pending';
        update quoteForTest;
            Test.stopTest();
        
        Group testGroup = [Select id,Name from Group Where Name='Finance Orders Quote Approvers' Limit 1 ];
        GroupMember gm= new GroupMember(); 
        gm.GroupId=testGroup.id;
        gm.UserOrGroupId = u.id;
        insert gm;
              System.assertNotEquals(null, gm.id);
             
           List<sbaa__Approval__c> approvaList = new List<sbaa__Approval__c>();
              for(Integer i=0;i<10;i++){
                approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Requested',
                                                       sbaa__ApprovalStep__c=i,
                                                       sbaa__RecordField__c='Test'+i,
                                                       sbaa__AssignedTo__c=UserInfo.getUserId()
                                                       ));
                  approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Assigned',
                                                       sbaa__ApprovalStep__c=i+10,
                                                       sbaa__RecordField__c='Test'+i+10,
                                                       sbaa__AssignedTo__c=UserInfo.getUserId()
                                                       ));
                  approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Assigned',
                                                       sbaa__ApprovalStep__c=i+20,
                                                       sbaa__RecordField__c='Test'+i+20,
                                                       sbaa__AssignedGroupId__c =testGroup.id,
                                         sbaa__AssignedTo__c=null
                                                       ));
                                    approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Requested',
                                                       sbaa__ApprovalStep__c=i+30,
                                                       sbaa__RecordField__c='Test'+i+30,
                                                       sbaa__AssignedGroupId__c =testGroup.id,
                                         sbaa__AssignedTo__c=null
                                                       ));
                }
             
           try{
quoteForTest.Quote_Approval_Reason__c = 'asdasdd';
update quoteForTest;
              insert approvaList;
            
        }
           catch(DmlException e) {
               checkError = true;   
          }
           System.assertEquals(false, checkError);
    
        ApexPages.StandardController stdLead = new ApexPages.StandardController(quoteForTest);

        ContractedPriceExtController controller = new ContractedPriceExtController(stdLead);
        controller.onSubmit();
        controller.onRecall();
        
        controller.approvals = approvaList;
        }
    }
}