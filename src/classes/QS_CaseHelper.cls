/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
global without sharing class QS_CaseHelper {
    
    public static Boolean deleteStakeholder(Id caseId, Id stakeholderId)
    {
        List<EntitySubscription> todel = [select id from EntitySubscription where SubscriberId=:stakeholderId and ParentId=:caseId limit 1];
        if(todel.size() > 0)
        {
            delete todel;
            return true;
        }
        else
            return false;
    }
    
    public static Boolean deleteStakeholders(Id caseId, list<String> stakeholderIds)
    {
        list<EntitySubscription> todel = [select id from EntitySubscription where SubscriberId in :stakeholderIds and ParentId = :caseId];
        if(todel.size() > 0)
        {
            delete todel;
            return true;
        }
        else
            return false;
    }

    public static Boolean addStakeholder(EntitySubscription stakeholder)
    {
        insert stakeholder;
        return true;
    }


    public static Boolean addStakeholders(EntitySubscription[] stakeholders )
    {
        insert stakeholders;
        return true;
    }

    @RemoteAction 
    global static void setEnvironmentToCase ( Id caseId, Id environmentId  ){
        if ( caseId != null && environmentId != null ){
            update new case (Id=caseId, EnvironmentOfCase__c = environmentId );
        }
    }
}