/**     * File Name:DelegateApprovalRequest
        * Description : This class is used to find the approver for opportunities which are registered through Qlik Commerce. If PAM is available
                        He will be the approver and if he is on vacation, make his delegated user as approver.
        * @author : ext_axw
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 July 26 2019 Anjuna Baby     Added new logic to support a delegate process for when PAM's are on holiday so that the approvals can be reassigned to another person.
        
*/
public class DelegateApprovalRequest{

    public static void handle(List<Opportunity> triggerOld, List<Opportunity> triggerNew){
        Map <String, Opportunity> oppApproverMap = new Map <String, Opportunity>();
        Map <Opportunity, String> oppDelegateApproverMap = new Map <Opportunity, String>();
    
        for(opportunity opp : triggerNew){
            if(opp.StageName =='Registration Pending Approval'){
                opp.sbaa__Approver__c = opp.Active_PSM__c;
                oppApproverMap.put(opp.Active_PSM__c,opp);
            }

        }
        
        //check any of the approver is on vacation, and then assign delegated approver as the approver.
        if(oppApproverMap.size() > 0) {
            
            for(User u : [SELECT id, name, DelegatedApproverId, Delegate_Start_Date__c, Delegate_End_Date__c 
                            FROM User 
                            WHERE Id IN : oppApproverMap.keyset()]) {
                if(u.Delegate_Start_Date__c != null && u.Delegate_End_Date__c != null && u.Delegate_Start_Date__c <= Date.Today()
                                                   && u.Delegate_End_Date__c >=Date.Today()) {
                    
                    oppDelegateApproverMap.put(oppApproverMap.get(u.Id), u.DelegatedApproverId);
                }    
            
            }
            
            if(oppDelegateApproverMap.size() > 0) {
                for(Opportunity opp : oppDelegateApproverMap.keyset()) {
                    if( oppDelegateApproverMap.get(opp) != null) {
                    
                      opp.sbaa__Approver__c = oppDelegateApproverMap.get(opp);
                      
                    }
                }
            }    
        }
        
    }
}