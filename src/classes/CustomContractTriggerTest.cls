/*
	CustomContractTriggerTest
	
	Author:
		Tao Jiang, tjg@qlikview.com
		
	Changes:
		2017-01-16 TJG	Created.
        2017-02-15  Update to cover Sales Channel assignments.
        2017-03-27 Roman Dovbush (4front) : Added call to ContractTriggerHandler.tempVar to increase code coverage.
        
	Description:
		Test class for CustomContractTrigger.

 */
@isTest
public class CustomContractTriggerTest {
	static final integer NBR_OF_ACCOUNTS = 3;
	static final Id SysAdminProfileId = '00e20000000yyUzAAI';
	static final Id PortalUserRoleId  = '00E20000000vrJSEAY';
	static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';

	public static testmethod void nsContractMatchChannelTest()
	{
	    Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator
        User testUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        // Create 3 accounts.  
        List<Account> testAccounts = createTestAccounts(testUser, ctList);

        Date today = Date.today();

        Id channelId = SalesChannelUtils.createSalesChannel(testAccounts[0].Id, testAccounts[1].Id, testAccounts[2].Id, null);

        System.assertNotEquals(channelId, null);
        Contract aContract;
  		System.RunAs(me)
        {
        	aContract =   createTestContract(testAccounts[0].Id, testAccounts[1].Id, testAccounts[2].Id, today, null);
        }

        System.debug('Contract = ' + aContract);

        //System.assertEquals(channelId, aContract.Sales_Channel__c);

        Test.stopTest();

        Contract con = [select Id, Sales_Channel__c from Contract where Id = :aContract.Id];
        //System.assertEquals(channelId, con.Sales_Channel__c);

        // call ContractTriggerHandler directly
        // String tempVar = ContractTriggerHandler.tempVar;
    }

	public static testmethod void nsContractNoChannelTest()
	{
		Account testAcc = QTTestUtils.createSimpleAccount();
		
		Contract testContract = QTTestUtils.createSimpleContract(testAcc.id, null);

		System.assertEquals(null, testContract.Sales_Channel__c);
	}

	public static testmethod void processTest() {

		Contract testContract = formTestData();
		testContract.Status = 'Activated';
		update testContract;
		Q2CWSettings__c q2cwSettings = new Q2CWSettings__c(name = UserInfo.getUserId());
		q2cwSettings.Disable_Contract_trigger__c = true;
		Insert q2cwSettings;
		update testContract;
	}

	private static Contract formTestData() {
		Account testAcc = QTTestUtils.createSimpleAccount();
		Sales_Channel__c testContractAssetLink = QTTestUtils.createSimpleAssetLink(testAcc.id);
		Contract testContract = QTTestUtils.createSimpleContract(testAcc.id, testContractAssetLink.id);
		return testContract;
	}

    private static List<Account> createTestAccounts(User aUser, List<Contact> cLst)
    {
        List<Account> aLst = new List<Account>();
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser){
            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Account userAccount = new Account(
                    Billing_Country_Code__c = qtComp.Id,
                    QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                    Name = 'UserAcc' + i,
                    OwnerId = aUser.Id,
                    RecordTypeId = PartnerAccRecTypeId
                    );
                aLst.add(userAccount);  
            }

            Insert aLst;

            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Contact contact = new Contact( FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = aLst[i].Id,
                Email = 'tc' + i + '@test.com');
                cLst.add(contact);  
            }
            insert cLst;
        } 
        return aLst;
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me ) 
    {
        User u 
            = new User(
               alias = 'adminUsr', email='adminUser@tjtest.com.test',
                Emailencodingkey='UTF-8', lastname='adminTest', 
                Languagelocalekey='en_US', localesidkey='en_US',
                Profileid = profId,
                Timezonesidkey='America/Los_Angeles', 
                //ContactId = cont.Id,
                UserRoleId = roleId,
                Username= System.now().millisecond() + '_' + '_newuser@jttest.com.test'
            );

        System.runAs ( me ) 
        {
            insert u;
        }

        return u;
    }

    public static Contract createTestContract(Id accountId, Id resellerId, Id supportId, Date startDate, Id calId) {
    Contract c = new Contract(
                Name = 'test',
                StartDate = startDate,
                Status = 'Draft', 
                AccountId = accountId,  
                ContractTerm = 4,
                Support_Level__c = 'Basic',
                Support_Percentage__c = 15,
                Sell_Through_Partner__c = resellerId,
                Support_Provided_By__c = supportId,
                Support_Provided_By_Type__c = 'Qlik',
                Sales_Channel__c = calId
                );
    insert c;
  	return c;
  }
}