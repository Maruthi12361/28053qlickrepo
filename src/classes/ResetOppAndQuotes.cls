/**     * File Name:ResetOppAndQuotes
        * Description : This class is the logic for reset opp button on opp layouts
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Feb 24th 2017 RamakrishnaKini   Added new logic.
        1.1 Feb 2nd 2018  Reshma Ravi       QCW-4762 Clear License Recipient and related SOI 
*/
global class ResetOppAndQuotes {
     private final Opportunity Opp;
     private static final String INTERNAL_URL = System.URL.getSalesforceBaseUrl().toExternalForm();					
     private static String BASE_URL = (UserInfo.getUserType().contains('Power')) ? Network.getSelfRegUrl(Network.getNetworkId()).substringBefore('/s/login/SelfRegister'):INTERNAL_URL;
                                
     public ResetOppAndQuotes(ApexPages.StandardController stdController) {
        this.Opp= (Opportunity)stdController.getRecord();
    }

    @RemoteAction
    public static String resetQuotesDeleteProducts(Id oppId) { 
        try{
            system.debug('I am inside'+oppId);
            String errorMsg =  BASE_URL + '/' + oppId;
            List<String> quoteStatus = new List<String>{'Order Placed', 'Accepted by Customer'};
            List<SBQQ__Quote__c> oppClosedQuotes = [select id from SBQQ__Quote__c where SBQQ__Opportunity2__c = :oppId and SBQQ__Status__c in :quoteStatus];
            if(oppClosedQuotes.isEmpty() || oppClosedQuotes.size() == 0){
                List<SBQQ__Quote__c> quoteInContracting = [select id from SBQQ__Quote__c where SBQQ__Opportunity2__c = :oppId and SBQQ__Status__c ='Contracting' and SBQQ__Primary__c = true];
                if(quoteInContracting.isEmpty() || quoteInContracting.size() == 0){
                    List<SBQQ__Quote__c> quotesForOpp = new List<SBQQ__Quote__c>();
                    List<SBQQ__Quote__c> primaryQuotesForUpd = new List<SBQQ__Quote__c>();
                    quotesForOpp = [select id,SBQQ__Primary__c from SBQQ__Quote__c where SBQQ__Opportunity2__c = :oppId];
                    for(SBQQ__Quote__c quote:quotesForOpp){
                        if(quote.SBQQ__Primary__c)
                            primaryQuotesForUpd.add(quote);
                    }
                    if(!primaryQuotesForUpd.isEmpty()){
                        for(SBQQ__Quote__c quote:primaryQuotesForUpd)
                            quote.SBQQ__Primary__c = false;
                    update primaryQuotesForUpd;
                    }
                    if(!quotesForOpp.isEmpty()){
                        List<Id> quoteIds = new List<Id>();
                        for(SBQQ__Quote__c quote:quotesForOpp)
                            quoteIds.add(quote.Id);
                        List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
                        quoteLines = [select id from SBQQ__QuoteLine__c where SBQQ__Quote__c in :quoteIds];
                        if(!quoteLines.isEmpty())
                            delete quoteLines;
                        if(!quoteIds.isEmpty())
                            delete quotesForOpp;
                    }
                }else{
                    errorMsg = errorMsg + '$' + 'Please cancel the SpringCM workflow PRIOR to having this button complete the actions.';
                }
            }else
                errorMsg = errorMsg + '$' + 'This process is not possible. There are Quotes in either Order Placed or Accepted by Customer on this opportunity.';
            //system.debug('here'+oppId+System.URL.getSalesforceBaseUrl().toExternalForm());
            
            //QCW-4762
            Opportunity o = [SELECT Id, License_Recipient__c FROM Opportunity WHERE Id = :oppId][0];
            
            List<Sphere_of_Influence__c> deleteSOIs = [SELECT Id FROM Sphere_of_Influence__c WHERE Opportunity__c = : oppId AND Contact__c = :o.License_Recipient__c];
            if(!deleteSOIs.isEmpty()){
                DELETE deleteSOIs;
            }
            
            o.Id = oppId;
            o.License_Recipient__c = null;
            UPDATE o;
            
            return errorMsg;
        }catch(Exception ex){
            system.debug('Exception Occurred '+ex.getmessage());
            return BASE_URL + '/' + oppId;   
        }
    }
}