@isTest
private class UserHelperTest {

    private static testMethod void testGetExtendedUser() {
        Test.startTest();
        User u = UserHelper.getExtendedUser(); 
        Test.stopTest();
        System.assertEquals(UserInfo.getUserId(), u.Id);
    }
    
}