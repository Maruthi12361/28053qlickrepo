@isTest
private class TestAttachmentUpdateForCase {
    /******************************************************

        TestAttachmentUpdateForCase
    
        This method makes unit test of the AttachmentUpdateForCase
        trigger.
    
        Changelog:
            2012-07-24  SAN     Created method
        	2012-11-20	SAN		Fix the unit test failure where contact need LeadSource to be mandatory
                            
    ******************************************************/     
    static testMethod void TestNewAttachmentUpdateForCase()
    {
    
        System.debug('TestAttachmentUpdateForCase: Starting');
        
        Account testAct = new Account( Name = 'Test Account', Navision_Status__c = 'Something', QlikTech_Company__c = 'QlikTech Nordic AB');            
        insert testAct;
        
        Contact ctct = new Contact( Email = 'testEmail@gmail.com', FirstName = 'TestFN', LastName = 'TestLN', AccountId = testAct.Id, LeadSource='WEB - Web Activity');
        insert ctct;
        
        Case c1 = new Case( Status = 'Closed', AccountId = testAct.Id, ContactId = ctct.Id);
        insert c1;      
        
        System.assert(c1.Attachment_added__c == null);      
        
        Attachment att = new Attachment(ParentId = c1.Id, Name = 'New attachment', Body = Blob.valueOf('String'));
        insert att;
        
        c1 = [select Attachment_added__c from Case where Id = :c1.Id];
        
        
        System.assertEquals( 'New attachment', c1.Attachment_added__c);
        
        System.debug('TestAttachmentUpdateForCase: Ending');

}
}