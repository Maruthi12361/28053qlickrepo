/**
* Change log:
*
*   2019-09-30 ext_bad  IT-2173: Multiple escalations page.
*/
public with sharing class AtRiskEscalationsListController {
    public String accId { get; set; }
    public String accName { get; set; }
    public List<At_risk_escalation__c> riskEscalations { get; set; }

    public AtRiskEscalationsListController(ApexPages.StandardController stdController) {
        accId = ((Account) stdController.getRecord()).Id;
        if (accId == null) {
            accId = ApexPages.currentPage().getParameters().get('accId');
        }
        init();
    }

    public void init() {
        riskEscalations = [
                SELECT Id, Account__c, Account__r.Name, Name, Date_of_Esculation__c, Completion_date__c, ICSM_Owner__c, Status__c,
                        Rejected__c, Escalated_By_Name__c
                FROM At_risk_escalation__c
                WHERE Account__c = :accId
                ORDER BY CreatedDate
        ];
        accName = !riskEscalations.isEmpty()
                ? riskEscalations.get(0).Account__r.Name
                : [SELECT Name FROM Account WHERE Id = :accId LIMIT 1].Name;
    }

    public PageReference createNewEscalation() {
        return new PageReference('/apex/AddAtRiskEscalationByAccount?accId=' + accId);
    }
}