/******************************************************************************************************
* Test Class UserUtilTest
*	Description:
*	Unit test class for UserUtilTest
*
*	ChangeLog:
*
* 	Initial development:
*	20130530	MTM	https://eu1.salesforce.com/a0CD000000YHtiu
******************************************************************************************************/

@isTest (SeeAllData=true)
public class UserUtilTest {
/*
	private static Contact PartnerContact;
	private static User user; 
	
    public testMethod static  void UpdateUserTest() {
    	test.startTest();
        
        Contact[] ctct = [select Id, Email, Name, Account.Name, Partner_Portal_Role__c from Contact where Partner_Portal_Role__c != NULL and Account_Type__c = 'Partner' LIMIT 1];
        
        string roleName = ctct[0].Account.Name + ' Partner User';    	
    	UserUtil.UpdateUser(ctct[0].Id, roleName);        
    	
    	test.stopTest();
    	        
        User [] u = [select Id, UserRoleId, UserRole.Name from User where ContactId = :ctct[0].Id];
        if (u.Size() > 0)
        {
        	System.AssertEquals(roleName, u[0].UserRole.Name);
        }    
    }

    public testMethod static  void UpdateUserTriggerTest() {
    	InitTestData();      
           
        test.startTest();
         		
        UserRole[] roles = [select Name, Id from UserRole where Name = 'Partner Account Partner User'];
        user.UserRoleId = roles[0].Id;
        System.RunAs(new User(Id = UserInfo.getUserId())){                     
        	update user;
        }
        test.stopTest();
        Contact c = [SELECT Id,Name, Partner_Portal_Role__c FROM Contact WHERE Name= 'My Partner ZZZ'];
        System.AssertEquals('Partner User', c.Partner_Portal_Role__c);
        
           
    }
    public testMethod static  void UpdateContactTest() {             
        test.startTest();
        
        Contact[] ctct = [select Id, Email, Name, Account.Name, Partner_Portal_Role__c from Contact where Partner_Portal_Role__c != NULL and Account_Type__c = 'Partner' LIMIT 1];
        string roleName = ctct[0].Account.Name + ' Partner Executive';
    	
        UserRole[] roles = [select Name, Id from UserRole where Name = :roleName];
        System.RunAs(new User(Id = UserInfo.getUserId())){
        	UserUtil.UpdateContact(roles[0].Id, ctct[0].Id);
        }
        test.stopTest();
        
        Contact c = [SELECT Id,Name, Partner_Portal_Role__c FROM Contact WHERE Id = :ctct[0].Id ];
        System.AssertEquals('Partner Executive', c.Partner_Portal_Role__c);
        
           
    }
    
    private static void InitTestData()
    {
        Account testAct = new Account(
            Name = 'Partner Account', 
            Navision_Status__c = 'Partner', 
            QlikTech_Company__c = 'QlikTech UK Ltd'
        );            
        insert testAct;
        
        PartnerContact = new Contact(
			FirstName = 'My Partner',
			LastName = 'ZZZ',
            Email = 'email101@qlikview.com',
			AccountId = testAct.Id           
		);
		insert PartnerContact;
        
        Profile p = [SELECT Id,Name FROM Profile WHERE Name= 'PRM - Base'];
        Contact c = [SELECT Id,Name FROM Contact WHERE Name= 'My Partner ZZZ'];
        
        user = new User(
            Username = 'email101@qlikview.com',
            LastName = 'ZZZ',
            Email = 'email101@qlikview.com', 
            Alias = 'zzz1',
            ContactId = c.Id,            
            CommunityNickname = 'zzz1',
            TimeZoneSidKey = 'Europe/Paris', 
            LocaleSidKey = 'es_ES',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId=p.Id,
            LanguageLocaleKey = 'en_US',
            IsActive=true
        );
        insert user;
    }
*/
}