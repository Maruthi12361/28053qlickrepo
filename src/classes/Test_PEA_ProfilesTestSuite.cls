/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Test_PEA_ProfilesTestSuite 
 * Description:
 * Create unit test for Partner Expertise Area object around Profiles.
 * This will go live on a second changeset on PEA Project after Profiles have been modified manually.
 *
 * Change Log:
 * 2013-07-01   RDZ     Development by RDZ Partner Expertise Project https://eu1.salesforce.com/a59D0000001BRzv PO0220
 *
 * 2013-07-01   RDZ     Adding some more test methods to try and run as other profiles that need only read access to PEA.
 *
 * 2016-01-25   TJG     Fix the error of FIELD_CUSTOM_VALIDATION_EXCEPTION, Account can be owned only by Master Reseller Partners.
 * 2017-04-03   UIN     Test error fix. Commented out all profiles as they are not helping code coverage as this is testing only profiles
 *  09.03.2017 : Bala : fix for query error
 * 2019-02-27   AIN     Fixed test class issue by commenting out code testing specific profiles. Does not lower code coverage.
 */

@isTest
private class Test_PEA_ProfilesTestSuite {

    @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
       }


    static List<String> OpAdminProfiles = new List<String> {
        'Operations Administrator'
        //,'Operations Admin without Manage users'
        //,'Operations Administrator Mint' CAH commented out deleted profile SCU-53
        //,'VP Operations Administrator'  CAH commented out deleted profile SCU-53
                                                            
    };

    //CCE 20131112 Commented out some profiles that were causing test to fail
    static List<String> PEAReadProfiles = new List<String> {'Custom: Alliances Manager'};
    
    public static testMethod void testWithSysAdmin() {
      System.runAs(QTTestUtils.createMockUserForProfile('System Administrator')) {
        // This debug indicates different profiles
        System.debug('ProfileId:' +Userinfo.getProfileId());
        System.assertEquals(true, Account.BillingCity.getDescribe().isUpdateable());
        
      }          
    }
    public static testMethod void testPEA_FLSWithSysAdmin() {
      System.runAs(QTTestUtils.createMockUserForProfile('System Administrator')) {
        // This debug indicates different profiles
        System.debug('ProfileId:' +Userinfo.getProfileId());
        //FSL
        System.assertEquals(true, Partner_Expertise_Area__c.Status__c.getDescribe().isAccessible());
        System.assertEquals(true, Partner_Expertise_Area__c.Dedicated_Expertise_Contact__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Area__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Renewal_Date2__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Marketing_Requirement__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Education_Requirement__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Start_Date__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Partner_Expertise_Fulfilled__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.PartnerExpertiseAreaCreationEmailSent__c.getDescribe().isAccessible());     
        System.assertEquals(true, Partner_Expertise_Area__c.Status__c.getDescribe().isAccessible());
        System.assertEquals(false, Partner_Expertise_Area__c.Status__c.getDescribe().isUpdateable());
        //ObjectAccess
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isDeletable());
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isAccessible());
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isCreateable());

      }          
    } 

    public static testMethod void testPEA_FLSWithCustom_ApiOnlyUser() {
      System.runAs(QTTestUtils.createMockUserForProfile('Custom: Api Only User')) {
        // This debug indicates different profiles
        System.debug('ProfileId:' +Userinfo.getProfileId());
        //FSL
        System.assertEquals(true, Partner_Expertise_Area__c.Status__c.getDescribe().isAccessible());
        System.assertEquals(true, Partner_Expertise_Area__c.Dedicated_Expertise_Contact__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Area__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Renewal_Date2__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Marketing_Requirement__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Education_Requirement__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Start_Date__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.Partner_Expertise_Fulfilled__c.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.PartnerExpertiseAreaCreationEmailSent__c.getDescribe().isAccessible());     
        System.assertEquals(true, Partner_Expertise_Area__c.Status__c.getDescribe().isAccessible());
        System.assertEquals(false, Partner_Expertise_Area__c.Status__c.getDescribe().isUpdateable());
        //ObjectAccess
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isDeletable());
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isUpdateable());
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isAccessible());
        System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isCreateable());

      }          
    } 

                                                            

    public static testMethod void testPEA_FLSWithOperationAdministrator() {
      List<User> uList= QTTestUtils.createMockUsersFromProfileNameList(OpAdminProfiles);
      for(User u : uList)
      {
          System.runAs(u) {
            // This debug indicates different profiles
            System.debug('ProfileId:' +Userinfo.getProfileId());
            //FLS
            System.assertEquals(true, Partner_Expertise_Area__c.Status__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Dedicated_Expertise_Contact__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Area__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Renewal_Date2__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Marketing_Requirement__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Education_Requirement__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Start_Date__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Partner_Expertise_Fulfilled__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.PartnerExpertiseAreaCreationEmailSent__c.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Status__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Dedicated_Expertise_Contact__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Area__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Renewal_Date2__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Marketing_Requirement__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Education_Requirement__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Start_Date__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Partner_Expertise_Fulfilled__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.PartnerExpertiseAreaCreationEmailSent__c.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            //ObjectAccess
            System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isDeletable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isCreateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
          }
      }          
    }    
    
    public static testMethod void testPEA_FLSForInternalUsersJustReadAccess() {
      List<User> userList= QTTestUtils.createMockUsersFromProfileNameList(PEAReadProfiles);
      for (User u : userList)
      {
          System.runAs(u) {
            // This debug indicates different profiles
            System.debug('ProfileId: ' +Userinfo.getProfileId());
            System.debug('ProfileName: ' +u.Profile__c);
            
            
            //FLS
            System.assertEquals(true, Partner_Expertise_Area__c.Status__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Dedicated_Expertise_Contact__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Area__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Renewal_Date2__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Marketing_Requirement__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Education_Requirement__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Expertise_Start_Date__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.Partner_Expertise_Fulfilled__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.PartnerExpertiseAreaCreationEmailSent__c.getDescribe().isAccessible(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            
            
            
            
            System.assertEquals(false, Partner_Expertise_Area__c.Status__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Dedicated_Expertise_Contact__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Expertise_Area__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Expertise_Renewal_Date2__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Expertise_Marketing_Requirement__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Expertise_Education_Requirement__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Expertise_Start_Date__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.Partner_Expertise_Fulfilled__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.PartnerExpertiseAreaCreationEmailSent__c.getDescribe().isUpdateable(),'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            
            //ObjectAccess
            System.assertEquals(false, Partner_Expertise_Area__c.sObjectType.getDescribe().isDeletable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.sObjectType.getDescribe().isUpdateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(true, Partner_Expertise_Area__c.sObjectType.getDescribe().isAccessible(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
            System.assertEquals(false, Partner_Expertise_Area__c.sObjectType.getDescribe().isCreateable(), 'ProfileName:' + u.Profile__c + ' ProfileId:' + u.ProfileId);
    
          }
      }          
    }
      

    /*
    * Test method to check that two partner expertise area of same expertise area cannot be created.
    * We should not allow partner accounts to have 2 partner expertise of Communications.
    * Expertise Areas available are: 
            --None--
            Manufacturing &amp; High Tech
            Retail &amp; Services
            Energy &amp; Utilities
            Communications
            Life Sciences
            Healthcare
            Financial Services
            QlikView Mobile
    * We should have just one PartnerExpertiseArea by Expertise Area Type.
    */
    static testMethod void PEAInsertAsOperationAdministrator() {
        
        test.startTest();
        
        // create test data
        
        
        RecordType PARecordType = [select Id, Name from RecordType 
                                        where Name = 'OEM Partner Account' and sobjecttype='Account'];// fix for query error
        User u = [SELECT Id, Name FROM User where Id= :System.Userinfo.getUserId()];
       
        Account acc = QTTestUtils.createMockAccount('TestAccount', u);
        acc.RecordTypeId = PARecordType.Id;
        update acc;
        
        //Insert Account contact
        Contact c = new Contact();
            c.FirstName = 'Sophie';
            c.LastName = 'Grigson';
            c.accountId = acc.Id;
            c.Email = 'rdztestmockcontact@test.com';
            
            insert c;
            
        System.runAs(QTTestUtils.createMockUserForProfile('Operations Administrator'))
        {
        
            //Check if they can create Solution Profiles
            
            //Check if they can update QVM Products Data
        
        
            Partner_Expertise_Area__c pea0 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Life Sciences');
            
            Partner_Expertise_Area__c pea1 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Communications');
            
            Partner_Expertise_Area__c pea2 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Healthcare');
            
            
            List<SObject> soToInsert = new List<SObject>();
            soToInsert.add(pea0);
            soToInsert.add(pea1);
            soToInsert.add(pea2);
            insert soToInsert;
            
            Partner_Expertise_Area__c pea3 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Communications');
            try{
                insert pea3;
                System.assert(false, 'We expect insert to fail when adding extra EA for Communications on Acc');
            }
            catch(DMLException ex)
            {
                //as we expected
                System.assert(true);
            }
        
        }
        
        
        test.stopTest();
    } 
    
        /*
    * Test method to check that two partner expertise area of same expertise area cannot be created.
    * We should not allow partner accounts to have 2 partner expertise of Communications.
    * Expertise Areas available are: 
            --None--
            Manufacturing &amp; High Tech
            Retail &amp; Services
            Energy &amp; Utilities
            Communications
            Life Sciences
            Healthcare
            Financial Services
            QlikView Mobile
    * We should have just one PartnerExpertiseArea by Expertise Area Type.
    */
    static testmethod void PEAInsertAsPartnerUser() {
        
        // create test data
        
        User u = [SELECT Id, Name FROM User where Id= :System.Userinfo.getUserId()];
       
        Account acc = QTTestUtils.createMockAccount('TestAccount', u);
        
            
        //Get portal user
        Contact c = new Contact();
            c.FirstName = 'Sophie';
            c.LastName = 'Grigson';
            c.accountId = acc.Id;
            c.Email = 'rdztestmockcontact@test.com';
            
            insert c;
            
    
    
        Partner_Expertise_Area__c pea0 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Life Sciences');
        
        Partner_Expertise_Area__c pea1 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Communications');
        
        Partner_Expertise_Area__c pea2 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Healthcare');
        
        
        List<SObject> soToInsert = new List<SObject>();
        soToInsert.add(pea0);
        soToInsert.add(pea1);
        soToInsert.add(pea2);
        insert soToInsert;
        
        Partner_Expertise_Area__c pea3 = new Partner_Expertise_Area__c(Partner_Account_Name__c=acc.Id, Dedicated_Expertise_Contact__c=c.Id, Expertise_Area__c='Communications');
        try{
            insert pea3;
            System.assert(false, 'We expect insert to fail when adding extra EA for Communications on Acc');
        }
        catch(DMLException ex)
        {
            //as we expected
            System.assert(true);
        }
    
        
    }   
    
   /* PRM - Base can't own accounts 
    static testmethod void  PEAInsertAsPartnerUserPRMBASE() {
        
        System.runAs(QTTestUtils.createMockUserForProfile('PRM - Base'))
        {
            PEAInsertAsPartnerUser();
        }
        
    } */
    
    /*static testmethod void  PEAInsertAsPartnerUserIND_QLIKBUY() {
        
        System.runAs(QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy'))
        {
            PEAInsertAsPartnerUser();
        }
        
    }*/
    
    /*
    static testmethod void  PEAInsertAsPartnerUserDEPENDENT_QLIKBUY() {
        
        System.runAs(QTTestUtils.createMockUserForProfile('PRM - Sales Dependent Territory + QlikBuy'))
        {
            PEAInsertAsPartnerUser();
        }
        
    } */


}