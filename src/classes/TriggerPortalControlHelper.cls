/******************************************************
    Trigger: CheckTriggerPortalControl
    Changelog:
        2017-10-05 - AYS - BMW-390/CHG0030491 - !System.isBatch() condition added
        2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
******************************************************/

public with sharing class TriggerPortalControlHelper {
	
	public static void checkPortalControl(List<Contact> triggernew){
		Set<ID> Contacts = new Set<ID>();
    
    	for (Contact contact : triggernew)
    	{
        	if (contact.TriggerPortalControl__c) 
        	{
            	Contacts.Add(contact.Id);
            	contact.TriggerPortalControl__c = false;
        	}   
    	}
     
    	if (Contacts.size() == 0)
        return;
         
    	List<string> Usernames = new List<string>();
    	List<string> Passwords = new List<string>();        
    	for (ULC_Details__c Detail : [select ULCName__c, ULC_Password__c from ULC_Details__c where ContactID__c in :Contacts])
    	{
        	Usernames.Add(Detail.ULCName__c);
        	Passwords.Add(Detail.ULC_Password__c);
    	}
    
    	if (Usernames.size() > 0)
    	{
        	System.debug('CheckTriggerPortalControl: Calling Portal Control ' + Usernames);
        	if(!System.isBatch()){
            ULC.CallPortalControl(Usernames, Passwords);
        	}
        
    	}
	}
}