/**********************************************************
* Name:         SalesLoftTaskTypeHandler
* Object:       Task
* Author:       CRW
* Date:         2020-05-29
* Description:  A new before handler being created for task to work in sync with the FUR changes and update lead status in the before trigger level. TaskSetTypeTest is providing the code coverage.
*               
* Changes Log:
*   Date     	author    CR#    Description
* 2020-06-12    CRW     DGM-245 Adding logic to handle task without type coming from salesloft
* 2020-07-04    CRW     DGM-373 Adding logic to increase call attempt number if type is Tk-Call attempt or TK-call outbound
*************************************************************/
public class SalesLoftTaskTypeHandler {
    public static void handle(List<Task> newSLTasks){
        List<Task> salesloftTasksWithoutType = new List<Task>();
        List<Task> salesloftTasks = new List<Task>();
        system.debug('SL task type handler start');
        for(Task tsk : newSLTasks){
            Id IdRTStandardTasks = Task.SObjectType.getDescribe().getRecordTypeInfosByName().get('Standard Tasks').getRecordTypeId();
            if(tsk.WhoId != null && tsk.RecordTypeId == IdRTStandardTasks && string.IsNotBlank(tsk.SalesLoft1__Call_Disposition__c)){
                if((tsk.Type == 'Select' || tsk.Type == null)){
                    salesloftTasksWithoutType.add(tsk);
                }else{salesloftTasks.add(tsk);}
            }
        }
        system.debug('taskwithtype' + salesloftTasks);
        if(salesloftTasksWithoutType.size() > 0){
            for(Task TK : salesloftTasksWithoutType){
                if(TK.SalesLoft1__Call_Disposition__c == 'Connected' && (TK.SalesLoft1__Call_Sentiment__c == 'Already Working' || TK.SalesLoft1__Call_Sentiment__c == 'Competitor' || TK.SalesLoft1__Call_Sentiment__c == 'Delayed Project' || TK.SalesLoft1__Call_Sentiment__c == 'Left Company' || TK.SalesLoft1__Call_Sentiment__c == 'No Budget' || TK.SalesLoft1__Call_Sentiment__c == 'Student' || TK.SalesLoft1__Call_Sentiment__c == 'Partner/Consultant' || TK.SalesLoft1__Call_Sentiment__c == 'Junk' || TK.SalesLoft1__Call_Sentiment__c == 'No Interest' || TK.SalesLoft1__Call_Sentiment__c == 'Referred to Other Contact' || TK.SalesLoft1__Call_Sentiment__c == 'Rescheduling Interested')){
                    TK.Type = 'TK - Call (Outbound)';
                }else if(TK.SalesLoft1__Call_Disposition__c == 'Rejected' && (TK.SalesLoft1__Call_Sentiment__c == 'Already Working' || TK.SalesLoft1__Call_Sentiment__c == 'Competitor' || TK.SalesLoft1__Call_Sentiment__c == 'Student' || TK.SalesLoft1__Call_Sentiment__c == 'Partner/Consultant' || TK.SalesLoft1__Call_Sentiment__c == 'Junk' || TK.SalesLoft1__Call_Sentiment__c == 'Invalid Jobfunction')){
                    TK.Type = 'Other';
                }else if(TK.SalesLoft1__Call_Sentiment__c == '1-No Contact' && (TK.SalesLoft1__Call_Disposition__c == 'Left Voicemail' || TK.SalesLoft1__Call_Disposition__c == 'No Answer' || TK.SalesLoft1__Call_Disposition__c == 'Not in Service' || TK.SalesLoft1__Call_Disposition__c == 'Wrong Number')){
                    TK.Type = 'TK - Call Attempt';
                }else if(TK.SalesLoft1__Call_Disposition__c == 'Connected' && TK.SalesLoft1__Call_Sentiment__c == 'Demo Scheduled'){
                    TK.Type = 'TK - Demo';
                }else if(TK.SalesLoft1__Call_Disposition__c == 'Connected' && TK.SalesLoft1__Call_Sentiment__c == 'Gatekeeper – No Contact with Prospect'){
                    TK.Type = 'TK - Call Attempt';
                }
                if((TK.Type == 'Select' || TK.Type == null)){
                    if(TK.Subject.startsWith('Call:')){
                        TK.Type = 'TK - Call Attempt';
                    }
                }
                salesloftTasks.add(TK);
            }
        }
        system.debug('taskFinal' + salesloftTasks);
        
        List <Id> lstL = new List<Id>();
        List <Id> lstC = new List<Id>();
        List<Lead> finalLeads = new List<Lead>();
        List<Contact> finalContacts = new List<Contact>();
        Map<Id,sObject> lstWithStatus = new Map<Id,sObject>();
        for(Task tsk : salesloftTasks){
            String whoId = tsk.WhoId;
            String whoIdPrefix = whoId.substring(0, 3);
            if (whoIdPrefix.equalsIgnoreCase('00Q')){
                lstL.add(tsk.WhoId);
            }else if(whoIdPrefix.equalsIgnoreCase('003')){
                lstC.add(tsk.WhoId);
            }
        }system.debug('taskleadcontact' + lstL + ',' + lstC);
        if(lstL.size() > 0){
            for (Lead L : [Select id,status,RecordType.Name,Follow_Up_Rejected_Reason__c,Follow_Up_Disqualified_Reason__c,Number_of_Follow_Up_Voicemails__c From Lead Where id IN :lstL]){
                    lstWithStatus.put(L.id,L);
            }
        }
        if(lstC.size() > 0){
            for (Contact C : [Select id,Contact_Status__c,RecordType.Name,Follow_Up_Rejected_Reason__c,Follow_Up_Disqualified_Reason__c,Number_of_Follow_Up_Voicemails__c From Contact Where id IN :lstC]){
                    lstWithStatus.put(C.id,C);
            }
        }
        system.debug('taskmap12' + lstWithStatus);
        for (Task tsk : salesloftTasks){
            String LStatusOLD;
            String LStatusNEW;
            String disqualifiedReason;
            String rejectReason;
            String recordType;
            Date cadenceDueDate;
            Boolean Attempt = false;
            Lead l;
            Contact c;
            system.debug('task1' + tsk);

            String whoId = tsk.WhoId;
            if (lstWithStatus.containsKey(whoId)){
                sObject sObj = lstWithStatus.get(whoId);
                if (sObj.getSObjectType() == Schema.Lead.sObjectType){
                    l = (Lead)sObj;
                    LStatusOLD = l.status;
                    recordType = l.RecordType.Name;
                }else if(sObj.getSObjectType() == Schema.Contact.sObjectType){
                    c = (Contact)sObj;
                    LStatusOLD = c.Contact_Status__c;
                    recordType = c.RecordType.Name;
                }
                if((LStatusOLD == 'Follow-Up Required' || LStatusOLD == 'Follow-Up Attempt 1' || LStatusOLD == 'Follow-Up Attempt 2' || LStatusOLD == 'Follow-Up Attempt 3' || LStatusOLD == 'Follow-Up Attempt 4' || LStatusOLD == 'Follow-Up Attempt 5') && (recordType == 'Standard lead' || recordType == 'Business Contact')){
                    
                    system.debug('call type' + tsk.Type + 'dips' + tsk.SalesLoft1__Call_Disposition__c);
                    if(tsk.Type == 'TK - Call (Outbound)' && tsk.SalesLoft1__Call_Disposition__c == 'Connected' && tsk.SalesLoft1__Call_Sentiment__c == 'Rescheduling Interested'){
                        LStatusNEW = 'Contacted-Additional Work Required';
                        Attempt = true;
                    }else if(tsk.Type == 'TK - Call (Outbound)' && tsk.SalesLoft1__Call_Disposition__c == 'Connected'){
                        if(tsk.SalesLoft1__Call_Sentiment__c == 'Already Working' || tsk.SalesLoft1__Call_Sentiment__c == 'Competitor' || tsk.SalesLoft1__Call_Sentiment__c == 'Delayed Project' || tsk.SalesLoft1__Call_Sentiment__c == 'Left Company' || tsk.SalesLoft1__Call_Sentiment__c == 'No Budget' 
                           || tsk.SalesLoft1__Call_Sentiment__c == 'Student' || tsk.SalesLoft1__Call_Sentiment__c == 'Partner/Consultant' || tsk.SalesLoft1__Call_Sentiment__c == 'Junk' || tsk.SalesLoft1__Call_Sentiment__c == 'No Interest'){
                               LStatusNEW = 'Follow-Up Disqualified';
                               disqualifiedReason = tsk.SalesLoft1__Call_Sentiment__c;
                               Attempt = true;
                           }else if(tsk.SalesLoft1__Call_Sentiment__c == 'Referred to Other Contact'){
                               LStatusNEW = 'Follow-Up Disqualified';
                               disqualifiedReason = 'No Interest';
                               Attempt = true;
                           }
                    }else if(tsk.SalesLoft1__Call_Disposition__c == 'Rejected' && tsk.SalesLoft1__Call_Sentiment__c == 'Invalid Jobfunction'){
                        LStatusNEW = 'Contacted-Additional Work Required';
                    }else if(tsk.Type == 'Other' && tsk.SalesLoft1__Call_Disposition__c == 'Rejected'){
                        if(tsk.SalesLoft1__Call_Sentiment__c == 'Already Working' || tsk.SalesLoft1__Call_Sentiment__c == 'Competitor' || tsk.SalesLoft1__Call_Sentiment__c == 'Student' || tsk.SalesLoft1__Call_Sentiment__c == 'Partner/Consultant' || tsk.SalesLoft1__Call_Sentiment__c == 'Junk'){
                            LStatusNEW = 'Follow-Up Rejected';
                            rejectReason = tsk.SalesLoft1__Call_Sentiment__c;
                        }
                    }else if(tsk.Type == 'TK - Demo' && tsk.SalesLoft1__Call_Disposition__c == 'Connected' && tsk.SalesLoft1__Call_Sentiment__c == 'Demo Scheduled'){
                        LStatusNEW = 'Contacted-Additional Work Required';
                    }else if(tsk.Type == 'TK - Call Attempt' && tsk.SalesLoft1__Call_Disposition__c == 'Junk' && tsk.SalesLoft1__Call_Sentiment__c == 'Junk'){
                        LStatusNEW = 'Follow-Up Disqualified';
                        disqualifiedReason = tsk.SalesLoft1__Call_Sentiment__c;
                        Attempt = true;
                    }
                    
                    if(l != null){
                        if(string.isNotBlank(LStatusNEW)){
                            l.status = LStatusNEW;
                            if (string.isNotBlank(disqualifiedReason)){
                                l.Follow_Up_Disqualified_Reason__c = disqualifiedReason;
                            }else if (string.isNotBlank(rejectReason)){
                                l.Follow_Up_Rejected_Reason__c = rejectReason;
                            }
                            if(Attempt){
                                if (l.Number_of_Follow_Up_Voicemails__c == null) {l.Number_of_Follow_Up_Voicemails__c = 0;}
                                l.Number_of_Follow_Up_Voicemails__c = l.Number_of_Follow_Up_Voicemails__c + 1;
                            }
                            finalLeads.add(l);
                        }
                    }else if(c != null){
                        if(string.isNotBlank(LStatusNEW)){
                            c.Contact_Status__c = LStatusNEW;
                            if (string.isNotBlank(disqualifiedReason)){
                                c.Follow_Up_Disqualified_Reason__c = disqualifiedReason;
                            }else if (string.isNotBlank(rejectReason)){
                                c.Follow_Up_Rejected_Reason__c = rejectReason;
                            }
                            if(Attempt){
                                if (c.Number_of_Follow_Up_Voicemails__c == null) {c.Number_of_Follow_Up_Voicemails__c = 0;}
                                c.Number_of_Follow_Up_Voicemails__c = c.Number_of_Follow_Up_Voicemails__c + 1;
                            }
                            finalContacts.add(c);
                        }
                    }
                }
            }
        }
        system.debug('lead/contact at end' + finalLeads + ',' + finalContacts);
        if(finalLeads.size() > 0){update finalLeads;}
        if(finalContacts.size() > 0){update finalContacts;}
    }

}