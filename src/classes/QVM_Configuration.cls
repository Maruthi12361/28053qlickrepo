/*
@author: Anthony Victorio, Model Metrics
@date: 03/26/2012
@description: Controller for the QlikMarket configuration page
*/
public with sharing class QVM_Configuration {
	
	public QVM_Settings__c settings {get;set;}
	
	public QVM_Configuration() {
		
		settings = (QVM_Settings__c.getOrgDefaults() == null) ? new QVM_Settings__c() : QVM_Settings__c.getOrgDefaults();
        
	}
	
	public void loadDefaults() {
		
		settings.QlikMarket_Website__c = 'http://qlikmarket-dev.qlikview.com/';
        settings.Pagination_After_Row__c = 25;
        settings.Max_CP_Rows__c = 5;
        
        settings.htaccess_Required__c = true;
        settings.htaccess_username__c = 'qlikview';
        settings.htaccess_password__c = 'qlikview100';
        
        settings.Magento_Endpoint__c = 'qlikmarket-dev.qlikview.com/index.php/api/inchoo/rest';
        settings.Magento_API_Key__c = 'qlikKEY1';
        settings.Magento_Username__c = 'qlikapi';
        
        settings.S3_Endpoint__c = 'http://s3.amazonaws.com/doc/2006-03-01/';
        settings.S3_Bucket__c = 'qlikview-market-dev';
        settings.S3_Key__c = 'AKIAJZB45BFTFO4AMS5Q';
        settings.S3_Secret__c = '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';
        
        settings.SQS_Endpoint__c = 'sqs.us-east-1.amazonaws.com';
        settings.SQS_Thumbnail_Queue__c = 'QVMThumbnail_SBX';
        settings.Thumbnail_Height__c = 77;
        settings.Thumbnail_Width__c = 77;
		
		settings.Default_Case_Origin__c = 'QlikMarket';
		settings.Default_Case_Priority__c = 'Medium / Low';
		settings.Default_Case_Record_Type__c = 'QlikMarket Case Record Type';
		settings.Default_Case_Status__c = 'New';
		
		settings.Default_Lead_Record_Type__c = 'Partner Opp Reg';
		settings.Default_Lead_Source__c = 'PART - Partner';
		settings.Default_Lead_Status__c = 'Raw - Prospect';
		
		try {
            upsert settings;
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
		
	}
	
	public void saveSettings() {
		try {
			upsert settings;
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		
	}

}