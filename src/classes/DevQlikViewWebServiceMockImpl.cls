@isTest
global class DevQlikViewWebServiceMockImpl implements WebServiceMock {

    // MockUp web service call
    
    global void doInvoke(
        object stub,
        object request,
        map<String,object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType)
    {
        system.debug('OBJECT request = ' + request);
        system.debug('OBJECT requestName = ' + requestName);
        system.debug('OBJECT responseName = ' + responseName);
        system.debug('OBJECT responseType = ' + responseType);
        
        if (requestName == 'InitializeApexTriggerRequest') {
            system.debug('PATH: InitializeApexTriggerRequest');
            // Add response element to the response parameter, as follows:
            response.put('response_x', new devQlikViewCom.InitializeApexTriggerResponse_element());
        }
    }
}