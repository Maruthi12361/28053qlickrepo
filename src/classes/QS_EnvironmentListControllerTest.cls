/*********************************
*
*   2109-04-16 AIN Test class for QS_EnvironmentListController
*   2019-05-05 AIN IT-1597 Created for Support Portal Redesign
*
*********************************/
@isTest
private class QS_EnvironmentListControllerTest {
    
    static testMethod void test() {
        Account a = new Account();
        a.Name = 'Test account';
        insert a;

        List<Environment__c> environments = new List<Environment__c>();
        Environment__c e1 = new Environment__c();
        e1.Account__c = a.Id;
        e1.Type__c = 'Production';

        Environment__c e2 = new Environment__c();
        e2.Account__c = a.Id;
        e2.Type__c = 'Test';

        Environment__c e3 = new Environment__c();
        e3.Account__c = a.Id;
        e3.Type__c = 'Development';

        environments.add(e1);
        environments.add(e2);
        environments.add(e3);
        insert environments;

        QS_EnvironmentListController controller = new QS_EnvironmentListController();
        controller = new QS_EnvironmentListController();
        controller.showForAccount = a.id;
        controller.showSelectionOption = true;
        controller.selectedEnvironment = e1;
        controller.selectedEnvironmentId = e1.id;
        controller.setEnvironments();
        controller.getShowAccountColumn();
        controller.processSelected();
    }
    
}