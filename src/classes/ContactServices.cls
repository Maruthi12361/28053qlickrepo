/******************************************************

	Class: ContactServices
	
	This class is a utlilty class to help with certain 
	contact related functionallity.
	
	Changelog:
		201x-xx-xx	xxx		Created file by External Consultant
		2011-11-30	MHG		#QUICK - Support ClosedCaseSurvey trigger
							CR #3619: https://eu1.salesforce.com/a0CD000000FyqZD
		2012-01-13  RDZ     #QUICK - Duplicate Email Addresses on ToAddresses
							CR #3870: https://eu1.salesforce.com/a0CD000000Fzsji
												
		2012-06-08  FLD		CR# 4882: code restructure, send template for different countries
		2014-03-30  MTM     CR 11399 Trigger and ULC change to handle @qlik.com email addresses
		2014-06-17	CCE		CR# 12696 https://eu1.salesforce.com/a0CD000000ibY16 support Swedish CaseClosedSendSurvey										
		2019-05-30  extbad  IT-1874 include 2 letter Preferred Language
******************************************************/
public class ContactServices {

	public static void sendEmail(List<Contact> contacts, List<Case> cases) {
	    if (!Test.isRunningTest()) {
			Messaging.sendEmail(createEmails(contacts, cases));
		}
    }
    
    public static List<Messaging.SingleEmailMessage> createEmails(List<Contact> contacts, List<Case> cases) {
    	QTCustomSettings__c settings = QTCustomSettings__c.getValues('Default');
		if (settings != null) {
			printDebugMessages(settings);
		}
		
    	String SupportSurveyFeedbackEmailAddress = settings.Support_Feedback_Email_address__c;
    	List<OrgWideEmailAddress> emails = [select id, Address, DisplayName from OrgWideEmailAddress 
											where Address = :SupportSurveyFeedbackEmailAddress limit 1];
		Id orgWideId = emails.size() > 0 ? emails[0].Id : null;				
				
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		for (Contact c : contacts) {
			contactMap.put(c.Id, c);
		}
		
		Map<String, Id> templates = createTemplateMap(settings);
		
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		for (Case c : cases) {
			
			Contact contact = contactMap.get(c.ContactId);
			if (contact == null) {
				continue;
			}
									
			Messaging.SingleEmailMessage mailMsg = new Messaging.SingleEmailMessage();
			if (orgWideId == null)	{
				System.debug('====> No Org Wide Email found');
				mailMsg.setReplyTo(SupportSurveyFeedbackEmailAddress);
	        	//mailMsg.setReplyTo('supportfeedback@qlik.com');
	        	mailMsg.setSenderDisplayName('Qlik Support');
			} else {
				mailMsg.setOrgWideEmailAddressId(orgWideId);
			}	
			
			Id templateId = templates.get(contact.Preferred_Language__c);
			if (templateId == null) {
				templateId = settings.Support_Survey_Template_Default__c;
			}
			mailMsg.setTemplateId(templateId);
				        
			//RDZ: setToAddresses should not be set if an object is added to the email
	 		//	   as object related (mailMsg.setWhatId(tempCase.Id); mailMsg.setTargetObjectId(ctct.Id)	 		 
			//mailMsg.setToAddresses(new List<String>{ctct.Email});
			
	        mailMsg.setWhatId(c.Id);
	        mailMsg.setTargetObjectId(contact.Id);
	      
			mails.add(mailMsg);
		}
		
		return mails;
    }
    
    private static Map<String, Id> createTemplateMap(QTCustomSettings__c settings) {
    	return new Map<String, Id>{
    		'English' => settings.Support_Survey_Template_Default__c,
			'en' => settings.Support_Survey_Template_Default__c,
			'French' => settings.Support_Survey_Template_French__c,
			'fr' => settings.Support_Survey_Template_French__c,
			'German' => settings.Support_Survey_Template_German__c,
			'de' => settings.Support_Survey_Template_German__c,
			'Japanese' => settings.Support_Survey_Template_Japan__c,
			'ja' => settings.Support_Survey_Template_Japan__c,
			'Spanish' => settings.Support_Survey_Template_Spanish__c,
			'es' => settings.Support_Survey_Template_Spanish__c,
			'Swedish' => settings.Support_Survey_Template_Swedish__c,
			'sv' => settings.Support_Survey_Template_Swedish__c	//CCE CR# 12696
		};
    }
    
    private static void printDebugMessages(QTCustomSettings__c settings) {    	
		if (settings.Support_Survey_Template_Default__c == null) {
			System.debug('No GeneralTemplate for CaseService.sendEmail could be found!');
		}
		if (settings.Support_Survey_Template_Japan__c == null) {
			System.debug('No TemplateJapan for CaseService.sendEmail could be found!');
		}
		if (settings.Support_Feedback_Email_address__c == null) {
			System.debug('No Support Survey feedback email address for ContactServices.createEmails could be found!');
		}
    }    
    
}