public class CaseEvent
{
	public Datetime eventDate {get; set;}
	public String eventDateFormatted
	{
		get
		{
			if(eventDate == null)
				return '';
			else
				return eventDate.format('dd/MM/yyyy HH:mm');
		}
	}
	public String eventType {get; set;}
	public String userName {get; set;}
	public String action {get; set;}
	public static CaseEvent[] sortByDate(CaseEvent[] ces, Boolean isAscending)
	{
		CaseEvent[] resultList = new CaseEvent[0];
		//Create a map of date to CaseEvent collection
		Map<Datetime, CaseEvent[]> dateToCEMap = new Map<Datetime, CaseEvent[]>();
		for(CaseEvent ce : ces)
		{
			if(dateToCEMap.containsKey(ce.eventDate))
			{
				dateToCEMap.get(ce.eventDate).add(ce);
			}	
			else
			{
				dateToCEMap.put(ce.eventDate, new CaseEvent[] {ce});
			}	
		}
		Datetime[] keys = new List<Datetime>(dateToCEMap.keySet());
		keys.sort();
		if(isAscending)
		{
			for(Datetime key : keys)
			{ 
				resultList.addAll(dateToCEMap.get(key));
			}
		}	
		else
		{
			for(Integer i = (keys.size() - 1); i >= 0; i--) 
			{
				resultList.addAll(dateToCEMap.get(keys[i]));
			}
		}		
		return resultList;
	}
}