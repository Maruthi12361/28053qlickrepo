/**     
 * File Name : Z_RenewalOpportunityBatchableTest
 * Description : This is a test class to test the Z_RenewalOpportunityBatchable class with following test methods:
 *               - renewalOpportunityBatchableTest
 *               - renewalOpportunityBatchableNegativeNoPreviousOppsTest
 *               - queryPreviousOpportunitiesTest
 *               - queryPreviousOpportunitiesNegativeTest
 *               - renewalOpportunityCreationTest
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

@IsTest
public class Z_RenewalOpportunityBatchableTest {

    private static Account testAcc;

    private static Contact testContact;

    private static Zuora__CustomerAccount__c testCustomerAcc;

    private static Opportunity testOppty;

    private static Zuora__Subscription__c testSubscription;

    private static zqu__Quote__c testQuote;

    private static QlikTech_Company__c testqtComp;

    private static Q2CWSettings__c testQ2cwSettings;

    private static Steelbrick_Settings__c testSteelbrickSettings;

    private static Renewal_Automation_Settings__c testRASettings;

    private static Apex_Debug_Log__c testApexDebugLog;

    static void setup() {
        testQ2cwSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();
        testRASettings = Z_RenewalTestFactory.settings;
        testApexDebugLog = Z_RenewalTestFactory.makeApexDebugLogForRenewalOpp();

        testqtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');

        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOppty = Z_RenewalTestFactory.makeRenewalOpportunity(testAcc);
        testSubscription = Z_RenewalTestFactory.makeSubscription();

        testAcc.QlikTech_Company__c = testqtComp.name;
        testAcc.Billing_Country_Code__c  = testqtComp.id;

        List<sObject> tests = new List<sObject>{
            testQ2cwSettings, testSteelbrickSettings, testRASettings, testApexDebugLog
        };

        insert tests;

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOppty, testSubscription
        };

        insert testObjects;

        List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();

        for (Integer i = 0; i < 5; i++) {
            ocrList.add(Z_RenewalTestFactory.makeOCR(testContact, testOppty));
        }

        insert ocrList;

        testQuote = Z_RenewalTestFactory.makeQuote(testAcc, testContact, testOppty, testCustomerAcc);

        System.debug('IH: quote id = ' + testQuote.Id);

        Organization_Info__c orgInfo = new Organization_Info__c(isSandbox__c=true);
        insert orgInfo;

        orgInfo.CreatedInOrgId__c = orgInfo.Id;
        update orgInfo;
    }

    static void negativeSetup() {
        testQ2cwSettings = Z_RenewalTestFactory.makeQ2CWSettings();
        testSteelbrickSettings = Z_RenewalTestFactory.makeSteelbrickSettings();
        testRASettings = Z_RenewalTestFactory.settings;

        testqtComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');

        testAcc = Z_RenewalTestFactory.makeAccount();
        testContact = Z_RenewalTestFactory.makeContact(testAcc);
        testCustomerAcc = Z_RenewalTestFactory.makeBillingAccount(testAcc);
        testOppty = Z_RenewalTestFactory.makeRenewalOpportunity(testAcc);
        testSubscription = Z_RenewalTestFactory.makeSubscription();

        testAcc.QlikTech_Company__c = testqtComp.name;
        testAcc.Billing_Country_Code__c  = testqtComp.id;

        List<sObject> tests = new List<sObject>{
            testQ2cwSettings, testSteelbrickSettings, testRASettings
        };

        insert tests;

        testOppty.Next_Renewal_Opportunity__c = 'non existing oppty';

        List<sObject> testObjects = new List<sObject>{
            testContact, testCustomerAcc, testOppty, testSubscription
        };

        insert testObjects;

        List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();

        for (Integer i = 0; i < 5; i++) {
            ocrList.add(Z_RenewalTestFactory.makeOCR(testContact, testOppty));
        }

        insert ocrList;

        testQuote = Z_RenewalTestFactory.makeQuote(testAcc, testContact, testOppty, testCustomerAcc);

        System.debug('IH: quote id = ' + testQuote.Id);

        Organization_Info__c orgInfo = new Organization_Info__c(isSandbox__c=true);
        insert orgInfo;

        orgInfo.CreatedInOrgId__c = orgInfo.Id;
        update orgInfo;
    }

    @IsTest
    static void renewalOpportunityBatchableTest() {
        Semaphores.SetAllSemaphoresToTrue();
        
        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;

        setup();

        Test.startTest();

        Renewal_Automation_Settings__c settings = Renewal_Automation_Settings__c.getOrgDefaults();

        Map<String, Map<String, Object>> subscriptionFieldsMapTest = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();
        Integer batchSize = (Integer) settings.OpportunityBatchSize__c;
        Set<String> excludePreviousOpportunityIds = new Set<String>();

        Z_RenewalOpportunityBatchable batchable = new Z_RenewalOpportunityBatchable(subscriptionFieldsMapTest, excludePreviousOpportunityIds);

        // this is here because without it, the batchable will not recognize the subscriptionNumbers variable
        // in the query string, see Z_RenewalOpportunityBatchable.start() method
        Set<String> subscriptionNumbers = subscriptionFieldsMapTest.keySet();

        Database.executeBatch(batchable, batchSize);

        Database.QueryLocator ql = batchable.start(null);

        List<zqu__Quote__c> quotes = Database.query(ql.getQuery());

        batchable.execute(null, quotes);
        batchable.finish(null);

        System.assertEquals(batchable.insertRenewalOpptyResults.size(), 1);
        System.assertEquals(batchable.insertOpptyCRResults.size(), 5);
        System.assertEquals(batchable.updateBaseOpptyResults.size(), 1);

        Test.stopTest();
    }

    @IsTest
    static void renewalOpportunityBatchableNegativeNoPreviousOppsTest() {
        Semaphores.SetAllSemaphoresToTrue();
        
        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;

        negativeSetup();

        Test.startTest();

        Renewal_Automation_Settings__c settings = Renewal_Automation_Settings__c.getOrgDefaults();

        Map<String, Map<String, Object>> subscriptionFieldsMapTest = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();
        Integer batchSize = (Integer) settings.OpportunityBatchSize__c;
        Set<String> excludePreviousOpportunityIds = new Set<String>();

        Z_RenewalOpportunityBatchable batchable = new Z_RenewalOpportunityBatchable(subscriptionFieldsMapTest, excludePreviousOpportunityIds);

        Database.executeBatch(batchable, batchSize);

        System.assertEquals(batchable.insertRenewalOpptyResults.size(), 0);
        System.assertEquals(batchable.insertOpptyCRResults.size(), 0);
        System.assertEquals(batchable.updateBaseOpptyResults.size(), 0);

        Test.stopTest();
    }

    @IsTest
    static void queryPreviousOpportunitiesTest() {
        Semaphores.SetAllSemaphoresToTrue();

        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;

        setup();

        Test.startTest();

        Map<String, Map<String, Object>> subscriptionFieldsMapTest = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();

        Set<String> excludePreviousOpportunityIds = new Set<String>();

        Z_RenewalOpportunityBatchable batchable = new Z_RenewalOpportunityBatchable(subscriptionFieldsMapTest, excludePreviousOpportunityIds);

        List<zqu__Quote__c> previousQuotes = new List<zqu__Quote__c> { testQuote };
        Map<Id, Opportunity> previousOpportunities = batchable.queryAndStorePreviousOpportunities(previousQuotes);

        System.assertEquals(previousOpportunities.size(), 1);

        for (Opportunity previousOpp : previousOpportunities.values()) {
            System.assertEquals(previousOpp.Type, 'New Customer');
            System.assertEquals(previousOpp.CurrencyIsoCode, 'USD');
            System.assertEquals(previousOpp.Revenue_Type__c, 'Direct');
            System.assertEquals(previousOpp.Consultancy_Total_Cost__c, 0.00);
            System.assertEquals(previousOpp.Referral__c, false);
            System.assertEquals(previousOpp.Quote_Price__c, 0.00);
            System.assertEquals(previousOpp.Subscription_Start_Date__c, Date.today());
            System.assertEquals(previousOpp.EIS_Credited__c, false);
        }

        Test.stopTest();
    }

    @IsTest
    static void queryPreviousOpportunitiesNegativeTest() {
        Semaphores.SetAllSemaphoresToTrue();

        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;

        negativeSetup();

        Test.startTest();

        Map<String, Map<String, Object>> subscriptionFieldsMapTest = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();

        Set<String> excludePreviousOpportunityIds = new Set<String>();

        Z_RenewalOpportunityBatchable batchable = new Z_RenewalOpportunityBatchable(subscriptionFieldsMapTest, excludePreviousOpportunityIds);
        Map<Id, Opportunity> previousOpportunities = batchable.queryAndStorePreviousOpportunities(new List<zqu__Quote__c>());

        System.assertEquals(previousOpportunities.size(), 0);

        Test.stopTest();
    }

    @IsTest
    static void renewalOpportunityCreationTest() {
        Semaphores.SetAllSemaphoresToTrue();

        Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger = false;
        Id renewalOppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription Renewal').getRecordTypeId();

        setup();

        Test.startTest();

        Map<String, Map<String, Object>> subscriptionFieldsMapTest = Z_RenewalAutomationUtility.getSubscriptionFieldsMap();

        Set<String> excludePreviousOpportunityIds = new Set<String>();

        Z_RenewalOpportunityBatchable batchable = new Z_RenewalOpportunityBatchable(subscriptionFieldsMapTest, excludePreviousOpportunityIds);
        
        List<zqu__Quote__c> previousQuotes = new List<zqu__Quote__c> { testQuote };
        Map<Id, Opportunity> previousOpportunities = batchable.queryAndStorePreviousOpportunities(previousQuotes);
        List<Opportunity> renewalOpportunities = batchable.createRenewalOpportunities(previousOpportunities);

        System.assertEquals(renewalOpportunities.size(), 1);

        for (Opportunity renewalOpp : renewalOpportunities) {
            System.assertEquals(renewalOpp.CloseDate, Date.today());
            System.assertEquals(renewalOpp.ForecastCategoryName, 'Omitted');
            System.assertEquals(renewalOpp.Name, 'Renewal Opportunity 12345');
            System.assertEquals(renewalOpp.RecordTypeId, renewalOppRecordTypeId);
            System.assertEquals(renewalOpp.StageName, 'Renewal Anticipated');
            // System.assertEquals(renewalOpp.Carve_out_Amount__c, null);
            // System.assertEquals(renewalOpp.Carve_out_Reason__c, null);
            // System.assertEquals(renewalOpp.Closed_date_for_conga__c, null);
            // System.assertEquals(renewalOpp.Finance_Approved__c, false);
            // System.assertEquals(renewalOpp.Finance_Comments__c, null);
            // System.assertEquals(renewalOpp.Included_Products__c, null);
            // System.assertEquals(renewalOpp.Maintenance_Carve_Out__c, null);
            // System.assertEquals(renewalOpp.Maintenance_Revenue_Recognition__c, null);
            // System.assertEquals(renewalOpp.Navision_Invoice_Number__c, null);
            // System.assertEquals(renewalOpp.NextStep, null);
            // System.assertEquals(renewalOpp.Next_Step_custom__c, null);
            System.assertEquals(renewalOpp.Opportunity_Source__c, 'Qlik Created');
            System.assertEquals(renewalOpp.Primary_Zuora_Quote_OF_Status__c, 'None');
            // System.assertEquals(renewalOpp.Product_Category__c, null);
            System.assertEquals(renewalOpp.Subscription_Forecast_ACV_Amount__c, 12345);
            System.assertEquals(renewalOpp.Short_Description__c, 'Renewal Opportunity 12345');
            // System.assertEquals(renewalOpp.Zuora_Quote_Number__c, null);
            System.assertEquals(renewalOpp.Zuora_Subscription_Number__c, '12345');
        }

        Test.stopTest();
    }
}