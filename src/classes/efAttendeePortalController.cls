//*********************************************************/
// Author: Mark Cane&
// Creation date: 20/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efAttendeePortalController{
	private efRegistrationManager regManager;
	private User user;
	private PageReference pr;
	private List<efAnnouncementWrapper> announcements = new List<efAnnouncementWrapper>();
	private efAppNavigator navigator;
	private String eventBannerUrl;
	private efActivityUtil activity;		    
    private List<efClassWrapper> classesByGroupVar;
    private List<efProductWrapper> activitiesList;
    private string contactId;
		
    private String Message = '' ;
    private boolean validatedOnActivityPage = false;

	public efAttendeePortalController(){
		if(user==null){
            try{
                initialize();
            } catch(Exception e){}
        }
	}
	
	public void initialize(){
    	// Comment & : validate input registration Id.
    	String registrationId = ApexPages.currentPage().getParameters().get('registrationId');    	    	
    	contactId = efUtility.getContactIdFromRegistration(registrationId);
    	pr = checkInitialState();    	   	
    	if (pr!=null) return;
    	
    	// Comment& : efRegistrationManager creates and holds 
    	// efRegistrationWrapper, efContactWrapper instances.     	
        regManager = new efRegistrationManager(contactId, registrationId);
        
        // Comment& : efAppNavigator encapsulates page navigation logic.
        navigator = new efAppNavigator(regManager.getEventWrapper(), registrationId);
    	navigator.setCurrentPage(efAppNavigator.ATTENDEE_PORTAL_HOME);
    	
        List<Announcement__c> listAnn = [Select a.Announcement_text__c,
                                        		a.LastModifiedDate,
                                                a.Name
                                         From Announcement__c a
                                         Where Event__c=:regManager.getEventWrapper().getEvent().Id And
                                             	Active__c = true And
                                             	Valid_Attendee_Types__c includes (:regManager.getFinalUserType())
                                             	Order By LastModifiedDate Desc Limit 5 ];
        if (listAnn.Size() >0){
            for (Integer i=0 ;i< listAnn.Size();i++){
                efAnnouncementWrapper a = new efAnnouncementWrapper(listAnn[i]);
                Announcements.add(a);
            }
        }
        
        activity = new efActivityUtil(regManager);
        classesByGroupVar = null;
    	activitiesList = null;
	}	
	
	public PageReference checkInitialState(){
    	try {
    		String regId = ApexPages.currentPage().getParameters().get('registrationId');
    		if (regId==null || regId.length()==0) return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    		if (contactId == null) efAppNavigator.pr(efAppNavigator.EVENTS_HOME);    
    		return efRegistrationManager.checkRegistrationIdPortal(regId);
    	} catch (Exception e){
    		return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    	}
    }
    
   	public PageReference checkState(){
        if (pr!=null)
        	return pr;
        else
        	return null;
    }
	// End initialisation.


	// Page furniture methods.	
    public String getEventBannerUrl(){    	
    	if (eventBannerUrl==null || eventBannerUrl.length()==0){
    		eventBannerUrl = getDocumentUrl(regManager.getEventWrapper().getEvent().Banner_Image_Document__c);
    	}
    	return eventBannerUrl;
    }
    
    private String getDocumentUrl(String documentUniqueName){
    	Document doc = [Select d.Id From Document d Where d.DeveloperName=:documentUniqueName Limit 1];
    	String imageid = doc.id; 
    	imageid = imageid.substring(0,15);
    	return '/servlet/servlet.FileDownload?file=' + imageid;
    }
    
    public String getAttendeeLabelActivityIntro(){
    	if (regManager==null) return '';
    	return regManager.getEventWrapper().getAttendeeLabelActivityIntro();
    }

    public String getEventName(){
    	return regManager.getEventWrapper().getEventName();
    }
    
    //End furniture methods.
    
    
    
    // Portal methods.
    
    public Boolean getShowSessions(){
    	if (regManager==null)
    		return false;
    	else
    		return regManager.getEventWrapper().getShowSessions();
    }
    
    public Boolean getShowActivities(){
    	if (regManager==null)
    		return false;
    	else    	
    		return regManager.getEventWrapper().getShowActivities();
    }
    
    public String getRegistrationId(){
    	if (regManager==null)
    		return '';
    	else    	
    		return regManager.getRegistrationWrapper().getId();
    }
    
    public Boolean hasClassesByGroup {get;set;}
    
    public List<efClassWrapper> getClassesByGroup(){
    	if (classesByGroupVar==null){
    		classesByGroupVar = efActivityUtil.getClassesByGroupCalendar(getTrainingAndActivities());
    	}    	
    	hasClassesByGroup = classesByGroupVar.size()>0 ? true : false;
    	return classesByGroupVar;
    }

    public List<efProductWrapper> getTrainingAndActivities(){
    	if (regManager==null)
    		return null;
    	else {   
    		if (activitiesList==null){
    			activitiesList = efActivityUtil.joinTrainingAndActivities(
									null, //Don't Show paid trainings
									efActivityUtil.getScheduledActivities(regManager.getRegistrationWrapper().getId()));
    		}
    		hasClassesByGroup = activitiesList.size()>0 ? true : false;	
    		return activitiesList;
    	}
    }
	// End portal methods.


	// Obsolete nmethods.
	
    public boolean isValidActivityData(){
        validatedOnActivityPage = true;
        return false;
    }
    
    public boolean getValidatedOnActivityPage(){ 
        return this.validatedOnActivityPage;
    }    
    
    public efActivityUtil getActivity(){
        return activity;
    }
	/*
    public String getUserId(){
        return UserInfo.getUserId();
    }
    */

    public efRegistrationWrapper getAttendeeRegistration(){
        return regManager.getRegistrationWrapper();
    }

    public PageReference goHome(){               
      PageReference p = new PageReference('/apex/homepage');
      p.setRedirect(true);
      return p;
    }    
 
    public Boolean getHasAnnouncements(){
        return announcements != null && announcements.size() > 0;
    }
    
    public List<efAnnouncementWrapper> getAnnouncements(){
        return this.Announcements ;
    }
   
    public String getMessage(){
        return this.Message ;
    }

    public efSessionWrapper[] getActivities(){
        if(activity!=null)
            return activity.getActivities();
        return null;
    }
    
    public void setActivities(efSessionWrapper[] s){
        if(activity!=null)
            activity.setActivities(s);  
    }
     
    public PageReference addActivities(){
        if (activity!=null){
            if (!activity.getIsValidActivities(regManager.getRegistrationWrapper())){
                return null;
            }            
            activity.addActivities();
        }        
        return null;
    }

    public PageReference removeActivities(){
		List<Id> sesslistId = new List<Id>();
		for(efClassWrapper cg : classesByGroupVar){
			for(efProductWrapper p : cg.getGroupClasses()){
				if(p.getIsSelected())
					sesslistId.add(p.getProductCode());
			}
		}
		List<Session__c> sesslist = new List<Session__c>();		
        List<Session_Attendance__c> slist = [Select s.Id,
        											s.Status__c,
                                                    s.Session__c,
                                                    s.Session__r.Used__c
                                             From Session_Attendance__c s
                                             Where s.Registration__c = :regManager.getRegistrationWrapper().getId() And
                                             s.Session__c In :sesslistId And
                                             s.Status__c = 'Registered'];		
		for(Session_Attendance__c sa : slist){
			Double used = sa.Session__r.Used__c;
            Session__c objs = new Session__c(ID=sa.Session__c,Used__c = used-1);
            sesslist.add(objs);
            sa.Status__c = 'Cancelled';			
		}
		if(slist.size()>0){
			update slist;
			update sesslist;
		}
		
		classesByGroupVar = null;
    	activitiesList = null;		
		getClassesByGroup(); // Comment& : always nullify the list vars when a changed is made.
        return null;
    }
}