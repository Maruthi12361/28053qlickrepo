/**
 *	Description		:	This class is tset class  for OpportunityTeamMemberTriggerHelper.
 *
 *	Created By		:	ext_nup
 *
 *	Created Date	:	12/13/2017
 *
 *	Version			:	V_1.0
 *
 *	Revision Log	:	V_1.0 - Created
 *
 *	Coverage		:	100% 
**/ 
@isTest(seeAllData = false)
private class OpportunityTeamMemberTriggerHelperTest {
	
	
	/** This method is used to test the functionality of populateExecutiveSponser **/
	static testMethod void testPopulateExecutiveSponser() {
		QTTestUtils.GlobalSetUp();
		//Query through Profile 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1]; 
        
        //Custom settoing
        Steelbrick_Settings__c cs3 = new Steelbrick_Settings__c();
		cs3.Name = 'SteelbrickSettingsDetails';
		cs3.SBPricebookId__c = '01s20000000E0PWAA0';
		cs3.Steelbrick_OppRecordType__c = /*'012260000000YTi'*/QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
		insert cs3;
            
        //List of Users
        List<User> users = new List<User>();
        users.add(new User(Alias = 'standt', Email='testCon1@test.com',  EmailEncodingKey='UTF-8', LastName='Test User 1', LanguageLocaleKey='en_US', 
                           	LocaleSidKey='en_US', ProfileId = p.Id,  TimeZoneSidKey='America/Los_Angeles', UserName = 'test1@test.'+ Math.random()));
                           	
        users.add(new User(Alias = 'standt', Email='testCon2@test.com',  EmailEncodingKey='UTF-8', LastName='Test User 2', LanguageLocaleKey='en_US', 
                           	LocaleSidKey='en_US', ProfileId = p.Id,  TimeZoneSidKey='America/Los_Angeles', UserName = 'test2@test.'+ Math.random()));
                           
        insert users;
	 	
	 	//Creating test data 
	 	List<Account> accounts = new List<Account>();
	 	accounts.add(new Account (Name = 'Test Acc 1 '));
	 	accounts.add(new Account (Name = 'Test Acc 2 '));
	 	
	 	insert accounts;
	 	
	 	//Opportunities 
	 	List<Opportunity> opportunities = new List<Opportunity>();
	 	opportunities.add(new Opportunity(Name = 'Test Op1 ' , StageName = 'Prospacting' , CloseDate = Date.Today() , AccountId = accounts[0].Id));
	 	opportunities.add(new Opportunity(Name = 'Test Op1 ' , StageName = 'Prospacting' , CloseDate = Date.Today() , AccountId = accounts[1].Id));
	 	
	 	insert opportunities;
	 	
	 	//List of Opportunity Team Members
	 	List<OpportunityTeamMember> oppTeamMembers = new List<OpportunityTeamMember>();
	 	oppTeamMembers.add(new OpportunityTeamMember(OpportunityId = opportunities[0].Id , UserId = UserInfo.getUserId() , TeamMemberRole = 'Executive Sponsor'));
	 	oppTeamMembers.add(new OpportunityTeamMember(OpportunityId = opportunities[1].Id , UserId = UserInfo.getUserId() , TeamMemberRole = 'Executive Sponsor'));
	 	oppTeamMembers.add(new OpportunityTeamMember(OpportunityId = opportunities[0].Id , UserId = users[0].Id , TeamMemberRole = 'OEM Partner Manager'));
	 	oppTeamMembers.add(new OpportunityTeamMember(OpportunityId = opportunities[1].Id , UserId = users[0].Id , TeamMemberRole = 'OEM Partner Manager'));
	 	oppTeamMembers.add(new OpportunityTeamMember(OpportunityId = opportunities[0].Id , UserId = users[1].Id , TeamMemberRole = 'OEM Partner Manager'));
	 	oppTeamMembers.add(new OpportunityTeamMember(OpportunityId = opportunities[1].Id , UserId = users[1].Id , TeamMemberRole = 'Executive Sponsor'));
	 	
	 	//Test start here 
	 	Test.StartTest();
	 	
	 	insert oppTeamMembers;
	 	
	 	//Assert for results 
	 	opportunities = [Select Id , Executive_Sponsor__c FROM Opportunity where ID IN : opportunities];
	 	
	 	System.assert(opportunities[0].Executive_Sponsor__c != '');
	 	System.assert(opportunities[1].Executive_Sponsor__c != '');
	 	
	 	//Update 
	 	oppTeamMembers[0].TeamMemberRole = 'OEM Partner Manager'; 
	 	oppTeamMembers[1].TeamMemberRole = 'OEM Partner Manager'; 
	 	update oppTeamMembers;
	 	
	 	//Assert for results 
	 	opportunities = [Select Id , Executive_Sponsor__c FROM Opportunity where ID IN : opportunities];
	 	
	 	System.assert(opportunities[0].Executive_Sponsor__c == null);
	 	System.assert(opportunities[1].Executive_Sponsor__c != '');
	 	
	 	delete oppTeamMembers[5];
	 	
	 	//Assert for results 
	 	opportunities = [Select Id , Executive_Sponsor__c FROM Opportunity where ID IN : opportunities];
	 	
	 	System.assert(opportunities[1].Executive_Sponsor__c == null);
	 	
	 	//Test stop here
	 	Test.stopTest();
	 	
	}    
}