/************************************************************
* Utility class for Qlik support portal
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
* 2020-01-04 extbad IT-2693 Fix nullpointer exception
************************************************************/

public with sharing class QS_Utility {
    private static List<String> approvedULCLevels;
    
    public static String GetQS_Persona_Category(String personas) {
        String searchCategory = '';
        for (String s : personas.split(';')) {
            s = s.trim();
            QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(s);
            if (categoryRecord != null) {
                searchCategory = searchCategory + (searchCategory == '' ? '' : ', ') + categoryRecord.Article_Categories__c;
            }
        }
        return searchCategory;
    }
    
    public static Boolean HasULCLevel(Id contactId, String ulcLevel) {
        Boolean found = false;
        try {
            if (approvedULCLevels == null) {
                approvedULCLevels = new List<String>();
                List<Assigned_ULC_Level__c>  ulcLevels = [SELECT Id, ULCLevelId__r.Name, Status__c FROM Assigned_ULC_Level__c WHERE ContactId__c =: contactId AND ULCLevelId__r.Status__c = 'Active'];
                for (Assigned_ULC_Level__c level : ulcLevels) {
                    if (level.Status__c != null && level.Status__c.contains('Approved')) {
                        approvedULCLevels.add(level.ULCLevelId__r.Name);
                    }
                }
            }
           
            for (String level : approvedULCLevels) {
                if (level.toLowerCase() == ulcLevel.toLowerCase()) {
                    found = true;
                }
            }
        } catch (Exception e) {
            System.debug('ERROR:' + e);
        }
        return found;
    }
}