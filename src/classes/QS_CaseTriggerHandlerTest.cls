/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_CaseTriggerHandlerTest {

    private static Id recTypeId;

    // Unit Test for QS_CaseEnvironmentAssociation_Tgr trigger and QS_CaseTriggerHandler Class
    static testMethod void test_CaseEnvironmentAssociation_Tgr() {
        // creates Billing Address to be used on Account record
        Semaphores.SetAllSemaphoresToTrue();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_Thomas', 'test_Jones', 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;

        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        //Create Environment
        Environment__c environment2 = TestDataFactory.createEnvironment(testAccount.Id, 'NewenvironmentName', 'operatingSystem', productLicense.Id, 'type', 'version1_2');
        insert environment2;

        List<Case> caseObjList = new List<Case>();
        //for(integer i=0; i<5; i++) {

        Group newOwner = new Group(Name='test group name', type='Queue', Email = 'testQueue@test.com');
        insert newOwner;
        System.runAs(new User(Id=UserInfo.getUserId())) {
            QueuesObject testQueue = new QueueSObject(QueueID = newOwner.id, SObjectType = 'Case');
            insert testQueue;
        }

        Case c = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                false, null, environment.Id,
                productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        c.Queue_Name_Owner__c = 'test group name';
        caseObjList.add(c);



        //}
        Semaphores.SetAllSemaphoresToFalse();
        insert caseObjList;

        System.assert([Select Id, Last_Used_Date__c From Environment__c where Id = :environment.Id][0].Last_Used_Date__c != null);
        System.assert([Select Id, Last_Used_Date__c From Environment__c where Id = :environment2.Id][0].Last_Used_Date__c == null);
        Test.startTest();
        System.runAs(communityUser) {

            //for(Case caseObj : caseObjList) {
            ApexPages.currentPage().getParameters().put('caseid', caseObjList[0].Id);

            QS_CaseController caseDetailRef = new QS_CaseController();
            caseDetailRef.EditEnvironmentLink();

            QS_EnvironmentController environmentController = new QS_EnvironmentController();
            environmentController.caseDetailRef = caseDetailRef;
            environmentController.selectedEnvironment = environment2.Id;
            caseDetailRef.selectedEnvironement = environment2.Id;
            environmentController.setEnviron();
            caseDetailRef.SelectedEnvironmentLink();

            // Seting the environment
            //caseObj.EnvironmentOfCase__c = environment2.Id;
            //}

            //update caseObjList;


            System.assert([Select Id, Last_Used_Date__c From Environment__c where Id = :environment2.Id][0].Last_Used_Date__c != null);
            delete caseObjList;
        }
        Test.stopTest();

    }


    // Get Record Type Id of the case based on record type name
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if (recTypeId != null) {
            return recTypeId;
        } else {

            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }

}