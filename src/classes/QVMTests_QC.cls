/*
@author: Malay Desai, Slalom LLC
@date: 01/18/2019
@description: Houses all tests units related to the QVM Pages in Qlik Commerce (QC) Community

2019-03-05  AIN Moved custom settings to QuoteTestHelper

*/
@isTest
private class QVMTests_QC {
  
  static testMethod void QVM_ConfigurationTest() {
    QVM_Configuration con = new QVM_Configuration();
    con.loadDefaults();
    con.saveSettings();
  }

    static testMethod void QVM_CasesTest() {

        QttestUtils.GlobalSetUp();
      
      String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        Test.startTest();
        
        System.runAs(testUser) {
            QVM_Cases con = new QVM_Cases();
            con.sort();
            con.createNewCase();
            con.goToQVMControlPanel();
        }
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ControlPanelConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            
        }
        
        Test.startTest();
        
            System.runAs(testUser) {
                
                PageReference pageRef = Page.QVM_ControlPanel_QC;
                Test.setCurrentPage(pageRef);
                
                QVM_ControlPanelCon_QC con = new QVM_ControlPanelCon_QC();
                
                list<QVM_Customer_Products__c> customerlist = con.getCustomerList();
                list<QVM_Product_Data__c> productdata = con.getProductList();
                list<Case> caselist = con.getCaseList();
                
                con.editPartnerProfile();
                con.viewAllCustomers();
                con.exportCustomersExcel();
                con.viewAllProducts();
                con.viewArchivedProducts();
                con.createNewProduct();
                con.viewAllCases();
                con.createNewCase();

            }
            
            System.runAs (thisUser) {
                
                PageReference pageRef = Page.QVM_ControlPanel_QC;
                Test.setCurrentPage(pageRef);
                
                QVM_ControlPanelCon_QC con = new QVM_ControlPanelCon_QC();
                
            }
            
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ControlPanelMainConTest() {

        QttestUtils.GlobalSetUp();
        
        Test.startTest();
        
            QVM_ControlPanelMainCon_QC con = new QVM_ControlPanelMainCon_QC();
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ControlPanelRegistrationConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        Test.startTest();
        
            System.runAs(testUser) {
                
                QVM_ControlPanelRegistrationCon_QC con = new QVM_ControlPanelRegistrationCon_QC();
                String regEdit = con.getRegistrationEditProfileMsg();
                con.submitForm();
                
                con.partnerData.Agreed_to_T_C__c = true;
                con.submitForm();

            }
            
            System.runAs(testUser) {
                
                QVM_ControlPanelRegistrationCon_QC con = new QVM_ControlPanelRegistrationCon_QC();
                String regSubmit = con.getRegistrationSubmittedMsg();
                
            }
                
        
        Test.stopTest();

    }
    
    static testMethod void QVM_CreateCaseTest() {

        QttestUtils.GlobalSetUp();
      
     
        Test.startTest();
          
        QVM_CreateCaseCon con = new QVM_CreateCaseCon();
        con.cancel();
        con.saveCase();
          
        Test.stopTest();
      
    }
    
    static testMethod void QVM_CustomerDetailConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        QVM_Customer_Products__c testCustomer = QVMTest.newCustomerProduct(testContact.Id, null, testPartner.Id, testProduct.Id);
        insert testCustomer;
        
        Test.startTest();
        
            PageReference pageRef = Page.QVM_CustomerDetail_QC;
            pageRef.getParameters().put('id',testCustomer.Id);
            Test.setCurrentPage(pageRef);
            
            QVM_CustomerDetailCon_QC con = new QVM_CustomerDetailCon_QC();
            
            con.convertToLead();
            con.viewAllCustomers();
            con.viewQlikMarket();
        
        Test.stopTest();
        
    }

    static testMethod void QVM_CustomersConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        QVM_Customer_Products__c testCustomer = QVMTest.newCustomerProduct(testContact.Id, null, testPartner.Id, testProduct.Id);
        insert testCustomer;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            testPartner.Approved__c = true;
            update testPartner;

        }
        
        Test.startTest();
            
            System.runAs(testUser) {
                QVM_CustomersCon_QC con = new QVM_CustomersCon_QC();
                
                con.customers[0].selected = true;
                con.createLead();
                
                con.addTableFilter();
                con.clearTableFilter();

                con.exportCustomersExcel();
                con.goToQVMControlPanel();
                list<QVM_CustomersCon_QC.customerWrapper> customers = con.getCustomerList();
                
                con.startDate = '05/27/2012';
                con.endDate = '05/27/2012';
                con.addTableFilter();
                con.sort();
                con.clearTableFilter();
                
                con.firstPage();
                con.prevPage();
                con.nextPage();
                con.lastPage();

                PageReference pageRef = con.checkRegistration();
            }
                
        
        Test.stopTest();

    }
    
    
    static testMethod void QVM_GlobalTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        Test.startTest();
        
            QVM_Global.archiveMagentoProduct(testProduct.Id);
            QVM_Global.enableMagentoProduct(testProduct.Id);
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_PartnerProfileTest1() {

        QttestUtils.GlobalSetUp();
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        testPartner.Approved__c = true;
        update testPartner;

        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;

        Test.startTest();
            System.runAs (testUser) {
                QVM_PartnerProfile_QC con = new QVM_PartnerProfile_QC();
                con.savePartner();
                
                QVM_PartnerProfile_QC con2 = new QVM_PartnerProfile_QC();
                con2.cancel();
            }
            
        Test.stopTest();
        
    }
    static testMethod void QVM_PartnerProfileTest2() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            testPartner.Approved__c = true;
            update testPartner;
        }
        
        Test.startTest();

            System.runAs (testUser) {
                PageReference pageRef = Page.QVM_PartnerProfile_QC;
                pageRef.getParameters().put('id',testPartner.Id);
                Test.setCurrentPage(pageRef);
                
                QVM_PartnerProfile_QC con = new QVM_PartnerProfile_QC();
                
                con.partner.Partner_Website__c = 'www.google.com/';
                con.savePartner();
                
                con.partner.Support_URL__c = 'www.google.com/';
                con.savePartner();
                
                con.partner.Partner_Website__c = 'http://www.google.com/';
                con.partner.Support_URL__c = 'http://www.google.com/';
                con.savePartner();
                
                con.cancel();

                pageRef = con.checkRegistration();
                
            }
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ProductBuilderStep1TestA() {

        QttestUtils.GlobalSetUp();
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        testPartner.Approved__c = true;
        update testPartner;

        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        Test.startTest();
            System.runAs (testUser) {
                QVM_ProductBuilderStep1_QC con = new QVM_ProductBuilderStep1_QC();
                con.saveProduct();
                con.nextPage();
            }
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ProductBuilderStep1TestB() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        testPartner.Approved__c = true;
        update testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        String profileId = QVMTest.getProfileId('PRM - Base');

        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        Test.startTest();
        System.runAs (testUser) {
            PageReference pageRef = Page.QVM_ProductBuilderStep1_QC;
            pageRef.getParameters().put('id',testProduct.Id);
            Test.setCurrentPage(pageRef);

            QVM_ProductBuilderStep1_QC con = new QVM_ProductBuilderStep1_QC();
            con.saveProduct();
            con.product.Short_Description__c = '<strong>something</strong>';
            con.saveProduct();
            con.product.Short_Description__c = 'something';
            con.product.Overview__c = 'Something';
            con.product.Ajax_Demo_Link__c = 'www.google.com';
            con.saveProduct();
            con.product.Ajax_Demo_Link__c = 'http://www.google.com/';
            con.saveProduct();
            con.nextPage();
            con.cancel();

            pageRef = con.checkRegistration();
        }
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ProductBuilderStep2Test() {

        QttestUtils.GlobalSetUp();
       
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        Test.startTest();
        
            PageReference pageRef = Page.QVM_ProductBuilderStep2_QC;
            pageRef.getParameters().put('id',testProduct.Id);
            Test.setCurrentPage(pageRef);
            
            QVM_ProductBuilderStep2_QC con = new QVM_ProductBuilderStep2_QC();
            
            try {
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.ProductTypeId = '15';
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.IndustryIds = new String[] {'15'};
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.FunctionIds = new String[] {'15'};
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.VersionIds = new String[] {'15'};
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            con.saveProduct();
            con.backPage();
            con.nextPage();
            con.previewPage();

            //for coverage as this function does not appear to be used
            //CTS SCU-7 Unused Apex Method Removal 
			//String rets = QVM.htmlEncoder('Test');
            //for coverage
            String rets = QVM.getLineBreaksToBR('Test');

        Test.stopTest();
        
        
    }
    
    static testMethod void QVM_ProductBuilderStep3Test() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        
        Test.startTest();
        
        PageReference pageRef = Page.QVM_ProductBuilderStep3_QC;
        pageRef.getParameters().put('id',testProduct.Id);
        Test.setCurrentPage(pageRef);
        
        QVM_ProductBuilderStep3_QC con = new QVM_ProductBuilderStep3_QC();
        
        String policy = con.getPolicy();
        String signedPolicy = con.getSignedPolicy();
        
        list<SelectOption> productmethods = con.getProductMethodOptions();
        
        con.product.Referral_Link__c = null;
        con.quickSave();
        
        con.product.Referral_Link__c = 'google.com';
        con.quickSave();
        
        con.product.Referral_Link__c = 'http://www.google.com/';
        con.quickSave();
        
        con.removeProductReferral();
        
        QVM_ProductBuilderStep3_QC.setupFileName('product.zip', testProduct.Id);
        
        con.alternateUploadFile();
        con.removeProductFile();
        
        con.backPage();
        con.saveProduct();
        con.nextPage();
        
        con.product.Referral_Link__c = null;
        con.product.Product_Download_Link__c = null;
        con.nextPage();
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ProductBuilderStep4Test() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        
        
        Test.startTest();
        
        PageReference pageRef = Page.QVM_ProductBuilderStep4_QC;
        pageRef.getParameters().put('id',testProduct.Id);
        Test.setCurrentPage(pageRef);
    
        QVM_ProductBuilderStep4_QC con = new QVM_ProductBuilderStep4_QC();
        
        list<QVM_ProductBuilderStep4_QC.fileWrapper> images = con.images;
        
        QVM_ProductBuilderStep4_QC.fileWrapper image = new QVM_ProductBuilderStep4_QC.fileWrapper();
        image.fileName = 'image1.jpg';
        image.fileSize = 1024;
        image.fileBody = Blob.valueOf('123456789');
        String dBody = image.docBody();
        
        con.image1 = image;
        con.image2 = image;
        con.image3 = image;
        con.image4 = image;
        con.image5 = image;
        
        con.uploadImage1();
        con.uploadImage2();
        con.uploadImage3();
        con.uploadImage4();
        con.uploadImage5();
        
        con.removeImage1();
        con.removeImage2();
        con.removeImage3();
        con.removeImage4();
        con.removeImage5();
        
        con.backPage();
        con.saveProduct();
        con.nextPage();
        
        Test.stopTest();
        
    }
    static testMethod void QVM_ProductPreviewConExceptionsTest() {

        QttestUtils.GlobalSetUp();
      
      String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        Test.startTest();
        
        System.runAs (testUser) {
          QVM_ProductPreviewCon_QC con = new QVM_ProductPreviewCon_QC();
            con.submitApproval();
            con.performPublishToMagento();
            con.editProduct();
        }
        
        Test.stopTest();

    }
    static testMethod void QVM_ProductPreviewConTest() {

        QttestUtils.GlobalSetUp();
      
      String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        Test.startTest();
        
        System.runAs(testUser) {
          PageReference pageRef = Page.QVM_ProductPreview_QC;
            pageRef.getParameters().put('id',testProduct.Id);
            Test.setCurrentPage(pageRef);
            
            QVM_ProductPreviewCon_QC con = new QVM_ProductPreviewCon_QC();
            con.product.Overview__c = 'some kind of overview';
            con.product.Short_Description__c = 'some kind of description';
            con.product.Type__c = 'Application';
            con.product.Industry__c = 'Education';
            con.product.Job_Function__c = 'Executive';
            con.product.Version__c = '1.0';
            con.product.QlikView_Version__c = '9.0';
            con.product.Referral_Link__c = 'http://www.google.com/';
            
            con.editProduct();
            con.goToQVMControlPanel();
            con.submitApproval();
            con.performPublishToMagento();
            
            con.product.Product_Download_Link__c  = 'http://qlikview-market-dev.s3.amazonaws.com/live/product.zip';
            con.product.Image_1__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image1.jpg';
            con.product.Image_2__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image2.jpg';
            con.product.Image_3__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image3.jpg';
            con.product.Image_4__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image4.jpg';
            con.product.Image_5__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image5.jpg';
            
            con.performPublishToMagento();
        }
        
        Test.stopTest();
        
    }
    static testMethod void QVM_ProductsTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            testPartner.Approved__c = true;
            update testPartner;
        }
        
        Test.startTest();
            testUser = [select Id, IsPortalEnabled from User where Id = :testUser.Id];
            System.runAs (testUser) {
                QVM_Products_QC con = new QVM_Products_QC();
                con.sort();
                con.goToQVMControlPanel();

                PageReference pageRef = con.checkRegistration();
            }

        Test.stopTest();
        
    }
    
    
    static testMethod void QVM_TableTest() {

        QttestUtils.GlobalSetUp();
        
        Test.startTest();
        
        
        
        Test.stopTest();
        
    }
    
    static testMethod void QVMTest() {

        QttestUtils.GlobalSetUp();
        
        Test.startTest();
        
        String[] arrayFromString = QVM.getArrayFromString('something;something', ';');
        
        list<SelectOption> options = new list<SelectOption> {
          new SelectOption('20','something')
        };
        
        //map<String,String> catMap = QVM.getMagentoCategoryMap(options);
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ProductDataTriggersTests() {

        QttestUtils.GlobalSetUp();
      
      String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        testProduct.Status__c = 'Draft';
        testProduct.Overview__c = 'some kind of overview';
        testProduct.Short_Description__c = 'some kind of description';
        testProduct.Type__c = 'Application';
        testProduct.Industry__c = 'Education';
        testProduct.Job_Function__c = 'Executive';
        testProduct.Version__c = '1.0';
        testProduct.QlikView_Version__c = '9.0';
        testProduct.Referral_Link__c = 'http://www.google.com/';
        testProduct.Product_Download_Link__c  = 'http://qlikview-market-dev.s3.amazonaws.com/live/product.zip';
        testProduct.Image_1__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image1.jpg';
        testProduct.Image_2__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image2.jpg';
        testProduct.Image_3__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image3.jpg';
        testProduct.Image_4__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image4.jpg';
        testProduct.Image_5__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image5.jpg';
        insert testProduct;
        
        testProduct.Status__c = 'Ready to Publish';
        update testProduct;
        
        testProduct.Magento_Product_Id__c = '1';
        testProduct.Status__c = 'Draft';
        update testProduct;
        
        testProduct.Status__c = 'Ready to Publish';
        update testProduct;
        
    }
    
}