/*
 File Name: CaseEntitlementProcessSyncHandler
 Moved from trigger CaseEntitlementProcessSync on Case (before insert, before update)
 Trigger to update case entitlement process

    2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.
    2018-04-02  ext_bad     INC0128561  Designated_Support_Engineer__c populating fix
*/

public with sharing class CaseEntitlementProcessSyncHandler {

    public static void handleCaseEntitlementProcessSync(List<Case> newCases, List<Case> oldCases) {
        Set<Id> contactIds = new Set<Id>();
        List<Case> cases_se = new List<Case>();// Modified by UIN to reduce soql execution
        Set<Id> entitlementIds = new Set<Id>();
        for (Integer i = 0; i < newCases.size(); i++) {
            if (String.isNotBlank(newCases[i].ContactId) && (
                    oldCases == null || (newCases[i].ContactId != oldCases[i].ContactId)
            )) {// Modified by UIN to reduce soql execution
                cases_se.add(newCases[i]);// Modified by UIN to reduce soql execution
                contactIds.Add(newCases[i].ContactId);
            }
        }

        if (!cases_se.isEmpty()) {// Modified by UIN to reduce soql execution
// Set the DSE from Account
            Map<Id, Contact> mapIds = new Map<Id, Contact>([
                    SELECT Id, AccountId, Account.QT_Designated_Support_Contact__c,
                            Account.QT_Designated_Support_Contact__r.Id, DSE_Access__c
                    FROM Contact
                    WHERE Id IN :contactIds
            ]);

            for (Case c : cases_se) {// Modified by UIN to reduce soql execution
                if (!mapIds.isEmpty()) {// Modified by UIN to reduce soql execution
                    if (c.ContactId != null
                            && mapIds.get(c.ContactId) != null
                            && mapIds.get(c.ContactId).DSE_Access__c != null
                            && mapIds.get(c.ContactId).DSE_Access__c == true) {
                        if (mapIds.get(c.ContactId).AccountId != null
                                && mapIds.get(c.ContactId).Account.QT_Designated_Support_Contact__c != null) {

                            system.debug(LoggingLevel.DEBUG, 'Setting Designated_Support_Engineer__c to '
                                    + mapIds.get(c.ContactId).Account.QT_Designated_Support_Contact__c);
                            c.Designated_Support_Engineer__c = mapIds.get(c.ContactId).Account.QT_Designated_Support_Contact__c;
                        }
                    }
                }// Modified by UIN to reduce soql execution
            }
        }

        List<Case> cases = new List<Case>();
        for (Integer i = 0; i < newCases.size(); i++) {
            if (oldCases == null
                    || (String.isNotBlank(newCases[i].EntitlementId) // Modified by UIN to reduce soql execution
                    && newCases[i].EntitlementId != oldCases[i].EntitlementId)
                    ) {// Modified by UIN to reduce soql execution
                entitlementIds.add(newCases[i].EntitlementId);
                cases.add(newCases[i]);// Modified by UIN to reduce soql execution
            }
        }

        if (!cases.isEmpty()) {// Modified by UIN to reduce soql execution
            Entitlement e;// Modified by UIN to reduce soql execution
            Map<Id, Entitlement> entitlements = new Map<Id, Entitlement>([
                    SELECT Id, SlaProcess.Name
                    FROM Entitlement
                    WHERE Id IN :entitlementIds
            ]);
            for (Case caze : cases) {// Modified by UIN to reduce soql execution
                if (!entitlements.isEmpty()) {// Modified by UIN to reduce soql execution
                    e = entitlements.get(caze.EntitlementId);// Modified by UIN to reduce soql execution
                    if (e != null) {
                        caze.Entitlement_Process__c = e.SlaProcess.Name;
                    }
                }
            }
        }
    }
}