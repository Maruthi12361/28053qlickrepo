/***********************************************
* Change log:
* 20140922 RDZ 	CR# 16608 Adding some more detail to identify issue on Exception on CaseAcntLicenseContExt and its test class TestCaseAcntLicenseContExt:
*				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' ' + e.getTypeName() + ' ' + e.getLineNumber() + ' StackTrace: '+e.getStackTraceString()));
*            
************************************************/
public with sharing class CaseAcntLicenseContExt 
{
	//Variables
	private final ApexPages.StandardController sc;
	public Case c;
	private User u;
	private Profile p;
	private CaseModel caseModel;
	private ContactModel contModel;
	//Controller Constructor
	public CaseAcntLicenseContExt(ApexPages.StandardController sc)
	{
		this.sc = sc;
		this.c = (Case) sc.getRecord();
		//Get record type Id
		String recTypeId = ApexPages.currentPage().getParameters().get('RecordType');
		if(recTypeId != null && recTypeId != '') c.RecordTypeId = recTypeId;
		isValid = false;
		isPartner = false;
		isCustomer = false;
		show = true;
		u = [select IsPortalEnabled from User where Id = :UserInfo.getUserId()];
		p = [select Name, UserLicense.Name from Profile where Id = :UserInfo.getProfileId()];
		try
		{
			Id contactId = ApexPages.currentPage().getParameters().get('contactId');
			if(contactId != null && String.valueOf(contactId).startsWith('003'))
			{
				contModel = new ContactModel(contactId);
				//Check if the Contact has a valid Id
				if(contModel.hasValidId)
				{
					//Gets Account related to Contact
					account = contModel.getAccountRelatedToContact();
					//Gets Account Licenses related to the Account 
					//and creates a List of AccountLicenseModel class												
					for(Account_License__c al : contModel.getAccountLicensesRelatedToAccount())
					{
						accountLicenseList.add(new AccountLicenseModel(al));
					}
					accountLicenseList.add(new AccountLicenseModel(new Account_License__c(Name='Do not know')));

					c.ContactId = contactId;
					c.AccountId = account.Id;
					if(p.UserLicense.Name.contains('Partner'))
						isPartner = true;
					else
						isCustomer = true;
				}
			}
			else //contactId is null, must be coming from existing case or regular user new case
			{
				caseModel = new CaseModel(c);
				if(caseModel.hasValidId)
				{
					contModel = new ContactModel(c.ContactId);
					//Gets Account related to Case
					account = caseModel.getAccountRelatedToCase();
					//Gets Account Licenses related to the Account 
					//and creates a List of AccountLicenseModel class												
					for(Account_License__c al : caseModel.getAccountLicensesRelatedToAccount())
						accountLicenseList.add(new AccountLicenseModel(al));
					accountLicenseList.add(new AccountLicenseModel(new Account_License__c(Name='Do not know')));
				}
				else
				{
					String fromContactId = ApexPages.currentPage().getParameters().get('def_contact_id');
					String fromAccountId = ApexPages.currentPage().getParameters().get('def_account_id');
					c.ContactId = fromContactId == null || fromContactId == '' ? null : fromContactId;
					c.AccountId = fromAccountId == null || fromAccountId == '' ? null : fromAccountId;
					if(c.ContactId != null && c.AccountId != null)
						getAccountLicenses();
					else
						show = false;
				}
			}
			if(contModel != null) contact = contModel.record;
			isValid = true;
		}
		catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' ' + e.getTypeName() + ' ' + e.getLineNumber() + ' StackTrace: '+e.getStackTraceString()));
            return;
		}
	}
	//Properties
	public Boolean isValid {get; private set;}
	public Boolean isPartner {get; private set;}
	public Boolean isCustomer {get; private set;}
	public Boolean show {get; private set;}
	public AccountLicenseModel[] accountLicenseList 
	{
		get
		{
			if(accountLicenseList == null)
			{
				accountLicenseList = new AccountLicenseModel[]{};
			}
			return accountLicenseList;		
		}	
		private set;
	}
	public Account account 
	{
		get
		{
			if(account == null)
			{
				account = new Account();	
			}
			return account;
		}
		private set;
	}
	public Contact contact
	{
		get
		{
			if(contact == null)
			{
				contact = new Contact();
			}
			return contact;
		}
		private set;
	}
	//Methods
	//This method updates the Case with the changed Account License if any selected 
	//and returns a PageReference to the Case edit page
	public PageReference changeAccountLicenseOnCase()
	{
		//Go over the list of Account Licenses and update Case 
		//with the Account License Info if any was selected 
		for(AccountLicenseModel alm : accountLicenseList)
		{
			if(alm.selected)
			{
				try
				{
					//Link Account License to Case and return the Account License Id
					CaseServices.updateCaseFromAcntLicense(c.Id, alm.record);
				}
		        catch(Exception e)
		        {
		            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getTypeName() + ': ' + e.getMessage()));
		            return null;
		        }
				return this.sc.view();
			}
		}
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check the appropriate license or "Do Not Know" then click "Save" again.'));
	    return null;
	}
	//This method updates the Case with the Account License if any selected 
	//and returns a PagerReference to the Case edit page
	public PageReference createCaseFromAccountLicense()
	{
		try
		{
			//Go over the list of Account Licenses and insert then update Case 
			//with the Account License Info if any was selected 
			for(AccountLicenseModel alm : accountLicenseList)
			{
				if(alm.selected)
				{
					//Link Account License to Case and return the Account License Id
					//CaseServices.createCaseFromAcntLicense(c, alm.record);
					//Return new page
					/* 
					//Sandbox URL parameters
					String url = '/500/e?nooverride=1&00NS0000000r862=1&retURL=' + EncodingUtil.urlEncode('/500/o', 'UTF-8');
					if(c.RecordTypeId != null) url += '&RecordType=' + EncodingUtil.urlEncode(c.RecordTypeId, 'UTF-8');
					if(alm.record.O_S__c != null) url += '&00N20000001P3WR=' + EncodingUtil.urlEncode(alm.record.O_S__c, 'UTF-8');
					if(alm.record.Environment__c != null) url += '&00N20000001P3W6=' + EncodingUtil.urlEncode(alm.record.Environment__c, 'UTF-8');
					if(alm.record.Product__c != null) url += '&00N20000001bovX=' + EncodingUtil.urlEncode(alm.record.Product__c, 'UTF-8');
					if(alm.record.Area__c != null) url += '&00N20000001bruD=' + EncodingUtil.urlEncode(alm.record.Area__c, 'UTF-8');
					if(alm.record.Issue__c != null) url += '&00N20000001P3Wc=' + EncodingUtil.urlEncode(alm.record.Issue__c, 'UTF-8');
					if(alm.record.X3rd_Party_Software__c != null) url += '&00N20000001bru3=' + EncodingUtil.urlEncode(alm.record.X3rd_Party_Software__c, 'UTF-8');
					if(alm.record.Client_Version__c != null) url += '&00N20000001P3WW=' + EncodingUtil.urlEncode(alm.record.Client_Version__c, 'UTF-8');
					if(alm.record.Server_Version__c != null) url += '&00N20000001P3Wb=' + EncodingUtil.urlEncode(alm.record.Server_Version__c, 'UTF-8');
					if(alm.record.Publisher_Version__c != null) url += '&00N20000001c241=' + EncodingUtil.urlEncode(alm.record.Publisher_Version__c, 'UTF-8');
					if(alm.record.Connector_Version__c != null) url += '&00N20000001c23c=' + EncodingUtil.urlEncode(alm.record.Connector_Version__c, 'UTF-8');
					if(alm.record.Customer_Patch_Version__c != null) url += '&00NS0000000kiZU=' + EncodingUtil.urlEncode(alm.record.Customer_Patch_Version__c, 'UTF-8');
					if(alm.record.Name != null) url += '&00N20000001brty=' + EncodingUtil.urlEncode(alm.record.Name, 'UTF-8');
					if(c.AccountId != null) url += '&cas4_lkid=' + EncodingUtil.urlEncode(((String)c.AccountId).substring(0, 15), 'UTF-8');
					if(c.ContactId != null) url += '&cas3_lkid=' + EncodingUtil.urlEncode(((String)c.ContactId).substring(0, 15), 'UTF-8');
					if(c.Account_Origin__r.Name != null) url += '&CF00N20000001brt5=' + EncodingUtil.urlEncode(c.Account_Origin__r.Name, 'UTF-8');
					if(c.Account_Origin__c != null) url += '&CF00N20000001brt5_lkid=' + EncodingUtil.urlEncode(((String)c.Account_Origin__c).substring(0, 15), 'UTF-8');
					if(c.Contact_Origin__r.Name != null) url += '&CF00N20000001brtA=' + EncodingUtil.urlEncode(c.Contact_Origin__r.Name, 'UTF-8');
					if(c.Contact_Origin__c != null) url += '&CF00N20000001brtA_lkid=' + EncodingUtil.urlEncode(((String)c.Contact_Origin__c).substring(0, 15), 'UTF-8');
					return new PageReference(url);
					*/
					//Production URL parameters
					String url = '/500/e?nooverride=1&00N20000002vUr4=1&retURL=' + EncodingUtil.urlEncode('/500/o', 'UTF-8');
					if(c.RecordTypeId != null) url += '&RecordType=' + EncodingUtil.urlEncode(c.RecordTypeId, 'UTF-8');
					if(alm.record.O_S__c != null) url += '&00N20000001P3WR=' + EncodingUtil.urlEncode(alm.record.O_S__c, 'UTF-8');
					if(alm.record.Environment__c != null) url += '&00N20000001P3W6=' + EncodingUtil.urlEncode(alm.record.Environment__c, 'UTF-8');
					if(alm.record.Product__c != null) url += '&00N20000001bovX=' + EncodingUtil.urlEncode(alm.record.Product__c, 'UTF-8');
					if(alm.record.Area__c != null) url += '&00N20000001bruD=' + EncodingUtil.urlEncode(alm.record.Area__c, 'UTF-8');
					if(alm.record.Issue__c != null) url += '&00N20000001P3Wc=' + EncodingUtil.urlEncode(alm.record.Issue__c, 'UTF-8');
					if(alm.record.X3rd_Party_Software__c != null) url += '&00N20000001bru3=' + EncodingUtil.urlEncode(alm.record.X3rd_Party_Software__c, 'UTF-8');
					if(alm.record.Client_Version__c != null) url += '&00N20000001P3WW=' + EncodingUtil.urlEncode(alm.record.Client_Version__c, 'UTF-8');
					if(alm.record.Server_Version__c != null) url += '&00N20000001P3Wb=' + EncodingUtil.urlEncode(alm.record.Server_Version__c, 'UTF-8');
					if(alm.record.Publisher_Version__c != null) url += '&00N20000001c241=' + EncodingUtil.urlEncode(alm.record.Publisher_Version__c, 'UTF-8');
					if(alm.record.Connector_Version__c != null) url += '&00N20000001c23c=' + EncodingUtil.urlEncode(alm.record.Connector_Version__c, 'UTF-8');
					if(alm.record.Customer_Patch_Version__c != null) url += '&00N20000002bQtq=' + EncodingUtil.urlEncode(alm.record.Customer_Patch_Version__c, 'UTF-8');
					if(alm.record.Name != null) url += '&00N20000001brty=' + EncodingUtil.urlEncode(alm.record.Name, 'UTF-8');
					if(c.AccountId != null) url += '&cas4_lkid=' + EncodingUtil.urlEncode(((String)c.AccountId).substring(0, 15), 'UTF-8');
					if(c.ContactId != null) url += '&cas3_lkid=' + EncodingUtil.urlEncode(((String)c.ContactId).substring(0, 15), 'UTF-8');
					if(c.Account_Origin__r.Name != null) url += '&CF00N20000001brt5=' + EncodingUtil.urlEncode(c.Account_Origin__r.Name, 'UTF-8');
					if(c.Account_Origin__c != null) url += '&CF00N20000001brt5_lkid=' + EncodingUtil.urlEncode(((String)c.Account_Origin__c).substring(0, 15), 'UTF-8');
					if(c.Contact_Origin__r.Name != null) url += '&CF00N20000001brtA=' + EncodingUtil.urlEncode(c.Contact_Origin__r.Name, 'UTF-8');
					if(c.Contact_Origin__c != null) url += '&CF00N20000001brtA_lkid=' + EncodingUtil.urlEncode(((String)c.Contact_Origin__c).substring(0, 15), 'UTF-8');
					return new PageReference(url);
				}
			}
		}
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getTypeName() + ': ' + e.getMessage()));
            return null;
        }
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check the appropriate license or "Do Not Know" then click "Create Case" again.'));
		return null;
	}
	public PageReference cancelNewCase()
	{
		return new PageReference('/500/o');
	}
	public void getAccountLicenses()
	{
		show = true;
		accountLicenseList.clear();
		getAccount();
		//Gets Account Licenses related to the Account 
		//and creates a List of AccountLicenseModel class												
		for(Account_License__c al : contModel.getAccountLicensesRelatedToAccount())
			accountLicenseList.add(new AccountLicenseModel(al));
		accountLicenseList.add(new AccountLicenseModel(new Account_License__c(Name='Do not know')));
	}
	public void getAccount()
	{
		if(c.Contact_Origin__c == null)
		{
			c.Account_Origin__c = null;
			contModel = new ContactModel(c.ContactId);
			contact = contModel.record;
			account = contModel.getAccountRelatedToContact();
			c.AccountId = account.Id;
			c.Account = account;
		}
		else
		{
			contModel = new ContactModel(c.Contact_Origin__c);
			contact = contModel.record;
			account = contModel.getAccountRelatedToContact();
			c.Contact_Origin__r = contact;
			c.Account_Origin__c = account.Id;
			c.Account_Origin__r = account;
			ContactModel partnerContactModel = new ContactModel(c.ContactId);
			c.Account = partnerContactModel.getAccountRelatedToContact();
			c.AccountId = c.Account.Id;
		}
	}
	public void clearContactLookup()
	{
		c.ContactId = null;
		c.AccountId = null;
		contModel = null;
		contact = new Contact();
		account = new Account();
	}
	public void clearContactOriginLookup()
	{
		c.Contact_Origin__c = null;
		c.Account_Origin__c = null;
		contModel = null;
		contact = new Contact();
		account = new Account();
	}
}