/*****************************************************************************************
* 2015-09-28 AIN Webservice Mock for class ulcv3QlikviewCom, using WebServiceMockDispatcher is prefered, see
* that class for details.
****************************************************************************************/
@isTest
public class ULCMock implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    system.debug('ULCMock Start');
    if(request instanceof ulcv3QlikviewCom.SelfServeChanged_element)
    {
      system.debug('instanceof ulcv3QlikviewCom.SelfServeChanged_element');
      response.put('response_x', new ulcv3QlikviewCom.SelfServeChangedResponse_element());
    }
    system.debug('ULCMock End');
      
   }
}