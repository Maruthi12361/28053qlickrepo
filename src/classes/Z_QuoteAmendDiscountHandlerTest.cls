/*
5/2/2019 BSL-1910 Test Class for Handler created by Shubham Gupta to Populate Partner Discount on 
Reseller Amend deals FROM initial Quote purchase instead of PCS record
*/

@isTest public class Z_QuoteAmendDiscountHandlerTest {
	
    
    @isTest static void test_Amend_Discount() {

        QuoteTestHelper.createCustomSettings();
        
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Uniqueness level over 9000', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;

        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Over 9000? Theres no way that can be right', '','65000-480');
        insert partner;
        partner.Pending_Validation__c =FALSE;
        update partner;

        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Need more uniqueness!', '','65000-480');
        insert secondPartner;
        
        secondPartner.Pending_Validation__c =FALSE;
        update secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        Opportunity testOpp2 = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp2.Sell_Through_Partner__c = partner.Id;
        testOpp2.Partner_Contact__c = partnerContact.Id;
        testOpp2.Second_Partner__c = secondPartner.Id;
        testOpp2.Revenue_Type__c = 'Reseller';
        insert testOpp2;

        //Zuora__CustomerAccount__c billingAccount = Z_TestFactory.createBillingAccount(endUserAccount,'test123456test');
        //insert billingAccount;

        

        Test.startTest();
        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        //testQuote.zqu__ZuoraSubscriptionID__c = 'testsub123467shd';
        insert testQuote;

        Zuora__CustomerAccount__c accGotFromQuote = [select id,Zuora__Account__c,Zuora__External_Id__c from Zuora__CustomerAccount__c where Zuora__Account__c =:endUserAccount.ID];

        Zuora__CustomerAccount__c billingPartnerAccount = Z_TestFactory.createBillingAccount(partner,'2c92c0f86923509b016025c6ab272380');
        insert billingPartnerAccount;

        Zuora__Subscription__c subsonaccount = Z_TestFactory.makeSubscription(accGotFromQuote,endUserAccount,'2c92c0g86923509b01402536ab272380');
        subsonaccount.Zuora__InvoiceOwner__c = billingPartnerAccount.id;
        insert subsonaccount;

        testQuote = [SELECT Id, Subscription_Partner_Discount__c, zqu__ZuoraSubscriptionID__c, Second_Subscription_Partner_Discount__c FROM zqu__Quote__c WHERE Id = :testQuote.Id];

        zqu__Quote__c testQuote2 = Z_TestFactory.makeQuote2(testOpp2, endUserAccount);
        testQuote2.zqu__BillToContact__c = endUserContact.id;
        testQuote2.zqu__SoldToContact__c = endUserContact.id;
        testQuote2.zqu__ExistSubscriptionID__c = testQuote.zqu__ZuoraSubscriptionID__c;
        testQuote2.zqu__QuoteBusinessType__c = 'Upsell';
        testQuote2.zqu__SubscriptionType__c = 'Amend Subscription';
        insert testQuote2;

        zqu__QuoteAmendment__c amend = Z_TestFactory.makeQuoteAmendment(testQuote2);
        insert amend;

        Test.stopTest();

        

        zqu__Quote__c testQuoteassert = [SELECT Id, Revenue_Type__c, zqu__SubscriptionType__c, Subscription_Partner_Discount__c, Second_Subscription_Partner_Discount__c FROM zqu__Quote__c WHERE Id = :testQuote2.Id];
        system.debug('@@suscription type' + testQuoteassert.zqu__SubscriptionType__c);
        system.debug('@@revenue type' + testQuoteassert.Revenue_Type__c);
        System.assertEquals(testQuoteassert.Subscription_Partner_Discount__c, testQuote.Subscription_Partner_Discount__c);
        System.assertEquals(testQuoteassert.Revenue_Type__c, 'Reseller');
    }
}