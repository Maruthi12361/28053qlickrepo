/****************************************************** 

Class: Test_Account_SetSupportOffice 

This class tests the Account_SetSupportOffice trigger 

Changelog: 
2012-07-18 CCE Initial creation 
2014-08-22 CCE Updated test for CR# 14397 https://eu1.salesforce.com/a0CD000000jXhIa
2016-02-04 OSO Updated test for CR# 74965 https://eu1.salesforce.com/a0CD000000xS7ly
2016-10-16 UIN Test class no longer used. Replaced by account_setsupportofficeTest
******************************************************/ 
@isTest  //(SeeAllData=true)
private class Test_Account_SetSupportOffice {
/*
    public static testmethod void testSetSupportOffice() { 
    
        //Populate the Custom setting that holds the list of users email addresses
        string toAddresses = 'tester1@qlik.com.sandbox,tester2@qlik.com.sandbox';
        QTCustomSettings__c mycs = QTCustomSettings__c.getValues('Default');
        if(mycs == null) {             
            mycs = new QTCustomSettings__c(Name = 'Default');
            mycs.null_Support_Office_Email__c = toAddresses;
            insert mycs;
        }

        //Create QlikTech Companies
        List<Id> QTCompany = new Id[26]; 
        QlikTech_Company__c qtComp_AUS = QTTestUtils.createMockQTCompany('QlikTech Australia Pty Ltd', 'AUS');
        QTCompany[0] = qtComp_AUS.Id;
        QlikTech_Company__c qtComp_HUN = QTTestUtils.createMockQTCompany('QlikTech Benelux', 'HUN');
        QTCompany[1] = qtComp_HUN.Id;
        QlikTech_Company__c qtComp_BRA = QTTestUtils.createMockQTCompany('QlikTech Brazil', 'BRA');
        QTCompany[2] = qtComp_BRA.Id;
        QlikTech_Company__c qtComp_CAN = QTTestUtils.createMockQTCompany('QlikTech Corporation', 'CAN');
        QTCompany[3] = qtComp_CAN.Id;
        QlikTech_Company__c qtComp_ISL = QTTestUtils.createMockQTCompany('QlikTech Denmark ApS', 'ISL');
        QTCompany[4] = qtComp_ISL.Id;
        
        QlikTech_Company__c qtComp_FIN = QTTestUtils.createMockQTCompany('QlikTech Finland OY', 'FIN');
        QTCompany[5] = qtComp_FIN.Id;
        QlikTech_Company__c qtComp_FRA = QTTestUtils.createMockQTCompany('QlikTech France SARL', 'FRA');
        QTCompany[6] = qtComp_FRA.Id;
        QlikTech_Company__c qtComp_DEU = QTTestUtils.createMockQTCompany('QlikTech GmbH', 'DEU');
        QTCompany[7] = qtComp_DEU.Id;
        QlikTech_Company__c qtComp_CHN = QTTestUtils.createMockQTCompany('QlikTech Hong Kong Ltd', 'CHN');
        QTCompany[8] = qtComp_CHN.Id;
        QlikTech_Company__c qtComp_ESP = QTTestUtils.createMockQTCompany('QlikTech Iberia SL', 'ESP');
        QTCompany[9] = qtComp_ESP.Id;

        QlikTech_Company__c qtComp_USA = QTTestUtils.createMockQTCompany('QlikTech Inc', 'USA');
        QTCompany[10] = qtComp_USA.Id;
        QlikTech_Company__c qtComp_IND = QTTestUtils.createMockQTCompany('QlikTech India Pvt Ltd', 'IND');
        QTCompany[11] = qtComp_IND.Id;
        QlikTech_Company__c qtComp_PSE = QTTestUtils.createMockQTCompany('QlikTech Int.Markets AB', 'PSE');
        QTCompany[12] = qtComp_PSE.Id;
        QlikTech_Company__c qtComp_MDV = QTTestUtils.createMockQTCompany('QlikTech International AB', 'MDV');
        QTCompany[13] = qtComp_MDV.Id;
        QlikTech_Company__c qtComp_JPN = QTTestUtils.createMockQTCompany('QlikTech Japan KK', 'JPN');
        QTCompany[14] = qtComp_JPN.Id;

        QlikTech_Company__c qtComp_ARG = QTTestUtils.createMockQTCompany('QlikTech LATAM AB', 'ARG');
        QTCompany[15] = qtComp_ARG.Id;
        QlikTech_Company__c qtComp_SWE = QTTestUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE');
        QTCompany[16] = qtComp_SWE.Id;
        QlikTech_Company__c qtComp_NOR = QTTestUtils.createMockQTCompany('QlikTech Norway AS', 'NOR');
        QTCompany[17] = qtComp_NOR.Id;
        QlikTech_Company__c qtComp_THA = QTTestUtils.createMockQTCompany('QlikTech Singapore Pte Ltd', 'THA');
        QTCompany[18] = qtComp_THA.Id;
        QlikTech_Company__c qtComp_GBR = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR');
        QTCompany[19] = qtComp_GBR.Id;

        QlikTech_Company__c qtComp_ITA = QTTestUtils.createMockQTCompany('QlikTech Italy S.r.l.', 'ITA');
        QTCompany[20] = qtComp_ITA.Id;
        QlikTech_Company__c qtComp_MEX = QTTestUtils.createMockQTCompany('QlikTech Mexico', 'MEX');
        QTCompany[21] = qtComp_MEX.Id;
        QlikTech_Company__c qtComp_POL = QTTestUtils.createMockQTCompany('QlikTech Poland', 'POL');
        QTCompany[22] = qtComp_POL.Id;
        QlikTech_Company__c qtComp_AUT = QTTestUtils.createMockQTCompany('QlikTech Austria', 'AUT');
        QTCompany[23] = qtComp_AUT.Id;
        QlikTech_Company__c qtComp_CHE = QTTestUtils.createMockQTCompany('QlikTech Switzerland', 'CHE');
        QTCompany[24] = qtComp_CHE.Id;
        //this is a fake company used to test the email sending part of the trigger i.e when we have a QlikTech company with no coresponding support office
        QlikTech_Company__c qtComp_XXX = QTTestUtils.createMockQTCompany('QlikTech TestXXX', 'XXX');
        QTCompany[25] = qtComp_XXX.Id;
        
        
        //Create some Support Offices
        List<Support_Office__c> spList = new List<Support_Office__c>();
        spList.add(new Support_Office__c( Name = 'Sydney'));
        spList.add(new Support_Office__c( Name = 'Lund'));
        spList.add(new Support_Office__c( Name = 'Raleigh'));
        //spList.add(new Support_Office__c( Name = 'Dusseldorf'));
        spList.add(new Support_Office__c( Name = 'Tokyo'));
        insert spList;

        //Initialise List with the correct support office for the QlikTech Company
        List<String> SupportOfficeList = new String[26]; 
        SupportOfficeList[0] = 'Sydney'; 
        SupportOfficeList[1] = 'Lund'; 
        SupportOfficeList[2] = 'Raleigh'; 
        SupportOfficeList[3] = 'Raleigh'; 
        SupportOfficeList[4] = 'Lund'; 
        
        SupportOfficeList[5] = 'Lund'; 
        SupportOfficeList[6] = 'Lund'; 
        //SupportOfficeList[7] = 'Dusseldorf'; 
        SupportOfficeList[7] = 'Lund';
        SupportOfficeList[8] = 'Sydney'; 
        SupportOfficeList[9] = 'Lund'; 
        
        SupportOfficeList[10] = 'Raleigh'; 
        SupportOfficeList[11] = 'Sydney'; 
        SupportOfficeList[12] = 'Lund'; 
        SupportOfficeList[13] = 'Lund'; 
        SupportOfficeList[14] = 'Tokyo'; 
        
        SupportOfficeList[15] = 'Raleigh'; 
        SupportOfficeList[16] = 'Lund'; 
        SupportOfficeList[17] = 'Lund'; 
        SupportOfficeList[18] = 'Sydney'; 
        SupportOfficeList[19] = 'Lund';
         
        SupportOfficeList[20] = 'Lund'; 
        SupportOfficeList[21] = 'Raleigh'; 
        SupportOfficeList[22] = 'Lund'; 
        SupportOfficeList[23] = 'Lund';
        SupportOfficeList[24] = 'Lund';
        SupportOfficeList[25] = null;
         
        Test.startTest(); 
        
        //Create and insert the Accounts 
        List<Account> accList = new List<Account>();
        string accName = ''; 
        FOR (Integer I = 0; I<QTCompany.size(); I++) { 
            accName = 'Test Account for SetSupportOffice' + I; 
            accList.add(new Account( Name = accName, Navision_Status__c = 'Customer', Billing_Country_Code__c = QTCompany[I]) );
        } 
        insert accList; 
        
        //Read all the Accounts except the last one and check to see if the Support Office field has been populated
        Account testAct; 
        Support_Office__c SupportOffice; 
        FOR (Integer I = 0; I<QTCompany.size()-1; I++) { 
            accName = 'Test Account for SetSupportOffice' + I; 
            System.Debug('testSetSupportOffice: accName is: ' + accName); 
        
            testAct = [select Id, Name, Support_Office__c from Account where Name = :accName];
            SupportOffice = [select Id, Name from Support_Office__c where Name = :SupportOfficeList[I]];
            
            System.Debug('testSetSupportOffice: testAct.Support_Office__c is: ' + testAct.Support_Office__c);
            System.Debug('testSetSupportOffice: SupportOffice.Id is: ' + SupportOffice.Id);
            System.assertEquals(testAct.Support_Office__c, SupportOffice.Id); 
        } 
        Test.stopTest(); 
    } */
}