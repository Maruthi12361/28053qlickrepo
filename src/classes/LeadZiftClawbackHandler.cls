/******************************************************

    Class: LeadZiftClawbackHandler
    
    Initiator: Björn Andersson
    
    Changelog:
        2017-07-04  BAD             Handler class for LeadZift trigger
                                    Handles the clawback process for PLM
                                    - Set Record type Partner Opp reg back to a Standard Lead
                                    - Set OM Queue through Assignemt Rule 
                                    - Set Lead Status
******************************************************/                                 
public class LeadZiftClawbackHandler {
    public LeadZiftClawbackHandler() {
    }

    public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {


        if(!Semaphores.TriggerHasRun('LeadZiftClawback')) 
        {
            System.debug('BAD start clawback');
            Set<Id> leadIDs = new Set<Id>();

            for(integer i=0; i < triggerNew.size(); i++)
            {
                System.debug('BAD old trigger zift status' + triggerOld[i].Zift_Lead_Status__c);
                System.debug('BAD new trigger zift status' + triggerNew[i].Zift_Lead_Status__c);
                if(triggerOld[i].Zift_Lead_Status__c != triggerNew[i].Zift_Lead_Status__c && triggerNew[i].Zift_Lead_Status__c == 'Closed - Not Converted')
                {
                    leadIDs.Add(triggerNew[i].Id);
                }
            }

            if(leadIDs.size() > 0){
                System.debug('BAD leads to clawback');
                List<Lead> Leads = new List<Lead>();
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule = true;             
                ZiftSettings__c csZift = ZiftSettings__c.getInstance();
                string followUpRejectedReason = '';

                for (Lead l : [select Id, OwnerID, Status, Follow_Up_Rejected_Reason__c, Zift_Unqualified_Invalid_Reason__c, Number_of_Follow_Up_Emails__c, Number_of_Follow_Up_Voicemails__c  from Lead where Id in :leadIDs])
                {
                    System.debug('BAD Zift reject reason ' + l.Zift_Unqualified_Invalid_Reason__c);
                    //Already Working With Another Partner
                    if(l.Zift_Unqualified_Invalid_Reason__c == (csZift.InvReason_AlreadyWorkingPart__c != null ? csZift.InvReason_AlreadyWorkingPart__c : ''))
                    {
                        System.debug('BAD Already Working With Another Partner');
                        l.Status = 'Follow-Up Disqualified';    
                        l.Follow_Up_Disqualified_Reason__c = 'Already Working';
                        l.RecordTypeId = csZift.RT_StandardLead__c;
                        l.setOptions(dmo);
                        Leads.add(l);
                    }
                    //Already Working With Qlik
                    else if(l.Zift_Unqualified_Invalid_Reason__c == (csZift.InvReason_AlreadyWorkingQlik__c != null ? csZift.InvReason_AlreadyWorkingQlik__c : ''))
                    {
                        System.debug('BAD Already Working With Qlik');
                        l.Status = 'Follow-Up Disqualified';    
                        l.Follow_Up_Disqualified_Reason__c = 'Already Working';
                        l.RecordTypeId = csZift.RT_StandardLead__c;
                        l.setOptions(dmo);
                        Leads.add(l);
                    }
                    //Incorrect Data/Junk
                    else if(l.Zift_Unqualified_Invalid_Reason__c == (csZift.InvReason_IncorrectDataJunk__c != null ? csZift.InvReason_IncorrectDataJunk__c : ''))
                    {
                        System.debug('BAD junk');
                        l.Status = 'Junk';  
                        l.RecordTypeId = csZift.RT_Archived__c;
                        l.OwnerID = csZift.LeadOwnerArchivedRT__c;
                        Leads.add(l);
                    }
                    //Lost to Qlik Competitor
                    else if(l.Zift_Unqualified_Invalid_Reason__c == (csZift.InvReason_LostToComp__c != null ? csZift.InvReason_LostToComp__c : ''))
                    {
                        System.debug('BAD Lost to Qlik Competitor');
                        l.Status = 'Follow-Up Disqualified';    
                        l.Follow_Up_Disqualified_Reason__c = 'No Interest';
                        l.RecordTypeId = csZift.RT_StandardLead__c;
                        l.setOptions(dmo);
                        Leads.add(l);
                    }
                    //No Budget or Interest
                    else if(l.Zift_Unqualified_Invalid_Reason__c == (csZift.InvReason_NoBudget__c != null ? csZift.InvReason_NoBudget__c : ''))
                    {
                        System.debug('BAD No Budget or Interest');
                        l.Status = 'Follow-Up Disqualified';    
                        l.Follow_Up_Disqualified_Reason__c = 'No Interest';
                        l.RecordTypeId = csZift.RT_StandardLead__c;
                        l.setOptions(dmo);
                        Leads.add(l);
                    }
                    //Failed to Reach Contact
                    else if(l.Zift_Unqualified_Invalid_Reason__c == (csZift.InvReason_FailReachContact__c != null ? csZift.InvReason_FailReachContact__c : ''))
                    {
                        System.debug('BAD Failed to Reach Contact');
                        l.Status = 'Follow-Up Disqualified';    
                        l.Follow_Up_Disqualified_Reason__c = 'No Response';
                        l.Number_of_Follow_Up_Emails__c = 2;
                        l.Number_of_Follow_Up_Voicemails__c = 6;
                        l.RecordTypeId = csZift.RT_StandardLead__c;
                        l.setOptions(dmo);
                        Leads.add(l);
                    }
                    //Already Working With Prospect or Customer
                    else if(l.Zift_Unqualified_Invalid_Reason__c == (csZift.InvReason_AlreadyWorkingProspOrCust__c != null ? csZift.InvReason_AlreadyWorkingProspOrCust__c : ''))
                    {
                        System.debug('BAD Already Working With Prospect or Customer');
                        l.Status = 'Follow-Up Rejected';    
                        l.Follow_Up_Rejected_Reason__c = 'Already Working';
                        Leads.add(l);
                    }
                }

                if (Leads.size() > 0)
                {
                    update Leads;
                }               
            }
        }
    }

}