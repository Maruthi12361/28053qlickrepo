/**
* PseProjectAgeingUpdateBatchJob
*
* Description:	Job to calculate Recent_Timecard_End_Date field for pse__Proj__c.
*
* Added: 		2019-05-22 - ext_vos - CHG0036145
*
* Change log:
*   2019-09-06 ext_ciz IT-1821 change query
*/
global without sharing class PseProjectAgeingUpdateBatchJob implements Database.Batchable<sObject>, Database.Stateful {
    global String query;

    global PseProjectAgeingUpdateBatchJob() {
        query = 'select Id from pse__Proj__c where Recent_Timecard_End_Date__c = null';
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<pse__Proj__c> scope) {
        List<Id> projectIds = new List<Id>();
        for (pse__Proj__c pr : scope) {
            projectIds.add(pr.Id);
        }
        if (!projectIds.isEmpty()) {
            PseTimecardHeaderTriggerHandler.updateRecentTimecardEndDateForProject(projectIds);
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
}