/*
    Initial development: MTM
    2015-02-06  MTM CR# 23458  AlertOnKeyExpiry.
    2015-22-12 Andreas - Not sure why the change was made
    2015-17-02 DNN - TO enhance the email with links for Account and Opportunity
    2016-13-2016 - NAD - Removed Responsible_Partner__r references per CR# 33068
*/
global class AlertOnKeyExpiry implements Database.Batchable<Account_License__c>, Schedulable//, Database.AllowsCallouts
{
    //Pass the number of weeks to license expiry
    global AlertOnKeyExpiry (Integer numberOfWeeks)
    {
        System.Debug('Starting: AlertOnKeyExpiry');
        number_OfWeeks = numberOfWeeks;
        System.Debug('number_OfWeeks = ' + number_OfWeeks);
    }
    
    Integer number_OfWeeks;
    Static String message = '';
    
    //Get a list of Account_Licenses about to expire in "numberOfWeeks"
    global Iterable<Account_License__c> start(Database.BatchableContext BC) {        
        // Get all Evaluation Keys about to expire
        System.Debug('Starting: AlertOnKeyExpiry :Start collecting Account Licenses');
        List<Account_License__c> licenses = new List<Account_License__c>();
        System.Debug('In Start: number_OfWeeks = ' + number_OfWeeks);
        Integer numberOfDays = 5;//number_OfWeeks * 7;       
        date comparedate = Date.today().addDays(numberOfDays);
        System.Debug('comparedate = ' + comparedate);
        licenses = [Select a.Time_Limit__c,
                      a.Opportunity__r.Sell_Through_Partner__r.Owner.Email,
                      a.Opportunity__r.Sell_Through_Partner__r.Name,
                      a.Opportunity__r.Sell_Through_Partner__r.Owner.FirstName,
                      a.Opportunity__r.Revenue_Type__c,  
                      a.Opportunity__r.End_User_Account_del__r.Name,
                      a.Opportunity__r.Account.Name,
                      a.Opportunity__r.Account.Owner.FirstName,
                      a.Opportunity__r.Account.Owner.Email,
                      a.Support_To__c,
                      a.Special_Edition__c,
                      a.Name,
                      a.INT_NetSuite_InternalID__c,
                      a.Description__c,
                      a.Account__r.OwnerId,
                      a.License_Responsible_Partner__c,
                      a.Account__r.Name,
                      a.Account__r.Owner.Email,
                      a.Account__r.Owner.FirstName,
                      a.Account__c
                      From Account_License__c a                      
                     WHERE Time_Limit__c != null
                     AND Time_Limit__c = :comparedate];                     
        System.Debug('Licenses count = ' + licenses.size());
        return licenses;                  
    }
    
    public String FromAddress 
    {
        get
        {
            String fromId = '0D2D0000000GndJKAS';
            for(OrgWideEmailAddress owa : [select id, DisplayName, Address from OrgWideEmailAddress where DisplayName like 'noreply%'])
            {
                if(owa.DisplayName.contains('noreplay'))
                {
                    fromId = owa.Id;
                    System.Debug('Got from address Id of = ' + owa.DisplayName);
                }
            }
            return fromId;
        }
    }
    //Create mail messages. Direct deals will go to Account owner. Reseller ones goes to Responsible Partner owner  
    global void execute(Database.BatchableContext BC, List<Account_License__c> scope) {
        
        System.Debug('Starting: execute alert on key expiry ');
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        //Create mail messages. Direct deals will go to Account owner. Reseller ones goes to Responsible Partner owner  
        for (Account_License__c a: scope)
        {
            try
            {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                string emailText = '';
				String toaddress = a.Account__r.Owner.Email;                
                if(a.Opportunity__r != null)
                {
                    if(a.Opportunity__r.Revenue_Type__c.containsIgnoreCase('OEM'))
                    {                    
                        emailText = FormatEmailText(a, true, false);
                        toaddress = a.Opportunity__r.Account.Owner.Email;
                    }
                    else if (a.Opportunity__r.Sell_Through_Partner__c != null)
                    {
                        emailText = FormatEmailText(a, false, true);
                        toaddress = a.Opportunity__r.Sell_Through_Partner__r.Owner.Email;                    
                    }
                    else
                    {
                        emailText = FormatEmailText(a, false, false);
                    }
                                    
                    if(String.isNotBlank(toaddress))
                    {  
                        String[] toAddresses = new String[]{toaddress};                            
                        mail.setSubject(Label.LicenseExpiryAlertSubject);        
                        mail.setToAddresses(toAddresses);
                        mail.setOrgWideEmailAddressId(FromAddress);
                        mail.setHtmlBody(emailText); 
                        mails.add(mail);
                    }
                }
            }
            catch(Exception e)
            {
                System.debug('Error on email' + e.getMessage());
            }
        }
        Messaging.sendEmail(mails);
        
        System.Debug('Finishing: execute alert on key expiry ');    
    }
    
    private String FormatEmailText(Account_License__c a, boolean isOEM, boolean isPartner)
    {
        try
            {
                String ownerName = '', accountName = '', partnerName = '', tempString = '', format = '';                                
                accountName = a.Account__r.Name;
                String specialEdition = a.Special_Edition__c !=null?a.Special_Edition__c:'';
                String endDate = a.Time_Limit__c.format();
                ownerName =  a.Account__r.Owner.FirstName;
                tempString = String.format(Label.LicenseExpiryAlertSub1, new List<String> {endDate});                
                if(isOEM)
                {
                    partnerName = a.Opportunity__r.Account.Name;
                    ownerName = a.Opportunity__r.Account.Owner.FirstName;                    
                }
                else if (isPartner)
                {
                    ownerName = a.Opportunity__r.Sell_Through_Partner__r.Owner.FirstName;
                    partnerName = a.Opportunity__r.Sell_Through_Partner__r.Name;                    
                }
                if (isOEM || isPartner)
                {
                    tempString = String.format(Label.LicenseExpiryAlertSub , new List<String> {partnerName,  endDate});
                }

                // CR# 41632 - Adding account and opp links in email
                String accountLink=String.Format('To View Account please <a href={0}/{1}> Click here.</a> <br>', new String[] {System.URL.getSalesforceBaseURL().toExternalForm(),a.Account__r.Id});
                String oppLink=String.Format('To View Opportunity please <a href={0}/{1}> Click here.</a> <br>', new String[] {System.URL.getSalesforceBaseURL().toExternalForm(),a.Opportunity__r.Id});

                // CR# 41632 - Adding arguments for account and opp 
                List<String> arguments = new List<String>{ownerName, a.Name, specialEdition, accountName, tempString,accountLink,oppLink};
                format =  Label.LicenseExpiryAlert;                    
                return(String.format(format, arguments));
            }
            catch(Exception e)
            {
                System.debug('Error on email' + e.getMessage());
                return '';
            }
    }
    
    global void execute(SchedulableContext sc)
	{ 
		AlertOnKeyExpiry a = new AlertOnKeyExpiry(number_OfWeeks);
        ID batchId = Database.executeBatch(a); 
	}

	global void finish(Database.BatchableContext BC) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex AlertOnKeyExpiry status ' + a.Status);
        mail.setPlainTextBody
        ('The batch Apex job  AlertOnKeyExpiry processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
        }
        catch(System.EmailException ex) {
            system.debug('Failed to send mail, error message: ' +ex.getMessage());
        }
    } 
    
}
/*
 * To Schedule the job use
AlertOnKeyExpiry m = new AlertOnKeyExpiry(1);
String sch = '0 0 5 * * ?';
String jobID = system.schedule('AlertOnKeyExpiry Job', sch, m);

AlertOnKeyExpiry m = new AlertOnKeyExpiry(2);
String sch = '0 15 5 * * ?';
String jobID = system.schedule('AlertOnKeyExpiry Job', sch, m);

AlertOnKeyExpiry m = new AlertOnKeyExpiry(3);
String sch = '0 30 5 * * ?';
String jobID = system.schedule('AlertOnKeyExpiry Job', sch, m);

*/