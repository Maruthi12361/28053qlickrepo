/**
  20200521 : CCE DGM-55 FURRevamp This class is called from the QSBTrialGreater21Days scheduled class and checks to see if any 
             Contacts have the QSB Trial field set for more than 21 days without the Overall Follow-Up Required field being set.
**/
global class QSBTrialGreater21DaysBatchContact implements Database.Batchable<sObject>, Database.stateful {
    global final String rtBusinessContact = '01220000000J24I';
    global String query;
    global QSBTrialGreater21DaysBatchContact() {
        query = 'SELECT Id, QSB_Trial__c, QSB_Trial_Start_Date__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c, Trial_QSB_Trial_greater_21__c FROM Contact'
            + ' WHERE LastModifiedDate = LAST_N_DAYS:30 AND RecordTypeId = \'' + rtBusinessContact + '\''
            + ' AND QSB_Trial__c = true AND Overall_Follow_up_Required__c = false AND QSB_Trial_Start_Date__c != null AND QSB_Trial_Start_Date__c != LAST_N_DAYS:21'
            + ' AND IsDeleted = false';

        if (test.isRunningTest()) {
        query = 'SELECT Id, QSB_Trial__c, QSB_Trial_Start_Date__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c, Trial_QSB_Trial_greater_21__c FROM Contact'
            + ' WHERE Firstname = \'Chris\' and LastName = \'QSBTrialGreater21DaysBatchContact\'';
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Database.QueryLocator ql = Database.getQueryLocator(query);
        return ql;
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        for (Contact c : scope) {
            c.QSB_Trial__c = false; 
            c.Trial_QSB_Trial_greater_21__c = true;            
        }
        System.debug('QSBTrialGreater21DaysBatchContact: scope size = ' + scope.size());
        Database.update(scope, false);
    }

    global void finish(Database.BatchableContext BC) {
    }
}