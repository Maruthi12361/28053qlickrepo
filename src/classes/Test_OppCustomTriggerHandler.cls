/****************************************************************
*
*  Test_OppCustomTriggerHandler 
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*****************************************************************/
@isTest
public class Test_OppCustomTriggerHandler {
	static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    public static testmethod void processTest() {
         QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
		/*
        Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
			*/
			Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAccount;
		Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
       List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
       insert listOfcreate;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        //Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
       //     insert  testAccount;
        Account testAccount2 = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount2;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Contact testContact2 = QuoteTestHelper.createContact(testAccount.id);
            insert testContact2;
        RecordType rType = [Select id From RecordType Where developerName = 'Sales_QCCS']; 
         
        Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', rType);
        insert oppCreatedForUpdate;
        oppCreatedForUpdate.CloseDate = Date.today().addDays(10); 
        oppCreatedForUpdate.AccountId = testAccount2.id;
        update oppCreatedForUpdate;
    	}
    }
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
            								Country_Code_Two_Letter__c = countryAbbr, 
            								Country_Name__c = countryName, 
            								Subsidiary__c = subsId);
    return qlikTechIns;
    }
}