public with sharing class MyCustomLookupController {

   public Influencer__c Influencer { get; set; }

   public Id accountplanid { get; set; }
      
   public MyCustomLookupController () {
       accountplanid = System.currentPageReference().getParameters().get('id');
       Influencer = new Influencer__c(Account_Plan__c = accountplanid );
       
   }
    
  
      
       
   
 
 

    public PageReference save() {
     Contact c = [SELECT Job_title__c,name FROM Contact WHERE Id = :Influencer.Contact__c];  
     system.debug('enrico'+c);
       
        //SAVE JOB TITLE CONTACT SELECTED
        Influencer.Title__c = c.Job_title__c;
        influencer.name=c.name;
        insert Influencer;
        PageReference acctPage = new PageReference('/' + Influencer.id);
        acctPage.setRedirect(true);
        return acctPage;
    }
    
    
       public PageReference cancel() {
      
        PageReference aPage = new PageReference('/' + accountplanid);
        aPage.setRedirect(true);
        return aPage;
    }
    

    
    
 
}