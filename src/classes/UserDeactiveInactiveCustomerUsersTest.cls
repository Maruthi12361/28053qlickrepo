/**************************************************************************************************** 
   UserDeactiveInactiveCustomerUsersTest
   IT-739 : Test class for UserDeactiveInactiveCustomerUsers
   IT-1013: Test for scheduled
****************************************************************************************************/
@isTest 
public with sharing class UserDeactiveInactiveCustomerUsersTest {
	static final Id SysAdminProfileId = '00e20000000yyUzAAI';
    static final Id PortalUserRoleId = '00E20000000vrJSEAY';

	static testMethod void testFlow() {

        QTTestUtils.GlobalSetUp();
        
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator

		User u = new User(
                        Alias = 'adminUsr', Email = 'adminUser@tjtest.com.test',
                        Emailencodingkey = 'UTF-8', FirstName = 'Andreas', LastName = 'UserDeactiveInactiveCustomerUsers',
                        Languagelocalekey = 'en_US', LocalesIdKey = 'en_US',
                        Profileid = SysAdminProfileId,
                        Timezonesidkey = 'America/Los_Angeles',
                        //ContactId = cont.Id,
                        UserRoleId = PortalUserRoleId, isActive = true,
                        Username = System.now().millisecond() + '_' + '_newuser@jttest.com.test');

		insert u;

		test.startTest();
		Database.executeBatch(new UserDeactiveInactiveCustomerUsers(1), 200);
		test.stopTest();

		u = [select id, isActive from User where FirstName = 'Andreas' and LastName = 'UserDeactiveInactiveCustomerUsers'];

		System.assert(u.isActive == false, 'user shouln\'t be active');
	}

    static testMethod void testRunBatch() {
        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.LimitForInactivationCustomerUsers__c = 2;
        insert settings;

        InactiveCustomerUsers_RunBatch csh1 = new InactiveCustomerUsers_RunBatch();

        Test.StartTest();
        csh1.execute();
        Test.stopTest();
    }
}