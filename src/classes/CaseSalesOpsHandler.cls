/*
 File Name: CaseSalesOpsHandler
 Moved from trigger CaseSalesOps on Case (before insert)

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.

*/

public with sharing class CaseSalesOpsHandler {

    public static void handleCaseSalesOps(List<Case> newCases) {

        Map<String, String> recordtypeEmails = new Map<String, String>{
                'Sales_Ops_Training' => 'salesopstraining@qlikview.com',
                'Sales_Ops_Internal_Apps' => 'salesopsinternalapps@qlikview.com',
                'Sales_Ops_Data' => 'salesopsdata@qlikview.com',
                'Sales_Ops_Requests' => 'globalsalesops@qlik.com',
                'Sales_Ops_APAC_Requests' => 'apacops@qlikview.com',
                'Sales_Ops_Americas_Requests' => 'americas_salesops@qliktech.com',
                'EducationServicesOperations' => 'DL-GlobalEducationServicesOperations@qlik.com',    //CR 17120
                'Sales_Ops_QlikHelp' => 'qlikhelp@qlikview.com',
                'SI_Partner_Presales_Support' => 'eu.presales.support@qlikview.com',
                'Sales_Ops_EMEA_Requests' => 'emeaops@qlik.com',
                'Sales_Ops_Beermea_Requests' => 'beermeaops@qlik.com',
                'ConsultingServiceOperations' => 'DL-GlobalConsultingServicesOperations@qlik.com'    //CR 17120
        };

        Set<Id> recordTypeIds = new Set<Id>();
        for (Case c : newCases) {
            recordTypeIds.add(c.RecordTypeId);
        }
        Map<Id, RecordType> recordTypes = new Map<Id, RecordType>([
                SELECT Id, DeveloperName
                FROM RecordType
                WHERE Id IN :recordTypeIds
        ]);

        for (Case c : newCases) {
            String developerName = recordTypes.get(c.RecordTypeId).DeveloperName;
            if (developerName == 'Sales_Ops_Training' ||
                    developerName == 'Sales_Ops_Internal_Apps' ||
                    developerName == 'Sales_Ops_Data' ||
                    developerName == 'Sales_Ops_Requests' ||
                    developerName == 'Sales_Ops_APAC_Requests' ||
                    developerName == 'Sales_Ops_Americas_Requests' ||
                    developerName == 'EducationServicesOperations' ||    //CR 17120
                    developerName == 'Sales_Ops_QlikHelp' ||
                    developerName == 'Sales_Ops_EMEA_Requests' ||
                    developerName == 'Sales_Ops_Beermea_Requests' ||
                    developerName == 'SI_Partner_Presales_Support' ||
                    developerName == 'ConsultingServiceOperations') {     //CR 17120

                c.Case_Origin__c = recordtypeEmails.get(developerName);

              List<Contact> contacts = [
                        SELECT Name, Account.Name, MobilePhone
                        FROM Contact
                        WHERE Email = :c.SuppliedEmail
                ];
                Contact contact = null;
                if (contacts.size() <= 0) {
                    List<User> users = [
                            SELECT Name, MobilePhone, LastName, FirstName
                            FROM User
                            WHERE Email = :c.SuppliedEmail
                    ];
                    if (users.size() > 0) {
                        User user = users[0];
                        contact = new Contact(
                                MobilePhone = user.MobilePhone,
                                Email = c.SuppliedEmail,
                                LastName = user.LastName,
                                FirstName = user.FirstName
                        );
                        insert contact;
                    }
                } else {
                    contact = contacts[0];
                }

                if (contact == null) {
                    contact = new Contact(
                            Email = c.SuppliedEmail,
                            LastName = c.SuppliedEmail
                    );
                    insert contact;
                }

                c.ContactId = contact.Id;
            }
        }
    }
}