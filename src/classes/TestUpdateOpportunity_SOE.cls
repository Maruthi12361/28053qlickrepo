@isTest
/*
 Author - Jina Chetia
 Company - Bluewolf
 Date - 09/09/2009

    2014-06-18  MHG     Fixed broken test method due to new forecasting
    2015-04-20  SLH     Added function and solution area for Op006 validation rule
	2020-07-09 EXTCDX  Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
				"Deployment Preference" is required in Goal Confirmed stage  

*/
private class TestUpdateOpportunity_SOE 
{
    static testMethod void test_UpdateOpportunity_SOE_1()
    {
                Profile p = [select id from profile where name='System Administrator']; 
          User firstUser = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');
        System.RunAs(firstUser)
        {
   
        QTTestUtils.GlobalSetUp();
        List<Sequence_of_Event__c> seqList = new List<Sequence_of_Event__c>();              

        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
        insert QTComp;
        Account acc = new Account();
        acc.Name = 'Test';
        acc.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
        acc.Billing_Country_Code__c = QTComp.Id;
        insert acc; 
        
        // MHG 2010-08-09 :
        // We need to create our own opp here so that we know which account is
        // used. If it is a Partner or customer account it will generate creation
        // of sharing rules. This will end up with lack of SOQL.
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test Opportunity 1';
        Opp.OwnerId = firstUser.id;
		Opp.Deployment_Preference__c = 'On-Premise'; //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        Opp.StageName = 'Goal Confirmed';
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.trigger
        Opp.CloseDate = Date.today();
        //Opp.AccountId = '0012000000UKxew';  // Swedbank Skog & Lantbruk AB (nither customer or partner)
        Opp.AccountId = acc.Id;
        Opp.Opportunity_Origin__c = 'SI Driven Solution';
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Included_Products__c = 'Qlik Sense';
         Test.startTest();
        insert Opp; 
                
        //Test Insert Sequence of Events
        for(Integer i=0; i<10; i++)
        {
           Sequence_of_Event__c seq = new Sequence_of_Event__c(Opportunity_ID__c = opp.Id, S_o_E_Date__c=System.today(), Checkpoint__c='Complete', Type__c = 'Customer', Proposed_Event__c = 'Other');
           seqList.add(seq);
        }
       
       
       
        insert seqList;
 
        List<Sequence_of_Event__c> seqUpList = new List<Sequence_of_Event__c>();
        List<Sequence_of_Event__c> seqUpdate = [select Id, Opportunity_ID__c, Type__c, Checkpoint__c from Sequence_of_Event__c where Opportunity_ID__c = :Opp.Id LIMIT 4];
        
        //Test Update and Delete Sequence of Events
        for(Sequence_of_Event__c seqUp: seqUpdate)
        {
            seqUp.Type__c = 'Qliktech';
            seqUpList.add(seqUp);
        } 
                        
        update seqUpList;

        delete seqUpList;

        Test.stopTest();
    }
}
    
}