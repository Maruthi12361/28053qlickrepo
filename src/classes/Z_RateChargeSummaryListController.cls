/*
    1.0, 03/01/2019, UIN, BSL-1265 Extension  class for new page Z_RateChargeSummaryList

    Change log:
    2019-03-14 - Linus Löfberg - BSL-1696
    2020-05-07 - DKF - DTR-185
    2020-06-08 - DOB - DTR-479
*/

public with sharing class Z_RateChargeSummaryListController {

    public List<zqu__QuoteChargeSummary__c> qChargeSummaries { get; private set; }
    public zqu__quote__c quote;

    public Z_RateChargeSummaryListController(ApexPages.StandardController stdController) {
        this.quote = (zqu__quote__c)stdController.getRecord();

        qChargeSummaries = [
        SELECT
        Id,
        Name,
        zqu__Product_Name__c,
        zqu__Description__c,
        zqu__QuoteRatePlan__r.Name,
        New_Quantity__c,
        zqu__Quantity__c,
        Previous_Quantity__c, 
        zqu__ListTotal__c,
        zqu__Discount__c,
        zqu__BillingDiscount__c,
        zqu__TotalPrice__c,
        zqu__BillingTotal__c,
        zqu__TCV__c,
        zqu__PreviewedTCV__c
        FROM
        zqu__QuoteChargeSummary__c
        WHERE
        Parent_Quote__c = :quote.Id ORDER BY zqu__QuoteRatePlan__r.Name];
    }
}