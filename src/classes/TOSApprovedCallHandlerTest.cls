/**************************************************************** 
* 
* ToSRegistrationAutomationTest.class
* 
* Test class for TOSApprovedCallHandler.class.
* 
* 2018-02-01 AYS : Test class created. Coverage: 100%
*****************************************************************/ 
@isTest
private class TOSApprovedCallHandlerTest {
	
	@isTest
	static void test_TOSApprovedCallHandlerTest() {

		TOS_Registration__c tosObject = new TOS_Registration__c();

		tosObject.Bill_to_First_Name__c = 'TFirst';
		tosObject.Bill_to_last_name__c = 'TLast';
		tosObject.Bill_to_Company__c = 'Test Company';
		tosObject.Billto_Address_Line_1__c = '1 Testing Lane';
		tosObject.Billto_Country__c = 'Finland';
		tosObject.Billto_City__c = 'Lahti';
		tosObject.Billto_Country_Code__c = 'FI';
		tosObject.Billto_Post_Code__c = '15140';
		tosObject.TOS_Legal_Approval_Status__c = 'Legal Approval Required';
		tosObject.Schedule_Start_date__c = system.today();
		tosObject.TOS_Contact_First_Name__c = 'TTFirst';
		tosObject.TOS_Contact_Last_name__c = 'TTLast';
		tosObject.TOS_Contact_Email__c = 'test@test.com';
		insert tosObject;

		tosObject.TOS_Legal_Approval_Status__c = 'Legal Approval Granted';
		update tosObject;

		Test.startTest();	

		Test.setMock(
			HttpCalloutMock.class, 
			new GeneralHttpCalloutMock(200, 'OK', '{"status":"ok"}', 
			new  Map<String, String> {'Content-Type' => 'application/json'}));

		//Call futere method with calling web servise separatelly from previouse DML operation in trigger in order to avoid error
		TOSApprovedCallHandler.call(new Set<Id>{tosObject.Id});

		Test.stopTest();

		tosObject = [SELECT id, Litmos_Response__c FROM TOS_Registration__c WHERE Id =: tosObject.id];

		System.assertEquals('200: OK: {"status":"ok"}', tosObject.Litmos_Response__c);
		

	}

}