/*
Name:  EntitlementSyncBatchable.cls
Copyright © 2011  salesforce.com consulting
======================================================
======================================================
Purpose:
-------
======================================================
======================================================
History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     Mark Cane&  2011-10-07  Initial development.
2.0		RDZ			2012-07-20	CR# 5568 https://eu1.salesforce.com/a0CD000000NI3h1
*								Add check on start/end date to Entitlement update classes
3.0		YKA			2013-04-15	CR# 7833 https://eu1.salesforce.com/a0CD000000XPrg9
*								Standard Support should be default when creating Entitlements
4.0		NAD 		2016-04-13  Replaced Account License Support_Level__c references with INT_NetSuite_Support_Level__c per CR 33068.
*/
global class EntitlementSyncBatchable implements Database.Batchable<sObject>{

	private final ID standardEntitlementProcessId;
    private final ID premiumEntitlementProcessId;
    private final ID twentyFourSevenBusinessHoursId;
    private final ID defaultBusinessHoursId;
    private final String standardSupportLevel;
	private final String premiumSupportLevel;
	private final String holdAccountName;
	
	public EntitlementSyncBatchable(){
		//& Retrieve configuration settings at the process level.
    	Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
		    
		this.standardEntitlementProcessId = [select Id from SlaProcess where name =:es.Standard_Entitlement_Process_Name__c limit 1].Id;
	    this.premiumEntitlementProcessId = [select Id from SlaProcess where name =:es.Premium_Entitlement_Process_Name__c limit 1].Id;
	    this.twentyFourSevenBusinessHoursId = [select Id from BusinessHours where name=:es.X24x7_Business_Hours_Name__c limit 1].Id;
	    this.defaultBusinessHoursId = [select Id from BusinessHours where isDefault=true limit 1].Id;
	    
	    this.premiumSupportLevel = es.Premium_Support_Level__c;
	    this.standardSupportLevel = es.Standard_Support_Level__c;
	    this.holdAccountName = es.Hold_Account_Name__c;
	}
	
    global Database.QueryLocator start(Database.BatchableContext BC){
    	
    	//& Gather Account License records modified today - the assumption is that the sync process will run before midnight.
    	//& Account License records related to Account License Groups that have changed are also gathered.
    	//& Note: the integration process (Rapidi Online) updates the Account License table every 6 hours.
    	//& Note: the integration process parents licenses with a holding account where no actual account parent can be identified.
        return Database.getQueryLocator([select a.Id, a.INT_NetSuite_Support_Level__c, a.Support_To__c, a.Support_From__c, a.Name, a.Account__c, a.Account_License_Group__r.Support_Level__c,
        										 a.Account_License_Group__r.Expiration_Date__c, a.Account_License_Group__r.Name, a.Account_License_Group__c 
        										 from Account_License__c a
        										 where /*(a.LastModifiedDate=today or a.Account_License_Group__r.LastModifiedDate=today)*/ 
        										 /*and*/ a.Account__c!=null and a.Account__r.Name!=:holdAccountName]);
    }
        
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        List<Account_License__c> changedLicenses = (List<Account_License__c>)scope;        
        /*
        ** FACT Existing Cases are unaffected by changes to the entitlement post SLA entry (i.e. relationship created).
        ** Premium support level provides 24/7 business hours otherwise the Account support office business hours are used.
        ** Less than 1000 per day anticipated volume.
        ** Compound Key is unique - 204k approx in production 2011-10-12.
        */
 		List<Entitlement> newEntitlements = new List<Entitlement>(); //& AL but no E.
 		List<Entitlement> updateEntitlements = new List<Entitlement>(); //& AL and E.
     	List<Entitlement> expireEntitlements = new List<Entitlement>(); //& E but no AL.
    	
     	Map<ID, ID> accountToBusinessHours = new Map<ID, ID>();
     	
     	//& Store Account to BusinessHours relationship.
        for (Account_License__c al : changedLicenses){
        	accountToBusinessHours.put(al.Account__c, null);
        }
        
        for (Account a : [select a.Id, a.Support_Office__r.Business_Hours__c from Account a where Id in:accountToBusinessHours.keySet()]){        	
        	accountToBusinessHours.put(a.Id, a.Support_Office__r.Business_Hours__c);
        }
        
        Map<ID, Entitlement> licenseToEntitlement = new Map<ID, Entitlement>();
        
        //& Store Account_License__c to Entitlement relationship - where exists.
        for (Entitlement e : [select Id, Account_License__c from Entitlement where AccountId in:accountToBusinessHours.keySet()]){
        	licenseToEntitlement.put(e.Account_License__c, e);
        }
        
        //& Enumerate scope.
        for (Account_License__c al : changedLicenses)
        {
        	
        	// RDZ CR#5568: If no values exist we skip the creation of the entitlements    
            if (al.Support_From__c == null && al.Support_To__c==null) continue;
            
            // RDZ: CR#5568 check if one of the days exist we will set the entitlement enddate and startdate to the value that exists 
            if (al.Support_From__c != null && al.Support_To__c==null)
            {
            	al.Support_To__c = al.Support_From__c;
            }
            if ((al.Support_From__c == null && al.Support_To__c!=null)||(al.Support_From__c > al.Support_To__c))
            {
            	al.Support_From__c = al.Support_To__c;
            }
        	
        	// aaron: added statement to avoid error of bad dates on entitlement creation
			//if (al.Support_From__c <= al.Support_To__c){
			Entitlement e;

			if (licenseToEntitlement.containsKey(al.Id))
			{
				e = new Entitlement(ID=licenseToEntitlement.get(al.Id).Id); //& Existing Entitlement for the Account License.
			} 
			else
			{
				e = new Entitlement(); //& New Entitlement.
				e.Account_License__c = al.Id;
				e.AccountId = al.Account__c;
			}

			e.Name = al.Name;
			
			
			e.StartDate = al.Support_From__c;
			e.EndDate = al.Support_To__c;
			
			if (al.Account_License_Group__c!=null)
			{
				//& If the Account License is related to an Account License Group use the support level from the group.
				// Assigning standardEntitlementProcess as default (2013-04-15 YKA: CR#7833)
				if (al.Account_License_Group__r.Expiration_Date__c!=null && al.Account_License_Group__r.Expiration_Date__c >= Date.today())
				{
					if (al.Account_License_Group__r.Support_Level__c == premiumSupportLevel) 
					{ 
						e.SlaProcessId = premiumEntitlementProcessId; 
					} 
					else 
					{ 
						e.SlaProcessId = standardEntitlementProcessId; 
					} 
				} 
				} 
				else 
				{ 
					if (al.INT_NetSuite_Support_Level__c == premiumSupportLevel) 
					{ 
						e.SlaProcessId = premiumEntitlementProcessId; 
					} 
					else 
					{ 
						e.SlaProcessId = standardEntitlementProcessId; 
					}					
			}

			if (accountToBusinessHours.containsKey(al.Account__c) && accountToBusinessHours.get(al.Account__c)!=null)
			{
				if (al.INT_NetSuite_Support_Level__c==premiumSupportLevel)
				{
					e.BusinessHoursId = twentyFourSevenBusinessHoursId; //& Premium support equals 24x7 BusinessHours - this overrides the support office setting.
				}
				else
				{
					e.BusinessHoursId = accountToBusinessHours.get(al.Account__c);
				}
			}
			else
			{
				e.BusinessHoursId = defaultBusinessHoursId;
			}

			if (licenseToEntitlement.containsKey(al.Id))
			{
				updateEntitlements.add(e);
			} else
			{
				newEntitlements.add(e);
			}
        }
        //}
        
        //& Final step - expire any active Entitlement not in the Account License object.
        //& TODO : This requires further analysis - in case Entitlements are created manually?
        /*for (List<Entitlement> entitlements : [select Id from Entitlement where Account_License__c not in (select Id from Account_License__c) and Status='Active']){
        	for (Entitlement e : entitlements){
        		e.EndDate = Date.today();
        		expireEntitlements.add(e);
        	}
        }*/
		
		if (newEntitlements.size()>0) insert newEntitlements;
		if (updateEntitlements.size()>0) update updateEntitlements;
		if (expireEntitlements.size()>0) update expireEntitlements;        
	}
    
    global void finish(Database.BatchableContext BC){}
}