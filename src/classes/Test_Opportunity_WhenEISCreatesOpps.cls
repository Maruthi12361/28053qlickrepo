/****************************************************** 

Class: Test_Opportunity_WhenEISCreatesOpps 

This class tests the Opportunity_WhenEISCreatesOpps trigger 

Changelog: 
2012-09-25 CCE Initial creation 
2012-12-05 TJG replace CCS - Customer Centric Selling record type with Qlikbuy CCS Standard
                or Direct / Reseller - Std Sales Process
2013-07-10   SLH: Set From_Lead_Conversion__c to false in the test Opportunity to avoid OP010_Cant_Create_4_EndUserAc_PendingVal validation rule
2014-02-10	Due to role “Americas - EIS – West” has recently changed to “Americas - EIS – North West” (in CR# 10976), SOQL draws blank. 
			Use wild card to match
2014-06-04  TJG     Fix error (on QTNS)
            System.DmlException: Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY, 
            Record Type ID: this ID value isn't valid for the user: 01220000000J1KRAA0: [RecordTypeId]
2014-12-02  Commented out as no longer required, part of CR 11817 - https://eu1.salesforce.com/a0CD000000gpoDA            
******************************************************/ 
@isTest  (SeeAllData=true)
private class Test_Opportunity_WhenEISCreatesOpps {
/*    static final String QlikbuyCSSII = 'Qlikbuy CCS Standard II'; // on QTNS  2014-06-04
    public static testmethod void testOpportunity_WhenEISCreatesOpps1() { 
        // ID ID_RecordType = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Qlikbuy CCS Standard').getRecordTypeId();   commeted out on 2014-06-04
        ID ID_RecordType = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get(QlikbuyCSSII).getRecordTypeId();   // 2014-06-04
        
        //Select Profile
        Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Custom: QlikBuy Inside Sales Std User Inside View Americas' LIMIT 1];
        //Select Role - we could use any Role with "- EIS" in the name
        //2014-02-10 was UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Americas - EIS - West' LIMIT 1];
        UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name like 'Americas - EIS - % West' LIMIT 1];
        //Create User
        User testUser = Util.createUser('TestUserName', 'test69', testProfile.Id, testUserRole.Id);
        insert testUser;
        
        //RunAs User
        System.runAs(testUser)
        {
            //Create test Account
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
                Country_Name__c = 'USA'         
            );
            insert QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c, Country_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account(Name='TestAccountWhenEISCreatesOpps1', OwnerId = testUser.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, From_Lead_Conversion__c = false);
            insert testAcnt;
        
            Test.startTest();
            
            // Create Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - DeleteMe',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Identified',
                RecordTypeId = ID_RecordType,
                CurrencyIsoCode = 'GBP',
                AccountId = testAcnt.Id
            );                               
            
            insert testNewOpp;
            
            //the Test
            Opportunity Opp = [SELECT Id, StageName FROM Opportunity WHERE Id = :testNewOpp.Id];
            System.assertEquals('Goal Identified', Opp.StageName);
            Test.stopTest();             
        }
    }
    
    public static testmethod void testOpportunity_WhenEISCreatesOpps2() { 
        //ID ID_RecordType = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Qlikbuy CCS Standard').getRecordTypeId(); commeted out on 2014-06-04
        ID ID_RecordType = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get(QlikbuyCSSII).getRecordTypeId(); // 2014-06-04
        //Select Profile
        Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Custom: Marketing Std User' LIMIT 1];
        //Select Role - we could use any Role with "- EIS" in the name
        UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Americas - Marketing Manager' LIMIT 1];
        //Create User
        User testUser = Util.createUser('TestUserName', 'test70', testProfile.Id, testUserRole.Id);
        insert testUser;
        
        //RunAs User
        System.runAs(testUser)
        {
            //Create test Account
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
                Country_Name__c = 'USA'         
            );
            insert QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c, Country_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account(Name='TestAccountWhenEISCreatesOpps2', OwnerId = testUser.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, From_Lead_Conversion__c = false);
            insert testAcnt;
        
            Test.startTest();
            
            // Create Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - DeleteMe',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Identified',
                RecordTypeId = ID_RecordType,
                CurrencyIsoCode = 'GBP',
                AccountId = testAcnt.Id
            );                               
            
            insert testNewOpp;
            
            //the Test
            Opportunity Opp = [SELECT Id, StageName FROM Opportunity WHERE Id = :testNewOpp.Id];
            System.assertEquals('Goal Identified', Opp.StageName);
            Test.stopTest();             
        }
    }
    
    public static testmethod void testOpportunity_WhenEISCreatesOpps3() { 
        ID ID_RecordType = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Direct / Reseller - Std Sales Process').getRecordTypeId();
        
        //Select Profile
        Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        //Select Role - we could use any Role with "- EIS" in the name
        UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        //Create User
        User testUser = Util.createUser('TestUserName', 'test71', testProfile.Id, testUserRole.Id);
        insert testUser;
        
        //RunAs User
        System.runAs(testUser)
        {
            //Create test Account
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
                Country_Name__c = 'USA'         
            );
            insert QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c, Country_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account(Name='TestAccountWhenEISCreatesOpps', OwnerId = testUser.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, From_Lead_Conversion__c = false);
            insert testAcnt;
        
            Test.startTest();
            
            // Create Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - DeleteMe',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Identified',
                RecordTypeId = ID_RecordType,
                CurrencyIsoCode = 'GBP',
                AccountId = testAcnt.Id
            );                               
            
            insert testNewOpp;
            
            //the Test
            Opportunity Opp = [SELECT Id, StageName FROM Opportunity WHERE Id = :testNewOpp.Id];
            System.assertEquals('Goal Confirmed', Opp.StageName);
            Test.stopTest();             
        }
    }*/
}