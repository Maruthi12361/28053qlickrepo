/*
27/02/2020 shubham gupta OTQ-44 test class for QdiCheckHandler
*/
@isTest
public class Test_QdiCheckHandler {
    @isTest 
    public static void qdiCheckOnOppties(){
        QuoteTestHelper.createCustomSettings();
        	
        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        insert subs;
       	QlikTech_Company__c qCompany = QuoteTestHelper.createQlickTechCompany(subs.id);
        insert qCompany;
        RecordType rTypeEndUser = [Select id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        RecordType rTypePartner = [Select id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        RecordType zuoratype = [Select id, DeveloperName From RecordType where DeveloperName = 'Sales_QCCS_Zuora_Subscription'];
            
        Account accenduser = QuoteTestHelper.createAccount(qCompany, rTypeEndUser, 'ForTestEnduser');
       	insert accenduser;
        system.assert(accenduser.Id != null);
        
         Account accpartner = QuoteTestHelper.createAccount(qCompany, rTypePartner, 'ForTestpartner');
        accpartner.QDIPartnerApproved__c = 'Analytics';
        accpartner.Name = 'ForTestpartner';
        insert accpartner;
        system.assert(accpartner.Id != null);
        
      	contact ConPartner = QuoteTestHelper.createContact(accpartner.ID);
        insert ConPartner;
        
        Opportunity oppty = QuoteTestHelper.createOpportunity(accenduser, accpartner.id, zuoratype);
        oppty.Revenue_Type__c = 'Reseller';
        oppty.Partner_Contact__c = ConPartner.id;
        insert oppty;
        system.debug('@@@revtype'+oppty.Revenue_Type__c);
        system.debug('@@@account value'+accpartner.QDIPartnerApproved__c);
        system.debug('@@@recordtype'+oppty.recordTypeId);
        system.debug('@@@value came'+oppty.QDIPartnerCheck__c);
        //system.assert(oppty.QDIPartnerCheck__c == 'Analytics');
        
        Test.startTest();
        Semaphores.CustomAccountTriggerAfterUpdate = false;
        accpartner.QDIPartnerApproved__c = 'Both';
        update accpartner;
        Test.stopTest();
              
        
    }
}