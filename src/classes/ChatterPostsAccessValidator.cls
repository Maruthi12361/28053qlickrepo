/***
* Change log:
*
* extcqb   2019-12-17 IT-2321 - new validation for chatter posts
* extbad   2019-12-17 IT-2329 - do not allow Chatter posts to Closed Attunity Cases for External users
* extbad   2020-01-31 IT-2456 Disable changes to Closed Cases for attunity profiles only
* extbad   2020-02-11 IT-2479   Disable posts to 'Chat Resolved' Status also
***/
public with sharing class ChatterPostsAccessValidator {

    public static final String ATTUNITY_READ_ONLY_PROFILE_NAME = 'Customer Portal Case Viewing Access - Attunity';
    public static final String ATTUNITY_PROFILE_NAME = 'Customer Portal Case Logging Access - Attunity';
    public static final String ATTUNITY_CASE_ERROR_MESSAGE = 'Edits are not accepted on closed Cases';
    public static final String ERROR_MESSAGE = 'Your User profile is not authorized to comment on Cases!';
    public static final List<String> CASE_CLOSED_STATUSES = new List<String>{'Closed', 'Chat Resolved'};

    public static Boolean isUserHaveAccess() {
        List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = :ATTUNITY_READ_ONLY_PROFILE_NAME];
        return profiles.isEmpty() ? true : UserInfo.getProfileId() != profiles.get(0).Id;
    }

    public static void setErrors(List<FeedItem> feedItems) {
        for (FeedItem feedItem : feedItems) {
            feedItem.addError(ERROR_MESSAGE);
        }
    }

    public static void setErrors(List<FeedComment> feedComments) {
        for (FeedComment feedComment : feedComments) {
            feedComment.addError(ERROR_MESSAGE);
        }
    }

    public static Map<Id, FeedComment> excludeAttunityClosedCases(Map<Id, FeedComment> feedComments) {
        if (isAttunityUser()) {
            List<Id> caseIds = new List<Id>();
            for (FeedComment feedComment : feedComments.values()) {
                caseIds.add(feedComment.ParentId);
            }
            Map<Id, Case> caseDetailMap = getAttunityClosedCases(caseIds);

            Map<Id, FeedComment> allowedComments = new Map<Id, FeedComment>();
            for (FeedComment feedComment : feedComments.values()) {
                if (caseDetailMap.get(feedComment.ParentId) != null) {
                    feedComment.addError(ATTUNITY_CASE_ERROR_MESSAGE);
                } else {
                    allowedComments.put(feedComment.Id, feedComment);
                }
            }
            return allowedComments;
        } else {
            return feedComments;
        }
    }

    public static Map<Id, FeedItem> excludeAttunityClosedCases(Map<Id, FeedItem> feedItems) {
        if (isAttunityUser()) {
            List<Id> caseIds = new List<Id>();
            for (FeedItem feedItem : feedItems.values()) {
                caseIds.add(feedItem.ParentId);
            }
            Map<Id, Case> caseDetailMap = getAttunityClosedCases(caseIds);

            Map<Id, FeedItem> allowedItems = new Map<Id, FeedItem>();
            for (FeedItem feedItem : feedItems.values()) {
                if (caseDetailMap.get(feedItem.ParentId) != null) {
                    feedItem.addError(ATTUNITY_CASE_ERROR_MESSAGE);
                } else {
                    allowedItems.put(feedItem.Id, feedItem);
                }
            }
            return allowedItems;
        } else {
            return feedItems;
        }
    }

    private static Boolean isAttunityUser() {
        User currentUser = [SELECT Id, Name, ProfileId, Profile.Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        String profileName = currentUser != null && currentUser.ProfileId != null && currentUser.Profile.Name != null ? currentUser.Profile.Name : '';
        return ATTUNITY_READ_ONLY_PROFILE_NAME.equals(profileName) || ATTUNITY_PROFILE_NAME.equals(profileName);
    }

    private static Map<Id, Case> getAttunityClosedCases(List<String> caseIds) {
        Map<Id, Case> caseDetailMap = new Map<Id, Case>();
        if (caseIds != null && !caseIds.isEmpty()) {
            caseDetailMap = new Map<Id, Case>([
                    SELECT Id, Status, OwnerId, CaseNumber
                    FROM Case
                    WHERE Id IN :caseIds AND Status IN :CASE_CLOSED_STATUSES
            ]);
        }
        return caseDetailMap;
    }
}