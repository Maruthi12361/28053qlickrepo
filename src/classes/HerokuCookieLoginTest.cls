//
// Test class for temporary user data access from heroku to create Cookie, before we move to Token
// for backward compatibility during ULC upgrade/transition
//
// SAN 26/04/2015
//

@isTest
private class HerokuCookieLoginTest {
	
  @testSetup static void setup() {
        Account a = QTTestUtils.createMockAccount('Test Account', new User(Id = UserInfo.getUserId()));
        Contact c = QTTestUtils.createMockContact(a.Id);
        ULC_Details__c d = QTTestUtils.createMockULCDetail(c.Id, 'Active');

        QTCustomSettings__c Settings = new QTCustomSettings__c();
        Settings.Name = 'Default';
        Settings.ULC_Base_URL__c = 'MyBaseURL';
        Settings.ULC_Crypto_Secret__c = 'Secret';
        Settings.ULC_Crypto_Key__c = 'KALLEKALLEKALLEKALLEKALLEKALLE12';
        insert Settings;
        //Fix for Mew entitlement trigger logic
        System.runAs(new User(Id = Userinfo.getUserId())) 
        {
        QuoteTestHelper.createCustomSettings();
        }
    }

    @isTest static void test_doGet() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        String usernameToTest = 'ulcusername';

        req.requestURI = System.URL.getSalesforceBaseURL() + '/services/apexrest/HerokuLoginREST/' + usernameToTest;  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        HerokuCookieLogin.RESTHerokuCookieData results = HerokuCookieLogin.doGet();

        System.assertEquals(usernameToTest, results.USERNAME);

    }
    
    
    @isTest static void test_LoadAdditionalLevels() {
                ULC_Details__c ulcd = [SELECT Id,
        								ULCName__c, 
        								ContactId__r.FirstName,
        								ContactId__r.LastName,
        								ContactId__r.Email,
        								ContactId__c,
        								ContactId__r.AccountId,
        								ContactId__r.Id,
        								ContactId__r.Account_Type__c,
        								ContactId__r.Account.Adopted_Navision_Status__c,
        								ContactId__r.Account.Partner_Type__c,        								
        								ContactId__r.Account.Parent.ParentId,
        								ContactId__r.MailingCity, 
        								ContactId__r.MailingCountry, 
        								ContactId__r.MailingPostalCode, 
        								ContactId__r.MailingState, 
        								ContactId__r.MailingStreet, 
        								ContactId__r.Country_Code__r.Name,
                                 		ContactId__r.Phone, 
        								LeadId__c,
        								LeadId__r.FirstName,
        								LeadId__r.LastName,
        								LeadId__r.Email, 
        								LeadId__r.City, 
        								LeadId__r.Street, 
        								LeadId__r.Country, 
        								LeadId__r.PostalCode, 
        								LeadId__r.State,
        								LeadId__r.Country_Code__r.Name,
                                 		LeadId__r.Phone FROM ULC_Details__c];
        HerokuCookieLogin.RESTHerokuCookieData c = new HerokuCookieLogin.RESTHerokuCookieData(ulcd);
        Id ULCId = [SELECT Id FROM ULC_Details__c].Id;
        Contact con = [SELECT Id FROM Contact];
        system.assertEquals('', c.LoadAdditionalLevels(con.Id));
        Assigned_ULC_Level__c alevel = QTTestUtils.createMockULCAssignedLevel(con.Id, 'TestLevel1');
        alevel = QTTestUtils.createMockULCAssignedLevel(con.Id, 'TestLevel2');
        string levels = c.LoadAdditionalLevels(con.Id);
        system.assertEquals(true, levels.contains('TestLevel1'));
        system.assertEquals(true, levels.contains('TestLevel2'));
    }
    
    @isTest static void test_HasActiveLicense() {
                ULC_Details__c ulcd = [SELECT Id,
        								ULCName__c, 
        								ContactId__r.FirstName,
        								ContactId__r.LastName,
        								ContactId__r.Email,
        								ContactId__c,
        								ContactId__r.AccountId,
        								ContactId__r.Id,
        								ContactId__r.Account_Type__c,
        								ContactId__r.Account.Adopted_Navision_Status__c,
        								ContactId__r.Account.Partner_Type__c,        								
        								ContactId__r.Account.Parent.ParentId,
        								ContactId__r.MailingCity, 
        								ContactId__r.MailingCountry, 
        								ContactId__r.MailingPostalCode, 
        								ContactId__r.MailingState, 
        								ContactId__r.MailingStreet, 
        								ContactId__r.Country_Code__r.Name,
                                 		ContactId__r.Phone, 
        								LeadId__c,
        								LeadId__r.FirstName,
        								LeadId__r.LastName,
        								LeadId__r.Email, 
        								LeadId__r.City, 
        								LeadId__r.Street, 
        								LeadId__r.Country, 
        								LeadId__r.PostalCode, 
        								LeadId__r.State,
        								LeadId__r.Country_Code__r.Name,
                                 		LeadId__r.Phone FROM ULC_Details__c];
        HerokuCookieLogin.RESTHerokuCookieData c = new HerokuCookieLogin.RESTHerokuCookieData(ulcd);
        Account a = [SELECT Id FROM Account];
        system.assertEquals(false, c.HasActiveLicense(a.Id));
        QTTestUtils.createMockAccountLicense(a.Id);
        system.assertEquals(true, c.HasActiveLicense(a.Id));
    }

    @isTest static void test_getGeneratedCrypto_1() {
        QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');
        system.assert(Settings != null);

        Account a = [SELECT Id, Navision_Status__c FROM Account];
        a.Navision_Status__c = 'Partner';
        a.Partner_Type__c = 'MasterReseller';
        update a;
        ULC_Details__c ulcd = [SELECT Id,
        								ULCName__c, 
        								ContactId__r.FirstName,
        								ContactId__r.LastName,
        								ContactId__r.Email,
        								ContactId__c,
        								ContactId__r.AccountId,
        								ContactId__r.Id,
        								ContactId__r.Account_Type__c,
        								ContactId__r.Account.Adopted_Navision_Status__c,
        								ContactId__r.Account.Partner_Type__c,        								
        								ContactId__r.Account.Parent.ParentId,
        								ContactId__r.MailingCity, 
        								ContactId__r.MailingCountry, 
        								ContactId__r.MailingPostalCode, 
        								ContactId__r.MailingState, 
        								ContactId__r.MailingStreet, 
        								ContactId__r.Country_Code__r.Name,
                                 		ContactId__r.Phone, 
        								LeadId__c,
        								LeadId__r.FirstName,
        								LeadId__r.LastName,
        								LeadId__r.Email, 
        								LeadId__r.City, 
        								LeadId__r.Street, 
        								LeadId__r.Country, 
        								LeadId__r.PostalCode, 
        								LeadId__r.State,
        								LeadId__r.Country_Code__r.Name,
                                 		LeadId__r.Phone FROM ULC_Details__c];
        HerokuCookieLogin.RESTHerokuCookieData c = new HerokuCookieLogin.RESTHerokuCookieData(ulcd);
        Assigned_ULC_Level__c alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel1');
        alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel2');
        QTTestUtils.createMockAccountLicense(a.Id);
        string encrypted = c.GetULCLevel(ulcd);
        system.assertNotEquals(null, encrypted);
        system.assert(encrypted.length() > 0);
        system.assertEquals(true, encrypted.contains('Base'));
        system.assertEquals(true, encrypted.contains('Partner'));
        system.assertEquals(true, encrypted.contains('MasterReseller'));
        system.assertEquals(true, encrypted.contains('TestLevel1'));
        system.assertEquals(true, encrypted.contains('TestLevel2'));
    }

    @isTest static void test_getGeneratedCrypto_2() {
        QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');
        system.assert(Settings != null);

        Account a1 = [SELECT Id, ParentId, Navision_Status__c FROM Account];
        Account a2 = QTTestUtils.createMockAccount('Test Account 2', new User(Id = UserInfo.getUserId()));
        a2.Navision_Status__c = 'Partner';
        a1.ParentId = a2.Id;
        List<Account> accs = new List<Account>();
        accs.add(a1); accs.add(a2);
        update accs;
        ULC_Details__c ulcd = [SELECT Id,
        								ULCName__c, 
        								ContactId__r.FirstName,
        								ContactId__r.LastName,
        								ContactId__r.Email,
        								ContactId__c,
        								ContactId__r.AccountId,
        								ContactId__r.Id,
        								ContactId__r.Account_Type__c,
        								ContactId__r.Account.Adopted_Navision_Status__c,
        								ContactId__r.Account.Partner_Type__c,        								
        								ContactId__r.Account.Parent.ParentId,
        								ContactId__r.MailingCity, 
        								ContactId__r.MailingCountry, 
        								ContactId__r.MailingPostalCode, 
        								ContactId__r.MailingState, 
        								ContactId__r.MailingStreet, 
        								ContactId__r.Country_Code__r.Name,
                                 		ContactId__r.Phone, 
        								LeadId__c,
        								LeadId__r.FirstName,
        								LeadId__r.LastName,
        								LeadId__r.Email, 
        								LeadId__r.City, 
        								LeadId__r.Street, 
        								LeadId__r.Country, 
        								LeadId__r.PostalCode, 
        								LeadId__r.State,
        								LeadId__r.Country_Code__r.Name,
                                 		LeadId__r.Phone FROM ULC_Details__c];
        HerokuCookieLogin.RESTHerokuCookieData c = new HerokuCookieLogin.RESTHerokuCookieData(ulcd);

        Assigned_ULC_Level__c alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel1');
        alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel2');
        QTTestUtils.createMockAccountLicense(a1.Id);
        string encrypted = c.GetULCLevel(ulcd);
        system.assertNotEquals(null, encrypted);
        system.assert(encrypted.length() > 0);
        system.assertEquals(true, encrypted.contains('Base'));
        system.assertEquals(true, encrypted.contains('Partner'));
        system.assertEquals(true, encrypted.contains('TestLevel1'));
        system.assertEquals(true, encrypted.contains('TestLevel2'));
    }


    @isTest static void test_GetParentNavisionStatus() {
                ULC_Details__c ulcd = [SELECT Id,
        								ULCName__c, 
        								ContactId__r.FirstName,
        								ContactId__r.LastName,
        								ContactId__r.Email,
        								ContactId__c,
        								ContactId__r.AccountId,
        								ContactId__r.Id,
        								ContactId__r.Account_Type__c,
        								ContactId__r.Account.Adopted_Navision_Status__c,
        								ContactId__r.Account.Partner_Type__c,        								
        								ContactId__r.Account.Parent.ParentId,
        								ContactId__r.MailingCity, 
        								ContactId__r.MailingCountry, 
        								ContactId__r.MailingPostalCode, 
        								ContactId__r.MailingState, 
        								ContactId__r.MailingStreet, 
        								ContactId__r.Country_Code__r.Name,
                                 		ContactId__r.Phone, 
        								LeadId__c,
        								LeadId__r.FirstName,
        								LeadId__r.LastName,
        								LeadId__r.Email, 
        								LeadId__r.City, 
        								LeadId__r.Street, 
        								LeadId__r.Country, 
        								LeadId__r.PostalCode, 
        								LeadId__r.State,
        								LeadId__r.Country_Code__r.Name,
                                 		LeadId__r.Phone
                                         FROM ULC_Details__c];
        HerokuCookieLogin.RESTHerokuCookieData c = new HerokuCookieLogin.RESTHerokuCookieData(ulcd);
        
        Account a1 = [SELECT Id, ParentId FROM Account];
        system.assertEquals(null, c.GetParentNavisionStatus(a1.Id, 0));

        Account a2 = QTTestUtils.createMockAccount('Test Account 2', new User(Id = UserInfo.getUserId()));
        Account a3 = QTTestUtils.createMockAccount('Test Account 3', new User(Id = UserInfo.getUserId()));      
        a1.ParentId = a2.Id;
        a2.Navision_Status__c = 'Partner';
        a3.Navision_Status__c = 'Customer';
        List<Account> accs = new List<Account>();
        accs.add(a1); accs.add(a2); accs.add(a3);
        update accs;
        
        system.assertEquals(null, c.GetParentNavisionStatus(a1.Id, 3));
        system.assertEquals('Partner', c.GetParentNavisionStatus(a1.Id, 0));
        system.assertEquals(null, c.GetParentNavisionStatus('001c000000k8WpW', 0));  // This ID should not be found - this is a negative test

        a1.ParentId = a3.Id;
        update a1;
        system.assertEquals('Customer', c.GetParentNavisionStatus(a1.Id, 0));       
    }

}