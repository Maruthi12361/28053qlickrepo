/**     
 * File Name : Z_RenewalOpportunityBatchable
 * Description : This is a batchable job to create renewal opportunities, triggered by Z_RenewalOpportunityJob schedulable class.  
 *               This job calls the following methods in this order:
 *               1. start - returns QueryLocator to pass a list of previous quotes
 *               2. execute 
 *               2a. queryAndStorePreviousOpportunities - using previous quotes, queries for the list of previous opportunities
 *               2b. createRenewalOpportunities - creates renewal quotes using the previous opportunities from step 2a
 *               2c. reassociateOpportunitiesAndCreateOpportunityContactRoles - links the previous opportunity and renewal opportunities
 *                   together, and creates the opportunity contact roles under the renewal opportunities
 *               3. finish - terminates the job, and sends email notification in case of system exception
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

global class Z_RenewalOpportunityBatchable implements Database.Batchable<SObject> {

    private Map<String, Map<String, Object>> subscriptionFieldsMap;

    private Map<Id, Apex_Debug_Log__c> apexDebugLogsMap;

    private Set<String> excludePreviousOpportunityIds;

    private Renewal_Automation_Settings__c settings;

    private final String CLASS_NAME = 'Z_RenewalOpportunityBatchable',
                         DEBUG_PREFIX = '> $ ' + this.CLASS_NAME;
                 
    private final Id RENEWAL_OPPTY_RECORD_TYPE_ID = Schema.SObjectType.Opportunity
                                                    .getRecordTypeInfosByName()
                                                    .get('Subscription Renewal')
                                                    .getRecordTypeId();
                 
    public transient List<Database.SaveResult> insertRenewalOpptyResults,
                                               updateBaseOpptyResults,
                                               insertOpptyCRResults,
                                               insertApexDebugLogResults;

    global Z_RenewalOpportunityBatchable(Map<String, Map<String, Object>> subscriptionFieldsMap, Set<String> excludePreviousOpportunityIds) {
        this.subscriptionFieldsMap = subscriptionFieldsMap;
        this.excludePreviousOpportunityIds = excludePreviousOpportunityIds;

        this.apexDebugLogsMap = new Map<Id, Apex_Debug_Log__c>();
        
        if (!Test.isRunningTest()) { Semaphores.SetAllSemaphoresToFalse(); }

        UpdateClosedOppQuotesHandler.OpportunityQuoteUpdate = false;
        OpportunitySetDefaultValueHandler.IsBeforeUpdateProcessing = false;
        
        this.settings = Renewal_Automation_Settings__c.getOrgDefaults();

        this.insertRenewalOpptyResults = new List<Database.SaveResult>();
        this.updateBaseOpptyResults = new List<Database.SaveResult>();
        this.insertOpptyCRResults = new List<Database.SaveResult>();
        this.insertApexDebugLogResults = new List<Database.SaveResult>();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        Set<String> subscriptionNumbers = this.subscriptionFieldsMap.keySet();
        Integer recordLimit = this.settings.OpportunityRecordsLimit__c  == null ? 0 : Integer.valueOf(this.settings.OpportunityRecordsLimit__c );
        
        String qString = 'SELECT zqu__Opportunity__c '
                       + 'FROM zqu__Quote__c  '
                       + 'WHERE zqu__Opportunity__c NOT IN :excludePreviousOpportunityIds '
                       + 'AND zqu__SubscriptionType__c IN (\'New Subscription\', \'Renew Subscription\') '
                       + 'AND Quote_Status__c IN (\'Accepted by Customer\', \'Order Placed\') '
                       + 'AND zqu__Subscription_Name__c IN :subscriptionNumbers '
                       + 'AND zqu__Status__c = \'Sent to Z-Billing\' '
                       + 'AND zqu__Opportunity__c != NULL '
                       + 'AND zqu__Opportunity__r.Next_Renewal_Opportunity__c = NULL '
                       + 'AND zqu__Opportunity__r.RecordType.Name IN (\'Sales QCCS Zuora Subscription\', \'Subscription Renewal\') '
                       + 'AND zqu__Opportunity__r.StageName != \'Closed Lost\' '
                       + 'ORDER BY zqu__SubscriptionTermEndDate__c ASC ';

        if (recordLimit > 0) {
            qString += 'LIMIT ' + recordLimit;
        }
        
        return Database.getQueryLocator(qString);
    }

    global void execute(Database.BatchableContext bc, List<zqu__Quote__c> previousQuotes) {
        try {
            Map<Id, Opportunity> previousOpportunities = queryAndStorePreviousOpportunities(previousQuotes);
            if (previousOpportunities.isEmpty()) {
                System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' there are no eligible base opportunities to clone.  Exiting');
                return;
            }
    
            List<Opportunity> renewalOpportunities = createRenewalOpportunities(previousOpportunities);
            reassociateOpportunitiesAndCreateOpportunityContactRoles(previousOpportunities, renewalOpportunities);
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' exception thrown while running the job');
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + e.getStackTraceString());

            String orgId = UserInfo.getOrganizationId().substring(0,15),
                   errorEmailDL = this.settings.ErrorEmailDL__c,
                   htmlMessage = '<p>Renewal Opportunity Job encountered a system error.  Error message was: <b>' + e.getStackTraceString() + '</b></p>';
                
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' ' + htmlMessage);

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] { errorEmailDL });
            mail.setSenderDisplayName(this.CLASS_NAME);
            mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Opportunity Job Failure Notification');
            mail.setHtmlBody(htmlMessage);
            
            if (!Test.isRunningTest()) { Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); }
        }
    }

    global void finish(Database.BatchableContext bc) {
        if (!Test.isRunningTest()) { 
            AsyncApexJob job = [SELECT Id, ExtendedStatus, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()];
            String extendedStatus = job.ExtendedStatus;
        
            if (null != extendedStatus) {
                String orgId = UserInfo.getOrganizationId().substring(0,15),
                       errorEmailDL = this.settings.ErrorEmailDL__c,
                       htmlMessage = '<p>Renewal Opportunity Job encountered a system error.  Error message was: <b>' + extendedStatus + '</b></p>';
                
                System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' ' + htmlMessage);

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] { errorEmailDL });
                mail.setSenderDisplayName(this.CLASS_NAME);
                mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Opportunity Job Failure Notification');
                mail.setHtmlBody(htmlMessage);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }    
        }
    }
    
    public Map<Id, Opportunity> queryAndStorePreviousOpportunities(List<zqu__Quote__c> previousQuotes) {
        Map<Id, Opportunity> previousOpportunities = new Map<Id, Opportunity>();
        List<Id> baseOpportunityIds = new List<Id>();
        Set<String> subscriptionNumbersSet = this.subscriptionFieldsMap.keySet();

        for (zqu__Quote__c q : previousQuotes) { 
            baseOpportunityIds.add(q.zqu__Opportunity__c);
        }

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' number of base oppties = ' + baseOpportunityIds.size());

        DescribeSObjectResult opptyDescResult = Opportunity.getSObjectType().getDescribe();
        List<String> opptyFieldApiNames = Z_RenewalAutomationUtility.getFieldApiNames(opptyDescResult, 'Renewal_Opp_Query_Fields');
        String baseOpptyQueryString = 'SELECT ' + String.join(opptyFieldApiNames, ',')
                                    + ', (SELECT ContactId, OpportunityId, Role FROM OpportunityContactRoles) '
                                    + ', (SELECT Id, zqu__Subscription_Name__c, Attunity_Quote__c, QDI_Product_Count__c '
                                    + ', zqu__Account__r.Attunity_Rep__c FROM zqu__Quotes__r WHERE zqu__Primary__c = TRUE '
                                    + '  AND Quote_Status__c IN (\'Order Placed\', \'Accepted by Customer\') '
                                    + '  AND zqu__Status__c = \'Sent to Z-Billing\' '
                                    + '  AND zqu__Subscription_Name__c IN :subscriptionNumbersSet) '
                                    + 'FROM Opportunity '
                                    + 'WHERE Id IN :baseOpportunityIds';

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' base oppty query string = ' + baseOpptyQueryString);

        for (Opportunity o : Database.query(baseOpptyQueryString)) {
            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' checking opportunity ' + o.Id + ' for validity');

            if (o.zqu__Quotes__r.size() == 1) {
                System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' adding opportunity ' + o.Id + ' for processing');
                previousOpportunities.put(o.Id, o);
            } else {
                System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' opportunity ' + o.Id + ' has no quotes sent.  Skipping...');
            }
        }

        return previousOpportunities;
    }

    public List<Opportunity> createRenewalOpportunities(Map<Id, Opportunity> previousOpportunities) {
        List<Opportunity> renewalOpportunities = new List<Opportunity>();

        for (Opportunity o : previousOpportunities.values()) {
            zqu__Quote__c previousQuote = o.zqu__Quotes__r.get(0);	
            String subscriptionNumber = previousQuote.zqu__Subscription_Name__c;	
            Map<String, Object> subscriptionFields = this.subscriptionFieldsMap.get(subscriptionNumber);	
            	
            if (null != subscriptionFields) {	
                Opportunity clonedOppty = o.clone(false, true, false, false);	
                clonedOppty.OwnerId = (previousQuote.Attunity_Quote__c || previousQuote.QDI_Product_Count__c > 0) 
                                    ? previousQuote.zqu__Account__r.Attunity_Rep__c
                                    : this.settings.AttunityRepresentativeUser__c;
                
                if (null != subscriptionFields.get('Zuora__SubscriptionEndDate__c')) {	
                    clonedOppty.CloseDate = (Date) subscriptionFields.get('Zuora__SubscriptionEndDate__c');    	
                }	
            	
                clonedOppty.ForecastCategoryName = 'Omitted';	
                clonedOppty.Name = 'Renewal Opportunity ' + subscriptionNumber;	
                clonedOppty.RecordTypeId = this.RENEWAL_OPPTY_RECORD_TYPE_ID;	
                clonedOppty.StageName = 'Renewal Anticipated';	
                clonedOppty.Opportunity_Source__c = 'Qlik Created';	
                clonedOppty.Previous_Opportunity__c = o.Id; 	
                clonedOppty.Primary_Zuora_Quote_OF_Status__c = 'None';	
                	
                if (null != subscriptionFields.get('Zuora__NextRenewalDate__c')) {	
                    clonedOppty.Renewal_Date__c = (Date) subscriptionFields.get('Zuora__NextRenewalDate__c');	
                }             	
              	
                clonedOppty.Subscription_Forecast_ACV_Amount__c = (Decimal) subscriptionFields.get('ACV__c');	
                clonedOppty.Short_Description__c = 'Renewal Opportunity ' + subscriptionNumber;	
                clonedOppty.Zuora_Subscription_Number__c = subscriptionNumber;	
            	
                renewalOpportunities.add(clonedOppty); 
            }
        }

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' whats about to be inserted: ' + renewalOpportunities);
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' how many in this list: ' + renewalOpportunities.size());

        try {
            this.insertRenewalOpptyResults = Database.insert(renewalOpportunities, false);
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' exception thrown while inserting renewal opps');
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + e.getStackTraceString());
        }

        return renewalOpportunities;
    }

    public void reassociateOpportunitiesAndCreateOpportunityContactRoles(Map<Id, Opportunity> previousOpportunities, List<Opportunity> renewalOpportunities) {
        List<OpportunityContactRole> opportunityContactRoles = new List<OpportunityContactRole>();
        List<Opportunity> updatePreviousOppties = new List<Opportunity>();
        Integer badOpportunityCounter = 0;

        String htmlMessage = '<table border="1"><tr><td>Opportunity ID</td><td>Quote ID</td><td>Affected Fields</td><td>Status Code</td><td>Error Message</td></tr>';

        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' how many renewal opportunities attempted to insert? ' + renewalOpportunities.size());

        for (Integer i = 0; i < renewalOpportunities.size(); i++) {
            Database.SaveResult result = this.insertRenewalOpptyResults[i];
            Opportunity renewalOppty = renewalOpportunities.get(i),
                        baseOppty = previousOpportunities.get(renewalOppty.Previous_Opportunity__c);
            zqu__Quote__c previousQuote = baseOppty.zqu__Quotes__r.get(0);
            if (result.isSuccess()) {
                baseOppty.Next_Renewal_Opportunity__c = result.getId();
                
                for (OpportunityContactRole ocr : baseOppty.OpportunityContactRoles) {
                    opportunityContactRoles.add(new OpportunityContactRole(ContactId=ocr.ContactId,
                                                                           OpportunityId=renewalOppty.Id,
                                                                           Role=ocr.Role));
                }

                this.apexDebugLogsMap.put(result.getId(), new Apex_Debug_Log__c(Apex_Class__c='Z_RenewalOpportunityJob', 
                                                                                Message__c='Processing',
                                                                                Method__c='createRenewalOpportunities',
                                                                                Record_Id__c=result.getId(),
                                                                                Stack_Trace__c=null,
                                                                                Type__c='Information'));

                System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' setting next renewal opportunity ' + result.getId() + ' on a base opportunity ' + baseOppty.Id);

                updatePreviousOppties.add(baseOppty);
            } else {
                badOpportunityCounter++;
                String badOpportunityId = baseOppty.Id,
                       affectedFields, statusCode, errorMessage;

                for (Database.Error error : result.getErrors()) {
                    affectedFields = String.join(error.getFields(), ',');
                    errorMessage = error.getMessage();
                    statusCode = error.getStatusCode().name();
                }

                htmlMessage += '<tr><td>' + badOpportunityId + '</td><td>' + previousQuote.Id + '</td><td>' + affectedFields + '</td><td>' + statusCode + '</td><td>' + errorMessage + '</td></tr>';
            }
        }

        try {
            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' Before insert' + this.apexDebugLogsMap.values());
            this.insertApexDebugLogResults = Database.insert(this.apexDebugLogsMap.values(), false);

            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' Apex log results' + this.insertApexDebugLogResults);
            this.insertOpptyCRResults = Database.insert(opportunityContactRoles, false);
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' exception thrown while inserting opp contact roles');
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + e.getStackTraceString());
        }

        try {
            Semaphores.SetAllSemaphoresToTrue();
            UpdateClosedOppQuotesHandler.OpportunityQuoteUpdate = true;
            OpportunitySetDefaultValueHandler.IsBeforeUpdateProcessing = true;

            this.updateBaseOpptyResults = Database.update(updatePreviousOppties, false);

            for (Integer i = 0; i < updatePreviousOppties.size(); i++) {
                Database.SaveResult result = this.updateBaseOpptyResults[i];
                
                System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' update result for opportunity ' + result.getId() + ': success? ' + result.isSuccess());
                
                if (!result.isSuccess()) {
                    Opportunity previousOppty = updatePreviousOppties.get(i);
                    String nextRenewalOpptyId = previousOppty.Next_Renewal_Opportunity__c;
                    
                    this.apexDebugLogsMap.remove(nextRenewalOpptyId);
                }
            }

            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' before delete' + this.apexDebugLogsMap);

            if (this.apexDebugLogsMap.size() > 0) {
                delete this.apexDebugLogsMap.values();
            }
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' exception thrown while attempting to update previous opportunities');
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + e.getStackTraceString());
        }

        if (Test.isRunningTest()) {
            badOpportunityCounter++;
        }

        if (badOpportunityCounter > 0) {
            List<Id> distroListUsers = new List<Id>();
            List<String> distroList = new List<String>();

            String orgId = UserInfo.getOrganizationId().substring(0,15);

            for (GroupMember groupMember : [SELECT UserOrGroupId FROM GroupMember WHERE Group.Name = 'Renewal Automation Group']) {
                distroListUsers.add(groupMember.UserOrGroupId);
            }

            for (User user : [SELECT Email FROM User WHERE Id IN :distroListUsers]) {
                distroList.add(user.Email);
            }

            htmlMessage += '</table>';
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(distroListUsers);
            mail.setSenderDisplayName(this.CLASS_NAME);
            mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Opportunity Job Failure Notification');
            mail.setHtmlBody(htmlMessage);
            if (!Test.isRunningTest()) { Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); }
        }
    }
}