/******************************************************

    UserListController        
        
    Changelog:
        2014-09-16  MTM     CR# 16915 Initial development   
        2016-12-14  Pramod Kumar V Increase code coverage                                                    
******************************************************/
@isTest
public class UserListControllerTest {
    static testMethod void TestUserListControllerTest() 
    {
        User usr1 = QTTestUtils.createMockOperationsAdministrator();
        User usr2 = QTTestUtils.createMockOperationsAdministrator();
        usr2.Prime_User__c = usr1.Id;
        update usr2;
        test.startTest();
        ApexPages.StandardController StandardController = new ApexPages.StandardController(usr1);
        UserListController controller = new UserListController(StandardController);
        List<User> userList = controller.getPrimeToUsers();
        List<User> userListadd =  new List<User>();
        userListadd.add (usr1);
        System.assertEquals(userList[0].Id, usr2.Id);
        test.stopTest();
    }
}