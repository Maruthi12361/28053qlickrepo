/***************************************************************************************************************************************

  Log Changes:

  2011-11-08:   RDZ     Changes related to CR# 3342: Trigger CPQ update if any of the mapped fields are changed
                        https://eu1.salesforce.com/a0CD000000FyLHs
                        Adding static method to check if specific fields have been changed and then the call to
                        update the cpq user.
  2011-12-07:   RDZ     Changes related to CR#1516 Send from email APEX change https://eu1.salesforce.com/a0CD000000CAy0Z
                        The changes involve when sending an email for ulc welcome email template, use noreply@qlikview.com as from email
  2011-12-12:   MHG     CR# 1758: Batchify CallUpdateAccountLevel
                        https://eu1.salesforce.com/a0CD000000EU5Ln
                        CR# 3713: Batchify PortalControl
                        https://eu1.salesforce.com/a0CD000000Fz8Mg
  2012-02-14:   CCE     Added QlikTech_Company__c and Billing to Account creation due to manditory QlikTech Company field on Account
  2012-04-11:   RDZ     Add Segment when creating Lead when is Qualified B  
  2012-06-15:   RDZ     CR# 4512 Changes to PE download process, a differen email template will be sent to Leads and Contacts if
                        the user ulc details have been created when registering to download QV Personal Edition.
  2013-02-01:   CCE     Modified test to work with changes made to OpportunityMarketType.trigger re CR# 7268    
  2013-04-17    YKA     CR# 7818: Code to merge ULC password to email being sent when reset password initiated by Qoncierge users
  2013-05-20    SAN     CR# 8161: make sure send detail populate empty password for newly created user
  2013-05-23    KMH     CR# 7812: Add "ext_" to the allowed exeptions for ULC Username
  2013-07-04    CCE     CR# 6546 Find Way to Eliminate Qualified - B from Lead & Contact Status picklist values https://eu1.salesforce.com/a0CD000000NL228
  2014-06-13    AIN     CR# 7283 https://eu1.salesforce.com/a0CD000000U9yv8
  2014-07-23    TJG     LMS project adding UpsertCsodUser callout                                     
  2014-09-08    AIN     Commented out code from above line that was not going to prod.
  2015-03-09    CCE     CR# 14398 Changed wording of reset email 
  2015-03-24    MHG_EXT ULCv4 Project - chaning how account activation works.                                        
  2015-06-05    TJG     CR# 37946 https://eu1.salesforce.com/a0CD000000t7WnX Updating this class for ViewCentral
  2015-09-17    SLH     Added OEM End User account record type for validation rule OP040
  2015-10-20    AIN     Moving test methods to a test class, updating API version to 31
  2015-11-16    TJG     CR# 65393 https://eu1.salesforce.com/a0CD000000wjQle - fix newly found broken button
  2015-12-01    SAN     eCommerce project: switch off ulc.cls sending email template, used sendgrid      
  2016-02-01    SAN     ULC-339 update lead/campaign from cloud  
  2016-11-06    BAD     CR# 97533 - Relayware
  2017-07-07    SMM     CR: CHG0030596
  2017-12-28    NAD     IT-138
  ****************************************************************************************************************************************/
// Adding 2013/04/17

public without sharing class ULC{

    static Map<string, Id> TemplateNameToId = new Map<string, ID>();
    
    public ULC()
    {       
        if (TemplateNameToId.size() == 0)
        {
            for (EmailTemplate et : [select Id, Name from EmailTemplate where IsActive = true and Folder.Name = 'ULC Templates'])
            {
                TemplateNameToId.put(et.Name, et.Id);
                
            }
        }       
    }

    @future (callout=true)
    public static void CallPortalControl(List<string> Usernames, List<string> Passwords)
    {
        ulcv3QlikviewCom.ServiceSoap service = new ulcv3QlikviewCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        ulcv3QlikviewCom.ArrayOfString UNames = new ulcv3QlikviewCom.ArrayOfString();
        ulcv3QlikviewCom.ArrayOfString PWords = new ulcv3QlikviewCom.ArrayOfString();
        UNames.string_x = Usernames;
        PWords.string_x = Passwords;
        
        try
        {
            service.PortalControlBatch(UNames, PWords);
        } catch(System.Exception Ex)
        {
            System.debug('CallPortalControl - Caught exception: ' +Ex.getMessage());            
        }
    } 

    @future (callout=true)
    public static void CallUpdateAccountLevel(List<string> AccountIDs)
    {
        ulcv3QlikviewCom.ServiceSoap service = new ulcv3QlikviewCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        ulcv3QlikviewCom.ArrayOfString Accounts = new ulcv3QlikviewCom.ArrayOfString();
        Accounts.string_x = AccountIDs;
        
        try 
        {
            service.UpdateAccountLevelBatch(Accounts);          
        }       
        catch (System.Exception Ex)
        {
            System.debug('CallUpdateAccountLevel -  Caught exception: ' + Ex.getMessage());
        }
    } 
    
    @future (callout=true)
    public static void CallUpsertViewCentralUser(List<string> Usernames)
    {
        System.debug('Entering CallUpsertViewCentralUser for ' + Usernames);
        ulcv3QlikviewCom.ServiceSoap service = new ulcv3QlikviewCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        for (integer i = 0; i < Usernames.size(); i++)
        {
            System.debug('About to call UpsertLMSUser for ' + Usernames[i]);
            string ReturnText = 'Ret';
            service.UpsertLMSUser(Usernames[i], ReturnText);
        }
    } 
    //added on 2014-07-23 by tjg
    //Commented out on 2014-09-08 for release
    /*
    @future (callout=true)
    public static void CallUpsertCsodUser(List<string> Usernames)
    {
        ulcv3QlikviewCom.ServiceSoap service = new ulcv3QlikviewCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        for (integer i = 0; i < Usernames.size(); i++)
        {
            string ReturnText = 'Ret';
            service.UpsertCsodUser(Usernames[i], ReturnText);
        }
    }*/
    
    @future (callout=true)
    public static void CallUpsertCPQUser(List<string> UserIDs)
    {
        string ReturnText = '';
        ulcv3QlikviewCom.ServiceSoap service = new ulcv3QlikviewCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        for (integer i = 0; i < UserIDs.size(); i++ )
        {
            service.UpsertCPQUser(UserIDs[i], ReturnText);
        }
    }   

    @future (callout=true)
    public static void SelfServiceChanged(List<string> AccountIDs)
    {
        string ReturnText = '';
        ulcv3QlikviewCom.ServiceSoap service = new ulcv3QlikviewCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        ulcv3QlikviewCom.ArrayOfString Accounts = new ulcv3QlikviewCom.ArrayOfString();
        Accounts.string_x = AccountIDs;

        service.SelfServeChanged(Accounts);
    }

    public void HandleStatusChanges(List<ULC_Details__c> ULCDetails, boolean isBefore, Map<Id, ULC_Details__c> ULCDetailsOld)
    {
        
        List<Messaging.Email> Mails = new List<Messaging.Email>();
        List<ULC_Details__c> ToUpdate = new List<ULC_Details__c>();
        List<string> LMSUsernames = new List<string>();
        //List<string> LMSPasswords = new List<string>();
        
        System.debug('Entering HandleStatusChanges - isBefore=' + isBefore + ',' + ULCDetails);        
        Set<Id> ULCDetailsID = new Set<Id>();
                
        for (ULC_Details__c ULC_Detail : ULCDetails) 
        {
            string TemplateID = '';
            string Who = '';
            List<string> Recipient = new List<string>();
            ULC_Details__c DetailsClone = null;
    
            /*********************************************************
            
                RESET/RESEND PASSWORD
            
            *********************************************************/
            if (ULC_Detail.ULCStatus__c == 'Resend Password') {         
                if (!isBefore) {
                    DetailsClone = ULC_Detail.clone();
                    DetailsClone.Id = ULC_Detail.Id;
                } else {
                    ULC_Detail.ULCStatus__c = 'Active';
                    if (TemplateNameToId.containsKey('ULC Reset Password'))
                    {
                        //TemplateID = TemplateNameToId.get('ULC Reset Password');
                    }
                }
            }   
            
            if (isBefore && ULC_Detail.ULCStatus__c == 'Reset Password') 
            {
                ULC_Detail.ULCStatus__c = 'Resend Password';            
            }                     
            
            /*********************************************************
            
                Upsert ViewCentral User
            
            *********************************************************/
            if (isBefore && ULC_Detail.ULCStatus__c == 'Upsert LMS User') 
            {   
                if(ULCDetailsOld != null) {  
                    System.debug('>>> Old Id: ' + ULCDetailsOld.get(ULC_Detail.Id).Id);
                    System.debug('>>> New/Current Id: ' + ULC_Detail.Id); 

                    System.debug('>>> Old Status: ' + ULCDetailsOld.get(ULC_Detail.Id).ULCStatus__c);
                    System.debug('>>> New Status: ' + ULC_Detail.ULCStatus__c);

                    if(ULC_Detail.Id == ULCDetailsOld.get(ULC_Detail.Id).Id) {
                        ULC_Detail.ULCStatus__c = ULCDetailsOld.get(ULC_Detail.Id).ULCStatus__c;
                        System.debug('>>> Successfully reverted status!');
                    } else {
                        ULC_Detail.ULCStatus__c = 'Active';    
                    }
                } else {
                    ULC_Detail.ULCStatus__c = 'Active';
                }
                                         
                LMSUsernames.Add(ULC_Detail.ULCName__c);
                //LMSPasswords.Add(ULC_Detail.ULC_Password__c); 
            }               
              
            /*********************************************************
            
                SEND DETAILS
            
            *********************************************************/
            if (!isBefore && ULC_Detail.ULCStatus__c == 'Send Details') {
                
                if (DetailsClone == null)
                {
                    DetailsClone = ULC_Detail.clone();
                    DetailsClone.Id = ULC_Detail.Id;
                }
                DetailsClone.ULCStatus__c = 'Awaiting Activation';                   // This might be removed and done via workflow instead
                 
                if (TemplateNameToId.containsKey('ULC Welcome Mail'))
                {
                    //TemplateID = TemplateNameToId.get('ULC Welcome Mail');
                }
            }   
            
            /*********************************************************
            
                CR# 4512> SEND DETAILS WITH Download Link
            
            *********************************************************/
            if (!isBefore && ULC_Detail.ULCStatus__c == 'Send Download Template') {
                
                if (DetailsClone == null)
                {
                    DetailsClone = ULC_Detail.clone();
                    DetailsClone.Id = ULC_Detail.Id;
                }
                DetailsClone.ULCStatus__c = 'Awaiting Activation';                   // This might be removed and done via workflow instead
                 
                if (TemplateNameToId.containsKey('ULC Credential Email From Download Form'))
                {
                    TemplateID = TemplateNameToId.get('ULC Credential Email From Download Form');
                }
            }   

            /*********************************************************
            
                Problem Type - Send to Account Manager
            
            *********************************************************/              
            if (!isBefore && ULC_Detail.ULC_Problem_Type__c == 'Send to Account Manager')
            {
                ULCDetailsID.add(ULC_Detail.Id);
                if (DetailsClone == null)
                {
                    DetailsClone = ULC_Detail.clone();
                    DetailsClone.Id = ULC_Detail.Id;
                }
                DetailsClone.ULC_Problem_Type__c = 'None';                  // This might be removed and done via workflow instead              
            }
    

            // Add Clone to Updates list -- need to do it this way to not put duplicates into the list  
            if (DetailsClone != null)
            {
                ToUpdate.add(DetailsClone);
            }
    
            /*********************************************************
            
                PREPARE FOR EMAIL SENDING
            
            *********************************************************/
            if (ULC_Detail.ContactId__c != null) {
                Who = ULC_Detail.ContactId__c;
            } else if (ULC_Detail.LeadId__c != null) {
                Who = ULC_Detail.LeadId__c;
            }
        
            if (TemplateID != '' && Who != '')
            {
                
                Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
                Message.setTargetObjectId(Who); 
                Message.setSaveAsActivity(false);
                Message.setWhatId(ULC_Detail.Id);
                Message.setTemplateId(TemplateID);
                Message.setUseSignature(false);                    
                /*                                              
                if (TemplateID == TemplateNameToId.get('ULC Reset Password'))
                {
                    Message.setTargetObjectId(Who); 
                    Message.setSubject('QlikView Account - Your password has been reset');
                    //Message.setPlainTextBody('Your QlikView Account password has been reset by request. If you did not initiate this request, please contact Qoncierge@qlik.com immediately. \n \nPlease keep this email for your records. \n\nUser name: '+ ULC_Detail.ULCName__c+ '\nPassword: '+ ULC_Detail.ULC_Password__c + '\n\n(c) 2009-2015 QlikTech International AB.'); //CCE changed wording of email CR 14398                    
                } 
                */     
                              
                
                //Set Org wide email address (ID for noreply@qlikview.com Live:0D2D0000000GndJKAS QTTEST:0D2M0000000009h )
                Message.setOrgWideEmailAddressId('0D2D0000000GndJKAS');
                
                Mails.add(Message);
            }
        }
          
        /*
         *  Fetch the Account Owners for the "Send to Account Manager"
         */
        if (ULCDetailsID.size() > 0 && TemplateNameToId.containsKey('HP: Send to Account Manager'))
        {
            string TemplateID = TemplateNameToId.get('HP: Send to Account Manager');

            for (ULC_Details__c Details : [select Id, LeadId__c, LeadId__r.OwnerId, ContactId__c, ContactId__r.Account.OwnerId from ULC_Details__c where Id in :ULCDetailsID])
            {
                Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
                
                Message.setTemplateId(TemplateID);
                if (Details.LeadId__c != null) 
                {   
                    Message.setTargetObjectId(Details.LeadId__r.OwnerId);
                } 
                else if (Details.ContactId__c != null)
                {
                    Message.setTargetObjectId(Details.ContactId__r.Account.OwnerId);
                } 
                else
                {
                    continue;
                }   
                Message.setSaveAsActivity(false);
                Message.setWhatId(Details.Id);
                Message.setUseSignature(false);
                
                Mails.add(Message);             
            }   
        }
        
        
        

        

        /*********************************************************
            
            SEND EMAILS
        
        *********************************************************/  
        if (Mails.size() > 0) 
        {
            try
            {
                Messaging.sendEmail(Mails);
            }
            catch (System.Exception Ex)
            {
                System.debug('ULC.HandleStatusChange - Cought exception while trying to send email - ' + Ex.getMessage());
            }       
        }   
        
        if (!isBefore && ToUpdate.size() > 0)
        {
            update ToUpdate;
        }
        if (LMSUsernames.size() > 0)
        {
            CallUpsertViewCentralUser(LMSUsernames);
        }
    }

    public static void updateULCfromHeroku(List<ULC_Details__c> NewValues)
    {
        Map<Id, String> MapLeadSourceDetails = new Map<Id, String>();       
        for (ULC_Details__c L:NewValues)
        {
    
            if (L.ULC_DataInjection__c != null && L.ULC_DataInjection__c != '')
            {                   

                String incentive = '';
                String campaignstatus = '';
                String leadsourcedetail = '';

                JSONParser parser = JSON.createParser(L.ULC_DataInjection__c);
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                        (parser.getText() == 'CampaignId')) {
                        // Get the value.
                        parser.nextToken();
                        incentive = parser.getText();
                    }
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                        (parser.getText() == 'CampaignStatus')) {
                        // Get the value.
                        parser.nextToken();
                        campaignstatus = parser.getText();
                    }
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                        (parser.getText() == 'Lead_Source_Detail_Mirror__c')) {
                        // Get the value.
                        parser.nextToken();
                        leadsourcedetail = parser.getText();
                    }
                }

                if(L.ContactId__c == null && leadsourcedetail != '') {
                    MapLeadSourceDetails.put(L.LeadId__c, leadsourcedetail);
                }

                if(incentive != '') {
                    CampaignMember cm = new CampaignMember();
                    List<CampaignMember> CampaignMembers = new List<CampaignMember>();
                    cm.CampaignId = incentive;
                    cm.Status = campaignstatus;
                    if (L.ContactId__c == null) {
                        cm.LeadId = L.LeadId__c;                        
                    } else {
                        cm.ContactId = L.ContactId__c;
                    }
                    CampaignMembers.add(cm);

                    insert CampaignMembers;
                }

                L.ULC_DataInjection__c = null;                  
            }
        }

        if (MapLeadSourceDetails.size() > 0)
        {       
            List<Lead> LeadsToUpdate = new List<Lead>();
            for(Lead lead : [select Id, Lead_Source_Detail_Mirror__c from Lead where Id in :MapLeadSourceDetails.keySet()])
            {
                lead.Lead_Source_Detail_Mirror__c = MapLeadSourceDetails.get(lead.Id);
                LeadsToUpdate.Add(lead);
            }
            
            if (LeadsToUpdate.size() > 0)
            {
                update LeadsToUpdate;
            }
        }        
    }

    public static void CheckChangeUsername(List<ULC_Details__c> NewValues, List<ULC_Details__c> OldValues)
    {
        
        Map<string, string> ContactToULC = new Map<string, string>();
        Set<string> NewUsers = new Set<string>();
        
        /* Need to create a Map to get the Account.Name as it cannot be referrenced from NewValues as
           relations(__r) are not being passed by trigger */        
        
        Map<Id, Contact> MapContactToAcc = new Map<Id, Contact>([SELECT Account.Name,Account.Id FROM Contact WHERE Id IN ( SELECT ContactID__c FROM ULC_Details__C WHERE Id IN:NewValues)]);
        
        for (integer i = 0; i < NewValues.size(); i++)
        {
            if (NewValues[i].ULCName__c != OldValues[i].ULCName__c) 
            {
               Contact MapAcc = MapContactToAcc.get(NewValues[i].ContactId__c);
                 
                //Check if it is Single Sign On(SSO) Account 
                if((MapAcc != null) && (MapAcc.Account.Id == '0012000000I7QJh' || (MapAcc.Account != null && MapAcc.Account.Name == 'QlikTech Single Signon Hold Account')))
                {
                    // Internal user login should only be 3 oR 7 Characters
                    if (NewValues[i].ULCName__c.length() != 7 && NewValues[i].ULCName__c.length() != 3 ) 
                    {
                        NewValues[i].addError('Internal UserName must be 3 or 7 characters');
                        continue;           
                    }                   
                }
                else 
                {   //Check for External user login
                    if (NewValues[i].ULCName__c.length() < 8 )
                    {           
                        NewValues[i].addError('External UserName is to short');
                        continue;
                    } 
                                
                }

                System.debug('Old username: ' + OldValues[i].ULCName__c + ' New username: ' + NewValues[i].ULCName__c);
                if (NewValues[i].ContactId__c != null) 
                {
                    ContactToULC.put(NewValues[i].ContactId__c, NewValues[i].ULCName__c);
                }
                NewUsers.add(NewValues[i].ULCName__c);
            }   
        }       
        
        if (NewUsers.size() == 0) {
            return;     // No usernames has been changed        
        } 
        
        // Check for UserNames that is already in use (in ULC_Details__c)
        for (ULC_Details__c ExistingUserNames : [select Id, ULCName__c from ULC_Details__c where ULCName__c in :NewUsers])
        {
            for (ULC_Details__c Detail : NewValues) {
                if (Detail.ULCName__c == ExistingUserNames.ULCName__c && Detail.Id != ExistingUserNames.Id) {
                    Detail.addError('UserName allready in use');
                    if (Detail.ContactId__c != null)
                    {
                        ContactToULC.remove(Detail.ContactId__c);
                    }                   
                }               
            }           
        }
        
        // At this point we only care about Contacts (Partners, Customer)
        if (ContactToULC.size() == 0) {
            return;                 // No usernames left to process
        }
        
        // Check if username exists in User tables
        Set <string> UserNames = new Set<string>();
        for (string username : ContactToULC.values())
        {
            UserNames.add(username + '@qlik.com');
        }
        
        for (User user : [select Id, username from User where username in :UserNames])
        {
            for (ULC_Details__c Detail : NewValues) {
                if (Detail.ULCName__c + '@qlik.com' == user.username) {
                    Detail.addError('UserName allready in use [USER]');
                    ContactToULC.remove(Detail.ContactId__c);
                }               
            }                                   
        }

        if (ContactToULC.size() == 0) {
            return;                 // No usernames left to process
        }

        /*

        // Change the usernames
        List<User> UsersToUpdate = new List<User>();
        for (User user : [select Id, ContactId, username from User where IsActive = true and ContactId in :ContactToULC.keySet()])
        {
            user.username = ContactToULC.get(user.ContactId) + '@qlik.com'; 
            UsersToUpdate.add(user);        
        }
        
        if (UsersToUpdate.size() > 0)
        {
            update UsersToUpdate;
        }
        
        */
        
    }


    public static Map<string, Boolean> IsRequiredCPQUserUpdate(List<User> NewValues, List<User> OldValues, Set<ID> Profiles)
    {
        
        Map<string, Boolean> MapIdRequiredUpdate = new Map<string,Boolean>();
        Boolean needsUpdate = false;
        //if old values != new values return true, otherwise false;
        
        Map<ID, string> ProfileIdToLicenseName = new Map<ID, string>();
        
        for (Profile profile : [select Id, UserLicense.Name from Profile where Id in :Profiles])
        {
            ProfileIdToLicenseName.put(profile.Id, profile.UserLicense.Name);               
        }
        
        for (integer i = 0; i<NewValues.size(); i++)
        {
            
            needsUpdate = false;
            
            User user = NewValues[i];
            User oldUser = OldValues[i];
            string LicenseName = ProfileIdToLicenseName.get(user.ProfileId);

            system.debug('IsRequiredCPQUserUpdate.LicenseName: ' + LicenseName);
            
            if (LicenseName != null && !LicenseName.toUpperCase().contains('SALESFORCE'))  // Only look at CRM user (no portal users)
            {
                continue;
            }
            system.debug('user.Trigger_CPQ_user_creation__c: ' + user.Trigger_CPQ_user_creation__c);
            system.debug('oldUser.Trigger_CPQ_user_creation__c: ' + oldUser.Trigger_CPQ_user_creation__c);
            system.debug('user.UserName: ' + user.UserName);
            system.debug('oldUser.UserName: ' + oldUser.UserName);
            if (user.Trigger_CPQ_user_creation__c == true && oldUser.Trigger_CPQ_user_creation__c==false)
            {
                needsUpdate = true;             
            }
            if (!needsUpdate && user.UserName != oldUser.UserName)
            {
                needsUpdate = true;             
            }
            if (!needsUpdate && user.Title != oldUser.Title)
            {
                needsUpdate = true;             
            }
            if (!needsUpdate && user.FirstName != oldUser.FirstName)
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.LastName != oldUser.LastName)
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.Email != oldUser.Email)
            {
                needsUpdate = true;
            }
            //Related with CPQGroup are CPQRegion and SFDCRoleName
            if (!needsUpdate && user.QlikTech_Region__c  != oldUser.QlikTech_Region__c )
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.UserRole.Name  != oldUser.UserRole.Name )
            {
                needsUpdate = true;
            }
            //Company code is related with QlikTechCompany
            if (!needsUpdate && user.QlikTech_Company__c != oldUser.QlikTech_Company__c)
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.IsActive  != oldUser.IsActive )
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.Manager.Username  != oldUser.Manager.Username)
            {
                needsUpdate = true;
            }
            //TODO Need to add Delegated Approver
            if (!needsUpdate && user.DelegatedApproverId  != oldUser.DelegatedApproverId)
            {
                needsUpdate = true;
            }
            
            if (!needsUpdate && user.City  != oldUser.City)
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.State  != oldUser.State)
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.PostalCode  != oldUser.PostalCode)
            {
                needsUpdate = true;
            }
            
            if (!needsUpdate && user.Country  != oldUser.Country)
            {
                needsUpdate = true;
            }
            if (!needsUpdate && user.Phone  != oldUser.Phone)
            {
                needsUpdate = true;
            }
            if (needsUpdate)
            {
                //Add Id and isrequired value to the map
                MapIdRequiredUpdate.put(user.Id, needsUpdate);
            }
        }
        
        return MapIdRequiredUpdate;
        
    }


    public static string GeneratePwd(integer length)
    {
        string AllowedChar = 'abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        string GeneratedPassword = '';
        
        for (integer i=0; i<length; i++) 
        {
            integer chr = Math.round(Math.floor(AllowedChar.length() * Math.random()));
            GeneratedPassword += AllowedChar.substring(chr, chr+1); 
        }
        System.debug('ULC.GeneratePwd generated password is: ' + GeneratedPassword);
        return GeneratedPassword;
    }
    
    public static void ContactLeadRelayware(List<ULC_Details__c> triggerNew, List<ULC_Details__c> triggerOld) 
    {
        Set<Id> ContactIDs = new Set<Id>();
        Set<Id> LeadIDs = new Set<Id>();
        List<Contact> ContactsToUpdate = new List<Contact>();
        List<Lead> LeadsToUpdate = new List<Lead>();

        for (Integer i = 0; i < triggerNew.size(); i++)
        {   
            System.debug('BAD NEW : ' + triggerNew[i].ContactId__c + '--' + ' or Lead: ' + triggerNew[i].LeadId__c);            
            System.debug('BAD OLD : ' + triggerOld[i].ContactId__c + '--' + ' or Lead: ' + triggerOld[i].LeadId__c);
            
            //CONTACT
            if(triggerNew[i].ContactId__c != null)
            {
                //Contacts have been swaped on ULC record
                if(triggerOld[i].ContactId__c != null && triggerOld[i].ContactId__c != triggerNew[i].ContactId__c)
                {
                    System.debug('BAD Contacts been swaped');
                    ContactIDs.add(triggerOld[i].ContactId__c);
                }
                //Lead has been changed to a Contact on ULC record
                else if(triggerOld[i].ContactId__c == null && triggerOld[i].LeadId__c != null)
                {
                    LeadIDs.Add(triggerOld[i].LeadId__c);
                }
            }
            //LEAD
            if(triggerNew[i].LeadId__c != null)
            {
                //Leads have been swaped on ULC record
                if(triggerOld[i].LeadId__c != null && triggerOld[i].LeadId__c != triggerNew[i].LeadId__c)
                {
                    LeadIDs.Add(triggerOld[i].LeadId__c);
                }
                //Contact has been changed to a Lead on ULC record
                else if(triggerOld[i].LeadId__c == null && triggerOld[i].ContactId__c != null)
                {
                    ContactIDs.add(triggerOld[i].ContactId__c);
                }
            }
            //LEAD & CONTACT, removed and no new lead or contact were added
            if(triggerNew[i].LeadId__c == null && triggerNew[i].ContactId__c == null)
            {
                System.debug('Allt borta');
                if(triggerOld[i].ContactId__c != null )
                {
                    ContactIDs.add(triggerOld[i].ContactId__c);
                }                
                else if(triggerOld[i].LeadId__c != null )
                {
                    LeadIDs.Add(triggerOld[i].LeadId__c);     
                }
            }
        }

        for(Contact con : [select Id, QlikID__C from Contact where Id in :ContactIDs and QlikID__C != null])
        {
            con.QlikID__C = '';
            ContactsToUpdate.Add(con);
        }

        for(Lead lead : [select isConverted, Id, QlikID__C from Lead where Id in :LeadIDs and QlikID__C != null])
        {
            //If process is triggered from a lead conversion, don't update Lead since reference won't exist
            if(!lead.isConverted) 
            {
                lead.QlikID__C = '';
                LeadsToUpdate.Add(lead);
            }
        }

        if (ContactsToUpdate.size() > 0)
        {
            update(ContactsToUpdate);
        }
        
        if (LeadsToUpdate.size() > 0)
        {           
            update(LeadsToUpdate);
        }        
    }

    public static void ulcOUserProcess(Id ulcId){
        System.debug('debug_var ' + System.isFuture());
        ULC_Details__c uLCDetails = [SELECT id, oUser__c FROM ULC_Details__c WHERE Id = : ulcId];

        HerokuUtillities.ULCData ULCData = new HerokuUtillities.ULCData ();
        ULCData = (HerokuUtillities.ULCData) JSON.deserialize (uLCDetails.oUser__c, HerokuUtillities.ULCData.class);  

        HerokuUtillities.retStruct res = HerokuUtillities.SetUser(new HerokuUtillities.ULCUser(ULCData.oUser), 
                (String.isBlank(ULCData.additionalParams.SupplimentalData) ? '' : ULCData.additionalParams.SupplimentalData),
                (String.isBlank(ULCData.additionalParams.Asset) ? '' : ULCData.additionalParams.Asset),
                (String.isBlank(ULCData.additionalParams.ZiftData) ? '' : ULCData.additionalParams.ZiftData)); 
        
        System.debug('OnCreate_ULCDetails - retStruct status starts');
        System.debug(res.success);
        System.debug('OnCreate_ULCDetails - retStruct status ends');

        
    }
}