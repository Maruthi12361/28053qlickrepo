/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_Unauthenticated_Chatter_CntrlTest {
    
    // Unit test for QS_Unauthenticated_Chatter_Controller Apex class and QS_Unauthenticated_Chatter Visualforce component
    static testMethod void testUnauthenticatedChatter_Controller() {
            
        QTTestUtils.GlobalSetUp();
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_Thomas', 'test_Jones', 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;

        Contact testContact2 = TestDataFactory.createContact('test_Thomas2', 'test_Jones2', 'testSandboxTJ2@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact2;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        User communityUser2 = TestDataFactory.createUser(profileId, 'testSandbox2@qlikTech.com', 'tSbox2', String.valueOf(testContact2.Id));
        insert communityUser;


        //Get the community user
        //List<User> communityUserList = [Select Id, Name, ContactId, Contact.Persona__c, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 2];
        
        //system.assert(communityUserList.size() > 1, 'No community users found.');

        //User communityUser = communityUserList[0];
        //User communityUser2 = communityUserList[1];
        
        // Create QS_HomePage_Chatter_Announcement__c
        //QS_HomePage_Chatter_Announcement__c announcementObj = TestDataFactory.createChatterAnnouncement('Announcement', 'As you might already know, we recently released the next-generation data visualisation and discovery application - Qlik Sense Desktop', 'Sam Qlik Engineer (Qlik.com)', 'Do you have any Questions about Qlik Sense Desktop?');
        //insert announcementObj;
        
        // Create collaborationGroup

        string chatterGroupName = 'testUnauthenticatedChatter CG';


        CollaborationGroup cg = new CollaborationGroup();
        cg.CollaborationType = 'Public';
        cg.CanHaveGuests = false;
        cg.Description = 'Test 1';
        cg.InformationBody = 'test';
        cg.InformationTitle = 'test';
        cg.Name = chatterGroupName;
        cg.OwnerId = Userinfo.getUserId();

        insert cg;


        CollaborationGroup collaborationGroup = //TestDataFactory.createChatterGroup(userinfo.getUserId(), communityUser.NetworkMemberUsers[0].NetworkId, System.Label.QS_Chatter_Group_Blog, System.Label.QS_Chatter_Group_Blog, 'Test Body', 'Test Description');
        [Select Id, Name From CollaborationGroup where Name = :chatterGroupName LIMIT 1]; //
        //insert collaborationGroup;
        
        //CollaborationGroupMember groupMember = new CollaborationGroupMember(CollaborationGroupId = collaborationGroup.Id, CollaborationRole = 'Standard', MemberId = communityUser.Id, NotificationFrequency = 'P');
        //insert groupMember;
        
        List<FeedItem> feedItemList = new List<FeedItem>();
        
        FeedItem testFeed1 = TestDataFactory.createFeedItem('\" Test Title \" \n Test Description.', collaborationGroup.Id);
        testFeed1.Type = 'TextPost';
        testFeed1.Visibility = 'AllUsers';
        
        feedItemList.add(testFeed1);
        
        FeedItem testFeed2 = TestDataFactory.createFeedItem('\" Test Title 2 \" \n Test Description2.', collaborationGroup.Id);
        testFeed2.Type = 'TextPost';
        testFeed2.Visibility = 'AllUsers';
        feedItemList.add(testFeed2);
        
        insert feedItemList;
        
        Test.startTest();
        
            QS_Unauthenticated_Chatter_Controller chatterController = new QS_Unauthenticated_Chatter_Controller();
            System.runAs(communityUser) {
                chatterController = new QS_Unauthenticated_Chatter_Controller();
            }
            
            System.runAs(communityUser2) {
                chatterController = new QS_Unauthenticated_Chatter_Controller();
            }
       
        Test.stopTest();
    
        
    }
        
}