// CR# 9885
// controller class behind the visualforce page: Opportunity_PDF_Template
// Change log:
// September 27, 2013 - Initial Implementation - Madhav Kakani - Fluido Oy
public with sharing class OpportunityPDFTemplateCtrl {
    private Opportunity opp;
    public List<OpportunityLineItem> lItems {get;set;}
    public List<Non_Licence_Related_ProductsCO_Non_Li__c> lNRPItems {get;set;}
    public Boolean showItems1 {get;set;}    
    public Boolean showItems2 {get;set;}
    
    public OpportunityPDFTemplateCtrl(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();

        showItems1 = false;
        lItems = [SELECT Name__c, Product_Family__c, Description, Quantity, ListPrice, Discount__c,
                    Total_Net_before_Partner_Margin__c, Partner_Margin_RO__c, UnitPrice, TotalPrice
                    FROM OpportunityLineItem WHERE OpportunityId = :opp.Id];
       if(lItems.size() > 0) showItems1 = true;

        showItems2 = false;
        lNRPItems = [SELECT Name, Application__c, Description__c, Qty__c, List_Price__c,
                       Discount_perc__c, Net_Price__c, Product_Family__c
                       FROM Non_Licence_Related_ProductsCO_Non_Li__c WHERE Opportunity__c = :opp.Id];
       if(lNRPItems.size() > 0) showItems2 = true;
    }
}