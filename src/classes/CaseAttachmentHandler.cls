/**
*   extcqb  2020-01-30 IT-2196 Make chatter posts about case attachments 'QlikTech Only'
*/

public class CaseAttachmentHandler {

    public static final String INTERNAL_VISIBILITY = 'InternalUsers';

    public static void handleCaseAttachments(List<Attachment> attachments) {
        populateFieldsOnCase(attachments);
        setAttachmentFeedItemVisibility(attachments);
    }

    private static void populateFieldsOnCase(List<Attachment> attachments) {
        Map<Id, Case> casesToUpdate = new Map<Id, Case>();
        for(Attachment attachment : attachments){
            casesToUpdate.put(attachment.ParentId, new Case(Id = attachment.ParentId, Attachment_added__c = attachment.Name));
        }
        if(!casesToUpdate.isEmpty()) {
            update casesToUpdate.values();
        }
    }

    private static void setAttachmentFeedItemVisibility(List<Attachment> attachments) {
        Set<Id> caseIds = new Set<Id>();
        for (Attachment attachment : attachments) {
            caseIds.add(attachment.ParentId);
        }
        updateCaseFeedItems(caseIds);
    }

    /* Future used here because we can't catch 'CreateRecordEvent' FeedItems in before insert FeedItem trigger
    * so this method updates last feed item on each Case to make it invisible for community users
    * */
    @Future
    private static void updateCaseFeedItems(Set<Id> caseIds) {
        List<FeedItem> feedItems = [SELECT Id, ParentId, CreatedDate, Type FROM FeedItem WHERE Type = 'CreateRecordEvent' AND ParentId IN :caseIds];
        List<FeedItem> feedItemsToUpdate = getLastFeedItemFromEachRecord(feedItems);
        for (FeedItem feedItem : feedItemsToUpdate) {
            feedItem.Visibility = INTERNAL_VISIBILITY;
        }
        if (!feedItemsToUpdate.isEmpty()) {
            update feedItemsToUpdate;
        }
    }

    private static List<FeedItem> getLastFeedItemFromEachRecord(List<FeedItem> feedItems) {
        Map<Id, FeedItem> lastFeedItemsByParentIds = new Map<Id, FeedItem>();
        for (FeedItem feedItem : feedItems) {
            if (!lastFeedItemsByParentIds.containsKey(feedItem.ParentId)) {
                lastFeedItemsByParentIds.put(feedItem.ParentId, feedItem);
            } else {
                if (lastFeedItemsByParentIds.get(feedItem.ParentId).CreatedDate < feedItem.CreatedDate) {
                    lastFeedItemsByParentIds.put(feedItem.ParentId, feedItem);
                }
            }
        }
        return lastFeedItemsByParentIds.values();
    }
}