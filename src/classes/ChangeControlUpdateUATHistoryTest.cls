/************************************************************************************
*
*	ChangeControlUpdateUATHistoryTest
*	Test class for trigger ChangeControlUpdateUATHistory
*	CR# 10110 - Add Release Management to CR Process
*	
*	2014-11-13 		AIN 	Initial Development
*
*************************************************************************************/
@isTest
public with sharing class ChangeControlUpdateUATHistoryTest {
	
	public static testmethod void SuccesTest() 
	{
		List<SLX__Change_Control__c> changes = new List<SLX__Change_Control__c>();
		List<Id> changeIds = new List<Id>();
		SLX__Change_Control__c change1 = new SLX__Change_Control__c();
		string evidence1 = 'Evidence 1';

		change1.Submitted_to_requestor__c = false;
		change1.UATEvidence__c = evidence1;

		changes.add(change1);

		insert changes;
		
		for(SLX__Change_Control__c c : changes)
		{
			changeIds.add(c.id);
		}

		change1.Submitted_to_requestor__c = true;

		update changes;

		changes = [select id, name, Submitted_to_requestor__c, UATEvidence__c, UATEvidence_History__c from SLX__Change_Control__c where id in :changeIds];
		if(changes.size() > 0)
		{
			change1 = changes[0];
			system.debug('Submitted_to_requestor__c: ' + change1.Submitted_to_requestor__c);
			system.debug('UATEvidence__c: ' + change1.UATEvidence__c);
			system.debug('UATEvidence_History__c: ' + change1.UATEvidence_History__c);
			system.debug('evidence1: ' + evidence1);

			system.assert(change1.UATEvidence__c == null);

			system.assert(change1.UATEvidence_History__c.Contains(evidence1));
		}
		else
			system.assert(false);

		
	}
	public static testmethod void FailureTest() 
	{
		List<SLX__Change_Control__c> changes = new List<SLX__Change_Control__c>();
		List<Id> changeIds = new List<Id>();
		SLX__Change_Control__c change1 = new SLX__Change_Control__c();
		string evidence1 = 'Evidence 1';

		change1.Submitted_to_requestor__c = false;
		change1.UATEvidence__c = evidence1;

		changes.add(change1);

		insert changes;
		
		for(SLX__Change_Control__c c : changes)
		{
			changeIds.add(c.id);
		}

		//change1.Submitted_to_requestor__c = true;

		update changes;

		changes = [select id, name, Submitted_to_requestor__c, UATEvidence__c, UATEvidence_History__c from SLX__Change_Control__c where id in :changeIds];
		if(changes.size() > 0)
		{
			change1 = changes[0];
			system.debug('Submitted_to_requestor__c: ' + change1.Submitted_to_requestor__c);
			system.debug('UATEvidence__c: ' + change1.UATEvidence__c);
			system.debug('UATEvidence_History__c: ' + change1.UATEvidence_History__c);
			system.debug('evidence1: ' + evidence1);

			system.assert(change1.UATEvidence__c == evidence1);

			system.assert(change1.UATEvidence_History__c == null);
		}
		else
			system.assert(false);
	}
}