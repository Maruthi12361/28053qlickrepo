/**
 * @author  Tquila Support
 * @date    27/10/2014
 *  
 * Trigger Handler class for QS_CaseEnvironmentAssociation_Tgr trigger.
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 *
 */

public class QS_CaseTriggerHandler {
    
    /**
     * @author  Tquila Support
     * @date    27/10/2014
     *  
     * @usedBy QS_CaseEnvironmentAssociation_Tgr trigger.
     * Update the associated environment with last used date which is the created date from the latest case related to the environment.
     * Trigger is fired after insert, update and delete event. 
     */
    public static void updateEnvironmentLastUsedDate(List<Case> caseNewList, Map<Id, Case> caseMapList, Boolean isInsert, Boolean isUpdate, Boolean isDelete) {
        
        Set<Id> environmentIdSet = new Set<Id>();
        List<AggregateResult> caseAggregateList;
        List<Environment__c> environmentUpdateList;
        
        if(isInsert || isUpdate) {
            for(Case caseObj : caseNewList) {
                if(isInsert) {
                    if(caseObj.EnvironmentOfCase__c != null)
                        environmentIdSet.add(caseObj.EnvironmentOfCase__c);
                } else if(isUpdate) {
                    if(caseMapList != null && caseMapList.keySet() != null && !caseMapList.keySet().isEmpty()
                        && caseObj.EnvironmentOfCase__c != caseMapList.get(caseObj.Id).EnvironmentOfCase__c) {
                        if(caseObj.EnvironmentOfCase__c != null)        
                            environmentIdSet.add(caseObj.EnvironmentOfCase__c);
                        if(caseMapList.get(caseObj.Id).EnvironmentOfCase__c != null)        
                            environmentIdSet.add(caseMapList.get(caseObj.Id).EnvironmentOfCase__c);
                    }
                }
                    
            }
        } else if(isDelete) {
            for(Id caseObjId : caseMapList.keySet()) {
                if(caseMapList.get(caseObjId) != null && caseMapList.get(caseObjId).EnvironmentOfCase__c != null)
                    environmentIdSet.add(caseMapList.get(caseObjId).EnvironmentOfCase__c);
            }
        }
        
        if(environmentIdSet != null && !environmentIdSet.isEmpty())
            caseAggregateList = [Select EnvironmentOfCase__c environmentId, MAX(CreatedDate) createdDate 
                                    From Case 
                                    where EnvironmentOfCase__c IN :environmentIdSet
                                    GROUP BY EnvironmentOfCase__c];
        
        if(caseAggregateList != null && !caseAggregateList.isEmpty()) {
            environmentUpdateList = new List<Environment__c>();
            for(AggregateResult caseAggrResult : caseAggregateList) {
                if(caseAggrResult != null) {
                    if(caseAggrResult.get('createdDate') != null && caseAggrResult.get('environmentId') != null) {
                        environmentUpdateList.add(new Environment__c(Id=((Id) caseAggrResult.get('environmentId')), Last_Used_Date__c = ((DateTime) caseAggrResult.get('createdDate'))));
                    }
                }
            }
            
            if(environmentUpdateList != null && !environmentUpdateList.isEmpty()) {
                Database.update(environmentUpdateList, false);
            }
        }
        
        
        
    }
    
    
    public static void updateCaseOwner(List<Case> caseList) {
        Set<String> caseQueueNameSet = new Set<String>();
        for(Case caseObj : caseList) {
            if(caseObj.Queue_Name_Owner__c != null && caseObj.Queue_Name_Owner__c != '')
                caseQueueNameSet.add(caseObj.Queue_Name_Owner__c);
        }
        
        if(caseQueueNameSet != null && !caseQueueNameSet.isEmpty()) {
            List<QueueSobject> queueList = [Select SobjectType, Queue.Type, Queue.Name, QueueId 
                                                        From QueueSobject 
                                                        Where SObjectType = 'Case' AND Queue.Name IN :caseQueueNameSet AND Queue.Type = 'Queue' 
                                                        ORDER BY Queue.Name, Queue.Type];
            
            if(queueList != null && !queueList.isEmpty()) {
                //create a Map for Queue Name and Queue Id
                Map<String, String> queueNameIdMap = new Map<String, String>();
                for(QueueSobject queueObj : queueList) {
                    if(queueNameIdMap == null || queueNameIdMap.keySet() == null || queueNameIdMap.keySet().isEmpty() || !queueNameIdMap.containsKey(queueObj.Queue.Name)) {
                        queueNameIdMap.put(queueObj.Queue.Name, queueObj.QueueId);
                    }
                }
                
                if(queueNameIdMap != null && queueNameIdMap.keySet() != null && !queueNameIdMap.keySet().isEmpty()) {
                    for(Case caseObj : caseList) {
                        if(caseObj.Queue_Name_Owner__c != null && caseObj.Queue_Name_Owner__c != '' 
                            && queueNameIdMap.containsKey(caseObj.Queue_Name_Owner__c) && queueNameIdMap.get(caseObj.Queue_Name_Owner__c) != null)
                            System.debug('%%%queueNameIdMap: '+ queueNameIdMap);
                            caseObj.OwnerId = queueNameIdMap.get(caseObj.Queue_Name_Owner__c);
                    }
                }
            }
        }
        
    }         
}