/***************************************************************************
Class: TEST_User_DeactivatedUpdateProfile 

This class tests the User_DeactivatedUpdateProfile trigger 
*   This trigger will make sure the User account will populate the Last profile field
*   and set the Profile to Archive User whenever a user is deactivated 

Changelog: 
2012-10-23 SAN new unit test
***************************************************************************/
@isTest
private class TEST_User_DeactivatedUpdateProfile {

    static testMethod void DeactivatedUser_Test() {
    	
        test.startTest();
        String originalProfile = 'Standard User';
    	Profile p = [SELECT Id,Name FROM Profile WHERE Name=:originalProfile];
		Profile p2 = [SELECT Id FROM Profile WHERE Name='Archive User'];
    	
        User userTest = new User(Username = 'Noemail11@qlikview.com', LastName = 'Test11 Lastname', Email = 'Noemail11@qlikview.com', 
    						 Alias = 'tula11', CommunityNickname = 'nick11', TimeZoneSidKey = 'Europe/Paris', 
    						 LocaleSidKey = 'es_ES', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', ProfileId=p.Id, IsActive=true);
        insert userTest;

    	System.assertEquals(p.Id, userTest.ProfileId, 'New User Profile failed');
 
        
      	User test2 = [SELECT ProfileId, IsActive  from User WHERE Id=:userTest.Id];
      
      	test2.IsActive = false;
      	update test2;
                                
   	  	test2 = [SELECT Last_Profile__c, ProfileId from User WHERE Id=:userTest.Id];
         
    	System.assertEquals(p.Name, test2.Last_Profile__c, 'Previous Profile failed');
    	System.assertEquals(p2.Id, test2.ProfileId, 'New Archive User Profile failed');
    	
        test.stopTest();        
    }
    
    static testMethod void DeactivatedNonEmployeeUser_Test() {
    	
        test.startTest();
        String originalProfile = 'Standard User';
    	Profile p = [SELECT Id,Name FROM Profile WHERE Name=:originalProfile];
		Profile p2 = [SELECT Id FROM Profile WHERE Name='Archive User'];
    	
        User userTest = new User(Username = 'Noemail22@email.com', LastName = 'Test22 Lastname', Email = 'Noemail22@email.com', 
    						 Alias = 'tula22', CommunityNickname = 'nick22', TimeZoneSidKey = 'Europe/Paris', 
    						 LocaleSidKey = 'es_ES', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', ProfileId=p.Id, IsActive=true);
        insert userTest;

    	System.assertEquals(p.Id, userTest.ProfileId, 'New User Profile failed');
 
        
      	User test2 = [SELECT ProfileId, IsActive  from User WHERE Id=:userTest.Id];
      
      	test2.IsActive = false;
      	update test2;
                                
   	  	test2 = [SELECT Last_Profile__c, ProfileId from User WHERE Id=:userTest.Id];
         
    	System.assertEquals(p.Id, test2.ProfileId, 'New Archive non employee User Profile failed');
    	
        test.stopTest();        
    }    
}