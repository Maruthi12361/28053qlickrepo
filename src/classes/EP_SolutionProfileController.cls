public with sharing class EP_SolutionProfileController {    
    public Solution_Profiles__c SolProfile{get; set;}
    public Solution_Profiles__c OriginalSolProfile{get; set;}
    public Account account {get; set;}
    public Integer StepNumber {get; set;}
    public Integer RedirectStepNumber {get; set;}
    public Partner_Expertise_Application_Lookup__c Lookup {get; set;}
    public Boolean IsProfileLocked {
        get { return SolProfile.Status__c == 'Under Review' || SolProfile.Status__c == 'Approved'; }
    }
    public Boolean ShowOpportunity 
    {
        get 
        {
            return (account.Partner_Category__c  == 'Master Reseller & Distribution' ||
                    account.Partner_Category__c  == 'Solution Provider');
        } 
   }
    public Partner_Expertise_Area__c Expertise 
    {
        get{
            return [Select Id, 
            Expertise_Area__c
            from Partner_Expertise_Area__c where Id =: ExpertiseId];
        }
       
        private set;
    }
    public Id ExpertiseId 
    {
        get 
        {
            return ExpertiseId;
        }  
        set
        {

            ExpertiseId = ApexPages.currentPage().getParameters().get('expertId');
        }
    }
    public Id SolProfileID
    {
        get 
        {
            return SolProfileID;
        }  
        set;
    }
    public List<Solution_Profiles__c> SolutionProfiles = new List<Solution_Profiles__c>();
    
    
    public EP_SolutionProfileController () {
        String currentPageUrl = ApexPages.currentPage().getUrl().toLowerCase();
        system.debug('Current URL:' +currentPageUrl);
        String accId = ApexPages.currentPage().getParameters().get('acc');
        system.debug('11111 accountId =' + accId);
        account = [SELECT Id,Name, Partner_Category__c, RecordTypeId FROM Account WHERE Id=:accId];
        ExpertiseId = ApexPages.currentPage().getParameters().get('expertId');
        system.debug('22222 ExpertiseId =' + ExpertiseId);

        //To check if the URL has Solution Profile ID
        
        Integer IndexSolProfId=currentPageUrl.indexOf('solprofid');
        SolProfileID=null;
        if (IndexSolProfId >= 0 && ApexPages.currentPage().getParameters().get('solprofid') != '')
        {
            SolProfileID = ApexPages.currentPage().getParameters().get('solprofid');
            system.debug('33333 SolProfileID =' + SolProfileID);
        }

        //To check if the URL has StepNumber
        Integer IndexStepNumber=currentPageUrl.indexOf('stepnumber');
        if (IndexStepNumber >=0)
        {
            StepNumber = Integer.valueof(ApexPages.currentPage().getParameters().get('stepNumber').trim());
            system.debug('44444 StepNumber =' + StepNumber);
        }

        system.debug('accid:' + account.Id + '--Expertise id:' + ExpertiseId);
        if(ExpertiseId != null && accId != null)
        {
            // Link from the Progress bar has Solution Profile ID if one was available in Lookup Obj
            if(SolProfileID != null) 
            {
                system.debug('1st If Condition');
                SolProfile=getNewOrExistingProfile(SolProfileID);
            }
            //Link from StepNumbers in Solution Profile wizard. No Solution Profile ID. 
            //Get the Solution profile by StepNumber if there is one 
            //Otherwise create new instance 
            else if(SolProfileID == null && StepNumber !=null ) 
            {
                system.debug('2nd If Condition');
                SolProfile=getNewOrExistingProfileByStep(StepNumber);
            }
            //Link from Add Customer References- Select Expertise Area and then load the incomplete solution profile 
            else 
            {
                system.debug('3rd If Condition');
                SolProfile=GetIncompleteSolutionProfile();
            }
            //Create deep copy of Solution profile
            OriginalSolProfile = SolProfile.Clone(true, true, true);
            //IsSolProfSame = true;
        }          
    }
    
    public Id Opportunity { get;set;}
    
    public List <SelectOption> Opportunities
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new selectOption('', 'None'));
            for ( Opportunity opp : ListOpportunities) 
            {
            	options.add(new SelectOption(opp.Id, opp.Name));            
        	} 
            return options;
        }
    }
    
    public List<Opportunity> ListOpportunities{
        get{
            List<Opportunity> listofOpps = [SELECT Id, 
                                            Name, 
                                            Account.Id
                                            FROM Opportunity 
                                            WHERE Sell_Through_Partner__c =: account.Id 
                                            AND StageName like '%Closed Won%' order by Name asc];
            return listofOpps;
        }
    }
    
    public List<selectOption> SolutionOwners {
        get{
            List<selectOption> options = new List<selectOption>(); 
            options.add(new selectOption('', 'None'));
            for (User users : [SELECT Id, Name FROM User WHERE AccountId =:account.Id order by Name asc]) 
            { 
                options.add(new selectOption(users.Id, users.Name));
            }
            return options;
        }
    }
    
    public Partner_Expertise_Application_Lookup__c GetLookupObject()
    {
        system.debug('Inside GetLookupObject accid:' + account.Id + '--Expertise id:' + ExpertiseId);
        Partner_Expertise_Application_Lookup__c LookupObject;
        List<Partner_Expertise_Application_Lookup__c> lookupList = [SELECT   a.Id,
                                                                   Account_Name__c,
                                                                   a.Account_Name__r.Name,
                                                                   Expertise_Area__c,
                                                                   Opportunity_Name__c,
                                                                   Solution_Profile_1_Name__c,
                                                                   Solution_Profile_one_Status__c,
                                                                   Solution_Profile_2_Name__c,
                                                                   Solution_Profile_Two_Status__c,
                                                                   Solution_Profile_3_Name__c,
                                                                   Solution_Profile_Three_Status__c,
                                                                   Solution_Profile_4_Name__c,
                                                                   Solution_Profile_Four_Status__c
                                                          FROM Partner_Expertise_Application_Lookup__c a
                                                          WHERE Account_Name__c =: account.Id
                                                          AND Expertise_Area__c =: ExpertiseId LIMIT 1];

        system.debug('After GetLookupObject query');
        if (lookupList.isEmpty())
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'There are no Partner_Expertise_Application_Lookup record defined!');
            
            LookupObject = new Partner_Expertise_Application_Lookup__c();
            LookupObject.Account_Name__c = account.Id;
            LookupObject.Expertise_Area__c = ExpertiseId;
        }
        else
        {
            LookupObject = lookupList[0];
        }
        system.debug('After instance GetLookupObject');
        return LookupObject;
    }
    
    public Solution_Profiles__c getNewOrExistingProfile(Id profile)
    {
        system.debug('getNewOrExistingProfile Profile Id :' + profile);
        Solution_Profiles__c solProf;
        if(profile == null)
        {
            //create new profile
            solProf = new Solution_Profiles__c();
            solProf.Partner_Expertise_Area_ID__c = ExpertiseId;
            solProf.Sector__c = Expertise.Expertise_Area__c;
            solProf.Account_Name__c = account.Id;            
        }
        else
        {
            solProf = [Select  s.Opportunity_Name__c, 
                        s.CreatedDate, 
                        s.Name, 
                        s.Id, 
                        //s.Expertise_Area__c, 
                        s.Account_Name__c,
                        s.Account_Name__r.Name,
                        s.Partner_Expertise_Area_ID__c,
                        s.Solution_Description__c,
                        s.Solution_Profile_End_User__c,
                        s.Sector__c,
                        s.Solution_Owner__c,
                        s.Industry__c,
                        s.Reviewed_by_Sales__c,
                        s.Function__c,
                        s.Reviewed_by_Marketing__c,
                        s.Department__c,
                        s.Not_for_Public_Use__c,
                        s.Solution_Area__c,
                        s.Go_Live_Date__c,
                        s.Deployment_Status__c,
                       Business_Issues_Goals__c,
                       IT_Issues_Goals__c,
                       Business_Value_Qualitative__c,
                       IT_Value_Qualitative__c,
                       No_of_Deployed_Users__c,
                       Time_to_Develop_Deploy__c,
                       Data_Volume_Records__c,
                       No_of_Potential_Users__c,
                       Data_Source_Systems__c,
                       Description_of_Data__c,
                       Description_of_Users__c,
                       Status__c,
                       CreatedById,
                       LastModifiedById,
                       Partner_Expertise_Area_ID__r.Expertise_Area__c
                       //OwnerId
                       From Solution_Profiles__c s 
                       WHERE Id =: profile];
        }
        return solProf;
    }
    public Solution_Profiles__c getNewOrExistingProfileByStep(Integer stepNumber)
    {
        system.debug('getNewOrExistingProfileByStep');
        Solution_Profiles__c solProf;
        Lookup = GetLookupObject();
        if(stepNumber==1)
        {
            solProf=getNewOrExistingProfile(Lookup.Solution_Profile_1_Name__c);
        }
        else if(stepNumber==2)
        {

            solProf=getNewOrExistingProfile(Lookup.Solution_Profile_2_Name__c);
        }
        else if(stepNumber==3)
        {

            solProf=getNewOrExistingProfile(Lookup.Solution_Profile_3_Name__c);
        }
        else if(stepNumber==4)
        {

            solProf=getNewOrExistingProfile(Lookup.Solution_Profile_4_Name__c);
        }
        return solProf;

    }
    
    @TestVisible private Boolean StatusEditable(String Status)
    {
        return (Status != 'Under Review' && Status != 'Approved');
    }
    public Solution_Profiles__c GetIncompleteSolutionProfile()
    {
        system.debug('555');
        Solution_Profiles__c solProf;
        Lookup = GetLookupObject();
        if(StatusEditable(lookup.Solution_Profile_one_Status__c))
        {
            solProf = getNewOrExistingProfile(lookup.Solution_Profile_1_Name__c);
            StepNumber = 1;
        }
        else if(StatusEditable(lookup.Solution_Profile_Two_Status__c))
        {
            solProf = getNewOrExistingProfile(lookup.Solution_Profile_2_Name__c);
            StepNumber = 2;
        }
        else if(StatusEditable(lookup.Solution_Profile_Three_Status__c ))
        {
            solProf = getNewOrExistingProfile(lookup.Solution_Profile_3_Name__c);
            StepNumber = 3;
        }
        else if(StatusEditable(lookup.Solution_Profile_Four_Status__c))
        {
            solProf = getNewOrExistingProfile(lookup.Solution_Profile_4_Name__c);
            StepNumber = 4;
        }
        else
        {
            solProf = getNewOrExistingProfile(lookup.Solution_Profile_1_Name__c);
            StepNumber = 1;

        }
        return solProf;
    }
     public void linkSolutionProfile(Solution_Profiles__c SolProfile)
     {
        system.debug('Inside linkSolutionProfile');
        system.debug(' linkSolutionProfile() -1 accid:' + account.Id + '--Expertise id:' + ExpertiseId);
        Lookup = GetLookupObject();
        system.debug('Sol Profile ID:' + SolProfile.Id);
          if(StepNumber==1)
          {
                Lookup.Solution_Profile_1_Name__c=SolProfile.Id;
          }
          else if (StepNumber==2)
          {
                Lookup.Solution_Profile_2_Name__c=SolProfile.Id;
          }
          else if (StepNumber==3)
          {
                Lookup.Solution_Profile_3_Name__c=SolProfile.Id;
          }
          else if (StepNumber==4)
          {
                Lookup.Solution_Profile_4_Name__c=SolProfile.Id;
          }

     }   
     public String updateSolProfileStatus()
     {
        if(//!(SolProfile.Opportunity_Name__c ==null && ShowOpportunity) &&
            SolProfile.Account_Name__c != null &&
            SolProfile.Partner_Expertise_Area_ID__c != null &&
            //SolProfile.Solution_Owner__c != null &&
            String.isNotEmpty(SolProfile.Solution_Description__c)  &&
            String.isNotEmpty(SolProfile.Sector__c) &&
            String.isNotEmpty(SolProfile.Industry__c) &&
            //String.isNotEmpty(SolProfile.Function__c) &&
            //String.isNotEmpty(SolProfile.Department__c) &&
            //String.isNotEmpty(SolProfile.Solution_Area__c) &&
            //String.isNotEmpty(SolProfile.Deployment_Status__c) &&
            String.isNotEmpty(SolProfile.Business_Issues_Goals__c) &&
            String.isNotEmpty(SolProfile.IT_Issues_Goals__c) &&
            String.isNotEmpty(SolProfile.Business_Value_Qualitative__c) &&
            String.isNotEmpty(SolProfile.IT_Value_Qualitative__c) &&
            SolProfile.No_of_Deployed_Users__c != null &&
            SolProfile.No_of_Potential_Users__c != null &&
            String.isNotEmpty(SolProfile.Data_Source_Systems__c) &&
            String.isNotEmpty(SolProfile.Description_of_Data__c) &&
            String.isNotEmpty(SolProfile.Description_of_Users__c) &&
            SolProfile.Go_Live_Date__c != null &&
            SolProfile.Time_to_Develop_Deploy__c != null &&
            SolProfile.Data_Volume_Records__c !=null)
        {
            System.debug('Updating status to Ready for Review');
            SolProfile.Status__c = 'Ready for Review';
        }
        else
        {
            System.debug('Updating status to In process');
            SolProfile.Status__c = 'In Process';
        }
         return SolProfile.Status__c;
     }
     public PageReference saveSolProfile( String action) {
       try{
            //this.catchExceptions();
            system.debug(' saveSolProfile() -1 accid:' + account.Id + '--Expertise id:' + ExpertiseId);
            updateSolProfileStatus();
            upsert SolProfile;
            system.debug(' saveSolProfile() -2 accid:' + account.Id + '--Expertise id:' + ExpertiseId);
            system.debug('After SolProfile Save');
            linkSolutionProfile(SolProfile);
            upsert Lookup;
            PageReference next;
            SolProfileID=null;
            system.debug('action =' + action);
            system.debug('SolProfile Id :' + SolProfileID);
            if(action == 'Save')
            {
                next = new Pagereference('/apex/EP_SolutionProfilePage?acc=' + account.Id + '&expertId=' + ExpertiseId + '&stepNumber=' + StepNumber); 
            }
            else if (action == 'Next')
            {
                if(StepNumber == 4)
                {
                    next = new Pagereference('/apex/QlikExpertiseProgram');
                }
                else
                {
                    StepNumber=StepNumber+1;
                    next = new Pagereference('/apex/EP_SolutionProfilePage?acc=' + account.Id + '&expertId=' + ExpertiseId + '&stepNumber=' + StepNumber ); 
                }
            }
            system.debug(next.getUrl());
            //next = Page.ppqonnectprogrampage?SubPage=PEP;
            //next = Page.EP_SolProfile1;
            SolProfileID=null;
            system.debug('SolProfile Id :' + SolProfileID);
            next.setRedirect(true);
/*
            if(SolProfile.Status__c=='In Process')
            {
                callfunc='<script> ConfirmSave(); </script>'; 
                return null;
            }
            else
           */     
			{
                return next;  
            }
                      
        } catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
     }
     
    public PageReference SaveProfile()
    {
        return saveSolProfile('Save');

    }
    public PageReference NextPage() 
    {
        system.debug(' SaveProfile() accid:' + account.Id + '--Expertise id:' + ExpertiseId + 'action=Next');

        return saveSolProfile('Next');
        
    }
    public PageReference SaveAndRedirect()
    {
        if(!IsProfileLocked)
        {
            Pagereference p=saveSolProfile('Save');
        }
        PageReference redirect = new PageReference('/apex/EP_SolutionProfilePage'); 
        redirect.getParameters().put('acc',account.Id); 
        redirect.getParameters().put('expertId',ExpertiseId); 
        redirect.getParameters().put('stepNumber',String.valueOf(RedirectStepNumber) ); 
        redirect.setRedirect(true); 
        return redirect;
    }

    public string callfunc { get; set;}
    
    public PageReference Cancel() {
		System.debug('Call in cancel' + SolProfile.Description_of_Data__c + '     '+ OriginalSolProfile.Description_of_Data__c);
        if(!System.equals(OriginalSolProfile, SolProfile))
        {
            callfunc='<script> ConfirmCancel(); </script>'; 
            return null;
        }
        else return new Pagereference('/apex/QlikExpertiseProgram');        
    }    
}