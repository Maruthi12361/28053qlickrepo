/***************************************************
Class: LeadScoreHandler
Object: Lead
Description: CR# 20060 – Add Predictive Lead Score Grade and Overall Priority
Description: CR# 23836 – Lead/Contact Lifecycle-Clear FUR Fields
Description: CR# 19391 – Add in Counters to Calculate Days and Date Stamp Entry Point into Waterfall Stage
Description: CR# 30746 – New Predictive Lead Score Field and Revise Grade
Change Log:
 20150312   Madhav Kakani - Fluido Denmark
 20150325   Madhav Kakani - Fluido Denmark - Added after update trigger logic 
 20150427   CCE CR# 35752 - Add conditions to Clear FUR fields only if the new Lead Status is different from the old one
 20150513   CCE CR# 35713 - Add exception condition to Clear FUR fields for Follow-Up Rejected Reason
 20150713   CCE/Madhav Kakani - Fluido Denmark - Changed logic for CR# 30746
 20151021   CCE CR# 58149 - Adding extra status values to test
 20160421   CCE CR# 82611 - Replacing Lead Status value "Follow-Up Accepted" with new values "Follow-Up Attempt 1,2,3"
 20161020   CCE CR# 95899 - Update Predictive Lead Score Grade field based on 6sense Scores
 20170810   CCE CHG0030380 - clear manual re-assignment flag
 2017-10-25 AYS BMW-402 : Migrated from LeadScore_Trigger.trigger.
 20171212   CCE CHG0032730 - remove manual re-assignment flag
 20180328   CCE CHG0033445 - Set Conversica options field to Stop (case sensitive)
 20180611   CCE CHG0034173 add additional FU clearing statuses
 20181012   CCE CHG0034825 BMW-1054 Fix order of execution issue by bringing "Set FU Disqualified for Lattice Scoring" workflow into this code
 20181012   CCE CHG0034697 BMW-1015 change follow up disposition conditions
 20200224   CCE FUR Revamp 2020 All scoring has been removed and now only used for Lead disposition.
 20200401   CCE CHG0038108 (FUR Revamp 2020)  Due to SalesLoft implementation remove "Follow-Up Disqualified" automation
 20200409   CRW DGM-85 Salesloft go live. Commit 6adda93. Adding map and if for salesloft.
 20200526   CRW DGM-279    Removing semophore check when looking at the converted leads to increase coverage
 20200705   CRW DGM-373    Commenting Task query and adding no. of follow attempt filter for No response dispositioning
******************************************************/
public class LeadScoreHandler {
	public LeadScoreHandler() {
		
	}
	public static void handle(List<Lead> triggerNew, Map<Id,Lead> triggerOldMap, Boolean isAfter, Boolean isBefore) {
		system.debug('LeadScoreHandler: Entering');
	    if(isBefore) {
	        system.debug('LeadScoreHandler: before update');
            /*map<Id,String> mapTasks = new map<Id,String>();
            for(Task T:[Select id,Type,WhoId From Task Where WhoId IN : triggerOldMap.keySet() ORDER BY CreatedDate DESC LIMIT 1]){
                if(T.SalesLoft1__SalesLoft_Type__c == 'Call'){
                    mapTasks.put(T.WhoId,T.Type);
                }
            }*/
	        for(Lead l : triggerNew) {       
	            
	            Lead oldLd = triggerOldMap.get(l.Id);  //CCE CR# 35752
	            if((l.Status == 'Follow-Up Required' || l.Status == 'Follow-Up Attempt 1' || l.Status == 'Follow-Up Attempt 2' || l.Status == 'Follow-Up Attempt 3' || l.Status == 'Follow-Up Attempt 4' || l.Status == 'Follow-Up Attempt 5') && l.SalesLoft1__Most_Recent_Cadence_Next_Step_Due_Date__c == null && oldLd.SalesLoft1__Most_Recent_Cadence_Next_Step_Due_Date__c != null && l.Number_of_Follow_Up_Voicemails__c >= 1 && l.Number_of_Follow_Up_Emails__c >= 1){
                    l.Status = 'Follow-Up Disqualified';
                    l.Follow_Up_Disqualified_Reason__c = 'No Response';
                }
	            //if ((l.Status == 'Follow-Up Attempt 1' || l.Status == 'Follow-Up Attempt 2' || l.Status == 'Follow-Up Attempt 3' || l.Status == 'Follow-Up Attempt 4' || l.Status == 'Follow-Up Attempt 5') && 
	            	//l.Number_of_Follow_Up_Voicemails__c >= 6 && l.Number_of_Follow_Up_Emails__c >= 1) {
	            	//l.Status = 'Follow-Up Disqualified';
	            	//l.Follow_Up_Disqualified_Reason__c = 'No Response';
	            //}

	            // CR# 23836 – Lead/Contact Lifecycle-Clear FUR Fields
	            System.debug('LeadScore_Trigger: l.Status = ' + l.Status);
	            System.debug('LeadScore_Trigger: oldLd.Status = ' + oldLd.Status);
	            if (((l.Status == 'Follow-Up Rejected' && oldLd.Status != 'Follow-Up Rejected') && (!(l.Follow_Up_Rejected_Reason__c == 'Incorrect Routing' || l.Follow_Up_Rejected_Reason__c == 'Incomplete/Inaccurate Info'))) ||   //CR# 35713 
	                (l.Status == 'Follow-Up Disqualified' && oldLd.Status != 'Follow-Up Disqualified') ||
	                (l.Status == 'Goal Discovery' && oldLd.Status != 'Goal Discovery') ||
	                (l.Status == 'Archived' && oldLd.Status != 'Archived') ||
	                (l.Status == 'Contacted-Additional Work Required' && oldLd.Status != 'Contacted-Additional Work Required') ||
	                // Adding next 5 status tests for CR# 58149
	                (l.Status == 'Lead - In Qualification' && oldLd.Status != 'Lead - In Qualification') || 
	                (l.Status == 'Lead - Not Now' && oldLd.Status != 'Lead - Not Now') ||
	                (l.Status == 'Lead - Never' && oldLd.Status != 'Lead - Never') ||
	                (l.Status == 'Junk' && oldLd.Status != 'Junk') ||
	                (l.Status == 'Follow-Up Flagged in Error' && oldLd.Status != 'Follow-Up Flagged in Error') ||
	                (l.Status == 'Follow-Up Failed' && oldLd.Status != 'Follow-Up Failed') ||
	                (l.Status == 'Qualified - B' && oldLd.Status != 'Qualified - B'))
	            {
	                system.debug('LeadScore_Trigger: clearing Follow-Up flags');
	                if (l.New_Responder_Follow_Up_Required__c == true) {
	                    l.New_Responder_Follow_Up_Required__c = false;
	                    l.New_Responder_Follow_Up_Completed_Date__c = System.today();
	                }
	                if (l.Trial_Follow_Up_Required__c == true) {
	                    l.QSB_Trial__c = false;
	                    l.Trial_Follow_Up_Required__c = false;
	                    l.Trial_Follow_Up_Completed_Date__c = System.today();
	                }
	                if (l.Overall_Follow_up_Required__c == true) {
	                    l.Overall_Follow_up_Required__c = false;
	                    l.Overall_Follow_Up_Completed_Date__c = System.today();
	                }
	                //CHG0033445
	                if ((l.AVA__AVAAI_hot_lead__c == true) || (l.AVA__AVAAI_action_required__c == true)) {
	                	l.AVA__AVAAI_options__c = 'Stop'; //Conversica stop
	                }
	                //l.Manually_Re_assigned__c = false;
	            }
                
                
	            // CR# 19391 - Update AQL Start Date and Inquiry End Date if they are not already set
	            if(l.Status == 'Follow-Up Required') {
	                if(l.AQL_Start_Date__c == null) {
	                    l.AQL_Start_Date__c = System.today();
	                }
	            }
	            
	            if(l.Status == 'Follow-Up Required' || l.Status == 'Follow-Up Attempt 1' || l.Status == 'Follow-Up Attempt 2' || l.Status == 'Follow-Up Attempt 3' || l.Status == 'Follow-Up Rejected'
	                || l.Status == 'Follow-Up Disqualified' || l.Status == 'Contacted-Additional Work Required' || l.Status == 'Archived') {
	                if(l.Inquiry_End_Date__c == null) {
	                    l.Inquiry_End_Date__c = System.today();
	                }
	            }
	        }
	    }
	    else if(isAfter) { // handles lead conversions
	        // CR# 19391
	        Map<Id, Lead> mapLeads = new Map<Id, Lead>(); // map of converted contact id and lead record
	        for(Lead l : triggerNew) { 
	            if(l.IsConverted == true && l.ConvertedContactId != null) mapLeads.put(l.ConvertedContactId, l);
	        }
	        
	        if(!mapLeads.isEmpty()) { // there are some converted leads                   
	            List<Contact> lstC = [SELECT Id, Inquiry_to_AQL__c, Inquiry_to_AQL_Age__c, Inquiry_Start_Date__c, Inquiry_Start_Date_Current__c,
	                                Inquiry_End_Date__c, AQL_Start_Date__c, AQL_Start_Date_Current__c
	                                FROM Contact WHERE Id IN :mapLeads.keySet()];
	            
	            for(Integer i = 0; i < lstC.size(); i++) {
	                Lead l = mapLeads.get(lstC[i].Id);
	                if(l == null) continue;
	                lstC[i].Inquiry_Start_Date__c = l.Inquiry_Start_Date__c;
	                lstC[i].Inquiry_Start_Date_Current__c = l.Inquiry_Start_Date__c;
	                lstC[i].Inquiry_End_Date__c = l.Inquiry_End_Date__c;
	                lstC[i].AQL_Start_Date__c = l.AQL_Start_Date__c;
	                lstC[i].AQL_Start_Date_Current__c = l.AQL_Start_Date__c;
	                
	                if(l.Inquiry_End_Date__c != null) lstC[i].Inquiry_to_AQL__c = true;
	            }
	            
	            if(!lstC.isEmpty()) update lstC;
	        }
	    }
	    system.debug('LeadScoreHandler: Finishing');
	}
}