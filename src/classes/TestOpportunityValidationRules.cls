/**
 * 2014-03-12	TJG CR# 9027 https://eu1.salesforce.com/a0CD000000ZxGyw
 *				Verify that validation rules still work after the modifications
 * 2015-03-03   CCE - updated as validation rule (OP020) changed for CR 16194
 * 2020-04-20   Leonardo Grauer - OTQ 399 - Record types deactivation
 */
@isTest
private class TestOpportunityValidationRules {
    	static final String ApiOnlyUserProfileId = '00e20000000zFfpAAE';	//Custom: Api Only User
    	static final String OperationAdminProfId = '00e20000001ODnHAAW';	//Operations Administrator
    	static final String StandardUserProfieId = '00e20000000yyV1AAI';	// Standard User
    	//static final String OppRecordTypeId = '01220000000J1KR'; //Qlikbuy CCS Standard //Record type Qlikbuy CCS Standard has been deactivated - OTQ-399
		 static final String OppRecordTypeId = '01220000000DoEj'; 
    	//static final String OppRecordTypeId = '012f0000000CuglAAC'; //Qlikbuy CCS Standard II on QTNS
    	static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    	
    	
    static testMethod void competitorInfoTestAsOpUser(){
    QTCustomSettings__c setting = new QTCustomSettings__c(
    	Name = 'Default', 
//    	eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012f0000000Cugl',
    	OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
    Insert setting;
    QuoteTestHelper.createCustomSettings();
   	User apiUser = [select Id from user where ProfileId = :ApiOnlyUserProfileId and alias <> 'qwebs' and IsActive=true and UserRoleId <> null limit 1];
		Test.startTest();
    	User opeUser = [select Id from user where ProfileId = :OperationAdminProfId and IsActive=true limit 1];
        Account TestResellerAccount = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Valrules Test Ltd.',
                Sub_Reseller__c = false,
                Navision_Status__c = 'Partner',
                QlikTech_Company__c = 'QlikTech Inc'                
            );
        insert TestResellerAccount;
 
         Contact TestContact = new Contact (
                AccountId = TestResellerAccount.Id,
                OwnerId = UserInfo.getUserId(),
                LastName = 'Wilson-Hemingway',
                FirstName = 'Daryl',
                Email = 'dwy@qlikview.com'
            );
            
        insert TestContact;
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();      
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'United Kingdom',
            CurrencyIsoCode = 'GBP',
            Language__c = 'English',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(
            Name = 'Test Val Rule 1',
            RecordTypeId = AccRecordTypeId_EndUserAccount,
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Legal_Approval_Notes__c = 'Some legal stuff',
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
            
        Opportunity Opp = new Opportunity(
        	RecordTypeId = OppRecordTypeId,
        	Name = 'Test Val Rule Opp',
        	CurrencyIsoCode = 'USD',
        	Short_Description__c = 'Partner Reseller',
        	//pse__Is_Services_Opportunity__c = false,
        	//Competitors__c = null,
			
        	StageName = 'Champion',
        	CloseDate = Date.today().addDays(7),
        	Type = 'New Customer',
        	AccountId = acc.Id,
        	ForecastCategoryName = 'Omitted',
        	Sell_Through_Partner__c = TestResellerAccount.Id,
			Included_Products__c='Qlik Sense'
        );
        Boolean testSuccess = false;

		//'Competitor Information' must be filled out in Stage 'Champion' or above
		System.RunAs(opeUser){
			String msg1 = 'Competitor Information';
			String msg2find = 'must be filled out in Stage';
			try {		     
        		insert Opp;
			} catch(DmlException ex) {
				String msg = ex.getMessage();
				testSuccess = msg.contains(msg1) || msg.contains(msg2find);
				System.assert(testSuccess, msg);
			}
            /*if (!testSuccess)
            {
                System.assert(false, msg1 + ' ' + msg2find + 'Champion or above');
                return;
            }*/
            testSuccess = false;
			//Stage must be at Deal split Create, Deal Split Established, Closed Won or Closed Lost when using this record type
			//Opp.RecordTypeId = '01220000000Dmf5';	// Deal Pplit Parent
			Opp.ForecastCategoryName = 'Pipeline';
			/*msg2find = 'Stage must be at Deal split Create, Deal Split Established, Closed Won or Closed Lost when using this record type'; //Deal Split Parent deactivated - OTQ-399
			try {		     
        		update Opp;
			} catch(DmlException ex) {
				String msg = ex.getMessage();
				testSuccess = msg.contains(msg2find);
				System.assert(testSuccess, msg);
			}
            if (!testSuccess)
            {
                System.assert(false, msg2find);
                return;
            }*/
            testSuccess = false;
			//You must choose a Reason Lost if stage = Closed Lost or Goal Rejected
			Opp.RecordTypeId = OppRecordTypeId; 
			Opp.StageName = 'Closed Lost';
			Opp.Primary_reason_lost__c = null;
			msg2find = 'You must choose a Reason Lost if stage = Closed Lost';   //cce - updated as validation rule changed for CR 16194
            //msg2find = 'You must choose a Reason Lost if stage = Closed Lost or Goal Rejected';
            try {		     
        		update Opp;
			} catch(DmlException ex) {
				String msg = ex.getMessage();
				testSuccess = msg.contains(msg2find);
				System.assert(testSuccess, msg);
			}
            if (!testSuccess)
            {
                System.assert(false, msg2find);
                return;
            }
			/* testSuccess = false;

			//Must have a Billing and Shipping address on the account before setting Stage on an Opportunity to this level (SiB on Std Sales/Extension Sales; Present Contracts on Order Only; Quote on Exclusively Reseller; Prove Value on Customer Centric Selling)
			Opp.StageName = 'SiB';
			msg2find = 'Must have a Billing and Shipping address on the account before setting Stage';
            string msg2 = 'Must have a Billing and Shipping address on the account before setting Stage on an Opportunity to this level (SiB on Std Sales/Extension Sales; Present Contracts on Order Only; Quote on Exclusively Reseller; Prove Value on Customer Centric Selling)';
			try {		     
        		insert Opp;
			} catch(DmlException ex) {
				String msg = ex.getMessage();
                msg2 = msg;
				testSuccess = msg.contains(msg2find);
				System.assert(testSuccess, msg);
			}
            if (!testSuccess)
            {
                System.assert(false, msg2);
                return;
            }*/
            testSuccess = false;
			// The opportunity must have been approved before you can set to Closed Won. Set to a lower stage and then submit Opportunity for Approval.	
			/*		
			Opp.pse__Is_Services_Opportunity__c = false;
			Opp.Discount_from_List__c = 0.10;
			Opp.RecordTypeId = '01220000000DZil';
			Opp.Opportunity_Approved__c = false;
			Opp.StageName = 'Closed Won';
			try {		     
        		insert Opp;
			} catch(DmlException ex) {
				System.assert(ex.getMessage().contains('The opportunity must have been approved before you can set to Closed Won.'), ex.getMessage());				
			}			
			*/
			// quote status must be at order placed (set within QlikBuy) in order to set the opportunity to closed won
			//Opp.RecordTypeId = '01220000000J1KR'; //Qlikbuy CCS Standard
			Opp.RecordTypeId = OppRecordTypeId;
			Opp.StageName = 'Closed Won';
			Opp.Electronic_Acceptance__c = 'Yes';
			//Opp.Quote_Status__c <> 'Order Placed'
			
			
			acc.BillingStreet = '1020 Eskdale';
			acc.BillingCity = 'Reading';
			acc.BillingPostalCode = 'RG41 5TS';
			acc.BillingCountry = 'United Kingdom';
			acc.ShippingStreet = '1020 Eskdale';
			acc.ShippingCity = 'Reading';
			acc.ShippingPostalCode = 'RG41 5TS';
			acc.ShippingCountry = 'United Kingdom';
			
			/*
			acc.BillingStreet = '9 Temasek Boulevard';
			acc.BillingCity = 'Singapore';
			acc.BillingPostalCode = 'S038989';
			acc.BillingCountry = 'Singapore';
			acc.ShippingStreet = '9 Temasek Boulevard';
			acc.ShippingCity = 'Singapore';
			acc.ShippingPostalCode = 'S038989';
			acc.ShippingCountry = 'Singapore';
			*/

            //This validation rule has been changed, removing test scenario
            /*
			msg2find = 'quote status must be at order placed (set within QlikBuy) in order to set the opportunity to closed won';
			try {
				update acc;		     
        		insert Opp;
			} catch(DmlException ex) {
				String msg = ex.getMessage();
				testSuccess = msg.contains(msg2find);
				System.assert(testSuccess, msg);
			}
            if (!testSuccess)
            {
                System.assert(false, msg2find);
            }*/
		}
		Test.stopTest();    	
    }	

    	

}