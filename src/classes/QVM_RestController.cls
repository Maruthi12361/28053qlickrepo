@RestResource(urlMapping='/QVMREST/*')
global class QVM_RestController {
    
    static QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
    
    
    @HttpGet(urlMapping='/GetPartnerDataForProduct/*')
    global static RESTPartnerData getPartnerDataForProduct (RestRequest req, RestResponse res) {
        String productId = req.requestURI;
        productId = productId.replace('/QVMREST/GetPartnerDataForProduct/', '');
        
        List<QVM_Product_Data__c> products = [Select q.QVM_Partner__r.Support_Phone__c, 
          q.QVM_Partner__r.Support_Email__c, 
          q.QVM_Partner__r.Support_URL__c, 
          q.QVM_Partner__r.Partner_Website__c, 
          q.QVM_Partner__r.Partner_Display_Name__c, 
          q.QVM_Partner__r.Partner_Profile__c,
          q.QVM_Partner__r.Partner_Account__c,
          q.QVM_Partner__c From QVM_Product_Data__c q where Id = :productId];
          
        
        for (QVM_Product_Data__c product:products) { 
            return new RESTPartnerData(product.QVM_Partner__r); 
        }
        
        //If we didn't return a value then there isn't one
        return null;
    }
    
    global class RESTPartnerData { 
        
        String SupportUrl;
        String SupportEmail;
        String PartnerWebsite;
        String PartnerDisplayName;
        String SupportPhone;
        String PartnerProfile;
        String PartnerId;
        String PartnerLogoUrl;
        
        RESTPartnerData (QVM_Partner_Data__c partnerData) {
            SupportUrl = partnerData.Support_URL__c;
            PartnerWebsite = partnerData.Partner_Website__c;
            PartnerDisplayName = partnerData.Partner_Display_Name__c;
            SupportEmail = partnerData.Support_Email__c;
            PartnerProfile = partnerData.Partner_Profile__c;
            SupportPhone = partnerData.Support_Phone__c;
            PartnerId = partnerData.Id;
    
            PartnerLogoUrl = 'http://s3.amazonaws.com/'+settings.S3_Bucket__c+'/logos/'+partnerData.Partner_Account__c+'.gif';
        }
    }
    
    
    static testMethod void testRESTPartnerData () {
        Account partnerAccount = new Account(Name='Test Partner');
        insert partnerAccount;
        
        QVM_Partner_Data__c partner = new QVM_Partner_Data__c();
        partner.Support_Email__c = 'test@support.com';
        partner.Partner_Account__c=partnerAccount.Id;
        insert partner;
        
        QVM_Product_Data__c product = new QVM_Product_Data__c();
        product.QVM_Partner__c = partner.Id;
        insert product;
        
        RestRequest request = new RestRequest();
        request.requestURI='/QVMREST/GetPartnerDataForProduct/'+product.Id;
        
        QVM_RestController.getPartnerDataForProduct(request, null);
        
    }
    
}