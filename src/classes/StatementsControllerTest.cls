@isTest
public with sharing class StatementsControllerTest {
	/* ******************************************************   
    
        TEST METHODS
    
    ******************************************************* */  
    
    /*******************************
    * test_getLimitSOQL()
    * Does: Checks if getLimitSOQL returns the expected limit
    * 07.02.2017   RVA :   changing methods 
    ********************************/
    public static TestMethod void test_getLimitSOQL()
    {
    	Customer_Statement_Custom_Settings__c settings  = new Customer_Statement_Custom_Settings__c();
    	settings.Name = 'Default';
    	settings.Statement_Query_Limit__c = 9999;
    	settings.Email_List__c = 'AccountsReceivableNA@qlikview.com';
    	insert settings;

        StatementsController SC = new StatementsController();
        
        if (!SC.setLimit)
        {
            System.assert(SC.GetLimitSOQL()=='');
        }
        else
        {
            Customer_Statement_Custom_Settings__c MySetting = Customer_Statement_Custom_Settings__c.getValues('Default');
            if (MySetting == null)
            {
                System.assert(SC.GetLimitSOQL()=='');
            }
            else
            {
                System.Debug('Limits: ' +SC.GetLimitSOQL());
                System.assert(SC.GetLimitSOQL()==' Limit '+ MySetting.Statement_Query_Limit__c.intValue(), String.Format('Expected {0} and was {1}', new String[] {' Limit '+MySetting.Statement_Query_Limit__c.intValue(), SC.GetLimitSOQL()}));
            }
        }
                
    }
    
    public static TestMethod void testList_StatementsController()
    {
        
        Apexpages.currentPage().getParameters().put('AID', '');
        Apexpages.currentPage().getParameters().put('DDP', '');     
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		/*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'SWE',
            Country_Name__c = 'SWE',
            QlikTech_Company_Name__c = 'QlikTech Nordic AB',
			Subsidiary__c = testSubs1.id
			);
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'United States');
			QTComp.Country_Name__c = 'SWE';
		update QTComp;
        Account acc = new Account();
        acc.Name = 'Climber AB';
        acc.Navision_Customer_Number__c = 'Partner';
        acc.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
        insert acc;

        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Testersson';
        c.Email = 'a@b.com';
        c.AccountId = acc.Id;
        insert c;

        c = [select name, Account.Name from Contact where id = :c.id];
        system.debug('Contact.Name: ' + c.Name);
        system.debug('Contact.Account.Name: ' + c.Account.Name);

        acc.Billing_Contact__c = c.Id;
        update acc;

        StatementHeader__c header = new StatementHeader__c();
        header.Account__c = acc.Id;
        header.Emailed__c = false;
        insert header;

        StatementLines__c line = new StatementLines__c();
        line.StatementHeader__c = header.Id;
        line.Amount__c = 1000;
        insert line;

        header = [select Account__r.QlikTech_Company__c, Emailed__c from StatementHeader__c where id = :header.Id];
        system.debug('header.Account__r.QlikTech_Company__c: ' + header.Account__r.QlikTech_Company__c);
        system.debug('header.Account__r.Emailed__c: ' + header.Emailed__c);


        
        // We cannot create an account with a Navision_Customer_Number__c since it will generate an outbound
        // webservice call == terminates this test. 
        // 0012000000I7Txg == Climber AB
        //Account Acc = [select Id, Navision_Customer_Number__c, Name from Account where Id = '0012000000I7Txg' LIMIT 1];

        StatementHeader__c SH = new StatementHeader__c();
        SH.Account__c = Acc.Id;
        SH.PurchaseNo__c = 'ABC123';
        SH.Posting_Date__c = Date.today().addDays(-90);
        SH.EntryNo__c = 'Ent123';
        SH.Emailed__c = false;
        SH.Due_Date__c = Date.today().addDays(-60);
        SH.DocumentNo__c = 'Doc123';
        insert SH;
        
        List<StatementLines__c> SLs = new List<StatementLines__c>();
        StatementLines__c SL = new StatementLines__c();
        SL.StatementHeader__c = SH.Id;
        SL.EntryNo__c = 'Ent123';
        SL.DocumentNo__c = 'Doc123';
        SL.Amount__c = 200000;
        SLs.Add(SL);
        SL = new StatementLines__c();
        SL.StatementHeader__c = SH.Id;
        SL.EntryNo__c = 'Ent123';
        SL.DocumentNo__c = 'Doc123';
        SL.Amount__c = -145000;
        SLs.Add(SL);
        insert SLs;
            
        test.startTest();

        StatementsController SC = new StatementsController();
       
        // Test List Page
        List<StatementsController.StatementListItem> Items = SC.getStatementList();
        System.debug('testList_StatementsController Items.size(): ' + Items.size());
        //System.assert(Items.size() != 0);       

        // Test setters and getters
        System.assert(SC.getAID()== '');
        System.assert(SC.getDDP() == '');
        SC.setAID(Acc.Id);
        SC.setDDP('30');            
        System.assert(SC.getAID()== Acc.Id);
        System.assert(SC.getDDP() == '30');
        System.assert(SC.getUser().Id == UserInfo.getUserId());
        System.assert(SC.getUser().Id == UserInfo.getUserId());
        
        
        // Test Single Item Page
        Apexpages.currentPage().getParameters().put('AID', Acc.Id);
        Apexpages.currentPage().getParameters().put('DDP', '30');            
        SC = new StatementsController();           
        SC.DueDate = '30';
        SC.QTEntity = 'QlikTech Nordic AB';
        SC.Template = 'StatementPreview_1';
        StatementsController.StatementListItem ThisItem = SC.getItem();
        System.assert(ThisItem != null);
        System.assert(ThisItem.AccountId == Acc.Id);
        System.assert(ThisItem.AccountName == Acc.Name);
        System.assert(ThisItem.Selected == false);
        System.assert(ThisItem.BillingContactId != null);
        System.assert(ThisItem.BillingContactName != null);
        System.assert(ThisItem.BillingContactEmail != null);
        System.assert(ThisItem.getStatements().size() > 0);
        boolean CorrectStatementFound = false;
        for (StatementsController.StatementListSubItem SLSI : ThisItem.getStatements())
        {
            if (SLSI.StatementId != SH.Id) continue;
            CorrectStatementFound = true;           
            System.assert(SLSI.StatementId == SH.Id);
            System.assert(SLSI.InvoiceNbr == SH.DocumentNo__c);
            System.assert(SLSI.PostDate == SH.Posting_Date__c);
            System.assert(SLSI.DueDate == SH.Due_Date__c);
            System.assert(SLSI.Amount == 200000);
            System.assert(SLSI.RemainingAmount == 55000);
            System.assert(SLSI.PO == SH.PurchaseNo__c);
            break;
        }
        System.assert(CorrectStatementFound);

        // Test sending emails
        Items = SC.getStatementList();
        System.assert(Items.size() != 0);
        CorrectStatementFound = false;
        for (StatementsController.StatementListItem LI : Items)
        {
            if (LI.AccountId != Acc.Id) continue;
            ThisItem = LI;
            ThisItem.Selected = true;
            CorrectStatementFound = true;
            break;        
        }
        System.assert(CorrectStatementFound);
                
        System.assert(SC.SendEmails() == null);
        System.debug('SH.Id: ' + SH.Id);
        SH = [select Id, Account__c, PurchaseNo__c, Posting_Date__c, EntryNo__c, Emailed__c, Due_Date__c, DocumentNo__c from StatementHeader__c where Id = :SH.Id LIMIT 99];
        System.debug('SH: ' + SH);
        System.assert(SH.Emailed__c == true);        
        System.assert(SC.Click() == null);
        
        test.stopTest();
    }
}