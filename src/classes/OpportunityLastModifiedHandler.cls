/******************************************************

    Class: OpportunityLastModifiedHandler 
    
    Changelog:
        2016-06-02  Roman@4front      Migrated from lastModifiedUpdate.trigger.
        2016-06-07 	TJG		Fix test failures in OppUpdateContactSOITriggerTest, OpportunityLockTest
        			System.DmlException: Insert failed. First exception on row 0; first error: CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY, 
        			OpportunityTrigger: execution of AfterInsert
					caused by: System.NullPointerException: Argument cannot be null.
					Class.OpportunityLastModifiedHandler.handle: line 57, column 1
					Trigger.OpportunityTrigger: line 26, column 1: []
        2016-06-13	TJG		
        			Address the problems of 3 SOQL select statements inside for loop. Combine them into one
        			and put it outside the loop and optimised the code
		2017-06-22 MTM  QCW-2711 remove sharing option
		2017-07-17 ASLAM QCW-1697 Update Next Step and Comment from parent
		*11-1-2018 Shubham Added bypass rule true to pass validation error 
******************************************************/

public class OpportunityLastModifiedHandler {

	public static void handle(List<Opportunity> triggerNew, List<Opportunity> triggerOld, Boolean isInsert, Boolean isUpdate) {
		//List<Opportunity> ToParentUpdate = new List<Opportunity>();
		List<Opportunity> toUpdate = new List<Opportunity>();                   
		List<Id> allDsParents = new List<Id>();
		List<Id> allOppsIds = new List<Id>();
		Set<Id> parentsDone = new Set<Id>();
		Set<Id> childrenDone = new Set<Id>();
		Integer iSize = triggerNew.size();

		// is there anything really needs to be done?
		Boolean nothingToDo = true;
		for (integer i = 0; i < iSize && nothingToDo; i++)
		{
			Opportunity anOpp = triggerNew[i];
		    if(isUpdate && anOpp.Split_Opportunity__c == true && 
		       (anOpp.StageName.ContainsIgnoreCase('Closed Won') ||
		        (anOpp.Amount != triggerOld[i].Amount) ||
		        (triggerOld[i].Navision_Invoice_Number__c != anOpp.Navision_Invoice_Number__c) ))
		    {
		    	nothingToDo = false;
		    }

		    if(
		    	nothingToDo &&
		    	null != anOpp.Deal_Split_Parent_Opportunity__c && 
		    	((isUpdate && anOpp.Deal_Split_Percentage__c != triggerOld[i].Deal_Split_Percentage__c ) || isInsert) 
		       ) 
		    {
		    	nothingToDo = false;
		    }

		    //added for QCW-1697
		    if(isUpdate && anOpp.Split_Opportunity__c == true && 		       
		        ( triggerOld[i].Next_Step_custom__c != anOpp.Next_Step_custom__c || triggerOld[i].Manager_Comments__c != anOpp.Manager_Comments__c))
		    {
		    	nothingToDo = false;
		    }
		    //added for QCW-1697

		}
				

		if (nothingToDo)
		{
			// get out now!
			return;
		}

		for (integer k = 0; k < iSize; k++)
		{
			allOppsIds.add(triggerNew[k].Id);
			if (null != triggerNew[k].Deal_Split_Parent_Opportunity__c)
			{
				allDsParents.add(triggerNew[k].Deal_Split_Parent_Opportunity__c);
			}
		}


		List<Opportunity> lstAllOpps = 
		[
			SELECT
				Id, 
				Deal_Split_Percentage__c, 
				Deal_Split_Parent_Opportunity__c,
				Navision_Invoice_Number__c,				
				StageName,
				Next_Step_custom__c,  //added for QCW-1697
				Manager_Comments__c   //added for QCW-1697
			FROM
				Opportunity 
			WHERE
				Deal_Split_Parent_Opportunity__c IN :allOppsIds
				Or Deal_Split_Parent_Opportunity__c IN :allDsParents
				Or Id IN :allDsParents
		];

	    // create map of Id to Opportunity and populate it with all parents
	    Map<Id,Opportunity> parentMap = new Map<Id, Opportunity>();
		for (Opportunity signleOpp : lstAllOpps)
		{
			if (!parentMap.containsKey(signleOpp.Id))
			{
				for (Opportunity anotherOpp : lstAllOpps)
				{
					if (signleOpp.Id == anotherOpp.Deal_Split_Parent_Opportunity__c && !parentMap.containsKey(signleOpp.Id))
					{
						parentMap.put(signleOpp.Id, signleOpp);
					}
				}
			}
		}		   

		
		for (integer i = 0; i < iSize; i++)
		{
			Opportunity anOpp = triggerNew[i];
		    if(isUpdate && anOpp.Split_Opportunity__c == true && 
		       (anOpp.StageName.ContainsIgnoreCase('Closed Won') ||
		        (anOpp.Amount != triggerOld[i].Amount) ||
		        (triggerOld[i].Navision_Invoice_Number__c != anOpp.Navision_Invoice_Number__c) ||  
		        (triggerOld[i].Next_Step_custom__c != anOpp.Next_Step_custom__c) || //added for QCW-1697
		        (triggerOld[i].Manager_Comments__c != anOpp.Manager_Comments__c)    //added for QCW-1697
		         ))
		    {		        
		        for(Opportunity child : lstAllOpps)
		        {
		        	// filter out all children of anOpp
		        	if (!childrenDone.contains(child.Id) && child.Deal_Split_Parent_Opportunity__c == anOpp.Id)
		        	{
			            if (anOpp.StageName.ContainsIgnoreCase('OEM - Closed Won'))
			            {
			                child.StageName = anOpp.StageName;
			            }
			            else if (anOpp.StageName.ContainsIgnoreCase('Closed Won'))
			                child.StageName = 'Closed Won';
			            
			            if(String.IsNotBlank(anOpp.Navision_Invoice_Number__c))
			                child.Navision_Invoice_Number__c = anOpp.Navision_Invoice_Number__c;

						/* added as part of QCW-1697 */			                
						
						if(String.IsNotBlank(anOpp.Next_Step_custom__c))
			                child.Next_Step_custom__c = anOpp.Next_Step_custom__c;

						if(String.IsNotBlank(anOpp.Manager_Comments__c))
			                child.Manager_Comments__c = anOpp.Manager_Comments__c;			                

						/* added as part of QCW-1697 */
			            
			            toUpdate.Add(child);
			            childrenDone.add(child.Id);
			        }
		        }           
		    }

		    Id theParentId = anOpp.Deal_Split_Parent_Opportunity__c;
		    //Update parent's split percentage as cumulative split percentages of children
		    if( 
		    	((isUpdate && anOpp.Deal_Split_Percentage__c != triggerOld[i].Deal_Split_Percentage__c ) || isInsert) 
		    	&&
		       	theParentId != null
		       	&&
		       	!parentsDone.contains(theParentId)
		       ) 
		    {
		        List<Opportunity> siblings = new List<Opportunity>();
		        siblings.add(anOpp);
		        // add all the other siblings
		        for (Opportunity sibOpp : lstAllOpps)
		        {
		        	if (sibOpp.Id != anOpp.Id && sibOpp.Deal_Split_Parent_Opportunity__c == theParentId)
		        	{
		        		siblings.add(sibOpp);
		        	}
		        }
		        double splitPercent = 0;
		        System.debug('lastModifiedUpdate: Children count =' + siblings.size());
		        if (siblings.size() > 0)
		        {
			        for (Opportunity child : siblings)
			        {
			        	if (child.Deal_Split_Percentage__c > 0)
			        	{
			            	splitPercent += child.Deal_Split_Percentage__c;
			            }
			        }
		    	}
		        System.debug('OpportunityLastModifiedHandler: splitPercent' + splitPercent);

		        if(splitPercent > 0)
		        {
		            Opportunity parent = parentMap.get(theParentId);
		            if (null != parent)
		            {
			            parent.Deal_Split_Percentage__c = splitPercent;
			            System.Debug('Adding Parent Id = ' + parent.Id);
			            toUpdate.Add(parent);
			            parentsDone.add(theParentId);
			        }
		        }           
		    }            
		}     
		if(toUpdate.size() > 0)
		{
			System.debug('stage name updated '+ToUpdate[0].stageName);
			
			try {
		    	update(ToUpdate);
			}catch(Exception e) {
				
			}
		}
	}
}