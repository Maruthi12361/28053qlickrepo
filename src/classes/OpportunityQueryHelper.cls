/******************************************************

        OpportunityQueryHelper
    
        This class is helper to query Opportunity object and retrieve
        records by id/key. It will try to use cache to reduce number of queries.
    
        Changelog:
            2016-05-30  Roman@4front     Created class
    ******************************************************/

public with sharing class OpportunityQueryHelper {
	public OpportunityQueryHelper() {
		
	}

	private static Map<Id, Opportunity> oppMap;

	public static List<Opportunity> getOpportunitiesByIds(List<Id> oppIds, Boolean reset) {
		oppMap = null;
		return getOpportunitiesByIds(oppIds);
	}

	public static List<Opportunity> getOpportunitiesByIds(List<Id> oppIds) {
		List<Id> oppIdMissingInCache = new List<Id>();
		List<Opportunity> returnValues = new List<Opportunity>();
		if(oppMap == null) {
			oppMap = new Map<Id, Opportunity>();
		}
		for(Id id : oppIds) {
			if(!oppMap.containsKey(id)) {
				oppIdMissingInCache.add(id);
			}
		}
		if(!oppIdMissingInCache.isEmpty()) {
			List<Opportunity> opportunities = new List<Opportunity>([SELECT Id, Name, CurrencyIsoCode FROM Opportunity WHERE Id In :oppIdMissingInCache]);
			for(Opportunity opp : opportunities){
				oppMap.put(opp.Id, opp);
			}
		}
		// now we're retrieving data from the opportunities list
		for(Id id : oppIds) {
			returnValues.add(oppMap.get(id));
		}
		return returnValues;
	}

	public static Opportunity getOpportunityById(Id id) {
		List<Id> oppIds = new List<Id>();
		oppIds.add(id);
		List<Opportunity> returnOpportunities = getOpportunitiesByIds(oppIds);
		if(!returnOpportunities.isEmpty()) {
			return returnOpportunities[0];
		}
		else {
			return null;
		}
	}
}