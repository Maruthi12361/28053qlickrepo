/**     * File Name: PartnerFocusAreaHandlerTest
        * Description : This handler class is used to check the account Industries field on insert,update or delete of approved Partner Focus Area records
        * @author : Pramod Kumar
        * Modification Log ===============================================================
        * Jira Id : PAEX - 220
*/
@isTest(seealldata=false) 
public class PartnerFocusAreaHandlerTest{
    static testMethod void PartnerFocusAreaHandlerHandlerTest(){
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo= cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get('Partner Account').getRecordTypeId();
        Account acc=new Account(name ='test',recordtypeid = rtId );
        insert acc;
        
        Test.startTest();
        Partner_Focus_Area__c pfa=new Partner_Focus_Area__c(Partner_Account__c=acc.id,Sector__c='Financial Services',Industry__c='Banking',Functional_Area__c='Finance');
            insert pfa;
            System.assertEquals('Financial Services', pfa.Sector__c);
            System.assertNotEquals('test', pfa.Industry__c);
            System.assertNotEquals(Null, pfa.Functional_Area__c);
            pfa.Sector__c='Financial Service';
            pfa.Industry__c='Banking';
            update pfa;
        
            delete pfa;
            Partner_Focus_Area__c pfa1=new Partner_Focus_Area__c(Partner_Account__c=acc.id,Sector__c='Financial Service',Industry__c='Banking');
            pfa1.Industry__c='Banking';
            insert pfa1;
            pfa1.Sector__c='Financial Service';
            pfa1.Industry__c='Banking';
            update pfa1;
       Test.stopTest();
    }
}