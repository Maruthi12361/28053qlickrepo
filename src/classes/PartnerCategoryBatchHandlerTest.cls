/*
* File PartnerCategoryBatchHandlerTest
    * @description : Unit test for PartnerCategoryBatchHandler
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       18.12.2017   Pramod Kumar V         Created Class

*/
@isTest(seeAlldata=false)
private class PartnerCategoryBatchHandlerTest{
    
   
    static testMethod void test_PartnerCategoryStatus() {
       
         Id rtId2= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
         Id rtId3 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('OEM Partner Account').getRecordTypeId();
         
        
         Id rtId = Schema.SObjectType.Partner_Category_Status__c.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
         Id rtId1 = Schema.SObjectType.Partner_Category_Status__c.getRecordTypeInfosByName().get('OEM Partner Account').getRecordTypeId();
         
         Id rtId4 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
         RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
         //Id rtId3 = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('OEM Partner Account').getRecordTypeId();
         Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
         Subsidiary__c subs1 = QuoteTestHelper.createSubsidiary();
         QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
         
          Account testAccount = new Account(QlikTech_Company__c ='QTcomp', name='Test AccountName',recordtypeid=rtId2,BillingCountry='India',Payment_Terms__c = '45 days',Navision_Status__c = 'Partner');
          insert testAccount;
          
          Contact conPartner = QuoteTestHelper.createContact(testAccount.id);
          insert conPartner;
          
                  
          
          Account testAccount2 =new Account(name='Test AccountName2', recordtypeid=rtId3,QlikTech_Company__c='test',Territory_Country__c='India',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222');
          insert testAccount2;
          
          Account testAccount3 =new Account(name='Test AccountName2', recordtypeid=rtId4,QlikTech_Company__c='test',Territory_Country__c='India',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222');
          insert testAccount3;
          
          Account testAccount4 = new Account(QlikTech_Company__c ='QTcomp', name='Test AccountName',recordtypeid=rtId2,BillingCountry='India',Payment_Terms__c = '45 days',Navision_Status__c = 'Partner');
          insert testAccount4;
          Contact conPartner1 = QuoteTestHelper.createContact(testAccount4.id);
          insert conPartner1;
          
          Contact contEndUser = QuoteTestHelper.createContact(testAccount3.Id);
          insert contEndUser;
          
          SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
          Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
          oppCreatedForUpdate.CurrencyIsoCode = 'USD';
          //insert oppCreatedForUpdate;

        Partner_Category_Status__c pcs1 = new Partner_Category_Status__c(Partner_Account__c = testAccount.Id,
                                                                            recordtypeid=rtId,
                                                                            Program_Version__c = 'QPP',
                                                                            Partner_Level__c = 'Elite',
                                                                            Partner_Category__c = 'MSP',
                                                                            Partner_Level_Status__c ='Active');
    
        Partner_Category_Status__c pcs2 = new Partner_Category_Status__c(Partner_Account__c = testAccount.Id,recordtypeid=rtId,
                                                                            Program_Version__c = 'Master Reseller',
                                                                            Partner_Level__c = 'Master Reseller',
                                                                            Partner_Category__c = 'Master Reseller',
                                                                            Partner_Level_Status__c ='Active');
        Partner_Category_Status__c pcs3 = new Partner_Category_Status__c(Partner_Account__c = testAccount.Id,recordtypeid=rtId,
                                                                            Program_Version__c = 'Partner Program 4.1',
                                                                            Partner_Level__c = 'Solution Provider',
                                                                            Partner_Category__c = 'Solution Provider',
                                                                            Partner_Level_Status__c='Active');
        Partner_Category_Status__c pcs4 = new Partner_Category_Status__c(Partner_Account__c=testAccount2.Id ,recordtypeid=rtId1, Partner_Level__c= 'OEM' , Partner_Category__c='OEM' , Program_Version__c = 'OEM',Partner_Training_Discount__c=null,
                                                                            Partner_Level_Status__c='Active');
         Partner_Category_Status__c pcs5 = new Partner_Category_Status__c(Partner_Account__c = testAccount.Id,recordtypeid=rtId,
                                                                            Program_Version__c = 'Distributor',
                                                                            Partner_Level__c = 'Distributor',
                                                                            Partner_Category__c = 'Distributor',
                                                                            Partner_Level_Status__c='Active');
                                                                            
                                                                            Partner_Category_Status__c pcs6 = new Partner_Category_Status__c(Partner_Account__c = testAccount.Id,
                                                                            recordtypeid=rtId,
                                                                            Program_Version__c = 'QPP',
                                                                            Partner_Level__c = 'Elite',
                                                                            Partner_Category__c = 'Resell',
                                                                            Partner_Level_Status__c ='Active');
                                                                            
        List<Partner_Category_Status__c> listToInsert = new List<Partner_Category_Status__c>{pcs1, pcs2, pcs3,pcs4,pcs5,pcs6 }; 
        insert listToInsert; 
        
        
        SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote, conPartner1.Id, testAccount3, testAccount4.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote.Quote_Recipient__c = contEndUser.Id;
            quote.Sell_Through_Partner__c = testAccount4.Id;
            quote.PartnerCategoryStatusSecond__c =pcs3.id;
            insert quote;
            
            SBQQ__Quote__c quote1 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, testAccount3, testAccount.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote1.Quote_Recipient__c = contEndUser.Id;
            quote1.Second_Partner__c= testAccount.Id;
            quote1.PartnerCategoryStatusSecond__c =pcs2.id;
            insert quote1;
            
            SBQQ__Quote__c quote2 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, testAccount3, testAccount.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote2.Quote_Recipient__c = contEndUser.Id;
            quote2.Revenue_Type__c='OEM';
            quote2.Sell_Through_Partner__c = testAccount.Id;
            quote2.SBQQ__Account__c=testAccount3.Id;
            quote2.PartnerCategoryStatusSecond__c =pcs4.id;
            insert quote2;
            
            SBQQ__Quote__c quote3 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, testAccount3, testAccount.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote3.Quote_Recipient__c = contEndUser.Id;
            quote3.Second_Partner__c= testAccount.Id;
            quote3.Revenue_Type__c='Distributor';
            quote3.Sell_Through_Partner__c = testAccount.Id;
            quote3.PartnerCategoryStatusSecond__c =pcs5.id;
            insert quote3;
            
            SBQQ__Quote__c quote4 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, testAccount3, testAccount.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote4.Quote_Recipient__c = contEndUser.Id;
            quote4.Second_Partner__c= testAccount.Id;
            //quote4.Revenue_Type__c='Distributor';
            //quote4.Sell_Through_Partner__c = testAccount.Id;
            quote4.PartnerCategoryStatusSecond__c =pcs6.id;
            insert quote4;
            
            SBQQ__Quote__c quote5 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, testAccount3, testAccount.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote5.Quote_Recipient__c = contEndUser.Id;
            quote5.Second_Partner__c= testAccount4.Id;
            //quote4.Revenue_Type__c='Distributor';
            //quote4.Sell_Through_Partner__c = testAccount.Id;
            quote5.SBQQ__Account__c=testAccount3.Id;
            quote5.PartnerCategoryStatusSecond__c =pcs6.id;
            insert quote5;
            
            
          Account testAccount5 = new Account(QlikTech_Company__c ='QTcomp', name='Test AccountName',recordtypeid=rtId2,BillingCountry='India',Payment_Terms__c = '45 days',Navision_Status__c = 'Partner');
          insert testAccount5;
          Contact conPartner2 = QuoteTestHelper.createContact(testAccount5.id);
          insert conPartner2;
            
            SBQQ__Quote__c quote6 = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, testAccount3, testAccount.Id, 'Reseller', 'Open', 'Quote', false, oppCreatedForUpdate.Id);
            quote6.Quote_Recipient__c = contEndUser.Id;
            quote6.Second_Partner__c= testAccount5.Id;
            //quote4.Revenue_Type__c='Distributor';
            quote4.Sell_Through_Partner__c = testAccount.Id;
            quote6.SBQQ__Account__c=testAccount3.Id;
            quote6.PartnerCategoryStatusSecond__c =pcs2.id;
            insert quote6;

            
            
        List<SBQQ__Quote__c> listQuote=new  List<SBQQ__Quote__c>();
        listQuote.add(quote);
        listQuote.add(quote1);
        listQuote.add(quote2);
        listQuote.add(quote3);
        listQuote.add(quote4);
         listQuote.add(quote5);
         listQuote.add(quote6);
        
        PartnerCategoryBatchHandler.processBatch(listQuote);                                                     
          
          }
      }