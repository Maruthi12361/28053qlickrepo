/* 
 *  2016-1-27  
 *  To test the QS_emailMessageforchatterTrigger trigger
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 */
@isTest
private class QS_FeedItemFromMailTriggerhandlerTest {

    //private static final string ContactResponseReveived = 'Contact Response Received';

    //private static final string PendingContactResponse = 'Pending Contact Response';
    private static Id recTypeId;
    private static Id networkId;
    
    static testMethod void test_QS_FeedItemFromMail() {

        QTTestUtils.GlobalSetUp();

        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
     
        es.Standard_Entitlement_Process_Name__c = 'Basic Support SLA';
        es.Standard_Support_Level__c = 'Basic';
        es.Email_Message_Phrase_Communication__c = 'CUSTOMER COMMUNICATION';
        es.Email_Message_Phrase_First_Response__c = 'FIRST RESPONSE';
        es.Hold_Account_Name__c = 'UnMatched License Hold Account';
        es.Premium_Entitlement_Process_Name__c = 'Enterprise Support SLA';
        es.Premium_Support_Level__c = 'Enterprise';
        es.X24x7_Business_Hours_Name__c = '24x7';
        insert es;
        
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'leadSource';
        insert testContact;
        
        //Create Contact
        Contact testContact2 = TestDataFactory.createContact('test_FName', 'test_LName2', 'testSandbox2@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact2.Persona__c = 'Decision Maker';
        insert testContact2;
        
        //Create Contact
        Contact testContact3 = TestDataFactory.createContact('test_FName', 'test_LName3', 'testSandbox3@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact3.Persona__c = 'Decision Maker';
        insert testContact3;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        
        // Create Community User
        User communityUser2 = TestDataFactory.createUser(profileId, 'testSandbox2@qlikTech.com', 'tSbo2', String.valueOf(testContact2.Id));
        insert communityUser2;
        
        // Create Community User
        User communityUser3 = TestDataFactory.createUser(profileId, 'testSandbox3@qlikTech.com', 'tSbo3', String.valueOf(testContact3.Id));
        insert communityUser3;
        
        User supportProfileUser = [select Id, Name from User where Profile.Name LIKE 'Custom: Support%' AND IsActive = true limit 1]; 
        
        
        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        
        
        List<Case> caseObjList = new List<Case>();
        
            caseObjList.add(TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c));
       
       
        insert caseObjList;
        
        

        Test.startTest();
        
        case cc = [select Id from case limit 1] ;
        
        EmailMessage emess = New EmailMessage(FromAddress ='ext_num@qlikview.com', ToAddress= 'ext_num@qlikview.com', Subject = 'Testing Apex trigger',
                                              TextBody = 'this is the message of the email', ParentId = cc.Id);
        Insert emess;
       
        Test.stopTest();
    }
    
      // Get the networkId if not assigned to the user
    private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
                User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];  
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }
    
      // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
    
   
}