/********************************************************
* CLASS: SSOSAMLJitHandler_Test
* DESCRIPTION: This is the test class for SSOSAMLJitHandler.
* Code Coverage 82%
* CHANGELOG:
*   2020-06-22 - UIN - ADded logic for community check to prevent login to attunity without appropriate permissions.
*********************************************************/   
@isTest
public class SSOSAMLJitHandler_Test{
    
    //Test method for create user method.
   @isTest    
    public static void createusertest_Attunity() {
    User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) { 
        Test.StartTEst();  
        SSOSAMLJitHandler ssoHand = new SSOSAMLJitHandler();
        ssoHand.createUser(testuser.Id, (id)'00Q3z000019OQM4',(id)'00Q3z000019OQM4', (id)'00Q3z000019OQM4',  new Map<STring, STring>{'community'=>'attunity'}, 'zzz');
        Test.StopTEst();  
    }}
   
   //Test method for update user method for attunity portal where user has access
   @isTest    
    public static void updateusertest_Attunity() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) { 
        Test.StartTEst();  
        QuoteTestHelper.createCustomSettings();

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        Contact testContactExt = TestDataFactory.createContact('testContactExt', 'test_LName', 'testSandbox999@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContactExt.SFDCAccessGranted__c ='DI Support Portal';
        insert testContactExt;
        Profile p = [SELECT Id FROM Profile WHERE Name = 'PRM - Independent Territory + QlikBuy'];
        User u = new User(Alias = 'admtus', Email = 'testSandbox999@qlikTech.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US', ProfileId = p.Id, Country = 'Sweden', contactid = testContactExt.Id,
                TimeZonesIdKey = 'America/Los_Angeles', Username = 'admtus' + System.now().millisecond() + '@chemtest.com',
                UserPreferencesDisableAllFeedsEmail = false);
        insert u;
        SSOSAMLJitHandler ssoHand = new SSOSAMLJitHandler();
        ssoHand.updateUser(u.Id, (id)'00Q3z000019OQM4',(id)'00Q3z000019OQM4', (id)'00Q3z000019OQM4', 'aaa', new Map<STring, STring>{'community'=>'attunity'}, 'zzz');
        Test.StopTEst();  
        }
    
    }
    
    //Test method for update user method for non attunity portal where user has access
    @isTest    
    public static void updateusertest_nonAttunity() {
        User testUser = new User(Id = UserInfo.getUserId());
        System.runAs(testUser) {   
        Test.StartTEst();  
        QuoteTestHelper.createCustomSettings();

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        Contact testContactExt = TestDataFactory.createContact('testContactExt', 'test_LName', 'testSandbox999@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContactExt.SFDCAccessGranted__c ='LeadsOpps';
        insert testContactExt;
        Profile p = [SELECT Id FROM Profile WHERE Name = 'PRM - Independent Territory + QlikBuy'];
        User u = new User(Alias = 'admtus', Email = 'testSandbox999@qlikTech.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US', ProfileId = p.Id, Country = 'Sweden', contactid = testContactExt.Id,
                TimeZonesIdKey = 'America/Los_Angeles', Username = 'admtus' + System.now().millisecond() + '@chemtest.com',
                UserPreferencesDisableAllFeedsEmail = false);
        insert u;
        SSOSAMLJitHandler ssoHand = new SSOSAMLJitHandler();
        ssoHand.updateUser(u.Id, (id)'00Q3z000019OQM4',(id)'00Q3z000019OQM4', (id)'00Q3z000019OQM4', 'aaa', new Map<STring, STring>{'community'=>'support'}, 'zzz');
        Test.StopTEst();  
        }
    
    }
}