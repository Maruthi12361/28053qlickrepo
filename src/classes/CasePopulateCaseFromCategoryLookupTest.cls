/*
* Change log: 
*
* 2018-09-27    ext_vos CHG0034706: update test for default value for Case.Type, if there is no appropriate CategoryLookup
*/
@isTest
private class CasePopulateCaseFromCategoryLookupTest {
	
	static testMethod void testPopulateIncident() {
		Semaphores.SetAllSemaphoresToTrue();
		QTTestUtils.GlobalSetUp();

		Category_Lookup__c catLook = new Category_Lookup__c();
		catLook.Name = 'I have a setup and configuration issue';
		catLook.Category_Level__c = 'I have an issue building a custom authentication solution';
		catLook.Functional_Area__c = 'Integrations';
		catLook.Severity__c = '2';
		catLook.Case_Owner__c = 'Team Technical';
		catLook.Service_Request_Type__c = 'Test';
		catLook.Question_Prompt_1__c = 'Line 1';
		catLook.Question_Prompt_2__c = 'Line 2';
		catLook.Question_Prompt_3__c = 'Line 3';
		catLook.Question_Prompt_4__c = 'Line 4';
		catLook.Question_Prompt_5__c = 'Line 5';
		catLook.Record_Type_Name__c	= 'QlikTech Master Support Record Type';
		insert catLook;


		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Case caze = new Case();
        caze.AccountId = testAccount.Id;
        caze.ContactId = testContact.Id;
        caze.RecordTypeId = '01220000000DZqGAAW';
        caze.Area__c = 'I have a setup and configuration issue';
		caze.Issue__c = 'I have an issue building a custom authentication solution';
		Semaphores.SetAllSemaphoresToFalse();
		insert caze;

		caze = [select id, CaseNumber, Area__c, Issue__c, Functional_Area__c, Severity__c, Type, Service_Request_Type__c, Description from Case where Id = :caze.Id];
		System.assertEquals(caze.Area__c, catLook.Name);
		System.assertEquals(caze.Issue__c, catLook.Category_Level__c);
		System.assertEquals(caze.Functional_Area__c, catLook.Functional_Area__c);
		System.assertEquals(caze.Severity__c, catLook.Severity__c);
		System.assertEquals(caze.Type, 'Incident');
		System.assertEquals(caze.Service_Request_Type__c, catLook.Service_Request_Type__c);
		String description = CasePopulateCaseFromCategoryLookup.createDescriptionText(
																	new List<string>{catLook.Question_Prompt_1__c, catLook.Question_Prompt_2__c, 
																					catLook.Question_Prompt_3__c, catLook.Question_Prompt_4__c, catLook.Question_Prompt_5__c});
		System.assertEquals(caze.Description, description.substring(0,description.length()-2));
	}

	static testMethod void testNoRelatesAreaIssue() {
		Semaphores.SetAllSemaphoresToTrue();
		QTTestUtils.GlobalSetUp();

		Category_Lookup__c catLook = new Category_Lookup__c();
		catLook.Name = 'I have a setup and configuration issue';
		catLook.Category_Level__c = 'I have an issue building a custom authentication solution';
		catLook.Functional_Area__c = 'Integrations';
		catLook.Severity__c = '2';
		catLook.Case_Owner__c = 'Team Technical';
		catLook.Service_Request_Type__c = 'Test';
		catLook.Question_Prompt_1__c = 'Line 1';
		catLook.Question_Prompt_2__c = 'Line 2';
		catLook.Question_Prompt_3__c = 'Line 3';
		catLook.Question_Prompt_4__c = 'Line 4';
		catLook.Question_Prompt_5__c = 'Line 5';
		catLook.Record_Type_Name__c	= 'QlikTech Master Support Record Type';
		insert catLook;


		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Case caze = new Case();
        caze.AccountId = testAccount.Id;
        caze.ContactId = testContact.Id;
        caze.RecordTypeId = '01220000000DZqGAAW';
        caze.Area__c = 'Any area';
		caze.Issue__c = 'And issue which dont relate with CategoryLookup';
		Semaphores.SetAllSemaphoresToFalse();
		insert caze;
		
		caze = [select id, Type from Case where Id = :caze.Id];
		System.assertEquals('Incident', caze.Type);

		// case with empty Area/Issue
		Case cazeEmpty = new Case();
        cazeEmpty.AccountId = testAccount.Id;
        cazeEmpty.ContactId = testContact.Id;
        cazeEmpty.RecordTypeId = '01220000000DZqGAAW';
		Semaphores.SetAllSemaphoresToFalse();
		insert cazeEmpty;

		cazeEmpty = [select id, Type from Case where Id = :cazeEmpty.Id];
		System.assertEquals(null, cazeEmpty.Type);		
	}

	static testMethod void testPopulateServiceRequest() {
		Semaphores.SetAllSemaphoresToTrue();
		QTTestUtils.GlobalSetUp();

		Category_Lookup__c catLook = new Category_Lookup__c();
		catLook.Name = 'I have a setup and configuration issue';
		catLook.Category_Level__c = 'I have an issue building a custom authentication solution';
		catLook.Functional_Area__c = 'Integrations';
		catLook.Severity__c = '2';
		catLook.Case_Owner__c = 'Team Qoncierge';
		catLook.Service_Request_Type__c = 'Test';
		catLook.Question_Prompt_1__c = 'Line 1';
		catLook.Question_Prompt_2__c = 'Line 2';
		catLook.Question_Prompt_3__c = 'Line 3';
		catLook.Question_Prompt_4__c = 'Line 4';
		catLook.Question_Prompt_5__c = 'Line 5';
		catLook.Record_Type_Name__c	= 'QlikTech Master Support Record Type';
		insert catLook;


		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Case caze = new Case();
        caze.AccountId = testAccount.Id;
        caze.ContactId = testContact.Id;
        caze.RecordTypeId = '01220000000DZqGAAW';

        caze.Area__c = 'I have a setup and configuration issue';
		caze.Issue__c = 'I have an issue building a custom authentication solution';
		Semaphores.SetAllSemaphoresToFalse();
		insert caze;

		caze = [select id, RecordType.Name, Area__c, Issue__c, Functional_Area__c, Severity__c, Type, Service_Request_Type__c, Description from Case where Id = :caze.Id];

		System.assertEquals(caze.Area__c, catLook.Name);
		System.assertEquals(caze.Issue__c, catLook.Category_Level__c);
		System.assertEquals(caze.Functional_Area__c, catLook.Functional_Area__c);
		System.assertEquals(caze.Severity__c, catLook.Severity__c);
		System.assertEquals(caze.Type, 'Service Request');
		System.assertEquals(caze.Service_Request_Type__c, catLook.Service_Request_Type__c);
		String description = CasePopulateCaseFromCategoryLookup.createDescriptionText(
																	new List<string>{catLook.Question_Prompt_1__c, catLook.Question_Prompt_2__c, 
																					catLook.Question_Prompt_3__c, catLook.Question_Prompt_4__c, catLook.Question_Prompt_5__c});
		System.assertEquals(caze.Description, description.substring(0, description.length()-2));
	}
}