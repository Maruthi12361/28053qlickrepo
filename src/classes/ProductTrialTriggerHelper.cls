//-----------------------------------------------------------------------------------------------------------
// Created by Leonardo Grauer - 16/03/20
//
// A helper class for ProductTrial.helper
//-----------------------------------------------------------------------------------------------------------
public class ProductTrialTriggerHelper  {

	public static void updateAccountERPStatus(List<Product_Trial__c> triggernew){
		
		List<Id> accountsIDs = new List<Id>();
		for(Product_Trial__c p: triggernew){
			if(p.Trial_License_Key__c != null) // checking the record was created as a result of the EVAL app (we could choose a few other fields as well)
				accountsIDs.add(p.Contact_Account__c);
		}//for

		List<Account> accountsToUpdate = [SELECT Id, Navision_Status__c FROM Account WHERE Navision_Status__c = null AND Id IN :accountsIDs];

		for(Account acc: accountsToUpdate){
			acc.Navision_Status__c = 'Trial License';
		}
		if(accountsToUpdate.size()>0)
			update accountsToUpdate;
	}// function updateAccountERPStatus
	
}//class