/**
 *	14-06-2017	Created by Oleksandr@4Front for QCW-2103.
 */
@isTest
public class SBQQQuoteUncheckLegalApprovedTest {
    public static testMethod void uncheckingTestSingleNegative() {

        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        insert subs;

        QlikTech_Company__c QTc = QuoteTestHelper.createQlickTechCompany(subs.Id);
        insert QTc;

        RecordType recTypeAccEndUser = QuoteTestHelper.getRecordTypebyDevName('End_User_Account');
        RecordType recTypeAccPartner = QuoteTestHelper.getRecordTypebyDevName('Partner_Account');
        RecordType recTypeOpp = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
        RecordType recTypeQuote = QuoteTestHelper.getRecordTypebyDevName('Quote');

        Account accEndUser = QuoteTestHelper.createAccount(QTc, recTypeAccEndUser, 'test2103');
        accEndUser.Local_Country__c = 'France';
        accEndUser.BillingCountry = 'France';
        insert accEndUser;

        Account accPartner = QuoteTestHelper.createAccount(QTc, recTypeAccPartner, 'test2103');
        accPartner.Local_Country__c = 'France';
        accEndUser.ShippingCountry = 'France';
        insert accPartner;

        Contact con = QuoteTestHelper.createContact(accEndUser.Id);
        insert con;

        Address__c addrShip = QuoteTestHelper.createAddress(accEndUser.Id, con.Id, 'Shipping');
        addrShip.Country__c = accEndUser.ShippingCountry;
        addrShip.Valid_Address__c = true;
        insert addrShip;

        Address__c addrBill = QuoteTestHelper.createAddress(accEndUser.Id, con.Id, 'Billing');
        addrBill.Country__c = accEndUser.BillingCountry;
        addrBill.Valid_Address__c = true;
        insert addrBill;

        QuoteTestHelper.createCustomSettings();

        SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote,con.Id, accEndUser, '', 'Direct', 'Open', 'Quote', false, '');
        quote.Shipping_Address__c = addrShip.Id;
        quote.Select_Billing_Address__c = addrBill.Id;
        insert quote;

        System.debug('SD quote: ' + quote.Id);
        System.debug('SD ship addr: ' + quote.Shipping_Address__c);
        System.debug('SD bill addr: ' + quote.Select_Billing_Address__c);
        System.debug('SD Legal approval after ' + quote.Legal_Approval_Triggered__c);

        SBQQ__Quote__c quoteAssert = [
                SELECT Id, Legal_Approval_Triggered__c
                FROM SBQQ__Quote__c
        ];

        System.assertEquals(quoteAssert.Legal_Approval_Triggered__c, true);
    }

    public static testMethod void uncheckingTestSinglePositive() {

        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        insert subs;

        QlikTech_Company__c QTc = QuoteTestHelper.createQlickTechCompany(subs.Id);
        insert QTc;

        RecordType recTypeAccEndUser = QuoteTestHelper.getRecordTypebyDevName('End_User_Account');
        RecordType recTypeAccPartner = QuoteTestHelper.getRecordTypebyDevName('Partner_Account');
        RecordType recTypeOpp = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
        RecordType recTypeQuote = QuoteTestHelper.getRecordTypebyDevName('Quote');

        Account accEndUser = QuoteTestHelper.createAccount(QTc, recTypeAccEndUser, 'test2103');
        accEndUser.Local_Country__c = 'France';
        accEndUser.BillingCountry = 'France';
        insert accEndUser;

        Account accPartner = QuoteTestHelper.createAccount(QTc, recTypeAccPartner, 'test2103');
        accPartner.Local_Country__c = 'France';
        accEndUser.ShippingCountry = 'France';
        insert accPartner;

        Contact con = QuoteTestHelper.createContact(accEndUser.Id);
        insert con;

        Address__c addrShip = QuoteTestHelper.createAddress(accEndUser.Id, con.Id, 'Shipping');
        addrShip.Country__c = accEndUser.ShippingCountry;
        addrShip.Valid_Address__c = true;
        insert addrShip;

        Address__c addrBill = QuoteTestHelper.createAddress(accEndUser.Id, con.Id, 'Billing');
        addrBill.Country__c = accEndUser.BillingCountry;
        addrBill.Valid_Address__c = true;
        insert addrBill;

        Test.startTest();

        QuoteTestHelper.createCustomSettings();

        SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote,con.Id, accEndUser, '', 'Direct', 'Open', 'Quote', false, '');
        quote.Shipping_Address__c = addrShip.Id;
        quote.Select_Billing_Address__c = addrBill.Id;
        insert quote;

        Address__c addrShipNew = QuoteTestHelper.createAddress(accEndUser.Id, con.Id, 'Shipping');
        addrShipNew.Zip__c = '75003';
        addrShipNew.Country__c = accEndUser.ShippingCountry;
        addrShipNew.Valid_Address__c = true;
        addrShipNew.Default_Address__c = false;
        insert addrShipNew;

        quote.Shipping_Address__c = addrShipNew.Id;
        update quote;

        System.debug('SD quote: ' + quote.Id);
        System.debug('SD ship addr: ' + quote.Shipping_Address__c);
        System.debug('SD bill addr: ' + quote.Select_Billing_Address__c);
        System.debug('SD Legal approval after ' + quote.Legal_Approval_Triggered__c);

        SBQQ__Quote__c quoteAssert = [
                SELECT Id, Legal_Approval_Triggered__c
                FROM SBQQ__Quote__c
        ];

        Test.stopTest();

        System.assertEquals(quoteAssert.Legal_Approval_Triggered__c, false);
    }
}