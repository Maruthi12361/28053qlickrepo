/**
 20130703	TJG Case 00186262 https://eu1.salesforce.com/500D000000QnpS3
 				Future methods to update probable accounts of leads especially after account merging
 20150126	CCE CR# 16699 https://eu1.salesforce.com/a0CD000000ksoid Add Key Account field			
 20150422	CCE CR# 22167 https://eu1.salesforce.com/a0CD000000pcfqd Add Navision Type for Mkto field
 */

global class LeadProbableAccountUpdate {
	static final String DEBUGPREFIX = '--FUFU--';
	@future
	public static void UpdateLeadFields(){
		List<Lead> leads = [Select Segment__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account_Lead__c, Company,
				Annual_Turnover__c, Site_Employee_Range__c, Owner_of_the_Linked_Account__c, Probable_Account__c,
				Link_to_Account_ID__c, Key_Account__c, Navision_Type_for_Mkto__c from lead where Probable_Account__c != null and IsConverted != true and LastModifiedDate = TODAY];
		
		if (0 == leads.size())
			return;
			
		Set<String> accIds = new Set<String>();
		
		for (Lead alead :leads){
			System.Debug(DEBUGPREFIX + ' Probable_Account__c = ' + alead.Probable_Account__c);
			System.Debug(DEBUGPREFIX + ' Link_to_Account_ID__c = ' + alead.Link_to_Account_ID__c);
			if (null != alead.Probable_Account__c && alead.Link_to_Account_ID__c != alead.Probable_Account__c){
				accIds.Add(alead.Probable_Account__c);
			}
		}		
		System.Debug(DEBUGPREFIX + ' accIds.size() = ' + accIds.size());
		if (accIds.IsEmpty()){
			return;
		}
			
		List<Lead> l2update = new List<Lead>();
		
		System.Debug(DEBUGPREFIX + ' accIds = ' + accIds);
	    Map<ID, Account> IdToProbAcc = new Map<ID, Account>([Select Id, Segment_New__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account__c, Name, Annual_Turnover__c, Site_Employee_Range__c, OwnerId, Key_Account__c, Navision_Status__c From Account where Id in : accIds]);
		System.Debug(DEBUGPREFIX + ' IdToProbAcc = ' + IdToProbAcc);
	    for(Lead lead : leads) {
	    	Id probAccId = lead.Probable_Account__c;
	    	System.Debug(DEBUGPREFIX + ' probAccId = ' + probAccId);
	        if (probAccId != null) {
	            Account probAcc = IdToProbAcc.Get(probAccId);
	    		System.Debug(DEBUGPREFIX + ' probAcc = ' + probAcc);        
	            if (probAcc != null) {
	                if ( lead.Segment__c != probAcc.Segment_New__c ||
	                	lead.SIC_Code_Name__c != probAcc.SIC_Code_Name__c ||
	                	lead.No_of_Employees__c != probAcc.No_of_Employees__c ||
	                	lead.Named_Account_Lead__c != probAcc.Named_Account__c ||
	                	lead.Company != probAcc.Name ||
	                	lead.Annual_Turnover__c != probAcc.Annual_Turnover__c ||
	                	lead.Site_Employee_Range__c != probAcc.Site_Employee_Range__c ||
	                	lead.Owner_of_the_Linked_Account__c != probAcc.OwnerId ||
	                	lead.Link_to_Account_ID__c != probAcc.Id ||
	                	lead.Key_Account__c != probAcc.Key_Account__c ||
	                	lead.Navision_Type_for_Mkto__c != probAcc.Navision_Status__c) {
	                		lead.Segment__c = probAcc.Segment_New__c;
		                    lead.SIC_Code_Name__c = probAcc.SIC_Code_Name__c;
		                    lead.No_of_Employees__c = probAcc.No_of_Employees__c;
		                    lead.Named_Account_Lead__c = probAcc.Named_Account__c;
		                    lead.Company = probAcc.Name;
		                    lead.Annual_Turnover__c = probAcc.Annual_Turnover__c;
		                    lead.Site_Employee_Range__c = probAcc.Site_Employee_Range__c;
		                    lead.Owner_of_the_Linked_Account__c = probAcc.OwnerId;
		                    lead.Link_to_Account_ID__c = probAcc.Id;
	                		lead.Key_Account__c = probAcc.Key_Account__c;
	                		lead.Navision_Type_for_Mkto__c = probAcc.Navision_Status__c;
	                		l2update.Add(lead);
	                	}
	            }
	        }
	    }
        
        if (0 < l2update.size()){
        	Update(l2update);
        }
        
    	System.Debug(DEBUGPREFIX + '---Ending UpdateLeadFields---');
	}
}