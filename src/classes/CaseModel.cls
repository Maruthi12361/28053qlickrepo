/*****************************************************
*Log History
*2011-09-27  RDZ     CR# 2334 - https://eu1.salesforce.com/a0CD000000EVGho
*2016-02-19  NAD     Removed Support_Level__c and Support_Per__c references per CR# 33068 (Responsible Partner change)
******************************************************/


public class CaseModel 
{
	//Constructor
    public CaseModel(Case c)
    {
        record = c; 
    }
    
    //Methods
    //Gets Case Comments for Internal T-View
    public List<CaseComment> getCaseCommentsForInternalTView()
    {
        return [SELECT CommentBody,
                       LastModifiedDate,
                       CreatedBy.Name
                  FROM CaseComment
                 WHERE ParentId =: record.Id];                                              
    }
    
    //Gets Case Comments for Customer T-View
    public List<CaseComment> getCaseCommentsForCustomerTView()
    {
        return [SELECT CommentBody,
		               LastModifiedDate,
		               CreatedBy.Name
		          FROM CaseComment
		         WHERE ParentId =: record.Id
		           AND IsPublished = true];                                              
    } 
    
    //Gets Case Activity History for Internal T-View
    public List<ActivityHistory> getActivityHistoryForInternalTView()
    {   
        Case activityHistoryForCase = [SELECT Id,
                                              (SELECT Subject,
                                                      ActivityDate, 
                                                      Owner.Name
                                                 FROM ActivityHistories)
                                         FROM Case
                                        WHERE Id = :record.Id];
        return activityHistoryForCase.ActivityHistories;                                               
    }
    
    //Gets Case Activity History for Customer T-View
    public List<ActivityHistory> getActivityHistoryForCustomerTView()
    {
        Case custTViewActivityHistForCase = [SELECT Id,
                                                    (SELECT Subject,
                                                            ActivityDate, 
                                                           	Owner.Name
                                                       FROM ActivityHistories
                                                      WHERE WhoId =: record.ContactId)
                                               FROM Case
                                              WHERE Id = :record.Id];
        return custTViewActivityHistForCase.ActivityHistories;                                                          
    }
    
    //Get Case Open Activities for T-View
    public List<OpenActivity> getOpenActivityForTView()
    {
        Case openActivityForCase = [SELECT Id,
                                           (SELECT ActivityDate,
                                                   Subject,
                                                   Owner.Name
                                              FROM OpenActivities)
                                      FROM Case
                                     WHERE Id = :record.Id];
        return openActivityForCase.OpenActivities;                                            
    }
    
    //Get Case Attachments for T-View
    public List<Attachment> getAttachmentsForTView()
    {
        return [SELECT Name,
					   LastModifiedDate,
					   LastModifiedBy.Name
                  FROM Attachment
                 WHERE ParentId = :record.Id];
    }    
    
    //Get information for the Account related to the Case
	public Account getAccountRelatedToCase()
	{
		return [SELECT Id, 
					   Name, 
					   Account_Prioritisation__c,  
					   Credit_Status__c,
					   QlikTech_Company__c,
					   By_Pass_st_Line_Support__c,
					   Responsible_Partner_count__c,
					   Red_Account__c,
					   Maintenance_Type__c
				FROM Account 
				WHERE Id = :record.AccountId LIMIT 1];
	}
	
	//Get information for the Account Licenses related to the Account
	public List<Account_License__c> getAccountLicensesRelatedToAccount()
	{
		return [SELECT Id, 
					   Name, 
					   Application__c, 
					   Version__c, 
					   Product_Level__c, 
					   Support_From__c, 
					   Support_To__c,
					   O_S__c,
					   Environment__c,
					   Product__c,
					   Area__c,
					   Issue__c,
					   X3rd_Party_Software__c,
					   Client_Version__c,
					   Server_Version__c,
					   Publisher_Version__c,
					   Connector_Version__c,
					   Customer_Patch_Version__c,
					   Product_Type__c,
					   Selling_Partner__r.Name
				FROM Account_License__c
				WHERE Account__c = :record.AccountId
				ORDER BY Name];
	}
	
	
    //Properties
    public Case record
    {
        get
        {
            if(record == null)
                record = new Case();
            return record;
        }
        set;
    }
    public Boolean hasValidId
    {
    	get
    	{
	        if(record.Id == null)
	            return false;
	        else
	            return true;
    	}
    }
    
    public Boolean userHasPermission {
    	
    	get
    	{
    		User oUser = [Select id, Access_Create_Problem_Case__c FROM User where id =: userinfo.getUserId() limit 1];
    	
    		return oUser.Access_Create_Problem_Case__c;
    	}
    	
    }
}