@isTest
private class ContactPortalUserDeactivateTest {
    
    static testMethod void contactPortalUserDeactivateTest() {
        //dummy test, it is very difficult to create a portal user
        Contact c = new Contact(LastName = 'TestLastName');
        insert c;
        
        ApexPages.Standardcontroller sc1 = new ApexPages.StandardController(c);
        ContactPortalUserDeactivate controller = new ContactPortalUserDeactivate(sc1);      
        controller.deactivatePortalUser();      
    }
    
    static testMethod void userDeactivateTest() {  
        Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());     
        User user = TestDataUtils.createUser('UsernameTest123@test.com', 'NickTest123');
        //User user = QTTestUtils.createMockOperationsAdministrator();
                
        ContactPortalUserDeactivate.deactivateUser(user);
        
        user = [SELECT IsActive,IsPortalEnabled  FROM User WHERE Id = :user.Id];
        System.assert(!user.IsActive);
        System.assert(!user.IsPortalEnabled);        
    }    
    
}