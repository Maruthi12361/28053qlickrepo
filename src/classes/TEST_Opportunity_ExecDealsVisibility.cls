/****************************************************** 

Class: TEST_Opportunity_ExecDealsVisibility 

This class tests the Opportunity_ExecDealsVisibility trigger 

Changelog: 
2012-10-19 SAN testing of filling data in field License_Amount_USD__c and Competitor_s__c 
2013-11-05 CCE/KMH Modified test to support changes to trigger for CR# 8968  
2013-11-27 BTN Added Influencing Partner Test (Copied from the Competitor test method) for CR# 9079. 
            Code reviewed by CCE and also KMH
2014-02-07  MTM     CR# 10453 - Fix to Opportunity_ExecDealsVisibility trigger USD conversion fix
2014-06-18  MHG Fixed broken test cases due to new forecasting functionallity
2014-09-02  RDZ Fixed broken test case due to change on name for influencing partner. James Jury Account Name.
                TODO change select that gets James Jury contact on sfdc and create acc type partner, and a contact for that
                account.
2014-11-03  TJG     CR# 17283 https://eu1.salesforce.com/a0CD000000lLIy7
                    Proper reporting based on USD
2015-04-20  SLH Added functin and solution area for OP006
2016-06-08  CCE Andrew Lokotosh Comented Forecast Amount fields line 43, 99,113
2020-07-09  EXTCDX  Adding Deployment_Preference__c = 'On-Premise', to comply OP063_Deployment_and_Cloud_Provider validation rule
				"Deployment Preference" is required in Goal Confirmed stage 
******************************************************/ 

@isTest
private class TEST_Opportunity_ExecDealsVisibility {

    public static testMethod void Amount_Null_Test() 
    {
        QTTestUtils.GlobalSetUp();

        List<CurrencyType> currencyRateList = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and ISOCode='GBP'];
        double usdAmount = 0.0;
        
        if(!currencyRateList.isEmpty())
        {
            User user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
            Account acc = QTTestUtils.createMockAccount('My own account', user);
            Opportunity testOpp = new Opportunity(
            Name = 'Test Opp 1',
			Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            StageName = 'Goal Confirmed',
            CloseDate = Date.today(),
            AccountId = acc.Id,
            CurrencyISOCode = 'GBP',    
          //  Total_Maintenance_Amount__c = 100.0,
            Function__c = 'Other',
            Solution_Area__c = 'Other',
            Signature_Type__c = 'Digital Signature',
            Included_Products__c = 'Qlik Sense',
            ForecastCategoryName = 'Omitted'    //Added to exclude from Opportunity_ManageForecastProducts.trigger
            );
            insert testOpp; 
            
            test.startTest();
            Opportunity oppty1 = [Select o.Total_Training_Amount__c, o.License_Amount_USD__c, o.Grand_Total__c, o.Grand_Total_USD__c, o.Amount From Opportunity o where id =: testOpp.id];
           // System.assertEquals(null, oppty1.Amount);            
            System.debug('ISOCode = ' + currencyRateList[0].ISOCode);
            System.debug('Grand total = ' + oppty1.Grand_Total__c);
            System.debug('ConversionRate = ' + currencyRateList[0].ConversionRate);
            System.debug('Grand_Total_USD__c = ' + oppty1.Grand_Total_USD__c);
            
            usdAmount = oppty1.Grand_Total__c/currencyRateList[0].ConversionRate;
            
            System.debug('usdAmount = ' + usdAmount);
            //System.assertEquals(null, oppty1.Amount);
            //System.assertEquals(oppty1.Total_Maintenance_Amount__c, oppty1.Grand_Total__c);
            //System.assertEquals(usdAmount, oppty1.Grand_Total_USD__c, 'conversion from GBP to USD failed');            
            test.stopTest();
        }   
    }
    
    public static testMethod void License_Amount_USD_Test() {
        System.debug('License_Amount_USD_Test: Starting');
        QTTestUtils.GlobalSetUp();

        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
        Semaphores.Opportunity_ManageForecastProductsIsInsert = true;    

        
        List<CurrencyType> c = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
        double ConvertedAmount = 0.0;
   
        test.startTest();
        
        // Adding an account with BillingCountryCode and Qliktech Company
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
        Name = 'GBR',
        QlikTech_Company_Name__c = 'QlikTech UK Ltd',
		Subsidiary__c = testSubs1.id      
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

            Opportunity testOpp = new Opportunity(
        Name = 'Test Opp 1',
		Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        StageName = 'Goal Confirmed',
        CloseDate = Date.today(),
        AccountId = acc.Id,
        CurrencyISOCode = 'GBP',
    //    License_Forecast_Amount__c = 100.0,    
        Amount = 200.0,
        Function__c = 'Other',
        Solution_Area__c = 'Other',
        Signature_Type__c = 'Digital Signature',
        Included_Products__c = 'Qlik Sense',
        ForecastCategoryName = 'Omitted'    //Added to exclude from Opportunity_ManageForecastProducts.trigger
        );
        insert testOpp;
        
         for(integer i = 0; i< c.size(); i++)
            { 
                if(testOpp.CurrencyISOCode == c[i].ISOCode)
                {  
                   // ConvertedAmount = testOpp.License_Forecast_Amount__c = 100.0 / c[i].ConversionRate; 
                    break;
                }
            }
        
        
        Opportunity  testOpp2 = [SELECT License_Amount_USD__c from Opportunity WHERE Id=:testOpp.Id];
                                
         
        //System.assertEquals(ConvertedAmount, testOpp2.License_Amount_USD__c, 'conversion from GBP to USD failed');

        test.stopTest();

        System.debug('License_Amount_USD_Test: Finished');
    }
    
    static testMethod void Competitors_Test() {
        System.debug('Competitors_Test: Starting');
        QTTestUtils.GlobalSetUp();
        test.startTest();
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        //CCE  Adding an account with BillingCountryCode and Qliktech Company
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
        Name = 'GBR',
        QlikTech_Company_Name__c = 'QlikTech UK Ltd',
		Subsidiary__c = testSubs1.id      
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

        Opportunity testOpp = new Opportunity(
            Name = 'Test Opp 1',
			Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            StageName = 'Goal Confirmed',
            CloseDate = Date.today(),
            AccountId = acc.Id,
            CurrencyISOCode = 'GBP',    
            Amount = 100.0,
            Function__c = 'Other',
            Solution_Area__c = 'Other',
            Signature_Type__c = 'Digital Signature',
            Included_Products__c = 'Qlik Sense',
            ForecastCategoryName = 'Omitted'    //Added to exclude from Opportunity_ManageForecastProducts.trigger
        );
        insert testOpp;        
    
        Competitor_Information__c comp = new Competitor_Information__c(
            Primary_Competitor__c = 'SAP',
            Current_System__c = 'Do Nothing',
            Competitive_Status__c = '1 - Behind Competition',
            Opportunity__c = testOpp.Id
        );       
        Insert comp;
    
        //reset semaphores to continue test
        Semaphores.Opportunity_ExecDealsVisibility_Insert = false;
        Semaphores.Opportunity_ExecDealsVisibility_Update = false;
        
        Competitor_Information__c comp2 = new Competitor_Information__c(
            Primary_Competitor__c = 'IBM',
            Current_System__c = 'Do Nothing',
            Competitive_Status__c = '1 - Behind Competition',
            Opportunity__c = testOpp.Id
        );       
        Insert comp2;
        
        Opportunity  testOpp2 = [SELECT Competitor_s__c from Opportunity WHERE Id=:testOpp.Id];                                
         
        //System.assertEquals('SAP,IBM', testOpp2.Competitor_s__c, 'List of Competitor failed to display');

        test.stopTest();

        System.debug('Competitors_Test: Finished');    
    }

    // Copy of Competitor test method above, CR# 9079 /BTN
    static testMethod void InfluencingPartner_Test() {
        System.debug('InfluencingPartner_Test: Starting');
        QTTestUtils.GlobalSetUp();
        test.startTest();
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id      
        );
        insert QTComp;
        
        Id partnerAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account testAcc = new Account(
            Name='Qgate Software Ltd', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
            recordtypeid = partnerAccRecordTypeId,
            Billing_Country_Code__c = qtComp.Id, 
            Navision_Status__c = 'Partner');
        insert testAcc;
        
        Contact testCon = new Contact();
        testCon.FirstName = 'James';
        testCon.LastName = 'Jury';
        testCon.accountId = testAcc.Id;
        insert testCon;
        testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE Id = :testCon.Id];
        
        //Contact testCon = [SELECT Id FROM Contact WHERE LastName = 'Jury' and FirstName = 'James' limit 1]; 
    
        //CCE  Adding an account with BillingCountryCode and Qliktech Company
        
        Account acc = new Account(Name = 'Test', recordtypeid = partnerAccRecordTypeId, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;
    
    

        Opportunity testOpp = new Opportunity(
        Name = 'Test Opp 1',
		Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
        StageName = 'Goal Confirmed',
        CloseDate = Date.today(),
        AccountId = acc.Id,
        CurrencyISOCode = 'GBP',    
        Amount = 100.0,
        Function__c = 'Other',
        Solution_Area__c = 'Other',
        Signature_Type__c = 'Digital Signature',
        Included_Products__c = 'Qlik Sense',
        ForecastCategoryName = 'Omitted'    //Added to exclude from Opportunity_ManageForecastProducts.trigger
        );
        insert testOpp;        

        //Influencing partner test    
        Influencing_Partner__c InfPart = new Influencing_Partner__c(
            Influencing_Partner_Account__c = acc.Id,
            Opportunity__c = testOpp.Id,
            Influencing_Partner_Contact__c = testCon.Id, 
            Opportunity_Origin__c = 'Other'
        );       
        Insert InfPart; 
    
        //reset semaphores to continue test
        Semaphores.Opportunity_ExecDealsVisibility_Insert = false;
        Semaphores.Opportunity_ExecDealsVisibility_Update = false;
        

        //Influencing partner test    
        Influencing_Partner__c InfPart2 = new Influencing_Partner__c(
            Influencing_Partner_Account__c = acc.Id,
            Opportunity__c = testOpp.Id,
            Influencing_Partner_Contact__c = testCon.Id, 
            Opportunity_Origin__c = 'Other'
        );       
        Insert InfPart2; 
        
        Opportunity  testOpp2 = [SELECT Influencing_Partners__c from Opportunity WHERE Id=:testOpp.Id];                                
         
        //System.assertEquals('Qgate Software Ltd,Qgate Software Ltd'.toLowerCase(), testOpp2.Influencing_Partners__c.toLowerCase(), 'List of Influencing Partner failed to display');

        test.stopTest();

        System.debug('InfluencingPartner_Test: Finished');  
    }


public static testMethod void Grand_Total_USD_Test() {
        System.debug('Grand_Total_USD_Test: Starting');
        QTTestUtils.GlobalSetUp();
        
        List<CurrencyType> c = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
        double ConvertedAmount = 0.0;
   
        test.startTest();
        
        // Adding an account with BillingCountryCode and Qliktech Company
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id      
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;

        Opportunity testOpp = new Opportunity(
            Name = 'Test Opp 1',
			Deployment_Preference__c = 'On-Premise', //added to comply OP063_Deployment_and_Cloud_Provider validation rule
            StageName = 'Goal Confirmed',
            CloseDate = Date.today().addDays(5), //To avoid test classes to fail after overnight run.
            AccountId = acc.Id,
            CurrencyISOCode = 'GBP',    
            Amount = 1000.0,
            Function__c = 'Other',
            Solution_Area__c = 'Other',
            Signature_Type__c = 'Digital Signature',
            Included_Products__c = 'Qlik Sense',
            ForecastCategoryName = 'Omitted'    //Added to exclude from Opportunity_ManageForecastProducts.trigger    
        );
        insert testOpp;        
        
        Opportunity  testOpp1 = [SELECT Grand_Total__c from Opportunity WHERE Id=:testOpp.Id];
        
        for(integer i = 0; i< c.size(); i++)
        {
           
            if(testOpp.CurrencyISOCode == c[i].ISOCode)
            {          
                ConvertedAmount = testOpp1.Grand_Total__c / c[i].ConversionRate; 
                break;
            }
        }          
        
        Opportunity  testOpp2 = [SELECT Grand_Total_USD__c from Opportunity WHERE Id=:testOpp.Id];
                     
        //System.assertEquals(ConvertedAmount, testOpp2.Grand_Total_USD__c, 'conversion from GBP to USD failed');

        test.stopTest();

        System.debug('Grand_Total_USD_Test: Finished');

    }   

    
}