/*
@author: Anthony Victorio, Model Metrics
@date: 02/06/2012
@description: Controller for the QVM Main page, determines if registration 
or control panel page shoudl be shown
*/
public with sharing class QVM_ControlPanelMainCon {
	
	public QVM_Partner_Data__c partnerData {get; private set;}
	public Boolean showRegistrationForm {get; private set;}
	public Boolean showControlPanel {get; private set;}
	
	public QVM_ControlPanelMainCon() {
		
		try {
			partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId()];
		} catch (Exception e) {
			// Do Nothing
		}
		
		showRegistrationForm = (partnerData == null || partnerData.Approved__c == false) ? true : false;
        showControlPanel = (partnerData == null || partnerData.Approved__c == false) ? false : true;
        
	}

}