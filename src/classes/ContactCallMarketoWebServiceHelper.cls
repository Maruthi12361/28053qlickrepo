/******************************************************

    CallMarketoWebService  
  
    This trigger look for "Trigger_Webservice" to be
    checked. When checked it will be reset and a call
    to the webservice at dev.qlikview.com will be
    performed. 

    Changelog:    
        2011-07-26  RDZ     Created trigger
		2018-03-29  CRW		Added !System.isBatch() to avoid future call from batch CHG0033482
******************************************************/ 

public with sharing class ContactCallMarketoWebServiceHelper {
	
	public static void callServiceOnBefore(List<Contact> triggernew){
		// A new Contact will not contain an Id until it's saved. A shadow field will
		// be used to singnal the after trigger to call the webservice
		for (Contact x : triggernew)
		{
			// Start of my clearing the shadow field (might be set if function has been used before)
			if (x.Trigger_Webservice_Shadow__c)
			{
				x.Trigger_Webservice_Shadow__c = false;
			}
			// If we should trigger. Clear the checkbox and select the shadow field. 
			if (x.Trigger_Webservice__c)
			{
				x.Trigger_Webservice_Shadow__c = true;
				x.Trigger_Webservice__c = false;
			}	
		}						
	}

	public static void callServiceOnAfter(List<Contact> triggernew){
		// In the after trigger we will always have an Id. We should only look at
		// the shadow field in the after trigger.
		Map<string, string> ContactToCall = new Map<string, string>();
		
		for (Contact x : triggernew)
		{
			if (x.Trigger_Webservice_Shadow__c)
			{
				string Id = x.Id;
				ContactToCall.put(Id, x.Email);
			}			
		}
		
		// If there is any Contacts in the Call queue, call the external webservice
		if (ContactToCall.size() > 0)
		{
			System.debug('ContactCallMarketoWebService: Calling array: ' + ContactToCall);
            if(!System.isBatch()){
                	System.debug('CallInitializeApexTrigger future method invoked for>>' + ContactToCall);
					devQlikViewCom.CallInitializeApexTrigger(ContactToCall, true);
            }
		}
	}
}