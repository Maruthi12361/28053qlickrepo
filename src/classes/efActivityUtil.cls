//*********************************************************/
// Author: Mark Cane&
// Creation date: 18/08/2010
// Intent:  
//          
// Change History
// --------------
// 2014-02-13   SAN     CR#11207 to put datetime in session as the same as eu1 sfdc server     
//
//*********************************************************/
public class efActivityUtil{
    private List<efSessionWrapper> sessionList = new List<efSessionWrapper>();
    private efRegistrationManager regMan;
    private String specialEventSelected='';
    private String activitiesMessage='';
    
    private static Map<String,String> sessionMap = efCustomSettings.getValues(efCustomSettings.SESSION_MAP);
    
    public efActivityUtil(efRegistrationManager regMan){
        this.regMan = regMan;
    }
        
    
    // Activity methods.
    public String getActivitiesMessage(){
        return activitiesMessage;
    }
    
    public List<efSessionWrapper> getActivities(){
        List<efSessionWrapper> sessList = getAllActivities();
        List<efSessionWrapper> actList = new List<efSessionWrapper>();
        Integer index = 0;
        
        if(sessList!=null && sessList.size()>0){
            for(efSessionWrapper s: sessList){
                if(!s.getSessions().Executive_Special_Event__c){
                    s.setCounter(index++);
                    actList.add(s);
                }
            }
            return actList;
        }
        return null;
    }

    public void setActivities(List<efSessionWrapper> sessionList){ 
        this.sessionList = sessionList;
    }

    public List<efSessionWrapper> getAllActivities(){
        if(sessionList != null && sessionList.size()>0){
            return sessionList;
        } else {
            Set<String> sessionAttIds = new Set<String>();
            // @todo time condition removed for testing, revert when done
            //Session__c[] sessions = [Select Session_Description__c,Session_Abstract__c, Used__c, Id, Name, Session_End_Time__c, Session_Start_Time__c,Venue__c,Balance__c,Executive_Special_Event__c,(Select Id  From Surveys__r where Type__c='Activity'),(Select Id, Name, Status__c From Session__r where Status__c!='Cancelled' AND Registration__c=:regMan.getRegistrationWrapper().getId()) From Session__c Where Active__c=true AND Event__c =:configMap.get('eventId') AND RecordTypeId =:sessionMap.get('activityrecordtypeid') AND Session_Start_Time__c >= TODAY  AND Valid_Attendee_types__c includes (:attendeeType) ORDER BY Session_Start_Time__c]; 
            Session__c[] sessions = [Select Session_Description__c,
                                            Session_Abstract__c,
                                            Used__c,
                                            Id,
                                            Name,
                                            Session_End_Time__c,
                                            Session_Date__c,
                                            Session_Time__c,
                                            Session_Start_Time__c,
                                            Venue__c,
                                            Balance__c,
                                            Executive_Special_Event__c,
                                            Mandatory__c,
                                            Default_Selected__c,
                                            Session_Topic_Category__c,
                                            Room__r.Name,
                                            (
                                                Select Id,
                                                       Name,
                                                       Status__c,
                                                       Session_Topic__c
                                                From Session__r
                                                Where Status__c!=:efConstants.REG_STATUS_CANCELLED And
                                                Registration__c=:regMan.getRegistrationWrapper().getId()
                                            )
                                            From Session__c
                                            Where Active__c = true And
                                            Event__c=:regMan.getEventWrapper().getEvent().Id And
                                            RecordTypeId =:sessionMap.get('activityrecordtypeid') And
                                            Valid_Attendee_types__c includes (:regMan.getFinalUserType()) Order By 
                                            Session_Start_Time__c];
            if(sessions != null && sessions.size()>0){
                for(Integer i=0;i<sessions.size();i++){
                    sessions[i].Session_Start_Time__c = efUtility.convertToGMT(sessions[i].Session_Start_Time__c);
                    sessions[i].Session_End_Time__c = efUtility.convertToGMT(sessions[i].Session_End_Time__c);
                    efSessionWrapper sess = new efSessionWrapper(sessions[i],true);
                    sess.setCounter(i);
                    
                    if(sess.getIsAddedToSchedule()){
                        sess.setIsSelected(true);
                        sess.setRecordCreated(true);
                    }

                    if((sess.getIsAddedToSchedule()) &&
                        sess.getSessions().Executive_Special_Event__c){
                        specialEventSelected = sess.getSessions().Id;
                    }                       
                    sessionList.add(sess);
                }
            }
            return sessionList;
        }
    }
    
    public Boolean getIsValidActivities(efRegistrationWrapper r){       
        return true;
    }
    
    public void addActivities(){
        if (this.sessionList != null && this.sessionList.size()>0){
            List<Session_Attendance__c> sessionattndLst = new List<Session_Attendance__c>();
            List<Session__c> sessionLst = new List<Session__c>();
            List<String> newSessionsAdded = new List<String>();
            List<Session_Attendance__c> salist = new List<Session_Attendance__c>();
            
            for(Integer i=0;i<sessionList.size();i++){                    
                if (sessionList[i].getIsSelected() && !sessionList[i].getIsAddedToSchedule()){
                    sessionList[i].setRecordCreated(true);
                    
                    if (sessionList[i].getSessions().Balance__c>0){
                        Session_Attendance__c objsa = new  Session_Attendance__c();
                        objsa.Session__c = sessionList[i].getSessions().Id ;
                        objsa.Registration__c=regMan.getRegistrationWrapper().getId() ;
                        objsa.Status__c = efConstants.SESSION_ATTENDANCE_REGISTERED;
                        
                        if (!efUtility.isNull(sessionList[i].getSelectedTopic())){
                            objsa.Session_Topic__c = sessionList[i].getSelectedTopic();
                        }                        
                        sessionattndLst.add(objsa);                         
                        sessionList[i].addSessionAttendance(objsa);
                        
                        Double used = 0;
                        
                        if(sessionList[i].getSessions().Used__c!=null)
                            used = sessionList[i].getSessions().Used__c;
                             
                        Session__c objs = new Session__c(ID=sessionList[i].getSessions().Id,Used__c = used+1);
                        sessionLst.add(objs);                                           
                    }
                } else if(sessionList[i].getSessions().Executive_Special_Event__c && (efUtility.isNull(specialEventSelected) || (!efUtility.isNull(specialEventSelected) && !specialEventSelected.equals(sessionList[i].getSessions().Id)))){
                    List<efSessionAttendanceWrapper> salistT = sessionList.get(i).getSessionAttendanceList();
                    
                    List<Session_Attendance__c> salistO = new List<Session_Attendance__c>();
                    
                    if(salistT!=null && salistT.size()>0){
                        for(Integer j=0;j<salistT.size();j++){
                            salistT[j].getSessionattendance().status__c = efConstants.SESSION_ATTENDANCE_CANCELLED;
                            salist.add(salistT[j].getSessionAttendance());
                            sessionList[i].getSessionAttendanceList().remove(j);
                        }
                            
                        sessionList[i].setIsSelected(false);
                        sessionList[i].setRecordCreated(false);
                    }
                }                    
            }
    
            if (sessionattndLst.size() > 0){
                update  sessionLst;
                insert  sessionattndLst;
            }
                            
            if(salist.size() > 0){
                update salist;
            }                
            updateSessionRecords(sessionattndLst);
        }
    }
    
    public void updateSessionRecords(List<Session_Attendance__c> sessionattndLst){
        if(sessionList!=null && sessionList.size()>0 && sessionattndLst!=null && sessionattndLst.size()>0){
            for(Session_Attendance__c sa:sessionattndLst){
                for(Integer i=0;i<sessionList.size();i++){
                    if(String.valueOf(sessionList[i].getSessions().Id).equals(String.valueOf(sa.Session__c))){
                        sessionList[i].addSessionAttendance(sa);
                    }   
                }
            }
        }
    }
    
    public List<efSessionGroupWrapper> getActivitiesByGroup(){
        List<efSessionGroupWrapper> groups = efActivityUtil.getActivitiesBySessionGroup(getWizardActivities());        
        return efActivityUtil.sortSessionsByGroup(groups);
    }
    
    private List<efSessionWrapper> getWizardActivities(){
        List<efSessionWrapper> activities = getActivities();

        if (activities != null){
            for (efSessionWrapper a:activities){
                if (a.getDefaultSelected()){
                    a.setIsSelected(true);
                }
            }
        }
        return activities;
    } 
    
    // End activity methods.
    
    
    // Static methods.
        
    
    //Join Products and Sessions to single List<efProductWrapper>
    public static List<efProductWrapper> joinTrainingAndActivities(List<efProductWrapper> products, List<efSessionWrapper> sessions){
        List<efProductWrapper> everything = new List<efProductWrapper>();

        if (sessions != null){
            for (efSessionWrapper s:sessions){
                Product2 product=new Product2();
               
                Session__c sc = s.getSessions();
                product.ProductCode = sc.Id;
                // 2014-02-13   SAN     CR#11207 to put datetime in session as the same as eu1 sfdc server     
                product.startdate__c = efUtility.convertToGMT(sc.Session_Start_Time__c);
                product.enddate__c = efUtility.convertToGMT(sc.Session_End_Time__c);
                product.Description = sc.Session_Abstract__c;
                product.Name = sc.Name;
                product.Room__r = sc.Room__r;
                efProductWrapper pi = new efProductWrapper(product);
                
                if (sc.RecordTypeId == sessionMap.get('sessionrecordtypeid')){
                    pi.genericType = efConstants.GENERIC_TYPE_SESSION;
                }
                else if (sc.RecordTypeId == sessionMap.get('activityrecordtypeid')){
                    pi.genericType = efConstants.GENERIC_TYPE_ACTIVITY;
                }
                everything.add(pi);
            }
        }
        
        if (products != null){
            for (efProductWrapper pi:products){
                if (pi.getProductFamily()==efConstants.PROD_FAMILY_CLASS){
                    pi.genericType = efConstants.GENERIC_TYPE_CLASS;
                }
                everything.add(pi);
            }
        }         
        return everything;
    }
    
    public static efProductWrapper[] getScheduledTraining(ID registrationID){
        List<efProductWrapper> classes = new List<efProductWrapper>();
        OpportunityLineItem[] oliItem = [Select Id,
                                                PricebookEntry.Product2.Family,
                                                PricebookEntry.Product2Id,
                                                PricebookEntry.Product2.startdate__c,
                                                PricebookEntry.Product2.enddate__c,
                                                PricebookEntry.Product2.description,
                                                PricebookEntry.Product2.name,
                                                PricebookEntry.Product2.Room__r.Name
                                         From OpportunityLineItem
                                         Where Registration__c = :registrationID
                                         And PricebookEntry.Product2.Family = :efConstants.PROD_FAMILY_CLASS
                                         And Status__c != :efConstants.OLI_STATUS_CANCELLED
                                         And Status__c != :efConstants.OLI_STATUS_CANCELLATION_REQUESTED
                                         And Status__c != :efConstants.OLI_STATUS_PAYMENT_DENIED
                                         Order By PricebookEntry.Product2.startdate__c];                      
        if (oliItem != null && oliItem.size() > 0){
            for(OpportunityLineItem o: oliItem){
                efProductWrapper pi = new efProductWrapper(o.PricebookEntry.Product2);                
                classes.add(pi);
            }
        }
        return classes;
    }
    
    public static List<efSessionWrapper> getScheduledActivities(Id registrationId){
        List<Session_Attendance__c> slist = [Select s.Id,
                                                    s.Session__c,
                                                    s.Session__r.Session_Abstract__c,
                                                    s.Session__r.Active__c,
                                                    s.Session__r.Id,
                                                    s.Session__r.Name,
                                                    s.Session__r.RecordTypeId,
                                                    s.Session__r.Session_End_Time__c,
                                                    s.Session__r.Session_Date__c,
                                                    s.Session__r.Session_Time__c,
                                                    s.Session__r.Session_Start_Time__c,
                                                    s.Session__r.Venue__c,
                                                    s.Session__r.Room__c,
                                                    s.Session__r.Room__r.Name
                                             From Session_Attendance__c s
                                             Where s.Registration__c = :registrationId
                                             And s.Session__r.Session_Status__c != :efConstants.SESSION_STATUS_CANCELLED
                                             And s.status__c != :efConstants.SESSION_ATTENDANCE_CANCELLED
                                             And s.Session__r.Active__c = true
                                             Order By s.Session__r.Session_Start_Time__c];                                             
        if (slist!=null && slist.size()>0){
            List<efSessionWrapper> sessionList = new List<efSessionWrapper>();
            
            for(Session_Attendance__c s: slist){
                sessionList.add(new efSessionWrapper(s.Session__r)); 
            }            
            return sessionList;
        }        
        return null;
    }
    
    public static String getFormatedTime(DateTime startDate, DateTime endDate){
        String formatedDate;
        
        if(startDate != null && endDate != null){
            formatedDate = startDate.format('h a') + ' - ' +  endDate.format('h a');
        }        
        return formatedDate;
    }
    
    public static String getFormatedDate(DateTime startDate, DateTime endDate){
        String formatedDate;
        
        if(startDate != null && endDate != null){
            formatedDate = startDate.format('E, MMM d');
        
            if(!startDate.isSameDay(endDate))
                formatedDate = formatedDate + endDate.format(' - E, MMM d');
        }        
        return formatedDate;
    }
    
    /* agrassi
     * @todo Should be merged with Classes.getClassesByGroup
     */
    public static List<efSessionGroupWrapper> getActivitiesBySessionGroup(List<efSessionWrapper> sessions){
        List<efSessionGroupWrapper> sessionGroups = new List<efSessionGroupWrapper>(); 
        Map<String,efSessionGroupWrapper> groupMap = new Map<String,efSessionGroupWrapper>();
        
        if(sessions!=null && sessions.size()>0){
            for(efSessionWrapper s:sessions){
                String groupName = s.getFormatedDate();
                   
                if(!efUtility.isNull(groupName)){
                    if(groupMap.containsKey(groupName)){
                        efSessionGroupWrapper g = groupMap.get(groupName);
                        g.addItem(s);
                        groupMap.put(groupName,g);
                    } else{
                        efSessionGroupWrapper g = new efSessionGroupWrapper();
                        g.setGroupName(groupName);
                        g.setGroupDate(s.getSDate());
                        g.addItem(s);
                        groupMap.put(groupName,g);
                    }
                }
            }

            if(!groupMap.isEmpty()){
                sessionGroups = groupMap.values();
            }
            system.debug('sessionGroups.size() : '+sessionGroups.size().format());
            system.debug('&&&& groupMap.size() : '+groupMap.size().format());
            return sortSessionsByGroup(sessionGroups);
        }
        
        return sessionGroups;
    }

    /* agrassi
     * @todo should be merged with Classes.sortClassesByGroup
     */
    public static List<efSessionGroupWrapper> sortSessionsByGroup(List<efSessionGroupWrapper> cgList){
        for (Integer i=0;i<cgList.size();i++){
            for(Integer j=0;j<cgList.size();j++){
                if(cgList.get(i).getGroupDate() < cgList.get(j).getGroupDate()){
                    efSessionGroupWrapper cTemp = cgList.get(i);
                    cgList.set(i,cgList.get(j));
                    cgList.set(j,cTemp);
                }
            }
        }        
        return cgList;      
    }

    /**
     * agrassi
     * @todo To be merged with session analog method.
     */
    public static List<efClassWrapper> getClassesByGroup(List<efProductWrapper> pRecords){
        List<efClassWrapper> classList = new List<efClassWrapper>(); 
        Map<String, efClassWrapper> classMap = new Map<String, efClassWrapper>();
        if(pRecords!=null && pRecords.size()>0){
            for(efProductWrapper p: pRecords){
                String groupName = p.getFormatedDate();
                                      
                if(!efUtility.isNull(groupName)){
                    if(classMap.containsKey(groupName)){
                        efClassWrapper c = classMap.get(groupName);
                        c.addClass(p);
                        classMap.put(groupName,c);
                    } else{
                        efClassWrapper c = new efClassWrapper();
                        c.setGroupName(groupName);
                        c.setGroupDate(p.getProduct().startdate__c);
                        c.addClass(p);
                        classMap.put(groupName,c);
                    }
                }
            }

            if(!classMap.isEmpty()){
                classList = classMap.values();
            }
            return sortClassesByGroup(classList);
        }        
        return classList;
    }

    public static List<efClassWrapper> getClassesByGroupCalendar(List<efProductWrapper> pRecords){
        Integer count =  0;
        List<efClassWrapper> classList = new List<efClassWrapper>(); 

        Map<String, efClassWrapper> classMap = new Map<String, efClassWrapper>();

        if(pRecords!=null && pRecords.size()>0){
            for(efProductWrapper p: pRecords){
                p.setCounter(count++);
                String groupName = p.getFormatedDateSingle();
                                      
                if(!efUtility.isNull(groupName)){
                    if(classMap.containsKey(groupName)){
                        efClassWrapper c = classMap.get(groupName);
                        c.addClass(p);
                        classMap.put(groupName,c);
                    } else{
                        efClassWrapper c = new efClassWrapper();
                        c.setGroupName(groupName);
                        c.setGroupDate(p.getProduct().startdate__c);
                        c.addClass(p);
                        classMap.put(groupName,c);
                    }
                }
            }

            if(!classMap.isEmpty()){
                classList = classMap.values();
            }
            return sortClassesByGroup(classList);
        }        
        return classList;
    }

    /**
     * agrassi
     * @todo To be merged with sessions analog method.
     */
    private static List<efClassWrapper> sortClassesByGroup(List<efClassWrapper> cgList){
        for (Integer i=0;i<cgList.size();i++){
            for(Integer j=0;j<cgList.size();j++){
                if(cgList.get(i).getGroupDate() < cgList.get(j).getGroupDate()){
                    efClassWrapper cTemp = cgList.get(i);
                    cgList.set(i,cgList.get(j));
                    cgList.set(j,cTemp);
                }
            }
        }        
        return cgList;      
    }
}