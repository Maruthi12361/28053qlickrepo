public class OppProductRollupMaker {
	public static void onBeforeInsert(List<OpportunityLineItem> inputList) {
    }
    
    public static void onBeforeUpdate(List<OpportunityLineItem> inputList) { 
        
    }
    
    public static void onAfterInsert(List<OpportunityLineItem>  inputList) {    
		processRollupNetPrice(inputList);  
    }
    
    public static void onAfterUpdate(List<OpportunityLineItem>  inputList, Map<id, OpportunityLineItem> inputMap) {
		List<OpportunityLineItem> listToProcess = new List<OpportunityLineItem>();
		for(OpportunityLineItem itemOppItem : inputList) {
			if(itemOppItem.Net_Price__c != inputMap.get(itemOppItem.id).Net_Price__c) {
				listToProcess.add(itemOppItem);
			}
		}
		if(listToProcess.size() > 0) {
			processRollupNetPrice(inputList);
		}
    }

	public static void onAfterDelete(List<OpportunityLineItem>  inputList) {
		processRollupNetPrice(inputList);
	}

	private static void processRollupNetPrice(List<OpportunityLineItem>  inputList) {
		List<Opportunity> listOppsForUpdate = new List<Opportunity>();
		List<String>  listOfOpps = new List<String>();
		Map<String, List<OpportunityLineItem>> mapOfOpps = new Map<String, List<OpportunityLineItem>>();
		for(OpportunityLineItem item : inputList) {
			listOfOpps.add(item.OpportunityId);
		}
		Map<String, Opportunity> mapOfOpsIds = new Map<String, Opportunity>();
		for(Opportunity itemOpps : [Select id, 	OEM_Prepay_Value__c, (Select id, Net_Price__c, Product_Family__c From OpportunityLineItems where Product_Family__c = 'Prepay') From Opportunity where Id =:  listOfOpps]) {
			mapOfOpps.put(itemOpps.id, itemOpps.OpportunityLineItems);
			mapOfOpsIds.PUT(itemOpps.id, itemOpps);
		}
		for(String itemOppty : mapOfOpps.keySet()) {
			mapOfOpsIds.get(itemOppty).OEM_Prepay_Value__c = 0;
			for(OpportunityLineItem itemOppItem : mapOfOpps.get(itemOppty)) {
				mapOfOpsIds.get(itemOppty).OEM_Prepay_Value__c += itemOppItem.Net_Price__c;
			}
			listOppsForUpdate.add(mapOfOpsIds.get(itemOppty));
		}
		if(listOppsForUpdate.size() >0 ) {
			update listOppsForUpdate;
		}
	}
}