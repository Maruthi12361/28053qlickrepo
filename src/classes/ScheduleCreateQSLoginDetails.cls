/********************************************************
* CLASS: ScheduleCreateQSLoginDetails
* DESCRIPTION: Scheduled class for Web Activity
* Takes one inparam for deciding the FirstLogin Type - QSD_FirstLogin or QSC_FirstLogin
* 
*
* CHANGELOG:    
*	2018-10-15 - BAD - Added Initial logic
*********************************************************/
global class ScheduleCreateQSLoginDetails implements Schedulable {
	String firstLoginType;

	global ScheduleCreateQSLoginDetails(String loginType) {
        firstLoginType = loginType;
	}
 
	global void execute(SchedulableContext sc) {
		List<Web_Activity__c> lstWA = [select id from Web_Activity__c where Status__c = 'Pending' and Type__c =: firstLoginType];
		Database.executeBatch(new CreateQSLoginDetails(lstWA, firstLoginType), 100);  
	}

}