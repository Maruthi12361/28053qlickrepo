/********
* NAME :NSSupportContractSharingTest
* Description: Tests for Helper class to sharing NS Support contracts
* 
*
*Change Log:
    IRN      2015-12-21   Partner share project Created test class

******/
@isTest
public class NSSupportContractSharingTest{

    static final integer NBR_OF_ACCOUNTS = 2;
    static final Id SysAdminProfileId = '00e20000000yyUzAAI';
    static final Id PortalUserRoleId  = '00E20000000vrJSEAY';
    //End User Account Record Type Id
    static final Id EndUserAccRecTypeId = '01220000000DOFuAAO';
    //PRM - Sales Dependent Territory + QlikBuy
    static final Id PrmProfileId = '00e20000001OyLwAAK';
    // Partner Account Record Type ID
    static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';

    private static void Setup(){
    }

     private static testMethod void TestShareContractsToPartner(){
        ApexSharingRules.TestingParnershare = true;
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User myUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        List<Account> myAccounts = createTestAccounts(myUser, ctList);
        
        List<User> pUsers = createUserFromProfileId(PrmProfileId, ctList); 

        NS_Support_Contract__c contract = createMockNSSupportContract(myAccounts);
        NSSupportContractSharing ns = new NSSupportContractSharing();
        List<NS_Support_Contract__c> contracts = new List<NS_Support_Contract__c>();
        contracts.add(contract);
        Map<Id, List<Id>> partnermap = ApexSharingRules.getUniquePartnersToShareWith(contracts);
        ns.shareContractsToPartner(partnermap);
        
        List<NS_Support_Contract__Share> share = [Select Id, AccessLevel, RowCause, ParentId, UserOrGroupId from NS_Support_Contract__Share where parentId = :contract.id]; 
        System.assertEquals(2, share.size());
        test.stopTest();
    }

    public static NS_Support_Contract__c createMockNSSupportContract(List<Account> acc){
        NS_Support_Contract__c ns = new NS_Support_Contract__c(Name='test', End_User__c = acc[0].Id, Reseller__c = acc[1].Id);
        insert ns;
        return ns;
    }

 
     public static User createMockPatnerUser(User me){

        User aUser = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');
        aUser.IsPortalEnabled = true;
        System.runAs ( me ) 
        {
            update aUser;
        }
        return aUser;
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me ) 
    {
        User u 
            = new User(
               alias = 'adminUsr', email='adminUser@tjtest.com.test',
                Emailencodingkey='UTF-8', lastname='adminTest', 
                Languagelocalekey='en_US', localesidkey='en_US',
                Profileid = profId,
                Timezonesidkey='America/Los_Angeles', 
                //ContactId = cont.Id,
                UserRoleId = roleId,
                Username= System.now().millisecond() + '_' + '_newuser@jttest.com.test'
            );

        System.runAs ( me ) 
        {
            insert u;
        }

        return u;
    }

    /*public static List<Account> createTestAccounts(User aUser, List<Contact> clst)
    {
        List<Account> alst = new List<Account>();
        //List<Contact> clst = new List<Contact>();
        User user1 = createMockPatnerUser(aUser);
        Boolean oddIndex = false;
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser){
            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                //Create account

                Account pAccount = new Account(
                    Billing_Country_Code__c = qtComp.Id,
                    QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                    Name = 'partneracc' + i,
                    RecordTypeId = PartnerAccRecTypeId
                    );
                if (oddIndex)
                {
                    pAccount.Owner = user1;
                }
                oddIndex = !oddIndex;
                alst.add(pAccount);  
            }
            Insert alst;

            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Contact contact = new Contact( FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = alst[i].Id,
                Email = 'tc' + i + '@test.com');
                clst.add(contact);  
            }
            insert clst;
        } 
        return alst;
    }*/
    
    public static List<Account> createTestAccounts(User aUser, List<Contact> cLst)
    {
        List<Account> aLst = new List<Account>();
        //List<Contact> clst = new List<Contact>();
        //User user1 = createMockPatnerUser(aUser);
        //Boolean oddIndex = false;
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser){
            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                //Create account

                Account userAccount = new Account(
                    Billing_Country_Code__c = qtComp.Id,
                    QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                    Name = 'UserAcc' + i,
                    OwnerId = aUser.Id,
                    RecordTypeId = EndUserAccRecTypeId
                    );
                //if (oddIndex)
                //{
                //    userAccount.Owner = user1;
                //    userAccount.OwnerId = user1.Id;
                //}
                //oddIndex = !oddIndex;
                aLst.add(userAccount);  
            }

            Insert aLst;

            for(Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Contact contact = new Contact( FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = aLst[i].Id,
                Email = 'tc' + i + '@test.com');
                cLst.add(contact);  
            }
            insert cLst;
        } 
        return aLst;
    }

    public static List<User> createUserFromProfileId(Id profId, List<Contact> clist) {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<User> usrs = new List<User>();
        System.runAs ( thisUser ) {
            integer ndx = 1;
            for (Contact ct : clist)
            {
                User u = new User(
                    alias = 'newUser', 
                    email = 'puser'+ ndx + '@tjtest.com.test',
                    Emailencodingkey = 'UTF-8', 
                    lastname = 'Testing', 
                    Languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',
                    Profileid = profId,
                    Timezonesidkey='America/Los_Angeles',
                    ContactId = ct.Id,
                    Username= 'nssuser' + ndx + '@tjtest.com.test'
                );

                usrs.add(u);
                
                ndx++;
            }

            insert usrs;

            for (integer i = 0; i < usrs.size(); i++)
            {
                usrs[i].IsPortalEnabled = true;                        
            }

            update usrs;
        }

        return usrs;
    } 
}