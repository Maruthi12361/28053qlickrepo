/**
	16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors
**/

@isTest
private class CaseNewControllerExtensionTest {
	
	@isTest static void test_method_one() {
		QTTestUtils.GlobalSetUp();


		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Case caze = new Case();
        caze.AccountId = testAccount.Id;
        caze.ContactId = testContact.Id;
        caze.RecordTypeId = '01220000000DZqGAAW';

		Insert caze;

		Non_Consumer_Entitlements__c ent = new Non_Consumer_Entitlements__c();
		ent.Name = 'Test';
		insert ent;


		ApexPages.StandardController stdController = new ApexPages.StandardController(caze);
    	CaseNewControllerExtension controller = new CaseNewControllerExtension(stdController);

    	ApexPages.currentPage().getParameters().put('RecordType', '012D0000000K3mZ');

    	controller.redirect();
    	controller.init();
    	Pagereference pr = controller.doSaveAndEdit();
        string s = pr.getUrl();
        System.assert(s.contains('/e'), 'Assertion failed as /e was not added to the Pagereference');
	}

	@isTest static void testPRM() {
		QTTestUtils.GlobalSetUp();


		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'PRM - Base' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@google.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Case caze = new Case();
        caze.AccountId = testAccount.Id;
        caze.ContactId = testContact.Id;
        caze.RecordTypeId = '01220000000DZqGAAW';

		Insert caze;

		Non_Consumer_Entitlements__c ent = new Non_Consumer_Entitlements__c();
		ent.Name = 'Test';
		insert ent;

		test.startTest();
		//system.runAs(communityUser){
			ApexPages.StandardController stdController = new ApexPages.StandardController(caze);
	    	CaseNewControllerExtension controller = new CaseNewControllerExtension(stdController);

	    	//ApexPages.currentPage().getParameters().put('RecordType', '012D0000000K3mZ');

	    	controller.redirect();
	    	controller.init();
	    	Pagereference pr = controller.doSaveAndEdit();
	        string s = pr.getUrl();
	        System.assert(s.contains('/e'), 'Assertion failed as /e was not added to the Pagereference');
	    //}
	}
	@isTest static void testNoPRM() {
		QTTestUtils.GlobalSetUp();


		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Case caze = new Case();
        caze.AccountId = testAccount.Id;
        caze.ContactId = testContact.Id;
        caze.RecordTypeId = '01220000000DZqGAAW';

		Insert caze;

		Non_Consumer_Entitlements__c ent = new Non_Consumer_Entitlements__c();
		ent.Name = 'Test';
		insert ent;

		test.startTest();
		system.runAs(communityUser){
			ApexPages.StandardController stdController = new ApexPages.StandardController(caze);
	    	CaseNewControllerExtension controller = new CaseNewControllerExtension(stdController);

	    	//ApexPages.currentPage().getParameters().put('RecordType', '012D0000000K3mZ');

	    	controller.redirect();
	    	controller.init();
	    	Pagereference pr = controller.doSaveAndEdit();
	        string s = pr.getUrl();
	        System.assert(s.contains('/e'), 'Assertion failed as /e was not added to the Pagereference');
	    }
	}
	@isTest static void testInternal() {
		QTTestUtils.GlobalSetUp();


		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'PRM - Base' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Case caze = new Case();
        caze.AccountId = testAccount.Id;
        caze.ContactId = testContact.Id;
        caze.RecordTypeId = '01220000000Ddyi';

		Insert caze;

		Non_Consumer_Entitlements__c ent = new Non_Consumer_Entitlements__c();
		ent.Name = 'Test';
		insert ent;


		
			ApexPages.StandardController stdController = new ApexPages.StandardController(caze);
    		CaseNewControllerExtension controller = new CaseNewControllerExtension(stdController);

    		//ApexPages.currentPage().getParameters().put('RecordType', '012D0000000K3mZ');

    		controller.redirect();
    		controller.init();
    		Pagereference pr = controller.doSaveAndEdit();
        	string s = pr.getUrl();
        	System.assert(s.contains('/e'), 'Assertion failed as /e was not added to the Pagereference');

	}
	

}