//*********************************************************/
// Author: Mark Cane&
// Creation date: 24/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efSessionAttendanceWrapper{
	private Session_Attendance__c sa;

	public efSessionAttendanceWrapper(Session_Attendance__c sa){
		this.sa = sa;
	}

	public Session_Attendance__c getSessionAttendance(){
		return this.sa; 
	}

	public Session__c getSession(){
		return this.sa.Session__r;
	}
}