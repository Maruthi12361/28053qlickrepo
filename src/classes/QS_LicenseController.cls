/************************************************************************
 * @author  SAN
 * 2014-10-30 customer portal project
 *  
 * Controller Class for License List View and Detail View QS_LicenseController .
 *
 * 2017-02-01 UIN Changes for request control number project CR# 90369
 * 2017-04-15 UIN Changes for request control number project CHG0030333. Modified query to make it more specifc in method checkReqNumberValidOrNot
 * 2018-04-05 AYS IT-117 Improve License display count in Support Portal
 * 2018-12-17 extbad Send email from Noreply. add footer to the email CHG0034910
 * 2019-03-18 AIN IT-1676 Fix for partner community project to only show accounts and licenses that are supported by the partner
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 * 2019-11-21 AIN IT-2320 Added filter expression for Coveo
 * 2020-05-08 extcqb IT-2799 Hide Support Level/Support SLA from the Support Portal
 *************************************************************************/
 
 
public with sharing class QS_LicenseController {

    private Integer licensesLimit = 0;
    private Integer licensesConsumed = 0;
    public Entitlement myLicense { get; set; }
    public Environment__c myEnv { get; set; }
    public String LicenseFilterText {get; set;}
    //public List<SelectOption> licenseOptionListFiltered {get; set;}
    //private transient List<SelectOption> v_licenseOptionList;
    public static List<Entitlement> licenseList;
    private Map<string, string> v_ProductMap;
    private List<SelectOption> v_ProductOptionList;
    public List<SelectOption> accountOptionListFiltered2 {get; set;}
    public String PartnerFilterText {get; set;}
    public static String proname{get;set;}
    public boolean IsSandbox {get;set;}
    public string CoveoFilterExpression { get; set; }
    
    public static String userIpAddress {get;set;}

    public String loggedInUserAccountId {get;set;}

    public void FilterPartners() {
        system.debug('FilterPartners start');
        
        Id contactId, accId = null, Acc;
        string Accounttype ='';
        User usr = [Select contactid from User where id =: Userinfo.getUserid()];
                
        List<SelectOption> newFilteredList = new List<SelectOption>();
        string filterText = PartnerFilterText.toLowerCase();

        for(SelectOption so : SpartnerList)
            if(so.getLabel().toLowerCase().contains(filterText))
                newFilteredList.Add(so);

        accountOptionListFiltered2 = new List<SelectOption>();
        
        system.debug('newFilteredList.Size(): ' + newFilteredList.Size());
        if(newFilteredList.size() > 100) {
            accountOptionListFiltered2.add(new SelectOption('','Too many accounts, please use the filter above ('+newFilteredList.Size()+')'));        
        }
        else //if(newFilteredList.size() > 0) 
        {
            accountOptionListFiltered2.add(new SelectOption('',(newFilteredList.Size()+1) + ' | Accounts Found'));
            accountOptionListFiltered2.add(new SelectOption('001D000000xnZr2','Qlik'));
            accountOptionListFiltered2.addAll(newFilteredList);
        }
        /*else
            accountOptionListFiltered2.add(new SelectOption('','No accounts found, please use the filter to find an account'));        */

        if(accountOptionListFiltered2.size() > 0) {
            selectedPartnerName = accountOptionListFiltered2[0].getValue();
        }

        system.debug('FilterPartners end');
    }
    
    private transient List<SelectOption> v_SpartnerList;
    public List<SelectOption> SpartnerList {   
        get {
            if(v_SpartnerList == null){
                v_SpartnerList = new list<SelectOption>();
                for(Responsible_Partner__c rp:  [select Id, Name, Partner__c, Partner_Name__c, End_User__c from Responsible_Partner__c where End_User__c = :accountId and Partner__c != null]) {
                    system.debug('rp:' + rp);
                    system.debug('rp.Partner__r.Name: ' + rp.Partner_Name__c);
                    v_SpartnerList.add(new SelectOption(rp.Partner__c, rp.Partner_Name__c));
                }
            }
            return v_SpartnerList;
        }
        set {
            v_SpartnerList= value;
        }
    }
    public integer SpartnerListSize {
        get {
            return SpartnerList.Size();
        }
        set {
        }
    }
    
    public List<SelectOption> ProductOptionListFiltered {
        get {     
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('', '-No Filter-'));
            options.add(new SelectOption('All', 'All'));
            for (string key : ProductMap.keySet()) {
                options.add(new SelectOption(key,key));
            }
            v_ProductOptionList = options;
            return v_ProductOptionList;
        }
        set {
            v_ProductOptionList = value;
        }
    }
    
     public Map<string, string> ProductMap {
        get{
            if(v_ProductMap == null)
                populateProductMap();
            return v_ProductMap;
        }
    }
     public void populateProductMap() {
        v_ProductMap = new Map<string, string>();
        List<QS_Knowledge_Data_Category_Mappings__c> prods = QS_Knowledge_Data_Category_Mappings__c.getall().values();
        for(QS_Knowledge_Data_Category_Mappings__c prod : prods) {
            v_ProductMap.put(prod.Name, prod.Group_Content__c);
        }
    }
      
    private boolean v_resultsLicensesNeedsUpdate { get; set; }
    public boolean resultsLicensesNeedsUpdate { 
        get{
            return v_resultsLicensesNeedsUpdate; 
        }
        set {
            system.debug('Setting resultsLicensesNeedsUpdate to ' + value);
            v_resultsLicensesNeedsUpdate = value;
        }
    }

    private boolean v_resultsExpiredLicensesNeedsUpdate { get; set; }
    public boolean resultsExpiredLicensesNeedsUpdate { 
        get{
            return v_resultsExpiredLicensesNeedsUpdate; 
        }
        set {
            v_resultsExpiredLicensesNeedsUpdate = value;
        }
    }
    
    public static String selectedSupportingPartner {get; set;}
    private transient List<SelectOption> v_SupportingPartnerOptionList;
    public List<SelectOption> SupportingPartnerOptionListFiltered {
        get {     
            List<SelectOption> options = new List<SelectOption>();
        
            if (selectedAccountName != null && selectedAccountName != ''){
        
                List<NS_Support_Contract__c> SupportingPartner = [select id, End_User__c , Responsible_Partner__c, Responsible_Partner__r.name 
                    from NS_Support_Contract__c
                    where End_User__c = :selectedAccountName];  
                         
                for ( NS_Support_Contract__c accLic : SupportingPartner ){
                    if(accLic.Responsible_Partner__r.name != null)                       
                        options.add ( new SelectOption ( accLic.Responsible_Partner__c, accLic.Responsible_Partner__r.name ) );
                }
            }
            v_SupportingPartnerOptionList = options;
            return v_SupportingPartnerOptionList;
        }
        set {
            v_SupportingPartnerOptionList = value;
        }
    }

    // selected Product
    public static String selectedProduct {get; set;}
  
    private string v_selectedAccountName;
    public String selectedAccountName {
        get {
            if(v_selectedAccountName == null)
                v_selectedAccountName = '';
            return v_selectedAccountName;
        }
        set {
            system.debug('Set selectedAccountName to ' + value);
            if(v_selectedAccountName != value) {
                system.debug('Setting selectedAccountNameHasChanged to true');
                selectedAccountNameHasChanged = true;
                resultsExpiredLicensesNeedsUpdate = true;
                resultsLicensesNeedsUpdate = true;
            }
            v_selectedAccountName = value;
        }
    }

    private string v_selectedSupportLevel;
    public String selectedSupportLevel {
        get {
            if(v_selectedSupportLevel == null)
                v_selectedSupportLevel = '';
            return v_selectedSupportLevel;
        }
        set {
            system.debug('Set v_selectedSupportLevel to ' + value);
            if(v_selectedSupportLevel != value) {
                system.debug('Setting selectedAccountNameHasChanged to true');
                v_selectedSupportLevel = value;
                selectedAccountNameHasChanged = true;
                resultsExpiredLicensesNeedsUpdate = true;
                resultsLicensesNeedsUpdate = true;
            }
        }
    }

    private string v_selectedPartnerName;
    public String selectedPartnerName {
        get {
            if(v_selectedPartnerName == null)
                v_selectedPartnerName = '';
            return v_selectedPartnerName;
        }
        set {
            system.debug('Set v_selectedPartnerName to ' + value);
            if(v_selectedPartnerName != value) {
                system.debug('Setting selectedAccountNameHasChanged to true');
                v_selectedPartnerName = value;
                selectedAccountNameHasChanged = true;
                resultsExpiredLicensesNeedsUpdate = true;
                resultsLicensesNeedsUpdate = true;
            }
        }
    }

    private transient List<SelectOption> v_SupportLevelOptionList;
    public List<SelectOption> SupportLevelOptionListFiltered {
        get {     
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','-No Filter-'));
            options.add(new SelectOption('Basic Support SLA','Basic'));
            options.add(new SelectOption('Enterprise Support SLA','Enterprise'));
            v_SupportLevelOptionList = options;
            return v_SupportLevelOptionList;
        }
        set {
            v_SupportLevelOptionList = value;
        }
    }
    public static String selectedLicense1 {get; set;}
 
    // all License(s) belongs to the user
    @TestVisible
    private transient List<QS_EntitlementWrapper> v_resultsLicenses;
    public List<QS_EntitlementWrapper> resultsLicenses {
        get {
            system.debug('resultsLicenses get start');
            system.debug('resultsLicensesNeedsUpdate: ' + resultsLicensesNeedsUpdate);
            system.debug('v_resultsLicenses: ' + v_resultsLicenses);
            licensesLimit = QS_License__c.getInstance().QS_LicenseQueryLimit__c != null ? (Integer)QS_License__c.getInstance().QS_LicenseQueryLimit__c : 1000;
            if(resultsLicensesNeedsUpdate || v_resultsLicenses == null) {
                List<QS_EntitlementWrapper> tempList = new List<QS_EntitlementWrapper>();
                boolean atLeastOneLicenseWasAdded = false;
                for (Entitlement lic : resultsOriginalLicenses) {
                    if((lic.Status=='Active' || lic.Status=='Inactive') && tempList.size() < licensesLimit) {
                        tempList.add(new QS_EntitlementWrapper(lic));
                        atLeastOneLicenseWasAdded = true;
                    }
                }
                v_resultsLicenses = tempList;
                if(atLeastOneLicenseWasAdded)
                    resultsLicensesNeedsUpdate = false;
            }
            
            if(v_resultsLicenses.size() > 0){
                System.debug('v_resultsLicenses.Size(): ' + v_resultsLicenses.Size());
                licensesConsumed = v_resultsLicenses.size();
            }
            return v_resultsLicenses;
        }
        set {
            v_resultsLicenses = value;
        }
    }
    @TestVisible
    private transient List<QS_EntitlementWrapper> v_resultsExpiredLicenses;
    public List<QS_EntitlementWrapper> resultsExpiredLicenses {   
        get {
            
            Integer licensesAvalible = Test.isRunningTest() ? 500 : licensesLimit - licensesConsumed;
            if(resultsExpiredLicensesNeedsUpdate || v_resultsExpiredLicenses == null) {
                if(licensesAvalible > 0){
                    System.debug('debug_licensesLimit ' + licensesLimit);
                    System.debug('debug_licensesConsumed ' + licensesConsumed);
                    v_resultsExpiredLicenses = new List<QS_EntitlementWrapper>();
                    for (Entitlement lic : resultsOriginalLicenses) {
                        if(lic.Status=='Expired' && v_resultsExpiredLicenses.size() < licensesAvalible) {
                            v_resultsExpiredLicenses.add(new QS_EntitlementWrapper(lic));
                        }

                    } 
                    resultsExpiredLicensesNeedsUpdate = false;
                    if(!v_resultsExpiredLicenses.isEmpty()){
                        System.debug('v_resultsExpiredLicenses.Size(): ' + v_resultsExpiredLicenses.Size());
                    } 
                }                  
            }
            return v_resultsExpiredLicenses;
        }
        set {
           v_resultsExpiredLicenses = value; 
        }
    }

    @TestVisible
    // all License(s) belongs to the user
    private transient List<Entitlement> v_resultsOriginalLicenses;
    public List<Entitlement> resultsOriginalLicenses {
        get {
            system.debug('resultsOriginalLicenses get start');
            system.debug('selectedAccountName: ' + selectedAccountName);
            system.debug('selectedLicense1 : ' + selectedLicense1 );
            system.debug('selectedProduct : ' + selectedProduct );
            system.debug('selectedSupportLevel : ' + selectedSupportLevel );
            system.debug('selectedSupportingPartner : ' + selectedSupportingPartner );
            system.debug('selectedPartnerName : ' + selectedPartnerName );
            system.debug('addednew : ' + v_resultsOriginalLicenses ); 
            boolean atLeastOneLicenseWasAdded = false;
            if(selectedAccountNameHasChanged || v_resultsOriginalLicenses == null) {
                if(!IsPartner) {
                   selectedAccountName = accountId;
                }
                if (selectedAccountName != null && selectedAccountName != ''){
                    system.debug('inside if : ');                
                
                    String querylicenses = '';  
                    List<Entitlement> licenses = new List<Entitlement>(); 
                    String recordtype = 'Obsolete'; 
                    //  need to add RecordType.Name != 'Obsolete'
        
                    querylicenses = 'select Id, License_Reference__c, Name, Status, Description__c,  Support_Provided_By_Id__c, Account.Name, Account.Id, License_Account_NetSuite_Id__c, StartDate, EndDate, Support_Level__c, SlaProcess.Description, SlaProcess.Name, Entitlement_Check__c , Legacy_License_Key__c,License_Netsuite_Internal_Id__c'
                            + ' From Entitlement';
                    querylicenses += ' WHERE Account.Id = :selectedAccountName and (EndDate >= TODAY or Entitlement_Check__c = true) and License_Netsuite_Internal_Id__c !=null ';
                    querylicenses += ' and (Account_License__r.Support_Provided_By__c =:loggedInUserAccountId or Account.Id =: loggedInUserAccountId)';


                    if (selectedLicense1 != null && selectedLicense1 != '' && selectedLicense1 != 'All'){
                        querylicenses += ' and Id = :selectedLicense1 ';
                    }
                    if (selectedSupportLevel != null && selectedSupportLevel != ''){
                        querylicenses += ' and SlaProcess.Name = :selectedSupportLevel ';
                    }
                    if (selectedProduct != null && selectedProduct != '' && selectedProduct != 'All'){
                        List<String> myproductlevel = new List<String>();
                        for(QS_ProductLevel_Products__c setting : QS_ProductLevel_Products__c.getAll().values()){
                            if(setting.Product__c == selectedProduct) {
                                myproductlevel.add(setting.Product_Level__c);
                            }
                        }
                        system.debug('myproductlevel'+myproductlevel);
                        querylicenses += ' and Account_License__r.Product_Level__c in :myproductlevel ';
                    }
                    if (selectedPartnerName != null && selectedPartnerName != ''){
                        querylicenses += ' and Account_License__r.Support_Provided_By__c = :selectedPartnerName';
                    }

                    querylicenses += ' Order by StartDate desc';
                    //, Name, Account.Name, Status';
                    system.debug('loggedInUserAccountId:' + loggedInUserAccountId);
                    system.debug('querylicenses:' + querylicenses);
                    licenses = (List<Entitlement>) Database.query(querylicenses);
                    system.debug('licenses : ' + licenses );                 
                   
                    if (licenses != null && licenses.size() > 0){
                        v_resultsOriginalLicenses = licenses;
                        atLeastOneLicenseWasAdded = true;
                    } else
                        v_resultsOriginalLicenses = new List<Entitlement>();

                }
                else if (v_resultsOriginalLicenses == null){
                    v_resultsOriginalLicenses = new List<Entitlement>();
                }
                if(atLeastOneLicenseWasAdded) {
                    selectedAccountNameHasChanged = false;
                    //resultsExpiredLicensesNeedsUpdate = true;
                    //resultsLicensesNeedsUpdate = true;
                }
                
            }
            system.debug('v_resultsOriginalLicenses.Size(): ' + v_resultsOriginalLicenses.Size());
            system.debug('resultsOriginalLicenses get end');
            
            return v_resultsOriginalLicenses;
        }
        set {
            v_resultsOriginalLicenses = value;
        }
    }

    // accountID of existing user
    private Id accountId;

    // contact ID
    private Id contactId;

    private boolean isPartnerAccount;

    @TestVisible
    // Checksum for LEF download
    private integer Chk;

    // boolean to toggle of displaying pop up dialog
    public boolean displayPopup {get; set;}    
    
    // Set to true when button"Save" is clicked. Used to display saved message.
    public boolean savedChanges {get; set;}     

    // store the string to display in popo up dialog
    private String letterBody;

    private String licenseNamesReference;

    //Which list of licenses is selected, Active or Expired
    private string selectedLicenseList = 'Active';

    public boolean selectedAccountNameHasChanged {get; set;}

    

    private transient User v_objUser;
    public User objUser {
        get {
            if(v_objUser == null)
                v_objUser = [Select id, name, accountid, Contact.Account.ispartner From User  where Id = :userinfo.getUserId()  LIMIT 1];
            return v_objUser;
        }
        set {
            v_objUser = value;
        }
        
    }
    private transient boolean v_isPartner;
    public boolean IsPartner {
        get {
            if(v_isPartner == null)
                v_isPartner = (objUser.Contact.Account.ispartner);
            return v_isPartner;
        }
        set {
            v_isPartner = value;
        }
    }

    private transient List<Account> v_accountList;
    public List<Account> accountList {
        get {
            if(v_accountList == null) {
                if(!IsPartner) {
                    Id accountId = objUser.AccountId;
                    v_accountList = [Select Id, Name, RecordtypeId, RecordType.Name  From Account 
                                    Where Id = :accountId LIMIT 1];
                } else {
                    Id accountId = objUser.AccountId;
                    List<Account> endUserAccounts = [select Id, Name, RecordtypeId, RecordType.Name from account where id in (select AccountId from entitlement where account_license__r.Support_Provided_By__c = :accountId and AccountId != :accountId ) Order By Name, RecordType.Name];
                    List<Account> partnerAccount = [Select Id, Name, RecordtypeId, RecordType.Name
                                    From Account Where Id = :accountId LIMIT 1];
                    v_accountList = partnerAccount;
                    v_accountList.addAll(endUserAccounts);
                }
            }
            return v_accountList;
        }
        set {
            v_accountList = value;
        }
    }
    public List<SelectOption> accountOptionListFiltered {get; set;}
    public String AccountFilterText {get; set;}

    private transient List<SelectOption> v_accountOptionList;
    public List<SelectOption> accountOptionList {   
        get {
            if(v_accountOptionList == null){
                       
                v_accountOptionList = new list<SelectOption>();
                for(Account acc : accountList) {
                    v_accountOptionList.add(new SelectOption(acc.Id, acc.Name));
                }
            }
            
            return v_accountOptionList;
        }
        set {
            v_accountOptionList = value;
        }
    }
    public integer accountListSize {
        get {
            return accountList.Size();
        }
        set {
        }
    }

    public boolean DSEAccess {get; set;}
    public String DSEName {get; set;}

    /**
     * CR# 90369 Param string names which will be replaced in actual email template
     */
    public static String PARAM_KEYWORD_FORMAT = '[TAG_KEYWORD {0}]';
    public enum TAG_KEYWORD {ACCOUNT_OWNER_NAME, REQ_USER_NAME, ACCOUNT_NAME, WARNING_MESSAGE, FOOTER_IMAGE}
    /**
     * CR# 90369 Notification parameters
     */
    private Map<String, String> notificationParams = new Map<String, String>();
    public static Integer ALERT_ONE_NUM_DAYS = 7;
    public static Integer ALERT_TWO_NUM_DAYS = 14;
    public static Integer ALERT_ONE_REQ_LIMIT = 10;
    public static Integer ALERT_TWO_REQ_LIMIT = 5;
    public static String ALERT_EMAIL_TEMPLATE_NAME = 'License_Control_Number_Requests_Warning';
    public static String QS_DATASOURCE_REC_TYPE = 'Request Control Number';
    private static String IMAGE_FOOTER_NAME = 'Qlik_Email_Footer';
    private static final String SERVLET_PART_URL = '/servlet/servlet.ImageServer?oid=';
    private static final String THIRD_PART_URL = '&id=';
    public static final String QLIK_BASE_URL = 'https://qlik.my.salesforce.com/';



    public QS_LicenseController(){
        IsSandbox = [select Id, IsSandbox from Organization limit 1].IsSandbox;
        selectedAccountNameHasChanged = false;
        resultsLicensesNeedsUpdate = true;
        resultsExpiredLicensesNeedsUpdate = true;
        AccountFilterText = '';
        LicenseFilterText = '';
        PartnerFilterText = '';
        Chk = 4711;
        displayPopup = false;
        selectedLicenseList = 'Active';
        DownloadLEFSelected = false;
        system.debug('inside controller');
        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        CoveoFilterExpression  = partnerPortalURLs.Coveo_Filter_Expression__c;   
        Id licenseId = (Id)ApexPages.CurrentPage().getparameters().get('LicId');
        userIpAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        if (licenseId != null) {
            try {
                myLicense = [select id, Name, Status, License_Reference__c, StartDate, EndDate, Account.Name,
                Designated_Support_Engineer__c, Responsible_Partner_Name__c, Description__c, BusinessHours.Name,
                Time_Limit__c, No_of_Servers_in_Cluster__c, No_of_Documents__c, Special_Edition__c, SlaProcess.Name, License_Netsuite_Internal_Id__c ,
                Document_CAL__c, Named_User_CAL__c, Session_CAL__c, Usage_CAL__c, Dynamic_update_RTS__c, Tokens__c, License_Account_NetSuite_Id__c , Support_Provided_By_Id__c,
                Web_Parts__c, Workbench__c , Legacy_License_Key__c, Account_License__r.Special_Edition__c, Subscription_Start_Date__c, Subscription_End_Date__c, Support_Provided_By_Text__c from Entitlement where id=:licenseId]; 
                myEnv = [select id, Name from Environment__c where Product_License__c=:licenseId limit 1]; 
            } catch (Exception ex) {
                System.debug('QS_LicenseController error: ' + ex.getMessage());
            }
        }       

        User userRec = [select Contact.Id, Contact.AccountId, email from User where Id = :UserInfo.getUserId() limit 1];
        accountId = userRec.Contact.AccountId;
        contactId = userRec.Contact.Id;
        /*ct = [select id, AccountId, Diff_Email_Domains__c,Account.ownerId, email, Multiple_Control_No_Req_Across_Weeks__c, Multiple_Control_No_Requests_In_A_Week__c 
                    from contact where id = :contactId]; */        
        system.debug('IsPartner: ' + IsPartner);
        if(!IsPartner) {
            selectedAccountName = accountId;
        }
        system.debug('selectedAccountName: ' + selectedAccountName);
        FilterAccounts();
        FilterPartners();
        system.debug('selectedAccountName: ' + selectedAccountName);
        
    }

    public void FilterAccounts() {
        system.debug('FilterAccounts start');
        
        Id contactId, accId = null, Acc;
        string Accounttype ='';
        User usr = [Select contactid from User where id =: Userinfo.getUserid()];
        if(usr !=null) {
            contactId = usr.contactId;
            List<Contact> contacts = new List<Contact>();
            contacts = [Select AccountId, Account_Type__c, DSE_Access__c, Designated_Support_Engineer__c from Contact where id =: contactId];
            if(contactId != null && contacts.Size() > 0) {
                Contact contacttype = contacts[0];
                accId = contacttype.AccountId;
                loggedInUserAccountId = accId;
                DSEAccess = contacttype.DSE_Access__c ;
                DSEName = contacttype.Designated_Support_Engineer__c ;
                if(contacttype.Account_Type__c != null)
                    Accounttype = contacttype.Account_Type__c ;    
            }
        }
                
        List<SelectOption> newFilteredList = new List<SelectOption>();
        string filterText = AccountFilterText.toLowerCase();

        if(accountOptionList != null)
            for(SelectOption so : accountOptionList)
                if(so.getLabel().toLowerCase().contains(filterText))
                    newFilteredList.Add(so);

        accountOptionListFiltered = new List<SelectOption>();
        
        if(newFilteredList.size() > 100) {
            accountOptionListFiltered.add(new SelectOption('','('+newFilteredList.Size()+')Too many accounts, please use the filter above ('+newFilteredList.Size()+')'));        
        }
        else if(newFilteredList.size() > 0) {
            //if(newFilteredList.size() > 1)
                accountOptionListFiltered.add(new SelectOption('','('+newFilteredList.Size()+')' + ' | Accounts Found'));
            accountOptionListFiltered.addAll(newFilteredList);
        }
        else
            accountOptionListFiltered.add(new SelectOption('','No accounts found, please use the filter to find an account'));        

        if(accountOptionListFiltered.size() > 0 && isPartner)
            selectedAccountName = accountOptionListFiltered[0].getValue();
        else if(!isPartner)
            selectedAccountName = accountId;

        system.debug('loggedInUserAccountId: ' + loggedInUserAccountId);

        //FilterLicenses();
        system.debug('FilterAccounts end');
    }
   
    @TestVisible
    private void SimpleCheck16_impl (String Buf) {
        for (integer i = 0; i < Buf.length(); i ++) {
            Chk *= 2;
            if (Chk >= 65536) {
                Chk -= 65535;
            }
            Chk ^= Buf.codePointAt(i);
        }
    }
  
    public void closePopup() {        
        displayPopup = false;    
    }
    
    public PageReference ControlNumberRequest() {
        return null;
    }      

    public void SaveLicense() {
        savedChanges = true;
        try {
            if(myLicense!=null)
                update myLicense;
            else
                savedChanges = false;
        }
        catch(DmlException ex) {
            ApexPages.addMessages(ex);
        }
    }

    /**
     * CR# 90369 This method is used to check if the request for control number is valid or not. Usually happens if user is not a contact on the license account
     */
    @RemoteAction
    public static Boolean checkReqNumberValidOrNot(String accountNSId, String contactId, String licenseSellThroughPartnerId){
        List<Contact> ct = new List<Contact>();
        system.debug('eeeeee'+accountNSId);
        system.debug('eeeeee1'+contactId);
        system.debug('eeeeee2'+licenseSellThroughPartnerId);
        ct = [select id, email, AccountId, Account.OwnerId from Contact where Account.INT_NetSuite_InternalID__c = :accountNSId and id = :contactId];//fix fo CHG0030333 Added contact to where clause
        if(String.isNotBlank(licenseSellThroughPartnerId) && ct.isEmpty())
            ct = [select id, email, AccountId, Account.OwnerId from Contact where AccountId = :licenseSellThroughPartnerId and id = :contactId];//fix fo CHG0030333 Added contact to where clause
        if(ct.size() > 0)
            return true;
        else
            return false;
    }

    /**
     * CR# 90369 This method is used to invoke boomi from salesforce and pass all the required parameters.
     *  The method also stores the request in the QS_DataCapture__c object.
     * After the request is raised based on the number of requests in the past 2 weeks a alert email is either sent or not.
     */
    @RemoteAction
    public static void invokeBoomiProcess(String orgId, String licenseIds, String accountNSId, String UserSFDCID, String UserEmail, String ipAddress, String methodName){
        String  sendSubsMessage = '<Record type="LicensedProduct" environment="' + orgId + '"><ID>' + licenseIds + '</ID><secondaryID type="Account">' +
                            accountNSId + '</secondaryID><secondaryID type="BOOmiEXecctrl">' + UserSFDCID + '</secondaryID><requesterEmail>' + UserEmail+ '</requesterEmail></Record>';
        BoomiUtil.callBoomiSync(UserSFDCID, methodName, sendSubsMessage);
        createRequestRecord(licenseIds, userinfo.getuserid(), system.now(), ipAddress);
        genExceptionEmails(userinfo.getuserid(), accountNSId);
    }

    
     /**
     * CR# 90369 This method has the code logic to check whether to send the alert emails to the account owner or not.
     */
    private static void genExceptionEmails(Id userId, String accountNSId){
        List<QS_DataCapture__c> qsDataCaptures = new List<QS_DataCapture__c>([select Id,Control_Number_Request_Time__c 
                                                            from QS_DataCapture__c where ownerid = :userId and (Control_Number_Request_Time__c > :system.now()-21)]);
        Integer firstWeekCt = 0, secWeekCt = 0;
        Set<Contact> conUpd = new Set<Contact>();
        for(QS_DataCapture__c qDtCap: qsDataCaptures){
            if(qDtCap.Control_Number_Request_Time__c.Date() <= Date.today() && (qDtCap.Control_Number_Request_Time__c.Date() > Date.Today() - ALERT_ONE_NUM_DAYS))
                firstWeekCt++;
            if(qDtCap.Control_Number_Request_Time__c.Date() <= (Date.today() - ALERT_ONE_NUM_DAYS) && (qDtCap.Control_Number_Request_Time__c.Date() > Date.Today() - ALERT_TWO_NUM_DAYS))
                secWeekCt++;
        }

        User userRec = [select ContactId, email,name from User where Id = :userId limit 1];
        Contact ct = [select id, email, AccountId, Account.OwnerId from Contact where id = :userRec.ContactId];
        Account acc = [select id,ownerid, name, owner.Name from account where INT_NetSuite_InternalID__c = :accountNSId limit 1];

        if(firstWeekCt > ALERT_ONE_REQ_LIMIT){
            sendEmails(createNotificationParams(userRec, ct, acc, Label.Request_Control_Number_Alert_1_Text), acc.ownerId);
        }if((secWeekCt > ALERT_TWO_REQ_LIMIT && firstWeekCt > ALERT_TWO_REQ_LIMIT) || (secWeekCt > 0 &&((secWeekCt + firstWeekCt) > ALERT_ONE_REQ_LIMIT))){
            sendEmails(createNotificationParams(userRec, ct, acc, Label.Request_Control_Number_Alert_1_Text2), acc.ownerId);
        }
    }

     /**
     * CR# 90369 This method has the code logic to format and send the email as per the count of requests and timeline.
     * Email template name:License_Control_Number_Requests_Warning
     */
    private static void sendEmails(Map<String, String> emailParams, String tObjectId){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<EmailTemplate> lTemplate = [select id,name,htmlvalue,body,subject,DeveloperName from EmailTemplate where DeveloperName = :ALERT_EMAIL_TEMPLATE_NAME];
        if(!lTemplate.isEmpty()){
            QTCustomSettings__c defSettings = QTCustomSettings__c.getValues('Default');
            String qlikNoreply = '';

            if (defSettings != null) {
                qlikNoreply = defSettings.QlikNoReplyEmailAddress__c;
                OrgWideEmailAddress[] orgwideaddress = [SELECT id FROM OrgwideEmailAddress
                WHERE Displayname = :qlikNoreply LIMIT 1];
                if (!orgwideaddress.isEmpty()) {
                    mail.setOrgWideEmailAddressId(orgwideaddress[0].id);
                }
            }

            mail.setTargetObjectId(tObjectId);
            mail.setSaveAsActivity(false);
            mail.setSubject(formatContentNameValuePairs(lTemplate[0].Subject,
                    emailParams));
            mail.setPlainTextBody(formatContentNameValuePairs(lTemplate[0].body,
                    emailParams));
            mail.setHtmlBody(formatContentNameValuePairs(lTemplate[0].HtmlValue,
                    emailParams));
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

    /**
     * CR# 90369 This method is used to format the custom email template License_Control_Number_Requests_Warning
     */
    public static String formatContentNameValuePairs(String sContent, Map<String, String> params) {
        if (sContent == null || sContent.length() == 0) return '';
        else {
            String emptyTags = '';
            for (String tm : params.keySet()) {
                try {
                    sContent = sContent.replace(tm, params.get(tm));
                }
                catch (System.NullPointerException ex) {
                    if (sContent.contains(tm)) emptyTags += tm + '\n';
                }
            }
            return sContent;
        }
    }

    /**
     * CR# 90369 This method is used to create the param values which are to be replaced in the email template
     */
    private static Map<String, String> createNotificationParams(User userRec, Contact ct, Account acc, String wMsg){
        Map<String, String> notParamVals = new Map<String, String>();
        // - Account owner Name
        notParamVals.put(String.format(PARAM_KEYWORD_FORMAT,
                new LIst<String>{TAG_KEYWORD.ACCOUNT_OWNER_NAME.name()}), String.isEmpty(acc.Owner.Name) ?
                '' : acc.Owner.Name);
        // - Requesting user Name
        notParamVals.put(String.format(PARAM_KEYWORD_FORMAT,
                new List<String>{TAG_KEYWORD.REQ_USER_NAME.name()}), String.isEmpty(userRec.name) ?
                '' : userRec.name);
        // - Account Name
        notParamVals.put(String.format(PARAM_KEYWORD_FORMAT,
                new List<String>{TAG_KEYWORD.ACCOUNT_NAME.name()}), String.isEmpty(acc.Name) ?
                '' : acc.Name);
        // - Alert Message
        notParamVals.put(String.format(PARAM_KEYWORD_FORMAT,
                new List<String>{TAG_KEYWORD.WARNING_MESSAGE.name()}), wMsg);
        // - Footer Image
        notParamVals.put(String.format(PARAM_KEYWORD_FORMAT,
                new List<String>{TAG_KEYWORD.FOOTER_IMAGE.name()}), getFooterImageLink());
        return notParamVals;

    }

    private static String getFooterImageLink() {
        String url = null;
        List<Document> docs = [SELECT Id, DeveloperName FROM Document WHERE DeveloperName = :IMAGE_FOOTER_NAME];

        if (Test.isRunningTest()) {
            docs = [SELECT Id, DeveloperName FROM Document WHERE DeveloperName = :IMAGE_FOOTER_NAME + '_TEST'];
        }
        if (docs.size() ==1) {
            Organization org = [SELECT Id, IsSandbox, InstanceName FROM Organization LIMIT 1];
            String baseUrl = QLIK_BASE_URL;
            if (org.IsSandbox) {
                baseUrl = 'https://qlik--' + UserInfo.getUserName().substringAfterLast('.') + '.' + org.InstanceName + '.my.salesforce.com/';
            }
            url = baseUrl + SERVLET_PART_URL + org.Id + THIRD_PART_URL + docs[0].Id;
        }
        return url;
    }

    /**
     * CR# 90369 This method is used to create the request record
     */
    private static void createRequestRecord(String licenseIds, Id userId, Datetime currentTime, String ipAddress){
        QS_DataCapture__c dtCapture = new QS_DataCapture__c(Licenses__c = licenseIds, Control_Number_Request_Time__c = currentTime, ip_address__c = ipAddress,
                RecordtypeId = Schema.SObjectType.QS_DataCapture__c.getRecordTypeInfosByName().get(QS_DATASOURCE_REC_TYPE).getRecordTypeId());
        insert dtCapture;
    }

    @RemoteAction
    public static PageReference navtoLicensePage(String vfLicenseNames){
        PageReference pageRef = Page.QS_License_DownloadLEF;
        String licenseNames = '';
        Integer i =0;
        if(String.isNotBlank(vfLicenseNames) && vfLicenseNames.contains(';')){
            List<String> licenses = vfLicenseNames.Split(';');
            for(String licenseName: licenses){
                if(i <= (licenses.size()-1) && String.isNotBlank(licenseNames))
                    licenseNames +=  '-regex-' + licenseName;
                else if(String.isBlank(licenseNames))
                    licenseNames = licenseName;
                i++;   
            }
        }else if(String.isNotBlank(vfLicenseNames))
            licenseNames = vfLicenseNames;
        pageRef.getParameters().put('licenseNames', licenseNames);
        return pageRef;     
    }

    public Boolean DownloadLEFSelected{get; set;}
    //Can't remove yet
    public PageReference openDownloadLEFPage() {     
        return null;
    }

    public PageReference SetSelectedLicenseListToActive() {
        selectedLicenseList = 'Active';
        return ApexPages.currentPage();
    }
    public PageReference SetSelectedLicenseListToExpired() {
        selectedLicenseList = 'Expired';
        return ApexPages.currentPage();
    }
    
    public String getLicenseNamesReference() {
        System.debug('%%%%%licenseNamesReference: '+licenseNamesReference);
        return licenseNamesReference;
    }
}