/****************************************************************
*
*  OpportunityExecDealsVisibilityHandler
*
*   This class will make sure Opportunity will populate License Amount in USD
*   and list of Competitors/list of Influencing Partners
*
*  2016-05-19 TJG : Migrated from the following trigger
                    Opportunity_ExecDealsVisibility.
*  2016-06-23 roman@4front Query for CurrencyType is replaced with CurrencyQueryHelper class where the query inside it.
*  2016-11-14 roman@4front Queries oppsCompInf and oppsInfPart are wraped in condition to prevent zero-result queries.
*  2017-06-22 MTM  QCW-2711 remove sharing option
*  2017-07-25 Rodion Vakulvoskyi QCW-2695
*  2018-12-05 FEH prevent multiple queries to related object in before insert and update 
*****************************************************************/
public class OpportunityExecDealsVisibilityHandler {

    
    private static List<Competitor_Information__c> competitorInfo; 
    private static List<Competitor_Information__c> getCompetitorInfo(Set<Id> oppIds) {
        if(competitorInfo == null) {
            competitorInfo  = [select Opportunity__c, Primary_Competitor__c from Competitor_Information__c where Opportunity__c in :oppIds];
        }
        return competitorInfo; 
    }

  	private static List<Influencing_Partner__c> influencingPartners; 
    private static List<Influencing_Partner__c> getInfluencingPartners(Set<Id> oppIds) {
        if(influencingPartners == null) {
          influencingPartners = [select Opportunity__c, Influencing_Partner_Account_Name__c from Influencing_Partner__c where Opportunity__c in :oppIds];
        }
        return influencingPartners; 
    }
    
    public static void reset(){
        competitorInfo = null;
        influencingPartners = null; 
    }
    
	// to be called on both events before Insert and before Update
	public static void handle(List<Opportunity> triggerNew, Boolean isInsert, Boolean isUpdate) {

        System.Debug('Starting OpportunityExecDealsVisibilityHandler');

        if ((isInsert && Semaphores.Opportunity_ExecDealsVisibility_Insert == false) || (isUpdate && Semaphores.Opportunity_ExecDealsVisibility_Update == false))
        {
            if (isInsert) {Semaphores.Opportunity_ExecDealsVisibility_Insert = true;}
            if (isUpdate) {Semaphores.Opportunity_ExecDealsVisibility_Update = true;}

            List<Opportunity> ToUpdate = triggerNew;
           
            List<Competitor_Information__c> oppsCompInf = new List<Competitor_Information__c>();
            List<Influencing_Partner__c> oppsInfPart = new List<Influencing_Partner__c>();
          
		    Set<Id> oppIds = new Set<Id>();
			for (Opportunity oppItem :ToUpdate) {
				oppIds.Add(oppItem.Id);
			}
            if(!oppIds.isEmpty()) {
				oppsCompInf = getCompetitorInfo(oppIds);
                oppsInfPart = getInfluencingPartners(oppIds);
            }
            for (Opportunity o:ToUpdate)
            { 
				if (o.Competitors__c > 0)
                {
                    String Competitors = '';
                    for (Competitor_Information__c comp : oppsCompInf)
                    {
                        if(comp.Opportunity__c == o.Id && comp.Primary_Competitor__c != null)
                            Competitors += comp.Primary_Competitor__c + ',';
                    }
                    if(Competitors!= '')
                        Competitors = Competitors.Substring(0,Competitors.length()-1);
                    o.Competitor_s__c = Competitors;

                }

                // Influencing Partner
                if (o.Influencing_Partners_Count__c > 0)
                {
                    String InfluencingPartners = '';
                    for (Influencing_Partner__c InfPartners : oppsInfPart)
                    {
                        if (InfPartners.Opportunity__c == o.Id && InfPartners.Influencing_Partner_Account_Name__c != null) {
							InfluencingPartners += InfPartners.Influencing_Partner_Account_Name__c + ',';
						}
                    }

                    if (InfluencingPartners != '') {
						InfluencingPartners = InfluencingPartners.Substring(0,InfluencingPartners.length()-1);
					}
                    o.Influencing_Partners__c = InfluencingPartners;
                }

            }
        }

        System.Debug('Finishing OpportunityExecDealsVisibilityHandler');
    }
}