@isTest
private class OpportunityQueryHelperTest {
	
	@isTest static void test_getOpportunitiesByIds() {
		QTTestUtils.GlobalSetUp();
        //QTTestUtils.SetupNSPriceBook('GBP');
        List<Id> oppIdList = new List<Id>();
        List<Opportunity> oppList = new List<Opportunity>();

        Id oppSalesQCCS_recType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id; // in QA '012260000000YTi'
        Profile p = [select id from profile where name='Standard User'];
        User objUser = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert objUser;
        System.assertNotEquals(null, objUser.Id);
        System.RunAs(QTTestUtils.createMockOperationsAdministrator()) {  
			
        //CCE  Adding an account with BillingCountryCode and Qliktech Company
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
	            Name = 'USA',
	            QlikTech_Company_Name__c = 'QlikTech Inc'            
	        );
	        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
	        insert QTComp;
			
	        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
	            
	        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId='01220000000DOFzAAO', Pending_Validation__c = false);
	        insert acc;
	          
	        // create the opportunity
	        Opportunity opp1 = new Opportunity(
	            name='Test Opp 1',
	            recordtypeid=oppSalesQCCS_recType,
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'GBP',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = acc.Id,
	            Finance_Approved__c = false        
	        );
	        insert opp1;
	        System.assertNotEquals(null, opp1.Id);
			// create the opportunity
	        Opportunity opp2 = new Opportunity(
	            name='Test Opp 2',
	            recordtypeid=oppSalesQCCS_recType,
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'USD',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = acc.Id       
	        );
	        insert opp2;
	        System.assertNotEquals(null, opp1.Id);

            Test.startTest();
            oppIdList.add(opp1.Id); 
            oppIdList.add(opp2.Id);  

            oppList = OpportunityQueryHelper.getOpportunitiesByIds(oppIdList);
            System.assertEquals(2, oppList.size());
       		System.assertEquals('GBP', oppList[0].CurrencyIsoCode);
       		System.assertEquals('USD', oppList[1].CurrencyIsoCode);
       		oppList.clear();
       		oppList = OpportunityQueryHelper.getOpportunitiesByIds(oppIdList, true);
       		System.assertEquals('GBP', oppList[0].CurrencyIsoCode);
       		System.assertEquals('USD', oppList[1].CurrencyIsoCode);

            Test.stopTest();
        }//RunAs
	}
	
	@isTest static void test_getOpportunityById() {
		QTTestUtils.GlobalSetUp();
		
        Id oppSalesQCCS_recType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id; // in QA '012260000000YTi'
        List<Opportunity> oppList = new List<Opportunity>();
        Profile p = [select id from profile where name='Standard User'];
        User objUser = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert objUser;
        System.assertNotEquals(null, objUser.Id);
        
        //CCE  Adding an account with BillingCountryCode and Qliktech Company
		System.RunAs(QTTestUtils.createMockOperationsAdministrator()) { 
			
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
	            Name = 'USA',
	            QlikTech_Company_Name__c = 'QlikTech Inc'            
	        );
	        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
	        insert QTComp;
			
	        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
	            
	        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId='01220000000DOFzAAO', Pending_Validation__c = false);
	        insert acc;
	        
	        // create the opportunity
	        Opportunity opp1 = new Opportunity(
	            name='Test Opp 1',
	            recordtypeid=oppSalesQCCS_recType,
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'GBP',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = acc.Id,
	            Finance_Approved__c = false        
	        );
	        insert opp1;
	        System.assertNotEquals(null, opp1.Id);

	        // create the opportunity
	        Opportunity opp2 = new Opportunity(
	            name='Test Opp 2',
	            recordtypeid=oppSalesQCCS_recType,
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'USD',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = acc.Id       
	        );

	        Test.startTest();  

            Opportunity testOpp1 = OpportunityQueryHelper.getOpportunityById(opp1.Id);
            System.assertNotEquals(null, testOpp1);
       		System.assertEquals('GBP', testOpp1.CurrencyIsoCode);

       		delete opp1;
       		Opportunity testOpp2 = OpportunityQueryHelper.getOpportunityById(opp2.Id);
            System.assertEquals(null, testOpp2);
            Test.stopTest();

	    }
	}
	
}