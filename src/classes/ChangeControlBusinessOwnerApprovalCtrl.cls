public class ChangeControlBusinessOwnerApprovalCtrl {
	ApexPages.StandardController stdController;
	private string varProcessInstanceWorkItemId;
	private boolean varVisible;
	private string varActorId;
	private string varErrorMessage;
	public ChangeControlBusinessOwnerApprovalCtrl(ApexPages.StandardController controller) {
		stdController = controller;
		Map<string,string> URLParameters = ApexPages.currentPage().getParameters();
		Visible = false;

		if(URLParameters.containsKey('id'))
		{
			string id = URLParameters.get('id');
			List<ProcessInstanceWorkItem> processInstanceWorkItems = [SELECT id, actorid from processinstanceworkitem where ProcessInstance.TargetObjectId = :id and ProcessInstance.Status = 'Pending' and ProcessInstance.IsDeleted = false];
			if(processInstanceWorkItems.size() > 0)
			{
				varProcessInstanceWorkItemId = processInstanceWorkItems[0].id;
				varActorId = processInstanceWorkItems[0].actorid;
				if(varActorId != UserInfo.getUserId())
				{
					List<User> users = [select name from user where id = :varActorId];
					if(users.size() > 0)
						ErrorMessage = 'You are not the current approver for the Change Control. Current approver is ' + users[0].name;
					else
						ErrorMessage = 'You are not the current approver for the Change Control.';
				}
				else
					visible = true;
			}
			else
				ErrorMessage = 'The record is not currenly in an approval process.';
		}
		else
			ErrorMessage = 'ID of the Change Control is invalid or not supplied.';
	}

	public string ProcessInstanceWorkItemId
	{
		get
		{
			return varProcessInstanceWorkItemId;
		}
		set
		{
			varProcessInstanceWorkItemId = value;
		}
	}
	public boolean Visible
	{
		get
		{
			return varVisible;
		}
		set
		{
			varVisible = value;
		}
	}
	public string ErrorMessage
	{
		get
		{
			return varErrorMessage;
		}
		set
		{
			varErrorMessage = value;
		}
	}

    public PageReference save() {
        PageReference pageRef = stdController.save();
		
		string approvalURL = '/p/process/ProcessInstanceWorkitemWizardStageManager?id=' + ProcessInstanceWorkItemId;
    	pageRef = new PageReference(approvalURL);
    	       
		return pageRef;
    }
}