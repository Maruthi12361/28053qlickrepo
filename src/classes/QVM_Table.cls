/*
@author: Anthony Victorio, Model Metrics
@date: 02/06/2012
@description: Table wrapper, allows for sorting and pagination

	2013-06-27	TJG CR 8221	https://eu1.salesforce.com/a0CD000000YITlq
		Modify the limit on QlikMarket Reponder Download list
*/
public class QVM_Table {
	
	//--------------------------------------------------------------------------
	// Constants
	private final Integer ROWS_TO_DISPLAY = Test.isRunningTest() ? 3 : 10;
	public Integer customRowsToDisplay { get; set; }

    public List<Column> columns { get; set; }
    public List<SObject> rows { get; private set; }
    private ApexPages.StandardSetController controller;
    public String additionalSort { get; set; }
    
    public class Column {
    	public String label { get; set; }
    	public String field { get; set; }
    	public Boolean allowSort { get; set; }
    	public String sortIndicator { get; set; }
    	
    	public Column(String label, String field, Boolean allowSort) {
    		this.label = label;
    		this.field = field;
    		this.allowSort = allowSort;
        }
    
	    public Column(String label) {
	    	this(label, null, false);
	    }
    }
    
    public QVM_Table(List<Column> columns, String initialSortField) {
    	this.columns = columns;
    	rows = new List<SObject>();
    	customRowsToDisplay = ROWS_TO_DISPLAY;
    	
    	sortField = previousSortField = initialSortField;
    	sortDirection = QVM.SOQL_DESC;
	}
	
	public void populate(String query) {
		if (!QVM.nullOrEmpty(query)) {
			
			// CR 8221 to cope with limit to
			string sLimitTo = '';
			integer iLimit = query.indexOf(' LIMIT ');
			if (iLimit >= 0) {
				sLimitTo = query.substring(iLimit);
				query = query.substring(0, iLimit);
			} 
			
			
			query += ' ORDER BY ' + sortField + ' ' + sortDirection;
			query += (QVM.SOQL_ASC == sortDirection) ? ' NULLS FIRST ' : ' NULLS LAST ';
			if (!QVM.nullOrEmpty(additionalSort)) {
				query += ',' + additionalSort;
			}
			// CR 8221
			if (iLimit >= 0) {
				query += sLimitTo;
			}
		} else {
			query = 'SELECT Id FROM Account LIMIT 0';
		}
		
		controller = new ApexPages.StandardSetController(Database.query(query));
		controller.setPageSize(customRowsToDisplay);
        rows = controller.getRecords();
    }
    
    public Boolean moreRecords() {
    	return !controller.getCompleteResult();
	}

    //--------------------------------------------------------------------------
    // Sorting
    private String previousSortField { get; set; }
    // if this is set twice to the same value, the sort order is reversed
    public String sortField { get;
    	set {
    		sortField = value;
    		for (QVM_Table.Column c : columns) {
    			c.sortIndicator = (value == c.field) ? 
               ((QVM.SOQL_ASC == sortDirection) ? QVM.SORT_UP : QVM.SORT_DOWN) : '';
            }
		}
	}
	
	public String sortDirection { get; 
		set {
			sortDirection = value;
			for (QVM_Table.Column c : columns) {
				c.sortIndicator = (sortField == c.field) ? 
				((QVM.SOQL_ASC == value) ? QVM.SORT_UP : QVM.SORT_DOWN) : '';
			}
		}
	}
	
	public void sort() {
		sortDirection = (previousSortField == sortField) ? QVM.SOQL_DESC : QVM.SOQL_ASC;
		previousSortField = (previousSortField == sortField) ? null : sortField;
	}
	
	//--------------------------------------------------------------------------
	// Pagination
	public Integer pg { get { return controller.getPageNumber(); }}
	public Integer pgCount { get { return rows.size(); }}
	public Integer allCount { get { return controller.getResultSize(); }}
	public Boolean hasNext { get { return controller.getHasNext(); }}
	public Boolean hasPrev { get { return controller.getHasPrevious(); }}
	public String pageOf {
		get {
			final Integer n = (pg - 1) * customRowsToDisplay;
			return String.format(Label.QVM_PageNtoNofN, new String[] {
				String.valueOf((allCount > 0) ? n+1 : n),
				String.valueOf(n + pgCount),
				String.valueOf(allCount)});
			}
	}
	
	public void firstPage() { controller.first(); rows = controller.getRecords(); }
	public void lastPage() { controller.last(); rows = controller.getRecords(); }
	public void prevPage() { controller.previous(); rows = controller.getRecords(); }
	public void nextPage() { controller.next(); rows = controller.getRecords(); }

}