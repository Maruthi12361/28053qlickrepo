/**     
 * File Name : Z_RenewalQuoteBuildAndSaveBatchable
 * Description : This is a batchable job to build and save the renewal quotes, triggered by Z_RenewalQuoteBatchable batchable class.  
 *               It calls the following methods in this order:
 *               1. start - returns QueryLocator to pass a list of renewal quotes created by Z_RenewalQuoteBatchable
 *               2. execute - calls the OOTB Zuora Quotes managed package's buildAndSave method to build and save the renewal quotes, 
 *                            that is to fill out the quote-specific details, such as quote amendments, quote rate plans, 
 *                            quote rate plan charges, etc.
 *               3. finish - terminates the job and kicks off the Z_RenewalQuoteRulesEngineBatchable batchable job
 * @author : Inki Hong
 * Modification Log =====================================================================
 * Ver     Date         Author         Modification
 * 1       05.05.2020   Inki Hong      Created new class (RTR-477)
 */

global with sharing class Z_RenewalQuoteBuildAndSaveBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts {
    
    private List<Id> quoteIds;

    private final String CLASS_NAME = 'Z_RenewalQuoteBuildAndSaveBatchable',
                         DEBUG_PREFIX = '> $ ' + this.CLASS_NAME;
    
    global Z_RenewalQuoteBuildAndSaveBatchable(List<Id> quoteIds) {
        this.quoteIds = quoteIds;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' at the job start:');
        for (Id quoteId : this.quoteIds) {
            System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' quote id = ' + quoteId);
        }

        String queryString = 'SELECT Id '
                           + 'FROM zqu__Quote__c '
                           + 'WHERE Id IN (\'' + String.join(this.quoteIds, '\',\'') + '\')';
        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext bc, List<zqu__Quote__c> quotes) {
        System.debug(LoggingLevel.DEBUG, this.DEBUG_PREFIX + ' at the job execute');
        for (zqu__Quote__c quote : quotes) {
            Id quoteId = quote.Id;
            zqu.Quote quoteInstance = zqu.Quote.getInstance(quoteId);
        
            try { 
                quoteInstance.buildAndSave(); 
            } catch (Exception e) { 
                System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' exception caught');
                return; 
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()]; 

        System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' status: ' + a.Status);
        System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' job id: ' + a.Id);
        
        for (Id quoteId : this.quoteIds) {
            System.debug(LoggingLevel.ERROR, this.DEBUG_PREFIX + ' quote id = ' + quoteId);
        }

        Database.executeBatch(new Z_RenewalQuoteRulesEngineBatchable(this.quoteIds), 1);
    }
}