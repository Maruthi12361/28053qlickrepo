/**
* Test class: PopulatePartnerDiscountDescriptQuoteTest.
* @description To test PopulatePartnerDiscountDescriptOnQuote class which is running from SBQQQuoteLineTriggerHandler.
*			   Field Partner_Discount_Description__c on Quote should be populated from related
*			   QuoteLines (Partner_Discount_Description__c formula field). Trigger recalculates value
*			   on every insert / update / delete.
*
* Changelog:
*	 2016-11-22 : Roman Dovbush (4front) : Initial development.
*	 2017-02-06 : RVA 					 : Changing CreateAcounts methods.
*    2017-08-01 : Aslam QCW-2934
*    2017-09-02 : Srinivasan PR - fix for query error	
*	 12-09-2017 : Linus Löfberg Q2CW-2953 setting quote recipient to conform with added validation conditions.
*	 13.02.2019 : Björn Andersson fixed test class error related to new duplicate rules on account and contact
*/


@isTest
private class PopulatePartnerDiscountDescriptQuoteTest {

	@testSetup static void testSetup() {
		QTTestUtils.GlobalSetUp();
		QTTestUtils.SetupNSPriceBook('USD');
		//QuoteTestHelper.createCustomSettings();

		Profile p = [select id from profile where name='System Administrator'];
		User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, isActive = true,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com');
		insert u;
		Id AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id;

        System.runAs(u) {
			Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
			testAccount.Name = 'Test Val Rule 1';
	        update testAccount;

			QlikTech_Company__c companyQT = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c];
	        Contact testContact = QTTestUtils.createMockContact(); // contact lastname = 'RDZTestLastName'
			Contact testContact2 = QTTestUtils.createMockContact(); // contact lastname = 'RDZTestLastName'
	        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
											where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
	        Account testPartnerAccount = QuoteTestHelper.createAccount(companyQT, rTypeAcc2, 'Partner'); // acc name = 'PartnerAcc'
	        testPartnerAccount.Partner_Margin__c = 12;
	        testPartnerAccount.Navision_Status__c = 'Partner';
			insert	testPartnerAccount;
			
			 /******* changes for qcw-2934 start *********/
             insert QuoteTestHelper.createPCS(testPartnerAccount);
             /******* changes for qcw-2934 end *********/
			 
			testContact.AccountId = testPartnerAccount.id;
			update testContact;

			testContact2.Lastname = 'TestLastName2';
            testContact2.AccountId = testAccount.Id;
            update testContact2;

	        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , true);
	        System.assertNotEquals(null, pr.Id);
	        System.assertEquals(true, pr.Generate_License__c);

	    }
	}

	@isTest static void testInsertUpdatePartnerDiscountDesc() {

		User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name like 'PartnerAcc%'];
        Contact testContact = [Select Id, Name From Contact Where Lastname like 'RDZTestLastName%'];
		Contact testContact2 = [Select Id, Name From Contact Where Lastname = 'TestLastName2'];
        Product2 pr = [Select Id, Name, Family, isActive, Generate_License__c From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];
        System.assertEquals('Licenses', pr.Family);
        System.assertEquals(true, pr.isActive);
        System.assertEquals(true, pr.Generate_License__c);

        //Product2 pr1 = [Select Id, Name From Product2 Where Name = 'QlikView Information Access Server' Limit 1];
        //Product2 pr2 = [Select Id, Name From Product2 Where Name = 'QlikView Local Server' Limit 1];
        //Product2 pr3 = [Select Id, Name From Product2 Where Name = 'QlikView Local Client' Limit 1];

   		System.runAs(u) {
			Test.startTest();
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
			quoteForTest.Quote_Recipient__c = testContact2.Id;
			insert quoteForTest;
            System.assertNotEquals(null, quoteForTest.Id);
			System.debug('Test quote: ' + quoteForTest);

			SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050, 20, '12345', null);
			System.assertNotEquals(null, quoteLine.Id);

   			// Generate_License__c should be true for this quote line
   			List<SBQQ__QuoteLine__c> quoteLineList = [Select Id, Partner_Discount_Description__c, Generate_License__c From SBQQ__QuoteLine__c Where SBQQ__Quote__c = :quoteForTest.Id];

   			// check if Partner_Discount_Description__c field was populated properly
   			quoteLine = [Select Id, Partner_Discount_Description__c, Generate_License__c From SBQQ__QuoteLine__c Where SBQQ__Quote__c = :quoteForTest.Id And Product_Name__c = 'QlikView Enterprise Edition Server'];
   			//System.assertEquals('Partner Discount 12%', quoteLine.Partner_Discount_Description__c);

   			quoteForTest = [Select Id, Partner_Discount_Description__c, (Select Id, SBQQ__Quote__c, Partner_Discount_Description__c, Generate_License__c From SBQQ__LineItems__r) From SBQQ__Quote__c Where Id = :quoteForTest.Id];
   			System.assertNotEquals(null, quoteForTest.Id);
   			//System.assertEquals('Partner Discount 12%', quoteForTest.Partner_Discount_Description__c);

   			quoteLine.Partner_Discount_from_Product__c = 10;
   			update quoteLine;
   			quoteLine = [Select Id, Partner_Discount_Description__c From SBQQ__QuoteLine__c Where Product_Name__c = 'QlikView Enterprise Edition Server'];
   			System.assertEquals('QlikView Enterprise Edition Server Discount 10%', quoteLine.Partner_Discount_Description__c);

   			SBQQQuoteLineTriggerHandler tempHandler = new SBQQQuoteLineTriggerHandler(true, 1);
   			tempHandler.OnBeforeDelete(new List<SBQQ__QuoteLine__c>{quoteLine}, new Map<Id, SBQQ__QuoteLine__c>{quoteLine.Id => quoteLine});
   			tempHandler.OnAfterDelete(new List<SBQQ__QuoteLine__c>{quoteLine}, new Map<Id, SBQQ__QuoteLine__c>{quoteLine.Id => quoteLine});
   			tempHandler.OnUndelete(new List<SBQQ__QuoteLine__c>{quoteLine});
   			Boolean tempVar = tempHandler.IsTriggerContext;
   			tempVar = tempHandler.IsVisualforcePageContext;
   			tempVar = tempHandler.IsWebServiceContext;
   			tempVar = tempHandler.IsExecuteAnonymousContext;
   			Test.stopTest();
		}
	}

	@isTest static void testDeleteQuoteLine() {

		User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rType = [Select id From Recordtype Where DeveloperName = 'Evaluation_Quote'];
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name like 'PartnerAcc%'];
        Contact testContact = [Select Id, Name From Contact Where Lastname like 'RDZTestLastName%'];
		Contact testContact2 = [Select Id, Name From Contact Where Lastname = 'TestLastName2'];
        Product2 pr = [Select Id, Name From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];

    	User me = [Select Id From User Where Id =:UserInfo.getUserId()];
   		System.runAs(me) {
			Test.startTest();
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
			quoteForTest.Quote_Recipient__c = testContact2.Id;
			insert quoteForTest;
            System.assertNotEquals(null, quoteForTest.Id);
			System.debug('Test quote: ' + quoteForTest);

			SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050,20, '12345', null);
   			Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', '12345', '23423', null);
			//SBQQ__QuoteLine__c quoteLine1 = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr1.Id, 'QlikView Information Access Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 8500,00, '12345', null);

   			quoteLine = [Select Id, Partner_Discount_Description__c From SBQQ__QuoteLine__c Where Id = :quoteLine.Id];
   			quoteForTest = [Select Id, Partner_Discount_Description__c, (Select Id, SBQQ__Quote__c, Partner_Discount_Description__c, Generate_License__c From SBQQ__LineItems__r) From SBQQ__Quote__c Where Id = :quoteForTest.Id];
   			//System.assertEquals('Partner Discount 12%', quoteLine.Partner_Discount_Description__c);

   			PopulatePartnerDiscountDescriptOnQuote.onAfterDelete(new Set<ID>{quoteLine.Id});

			Test.stopTest();

		}
	}
}