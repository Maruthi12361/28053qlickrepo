/*************************************************************
* Change Log:
* 20151022  RDZ     Changing api from 19 to 31
*
**************************************************************/
public class TViewContExt 
{
    //Variables
    private final String INTERNAL_TVIEW = 'Internal T-View';
    private final String CUSTOMER_TVIEW = 'Customer T-View';
    private final Case c;
    
    //Controller Constructor
    public TViewContExt(ApexPages.StandardController sc)
    {
        c = (Case)sc.getRecord();
        tViewType = ApexPages.currentPage().getParameters().get('tViewType');
        initPage();
    } 
    
    //Properties
    public String tViewType {get; set;} 
    public String tViewTitle {get; set;}    
    public CaseEvent[] caseEvents
    {
    	get
    	{
    		if(caseEvents == null) caseEvents = new CaseEvent[0];
    		return caseEvents;
    	}
    	private set;
    }

    //Methods
    public void initPage()
    {   
        try
        {       
            CaseModel tvm = new CaseModel(c);
            
            //Check if Case has valid Id
            if(tvm.hasValidId)
            {
            	caseEvents = CaseServices.getCaseTimeline(tvm, tViewType);
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This is not a valid case record!'));
                return;
            }
        }       
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' ' + e.getTypeName()));
            return;
        }
        //Check URL parameter and set the Page Title as 'Internal T-View' or 'Customer T-View'
        if(tViewType == 'internal')
            tViewTitle = INTERNAL_TVIEW;
        else if(tViewType == 'customer')
            tViewTitle = CUSTOMER_TVIEW;
    }
}