/****************************************************************
*
*  OppUpdateAccOwnerNameHandler
*
*   This class Populates the Acc_Owner_Name__c field on Opportunity
*   populates 'Game Changer Manager at Closed Won' field. CR# 7167
*	also Qonnect_Manager_Name_at_Closed_Won__c and Qonnect_Manager_at_close__c
*	
*
*  2016-05-20 TJG : Migrated from the following trigger OppUpdateAccOwnerName 
*  2016-07-19 NAD : Updated per CR 87308 - Add Acc Owner Name at Closed Won for Qlik Online Purchase Opportunities
*  2017-06-22 MTM  QCW-2711 remove sharing option
*  2017-09-21 Viktor Yaremenko @4front.se QCW-3747 - Sales Credit Information fields not populating upon Closed Won
*****************************************************************/
public class OppUpdateAccOwnerNameHandler {
    // to be called only on event before Update
    public static void handle(Map<Id, Opportunity> triggerOldMap, List<Opportunity> triggerNew) {
        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Starting with Semaphores.OppUpdateAccOwnerNameHasRun '
                                + Semaphores.OppUpdateAccOwnerNameHasRun); //Viktor@4front.se QCW-3747

        if (Semaphores.OppUpdateAccOwnerNameHasRun == false) {
            //Semaphores.OppUpdateAccOwnerNameHasRun = true;    2014-08-04 deferred to later time

            //Get a Set of Account Id's from the list of Opportunitys
            Set<Id> acctIDs = new Set<Id>();
            Set<Id> oppIds = new Set<Id>();
            Map<Id, Boolean> doNeeded = new Map<Id, Boolean>();
            // any accountId actually changed?
            Boolean accountIdChanged = false;
            //Qlik Online Purchase Opportunities per CR# 87308
            Boolean onlinePurchaseOpp = false;

            for (Opportunity opp : triggerNew) {
                if (opp.AccountId != null) acctIDs.add(opp.AccountId);
                if (opp.Sell_Through_Partner__c != null) acctIDs.add(opp.Sell_Through_Partner__c);
                oppIds.add(opp.Id);
                Opportunity oldOpp = triggerOldMap.get(opp.Id);
                if (opp.AccountId <> oldOpp.AccountId) {
                    accountIdChanged = true;
                }

                //CR# 87308 - Verify opportunity is Qlik Online Purchase Opportunities (Record Type)
                if (opp.RecordTypeId == '012D0000000KJRS') {
                    onlinePurchaseOpp = true;
                }
                System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: AccountId - ' + opp.AccountId
                                    + '; Old Stage - ' + oldOpp.stageName + '; New Stage -' + Opp.stageName);  //Viktor@4front.se QCW-3747
                if ((opp.AccountId != null) && (oldOpp.stageName != Opp.stageName) && (
                        (opp.StageName == 'Closed Won') || (opp.StageName == 'OEM - Closed Won')
                         || (opp.StageName == 'OEM Pre-Pay Closed Won') || (opp.StageName == 'Deal Split-closed Won')
                         || (opp.StageName == 'ER - Closed Won') || (opp.StageName == 'QSG Closed Won'))
                ) {
                    System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Add to update list - ' + opp.Id);
                    doNeeded.put(opp.Id, true);
                    Semaphores.OppUpdateAccOwnerNameHasRun = true;
                } else {
                    System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Remove from update list - ' + opp.Id);
                    doNeeded.put(opp.Id, false);
                }
            }

            //and create a Map of Id's and Accounts Owner Name based on our Set of Account Id's
            // Added User__r.Name for CR# 7167
            // Added Account_SSM fields for CR# 7593
            // Added onlinePurchaseOpp check per CR# 87308
            Map<Id, Account> acctMap;
            if (accountIdChanged || Semaphores.OppUpdateAccOwnerNameHasRun || onlinePurchaseOpp) {
                acctMap = new Map<Id, Account>(
                [
                        select Owner.Name,
                                OwnerId,
                                Game_Changer_Manager__r.Name,
                                Qonnect_Manager__r.Name,
                                Qonnect_Manager__r.Id,
                                Account_SSM__r.Name,
                                Account_SSM__C,
                                Account_SSM_New__r.Name,
                                Account_SSM_New__c,
                                Account_EIS_New__c,
                                Account_EIS_New__r.Name,
                                Account_ISER__r.Name,
                                Account_ISER__c
                        from Account
                        where Id in :acctIDs
                ]);
            }
            List<Influencing_Partner__c> allIPs;
            List<Influencing_Partner__c> oppIPs;
            Map<Id, RecordType> recordTypes;
            // Do SOQL query only if absolutely needed
            if (true == Semaphores.OppUpdateAccOwnerNameHasRun) {
                // retrieving all Influencing Partners
                //FIXME DUPLICATE SOQL
                allIPs = new List<Influencing_Partner__c>([Select Opportunity__c, Influencing_PSM_QM__r.Name, Influencing_PSM_QM__c, Opportunity_Origin__c From Influencing_Partner__c where Opportunity__c in :oppIds]);
                oppIPs = new List<Influencing_Partner__c>();
                System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Influence partner list - ' + allIPs); //Viktor@4front.se QCW-3747
                List<string> OEMRecordTypes = new List<string>{
                        'OEM_Order', 'OEM_Prepay', 'OEM_Recruitment_CCS'
                };
                recordTypes = new Map<Id, RecordType>([select Id, DeveloperName from RecordType where sObjectType = 'Opportunity' and DeveloperName in :OEMRecordTypes]);
            }
            // anything needs to be done?

            if (false == accountIdChanged && false == Semaphores.OppUpdateAccOwnerNameHasRun) {
                //CR# 87308 - Add Acc Owner Name at Closed Won for Qlik Online Purchase Opportunities
                for (Opportunity opp : triggerNew) {
                    if (opp.RecordTypeId == '012D0000000KJRS') {
                        opp.Acc_Owner_Name__c = acctMap.get(opp.AccountId).Owner.Name;
                    }
                }

                System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Nothing to do, exiting'); //Viktor@4front.se QCW-3747
                return;
            }

            //and update the Opportunity Acc_Owner_Name__c field with the Owner Name from Account but only if at 'Closed Won'
            for (Opportunity opp : triggerNew) {
                // Only fire when stage changes to a closed stage (not if the Opp is saved repeatedly - allow people to manually edit the fields when needed) CR# 7775
                ////Opportunity oldOpp = triggerOldMap.get(opp.Id);
                ////if ((opp.AccountId != null) && (oldOpp.stageName != Opp.stageName) && ((opp.StageName == 'Closed Won') || (opp.StageName == 'OEM - Closed Won') || (opp.StageName == 'OEM Pre-Pay Closed Won') || (opp.StageName == 'Deal Split-closed Won') || (opp.StageName == 'ER - Closed Won') || (opp.StageName == 'QSG Closed Won') ))
                ////{
                if (doNeeded.get(opp.Id)) {
                    System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Update opportunity - ' + opp.Id); //Viktor@4front.se QCW-3747
                    opp.Acc_Owner_Name__c = acctMap.get(opp.AccountId).Owner.Name;
                    opp.Acc_Owner_at_Closed_Won__c = acctMap.get(opp.AccountId).OwnerId; //Viktor@4front.se QCW-3747
                    opp.Game_Changer_Manager_at_Closed_Won__c = acctMap.get(opp.AccountId).Game_Changer_Manager__r.Name;  //CR# 7167

                    opp.Game_Changer_Manager_ID_at_Closed_Won__c = acctMap.get(opp.AccountId).Game_Changer_Manager__c;  //CR# 7332

                    //CR# 7593 Populate SSM at Closed Won and SSM at Closed Won ID
                    opp.SSM_at_Closed_Won__c = acctMap.get(opp.AccountId).Account_SSM__r.Name;
                    opp.SSM_at_Closed_Won_ID__c = acctMap.get(opp.AccountId).Account_SSM__c;

                    //CR# 7841 The "SSM at Closed Won" field should be auto-populated from the "Account SSM" (on the End User Account record)
                    //upon the point of opportunity being set to "Closed Won" in the Stage field
                    Account acc = acctMap.get(opp.AccountId);
                    opp.SSM_at_Closed_Won__c = acc.Account_SSM_New__r.Name;
                    opp.SSM_at_Closed_Won_ID__c = acc.Account_SSM_New__c;
                    opp.Active_EIS_on_Closed_Won__c = acc.Account_EIS_New__c;
                    opp.Active_EIS_on_Closed_Won_Name__c = acc.Account_EIS_New__r.Name;

                    if (acc.Account_ISER__c != null) {
                        opp.Account_ISER_at_Closed_Won__c = acc.Account_ISER__r.Name;
                        opp.Account_ISER_at_Closed_Won_ID__c = acc.Account_ISER__c;
                    }

                    // CR 7332 Add logic to OppUpdateAccOwnerName to populate Qonnect_Manager_Name_at_Closed_Won__c and Qonnect_Manager_at_close__c
                    //This logic in ActivePSM update
                    // CR# 7776 Now this logic is back :-) changed definition means it makes more sense back here
                    if (opp.Sell_Through_Partner__c != null) {
                        opp.Qonnect_Manager_at_close__c = acctMap.get(opp.Sell_Through_Partner__c).Qonnect_Manager__r.Id;
                        opp.Qonnect_Manager_Name_at_Closed_Won__c = acctMap.get(opp.Sell_Through_Partner__c).Qonnect_Manager__r.Name;
                    }

                    if (opp.AccountId != null && recordTypes.get(opp.RecordTypeId) != null) {
                        opp.Qonnect_Manager_at_close__c = acctMap.get(opp.AccountId).Qonnect_Manager__r.Id;
                        opp.Qonnect_Manager_Name_at_Closed_Won__c = acctMap.get(opp.AccountId).Qonnect_Manager__r.Name;
                    }


                    // filter out IPs for this opp
                    if (!oppIPs.IsEmpty()) {
                        oppIPs.clear();
                    }

                    for (Influencing_Partner__c anIP : allIPs) {
                        //CR# 36719 Removing check on Opportunity_Origin__c all influencers are counted now
                        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Check partner for opp - '
                                                + opp.Id + ' - ' + anIP.Opportunity__c); //Viktor@4front.se QCW-3747
                        if (opp.Id == anIP.Opportunity__c) {
                            oppIPs.add(anIP);
                        }
                    }
                    System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Partners count for opp - '
                                                        + opp.Id + ' - ' + oppIPs.size()); //Viktor@4front.se QCW-3747
                    if (oppIPs.size() > 0) {
                        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Influencing_PSM_QM_at_Closed_Won_1__c - '
                                                + oppIPs.get(0).Influencing_PSM_QM__r.Name); //Viktor@4front.se QCW-3747
                        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Influencing_PSM_QM_at_Closed_Won_ID_1__c - '
                                                + oppIPs.get(0).Influencing_PSM_QM__c); //Viktor@4front.se QCW-3747
                        opp.Influencing_PSM_QM_at_Closed_Won_1__c = oppIPs.get(0).Influencing_PSM_QM__r.Name;
                        opp.Influencing_PSM_QM_at_Closed_Won_ID_1__c = oppIPs.get(0).Influencing_PSM_QM__c;
                    }

                    if (oppIPs.size() > 1) {
                        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Influencing_PSM_QM_at_Closed_Won_2__c - '
                                            + oppIPs.get(1).Influencing_PSM_QM__r.Name); //Viktor@4front.se QCW-3747
                        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Influencing_PSM_QM_at_Closed_Won_ID_2__c - '
                                            + oppIPs.get(1).Influencing_PSM_QM__c); //Viktor@4front.se QCW-3747
                        opp.Influencing_PSM_QM_at_Closed_Won_2__c = oppIPs.get(1).Influencing_PSM_QM__r.Name;
                        opp.Influencing_PSM_QM_at_Closed_Won_ID_2__c = oppIPs.get(1).Influencing_PSM_QM__c;
                    }
                    if (oppIPs.size() > 2) {
                        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Influencing_PSM_QM_at_Closed_Won_3__c - '
                                            + oppIPs.get(2).Influencing_PSM_QM__r.Name); //Viktor@4front.se QCW-3747
                        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Influencing_PSM_QM_at_Closed_Won_ID_3__c - '
                                            + oppIPs.get(2).Influencing_PSM_QM__c); //Viktor@4front.se QCW-3747
                        opp.Influencing_PSM_QM_at_Closed_Won_3__c = oppIPs.get(2).Influencing_PSM_QM__r.Name;
                        opp.Influencing_PSM_QM_at_Closed_Won_ID_3__c = oppIPs.get(2).Influencing_PSM_QM__c;
                    }
                }
                if (opp.AccountId != null) {
                    //CR# 11205 make sure account exists
                    Account acc1 = acctMap.get(opp.AccountId);
                    if (null != acc1) {
                        opp.Account_Owner__c = acc1.Owner.Name;
                    }
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, 'OppUpdateAccOwnerNameHandler: Finishing');

    }
}