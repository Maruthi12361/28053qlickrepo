// 	2013-01-08		SAN		Adding changes for partner paying by invoice						

public class efRegisterGroupController 
{

    public efEventWrapper event { get; set; }
    public List<ContactGroupRegistrant> Contacts { get; set; }
    private string AccountId = null;
    private string ProfileName = null;
    //public Contact AttendeeContact { get; set; }
    public Boolean SecondRun { get; set; } 
    //public efContactValidator Validator { get; set; }

    public efRegisterGroupController()
    {
        try {
            initialize();
        } catch (Exception e) {}        
    }
    
    private void initialize()
    {
        String eventId = ApexPages.currentPage().getParameters().get('eventId');
        if (eventId==null || eventId.length()==0) return;

        List<efEventWrapper> eventList = efEventWrapper.fetchMyEvents(null,null,eventId);
        if (eventList.size() != 1)
        { 
            return; 
        }   
        event = eventList[0];

        if (AccountId == null)
        {
            User u = [select Contact.AccountId, Profile.Name from User where Id = :UserInfo.getUserId()];
            AccountId = u.Contact.AccountId;
            ProfileName = u.Profile.Name;
            
            if (AccountId == null)
            {
                return;
            }           
        }              
        if (Contacts == null)
        {
            Contacts = new List<ContactGroupRegistrant>();
            for (Contact C : [select Id, Name, (select Id,Name,Status__c from Registrations1__r where Event__c = :event.eventId and (Status__c='Registered' or Status__c='Saved for Later' or Status__c='Pending')) from Contact where AccountId = :AccountId LIMIT 1000])
            {
                ContactGroupRegistrant CGR = new ContactGroupRegistrant();
                CGR.ContactId = C.Id;
                CGR.Name = C.Name;
                if (C.Registrations1__r.size() > 0)
                {
                    CGR.RegistrationId = C.Registrations1__r[0].Id;
                    CGR.IsRegistered = (C.Registrations1__r[0].Status__c == 'Registered' || C.Registrations1__r[0].Status__c == 'Invoiced' ? true : false);
                }
                else
                {
                    CGR.RegistrationId = null;
                    CGR.IsRegistered = false;               
                }
                Contacts.Add(CGR);
            }
        }
        
        /*
        if (getCanCreateContact())
        {
	        AttendeeContact = new Contact();
	        AttendeeContact.AccountId = AccountId;
	        Validator = new efContactValidator(AttendeeContact);
        }
        */
    }

	/*
	public Boolean getCanCreateContact() 
	{
		Schema.DescribeSObjectResult Description = Contact.SObjectType.getDescribe();
		System.debug('Contact is editable: '+Description.isUpdateable());
		return Description.isCreateable();
	} 
	*/   

    public PageReference goRegister(){
		string ContactId = ApexPages.currentPage().getParameters().get('contactId');
		string userContactName;
        String registrationId;
         
        if (event.eventId==null || ContactId ==null){
        	return null;   	
        }
        
        for (ContactGroupRegistrant CGR : Contacts) {
        	if (CGR.ContactId == ContactId) 
        	{
        		if (CGR.RegistrationId != null)
        		{
        			registrationId = CGR.RegistrationId;        		
        		}
        		userContactName = CGR.Name;
        	}
        }
        
        if(registrationId==null){        	

        	if (!efUtility.checkContactBelongsToAccount(ContactId, AccountId))
        	{
        		return null;
        	}              
            
            List<Registration__c> listReg = [Select Id From Registration__c 
            								Where Event__c=:event.eventId And Registrant__c=:ContactId And Status__c=:efConstants.REG_STATUS_SAVED_FOR_LATER
            								Order By CreatedDate Desc];
            if (listReg.size()>0){
            	registrationId = listReg[0].Id;
            }else {
	            Registration__c reg = new Registration__c(Event__c=event.eventId);
	            reg.Portal_Attendee_Type__c = getAttendeeType();
	            reg.Registrant__c = ContactId;
	            reg.Account__c = AccountId;
	            reg.Status__c = efConstants.REG_STATUS_SAVED_FOR_LATER;
	            reg.Name = event.eventName+' - '+ userContactName;                
	            insert reg;
	            
	            registrationId = reg.Id;            	
            }
        } 
        
        List<Payment_Details__c> pList = [Select Id From Payment_Details__c Where RegistrationId__c=:registrationId Limit 1];
        if (pList==null || pList.size()==0){
   		  	Payment_Details__c p= new  Payment_Details__c(Amount__c = 0.0,
	                                                  		Transaction_Date__c=system.today(),
			                                               	Return_Code__c='-1', //For Pending
			                                               	CurrencyIsoCode='USD',
			                                               	RegistrationId__c=registrationId);
	    	insert p;
        }    
        
        PageReference pr = efAppNavigator.pr(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE);
        pr.getParameters().put('registrationId', registrationId);
        pr.getParameters().put('contactId', ContactId);
        return pr;

    }

    private String getAttendeeType(){
        if(ProfileName.contains('Eventforce Portal – Partner'))
            return 'Partner';
        else if(ProfileName.contains('Eventforce Portal – Employee'))
            return 'Employee';
        else if(ProfileName.contains('Eventforce Portal – Customer'))
            return 'Customer';
        else if(ProfileName.contains('Eventforce Portal – Non Customer'))
            return 'Prospect';
        else if(ProfileName.contains('Partner') || ProfileName.contains('PRM'))
            return 'Partner';
        else if(ProfileName.contains('Customer Portal'))
            return 'Customer';
        else 
            return 'Employee';      
    }    

    public PageReference goAttendeePortal()
    {       
        String registrationId = ApexPages.currentPage().getParameters().get('registrationId');
        if (registrationId==null || registrationId.length()==0)
        {
            System.debug('goAttendeePortal - No registrationId');
            return null;
        }
        System.debug('goAttendeePortal - registrationId - ' + registrationId);
        PageReference pr = efAppNavigator.pr(efAppNavigator.ATTENDEE_PORTAL_HOME);
        pr.getParameters().put('registrationId', registrationId);               
        return pr;  
    }

	/*        
    public PageReference goAddContact(){
        return null;    
    } 
    */

    public class ContactGroupRegistrant
    {
        public string ContactId { get; set; }
        public string Name { get; set; }
        public string RegistrationId { get; set; }
        public Boolean IsRegistered { get; set; }
    }
    
    /*
    public class efContactValidator
    {
        Contact Con;
        
        public efContactValidator(Contact C)
        {
            Con = C;        
        }
        
        public Boolean getIsValidFirstName(){
            return !efUtility.isNull(Con.firstName);
        }
        
        public Boolean getIsValidLastName(){
            return !efUtility.isNull(con.lastName);
        }   
        
        public Boolean getIsValidEmail(){
            return !efUtility.isNull(con.email);
        }       
        
        public Boolean IsValid()
        {
            return (getIsValidFirstName() && getIsValidLastName() && getIsValidEmail());    
        }
    }
    */
}