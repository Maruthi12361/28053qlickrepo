/*
ChangeLog
    2013-08-12  CCE Initial test class creation for CR# 8441.
					This is the test class for CasePopulateSupportEmailFields 
    2018-06-22 ext_vos: CHG0034223. Test for population of 2 more Case.Regional_Support_Manager emails.
*/
@isTest
private class Test_CasePopulateSupportEmailFields {

    static testMethod void CasePopulateSupportEmailFields() {
        System.debug('CasePopulateSupportEmailFields: Starting');
        
        //Create the Support Office and populate the Regional Support Manager and Team lead (1) fields
        User johnBrown = QTTestUtils.createMockUserForProfile('System Administrator');
        string jbemail = johnBrown.Email;
        System.debug('CasePopulateSupportEmailFields: jbemail='+jbemail);
        
        User bobbyBall = QTTestUtils.createMockUserForProfile('System Administrator');
        string bbemail = bobbyBall.Email;
        System.debug('CasePopulateSupportEmailFields: bbemail='+bbemail);
        
        Support_Office__c cancunSupportOffice = new Support_Office__c(
			Name = 'Cancun',
	        Regional_Support_Manager__c = johnBrown.Id,
            Regional_Support_Manager_2__c = bobbyBall.Id,
	        Team_lead_1__c = bobbyBall.Id
        );
		insert cancunSupportOffice;
		
		//Create the Account
		//Get record type
        RecordType EU_RecordType = [select Id, Name from RecordType where Name = 'End User Account'];        
        System.debug('CasePopulateSupportEmailFields: EU_RecordType='+EU_RecordType);
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('TestAccount', u );
        acc.RecordTypeId = EU_RecordType.Id;
        acc.Support_Office__c = cancunSupportOffice.Id;
        update acc;
        
        //Create the Case
        RecordType C_RecordType = [select Id, Name from RecordType where Name = 'QlikTech Master Support Record Type'];        
        System.debug('CasePopulateSupportEmailFields: C_RecordType='+C_RecordType);
        Case testCase = new Case(
        	AccountId = acc.Id,
        	RecordTypeId = C_RecordType.Id,
        	Status='New',
        	OwnerId = u.Id,
        	Origin='Customer Portal'
        );
        
        test.startTest();
        insert testCase;
        
        //Regional_Support_Manager_Email__c and Support_Office_Team_Lead_1_Email__c should be updated by the
        // trigger when we insert the Case.
        Case retCase = [select Id, Regional_Support_Manager_Email__c, Support_Office_Team_Lead_1_Email__c, Regional_Support_Manager_2_Email__c 
                        from Case where Id = :testCase.Id];   
        System.assertEquals(jbemail, retCase.Regional_Support_Manager_Email__c);	
        System.assertEquals(bbemail, retCase.Regional_Support_Manager_2_Email__c);  
		System.assertEquals(bbemail, retCase.Support_Office_Team_Lead_1_Email__c);	
				
        test.stopTest();
    }
}