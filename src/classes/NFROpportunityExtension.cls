/******************************************************

	Class: NFROpportunityExtension
	
	Initiator: MTM CR# 16147 New Opportunity Record Type for NFR requests 
	
	Changelog:
		2015-07-10	MTM		Created file	
		
******************************************************/
public without Sharing class NFROpportunityExtension {
    public NFROpportunityExtension(ApexPages.StandardSetController stdController) 
    {        
        tempOpp = (Opportunity)stdController.getRecord();
        tempOpp.RecordTypeId = NFRRecordType.Id;//'012c00000000hQV'; 
        tempOpp.Quote_Status__c = 'New';
    }
    public PageReference Save()
    {
        Opportunity opp = new Opportunity();
        try
        {
            string accId = ApexPages.currentPage().getParameters().get('Id');
            Account acc = [Select Id, Name from Account where Id =: accId];     
            
            System.debug(acc.Name + ' ' + accId);            
            opp.AccountId = accId;
            opp.Partner_Not_for_Resale_Opportunities__c = accId;
            opp.Name = acc.Name + ' ' + tempOpp.Revenue_Type__c  + ' ' + Date.today().format();        
            opp.StageName = 'Open NFR';
            opp.Quote_Status__c = tempOpp.Quote_Status__c;        
            opp.RecordTypeId = tempOpp.RecordTypeId;
            opp.Revenue_Type__c = tempOpp.Revenue_Type__c;
            opp.CloseDate = Date.today();//tempOpp.CloseDate;
            opp.Partner_Contact_for_NFR__c = tempOpp.Partner_Contact_for_NFR__c;
            opp.Type = 'Existing Customer';
            insert opp;
        } 
        catch (System.DmlException e) {
            ApexPages.addMessages(e);
            return null;
        }
        return new PageReference('/'+ opp.Id);
    }
    public PageReference Cancel()
    {
        Id accId = ApexPages.currentPage().getParameters().get('Id');
        return new PageReference('/' + accId);
    }   
    private Opportunity tempOpp;
    Static RecordType NFRRecordType = [select Id, Name from RecordType where Name = 'Partner Not For Resale'];
}