/**     * File Name:AccountAddressMappingHandler
        * Description : This handler class is used to set the account support office if billing country code is populated or
        *                billing country code is changed
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Sept 30th 2016 RamakrishnaKini     Added new trigger logic.
        1.1 Nov 17th 2016 RamakrishnaKini     Modified to use geographical segment in place of operating region.
        1.2     2017-09-04   AIN            Added support for Bangalore Support Office
        2018-04-18  ext_bad CHG0033720   Select 'noreply' from custom settings.
        29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta
*/
public with sharing class AccountSetSupportOfficeHandler {

    enum regCodes {APAC, AMERICAS, EMEA}
    enum supportOfficeLocations {Sydney, Tokyo, Lund, Raleigh, Bangalore}
    static Set<Id> countryCodeIds = new Set<Id>();
    static List<Account> lAccs = new List<Account>();
    static List<Id> nullSupportOffice = new List<String>();
    static Map<ID, Account> accNullSupportOfficeMap = new Map<ID, Account>();   

    public AccountSetSupportOfficeHandler() {
        
    }

    /*  @Description :This common method is used to update territory or billing address fields on account on before insert trigger context.
        @parameter inputList: Trigger.new Account list
    */
    public static void onBeforeInsert(List<Account> inputList) {
        for(Account acc: inputList){
            if (String.isBlank(acc.Support_Office__c) && String.isNotBlank(acc.Billing_Country_Code__c)) { 
                countryCodeIds.add(acc.Billing_Country_Code__c);
                lAccs.add(acc);
            }
        }
        if(!lAccs.isEmpty())
            updAccSupportOffice(lAccs);
        countryCodeIds.clear();
    }

    /*  @Description :This common method is used to update billing country code fields on account on before update trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap 
        @parameter oInputList: Trigger.old Account list  
    */
    public static void onBeforeUpdate(List<Account> inputList, Map<id, Account> inputMap, Map<id, Account> oInputMap) {
        for(Account acc: inputList){
            if ((String.isBlank(acc.Support_Office__c) && String.isNotBlank(acc.Billing_Country_Code__c)) || ( 
                    (acc.Billing_Country_Code__c != oInputMap.get(acc.Id).Billing_Country_Code__c) 
                        && String.isNotBlank(acc.Billing_Country_Code__c) && String.isBlank(acc.Support_Office__c))) { 
                countryCodeIds.add(acc.Billing_Country_Code__c);
                lAccs.add(acc);
            }
        }
        if(!lAccs.isEmpty())
            updAccSupportOffice(lAccs);
        countryCodeIds.clear();
    }

    /**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/
    
    /*  @Description :This method is used to fetch qliktech company details for accounts which have billing country populated
            and update the billing country code lookup
        @parameter inputList: Accounts whose billing country is populated
    */
    private static void updAccSupportOffice(List<Account> lAccounts){
        //Create a Map of Support office Names and Id's 
        Map<string, Id> soMap = new Map<string, Id>(); 
        for (Support_Office__c so : [select Id, Name from Support_Office__c]){ 
            soMap.put(so.Name, so.Id); 
        }

        //Create a Map of Country Codes and QlikTech Company Name 
        Map<Id, string> ccMap = new Map<Id, string>();
        Map<Id, string> ccNameMap = new Map<Id, string>();  
        Map<Id, string> ccRegionMap = new Map<Id, string>();  
        for (QlikTech_Company__c cc : [select Id, QlikTech_Company_Name__c, Name,QlikTech_Operating_Region__c,Country_Name__c,QlikTech_Geographic_Segment__c from QlikTech_Company__c where id in :countryCodeIds]){
            ccMap.put(cc.Id, cc.QlikTech_Geographic_Segment__c); 
            ccNameMap.put(cc.Id, cc.Name);
            ccRegionMap.put(cc.Id, cc.QlikTech_Operating_Region__c);
        }

        for (Account acc:lAccounts){
            if (String.isBlank(acc.Support_Office__c)){ 
                //the Support Office field is blank so get the QlikTech Company field 
                //so use the Name to lookup the ID (as the support office field is a lookup) 
                system.debug('kkkkk'+acc.Billing_Country_Code_Country__c);
                if (regCodes.APAC.name().equalsIgnoreCase(ccMap.get(acc.Billing_Country_Code__c)) && 'INDIA'.equalsIgnoreCase(ccRegionMap.get(acc.Billing_Country_Code__c))) { 
                    acc.Support_Office__c = soMap.get(supportOfficeLocations.Bangalore.name()); 
                }
                else if (regCodes.APAC.name().equalsIgnoreCase(ccMap.get(acc.Billing_Country_Code__c)) && !'JPN'.equalsIgnoreCase(ccNameMap.get(acc.Billing_Country_Code__c))) { 
                    acc.Support_Office__c = soMap.get(supportOfficeLocations.Sydney.name()); 
                } 
                else if (regCodes.APAC.name().equalsIgnoreCase(ccMap.get(acc.Billing_Country_Code__c)) && 'JPN'.equalsIgnoreCase(ccNameMap.get(acc.Billing_Country_Code__c))) { 
                    acc.Support_Office__c = soMap.get(supportOfficeLocations.Tokyo.name());  
                } else if(regCodes.AMERICAS.name().equalsIgnoreCase(ccMap.get(acc.Billing_Country_Code__c))) { 
                    acc.Support_Office__c = soMap.get(supportOfficeLocations.Raleigh.name()); 
                } else if(regCodes.EMEA.name().equalsIgnoreCase(ccMap.get(acc.Billing_Country_Code__c))) { 
                    acc.Support_Office__c = soMap.get(supportOfficeLocations.Lund.name()); 
                } else {
                    acc.Support_Office__c = null;
                    nullSupportOffice.add(acc.Id);
                    accNullSupportOfficeMap.put(acc.Id,acc);
                }
            }
        }
        if (!nullSupportOffice.isEmpty()) 
            sendErrorMails();
    }

    /*  @Description :This method is used to send error mails for accounts where no support office is set
    */
    @testVisible
    private static void sendErrorMails(){
        //Find out who to send the email to (there is a list of addresses in a QTCustomSetting)
        String soEmailDefault = (UserInfo.getOrganizationId().startsWith('00D20000000IGPX') ? 'mme@qlik.com' : 'mme@qlik.com.sandbox'); //initailise a default email address based on Live or sandbox Org
        //System.Debug('Account_SetSupportOffice: soEmailDefault = ' + soEmailDefault);
        QTCustomSettings__c qtcs = QTCustomSettings__c.getValues('Default');
        String nso = qtcs != null ? qtcs.null_Support_Office_Email__c : soEmailDefault;
        // Strings to hold the email addresses to which you are sending the email.
        if (nso != null) {  //Can't send an email if we have no addresses to send to
            list<string> toAddresses = nso.split(',');
            System.Debug('Account_SetSupportOffice: toAddresses = ' + toAddresses);

            boolean orgAllowedToSendEmails = true;
            try{
                Messaging.reserveSingleEmailCapacity(toAddresses.size()); //This changes depending on the number of recipients in our Custom Setting
            }
            catch (System.NoAccessException ex){
                system.debug('Org is not allowed to send email.');
                orgAllowedToSendEmails = false;
            }
            String noReplyAddress = '';

            if(QTCustomSettings__c.getInstance('Default') != null) {
                noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
            }
            if(Test.isRunningTest()) {
                noReplyAddress = 'no-reply@qlik.com'; 
            }

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setReplyTo(noReplyAddress);
            mail.setSenderDisplayName('Account_SetSupportOffice Trigger');
            mail.setSubject('Recently updated Accounts with null Support Office');
            mail.setBccSender(false);
            mail.setUseSignature(false);
            Account acc;
            // Specify the text content of the email.
            String soErrors ='';
            for(Id accId : nullSupportOffice) {
                //soErrors += str + ',' ;
                if(accNullSupportOfficeMap.containsKey(accId)){
                    acc = accNullSupportOfficeMap.get(accId);
                    //System.Debug('Account QT Company:' + acc.QlikTech_Company__c + 'Account_SetSupportOffice:'  + acc.Support_Office__c);
                    if(acc.QlikTech_Company__c !=null && acc.Support_Office__c == null && acc.Navision_Status__c != null){                   
                        soErrors += accId + ',' ;
                    } 
                }
            }
            if(soErrors !=''){
                soErrors = soErrors.removeEnd(',');
                mail.setPlainTextBody('Recently updated Accounts with null Support Office: ' + soErrors);
                if(orgAllowedToSendEmails)
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            } 
        }
    }



}