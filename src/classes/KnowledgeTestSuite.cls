/*
Name:  KnowledgeTestSuite.cls
Copyright © 2011  salesforce.com consulting
======================================================
======================================================
Purpose:
-------
Provide test case and code coverage for :
1. *AgentContributionArticleController class

TODO : Move data setup code to a helper class.
======================================================
======================================================
History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     Mark Cane&  2011-10-14  Initial development.
1.1     Madhav Kakani2014-05-10 Fluido Oy - Modified for CR#11349 
06.02.2017   RVA :   changing QT methods	
*/

@isTest
//@isTest(SeeAllData=true)
private class KnowledgeTestSuite {
    private static Account a = new Account();
    private static Case c = new Case();
    private static final String ARTICLE_TYPE = 'KnowledgeArticleVersion';
    private static final String NON_BREAKING_SPACE = '&nbsp;&nbsp;';

    static testMethod void AgentContributionArticleController() {
        setup();
        System.debug(c.Id);
        
        ApexPages.currentPage().getParameters().put('sourceId', c.Id); 
        ApexPages.currentPage().getParameters().put('sfdc.override', '1');      
        
        //aaron: modified line due to Martin Haagen's SF support response. Email 04/11/11
        //Diagnostic__kav ar = new Diagnostic__kav(SourceId = c.Id);
        
        Basic__kav ar = new Basic__kav();
        
        ApexPages.KnowledgeArticleVersionStandardController ctl = new ApexPages.KnowledgeArticleVersionStandardController(ar);
        System.debug(ctl.getSourceId());
        
        AgentContributionArticleController acac = new AgentContributionArticleController(ctl);
        
        //& check field mappings.
        System.assertEquals(ar.Title, c.Subject);
        System.assertEquals(ar.Description__c, c.Description);
        System.assertEquals(ar.Tags__c, c.Area__c);

        // Get the coverage for the Service Request type
        c.Type = 'Service Request';
        c.Service_Request_Type__c = 'Training';
        update c;

        ctl = new ApexPages.KnowledgeArticleVersionStandardController(ar);
        acac = new AgentContributionArticleController(ctl);  
    }
    
    static testMethod void testInvalidArticleType(){
        setup();
        
        ApexPages.currentPage().getParameters().put('sourceId', c.Id); 
        ApexPages.currentPage().getParameters().put('sfdc.override', '1');
        ApexPages.KnowledgeArticleVersionStandardController ctl = new ApexPages.KnowledgeArticleVersionStandardController(new KnowledgeArticle());
        
        AgentContributionArticleController acac = new AgentContributionArticleController(ctl);
        
        for(Apexpages.Message msg : Apexpages.getMessages()){
            System.assertEquals('This Article type is no longer in use. Please select Basic instead.', msg.getSummary());
        }  
    }
    
    static testMethod void testIncidentType(){
        setup();
        System.debug(c.Id);
        c.Type = 'Service Request';
        c.Service_Request_Type__c = 'Other';
        update c;
        
        ApexPages.currentPage().getParameters().put('sourceId', c.Id); 
        ApexPages.currentPage().getParameters().put('sfdc.override', '1');      
        
        Basic__kav ar = new Basic__kav();
        
        ApexPages.KnowledgeArticleVersionStandardController ctl = new ApexPages.KnowledgeArticleVersionStandardController(ar);
        System.debug(ctl.getSourceId());
        
        AgentContributionArticleController acac = new AgentContributionArticleController(ctl);
        
        System.debug('testIncidentType ar = ' + ar);
        System.debug('testIncidentType acac = ' + acac);
        //ctl.selectDataCategory(String categoryGroup, String category)
        
        List<Basic__DataCategorySelection> dcna = [SELECT Id, DataCategoryName, ParentId FROM Basic__DataCategorySelection WHERE ParentId= :c.Id];
        System.debug('testIncidentType dcna = ' + dcna);

        List<Basic__DataCategorySelection> dcnd = [SELECT Id, DataCategoryName, ParentId FROM Basic__DataCategorySelection WHERE ParentId= :ar.Id];
        System.debug('testIncidentType dcnd = ' + dcnd);


        Describedatacategorygroupresult[] results = getDescribeDataCategoryGroupResults();
        for (Describedatacategorygroupresult singleResult : results) {
            System.debug('testIncidentType singleResult = ' + singleResult);
            System.debug('testIncidentType singleResult.getName() = ' + singleResult.getName());

            //Getting the name of label
            System.debug('testIncidentType singleResult.getLabel() = ' + singleResult.getLabel());

            //Getting description
            System.debug('testIncidentType singleResult.getDescription() = ' + singleResult.getDescription());

            //Getting the sobject
            System.debug('testIncidentType singleResult.getSobject() = ' + singleResult.getSobject());
            //getNametting the sobject
            //System.debug('testIncidentType getCategoryStructure(singleResult.getName()) = ' + getCategoryStructure(singleResult.getName()));            

            Datacategorygroupsobjecttypepair pair = new Datacategorygroupsobjecttypepair();
            pair.setSobject(ARTICLE_TYPE);
            pair.setDataCategoryGroupName(singleResult.getName());
            Describedatacategorygroupstructureresult structureResult = Schema.describeDataCategoryGroupStructures(new Datacategorygroupsobjecttypepair[] { pair }, false)[0];
            System.debug('testIncidentType structureResult = ' + structureResult);
            
        }






/*
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        try {
            //Creating the list of sobjects to use for the describe
            //call
            List<String> objType = new List<String>();

            objType.add('KnowledgeArticleVersion');
            objType.add('Question');

            //Describe Call
            describeCategoryResult = Schema.describeDataCategoryGroups(objType);

            //Using the results and retrieving the information
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
                //Getting the name of the category
                System.debug('testIncidentType singleResult.getName() = ' + singleResult.getName());

                //Getting the name of label
                System.debug('testIncidentType singleResult.getLabel() = ' + singleResult.getLabel());

                //Getting description
                System.debug('testIncidentType singleResult.getDescription() = ' + singleResult.getDescription());

                //Getting the sobject
                System.debug('testIncidentType singleResult.getSobject() = ' + singleResult.getSobject());
            }         
        } catch(Exception e){
            System.debug('testIncidentType exception ' + e);
        }*/


        //& check field mappings.
        //System.assertEquals(ar.Title, c.Subject);
        //System.assertEquals(ar.Description__c, c.Description);
        //System.assertEquals(ar.Tags__c, c.Area__c);
    }
    
    private static Describedatacategorygroupresult[] getDescribeDataCategoryGroupResults() {
        String[] objTypes = new String[] {ARTICLE_TYPE};
        return Schema.describeDataCategoryGroups(objTypes);
    }

 /*   //Retrieves an ordered list of all Categories for the specified category group
    private static DataCategoryInfo[] getCategoryStructure(String categoryGroupName) {
        DataCategory root = getRootCategory(categoryGroupName);
        DataCategory[] yetToBeProcessed = root.getChildCategories();
        DataCategoryInfo[] allCategories = new DataCategoryInfo[] { processCategory(root, '') };
        getAllCategories(yetToBeProcessed, allCategories, NON_BREAKING_SPACE);
        return allCategories;
    }

    private static void getAllCategories(DataCategory[] yetToBeProcessed, DataCategoryInfo[] processed, String labelPrefix) {
        for (DataCategory category : yetToBeProcessed) {
            processed.add(processCategory(category, labelPrefix));
            getAllCategories(category.getChildCategories(), processed, labelPrefix + NON_BREAKING_SPACE);
        }
    }

    private static DataCategoryInfo processCategory(DataCategory category, String labelPrefix) {
        return new DataCategoryInfo(category.getName(), category.getLabel(), labelPrefix + category.getLabel());
    }*/


    
    private static void setup(){
        //Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
		/*
		Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs.id            
        );
        insert QTComp;*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        a.Name = 'Clearsight EU';
        a.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
        a.Billing_Country_Code__c = QTComp.Id;
        insert a;
            
        c.Subject = 'Lost password';
        c.Description = 'Lost my Qlikview password';
        c.AccountId = a.Id;
        c.Origin = 'Email';
        c.Status = 'New';
        c.Area__c = 'London';
        c.Type = 'Incident';
        c.Product__c = 'Desktop';
        insert c;
    }
}