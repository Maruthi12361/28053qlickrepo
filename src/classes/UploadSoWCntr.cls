/****************************************************************
*
*  UploadSoWCntr 
*  03.05.2017 : Rodion Vakulvsokyi : QCW-2228
*  27.06.2017 : Aslam Kamal : QCW-2228 Upated if condition
*  04.02.2019 : UIN : BFC-2092 New condition check
*****************************************************************/
public class UploadSoWCntr {
    public Opportunity opp {get; set;}
    public Boolean primaryQuoteStatus{set;get{
                            if(opp.SBQQ__PrimaryQuote__c!=null && opp.SBQQ__PrimaryQuote__r.SBQQ__Status__c == 'Approved') {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
    public Boolean oppSowStatus{set;get{
        if(opp.SOW_Status__c!=null && opp.SOW_Status__c == 'Finance Approved') {
                return true;
            } else {
                return false;
            }
        }
    }

    public ApexPages.StandardController controller;
    public UploadSoWCntr (ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()){
            stdController.addFields(new List<String>{'SBQQ__PrimaryQuote__c', 'SBQQ__PrimaryQuote__r.SBQQ__Status__c', 'AccountId', 'SOW_Status__c'});
        }
        opp = (Opportunity)stdController.getRecord();
    }
}