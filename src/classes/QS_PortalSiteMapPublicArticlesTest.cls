/* 
* Test Class for QS_PortalSiteMapPublicArticlesController.
*
* Change log:
* 05-02-2019    ext_vos     CHG0035417: created. 
* 2019-05-05    AIN         IT-1597 Created for Support Portal Redesign
* 2019-11-07    AIN         IT-2229 Articles now require an update to publish
* 2020-07-13    ext_cqb     IT-2939 disable automation which sets IsVisibleInPKB
*/
@isTest
public class QS_PortalSiteMapPublicArticlesTest {

    static testMethod void runController() {     
        List<QS_Partner_Portal_Urls__c> settings = [select Id, Support_Portal_Login_Url__c from QS_Partner_Portal_Urls__c];
        if (settings.isEmpty()) {
            QS_Partner_Portal_Urls__c setting = new QS_Partner_Portal_Urls__c(Support_Portal_Login_Url__c = 'https://test-qliksupport.force.com');
            insert setting;
        }
        Id kaIdB = createBasicArticle();   
        Id kaIdD = createDiagnosticArticle();

        QS_PortalSiteMapPublicArticlesController contr = new QS_PortalSiteMapPublicArticlesController();       
        System.assertEquals(2, contr.articles.size());
        System.assert(contr.externalUrl.contains('test-qliksupport'));
    }
    
    private static ID createBasicArticle() {
        Basic__kav basic = new Basic__kav(Title = 'test Basic article', UrlName = 'testURL' + System.now().millisecond(), 
                                            Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true, IsVisibleInPkb = true);
        insert basic;
        update basic;
        basic = [select Id, KnowledgeArticleId from Basic__kav where Id =: basic.Id];

        KbManagement.PublishingService.publishArticle(basic.KnowledgeArticleId, true);
        return basic.KnowledgeArticleId;
    }

    private static ID createDiagnosticArticle() {
        Diagnostic__kav article = new Diagnostic__kav(Title = 'test Diagnostic article', UrlName = 'testURL' + System.now().millisecond(), 
                                                        Summary = 'test summary', Language = 'en_US', IsVisibleInCsp = true, IsVisibleInPkb = true);
        insert article;
        update article;
        article = [select Id, KnowledgeArticleId from Diagnostic__kav where Id =: article.Id];

        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        return article.KnowledgeArticleId;
    }        
}