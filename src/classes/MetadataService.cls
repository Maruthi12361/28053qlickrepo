/**
 Changelog:
        2013-09-12  KMH     Initial version
                            
        
**/

public class MetadataService {
      
      
      public class Workflow extends Metadata {
        public String type = 'Workflow';
        public String fullName;
        public MetadataService.WorkflowAlert[] alerts;
        public MetadataService.WorkflowFieldUpdate[] fieldUpdates;
        public MetadataService.WorkflowKnowledgePublish[] knowledgePublishes;
        public MetadataService.WorkflowOutboundMessage[] outboundMessages;
        public MetadataService.WorkflowRule[] rules;
        public MetadataService.WorkflowSend[] send;
        public MetadataService.WorkflowTask[] tasks;
        private String[] type_att_info = new String[]{'xsi:type'};
        private String[] fullName_type_info = new String[]{'fullName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] alerts_type_info = new String[]{'alerts','http://soap.sforce.com/2006/04/metadata','WorkflowAlert','0','-1','false'};
        private String[] fieldUpdates_type_info = new String[]{'fieldUpdates','http://soap.sforce.com/2006/04/metadata','WorkflowFieldUpdate','0','-1','false'};
        private String[] knowledgePublishes_type_info = new String[]{'knowledgePublishes','http://soap.sforce.com/2006/04/metadata','WorkflowKnowledgePublish','0','-1','false'};
        private String[] outboundMessages_type_info = new String[]{'outboundMessages','http://soap.sforce.com/2006/04/metadata','WorkflowOutboundMessage','0','-1','false'};
        private String[] rules_type_info = new String[]{'rules','http://soap.sforce.com/2006/04/metadata','WorkflowRule','0','-1','false'};
        private String[] send_type_info = new String[]{'send','http://soap.sforce.com/2006/04/metadata','WorkflowSend','0','-1','false'};
        private String[] tasks_type_info = new String[]{'tasks','http://soap.sforce.com/2006/04/metadata','WorkflowTask','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'fullName','alerts','fieldUpdates','knowledgePublishes','outboundMessages','rules','send','tasks'};
    }
    
    public class WorkflowRule extends Metadata {
        public String type = 'WorkflowRule';
        public String fullName;
        public MetadataService.WorkflowActionReference[] actions;
        public Boolean active;
        public String booleanFilter;
        public MetadataService.FilterItem[] criteriaItems;
        public String description;
        public String formula;
        public String triggerType;
        public MetadataService.WorkflowTimeTrigger[] workflowTimeTriggers;
        private String[] type_att_info = new String[]{'xsi:type'};
        private String[] fullName_type_info = new String[]{'fullName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] actions_type_info = new String[]{'actions','http://soap.sforce.com/2006/04/metadata','WorkflowActionReference','0','-1','false'};
        private String[] active_type_info = new String[]{'active','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] booleanFilter_type_info = new String[]{'booleanFilter','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] criteriaItems_type_info = new String[]{'criteriaItems','http://soap.sforce.com/2006/04/metadata','FilterItem','0','-1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] formula_type_info = new String[]{'formula','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] triggerType_type_info = new String[]{'triggerType','http://soap.sforce.com/2006/04/metadata','WorkflowTriggerTypes','1','1','false'};
        private String[] workflowTimeTriggers_type_info = new String[]{'workflowTimeTriggers','http://soap.sforce.com/2006/04/metadata','WorkflowTimeTrigger','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'fullName','actions','active','booleanFilter','criteriaItems','description','formula','triggerType','workflowTimeTriggers'};
    }
        
        public class RemoteSiteSetting extends Metadata {
        public String type = 'RemoteSiteSetting';
        public String fullName;
        public String description;
        public Boolean disableProtocolSecurity;
        public Boolean isActive;
        public String url;
        private String[] type_att_info = new String[]{'xsi:type'};
        private String[] fullName_type_info = new String[]{'fullName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] disableProtocolSecurity_type_info = new String[]{'disableProtocolSecurity','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] isActive_type_info = new String[]{'isActive','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] url_type_info = new String[]{'url','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'fullName','description','disableProtocolSecurity','isActive','url'};
    }
     
   
    public class createResponse_element {
        public MetadataService.AsyncResult[] result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/2006/04/metadata','AsyncResult','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
           
    public class MetadataPort {
        public String endpoint_x;
        {
            // Workaround to platform bug (?) where the following method returns a none HTTPS URL in a Batch Apex context
            URL baseUrl = URL.getSalesforceBaseUrl();
            System.debug('Protocol is ' + baseUrl.getProtocol());
            if(baseUrl.getProtocol() == 'http')
                baseUrl = new URL('https', baseUrl.getHost(), baseUrl.getPort(), baseUrl.getFile()); // Switch to https protocol
            endpoint_x = baseUrl.toExternalForm() + '/services/Soap/m/27.0';
            
           
            
        }
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public MetadataService.SessionHeader_element SessionHeader;
        public MetadataService.DebuggingInfo_element DebuggingInfo;
        public MetadataService.CallOptions_element CallOptions;
        public MetadataService.DebuggingHeader_element DebuggingHeader;
        private String SessionHeader_hns = 'SessionHeader=http://soap.sforce.com/2006/04/metadata';
        private String DebuggingInfo_hns = 'DebuggingInfo=http://soap.sforce.com/2006/04/metadata';
        private String CallOptions_hns = 'CallOptions=http://soap.sforce.com/2006/04/metadata';
        private String DebuggingHeader_hns = 'DebuggingHeader=http://soap.sforce.com/2006/04/metadata';
        private String[] ns_map_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata', 'MetadataService'};
               
        
        public MetadataService.AsyncResult[] create(MetadataService.Metadata[] metadata) {
            MetadataService.create_element request_x = new MetadataService.create_element();
            MetadataService.createResponse_element response_x;
            request_x.metadata = metadata;
            Map<String, MetadataService.createResponse_element> response_map_x = new Map<String, MetadataService.createResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/2006/04/metadata',
              'create',
              'http://soap.sforce.com/2006/04/metadata',
              'createResponse',
              'MetadataService.createResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            system.debug('respo' + response_x);
            return response_x.result;
        }
        
         public MetadataService.AsyncResult[] updateMetadata(MetadataService.UpdateMetadata[] UpdateMetadata) {
            MetadataService.update_element request_x = new MetadataService.update_element();
            MetadataService.updateResponse_element response_x;
            request_x.UpdateMetadata = UpdateMetadata;
            Map<String, MetadataService.updateResponse_element> response_map_x = new Map<String, MetadataService.updateResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/2006/04/metadata',
              'update',
              'http://soap.sforce.com/2006/04/metadata',
              'updateResponse',
              'MetadataService.updateResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
       
        public MetadataService.AsyncResult[] checkStatus(String[] asyncProcessId) {
            MetadataService.checkStatus_element request_x = new MetadataService.checkStatus_element();
            MetadataService.checkStatusResponse_element response_x;
            request_x.asyncProcessId = asyncProcessId;
            Map<String, MetadataService.checkStatusResponse_element> response_map_x = new Map<String, MetadataService.checkStatusResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/2006/04/metadata',
              'checkStatus',
              'http://soap.sforce.com/2006/04/metadata',
              'checkStatusResponse',
              'MetadataService.checkStatusResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
   
    }
    
    
    public class AsyncResult {
        public Boolean checkOnly;
        public Boolean done;
        public String id;
        public String message;
        public Integer numberComponentErrors;
        public Integer numberComponentsDeployed;
        public Integer numberComponentsTotal;
        public Integer numberTestErrors;
        public Integer numberTestsCompleted;
        public Integer numberTestsTotal;
        public String state;
        public String stateDetail;
        public DateTime stateDetailLastModifiedDate;
        public String statusCode;
        private String[] checkOnly_type_info = new String[]{'checkOnly','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] done_type_info = new String[]{'done','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] id_type_info = new String[]{'id','http://soap.sforce.com/2006/04/metadata','ID','1','1','false'};
        private String[] message_type_info = new String[]{'message','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] numberComponentErrors_type_info = new String[]{'numberComponentErrors','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] numberComponentsDeployed_type_info = new String[]{'numberComponentsDeployed','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] numberComponentsTotal_type_info = new String[]{'numberComponentsTotal','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] numberTestErrors_type_info = new String[]{'numberTestErrors','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] numberTestsCompleted_type_info = new String[]{'numberTestsCompleted','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] numberTestsTotal_type_info = new String[]{'numberTestsTotal','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] state_type_info = new String[]{'state','http://soap.sforce.com/2006/04/metadata','AsyncRequestState','1','1','false'};
        private String[] stateDetail_type_info = new String[]{'stateDetail','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] stateDetailLastModifiedDate_type_info = new String[]{'stateDetailLastModifiedDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] statusCode_type_info = new String[]{'statusCode','http://soap.sforce.com/2006/04/metadata','StatusCode','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'checkOnly','done','id','message','numberComponentErrors','numberComponentsDeployed','numberComponentsTotal','numberTestErrors','numberTestsCompleted','numberTestsTotal','state','stateDetail','stateDetailLastModifiedDate','statusCode'};
    }    
    
    public class checkStatus_element {
        public String[] asyncProcessId;
        private String[] asyncProcessId_type_info = new String[]{'asyncProcessId','http://soap.sforce.com/2006/04/metadata','ID','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'asyncProcessId'};
    }
    
    public class checkStatusResponse_element {
        public MetadataService.AsyncResult[] result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/2006/04/metadata','AsyncResult','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    public class PackageTypeMembers {
        public String[] members;
        public String name;
        private String[] members_type_info = new String[]{'members','http://www.w3.org/2001/XMLSchema','string','0','-1','false'};
        private String[] name_type_info = new String[]{'name','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'members','name'};
    }
    public class DebuggingHeader_element {
        public MetadataService.LogInfo[] categories;
        public String debugLevel;
        private String[] categories_type_info = new String[]{'categories','http://soap.sforce.com/2006/04/metadata','LogInfo','0','-1','false'};
        private String[] debugLevel_type_info = new String[]{'debugLevel','http://soap.sforce.com/2006/04/metadata','LogType','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'categories','debugLevel'};
    }
    public class LogInfo {
        public String category;
        public String level;
        private String[] category_type_info = new String[]{'category','http://soap.sforce.com/2006/04/metadata','LogCategory','1','1','false'};
        private String[] level_type_info = new String[]{'level','http://soap.sforce.com/2006/04/metadata','LogCategoryLevel','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'category','level'};
    }
    
    public virtual class Metadata {
        public String fullName;
        private String[] fullName_type_info = new String[]{'fullName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'fullName'};
    }
    
    public class SessionHeader_element {
        public String sessionId;
        private String[] sessionId_type_info = new String[]{'sessionId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'sessionId'};
    }
    
   
    public class retrieveResponse_element {
        public MetadataService.AsyncResult result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/2006/04/metadata','AsyncResult','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }

    public class DebuggingInfo_element {
        public String debugLog;
        private String[] debugLog_type_info = new String[]{'debugLog','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'debugLog'};
    }
    
    public class CallOptions_element {
        public String client;
        private String[] client_type_info = new String[]{'client','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'client'};
    }
    
   
    
    public class checkDeployStatus_element {
        public String asyncProcessId;
        private String[] asyncProcessId_type_info = new String[]{'asyncProcessId','http://soap.sforce.com/2006/04/metadata','ID','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'asyncProcessId'};
    }
    
     public class create_element {
        public MetadataService.Metadata[] metadata;
        private String[] metadata_type_info = new String[]{'metadata','http://soap.sforce.com/2006/04/metadata','Metadata','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'metadata'};
    }
    
     public class update_element {
        public MetadataService.UpdateMetadata[] UpdateMetadata;
        private String[] UpdateMetadata_type_info = new String[]{'UpdateMetadata','http://soap.sforce.com/2006/04/metadata','UpdateMetadata','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'UpdateMetadata'};
    }
    
    public class updateResponse_element {
        public MetadataService.AsyncResult[] result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/2006/04/metadata','AsyncResult','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    
    public class UpdateMetadata {
        public String currentName;
        public MetadataService.Metadata metadata;
        private String[] currentName_type_info = new String[]{'currentName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] metadata_type_info = new String[]{'metadata','http://soap.sforce.com/2006/04/metadata','Metadata','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'currentName','metadata'};
    }
    
    public class FilterItem {
        public String field;
        public String operation;
        public String value;
        public String valueField;
        private String[] field_type_info = new String[]{'field','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] operation_type_info = new String[]{'operation','http://soap.sforce.com/2006/04/metadata','FilterOperation','1','1','false'};
        private String[] value_type_info = new String[]{'value','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] valueField_type_info = new String[]{'valueField','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'field','operation','value','valueField'};
    }
    
    public class WorkflowTimeTrigger {
        public MetadataService.WorkflowActionReference[] actions;
        public String offsetFromField;
        public String timeLength;
        public String workflowTimeTriggerUnit;
        private String[] actions_type_info = new String[]{'actions','http://soap.sforce.com/2006/04/metadata','WorkflowActionReference','0','-1','false'};
        private String[] offsetFromField_type_info = new String[]{'offsetFromField','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] timeLength_type_info = new String[]{'timeLength','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] workflowTimeTriggerUnit_type_info = new String[]{'workflowTimeTriggerUnit','http://soap.sforce.com/2006/04/metadata','WorkflowTimeUnits','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'actions','offsetFromField','timeLength','workflowTimeTriggerUnit'};
    }
    
    public class WorkflowActionReference {
        public String name;
        public String type_x;
        private String[] name_type_info = new String[]{'name','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] type_x_type_info = new String[]{'type','http://soap.sforce.com/2006/04/metadata','WorkflowActionType','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'name','type_x'};
    }
    
    public class WorkflowAlert {
        public String[] ccEmails;
        public String description;
        public Boolean protected_x;
        public MetadataService.WorkflowEmailRecipient[] recipients;
        public String senderAddress;
        public String senderType;
        public String template;
        private String[] ccEmails_type_info = new String[]{'ccEmails','http://www.w3.org/2001/XMLSchema','string','0','-1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] protected_x_type_info = new String[]{'protected','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] recipients_type_info = new String[]{'recipients','http://soap.sforce.com/2006/04/metadata','WorkflowEmailRecipient','0','-1','false'};
        private String[] senderAddress_type_info = new String[]{'senderAddress','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] senderType_type_info = new String[]{'senderType','http://soap.sforce.com/2006/04/metadata','ActionEmailSenderType','0','1','false'};
        private String[] template_type_info = new String[]{'template','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'ccEmails','description','protected_x','recipients','senderAddress','senderType','template'};
    }
    
    public class WorkflowEmailRecipient {
        public String field;
        public String recipient;
        public String type_x;
        private String[] field_type_info = new String[]{'field','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] recipient_type_info = new String[]{'recipient','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] type_x_type_info = new String[]{'type','http://soap.sforce.com/2006/04/metadata','ActionEmailRecipientTypes','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'field','recipient','type_x'};
    }
    
    public class WorkflowFieldUpdate {
        public String description;
        public String field;
        public String formula;
        public String literalValue;
        public String lookupValue;
        public String lookupValueType;
        public String name;
        public Boolean notifyAssignee;
        public String operation;
        public Boolean protected_x;
        public Boolean reevaluateOnChange;
        public String targetObject;
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] field_type_info = new String[]{'field','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] formula_type_info = new String[]{'formula','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] literalValue_type_info = new String[]{'literalValue','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] lookupValue_type_info = new String[]{'lookupValue','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] lookupValueType_type_info = new String[]{'lookupValueType','http://soap.sforce.com/2006/04/metadata','LookupValueType','0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] notifyAssignee_type_info = new String[]{'notifyAssignee','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] operation_type_info = new String[]{'operation','http://soap.sforce.com/2006/04/metadata','FieldUpdateOperation','1','1','false'};
        private String[] protected_x_type_info = new String[]{'protected','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] reevaluateOnChange_type_info = new String[]{'reevaluateOnChange','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] targetObject_type_info = new String[]{'targetObject','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'description','field','formula','literalValue','lookupValue','lookupValueType','name','notifyAssignee','operation','protected_x','reevaluateOnChange','targetObject'};
    }
    
    public class WorkflowKnowledgePublish {
        public String action;
        public String description;
        public String label;
        public String language;
        public Boolean protected_x;
        private String[] action_type_info = new String[]{'action','http://soap.sforce.com/2006/04/metadata','KnowledgeWorkflowAction','1','1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] label_type_info = new String[]{'label','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] language_type_info = new String[]{'language','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] protected_x_type_info = new String[]{'protected','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'action','description','label','language','protected_x'};
    }
    
    public class WorkflowOutboundMessage {
        public Double apiVersion;
        public String description;
        public String endpointUrl;
        public String[] fields;
        public Boolean includeSessionId;
        public String integrationUser;
        public String name;
        public Boolean protected_x;
        public Boolean useDeadLetterQueue;
        private String[] apiVersion_type_info = new String[]{'apiVersion','http://www.w3.org/2001/XMLSchema','double','1','1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] endpointUrl_type_info = new String[]{'endpointUrl','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] fields_type_info = new String[]{'fields','http://www.w3.org/2001/XMLSchema','string','0','-1','false'};
        private String[] includeSessionId_type_info = new String[]{'includeSessionId','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] integrationUser_type_info = new String[]{'integrationUser','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] protected_x_type_info = new String[]{'protected','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] useDeadLetterQueue_type_info = new String[]{'useDeadLetterQueue','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'apiVersion','description','endpointUrl','fields','includeSessionId','integrationUser','name','protected_x','useDeadLetterQueue'};
    }
    
     public class WorkflowSend {
        public String action;
        public String description;
        public String label;
        public String language;
        public Boolean protected_x;
        private String[] action_type_info = new String[]{'action','http://soap.sforce.com/2006/04/metadata','SendAction','1','1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] label_type_info = new String[]{'label','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] language_type_info = new String[]{'language','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] protected_x_type_info = new String[]{'protected','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'action','description','label','language','protected_x'};
    }
    
    public class WorkflowTask {
        public String assignedTo;
        public String assignedToType;
        public String description;
        public Integer dueDateOffset;
        public Boolean notifyAssignee;
        public String offsetFromField;
        public String priority;
        public Boolean protected_x;
        public String status;
        public String subject;
        private String[] assignedTo_type_info = new String[]{'assignedTo','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] assignedToType_type_info = new String[]{'assignedToType','http://soap.sforce.com/2006/04/metadata','ActionTaskAssignedToTypes','1','1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] dueDateOffset_type_info = new String[]{'dueDateOffset','http://www.w3.org/2001/XMLSchema','int','1','1','false'};
        private String[] notifyAssignee_type_info = new String[]{'notifyAssignee','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] offsetFromField_type_info = new String[]{'offsetFromField','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] priority_type_info = new String[]{'priority','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] protected_x_type_info = new String[]{'protected','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] status_type_info = new String[]{'status','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] subject_type_info = new String[]{'subject','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'assignedTo','assignedToType','description','dueDateOffset','notifyAssignee','offsetFromField','priority','protected_x','status','subject'};
    }
}