/**     * File Name:AccLicenseCreateProdLicenseHandler
        * Description : This handler class is used to update create a product license(Entitlement) on insert of account license or 
        *               in case of update of account license if there is no existing product license
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Feb 3rd 2017 RamakrishnaKini     Added new trigger logic.
        1.1 Feb 23rd 2017 RamakrishnaKini     Added new trigger logic for account license delete scenarion to inactivate the product license.
        1.2 May 3rd 2017 RamakrishnaKini     Logic for null checks and end date greater than startdate.
        1.3 July 23rd 2017 QCW-3462 RamakrishnaKini  Modified logic to update entitlements when account on account license is changed
        1.4 Oct 25rd 2017 CHG0032086 ext_bad  Modified logic of populating Business Hours for Entitlement
        1.5 July 5rd 2019 IT-1995 ext_bad  Add Subscription__c sync
        1.6 17.02.2020  ext_bad IT-2497   Added Legacy Enterprise Support Entitlement Process

*/
public with sharing class AccLicenseCreateProdLicenseHandler {
    private static final String ENTITLEMENT_OBJECT = 'Entitlement';
    private static final String STANDARD_DEV_NAME = 'Standard';
    static Id twentyFourSevenBusinessHoursId;
    static Id defaultBusinessHoursId;
    static String standardSupportLevel;
    static String premiumSupportLevel;
    static String holdAccountName;
    static SlaProcess standardEntitlementProcess;
    static SlaProcess legacyEntitlementProcess;
    static SlaProcess signatureEntitlementProcess;
    static SlaProcess enterpriseEntitlementProcess;

    public AccLicenseCreateProdLicenseHandler() {

    }

    /*  @Description :This common method is used to create the product license on account license after insert
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap Account Map
   
    */
    public static void onAfterInsert(List<Account_License__c> inputList, Map<Id, Account_License__c> inputMap) {
        System.debug('AccLicenseCreateProdLicenseHandler start');
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accLicenseIds = new Set<Id>();
        Set<String> accLicenseNames = new Set<String>();// UIN New
        Map<Id, Id> accountToBusinessHours = new Map<Id, Id>();
        Map<Id, Id> accountToEnterpriseBusinessHours = new Map<Id, Id>();
        Map<String, Entitlement> mEntNameEntitlement = new Map<String, Entitlement>();
        List<Entitlement> newEntitlements = new List<Entitlement>();
        List<Entitlement> updExstEntitlements = new List<Entitlement>();
        List<Account_License__c> accLicenses = new List<Account_License__c>();

        System.debug('AccLicenseCreateProdLicenseHandler before input list');
        for (Account_License__c accLicense : inputList) {
            System.debug('AccLicenseCreateProdLicenseHandler inside input list accLicense.Account__c:' + accLicense.Account__c);
            if (String.isNotBlank(accLicense.Account__c)) {
                System.debug('AccLicenseCreateProdLicenseHandler inside input list not blank');
                accountIds.add(accLicense.Account__c);
                accLicenseIds.add(accLicense.Id);
                accLicenseNames.add(accLicense.Name); // UIN New
            }
        }
        if (!accLicenseNames.isEmpty()) {
            for (Entitlement e : [
                    SELECT Id, Name, RecordTypeId, Account_License__c, Subscription__c
                    FROM Entitlement
                    WHERE Name IN :accLicenseNames AND Account_License__c = NULL
            ]) {
                if (!mEntNameEntitlement.containsKey(e.Name)) {
                    mEntNameEntitlement.put(e.Name, e);
                }
            }
        }
        System.debug('AccLicenseCreateProdLicenseHandler after input list');

        if (!accLicenseIds.isEmpty()) {
            if (!accountIds.isEmpty()) {
                for (Account a : [
                        SELECT a.Id, a.Support_Office__r.Business_Hours__c, a.Support_Office__r.Enterprise_Business_Hours__c
                        FROM Account a
                        WHERE Id IN :accountIds
                ]) {
                    accountToBusinessHours.put(a.Id, a.Support_Office__r.Business_Hours__c);
                    accountToEnterpriseBusinessHours.put(a.Id, a.Support_Office__r.Enterprise_Business_Hours__c);
                }
            }
            accLicenses = [
                    SELECT Id, Name, Account__c, Account__r.Premier_Support__c, Support_From__c, Support_To__c, INT_NetSuite_Support_Level__c, Subscription__c,
                            Legacy_Enterprise_End_Date__c, Signature_End_Date__c
                    FROM Account_License__c
                    WHERE Id IN :accLicenseIds
            ];

            if (!accLicenses.isEmpty()) {
                List<RecordType> entStdRecType = [SELECT Id FROM RecordType WHERE DeveloperName = :STANDARD_DEV_NAME AND SobjectType = :ENTITLEMENT_OBJECT];
                retrieveSettings();
                for (Account_License__c al : accLicenses) {
                    if (al.Support_From__c <= al.Support_To__c) {// Fix for end data greater than start date
                        if (!mEntNameEntitlement.containsKey(al.Name)) {
                            newEntitlements.add(createEntitlement(al, accountToBusinessHours, accountToEnterpriseBusinessHours));
                        } else {
                            if (mEntNameEntitlement.containsKey(al.Name)) {
                                mEntNameEntitlement.get(al.Name).Account_License__c = al.Id;
                                mEntNameEntitlement.get(al.Name).RecordTypeId = entStdRecType[0].Id;
                                mEntNameEntitlement.get(al.Name).StartDate = al.Support_From__c;
                                mEntNameEntitlement.get(al.Name).EndDate = al.Support_To__c;
                                mEntNameEntitlement.get(al.Name).Subscription__c = al.Subscription__c;
                                updExstEntitlements.add(mEntNameEntitlement.get(al.Name));
                            }
                        }
                    }
                }

                if (!newEntitlements.isEmpty()) {
                    insert newEntitlements;
                }
                if (!updExstEntitlements.isEmpty()) {
                    update updExstEntitlements;
                }
            }
        }
    }

    /*  @Description :This common method is used to update the product license on account license after update
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap Account Map
   
    */
    public static void onAfterUpdate(List<Account_License__c> inputList, Map<Id, Account_License__c> inputMap, Map<Id, Account_License__c> oInputMap) {
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accLicenseIds = new Set<Id>();
        Set<Id> accChangedLicenseIds = new Set<Id>();
        List<Account_License__c> accLicenses = new List<Account_License__c>();
        List<Entitlement> newEntitlements = new List<Entitlement>();
        Map<Id, Id> accountToBusinessHours = new Map<Id, Id>();
        Map<Id, Id> accountToEnterpriseBusinessHours = new Map<Id, Id>();
        Set<String> accLicenseNames = new Set<String>();// UIN New
        Map<String, Entitlement> mEntNameEntitlement = new Map<String, Entitlement>();// UIN New
        List<Entitlement> updExstEntitlements = new List<Entitlement>();// UIN New
        for (Account_License__c accLicense : inputList) {
            if (String.isNotBlank(accLicense.Account__c)) {
                accountIds.add(accLicense.Account__c);
                accLicenseIds.add(accLicense.Id);
                accLicenseNames.add(accLicense.Name);// UIN New
            }
        }
        //Added by UIN for QCW-3462
        for (Account_License__c accLicense : inputList) {
            if (String.isNotBlank(accLicense.Account__c) && accLicense.Account__c != oInputMap.get(accLicense.Id).Account__c) {
                accChangedLicenseIds.add(accLicense.Id);
            }
        }

        if (!accChangedLicenseIds.isEmpty()) {
            for (Entitlement ent : [SELECT Id, Account_License__c, Account_License__r.Account__c, AccountId FROM Entitlement WHERE Account_License__c IN :accChangedLicenseIds]) {
                if (ent.Account_License__r.Account__c != ent.AccountId) {
                    ent.AccountId = ent.Account_License__r.Account__c;
                    newEntitlements.add(ent);
                }
            }

            if (!newEntitlements.isEmpty()) {
                update newEntitlements;
            }
        }
        //Added by UIN for QCW-3462
        if (!accLicenseIds.isEmpty()) {
            Set<Id> mAccLicWithEntitlements = new Set<Id>();
            Set<String> sAccLicNamesWithEntitlements = new Set<String>();
            for (Entitlement ent : [SELECT Id, Account_License__c, Name FROM Entitlement WHERE Account_License__c IN :accLicenseIds]) {
                if (!mAccLicWithEntitlements.contains(ent.Account_License__c)) {
                    mAccLicWithEntitlements.add(ent.Account_License__c);
                    sAccLicNamesWithEntitlements.add(ent.Name);
                }
            }

            accLicenseIds.removeAll(mAccLicWithEntitlements);
            accLicenseNames.removeAll(sAccLicNamesWithEntitlements);
            if (!accLicenseNames.isEmpty()) {

                for (Entitlement e : [SELECT Id, Name, RecordTypeId, Account_License__c, Subscription__c FROM Entitlement WHERE Name IN :accLicenseNames AND Account_License__c = NULL]) {
                    if (!mEntNameEntitlement.containsKey(e.Name)) {
                        mEntNameEntitlement.put(e.Name, e);
                    }
                }
            }
            if (!accLicenseIds.isEmpty()) {
                if (!accountIds.isEmpty()) {
                    for (Account a : [
                            SELECT a.Id, a.Support_Office__r.Business_Hours__c, a.Support_Office__r.Enterprise_Business_Hours__c
                            FROM Account a
                            WHERE Id IN :accountIds
                    ]) {
                        accountToBusinessHours.put(a.Id, a.Support_Office__r.Business_Hours__c);
                        accountToEnterpriseBusinessHours.put(a.Id, a.Support_Office__r.Enterprise_Business_Hours__c);
                    }
                }
                accLicenses = [
                        SELECT Id, Name, Account__c, Account__r.Premier_Support__c, Support_From__c, Support_To__c, INT_NetSuite_Support_Level__c, Subscription__c,
                                Legacy_Enterprise_End_Date__c, Signature_End_Date__c
                        FROM Account_License__c
                        WHERE Id IN :accLicenseIds
                ];
                if (!accLicenses.isEmpty()) {
                    List<RecordType> entStdRecType = [SELECT Id FROM RecordType WHERE DeveloperName = :STANDARD_DEV_NAME AND SobjectType = :ENTITLEMENT_OBJECT];
                    retrieveSettings();
                    for (Account_License__c al : accLicenses) {
                        if (al.Support_From__c <= al.Support_To__c) { // Fix for end data greater than start date
                            if (!mEntNameEntitlement.containsKey(al.Name)) {
                                newEntitlements.add(createEntitlement(al, accountToBusinessHours, accountToEnterpriseBusinessHours));
                            } else {
                                if (mEntNameEntitlement.containsKey(al.Name)) {
                                    mEntNameEntitlement.get(al.Name).Account_License__c = al.Id;
                                    mEntNameEntitlement.get(al.Name).RecordTypeId = entStdRecType[0].Id;
                                    mEntNameEntitlement.get(al.Name).StartDate = al.Support_From__c;
                                    mEntNameEntitlement.get(al.Name).EndDate = al.Support_To__c;
                                    mEntNameEntitlement.get(al.Name).Subscription__c = al.Subscription__c;
                                    updExstEntitlements.add(mEntNameEntitlement.get(al.Name));
                                }
                            }
                        }
                    }
                    if (!newEntitlements.isEmpty()) {
                        insert newEntitlements;
                    }
                    if (!updExstEntitlements.isEmpty()) {
                        update updExstEntitlements;
                    }
                }
            }
        }

    }

    /*  @Description :This common method is used to delete the product license on account license after delete
        @parameter inputList: Trigger.old Account list
        @parameter inputMap: Trigger.oldMap Account Map
    */
    public static void onBeforeDelete(List<Account_License__c> inputList, Map<Id, Account_License__c> inputMap) {
        Set<Id> accLicenseIds = new Set<Id>();
        List<Entitlement> entitlementsForDel = new List<Entitlement>();
        List<Entitlement> entitlementsForUpd = new List<Entitlement>();
        for (Account_License__c accLicense : inputList) {
            accLicenseIds.add(accLicense.Id);
            System.debug('accLicense.Id: ' + accLicense.Id);
        }

        List<RecordType> entObsoleteRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Obsolete' AND SobjectType = :ENTITLEMENT_OBJECT];

        if (!accLicenseIds.isEmpty() && entObsoleteRecType.size() > 0) {
            entitlementsForDel = [SELECT Id, RecordTypeId FROM Entitlement WHERE Account_License__c IN :accLicenseIds AND RecordTypeId != :entObsoleteRecType[0].Id];
            if (!entitlementsForDel.isEmpty()) {
                for (Entitlement e : entitlementsForDel) {
                    e.RecordTypeId = entObsoleteRecType[0].Id;
                    entitlementsForUpd.add(e);
                }
                //Update record type for product licenses whose account licenses are deleted
                if (!entitlementsForUpd.isEmpty()) {
                    update entitlementsForUpd;
                }
            }
        }
    }

    /**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/

    private static SlaProcess standardOrEnterpriseProcess(String supportLevel) {
        return supportLevel == premiumSupportLevel ? enterpriseEntitlementProcess : standardEntitlementProcess;
    }

    private static Entitlement createEntitlement(Account_License__c al, Map<Id, Id> accountToBusinessHours, Map<Id, Id> accountToEnterpriseBusinessHours) {
        Entitlement e = new Entitlement (Account_License__c = al.Id, AccountId = al.Account__c, Automatic__c = true);
        e.Name = al.Name;
        e.StartDate = al.Support_From__c;
        e.EndDate = al.Support_To__c;
        e.Subscription__c = al.Subscription__c;

        SlaProcess process;
        if (al.Legacy_Enterprise_End_Date__c != null && al.Legacy_Enterprise_End_Date__c >= Date.today()) {
            process = legacyEntitlementProcess;
        } else if ((al.Signature_End_Date__c != null && al.Signature_End_Date__c >= Date.today())
                || al.Account__r.Premier_Support__c) {
            process = signatureEntitlementProcess;
        } else {
            process = standardOrEnterpriseProcess(al.INT_NetSuite_Support_Level__c);
        }
        e.SlaProcessId = process.Id;
        e.SLA_Process_Name__c = process.Name;

        Id businessHoursId = accountToBusinessHours.get(al.Account__c);
        Id enterpriseBusinessHoursId = accountToEnterpriseBusinessHours.get(al.Account__c);
        if (businessHoursId != null) {
            e.BusinessHoursId = (e.SlaProcessId == legacyEntitlementProcess.Id)
                    ? enterpriseBusinessHoursId != null ? enterpriseBusinessHoursId : defaultBusinessHoursId
                    : businessHoursId;
        } else {
            e.BusinessHoursId = defaultBusinessHoursId;
        }
        return e;
    }

    private static void retrieveSettings() {
        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        premiumSupportLevel = es.Premium_Support_Level__c;
        standardSupportLevel = es.Standard_Support_Level__c;
        holdAccountName = es.Hold_Account_Name__c;

        standardEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name = :es.Standard_Entitlement_Process_Name__c LIMIT 1];
        legacyEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name = :es.Premium_Entitlement_Process_Name__c LIMIT 1];
        signatureEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name = :es.Signature_Entitlement_Process_Name__c LIMIT 1];
        enterpriseEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name = :es.Enterprise_Entitlement_Process_Name__c LIMIT 1];
        twentyFourSevenBusinessHoursId = [SELECT Id FROM BusinessHours WHERE Name = :es.X24x7_Business_Hours_Name__c LIMIT 1].Id;
        defaultBusinessHoursId = [SELECT Id FROM BusinessHours WHERE IsDefault = TRUE LIMIT 1].Id;
    }
}