global class PickUpCaseFromDetailPage
{
	//This method changes the Case Status to 'In Process' and changes the Owner to the current logged in User
	WebService static String pickupCase(Id caseId)
	{
		final String updateStatus = 'success';
		Id caseOwnerId = UserInfo.getUserId();
		Customizations__c custs = Customizations__c.getOrgDefaults();
		if(custs == null)
		{
			String errorMsg = 'Error retrieving Customization Config custom settings. Please ensure Default Organization Level Values have been configured correctly.';
            return errorMsg;
		}
		Case caseForPickup = new Case(Id = caseId, Status = custs.Pickup_Case_Status__c, OwnerId = caseOwnerId);
    	update caseForPickup;
    	return updateStatus;
	}
}