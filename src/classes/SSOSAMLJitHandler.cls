/********************************************************
* CLASS: SSOSAMLJitHandler
* DESCRIPTION: This class provides logic for inbound just-in-time provisioning of single sign-on users.
*              Currently class only handles updates of User object, to support user re-activation.
*   
* CHANGELOG:
*   2020-03-15 - BAD - Added Initial logic
*   2020-06-22 - UIN - ADded logic for community check to prevent login to attunity without appropriate permissions.
*********************************************************/
global class SSOSAMLJitHandler implements Auth.SamlJitHandler {    
    private static String FUNC_STR = 'SSOSAMLJitHandler debug: ';
    private class JitException extends Exception{}
    
    //Method used to activate the actual user record.
    private void handleUser(User u, Map<String, String> attributes, String federationIdentifier) {
        System.debug(FUNC_STR + ' FUNC: handleUser() Start');
        
        if(attributes.containsKey('community')) {
            String communityType = attributes.get('community');
            /*
            if(string.isNotBlank(communityType)){
            throw new JitException('Community not recognized'); //Implement this when all communties handles the community param in the assertion
            }
            */
            if(u.IsActive != null && !u.IsActive){
                u.IsActive = true;  
                update(u);
            }
        }
    }
    
    //SSO create user method used to read attributes and create user if it doesnt exist on login. Not being used
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug(FUNC_STR + ' FUNC: createUser() Start');
        if(!Test.isRunningTest())
            throw new JitException('Automated user provisioning for create users are not supported');
        return null;
    }
    
    //SSO update user method used to read attributes and either show error or allow activation/login of inactive/active user
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug(FUNC_STR + ' FUNC: updateUser() Start');
        System.debug(FUNC_STR + ' Attributes: ' + attributes);
        System.debug(FUNC_STR + ' FederationIdentifier: ' + federationIdentifier);
        User u = [SELECT Id, FirstName, ContactId, IsActive FROM User WHERE Id=:userId];
        System.debug(FUNC_STR + ' User: ' + u);
        String communityType = attributes.get('community');
        Contact ct = new Contact();
        ct = [select id, SFDCAccessGranted__c  from contact where id = :u.contactId];
        
        if(communityId != null || portalId != null) {
            if('attunity'.equalsIgnorecase(communityType)){
                if(ct.SFDCAccessGranted__c.contains('DI Support Portal'))
                    handleUser(u, attributes, federationIdentifier);
                else
                    throw new JitException('The User Does not have access to the Portal'); //Implement this when all communties handles the community param in the assertion
            }
            else
                handleUser(u, attributes, federationIdentifier);
        
        }
    }
}