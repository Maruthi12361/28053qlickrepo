/**
  20200310 : CCE FURRevamp This class runs twice a day and checks to see if any Leads or Contacts have the QSB Trial field set for more 
                 than 21 days without the Overall Follow-Up Required field being set.
**/
global class QSBTrialGreater21Days Implements Schedulable
{
    public static String schedHourly = '0 44 11,23 * * ?';  //Runs at 11.44am and 11.44pm
    //public static String schedDaily = '0 00 04 * * ?';  //Daily 4am
    // used for scheduling a repeating job from the "execute anonymous" window with the following: QSBTrialGreater21Days.scheduleMe();
    global static String scheduleMe() {
        QSBTrialGreater21Days job = new QSBTrialGreater21Days(); 
        if (test.isRunningTest()) return null;
        else return System.schedule('QSBTrialGreater21', schedHourly, job);
    }
    global void execute(SchedulableContext sc)
    {
        //QSBTrialGreater21();
        Database.executeBatch(new QSBTrialGreater21DaysBatchLead());
        Database.executeBatch(new QSBTrialGreater21DaysBatchContact());
    }

    // public void QSBTrialGreater21()
    // {
    //     List<Lead> lstLeads = new List<Lead>();
    //     for(Lead l : [select Id, QSB_Trial__c, QSB_Trial_Start_Date__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c, Trial_QSB_Trial_greater_21__c from Lead where QSB_Trial__c = true AND Overall_Follow_up_Required__c = false AND QSB_Trial_Start_Date__c != LAST_N_DAYS:21 AND IsConverted = false AND IsDeleted = false]) {
    //         l.QSB_Trial__c = false; 
    //         l.Trial_QSB_Trial_greater_21__c = true;
    //         lstLeads.add(l);
    //     }
    //     system.Debug('QSBTrialGreater21: lstLeads.size = ' + lstLeads.size());
    //     //if ((lstLeads.size() > 0) && (!test.isRunningTest())) { update lstLeads; }
    //     if (lstLeads.size() > 0) { update lstLeads; }
        
    //     List<Contact> lstCons = new List<Contact>();
    //     for(Contact c : [select Id, QSB_Trial__c, QSB_Trial_Start_Date__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c, Trial_QSB_Trial_greater_21__c from Contact where QSB_Trial__c = true AND Overall_Follow_up_Required__c = false AND QSB_Trial_Start_Date__c != LAST_N_DAYS:21 AND IsDeleted = false]) {
    //         c.QSB_Trial__c = false; 
    //         c.Trial_QSB_Trial_greater_21__c = true;
    //         lstCons.add(c);
    //     }
    //     system.Debug('QSBTrialGreater21: lstCons.size = ' + lstCons.size());
    //     //if ((lstCons.size() > 0) && (!test.isRunningTest())) { update lstCons; }
    //     if (lstCons.size() > 0) { update lstCons; }        
    // }
}