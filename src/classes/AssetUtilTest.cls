/******** prod
* NAME :AssetUtilTest.cls
* Description: TestClass for the AssetUtil class
*
*Change Log:
    IRN      2016-08-19   created this class for the Q2CW project
	06.02.2017  RVA :   changing CreateAcounts methods
03.09.2017 Srinivasan PR 2934
16.01.2017 UIN fix for soql issues.

******/
@isTest
private class AssetUtilTest{
	static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    static testMethod void test_createAsset(){
    	System.debug('Test create asset');
        QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

		System.runAs(u){
			
			Account testAcc = QTTestUtils.createMockAccount('TestCompany', u, false);
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAcc.BillingCountry = 'France';
            insert testAcc;

            Contact ct = new Contact(AccountId=testAcc.Id,lastname='Contact1',firstname='Test', Email = 'junk@gmail.com');
            insert ct;

            List<Address__c> addrs = new List<Address__c>{
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Billing'),
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Shipping')
            };
            insert addrs;

	        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , true);
	        RecordType rType = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Evaluation_Quote'];
			
			Test.startTest();
				//SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
				SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, ct.id, testAcc, '', 'Direct', 'Open', 'Quote', false, '');
				insert quoteForTest;
            //system.assertEquals(0,Limits.getQueries());//FIXME remove me
            SBQQ__Quote__c quoteForSelect = [Select id, Second_Partner__c, SBQQ__MasterContract__c, SBQQ__StartDate__c, Revenue_Type__c, Record_Type_Dev_Name__c, Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c from SBQQ__Quote__c where id = :quoteForTest.id];
            System.debug(quoteForTest + 'Trying');
            SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForSelect.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050, 20, '12345', null);
            Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1', '32232', '12345', '23423', null);
            List<Asset> assets = [select Id, Quote__c from Asset where Quote__c = :quoteForSelect.Id];
            System.assertEquals(0, assets.size());
            AssetUtil au = new AssetUtil();
            Set<Id> quotesToCreateAssetsFor = new Set<Id>();
            quotesToCreateAssetsFor.add(quoteForSelect.Id);
            Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
            quotesMap.put(quoteForSelect.Id, quoteForSelect);
            List<Sales_Channel__c> contractAssetLink = [Select Id from Sales_Channel__c];
            System.assertEquals(0, contractAssetLink.size());
            au.createAsset(quotesToCreateAssetsFor, quotesMap);
            assets = [select Id, Quote__c, NC_Disable_AJAX__c from Asset];
            assets[0].NC_Disable_AJAX__c = 'no';
            System.debug(assets);
            contractAssetLink = [Select Id from Sales_Channel__c];
            System.assertEquals(1, assets.size());
            System.assertEquals('no', assets[0].NC_Disable_AJAX__c);
            System.assertEquals(1, contractAssetLink.size());
            quoteline.Disable_AJAX__c = true;
            update quoteline;
            assets = [select Id, Quote__c, NC_Disable_AJAX__c from Asset where Quote__c = :quoteForSelect.Id];
            assets[0].NC_Disable_AJAX__c = 'no';
            System.assertEquals(1, assets.size());
            System.assertEquals('no', assets[0].NC_Disable_AJAX__c.toLowerCase());

				update quoteLine;
			Test.stopTest();
        }	
    }

    static testMethod void test_createAssetForWithLEFdetails(){
    	System.debug('test_createAssetForWithLEFdetails');
    	QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.runAs(u) {
		
			
			 //system.assertEquals(0,Limits.getQueries());
            Account testAcc = QTTestUtils.createMockAccount('TestCompany', u, false);
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAcc.BillingCountry = 'France';
            insert testAcc;
            Contact ct = new Contact(AccountId=testAcc.Id,lastname='Contact1',firstname='Test', Email = 'junk@gmail.com');
			insert ct;
            List<Address__c> addrs = new List<Address__c>{
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Billing'),
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Shipping')
            };
            insert addrs;

	        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0, true);
	        RecordType rType = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Evaluation_Quote'];


			Test.startTest();
				//SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
				SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, ct.id, testAcc, '', 'Direct', 'Open', 'Quote', false, '');
				insert quoteForTest;
            SBQQ__Quote__c quoteForSelect = [Select id, Second_Partner__c,SBQQ__MasterContract__c, SBQQ__StartDate__c, Revenue_Type__c, Record_Type_Dev_Name__c, Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c from SBQQ__Quote__c where id = :quoteForTest.id];

				SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForSelect.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050,20, null, '55555');

				Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', null, '12345', '55555');
				List<Asset> assets = [select Id, Quote__c from Asset where Quote__c =: quoteForSelect.Id];
				System.assertEquals(0, assets.size());
				AssetUtil au = new AssetUtil();
				Set<Id> quotesToCreateAssetsFor = new Set<Id>();
				quotesToCreateAssetsFor.add(quoteForSelect.Id);
				Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
				quotesMap.put(quoteForSelect.Id, quoteForSelect);
				au.createAsset(quotesToCreateAssetsFor, quotesMap);
				assets = [select Id, Quote__c, NC_Disable_AJAX__c  from Asset where Quote__c =: quoteForSelect.Id];
				assets[0].NC_Disable_AJAX__c = 'no';
				System.assertEquals(1, assets.size());
				System.assertEquals('no', assets[0].NC_Disable_AJAX__c);
				quoteline.NC_Disable_AJAX__c = 'yes';
				update quoteline;
				au.createAsset(quotesToCreateAssetsFor, quotesMap);
				assets = [select Id, Quote__c, NC_Disable_AJAX__c  from Asset where Quote__c =: quoteForSelect.Id];
				System.assertEquals(1, assets.size());
				System.assertEquals('yes', assets[0].NC_Disable_AJAX__c.toLowerCase());
			Test.stopTest();
        }

        
    }

    static testMethod void test_createAsseMasterContract(){
    	System.debug('test_createAsseMasterContract');
    	QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.runAs(u) {

			
			Account testAcc = QTTestUtils.createMockAccount('TestCompany', u, false);
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAcc.BillingCountry = 'France';
            insert testAcc;

	        Sales_Channel__c ca = new Sales_Channel__c();
	        ca.License_Customer__c = testAcc.Id;
        	insert ca;
        	Contract contract = new Contract();
        	contract.Sales_Channel__c= ca.Id;
        	contract.AccountId = testAcc.Id;
        	insert contract;
			
			Contact ct = new Contact(AccountId=testAcc.Id,lastname='Contact1',firstname='Test', Email = 'junk@gmail.com');
            insert ct;
			List<Address__c> addrs = new List<Address__c>{
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Billing'),
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Shipping')
            };
            insert addrs;
			Test.startTest();
	        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , true);

	        RecordType rType = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Evaluation_Quote'];
			//start test
			
	    	//SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, ct.id, testAcc, '', 'Direct', 'Open', 'Quote', false, '');
			quoteforTest.SBQQ__MasterContract__c = contract.Id;
			insert quoteForTest;
            SBQQ__Quote__c quoteForSelect = [Select id, second_partner__c, SBQQ__MasterContract__c, SBQQ__StartDate__c, Revenue_Type__c, Record_Type_Dev_Name__c, Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c from SBQQ__Quote__c where id = :quoteForTest.id];

	    	
	    	//update quoteForTest;
   			SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050,20, '12345', null);
   			Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', '12345', '23423', null);
   			List<Asset> assets = [select Id, Quote__c from Asset where Quote__c =: quoteForSelect.Id];
	 		System.assertEquals(0, assets.size());
	   		AssetUtil au = new AssetUtil();
	   		Set<Id> quotesToCreateAssetsFor = new Set<Id>();
	   		quotesToCreateAssetsFor.add(quoteForSelect.Id);
	   		Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
	   		quotesMap.put(quoteForSelect.Id, quoteForSelect);
	   		List<Sales_Channel__c> contractAssetLink = [Select Id, License_Customer__c, Revenue_Type__c, Sell_Through_Partner__c, Support_Provided_by__c from Sales_Channel__c];
	   		system.debug('saleschannel ' + contractAssetLink);
	   		System.assertEquals(1,contractAssetLink.size());
	   		au.createAsset(quotesToCreateAssetsFor, quotesMap);
	   		assets = [select Id, Quote__c, NC_Disable_AJAX__c  from Asset where Quote__c =: quoteForSelect.Id];
	   		assets[0].NC_Disable_AJAX__c = 'no';
	   		contractAssetLink = [Select Id from Sales_Channel__c];
	   		System.assertEquals(1, assets.size());
	   		System.assertEquals('no', assets[0].NC_Disable_AJAX__c);
	   		System.assertEquals(2,contractAssetLink.size());
			Test.stopTest();

        }
    }

    static testMethod void test_createAsseWithLEFdetailsMasterContract(){
    	System.debug('test_createAsseWithLEFdetailsMasterContract');
    	QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.runAs(u) {

			
			Account testAcc = QTTestUtils.createMockAccount('TestCompany', u, false);
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAcc.BillingCountry = 'France';
            insert testAcc;

	        Sales_Channel__c ca = new Sales_Channel__c();
	        ca.License_Customer__c = testAcc.Id;
        	insert ca;
        	Contract contract = new Contract();
        	contract.Sales_Channel__c = ca.Id;
        	contract.AccountId = testAcc.Id;
        	insert contract;
			
			Contact ct = new Contact(AccountId=testAcc.Id,lastname='Contact1',firstname='Test', Email = 'junk@gmail.com');
            insert ct;

            List<Address__c> addrs = new List<Address__c>{
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Billing'),
                    QuoteTestHelper.createAddress(testAcc.Id, ct.Id, 'Shipping')
            };
            insert addrs;
	        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , true);

	        RecordType rType = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Evaluation_Quote'];
			//start test
			Test.startTest();
//            SBQQ__Quote__c quoteForTest = TestQuoteUtil.createQuote(rType, testAcc);
				SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, ct.id, testAcc, '', 'Direct', 'Open', 'Quote', false, '');
				insert quoteForTest;

				SBQQ__Quote__c quoteForSelect = [Select id, SBQQ__MasterContract__c, SBQQ__StartDate__c, Revenue_Type__c, Record_Type_Dev_Name__c, 
												Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, 
												Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c , Second_Partner__c 
												from SBQQ__Quote__c where id = :quoteForTest.id];

				quoteforTest.SBQQ__MasterContract__c = contract.Id;
				update quoteForTest;

				SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050,20, null, '65498');
				Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', null, '23423', '65498');
				List<Asset> assets = [select Id, Quote__c from Asset where Quote__c =: quoteForTest.Id];
				System.assertEquals(0, assets.size());
				AssetUtil au = new AssetUtil();
				Set<Id> quotesToCreateAssetsFor = new Set<Id>();
				quotesToCreateAssetsFor.add(quoteForSelect.Id);
				Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
				quotesMap.put(quoteForSelect.Id, quoteForSelect);
				List<Sales_Channel__c> contractAssetLink = [Select Id from Sales_Channel__c];
				System.assertEquals(1,contractAssetLink.size());
				au.createAsset(quotesToCreateAssetsFor, quotesMap);
				assets = [select Id, Quote__c, NC_Disable_AJAX__c  from Asset where Quote__c =: quoteForSelect.Id];
				assets[0].NC_Disable_AJAX__c = 'no';
				contractAssetLink = [Select Id from Sales_Channel__c];
				System.assertEquals(1, assets.size());
				System.assertEquals('no', assets[0].NC_Disable_AJAX__c);
				System.assertEquals(2,contractAssetLink.size());
			Test.stopTest();
	   		//update quoteLine;

        }
    }

    static testMethod void test_createAsseWithRevTypeOEM(){
    	System.debug('Test create asset');
        QuoteTestHelper.createCustomSettings();
        //Before all test data is inserted, triggers are disabled
        Semaphores.SetAllSemaphoresToTrue();
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

			System.runAs(u){

			Account testAcc = QTTestUtils.createMockAccount('TestCompany', u, false);
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            testAcc.BillingCountry = 'France';
            testAcc.Territory_Country__c='France';
            insert testAcc;

			Account  oemPartnerAcc = QTTestUtils.createMockAccount('oem company', u, true);
			RecordType oemRType = QuoteTestHelper.getRecordTypebyDevName('OEM_Partner_Account');
	        oemPartnerAcc.RecordtypeId = oemRType.Id;
	        update oemPartnerAcc;

	        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , true);
			RecordType rType = QuoteTestHelper.getRecordTypebyDevName('OEM_Order');
	        Opportunity opp = QuoteTestHelper.createOpportunity(testAcc, '', rType);
	        opp.End_User_Account_del__c = oemPartnerAcc.Id;
			insert opp;
			Contact testContact = QuoteTestHelper.createContact(testAcc.id);
            insert testContact;
			List<Address__c> addrs = new List<Address__c>{
                    QuoteTestHelper.createAddress(testAcc.Id, testContact.Id, 'Billing'),
                    QuoteTestHelper.createAddress(testAcc.Id, testContact.Id, 'Shipping')
            };
            insert addrs;
			RecordType rTypeQuote = QuoteTestHelper.getRecordTypebyDevName('Quote');
			insert QuoteTestHelper.createPCS(testAcc);//2934
			Test.startTest();

				SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact.id, testAcc, '', 'OEM', 'Open', 'Quote', false, opp.Id);
				insert quoteForTest;

				//SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
            SBQQ__Quote__c quoteForSelect = [Select id, Second_Partner__c , SBQQ__MasterContract__c, SBQQ__StartDate__c, Revenue_Type__c, Record_Type_Dev_Name__c, Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c, SBQQ__Opportunity2__c from SBQQ__Quote__c where id = :quoteForTest.id];
				System.debug(quoteForTest+'Trying');
				SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForSelect.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050,20, '12345', null);
				Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', '12345', '23423', null);
				List<Asset> assets = [select Id, Quote__c from Asset where Quote__c =: quoteForSelect.Id];
				System.assertEquals(0, assets.size());

				//After all test data is inserted, triggers are re-enabled
				Semaphores.SetAllSemaphoresToFalse();

				AssetUtil au = new AssetUtil();
				Set<Id> quotesToCreateAssetsFor = new Set<Id>();
				quotesToCreateAssetsFor.add(quoteForSelect.Id);
				Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
				quotesMap.put(quoteForSelect.Id, quoteForSelect);
				List<Sales_Channel__c> contractAssetLink = [Select Id from Sales_Channel__c];
				System.assertEquals(0,contractAssetLink.size());
				au.createAsset(quotesToCreateAssetsFor, quotesMap);
				 assets = [select Id, Quote__c from Asset where Quote__c =: quoteForSelect.Id];
				System.debug(assets);
				contractAssetLink = [Select Id from Sales_Channel__c];
				System.assertEquals(1, assets.size());
				System.assertEquals(1,contractAssetLink.size());

			Test.stopTest();

        }

    }

}