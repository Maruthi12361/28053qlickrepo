/*
* File CheckTriggerPortalControlTest
    * @description : Unit test for CheckTriggerPortalControl Trigger
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       19.12.2017   Pramod Kumar V         Created Class

*/
@isTest(seeAlldata=false)
private class CheckTriggerPortalControlTest{
    
   
    static testMethod void test_CheckTriggerPortalControl() {
    
           Id rtId2= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
           Subsidiary__c subs = QuoteTestHelper.createSubsidiary();    
           QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
           Account testAccount = new Account(QlikTech_Company__c ='QTcomp', name='Test AccountName',recordtypeid=rtId2,BillingCountry='India',Payment_Terms__c = '45 days',Navision_Status__c = 'Partner');
           insert testAccount;
          ULC_Details__c Detail =new ULC_Details__c (ULCName__c='test', ULC_Password__c ='test');
          insert Detail ;
          Contact conPartner = QuoteTestHelper.createContact(testAccount.id);
          conPartner.TriggerPortalControl__c = true;
          insert conPartner;
          //ULC_Details__c Detail =new ULC_Details__c (ULCName__c='test', ULC_Password__c ='test',ContactId__c=conPartner.id);
          //insert Detail ;
          //ULC_Details__c Detail2=[select ULCName__c, ULC_Password__c from ULC_Details__c where ContactID__c in :conPartner]
          }    
}