/**
*	Class Name: AccountSharingBatch
*	2018-04-01 | AYS | BMW-739 | Created 
*/
global class AccountSharingBatch implements Database.Batchable<Id> {
	
	List<Id> l_endUsersIds;

	global AccountSharingBatch(List<Id> l_endUsersIds) {
		this.l_endUsersIds = l_endUsersIds;
	}
	
	global Iterable<Id> start(Database.BatchableContext BC) {
		return l_endUsersIds;
 	}

   	global void execute(Database.BatchableContext BC, List<Id> scope) {
 		AccountSharing.updateAccountSharing((List<Id>)scope);
	}
	
	global void finish(Database.BatchableContext BC) {

		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] to = new string[] {UserInfo.getUserEmail()};
        email.setToAddresses(to);
		email.setSubject(Label.BMW739_batchFinishedHeader);     
        email.setPlainTextBody(Label.BMW739_batchFinished);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });

	}
	
}