/*******************************************************************
* Class CaseQuickCloseTest
*
* Test class for CaseQuickClose
* 
* Change Log:
* 2014-02-05   AIN (Andreas Nilsson)	 	Initial Development
*											CR# 10123 #Quick quick Close option
**********************************************************************/

@isTest
//Tests the trigger CaseQuickClose.trigger which triggers on insert and update.
private class CaseQuickCloseTest {

	private static testmethod void TestQuickCloseInsert() {
		
		boolean allTestsSucceeded = false;
		List<CaseHolder> caseHolders = new List<CaseHolder>();
		List<Case> casesForInsert = new List<Case>();

		//Create a list of case holders, containing an empty case with the close case field set to the first parameter.
		//the second parameter is a list of the expected result to match with the case after it has been updated by the function
		caseHolders.add(new CaseHolder('New User Creation', new List<string>{'Closed','User Creation/Modification','User Creation', 'General Query'},true));
		caseHolders.add(new CaseHolder('User Termination', new List<string>{'Closed','User Creation/Modification','User Termination', 'General Query'},true));
		caseHolders.add(new CaseHolder('Webex', new List<string>{'Closed','WebEx','WebEx', 'Webex'},true));
		caseHolders.add(new CaseHolder('ULC Details Sent', new List<string>{'Closed','ULC Related','SFDC', 'ULC Issues'},true));
		caseHolders.add(new CaseHolder('Duplicate Case', new List<string>{'Closed','Duplicate Case', null, null},true));
		caseHolders.add(new CaseHolder('', new List<string>{'New',null, null, null},true));
		caseHolders.add(new CaseHolder('Not found', new List<string>{'New',null,null,null},true));

		//Add cases to a list for insert
		for(Caseholder ch : caseHolders)
			casesForInsert.add(ch.caseObj);

		//Insert all cases at once, Quick Close Case is set to the value in Caseholder.QuickCloseCase, all relevant fields should automatically update
		insert casesForInsert;

		test.startTest();
		system.debug('----------------------');
		system.debug('- Running insert tests');
		system.debug('----------------------');
		allTestsSucceeded = TestQuickClose(caseHolders);		
		test.stopTest();
		system.debug('- All tests succeeded: ' + allTestsSucceeded);

		system.assert(allTestsSucceeded, 'Test unsuccessful!');
	}
	
	private static testmethod void TestQuickCloseUpdate() {

		boolean allTestsSucceeded = false;
		List<CaseHolder> caseHolders = new List<CaseHolder>();
		List<Case> casesForInsert = new List<Case>();

		//Create a list of case holders, containing an empty case with the close case field set to the first parameter.
		//the second parameter is a list of the expected result to match with the case after it has been updated by the function
		caseHolders.add(new CaseHolder('New User Creation', new List<string>{'Closed','User Creation/Modification','User Creation', 'General Query'}, false));
		caseHolders.add(new CaseHolder('User Termination', new List<string>{'Closed','User Creation/Modification','User Termination', 'General Query'}, false));
		caseHolders.add(new CaseHolder('Webex', new List<string>{'Closed','WebEx','WebEx', 'Webex'}, false));
		caseHolders.add(new CaseHolder('ULC Details Sent', new List<string>{'Closed','ULC Related','SFDC', 'ULC Issues'}, false));
		caseHolders.add(new CaseHolder('Duplicate Case', new List<string>{'Closed','Duplicate Case', null, null}, false));
		caseHolders.add(new CaseHolder('', new List<string>{'New',null, null, null}, false));
		caseHolders.add(new CaseHolder('Not found', new List<string>{'New',null,null,null}, false));

		//Add cases to a list for insert
		for(Caseholder ch : caseHolders)
			casesForInsert.add(ch.caseObj);

		//Insert all cases at once
		insert casesForInsert;

		for(Caseholder ch : caseHolders)
			ch.UpdateCase();

		Update casesForInsert;			

		test.startTest();
		system.debug('----------------------');
		system.debug('- Running update tests');
		system.debug('----------------------');
		allTestsSucceeded = TestQuickClose(caseHolders);		
		test.stopTest();
		system.debug('- All tests succeeded: ' + allTestsSucceeded);

		system.assert(allTestsSucceeded, 'Test unsuccessful!');
	}
	private static testmethod void TestQuickCloseCaseDeleted() {
		
		//Seperate function for failing test
		boolean allTestsSucceeded = false;
		CaseHolder failingCaseHolder = new CaseHolder('New User Creation', new List<string>{'Closed','User Creation/Modification','User Creation', 'General Query'}, true);
		List<CaseHolder> caseHolders = new List<CaseHolder>();
		insert failingCaseHolder.CaseObj;
		delete failingCaseHolder.CaseObj;
		caseholders.add(failingCaseHolder);

		test.startTest();
		system.debug('----------------------');
		system.debug('- Running delete tests');
		system.debug('----------------------');
		allTestsSucceeded = TestQuickClose(caseHolders);		
		test.stopTest();
		system.debug('- All tests succeeded: ' + allTestsSucceeded);

		system.assert(!allTestsSucceeded, 'Test unsuccessful!');
	}

	private static boolean TestQuickClose(List<CaseHolder> caseHolders) {

		List<Id> casesIDs = new List<Id>();
		Case caseObj;
		CaseHolder caseHolderBeforeUpdate;
		boolean success = false;
		boolean allTestsSucceeded = true;
		integer returnedCaseCounter = 0;
 		
 		//Extracts the IDs for use in select 
		for(CaseHolder caseHolder : caseHolders)
		{
			casesIDs.add(caseHolder.CaseObj.Id);
		}

		//Selects the cases after trigger is run.
		for(Case caseAfterUpdate : [Select Id, Closure_Code__c, System__c, Reported_Issue__c, Quick_Close__c,Status from Case where Id in :casesIDs])
		{
			returnedCaseCounter++;
			caseHolderBeforeUpdate = null;
			//Searches the case holders for the selected case
			for(CaseHolder ch : caseHolders)
			{
				if(ch.CaseObj.Id == caseAfterUpdate.Id)
				{
					caseHolderBeforeUpdate = ch;
				}
			}

			//Checks if Case is found after update. If we have a match, we compare the results with the expected results
			if(caseHolderBeforeUpdate == null)
				return false;

			success = (caseAfterUpdate.Status == caseHolderBeforeUpdate.expectedResult[0] && caseAfterUpdate.Closure_Code__c == caseHolderBeforeUpdate.expectedResult[1] &&
				caseAfterUpdate.System__c == caseHolderBeforeUpdate.expectedResult[2] && caseAfterUpdate.Reported_Issue__c == caseHolderBeforeUpdate.expectedResult[3]);

			system.debug('------------------');
 			system.debug('- Running test with Quick Close: '+caseHolderBeforeUpdate.caseObj.Quick_Close__C);
			system.debug('------------------');
			system.debug('- Id: ' + caseHolderBeforeUpdate.caseObj.Id);
			system.debug('- Status          Value: ' + caseAfterUpdate.Status + ', Expected: ' + caseHolderBeforeUpdate.ExpectedResult[0] + ', Assert: ' + (caseAfterUpdate.Status == caseHolderBeforeUpdate.ExpectedResult[0]));
			system.debug('- Closure Code    Value: ' + caseAfterUpdate.Closure_Code__c + ', Expected: ' + caseHolderBeforeUpdate.ExpectedResult[1] + ', Assert: ' + (caseAfterUpdate.Closure_Code__c == caseHolderBeforeUpdate.ExpectedResult[1]));
			system.debug('- System          Value: ' + caseAfterUpdate.System__c + ', Expected: ' + caseHolderBeforeUpdate.ExpectedResult[2] + ', Assert: ' + (caseAfterUpdate.System__c == caseHolderBeforeUpdate.ExpectedResult[2]));
			system.debug('- Reported Issue  Value: ' + caseAfterUpdate.Reported_Issue__c + ', Expected: ' + caseHolderBeforeUpdate.ExpectedResult[3] + ', Assert: ' + (caseAfterUpdate.Reported_Issue__c == caseHolderBeforeUpdate.ExpectedResult[3]));
			system.debug('------------------');
			system.debug('- As expected: ' + success);
			system.debug('------------------');
			system.debug('');

			system.assert(caseAfterUpdate.Status == caseHolderBeforeUpdate.ExpectedResult[0]);
			system.assert(caseAfterUpdate.Closure_Code__c == caseHolderBeforeUpdate.ExpectedResult[1]);
			system.assert(caseAfterUpdate.System__c == caseHolderBeforeUpdate.ExpectedResult[2]);
			system.assert(caseAfterUpdate.Reported_Issue__c == caseHolderBeforeUpdate.ExpectedResult[3]);

			allTestsSucceeded = allTestsSucceeded && success;
		}

		return allTestsSucceeded && (returnedCaseCounter == caseholders.size()) ;
	}
	
	//Helper class that holds the value of the Quick_Close__c field, the Case object we're testomg on and the expected result from TestQuickClose
	private class CaseHolder
	{
		public Case CaseObj;
		private string QuickClose;
		public List<string> ExpectedResult;

		public CaseHolder(string QuickClose, List<string> ExpectedResult, boolean testInsert)
		{
			this.QuickClose = QuickClose;
			this.ExpectedResult = ExpectedResult;
			//If we're testing inserts, set Quick_Close__c when creating the record
			if(testInsert)
				InsertCase(QuickClose);
			else //If we're testing updates, insert a blank record
				InsertCase(null);
 		}
 		//Creates a new case with Quick_Close__C set to the provided string, on insert the trigger should fire and update relevant fields
		private void InsertCase(string quickClose)
		{
			CaseObj = new Case();
			CaseObj.Subject = 'Test CaseQuickClose';
			CaseObj.Quick_Close__C = quickClose;
			CaseObj.Closure_Code__c = null;
			CaseObj.System__c = null;
			CaseObj.Reported_Issue__c  = null;
			CaseObj.Status = 'New';
		}
		//Updates the Quick_Close__c field with the value of Quick_Close, on update the trigger should fire and update relevant fields
		public void UpdateCase()
		{
			CaseObj.Quick_Close__C = QuickClose;
		}
	}
}