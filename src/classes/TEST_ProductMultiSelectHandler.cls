/****************************************************************
*
*  TEST_ProductMultiSelectHandler 
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*****************************************************************/
@isTest
public class TEST_ProductMultiSelectHandler {
    public static testmethod void processTestingInsertBlank() {
       Id pricebookId = Test.getStandardPricebookId();
       
	   Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
	   
       List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
       insert listOfcreate;
        
       Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
       insert productForTest;
		
       PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;
        
        
       Product2 selectedProduct = [Select id, Visibility_Countries_A_I__c, Visibility_Countries_J_R__c, Visibility_Included_Countries__c, Visibility_Excluded_Countries__c, Country_Visibility__c from Product2 where id =: productForTest.id];
       System.debug(selectedProduct);
       System.assertEquals(false, String.isBlank(selectedProduct.Visibility_Countries_J_R__c));
    }
    
    public static testmethod void processTestingUpdateInclude() {
       Id pricebookId = Test.getStandardPricebookId();
        
       Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs; 
	   List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id)};
  	   insert listOfcreate;
        
       Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

       PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;
       productForTest.Visibility_Included_Countries__c = 'France;Afghanistan;Japan';
         
       update productForTest;
       
       
	   Product2 selectedProduct = [Select id, Visibility_Countries_A_I__c, Visibility_Included_Countries__c, Visibility_Excluded_Countries__c, Country_Visibility__c from Product2 where id =: productForTest.id]; 
       System.debug(selectedProduct);
        
       System.assertEquals('AF,FR,JP,', selectedProduct.Country_Visibility__c);  
    }
    
    public static testmethod void processTestingUpdateExclude() {
       Id pricebookId = Test.getStandardPricebookId();
        
	   Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs; 
	   List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
  	   insert listOfcreate;
        
       Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

       PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;
        
       productForTest.Visibility_Excluded_Countries__c = 'France;Afghanistan;Japan';
         
       update productForTest;
        
       Product2 selectedProduct = [Select id, Visibility_Countries_A_I__c, Visibility_Included_Countries__c, Visibility_Excluded_Countries__c, Country_Visibility__c  from Product2 where id =: productForTest.id]; 

       System.assertEquals(false, selectedProduct.Country_Visibility__c.contains('AF;'));  
    }
    
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
            								Country_Code_Two_Letter__c = countryAbbr, 
            								Country_Name__c = countryName,
            								NS_Country_Name__c = countryName, 
            								Subsidiary__c = subsId);
    return qlikTechIns;
    }
}