/******************************************************
ReferralTriggerController

Purpose:
-------
    Validate referral approval submission
======================================================

2020-03-11  DOB       OTQ-222 OTQ-221 Update Referral PAM Fields, Validate criteria before submitting for approval.
******************************************************/


public class ReferralTriggerController {
    
    public static void validateOpportunityAssociationBeforeApproval(List<Referral__c> refs, Map<Id, Referral__c> oldMap){
        Map<Id, Referral__c> approvedReferrals = new Map<Id, Referral__c>();
        for(Referral__c ref: refs){
            Referral__c oldRef = oldMap.get(ref.Id);
            if (oldRef.Referral_Status__c == Label.Status_Pending && ref.Referral_Status__c == 'Pending SSD Approval' && ref.Opportunity__c == null){ 
                    approvedReferrals.put(ref.Id, ref);
                }
        }
        for (ProcessInstance pi : [SELECT TargetObjectId, (  
                                       SELECT Id, StepStatus, Comments FROM Steps WHERE StepStatus = :Label.Status_Approved ORDER BY CreatedDate DESC LIMIT 1 
                                   ) FROM ProcessInstance WHERE TargetObjectId In  :approvedReferrals.keySet() ORDER BY CreatedDate DESC
                                  ]) {      
            if (pi.Steps.size()>0){ system.debug('validateOpportunityAssociationBeforeApproval'); approvedReferrals.get(pi.TargetObjectId).addError(Label.Must_Associate_Opportunity);}
        }
        
        
    }
    
    public static void validateTermsConditionsBeforeApproval(List<Referral__c> refs, Map<Id, Referral__c> oldMap){
        Map<Id, Referral__c> approvedReferrals = new Map<Id, Referral__c>();
        for(Referral__c ref: refs){
            Referral__c oldRef = oldMap.get(ref.Id);
            if (oldRef.Referral_Status__c == 'Draft' && ref.Referral_Status__c == 'Pending' && ref.Accepted_Terms_and_Conditions__c == false){ 
                    approvedReferrals.put(ref.Id, ref);
                }
        }
        system.debug('approvedReferrals ' + approvedReferrals);
        for (ProcessInstance pi : [SELECT TargetObjectId, 
                                   (  
                                       SELECT Id, StepStatus, Comments 
                                       FROM Steps
                                       WHERE StepStatus = 'Pending'
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1 
                                   )
                                   FROM ProcessInstance
                                   WHERE TargetObjectId In 
                                   :approvedReferrals.keySet()
                                   ORDER BY CreatedDate DESC
                                  ]) {     
            system.debug('pi.Steps' + pi.Steps);
            approvedReferrals.get(pi.TargetObjectId).addError('Terms and Conditions must be accepted before submitting for approval');
        }
        
    }
    
    public static void validateRejectionReasonForRejectedReferral(List<Referral__c> refs, Map<Id, Referral__c> oldMap){
        Map<Id, Referral__c> rejectedReferrals = new Map<Id, Referral__c>();
        for(Referral__c ref: refs){
            Referral__c oldRef = oldMap.get(ref.Id);
            if (oldRef.Referral_Status__c == Label.Status_Pending && ref.Referral_Status__c == Label.Status_Rejected && ref.Rejection_Reason__c == null){ 
                    rejectedReferrals.put(ref.Id, ref);
                }
        }
        for (ProcessInstance pi : [SELECT TargetObjectId, 
                                   (  
                                       SELECT Id, StepStatus, Comments 
                                       FROM Steps
                                       WHERE StepStatus = :Label.Status_Rejected
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1 
                                   )
                                   FROM ProcessInstance
                                   WHERE TargetObjectId In 
                                   :rejectedReferrals.keySet()
                                   ORDER BY CreatedDate DESC
                                  ]) {     
            if (pi.Steps.size()>0){ system.debug('validateRejectionReasonForRejectedReferral'); rejectedReferrals.get(pi.TargetObjectId).addError(Label.Must_Provide_Rejected_Reason);}
        }
        
    }
    
    //2020-03-11  DOB OTQ-222 OTQ-221 Update Referral PAM Fields
    public static void updatePAMFieldsInOppty(List<Referral__c> refs, Map<Id, Referral__c> oldMap){
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        Set<Id>opptyId = new Set<Id>();
        
        for(Referral__c ref: refs){
            if(ref.SSD_Approved__c == true && (ref.SSD_Approved__c != oldMap.get(ref.Id).SSD_Approved__c || ref.Won_Opportunity__c != oldMap.get(ref.Id).Won_Opportunity__c)) 
                opptyId.add(ref.Opportunity__c);
        }
        for(Opportunity opp: [SELECT Id, Referring_PSM_QM_Name_at_Closed_Won__c, Referring_PSM_QM_at_Closed_Won__c, Referring_PAM_at_Closed_Won_2__c, Referring_PAM_at_Closed_Won_2_ID__c, 
                              Referring_PAM_at_Closed_Won_3__c, Referring_PAM_at_Closed_Won_3_ID__c FROM Opportunity WHERE Stagename Like '%Closed Won%' and Id IN :opptyId]){ 
                oppMap.put(opp.Id, opp);                          
        }
        system.debug('oppMap -- ' + oppMap);
        if(oppMap.size() > 0){
            for(Referral__c ref: refs){
                if(ref.SSD_Approved__c == true && (ref.SSD_Approved__c != oldMap.get(ref.Id).SSD_Approved__c || ref.Won_Opportunity__c != oldMap.get(ref.Id).Won_Opportunity__c)) {
                    Opportunity oppty = oppMap.get(ref.Opportunity__c);
                    if(String.isBlank(oppty.Referring_PSM_QM_Name_at_Closed_Won__c)){
                        oppty.Referring_PSM_QM_Name_at_Closed_Won__c = ref.Partner_Fee_PAM__c;
                        oppty.Referring_PSM_QM_at_Closed_Won__c = ref.Partner_Manager__c;
                    }else if(String.isBlank(oppty.Referring_PAM_at_Closed_Won_2__c)){
                        oppty.Referring_PAM_at_Closed_Won_2__c = ref.Partner_Fee_PAM__c;
                        oppty.Referring_PAM_at_Closed_Won_2_ID__c = ref.Partner_Manager__c;
                    }else if(String.isBlank(oppty.Referring_PAM_at_Closed_Won_3__c)){
                        oppty.Referring_PAM_at_Closed_Won_3__c = ref.Partner_Fee_PAM__c;
                        oppty.Referring_PAM_at_Closed_Won_3_ID__c = ref.Partner_Manager__c;
                    }
                }
            }
            system.debug('updatePAMFieldsInOppty');
            update oppMap.values();
        }
        
    }
}