/**********************************************************
* Class: LiveChatTranscript
* Changes Log:
*   2018-02-13 ext_bad LCE-52 populate Case fields from LiveChatTranscript
*   2018-03-01 ext_bad LCE-17 change visibility of ChatTranscriptPost
*************************************************************/
public class LiveChatTranscriptHandler {
    static Map<ID, Schema.RecordTypeInfo> caseRecordTypeMap = Case.sObjectType.getDescribe().getRecordTypeInfosById();
    static final String LIVE_CHAT_RECORD_TYPE = 'Live Chat Support Record Type';
    static final String CHAT_TRANSCRIPT_POST = 'ChatTranscriptPost';
    static final String ALL_VISIBILITY = 'AllUsers';

    public static void populateCaseFields(List<LiveChatTranscript> transcripts, Map<Id, Case> casesMap) {
        for (LiveChatTranscript transcript : transcripts) {
            if (transcript.CaseId != null) {
                Case chatCase = casesMap.get(transcript.CaseId);
                chatCase.Chat_Start_Time__c = transcript.StartTime;
                chatCase.Chat_End_Time__c = transcript.EndTime;
                chatCase.Chat_Ended_By__c = transcript.EndedBy;

                if (LIVE_CHAT_RECORD_TYPE.equalsIgnoreCase(caseRecordTypeMap.get(chatCase.RecordTypeId).getName())) {
                    chatCase.OwnerId = transcript.OwnerId;
                }
            }
        }
        update casesMap.values();
    }

    public static void changeTranscriptPostVisibility(List<string> caseIds) {
        List<FeedItem> items = [
                SELECT Id, Visibility
                FROM FeedItem
                WHERE ParentId IN :caseIds AND Type = :CHAT_TRANSCRIPT_POST
        ];

        for (FeedItem item : items) {
            item.Visibility = ALL_VISIBILITY;
        }
        update items;
    }
}