/**
* QS_SupportAnnouncementsController 
*
* Change log:
*
* 16-11-2018 - ext_vos - CHG0034901: Add component for showing SupportAnnouncements.
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*
*/
public class QS_SupportAnnouncementsController {

    Integer announcementToReturn = 0;

    public List<Support_Announcement__c> announcements { get; set; }
    public List<Announcement> AnnouncementsList { get; set; }

    public String announcementHeader { get; set; }
    public String announcement { get; set; }
    public string test { get; set; }

    public QS_SupportAnnouncementsController() {
        test = 'Hello';
        loadAnnouncement();

    }

    private void loadAnnouncement() {
        
        AnnouncementsList = new List<Announcement> ();
        List<Support_Announcement__c> allAnnouncements = [SELECT Id, Announcement_Header__c, Announcement_Text__c, CreatedDate, Start_Date__c
                                                            FROM Support_Announcement__c
                                                            WHERE (Start_Date__c <= :system.today() 
                                                                    AND End_Date__c >= :system.today()
                                                                    AND Active__c = true) 
                                                                OR (Active__c = true AND End_Date__c = null) 
                                                            Order By Start_Date__c desc];

        if (allAnnouncements != null && allAnnouncements.size() > 0) {
            announcements = allAnnouncements;
            setNextAnnouncement();

            for(Support_Announcement__c sa : allAnnouncements)
                AnnouncementsList.add(new Announcement(sa.Announcement_Header__c, sa.Announcement_Text__c, sa.CreatedDate, sa.Start_Date__c));
        }
        else
            announcements = new list<Support_Announcement__c>();
    }
         
    public void setNextAnnouncement() {
        if (getHasAnnouncements()) {            
            announcementHeader = announcements[announcementToReturn].Announcement_Header__c;
            announcement = announcements[announcementToReturn].Announcement_Text__c;
            if (announcementToReturn < announcements.size() - 1) {
                announcementToReturn++;
            } else {
                announcementToReturn = 0;
            }
        }
    } 

    public boolean getHasAnnouncements() { 
        return announcements != null && announcements.size() > 0;
    }

    public Integer getNumberOfAnnouncements() {
        if (announcements != null ) {
            return announcements.size();
        } else {
            return 0;
        }
    }

    class Announcement
    {
        public string Header { get; set; }
        public string Body { get; set; }
        public Datetime CreatedDate { get; set; }
        public Datetime StartDate { get; set; }

        public Announcement(string Header, string Body, Datetime CreatedDate, Datetime StartDate) {
            this.Header = Header;
            this.Body = Body;
            this.CreatedDate = CreatedDate;
            this.StartDate = StartDate;
        }
    }
}