/*********************************************
Change Log:

2020-05-05 AIN IT-2776 added logic to update bug weight when related fields change. Covered by test class BugWeightRecalculatorTest
********************************************/
public class CaseBugWeightRecalculator {
    public static Map<Id, Bugs__c> bugsToUpdate = new Map<Id, Bugs__c>();
    public static void recalculateBugWeightBefore(List<Case> triggerNew, Map<Id, Case> triggerOldMap) {
        
        system.debug('CaseBugWeightRecalculator.recalculateBugWeightBefore start');
        Map<Id, Case> casesToRecalculate = new Map<Id, Case>();
        //record deleted, recalculate bug
        if(triggerNew == null){
            system.debug('Delete, recalculating bug');
            for(Case c : triggerOldMap.values()){
                if(c.Bug__c != null){
                    Bugs__c b = new Bugs__c();
                    b.Id = c.Bug__c;
                    if(!bugsToUpdate.containsKey(b.Id) && b.id != null)
                        bugsToUpdate.put(b.id, b);
                }
            }
        }
        //Insert, recalculate bug
        else if(triggerOldMap == null) {
            for(Case c : triggerNew){
                if(c.Bug__c != null){
                    Bugs__c b = new Bugs__c();
                    b.Id = c.Bug__c;
                    if(!bugsToUpdate.containsKey(b.Id) && b.id != null)
                        bugsToUpdate.put(b.id, b);
                }
            }
        }
        //update, recalculate on case by case basis
        else{
            for(Case c : triggerNew){
                //Bug weight on the case escalations has changed, update case bug weight and then send update to bug
                if(c.Bug_Weight_Recalculate__c) {
                    casesToRecalculate.put(c.id, c);
                    system.debug('Case to recalculate: ' + c.id);
                }
                //Bug weight on the case has changed, send update to bug
                else if (triggerOldMap.ContainsKey(c.Id)) {
                    //Bug has changed, update old bug and new bug
                    if(triggerOldMap.Get(c.Id).Bug__c != c.Bug__c) {
                        if(triggerOldMap.Get(c.Id).Bug__c != null){
                            system.debug('Previous bug to recalculate: ' + triggerOldMap.Get(c.Id).Bug__c);
                            Bugs__c b = new Bugs__c();
                            b.Id = triggerOldMap.Get(c.Id).Bug__c;
                            if(!bugsToUpdate.containsKey(b.Id) && b.id != null)
                                bugsToUpdate.put(b.id, b);
                        }
                        if(c.Bug__c != null){
                            system.debug('New bug to recalculate: ' + c.Bug__c);
                            Bugs__c b = new Bugs__c();
                            b.Id = c.Bug__c;
                            if(!bugsToUpdate.containsKey(b.Id) && b.id != null)
                                bugsToUpdate.put(b.id, b);
                        }
                    }
                    else if(triggerOldMap.Get(c.Id).Bug_Weight_Case__c != c.Bug_Weight_Case__c) {
                        Bugs__c b = new Bugs__c();
                        b.Id = c.Bug__c;
                        if(!bugsToUpdate.containsKey(b.Id) && b.id != null)
                            bugsToUpdate.put(b.id, b);
                    }                    
                }
                c.Bug_Weight_Recalculate__c = false;
            }
        }
        if(casesToRecalculate.size() > 0){
            Map<id, AggregateResult> escalations = new Map<Id, AggregateResult>([SELECT Case__c Id, max(Bug_Weight_Value__c) BugWeightValue FROM Qlikview_Support_Escalation__c where Case__c in :casesToRecalculate.keySet() group by Case__c]);
            system.debug('escalations.size(): ' + escalations.size());
            
            for(Id cid : casesToRecalculate.keySet()) {
                
                if(escalations.containsKey(cid)) {
                    system.debug('Getting bug weight from escalations');
                    for (AggregateResult escalation : escalations.Values()){
                        Id caseId = (id)escalation.get('Id');
                        if(caseId == cid){
                            Decimal escalationBugWeightValue = (decimal)escalation.get('BugWeightValue');
                            system.debug('escalation caseId: ' + caseId);
                            system.debug('escalation escalationBugWeightValue: ' + escalationBugWeightValue);
                            if(casesToRecalculate.get(caseId).Bug_Weight_Case_Escalation__c != escalationBugWeightValue){
                                casesToRecalculate.get(caseId).Bug_Weight_Case_Escalation__c = escalationBugWeightValue;
                                system.debug('Setting new bug weight on case ' + caseId + ' to ' + escalationBugWeightValue);
                                if(casesToRecalculate.get(caseId).Bug__c != null){
                                    Bugs__c b = new Bugs__c();
                                    b.Id = casesToRecalculate.get(caseId).Bug__c;
                                    if(!bugsToUpdate.containsKey(b.Id) && b.id != null)
                                        bugsToUpdate.put(b.id, b);
                                }
                            }
                        }
                    }
                }
                else{
                    if(casesToRecalculate.get(cid).Bug__c != null && casesToRecalculate.get(cid).Bug_Weight_Case_Escalation__c != 0){
                        Bugs__c b = new Bugs__c();
                        b.Id = casesToRecalculate.get(cid).Bug__c;
                        if(!bugsToUpdate.containsKey(b.Id) && b.id != null)
                            bugsToUpdate.put(b.id, b);
                    }
                    system.debug('Setting new bug weight on case ' + cid + ' to ' + 0);
                    casesToRecalculate.get(cid).Bug_Weight_Case_Escalation__c = 0;
                }
            }
        }
        system.debug('CaseBugWeightRecalculator.recalculateBugWeightBefore end');
    }
    public static void recalculateBugWeightAfter() {
        system.debug('CaseBugWeightRecalculator.recalculateBugWeightAfter start');
        if(bugsToUpdate != null && bugsToUpdate.size()>0){
            for(Bugs__c bug : bugsToUpdate.values()) {
                bug.Bug_Weight_Recalculate__c = true;
            }
            update bugsToUpdate.values();
        }
        system.debug('CaseBugWeightRecalculator.recalculateBugWeightAfter end');
    }
}