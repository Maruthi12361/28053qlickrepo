/************************************************************
* 2020-02-05 extcqb IT-2335 test class for PlanOfActionController
************************************************************/
@IsTest
private class PlanOfActionControllerTest {

    @IsTest(SeeAllData=true)
    static void testPlanOfActionPosting() {
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        Case theCase = new Case(Subject = 'Test Case', AccountId = testAccount.Id);
        insert theCase;

        PlanOfActionController planOfActionController = new PlanOfActionController(new ApexPages.StandardController(theCase));
        planOfActionController.problemHypothesis = 'test';
        planOfActionController.troubleshootingDone = 'test';
        planOfActionController.nextActionItems = 'test';
        planOfActionController.nextActionOwners = 'test';
        planOfActionController.followupCommitment = 'test';

        Test.startTest();
        planOfActionController.postPlanOfAction();
        Test.stopTest();

        FeedItem feedItem = [SELECT Visibility FROM FeedItem WHERE ParentId = :theCase.Id LIMIT 1];
        System.assertEquals('InternalUsers', feedItem.Visibility);
        theCase = [SELECT Id, PoA_Created__c FROM Case WHERE Id = :theCase.Id];
        System.assertNotEquals(null, theCase.PoA_Created__c);
    }

    @IsTest
    static void testPlanOfActionValidation() {
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        Case theCase = new Case(Subject = 'Test Case', AccountId = testAccount.Id);
        insert theCase;

        PlanOfActionController planOfActionController = new PlanOfActionController(new ApexPages.StandardController(theCase));
        planOfActionController.problemHypothesis = 'test';
        planOfActionController.followupCommitment = '';

        Test.startTest();
        planOfActionController.postPlanOfAction();
        Test.stopTest();

        Integer feedItemsNumber = [SELECT count() FROM FeedItem WHERE ParentId = :theCase.Id];
        System.assertEquals(0, feedItemsNumber);
    }
}