// 2015-05-05 TJG Created for CR 20082 https://eu1.salesforce.com/a0CD000000nqsw3 


@isTest 
private class TaskControllerExtensionTest { 

    static testMethod void test_taskCtrlExt() { 
        System.debug('TaskControllerExtensionTest.test_taskCtrlExt: Starting'); 
        //User stdUser = [select Id, CallCenterId from user where Profile__c like '%Marketing Std User%' and IsActive=true and CallCenterId<>null limit 1]; 
        List <CallCenter> centers = [select Id, AdapterUrl from CallCenter]; 
        CallCenter cCenter = null;
        if(centers.Size() > 0)
            cCenter = centers[0];
        else
            system.assert(false, 'No call center found');
        User stdUser = QTTestUtils.createMockUserForProfile('Custom: Marketing Std User');
        stdUser.CallCenterId = cCenter.Id;
        update stdUser;
        //CallCenter cCenter = [select AdapterUrl from CallCenter where Id = :stdUser.CallCenterId limit 1]; 
        //User nuUser = [select Id, CallCenterId from user where IsActive=true and CallCenterId=null limit 1]; 
        User nuUser = QTTestUtils.createMockUserForProfile('Custom: Marketing Std User');

        String baseUrl = cCenter.AdapterUrl; 

        if (baseUrl == null) { 
            System.debug('User has no call center adapterUrl.'); 
            return; 
        } 

        integer endIndex = baseUrl.IndexOf('/', 8); 

        String expected = endIndex < 0 ? baseUrl : baseUrl.Substring(0, endIndex); 

        Task myTask = new Task(Subject='Call',Status='New',OwnerId=stdUser.Id,Type='Call'); 
        insert myTask; 


        test.startTest(); 
        System.RunAs(nuUser){ 
            TaskControllerExtension tcExt = new TaskControllerExtension(new ApexPages.StandardController(myTask)); 

            String nullUrl = tcExt.getCaUrlBase(); 

            System.assertEquals(null, nullUrl); 

        } 

        System.RunAs(stdUser){ 
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(myTask); 

            TaskControllerExtension tce = new TaskControllerExtension(stdCtrl); 

            String urlBase = tce.getCaUrlBase(); 

            System.assertEquals(expected, urlBase); 
        } 
        test.stopTest(); 
        System.debug('TaskControllerExtensionTest.test_taskCtrlExt: Ended'); 
    } 
}