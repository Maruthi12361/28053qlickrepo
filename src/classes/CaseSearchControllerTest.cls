/**************************************************
* Author: Saúl Santos
*
* Change Log:
* 2015-10-29 Created        
* 
* 2016-01-25	TJG fix the error of FIELD_CUSTOM_VALIDATION_EXCEPTION, Account can be owned only by Master Reseller Partners.
**************************************************/
@isTest
private class CaseSearchControllerTest {	
	
	static testmethod void caseSearchControllerTest()
	{
		User u = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');
		Account Partner = QTTestUtils.createMockAccount('QGate Software Ltd', u, true);
		Account Customer = QTTestUtils.createMockAccount('Dako Denmark A/S', u, true);
        Case TheCase = new Case(Subject = 'Test Account', AccountId = Partner.Id);
		Case TheSecondCase = new Case(Subject = 'Test Account II', AccountId = Partner.Id);
		List<Case> toSave = new List<Case>();
		toSave.add(TheCase);
		toSave.add(TheSecondCase);
		insert toSave;
		
		Apexpages.currentPage().getParameters().put('CaseNumber', '');
	    Apexpages.currentPage().getParameters().put('Subject', 'Test Account II');
	    Apexpages.currentPage().getParameters().put('BugID', '');
		
		System.currentPageReference().getParameters().put('CaseId', TheCase.Id);
		
		test.startTest();
		
		CaseSearchController ThisTest = new CaseSearchController();	
		ThisTest.ThisCase = TheCase;
		ThisTest.runSearch();
		ThisTest.ThisCase.Problem_Case2__c = TheSecondCase.Id;
		ThisTest.relateCase();
		ThisTest.relateCase();
		ThisTest.backToParentCase();			
		ThisTest.toggleSort();
		System.assert(ThisTest.debugSoql != null); 
		System.assert(ThisTest.getCaseId() != null);
		test.stopTest();
		
	}
}