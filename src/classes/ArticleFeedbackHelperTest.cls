/**
*
* extbad    2019-09-17 IT-2037 Send an email to the 'Submitted_by'
* extcqb    2020-03-06 IT-2537 Select article version by article id
*/
@IsTest
private class ArticleFeedbackHelperTest {

    static testMethod void testSendFeedbackEmail() {
        Basic__kav articleVersion = new Basic__kav (
                Title = 'Test Article',
                Summary = 'Test Body',
                UrlName = 'testSendFeedbackEmail',
                Language = 'en_US'
        );
        insert articleVersion;

        Basic__kav currentArticleVersion = [
                SELECT KnowledgeArticleId
                FROM Basic__kav
                WHERE Id = :articleVersion.Id
        ];

        KbManagement.PublishingService.publishArticle(currentArticleVersion.KnowledgeArticleId, true);

        Case cs = new Case();
        cs.Submitted_by__c = UserInfo.getUserId();
        cs.Knowledge_Article__c = currentArticleVersion.KnowledgeArticleId;
        cs.Subject = 'test';
        cs.Feedback_to_submitter_Actions_taken__c = 'test';
        insert cs; 

        Test.startTest();
        String result = ArticleFeedbackHelper.sendFeedbackEmail(cs.Id);
        Test.stopTest();
 
        System.assertEquals(ArticleFeedbackHelper.RESULT_SUCCESS, result);
    }

    static testMethod void testSendFeedbackEmailWithUnpublishedArticle() {
        Case cs = new Case();
        cs.Submitted_by__c = UserInfo.getUserId();
        cs.Knowledge_Article__c = '234043034';
        cs.Subject = 'test';
        cs.Feedback_to_submitter_Actions_taken__c = 'test';
        insert cs;

        Test.startTest();
        String result = ArticleFeedbackHelper.sendFeedbackEmail(cs.Id);
        Test.stopTest();

        System.assertEquals(ArticleFeedbackHelper.RESULT_ARTICLE_NOT_PUBLISHED, result);
    }

}