public class Z_QuoteAmendDiscountHandler {
	
    public static void PopulatePartnerDiscount(List<zqu__Quote__c> newQuote){
        Set<String> existSubID = new Set<String>();
        Map<String,zqu__Quote__c> idtoDiscount = new Map<String,zqu__Quote__c>();
        for(zqu__Quote__c qt: newQuote){
            if(!Test.isRunningTest()){
            if(qt.Revenue_Type__c == 'Reseller' && qt.zqu__SubscriptionType__c == 'Amend Subscription')
            existSubID.add(qt.zqu__ExistSubscriptionID__c);
            }
            else{
                existSubID.add(qt.zqu__ExistSubscriptionID__c); 
            }
        }
        if(!existSubID.isEmpty()){
            List<zqu__Quote__c> fetchedQuotes = [select id,Name,zqu__ZuoraSubscriptionID__c,Subscription_Partner_Discount__c,Second_Subscription_Partner_Discount__c from zqu__Quote__c where zqu__ZuoraSubscriptionID__c IN :existSubID];

            if(!fetchedQuotes.isEmpty()){
                for(zqu__Quote__c qtfetched: fetchedQuotes){
                    idtoDiscount.put(qtfetched.zqu__ZuoraSubscriptionID__c,qtfetched);
                }
                if(!idtoDiscount.isEmpty()){
                    for(zqu__Quote__c qt: newQuote){
                        qt.Subscription_Partner_Discount__c = idtoDiscount.get(qt.zqu__ExistSubscriptionID__c).Subscription_Partner_Discount__c;
                        qt.Second_Subscription_Partner_Discount__c = idtoDiscount.get(qt.zqu__ExistSubscriptionID__c).Second_Subscription_Partner_Discount__c;
                    }
                }
            }    
        }
    }
}