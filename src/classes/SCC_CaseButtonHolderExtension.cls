/**
* Change log:
*
* 2018-03-05	ext_vos		CHG0031154: add 'To queue' logic.
* 2018-05-17	ext_vos		CHG0034020: fix status updating for "To Queue" functionality.
* 2019-10-16	ext_bad		IT-2185: Added Attunity queue.
* 2020-07-13    ext_cqb     IT-2939 Move public Article creation to the Community
*/
public with sharing class SCC_CaseButtonHolderExtension {
	private static final String NAME_BUTTON_PICKUP = 'Case Pickup';
	private static final String NAME_BUTTON_BACK = 'To Queue';
	private static final String TITLE_BUTTON_PICKUP = 'Case owner set to you, and status set to In process';
	private static final String TITLE_BUTTON_BACK = 'Case owner set to queue (Incident: Technical Product Support queue, ' +
			'Service Request: Customer Support queue, DSE: Designated Support Engineer queue, ' +
			'Attunity: Attunity Case Queue)';
	private static final String TYPE_INCIDENT = 'Incident';
	private static final String TYPE_SR = 'Service Request';
	private static final String PICKUP_STATUS = 'In Process';
	private static final String QUEUE_INCIDENT = 'Technical_Product_Support';
	private static final String QUEUE_SR = 'Qoncierge_Service';
	private static final String QUEUE_DSE = 'Designated_Support_Engineer';
	private static final String QUEUE_ATTUNITY = 'Attunity_Case_Queue';
	private static final String TEXT_ATTUNITY = 'Attunity';
	private static final String ERROR_DML = 'Error occurred during Case updating. Please, reload the page and try again.';
	private static final String NEW_PUBLIC_ARTICLE_URL = 'https://community.qlik.com/t5/tkb/articleeditorpage/tkb-id/qlik-support-knowledge-base';
	
	private final Case caseObj;	
	private Case currentCase;

	public String errorMsg;
	public Boolean isCanPickup {get; set;}
	public String pickupButtonTitle {get; set;}
	public String pickupButtonName {get; set;}

	public SCC_CaseButtonHolderExtension(ApexPages.StandardController stdController) {
        this.caseObj = (Case)stdController.getRecord();
        errorMsg = '';
        currentCase = [select OwnerId, Status, Type, Designated_Support_Engineer__c from Case where id = :caseObj.id];
        checkPickupButton(currentCase, currentCase.OwnerId);
    }

	public void CasePickup() {
		errorMsg = '';
		currentCase = [select Id, OwnerId, Status, Type, Designated_Support_Engineer__c, Attunity_Product__c, Area__c from Case where id = :caseObj.id];

		String ownerId;
		if (UserInfo.getUserId() == currentCase.OwnerId) {
			caseObj.Status = currentCase.Status;
			if (currentCase.Attunity_Product__c || (currentCase.Area__c != null && currentCase.Area__c.contains(TEXT_ATTUNITY))) {
				ownerId = getQueueIdByName(QUEUE_ATTUNITY);
			} else if (currentCase.Designated_Support_Engineer__c != null) {
				ownerId = getQueueIdByName(QUEUE_DSE);
			} else if (currentCase.Type == TYPE_INCIDENT) {
				ownerId = getQueueIdByName(QUEUE_INCIDENT);
			} else if (currentCase.Type == TYPE_SR) {
				ownerId = getQueueIdByName(QUEUE_SR);
			}
		} else {
			ownerId = UserInfo.getUserId();
			caseObj.Status = PICKUP_STATUS;
		} 
		if (ownerId != null) {
			caseObj.OwnerId = ownerId;
			try {
				update caseObj;
			} catch (DmlException ex){
				errorMsg = ERROR_DML;
			}			
		}	
		checkPickupButton(currentCase, ownerId);
	}

	public pageReference newPublicArticle() {
		PageReference pageRef = new PageReference(NEW_PUBLIC_ARTICLE_URL);
		pageRef.setRedirect(true);
		return pageRef;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public Case getCaseObj() {
		return caseObj;
	}
	
	public Boolean getIsCanPickup() {
		return isCanPickup;
	}

	private void checkPickupButton(Case cs, String ownerId) {
		if (UserInfo.getUserId() == ownerId) {
			pickupButtonTitle = TITLE_BUTTON_BACK;
			pickupButtonName = NAME_BUTTON_BACK;
			isCanPickup = cs.Designated_Support_Engineer__c != null || cs.Type == TYPE_INCIDENT || cs.Type == TYPE_SR;
		} else {
			pickupButtonTitle = TITLE_BUTTON_PICKUP;
			pickupButtonName = NAME_BUTTON_PICKUP;
			isCanPickup = true;
		} 
	}

	private String getQueueIdByName(String name) {
		String groupId;
		List<Group> grs = [select Id from Group where DeveloperName =: name limit 1];
		if (grs.size() > 0) {
			groupId = grs[0].Id;
		}
		return groupId;
	}
}