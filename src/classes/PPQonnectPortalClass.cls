/*************************************************************************************************************
 Name:PPMarketingPortalClass
 Author: Santoshi Mishra
 Purpose: This class controlls all the functionality of new partner portal
 Created Date : March 2012
 
 History Log:
 2012-08-??     MLN  Adding new code for Shareview and PPProductSupportRight
 2012-09-03     CCE  Adding a check to see if we are in production so we use sharevue production settings, if not test settings.
 2012-09-05     RDZ  Fixing issue with Key Map not found on ObjSummaryMap due to use labels instead of Object Names 
 2012-09-06     SM   Fixing issue to accept null values passed in the frameURL method.
 2012-09-26     MLN  Fixed UID in SharedVue URL (line 236) 
 2012-10-4      SM   Fixed mini Dashboard issue for Goal data in the method getBusinessInfo() - line 406
 2012-11-08     MLN  Adjusted code to use as test class for the most popular resource display issue
 2018-10-12     Cognizant  BSL-1040 Commented code as part of Clean Up Activity 
*************************************************************************************************************/
public with sharing class PPQonnectPortalClass {
    public String SubPage{get;set;}// Stores the parameter to render subpage.
    public User currUser {get;set;}// Stores logged in User info
    public Account acc {get;set;}// Stores partner portal User's Account info
    public List<Account> custList {get;set;}// Stores partner portal's cust list
    public Integer optyCounts {get;set;}// Stores Open Opporutnities Count
    public Decimal optyAmounts {get;set;}//Stores Open Opportunities Total Amount.
    public Decimal gpfAmounts {get;set;}//Stores Open GPFs Total Amount.
    public Decimal quarterGoal {get;set;}//Stores Open Opportunities Total Amount.
    public Decimal yearlyTarget {get;set;}//Stores business plan yearly target revenue.
    public String optyListViewId{get;set;}//Stores Open List View Id.
    public Integer caseCounts {get;set;}// Stores Open Cases Count
    public List<Case> caseList; // Stores case info related to Partner User
    public String caseListViewId{get;set;}//Stores Open Cases List View Id.
    public String newsListViewId{get;set;}//Stores news List View Id.
    public String eventsListViewId{get;set;}//Stores events List View Id.
    public String custListViewId{get;set;}//Stores Open Cases List View Id.
    public List <objectSummary>  ObjInfo{get;set;}// Inner class list which stores object navigation links and accessebility info
    public List <What_s_New__c> whatsNewList {get;set;} // Stores Whats New records
    public List <Upcoming_Events__c> upcoimngEventsList {get;set;} // Stores UpComing Events records
    public Map <String,objectSummary>  ObjSummaryMap{get;set;}// Stores object name versus accessebility info
    public Map<String,String> portalPublisherMap {get;set;}// Stores Portal Publishers redirect infos
    public List<String> openGPF= new List <String> {'Submitted','Approved'};
    public boolean showNews {get;set;}// Determines whether view all news should be visible
    public boolean showEvents {get;set;}// Determines whether view all events should be visible
    public Contact contactInfo {get;set;}// Stores partner portal User's Account info
    public String hashInfo {get;set;}// Stores SHA1 hashed info 
    public String sharedvueURL {get;set;}// Stores SHA1 hashed info   
    public static Contact testContact {get;set;}// It is executed from the test class for overcoming some limitations introduced due to addition of new frame url code.
    public PPQonnectPortalClass()
    {
         initialize();// intializes data on the basis of logged in User
    }
    public void initialize()// intializes data on the basis of logged in User
    {
         SubPage=System.currentPageReference().getParameters().get('SubPage');
         ObjSummaryMap= new Map <String,objectSummary>();
        if (UserInfo.getUserType().equals('PowerPartner')) // Determines if the user is Partner User
         {
             getUserInfo();// Retrieves User Info
          //   setHomelinks();//Sets Data for links on Home Page
         //    checkAccess();// Retrieves user profile permission on Salesforce objects
          //   getWhatsNew();// Retrieves What's New data
          //   getUpcomingEvent();// Retrieves Upcoming Events data
         //    if(System.test.isRunningTest()==false) // Test Class do not support getContent call. So, this checks skips createPortalPublisherMap when test class is executed to avoid test class error.
        //     createPortalPublisherMap();// Stores Portal Publisher URLs
        //     getBusinessInfo();// Pulls data from business plan object
           //  if(contactInfo!=null)
           //  {
               // frameURL();// Frames URL created by Marc. // commented this as this action is called from Marketing Page now.
           //  }
         }
    }
   /* public Integer getCurrentFiscalQuarter()// Determines the current fiscal quarter to set goal for quarter on Home Page.
    {
        Period p=[Select p.Number From Period p Where p.type = 'Quarter' and p.StartDate = THIS_FISCAL_QUARTER];
        return (p.Number);
    }Commented by SM on 10/4/2012 as current target field from business plan object is used instead of caluclating the current quarter revenue in code.*/
    
    /* 2018-11-26     Cognizant  BSL-1040 Commented code as part of Clean Up Activity
    public void createPortalPublisherMap() // Stores Portal Publisher URLs
    {
        portalPublisherMap = new Map<String,String>();
        portalPublisherMap.put('ForresterPromo',new PageReference( '/apex/PPCustomMessagePage?tabName=ForresterPromo').getContent().toString());
        portalPublisherMap.put('ppContent1',new PageReference( '/apex/PPCustomMessagePage?tabName=ppContent1').getContent().toString());
        portalPublisherMap.put('GartnerPromo',new PageReference( '/apex/PPCustomMessagePage?tabName=GartnerPromo').getContent().toString());
        portalPublisherMap.put('QlikLinks',new PageReference( '/apex/PPCustomMessagePage?tabName=QlikLinks').getContent().toString());
        portalPublisherMap.put('PPMarketingRight',new PageReference( '/apex/PPCustomMessagePage?tabName=PPMarketingRight').getContent().toString());
        portalPublisherMap.put('QdaysPromo',new PageReference( '/apex/PPCustomMessagePage?tabName=QdaysPromo').getContent().toString());
        portalPublisherMap.put('TimeMagazinePromo',new PageReference( '/apex/PPCustomMessagePage?tabName=TimeMagazinePromo').getContent().toString());
        portalPublisherMap.put('QlikeView10Promo',new PageReference( '/apex/PPCustomMessagePage?tabName=QlikeView10Promo').getContent().toString());
        portalPublisherMap.put('ppHomePromo',new PageReference( '/apex/PPCustomMessagePage?tabName=ppHomePromo').getContent().toString());
        portalPublisherMap.put('ppSubPage1',new PageReference( '/apex/PPCustomMessagePage?tabName=ppSubPage1').getContent().toString());
        portalPublisherMap.put('PPSalesRight',new PageReference( '/apex/PPCustomMessagePage?tabName=PPSalesRight').getContent().toString());
        portalPublisherMap.put('PPQonnectRight',new PageReference( '/apex/PPCustomMessagePage?tabName=PPQonnectRight').getContent().toString());
        portalPublisherMap.put('PPQlikMarketRight',new PageReference( '/apex/PPCustomMessagePage?tabName=PPQlikMarketRight').getContent().toString());
        portalPublisherMap.put('PPSalesLeft',new PageReference( '/apex/PPCustomMessagePage?tabName=PPSalesLeft').getContent().toString());
        portalPublisherMap.put('PPContentLeft',new PageReference( '/apex/PPCustomMessagePage?tabName=PPContentLeft').getContent().toString());
        portalPublisherMap.put('PPContentRight',new PageReference( '/apex/PPCustomMessagePage?tabName=PPContentRight').getContent().toString());
        portalPublisherMap.put('PPEducationRight',new PageReference( '/apex/PPCustomMessagePage?tabName=PPEducationRight').getContent().toString());
        portalPublisherMap.put('PPProductSupportRight',new PageReference( '/apex/PPCustomMessagePage?tabName=PPProductSupportRight').getContent().toString());
    }
    */
    
    public List<ContentVersion> getContentVersions() // Populates Most popular resources on the basis of most downloaded content in 6 months
    {
        Date prevDate= System.Today()-60; // Sets 2 months earlier date.
        List<String> contentList = new List<String>();// It stores the most downloaded contentIds.
        Map <Integer,List<String>> contentVersionMap = new Map<Integer,List<String>>(); // It stores the count versus content Ids.
        AggregateResult[] groupedResults;
        if(System.test.isRunningTest()==true)//checks to see if test class calls this.
        {
             groupedResults = [select contentVersionId,COUNT(Id) from ContentVersionHistory where field = 'contentVersionDownloaded' and createddate>:prevDate group by contentVersionid   limit 5];
        }
        else
        {
         groupedResults = [select contentVersionId,COUNT(Id) from ContentVersionHistory where field = 'contentVersionDownloaded' and createddate>:prevDate group by contentVersionid  limit 49900];
        }
        for (AggregateResult ar : groupedResults)  // Aggregate results are traversed and count vs ContentList is stored.
            {
                 Integer cnt=ar.get('expr0') == null? 0 : (Integer)ar.get('expr0'); 
                 if(!contentVersionMap.containskey(cnt))
                 {
                    contentVersionMap.put(cnt,new List<String>());
                 }
                 List <String> tmp =contentVersionMap.get(cnt);
                 tmp.add((String)ar.get('contentVersionId'));
                 contentVersionMap.put(cnt,tmp);
            }
        Integer mapCount=0;
        List <Integer> keySets = new List<Integer>();
        keySets.addALL(contentVersionMap.keySet());
        keySets.sort();// Sorts the counts in ascending orders.
        for(Integer i=keySets.size()-1;i>=Math.max(0,keySets.size()-9);i--) // List is traversed from down as most downloaded contents are at the bottom.
            {
                contentList.addAll(contentVersionMap.get(keySets[i]));
            }
        if(contentList.size()>8)// Incase more than 8 items are added remove them.
            {
                for(Integer i=8;i<contentList.size();i++)
                {
                    contentList.remove(i);
                    i--;
                }
            }
        // Queries the information for the most downloaded contents.
        Map<Id,ContentVersion> conMap= new Map<Id,ContentVersion> ([select id, Title, Description, FileType,Owner.Name, VersionNumber from ContentVersion Where id in :contentList]);
        List<ContentVersion> conList= new List<ContentVersion>();
        for(String str:contentList) // Queried items are added in the list on the basis of most downloaded to display the most downloaded first and so on.
            {
                conList.add(conMap.get(str));
            }
        return conList;
    }
    public void getWhatsNew() // Retrieves What's New data
    {
        whatsNewList=[Select w.Link__c, w.Id, w.Headline__c, w.Published_Date__c,w.Active__c From What_s_New__c w where w.Active__c=true order by w.Published_Date__c desc limit 6 ];
        if(whatsNewList.size()>5)// To ensure only 5 items are displayed on page and to render View more news link.
        {
            whatsNewList.remove(whatsNewList.size()-1);
            showNews=true;
        }
        else// if items are less than 5 view more news link is not displayed.
        {
            showNews=false;
        }
    }
    public void getUpcomingEvent()// Retrieves Upcoming Events data
    {
        upcoimngEventsList=[Select u.Name, u.Link__c, u.Id, u.Event_Name__c, u.Published_Date__c,u.Active__c From Upcoming_Events__c u where u.Active__c=true order by u.Published_Date__c desc limit 6 ];
        if(upcoimngEventsList.size()>5)// To ensure only 5 items are displayed on page and to render View more event link.
        {
            upcoimngEventsList.remove(upcoimngEventsList.size()-1);
            showEvents=true;
        }
        else// if items are less than 5 view more events link is not displayed.
        {
            showEvents=false;
        }
    }
    public void getUserInfo()// Retrieves User Info
    {
        List<Account> accList = new List<Account>();
        List<Contact> conLists = new List<Contact>();
        acc=new Account();
        custList= new List<Account>();
        currUser=[select AccountId,ContactId,FirstName,Id from User where id=:System.UserInfo.getUserId()];
        System.debug('checking user id---'+currUser.ContactId);
        conLists=[SELECT Id, Name, FirstName, LastName, Email, Phone, OwnerId, Account_Country_Code__c, Account_Street__c,Account_City__c, Account_State_Province__c, Account_Zip_Postal_Code__c, pse__Region__c FROM Contact Where Id=:currUser.ContactId limit 1];
        System.debug('checking conLists---'+conLists);
        if(!conLists.isempty())  
        {
            contactInfo=conLists[0];
        }
       else if(system.Test.isRunningTest())
        {
            contactInfo=testContact;
        }
        accList=[Select Partner_Type__c,Website, Billing_Country_Code_Country__c,First_Agreement_Date__c, Responsible_Partner__c, Name,CurrencyISOCode, Id From Account a where Id=:currUser.AccountId or Responsible_Partner__c=:currUser.AccountId];
        for(Account ac:accList )
        {
            if(ac.Id ==currUser.AccountId )
            {
                acc =ac; // Stores logged in partner's Account Id.
            }
            else if(ac.Responsible_Partner__c==currUser.AccountId)// Displays number of customer user for partners on home page.
            {
                custList.add(ac);
            }
        }
        
    }
    public void frameURL() // This code is added for making Marc's URL
    {
      
        System.debug('currUser = ' + currUser);
        System.debug('contactInfo = ' + contactInfo);
        //Added to allow URL selection based on Org Id
        string sSharedvueURL='http://qlikview-staging.sharedvue.net/sso/login/?pid='+currUser.AccountId;    //qttest
        if (UserInfo.getOrganizationId().startsWith('00D20000000IGPX')) //Live
        {
            sSharedvueURL='http://qlikview.sharedvue.net/sso/login/?pid='+currUser.AccountId;   //live          
        }
        String myData = currUser.AccountId +  contactInfo.Email + 'QlikViewSyndic8';
        Blob hash = Crypto.generateDigest('SHA1',Blob.valueOf(myData));
        hashInfo = EncodingUtil.convertToHex(hash).toUpperCase();
        sharedvueURL=sSharedvueURL;
         // 9-6-2012 SM - Adding null check to accept the null values in the URL formation.
         if(acc.Name!=null)
         {
            sharedvueURL=sharedvueURL+'&company='+EncodingUtil.urlEncode(acc.Name, 'UTF-8');
         }
         else
         {
            sharedvueURL=sharedvueURL+'&company='+EncodingUtil.urlEncode('NotSpecified', 'UTF-8');
         }
         if(acc.Website!=null)
         {
            sharedvueURL=sharedvueURL+'&url='+EncodingUtil.urlEncode(acc.Website, 'UTF-8');
         }
        else
        {
            sharedvueURL=sharedvueURL+'&url='+EncodingUtil.urlEncode('www.NotSpecified.com', 'UTF-8');  
        }

        sharedvueURL=sharedvueURL+'&uid='+System.UserInfo.getUserId();

        if(contactInfo.Email!=null)// Though this should always have value still adding check.
        {
            sharedvueURL=sharedvueURL+'&email='+EncodingUtil.urlEncode(contactInfo.Email, 'UTF-8'); 
        }
        else
        {
            sharedvueURL=sharedvueURL+'&email='+EncodingUtil.urlEncode('NotSpecified@qt.com', 'UTF-8'); 
        }
        if(contactInfo.FirstName!=null) 
        {
            sharedvueURL=sharedvueURL+'&first='+EncodingUtil.urlEncode(contactInfo.FirstName, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&first='+EncodingUtil.urlEncode('NotSpecified', 'UTF-8');
        }
        if(contactInfo.LastName!=null)
        {
            sharedvueURL=sharedvueURL+'&last='+EncodingUtil.urlEncode(contactInfo.LastName, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&last='+EncodingUtil.urlEncode('NotSpecified', 'UTF-8');
        }
        if(contactInfo.Phone!=null)
        {
            sharedvueURL=sharedvueURL+'&phone='+EncodingUtil.urlEncode(contactInfo.Phone, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&phone='+EncodingUtil.urlEncode('1111111111', 'UTF-8');
        }
        if(acc.Billing_Country_Code_Country__c!=null)
        {
            sharedvueURL=sharedvueURL+'&country='+acc.Billing_Country_Code_Country__c;
        }  
        else
        {
            sharedvueURL=sharedvueURL+'&country='+'NotSpecified';
        }
        if(contactInfo.Account_Country_Code__c!=null)
        {
            sharedvueURL=sharedvueURL+'&region='+EncodingUtil.urlEncode(contactInfo.Account_Country_Code__c, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&region='+EncodingUtil.urlEncode('NotSpecified', 'UTF-8');
        }
        if(contactInfo.Account_Street__c!=null)
        {
            sharedvueURL=sharedvueURL+'&address='+EncodingUtil.urlEncode(contactInfo.Account_Street__c, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&address='+EncodingUtil.urlEncode('NotSpecified', 'UTF-8');
        }
        if(contactInfo.Account_City__c!=null)
        {
            sharedvueURL=sharedvueURL+'&city='+EncodingUtil.urlEncode(contactInfo.Account_City__c, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&city='+EncodingUtil.urlEncode('NotSpecified', 'UTF-8');
        }
        if(contactInfo.Account_State_Province__c!=null)
        {
            sharedvueURL=sharedvueURL+'&state='+EncodingUtil.urlEncode(contactInfo.Account_State_Province__c, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&state='+EncodingUtil.urlEncode('NotSpecified', 'UTF-8');
        }   
        if(contactInfo.Account_Zip_Postal_Code__c!=null)
        {
            sharedvueURL=sharedvueURL+'&zip='+EncodingUtil.urlEncode(contactInfo.Account_Zip_Postal_Code__c, 'UTF-8');
        }
        else
        {
            sharedvueURL=sharedvueURL+'&zip='+EncodingUtil.urlEncode('00000', 'UTF-8');
        }
        
        sharedvueURL=sharedvueURL+'&v='+EncodingUtil.urlEncode(hashInfo, 'UTF-8');
                
    }
    public List<inner1> getTask() // Retrieves Task List
    {
     List<inner1> innerList = new List<inner1>();
     List<Task> tskList =[Select t.Status, t.OwnerId, t.Id, t.Description,t.Subject,t.WhoId,t.WhatId, t.ActivityDate, t.AccountId From Task t where t.IsClosed =false and  t.OwnerId = : currUser.Id order by t.ActivityDate];
     Integer counter=0;
        for(Task r:tskList)
        {
            inner1 i = new inner1(r,Math.mod(counter, 2));
            counter++;// This tracks the row is odd or even to provide odd row one type of styling and other to even rows.Thus all the odd rows and event rows are highlighted differently.
            innerList.add(i);
        }
        
        return innerList;
    }
    class inner1// This class does styling of rows for task on home page.
     {
        public Task t {get;set;}
        public integer c {get;set;}
        inner1(Task rw, Integer i)
          {
            t=rw;
            c=i;
          }
    }
    public void setHomelinks()// Sets Welcome Links on Home Page
    {
        Map<String,PPConfigSettings__c> listIds = PPConfigSettings__c.getall();// Custom Settings storing values for new Partner Portal.
        getOptyInfo();// Retrieves the open opportunities info
        optyListViewId= listIds.get('optyListView').Id__c;      
        getCaseInfo();// Retrieves open Cases. 
        caseListViewId= listIds.get('caseListView').Id__c;  
        custListViewId= listIds.get('custListView').Id__c; 
        newsListViewId=listIds.get('NewsListView').Id__c;
        eventsListViewId=listIds.get('eventsListView').Id__c;
        //2018-10-12 Cognizant BSL-1040 Commented code as part of Clean Up Activity
		//getGPF(); // Retrieves total fund amounts for open GPFs.
    }
    
    /* 2018-10-12 Cognizant BSL-1040 Commented code as part of Clean Up Activity*/
	public void getGPF() // Retrieves total fund amounts for open GPFs.
    {
        /* AggregateResult OpenGPFs ;
             try{
                    OpenGPFs = 
                    [Select  Sum(g.CTSK_PFM__Funding_Approved__c) totalAmount From CTSK_PFM__Fund_Request__c g where (g.CTSK_PFM__Account__c= :currUser.AccountId) and g.CTSK_PFM__Status__c  in :openGPF];
               }catch(Exception e)
                {
                    System.debug('exception.'+e);
                }    
             if(OpenGPFs!=null)
                {
                     gpfAmounts=OpenGPFs.get('totalAmount') == null? 0 : (Decimal)OpenGPFs.get('totalAmount'); // Stores sum of Amounts of open fund requests.
             
                }*/
    }
	
    
    public void getCaseInfo()//Retrieves case info related to current user
    {
        //List<Case> caseList=[select Id from Case where IsClosed=false  and ContactId =:currUser.ContactId];
        List<Case> caseList=[select Id from Case where IsClosed=false  and ContactId =:currUser.ContactId and RecordType.Name in ('QlikTech Master Support Record Type','QT Support Customer Portal Record Type', 'QT Support Partner Portal Record Type')];
        caseCounts=caseList.size();
    }
    
    public void getOptyInfo()   //Retrieves opty info related to current user
    {
          AggregateResult OpenOptys ;
             try{
                 OpenOptys = 
                    [Select Count(o.Id) OptyCount,Sum(o.Amount) totalAmount From Opportunity o where (o.Referring_Partner__c= :currUser.AccountId  OR o.Sell_Through_Partner__c=:currUser.AccountId) and o.IsClosed=false];
              
                }catch(Exception e)
                {
                    System.debug('exception.'+e);
                }   
                if(OpenOptys!=null)
                {
                    System.Debug(OpenOptys.get('OptyCount'));
                    System.Debug(OpenOptys.get('totalAmount'));
                    Decimal temp = OpenOptys.get('OptyCount') == null? 0 : (Decimal)OpenOptys.get('OptyCount');// Stores number of open optys.
                    optyAmounts = OpenOptys.get('totalAmount') == null? 0 : (Decimal)OpenOptys.get('totalAmount'); // Stores sum of Amounts for the related opn optys.
                    OptyCounts = Integer.Valueof(temp); 
                } 
                System.Debug('Opp: ' + optyAmounts + ' & ' + OptyCounts);
    }
    
    public void getBusinessInfo()   //Retrieves opty info related to current user
    {
        //SM- 10/4/2012-Changing the SOQL query inorder to fix minidashboard issue.Adding filter condition to retrieve records valid for only current year using ValidFrom Date Field.
        
          List<Business_Plan__c> bsRev ;
             try{
                  bsRev = 
                        [Select b.Yearly_Revenue_Target__c,b.Current_Target__c From Business_Plan__c b where b.Partner_Account__c= :currUser.AccountId and b.Valid_From__c =THIS_YEAR];
                }catch(Exception e)
                {
                    System.debug('exception.'+e);
                }
                if(!bsRev.isempty())
                {
                         yearlyTarget = bsRev[0].Yearly_Revenue_Target__c;
                         quarterGoal=bsRev[0].Current_Target__c;
                         
                } 
    }
    public void redirect()// Retrieves SubPage info to render appropriate Sub Page on Main Page
    { 
        SubPage=System.currentPageReference().getParameters().get('SubPage');
    }
    public void checkAccess() // Retrieves user profile permission on Salesforce objects
    {
        Map <String, Schema.SObjectType> objIn= Schema.getGlobalDescribe();
        ObjInfo = new List<objectSummary>();
        for(String str: objIn.keySet())
        {
             Schema.DescribeSObjectResult res = objIn.get(str).getDescribe();
            //if((res.getName().contains('Solution_Profiles')||res.getlabel()=='Lead' ||res.getlabel()=='Case' ||res.getlabel()=='Task'||res.getlabel()=='Event'|| res.getlabel()=='Account'|| res.getlabel()=='Contact'|| res.getlabel()=='Opportunity'|| res.getlabel()=='Report'))
            //Modify by RDZ As the label gets translated, using getName instead of getlabel
            System.debug('---------------------------');
            System.debug('res.getName(): ' + res.getName());
            System.debug('---------------------------');
            
            if((res.getName().contains('Solution_Profiles')||res.getName()=='Lead' ||res.getName()=='Case' ||res.getName()=='Task'||res.getName()=='Event'|| res.getName()=='Account'|| res.getName()=='Contact'|| res.getName()=='Opportunity'|| res.getName()=='Report'))
            {
                String url='';
                Boolean vsbl;
                if(res.isCreateable()==true) // Checks if the user has create access to redirect to standard object creation page.
                {
                    if(!res.getRecordTypeInfos().isempty()  && res.getRecordTypeInfos().size()==1)// This block creates url to redirect to standard object creation page for object which do not have any record type.
                    {
                        URL='/'+res.getKeyPrefix()+'/e?retURL=%2Fapex%2FPPhome';
                    }
                    else if(res.getName()=='Event' || res.getName()=='Task')// This block creates url for task having record type to redirect to standard object creation page.
                    {
                        URL='/setup/ui/recordtypeselect.jsp?ent='+res.getName()+'&retURL=%2Fapex%2FPPhome&save_new_url=%2F00T%2Fe%3FretURL%3D%2Fapex%2FppHome&saveURL=%2Fapex%2FppHome&save_new_url=%2Fapex%2FppHome';
                    }   
                    else // This block creates url for all the objects (except task and event) having record type to redirect to standard object creation page.
                    {
                        URL='/setup/ui/recordtypeselect.jsp?ent='+res.getName()+'&retURL=%2Fapex%2FPPhome&save_new_url=%2F'+res.getKeyprefix()+'%2Fe%3FretURL%3D%252F'+res.getKeyprefix()+'%252Fo';
                    }
                }
                        
                objectSummary obj = new objectSummary(res.getName(),res.getLabel(),res.getKeyPrefix(),res.isAccessible(),res.isCreateable(),res.isUpdateable(),URL);
                ObjInfo.add(obj);
                //By RDZ: Changing this to use Name instead of Label
                //ObjSummaryMap.put(res.getLabel(),obj);
                ObjSummaryMap.put(res.getName(),obj);
            
            }
        }
     
    }
    public pagereference newTask()// redirects to new task creation page
    {
        String URL=ObjSummaryMap.get('Task').URL;
        Pagereference p = new Pagereference(URL);
        return p;
    }
    public class objectSummary // This class stores the salesforce standard object permissions based on the current user loggged in.
    {
        public String Name {get;set;}// Stores object Name
        public String Label {get;set;}//Stores object label
        public String prefix {get;set;}// Stores object prefix.
        public Boolean isAccess {get;set;}// Stores read permission on object
        public Boolean iscreate {get;set;}//Stores create permission
        public Boolean isupdate {get;set;}//Stores edit permission
        public String URL {get;set;}//Stores the URL for corresponding object to redirect it to corresponding standard salesforce pages.
        public Boolean isVisible {get;set;}
        public objectSummary(String nm,String lbl,String key,Boolean acc,Boolean cre,Boolean upd,String UR)// Constructor to initialize the variables.
        {
            Name =nm;
            label=lbl;
            prefix=key;
            isAccess=acc;
            isCreate=cre;
            isupdate=upd;
            URL=UR;
            
        }
    }
}