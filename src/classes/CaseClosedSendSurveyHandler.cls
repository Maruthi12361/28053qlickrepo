/*
 File Name: CaseClosedSendSurveyHandler
 Moved from trigger CaseClosedSendSurvey on Case (after update, after insert)
 This trigger sends Surveys when the case has been closed.

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.
 2020-01-29 extbad IT-2347 Added Closure Code 'License Delivery' to exceptions, removed unused code

*/

public with sharing class CaseClosedSendSurveyHandler {
    private static final String CLOSED_STATUS = 'Closed';

    public static void handleCaseClosedSendSurvey(List<Case> newCases, Map<Id, Case> oldCasesMap) {

        Set<Id> sendSurveyRecordTypeIds = CaseUtils.querySendSurveyRecordTypeIds(); //CCE CR#6763

        Set<String> ClosureCodes = new Set<String>{
                'Duplicate Case',
                'No Response',
                'Pending Confirmation',
                'Test Case',
                'Spam',
                'Case Deflected',
                'License Delivery'
        };
        List<Case> filteredCases = new List<Case>();
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> CaseIdSet = new Set<Id>();
        for (Case c : newCases) {
            if (!sendSurveyRecordTypeIds.contains(c.RecordTypeId)
                    || c.Status != CLOSED_STATUS
                    || c.ContactId == null  //CCE CR#6763
                    || c.Suppress_Survey__c
                    || ClosureCodes.contains(c.Closure_Code__c)
                    || oldCasesMap != null && CLOSED_STATUS.equals(oldCasesMap.get(c.Id).Status)) {
                continue;
            }    //CCE (fzs41or BTN) CR#6567
            filteredCases.add(c);
            CaseIdSet.add(c.Id);
        }

//KMH - Added as part of support project
        Map<Id, Survey__c> SurveyList = new Map<Id, Survey__c>();

        for (Survey__c sur : [
                SELECT Id,Case_ID__c,Case_ID__r.ContactId
                FROM Survey__c
                WHERE Case_ID__c = :CaseIdSet
        ]) {
            SurveyList.put(sur.Case_ID__c, sur);
        }

        for (Case cas : filteredCases) {
            if (!SurveyList.containsKey(cas.Id)) {//Survey completed so exclude those cases/contacts
                contactIdSet.add(cas.ContactId);
            }
        }

        List<Contact> contactList = [
                SELECT QlikTech_Company__c, Id, Name, Email, Do_Not_Survey__c, MailingCountry,
                        Survey_Count__c, AccountId, RecordTypeId, Preferred_Language__c,
                        Survey_Date_1__c, Survey_Date_2__c, Survey_Date_3__c, LeadSource
                FROM Contact
                WHERE Id IN :contactIdSet AND Email != NULL AND Email != ''
        ];

        List<Contact> contactEmailList = new List<Contact>();
        for (Contact contact : contactList) {
            Boolean isEmployee = contact.AccountId == '0012000000I7QJh';
            if (isEmployee || contact.Do_Not_Survey__c
                    || contact.Survey_Date_1__c > Datetime.now().addMinutes(-1)) {
                continue;
            }

            contact.Survey_Date_3__c = contact.Survey_Date_2__c;
            if (contact.Survey_Date_1__c != null) {
                contact.Survey_Date_2__c = contact.Survey_Date_1__c.date();
            }

            contact.Survey_Date_1__c = Datetime.now();
            contact.Survey_Count__c = contact.Survey_Count__c == null ? 1 : contact.Survey_Count__c + 1;

            if (contact.LeadSource == null || contact.LeadSource == '') {//SAN CR6492
                filteredCases[0].addError('The Contact needs to be updated with a Lead Source before closing the Case ');
            } else {
                contactEmailList.add(contact);
            }
        }

        if (contactEmailList.size() > 0) {
            update contactEmailList;
            try {
                ContactServices.sendEmail(contactEmailList, filteredCases);
            } catch (EmailException e) { //CCE CR 9952
                filteredCases[0].addError('An email to the Case contact has likely previously bounced, '
                        + 'please make sure that the email address has been entered correctly and try to close the Case again. '
                        + '----------------------------------------------------------------------------------------------------- ' + e); //CCE CR# 11506
            } catch (Exception e) { //SAN CR6492 CCE CR 9952
                String msg = 'An error has occured with message: ' + e;
                filteredCases[0].addError(msg);
            }
        }
    }

}