/******************************************************

    Class: UpdateNFROppTimeLimitTest
    
    Initiator: CR# 16147 New Opportunity Record Type for NFR requests 
    
    Changelog:
        2015-07-10  MTM     Created file    
        2017-03-29  Roman Dovbush : Commented out QTTestUtils.GlobalSetUp() because we call createCustomSettings second time in CreateAccountForInternalUsers().
******************************************************/
@isTest
public class UpdateNFROppTimeLimitTest {
    
    static testMethod void testOpportunityTimeLimit() {
        SetUpData();
        Test.startTest();
        accountLicense.Opportunity__c = nfrOpp.Id;
        update accountLicense;
        nfrOpp = [Select Id,Time_Limit__c From Opportunity Where Id =: nfrOpp.Id];
        System.assertEquals(nfrOpp.Time_Limit__c, accountLicense.Time_Limit__c);
        Test.stopTest();        
    }

    
    private static void SetUpData()
    {
        //QTTestUtils.GlobalSetUp();
        User user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Sales Admin');
        Account account = QTTestUtils.CreateAccountForInternalUsers('NFR Test Account', user); 
        //createOpportunity(String sShortDescription, String sName, String sType, String sRevenueType, String sStage, String sRecordTypeId, String sCurrencyIsoCode, User user, Account acc)
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        nfrOpp = QTTestUtils.createOpportunity('NFRTests', 'NFR Test Account Reseller Not For Resale 20/07/2015', 'New Customer', 'Reseller Not For Resale','Open NFR' , NFRRecordType.id, 'GBP', user, account);
        accountLicense = QTTestUtils.createMockAccountLicense(account.Id);
    }
    Static Opportunity nfrOpp;

    Static RecordType NFRRecordType = [select Id, Name from RecordType where Name = 'Partner Not For Resale'];
    Static Account_License__c  accountLicense;
}