// Test case to test the triggers: CopyAssignment on pse__Assignment__c and PseProjectClosedTrigger on pse__Proj__c
// Changelog:
// 2013-10-14  Madhav Kakani - Fluido Oy Initial development for CR 9634
// 07.02.2017   RVA :   changing methods
// 2017-11-09   ext_vos  CR#CHG0031444 Add required fields for pse__Proj__c creation.
// 2019-02-15   ext_vos  INC0161360: Add test for populate child TimeCards.
@isTest
private class CopyProjectTriggerTest {
    public static QlikTech_Company__c qtc;
    public static Account act;
    public static Contact ct;
    public static pse__Permission_Control__c pc1;
    public static pse__Region__c rg;
    public static pse__Practice__c practice;
        
    static testMethod void TriggersTest() {
        setupTestData();
                                        
        // Create a test project
        pse__Proj__c proj1 = new pse__Proj__c();
        proj1.pse__Account__c = act.Id;
        proj1.Name = 'Test Project 1';
        proj1.Sales_Classification__c = 'Direct';
        proj1.pse__Region__c = rg.Id;
        proj1.pse__Project_Manager__c = ct.Id;
        proj1.pse__Stage__c = 'In Progress';
        proj1.pse__Start_Date__c = Date.today().addDays(-10);
        proj1.pse__End_Date__c = Date.today().addDays(30);
        proj1.Key_Engagement_Features__c = 'QlikView';
        proj1.Purpose_of_Engagement__c = 'Test';
        proj1.Sales_Classification__c = 'Internal';
        proj1.Customer_Critial_Success_Factors__c = 'Test';
        proj1.Invoicing_Type__c = 'Deferred';
        proj1.pse__Practice__c = practice.id;
        insert proj1;

        pse__Permission_Control__c pc2 = new pse__Permission_Control__c(pse__User__c = Userinfo.getUserId(),
                pse__Cascading_Permission__c = true, pse__Staffing__c = true, pse__Project__c = proj1.Id);
        insert pc2;
        
        Test.StartTest();

        // Check that a copy of the above project is created
        Project_SE__c projSE1 = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c = :proj1.Id];
        system.assert(projSE1 != null);
        
        // setup assignments
        List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();
        for (Integer i = 0; i < 2; i ++) {
            pse__Assignment__c assignment = new pse__Assignment__c(Name = 'Test Assignment ' + i, 
                    pse__Project__c = proj1.Id, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                    pse__Resource__c = ct.Id);
            assignments.add(assignment);
        }
        insert assignments; // this also copies the data to Assignment_SE__c
        
        List<Assignment_SE__c> clonedAssignments = [SELECT Id FROM Assignment_SE__c 
                                        WHERE Project_SE__c = :projSE1.Id];
        system.assert(clonedAssignments.size() == 2);

        // Delete the project and check that the copied project is deleted as well
        delete proj1;
        try {
            projSE1 = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c = :proj1.Id];
        } catch(QueryException e) {
            system.assert('List has no rows for assignment to SObject' == e.getMessage());
        }
 
        Test.StopTest();
  }

    // test the batch classes
    static testMethod void CopyBatchesTest() {
        setupTestData();
        
        // Create a test project
        pse__Proj__c proj1 = new pse__Proj__c();
        proj1.pse__Account__c = act.Id;
        proj1.Name = 'Test Project 1';
        proj1.Sales_Classification__c = 'Direct';
        proj1.pse__Region__c = rg.Id;
        proj1.pse__Project_Manager__c = ct.Id;
        proj1.pse__Stage__c = 'In Progress';
        proj1.pse__Start_Date__c = Date.today().addDays(-10);
        proj1.pse__End_Date__c = Date.today().addDays(30); 
        proj1.Key_Engagement_Features__c = 'QlikView';
        proj1.Purpose_of_Engagement__c = 'Test';
        proj1.Sales_Classification__c = 'Internal';
        proj1.Customer_Critial_Success_Factors__c = 'Test';
        proj1.Invoicing_Type__c = 'Deferred';
        proj1.pse__Practice__c = practice.id;
        insert proj1;

        pse__Permission_Control__c pc2 = new pse__Permission_Control__c(pse__User__c = Userinfo.getUserId(),
                pse__Cascading_Permission__c = true, pse__Staffing__c = true, pse__Project__c = proj1.Id);
        insert pc2;
        
        // setup assignments
        List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();
        for (Integer i = 0; i < 2; i ++) {
            pse__Assignment__c assignment = new pse__Assignment__c(Name = 'Test Assignment ' + i, 
                    pse__Project__c = proj1.Id, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                    pse__Resource__c = ct.Id);
            assignments.add(assignment);
        }
        insert assignments;
        
        Test.StartTest();

        CopyProjectsBatch cpb = new CopyProjectsBatch();           
        ID cpbID = Database.executeBatch(cpb, 1);

        CopyAssignmentsBatch cab = new CopyAssignmentsBatch();           
        ID cabID = Database.executeBatch(cab, 1);
                                 
        Test.StopTest();
  }

    static testMethod void BulkTriggersTest() {
        setupTestData();
                                        
        // Create a test projects and permission controls
        List<pse__Proj__c> lstProj = new List<pse__Proj__c>();
        for(Integer i = 0; i < 10; i++) {
            pse__Proj__c proj = new pse__Proj__c();
            proj.pse__Account__c = act.Id;
            proj.Name = 'Test Project 1';
            proj.Sales_Classification__c = 'Direct';
            proj.pse__Region__c = rg.Id;
            proj.pse__Project_Manager__c = ct.Id;       
            proj.pse__Stage__c = 'In Progress';
            proj.pse__Start_Date__c = Date.today().addDays(-10);
            proj.pse__End_Date__c = Date.today().addDays(30);
            proj.Key_Engagement_Features__c = 'QlikView';
            proj.Purpose_of_Engagement__c = 'Test';
            proj.Sales_Classification__c = 'Internal';
            proj.Customer_Critial_Success_Factors__c = 'Test';
            proj.Invoicing_Type__c = 'Deferred';
            proj.pse__Practice__c = practice.id;
            lstProj.add(proj);                    
        }

        insert lstProj;

        List<pse__Permission_Control__c> lstPC = new List<pse__Permission_Control__c>();
        Set<Id> projIds = new Set<Id>(); // list of project ids

        for(pse__Proj__c proj : lstProj) {
            projIds.add(proj.Id);

            pse__Permission_Control__c pc2 = new pse__Permission_Control__c(pse__User__c = Userinfo.getUserId(),
                pse__Cascading_Permission__c = true, pse__Staffing__c = true, pse__Project__c = proj.Id);
            lstPC.add(pc2);
        }
        insert lstPC;
        
        Test.StartTest();
        
        // Check that copies of the above projects are created
        List<Project_SE__c> lstProjSE = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c IN :projIds];
        system.assert(lstProjSE != null);
        system.assert(lstProjSE.size() == 10);       
        
        // setup assignments
        List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();
        for (pse__Proj__c proj : lstProj) {
            pse__Assignment__c assignment = new pse__Assignment__c(Name = 'Test Assignment ' + proj.Id, 
                    pse__Project__c = proj.Id, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                    pse__Resource__c = ct.Id);
            assignments.add(assignment);
        }
        insert assignments; // this also copies the data to Assignment_SE__c
        
        // check that the assignments are copied
        Set<Id> projSEIds = new Set<Id>(); // list of project SE ids
        for(Project_SE__c proj : lstProjSE) projSEIds.add(proj.Id);
  
        List<Assignment_SE__c> clonedAssignments = [SELECT Id FROM Assignment_SE__c 
                                        WHERE Project_SE__c IN :projSEIds];
        system.assert(clonedAssignments.size() == 10);

        // Delete the projects and check that the copied projects are deleted as well
        delete lstProj;
        try {
            List<Project_SE__c> lstProjSE1 = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c IN :projIds];
        } catch(QueryException e) {
            system.assert('List has no rows for assignment to SObject' == e.getMessage());
        }
        
        Test.StopTest();
  }

  static testMethod void updateChildTimeCardsTest() {
        setupTestData();
        Contact testResource = new Contact(LastName='test', pse__Resource_Role__c = 'Consultant', pse__Salesforce_User__c = UserInfo.getUserId(),
                                            pse__Is_Resource__c = true, pse__Is_Resource_Active__c = true);
        insert testResource;
        pse__Permission_Control__c pct = new pse__Permission_Control__c(pse__User__c = Userinfo.getUserId(),
                                                pse__Resource__c = testResource.Id, pse__Cascading_Permission__c = true, pse__Staffing__c = true);
        insert pct;
                                        
        // Create a test project
        pse__Proj__c proj1 = new pse__Proj__c();
        proj1.pse__Is_Active__c = true;
        proj1.pse__Account__c = act.Id;
        proj1.Name = 'Test Project 1';
        proj1.Sales_Classification__c = 'Direct';
        proj1.pse__Region__c = rg.Id;
        proj1.pse__Project_Manager__c = testResource.Id;
        proj1.pse__Stage__c = 'In Progress';
        proj1.pse__Start_Date__c = Date.today().addDays(-10);
        proj1.pse__End_Date__c = Date.today().addDays(30);
        proj1.Key_Engagement_Features__c = 'QlikView';
        proj1.Purpose_of_Engagement__c = 'Test';
        proj1.Sales_Classification__c = 'Internal';
        proj1.Customer_Critial_Success_Factors__c = 'Test';
        proj1.Invoicing_Type__c = 'Deferred';
        proj1.Time_Approval_Method__c = 'Project Manager';
      proj1.pse__Practice__c = practice.id;
        insert proj1;

        pse__Permission_Control__c pc = new pse__Permission_Control__c(pse__User__c = Userinfo.getUserId(),
                                                    pse__Cascading_Permission__c = true, pse__Staffing__c = true, 
                                                    pse__Project__c = proj1.Id);
        insert pc;
        
        Test.StartTest();
        // Check that a copy of the above project is created
        Project_SE__c projSE1 = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c = :proj1.Id];
        system.assert(projSE1 != null);
        
        // setup assignments
        pse__Schedule__c s1 = new pse__Schedule__c();
        s1.pse__Start_Date__c = Date.Today();
        s1.pse__End_Date__c = Date.Today().addDays(20);
        s1.pse__Monday_Hours__c = 8;
        s1.pse__Tuesday_Hours__c = 8;
        s1.pse__Wednesday_Hours__c = 8;
        s1.pse__Thursday_Hours__c = 8;
        s1.pse__Friday_Hours__c = 8;
        insert s1;
        pse__Assignment__c assignment = new pse__Assignment__c(Name = 'Test Assignment', pse__Schedule__c = s1.Id,
                                                pse__Project__c = proj1.Id, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                                                pse__Resource__c = testResource.Id, pse__Cost_Rate_Amount__c = 120);
        insert assignment;

        // setup Timecards
        List<pse__Timecard_Header__c> cards = new List<pse__Timecard_Header__c>();
        pse__Timecard_Header__c cardNew = new pse__Timecard_Header__c(
                                                pse__Start_Date__c = Date.Today(),
                                                pse__End_Date__c = Date.Today().addDays(6), 
                                                pse__Project__c = proj1.Id, pse__Resource__c = testResource.Id, pse__Assignment__c = assignment.Id,
                                                pse__Status__c = 'Submitted', pse__Sunday_Hours__c = 0.00,
                                                pse__Monday_Hours__c = 8.00, pse__Tuesday_Hours__c = 8.00,
                                                pse__Wednesday_Hours__c = 8.00, pse__Thursday_Hours__c = 8.00,
                                                pse__Friday_Hours__c = 8.00, pse__Saturday_Hours__c = 0.00,
                                                pse__Sunday_Notes__c = 'a', pse__Monday_Notes__c = '',
                                                pse__Tuesday_Notes__c = 'b', pse__Wednesday_Notes__c = '',
                                                pse__Thursday_Notes__c = 'c', pse__Friday_Notes__c = '',
                                                pse__Saturday_Notes__c = 'd', pse__Bill_Rate__c = 200.00,
                                                pse__Daily_Bill_Rate__c = false, pse__Daily_Cost_Rate__c = false, 
                                                pse__Billable__c = true, pse__External_Resource__c = false, pse__Include_In_Financials__c = true,
                                                pse__Approver__c = UserInfo.getUserId());
        cards.add(cardNew);
        pse__Timecard_Header__c cardApproved = new pse__Timecard_Header__c(
                                                pse__Start_Date__c = Date.Today(),
                                                pse__End_Date__c = Date.Today().addDays(6),
                                                pse__Project__c = proj1.Id, pse__Resource__c = testResource.Id, pse__Assignment__c = assignment.Id,
                                                pse__Status__c = 'Approved', pse__Sunday_Hours__c = 0.00,
                                                pse__Monday_Hours__c = 8.00, pse__Tuesday_Hours__c = 8.00,
                                                pse__Wednesday_Hours__c = 8.00, pse__Thursday_Hours__c = 8.00,
                                                pse__Friday_Hours__c = 8.00, pse__Saturday_Hours__c = 0.00,
                                                pse__Sunday_Notes__c = 'a', pse__Monday_Notes__c = '',
                                                pse__Tuesday_Notes__c = 'b', pse__Wednesday_Notes__c = '',
                                                pse__Thursday_Notes__c = 'c', pse__Friday_Notes__c = '',
                                                pse__Saturday_Notes__c = 'd', pse__Bill_Rate__c = 200.00,
                                                pse__Daily_Bill_Rate__c = false, pse__Daily_Cost_Rate__c = false, 
                                                pse__Billable__c = true, pse__External_Resource__c = false, pse__Include_In_Financials__c = true,
                                                pse__Approver__c = UserInfo.getUserId());
        cards.add(cardApproved);
        insert cards;

        Test.StopTest();

        // check Cost_Rate on cards
        pse__Assignment__c testAssignment = [select Id, pse__Cost_Rate__c from pse__Assignment__c where Id =: assignment.Id];
        
        pse__Timecard_Header__c testCard = [select Cost_Rate_PSE_Time__c from pse__Timecard_Header__c where Id =: cardNew.Id];        
        System.assert(testAssignment.pse__Cost_Rate__c.contains('' + testCard.Cost_Rate_PSE_Time__c.intValue()));

        testCard = [select Cost_Rate_PSE_Time__c from pse__Timecard_Header__c where Id =: cardApproved.Id];        
        System.assert(testAssignment.pse__Cost_Rate__c.contains('' + testCard.Cost_Rate_PSE_Time__c.intValue()));

        // update Assignment
        assignment.pse__Cost_Rate_Amount__c = 100;
        update assignment;

        testAssignment = [select Id, pse__Cost_Rate__c from pse__Assignment__c where Id =: assignment.Id];
        
        testCard = [select Cost_Rate_PSE_Time__c from pse__Timecard_Header__c where Id =: cardNew.Id];        
        //System.assert(testAssignment.pse__Cost_Rate__c.contains('' + testCard.Cost_Rate_PSE_Time__c.intValue()));
        
        testCard = [select Cost_Rate_PSE_Time__c from pse__Timecard_Header__c where Id =: cardApproved.Id];        
        System.assert(!testAssignment.pse__Cost_Rate__c.contains('' + testCard.Cost_Rate_PSE_Time__c.intValue()));      
    }
  
    static void setupTestData() {
		/*
		Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        qtc = new QlikTech_Company__c();
        qtc.name = 'SWE';
        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
        qtc.Country_Name__c = 'Sweden';
        qtc.CurrencyIsoCode = 'SEK';
		qtc.Subsidiary__c = testSubs.id;
        insert qtc;
		*/
		qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
			qtc.name = 'SWE';
			qtc.Country_Name__c = 'Sweden';
			qtc.CurrencyIsoCode = 'SEK';
		update qtc;
        act = new Account(name='Test Account');
        act.QlikTech_Company__c = 'QlikTech Nordic AB';
        act.Billing_Country_Code__c = qtc.Id;
        insert act;
       
        // setup resource
        ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
        ct.pse__Is_Resource_Active__c = true;
        ct.pse__Is_Resource__c = true;
        insert ct;

        // permission
        pc1 = new pse__Permission_Control__c(pse__User__c = Userinfo.getUserId(),
                pse__Resource__c = ct.Id, pse__Cascading_Permission__c = true, pse__Staffing__c = true);
        insert pc1;

        rg = new pse__Region__c();
        rg.Name = 'Sweden';
        insert rg;

        practice = new pse__Practice__c(Name = 'test');
        insert practice;
    }

}