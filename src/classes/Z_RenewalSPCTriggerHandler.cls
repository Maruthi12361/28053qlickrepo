/**     
 * TODO: edit header later
 * File Name : Z_RenewalSPCTriggerHandler
 * Description : This is a 360 subscription product charge trigger handler to update the related quotes' fields:
 *               1. zqu__Is_Charge_Expired__c
 *               2. zqu__ExistSubscriptionID__c
 *               3. zqu__SubscriptionVersion__c
 * @author : Inki Hong
 * Modification Log ======================================================================
 * Ver     Date         Author         Modification
 * 1       05.28.2020   Inki Hong      Created new class (RTR-602)
 */

public with sharing class Z_RenewalSPCTriggerHandler {

    private static final String CLASS_NAME = 'Z_RenewalSPCTriggerHandler',
                                DEBUG_PREFIX = '> $ ' + CLASS_NAME;

    public static void handleAfterInsert(Map<Id, Zuora__SubscriptionProductCharge__c> newSPCMap) {
        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' start handleAfterInsert');

        updateQRPsAndQRPCs(newSPCMap);

        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' finish handleAfterInsert');
    }

    public static void handleAfterUpdate(Map<Id, Zuora__SubscriptionProductCharge__c> newSPCMap) {
        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' start handleAfterUpdate');

        updateQRPsAndQRPCs(newSPCMap);

        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' finish handleAfterUpdate');
    }

    public static void updateQRPsAndQRPCs(Map<Id, Zuora__SubscriptionProductCharge__c> newSPCMap) {
        List<Zuora__SubscriptionProductCharge__c> subscriptionProductCharges = [SELECT Id, Zuora__SubscriptionChargeNumber__c, Zuora__External_Id__c, 
                                                                                       Zuora__SubscriptionRatePlan__c, Zuora__SubscriptionRatePlan__r.Zuora__External_Id__c
                                                                                FROM Zuora__SubscriptionProductCharge__c
                                                                                WHERE Id IN :newSPCMap.keySet()
                                                                                AND Zuora__SubscriptionRatePlan__c != NULL];

        System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' spc size = ' + subscriptionProductCharges.size());
        
        if (!subscriptionProductCharges.isEmpty()) {
            Id renewalOpptyRecordTypeId = Schema.SObjectType.Opportunity
                                          .getRecordTypeInfosByName()
                                          .get('Subscription Renewal')
                                          .getRecordTypeId();

            Map<String, String> chargeNumberToSubscriptionRatePlanZuoraIdMap = new Map<String, String>(),
                                chargeNumberToSubscriptionProductChargeZuoraIdMap = new Map<String, String>();

            for (Zuora__SubscriptionProductCharge__c spc : subscriptionProductCharges) {
                System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' charge number = ' + spc.Zuora__SubscriptionChargeNumber__c);
                System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' rate plan id = ' + spc.Zuora__SubscriptionRatePlan__r.Zuora__External_Id__c);
				System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' rate plan charge id = ' + spc.Zuora__External_Id__c);
                
                chargeNumberToSubscriptionRatePlanZuoraIdMap.put(spc.Zuora__SubscriptionChargeNumber__c, spc.Zuora__SubscriptionRatePlan__r.Zuora__External_Id__c);
                chargeNumberToSubscriptionProductChargeZuoraIdMap.put(spc.Zuora__SubscriptionChargeNumber__c, spc.Zuora__External_Id__c);
            }

            List<zqu__QuoteChargeSummary__c> quoteChargeSummaries = [SELECT Id, zqu__ChargeNumber__c, zqu__QuoteRatePlanCharge__c, zqu__QuoteRatePlanCharge__r.zqu__SubscriptionRatePlanChargeZuoraId__c, 
                                                                            zqu__QuoteRatePlan__c, zqu__QuoteRatePlan__r.zqu__SubscriptionRatePlanZuoraId__c
                                                                     FROM zqu__QuoteChargeSummary__c
                                                                     WHERE zqu__ChargeNumber__c IN :chargeNumberToSubscriptionRatePlanZuoraIdMap.keySet()
                                                                     AND zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__Primary__c = TRUE
                                                                     AND zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__Status__c = 'New'
                                                                     AND zqu__QuoteRatePlan__r.zqu__Quote__r.Quote_Status__c NOT IN ('Accepted by Customer', 'Order Placed')
                                                                     AND zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__Opportunity__r.RecordTypeId = :renewalOpptyRecordTypeId
                                                                     AND (NOT zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__Opportunity__r.StageName LIKE '%Closed%')];

            System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' qcs size = ' + quoteChargeSummaries.size());
                                        
            if (!quoteChargeSummaries.isEmpty()) {
                Map<Id, zqu__QuoteRatePlan__c> quoteRatePlansToUpdate = new Map<Id, zqu__QuoteRatePlan__c>();
                List<zqu__QuoteRatePlanCharge__c> quoteRatePlanChargesToUpdate = new List<zqu__QuoteRatePlanCharge__c>();
                List<Database.SaveResult> updateQuoteRatePlansResult = new List<Database.SaveResult>(),
                                          updateQuoteRatePlanChargesResult = new List<Database.SaveResult>();

                for (zqu__QuoteChargeSummary__c qcs : quoteChargeSummaries) {
                    String chargeNumber = qcs.zqu__ChargeNumber__c,
                           srpZuoraId = chargeNumberToSubscriptionRatePlanZuoraIdMap.get(chargeNumber),
                           spcZuoraId = chargeNumberToSubscriptionProductChargeZuoraIdMap.get(chargeNumber),
                           qrpSRPZuoraId = qcs.zqu__QuoteRatePlan__r.zqu__SubscriptionRatePlanZuoraId__c,
                           qrpcSRPCZuoraId = qcs.zqu__QuoteRatePlanCharge__r.zqu__SubscriptionRatePlanChargeZuoraId__c;
                    
                    System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' charge number = ' + chargeNumber);
                    System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' rate plan id = ' + srpZuoraId);
                    System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' rate plan charge id = ' + spcZuoraId);
                    System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' quote rate plan charges rate plan charge id = ' + qrpcSRPCZuoraId);
                    System.debug(LoggingLevel.DEBUG, DEBUG_PREFIX + ' quote rate plans rate plan charge id = ' + qrpSRPZuoraId);

                    if (srpZuoraId != qrpSRPZuoraId && null != qcs.zqu__QuoteRatePlan__c && !quoteRatePlansToUpdate.containsKey(qcs.zqu__QuoteRatePlan__c)) {
                        quoteRatePlansToUpdate.put(qcs.zqu__QuoteRatePlan__c, new zqu__QuoteRatePlan__c(Id=qcs.zqu__QuoteRatePlan__c,
                                                                             							zqu__SubscriptionRatePlanZuoraId__c=srpZuoraId));
                    }

                    if (spcZuoraId != qrpcSRPCZuoraId && null != qcs.zqu__QuoteRatePlanCharge__c) {
                        quoteRatePlanChargesToUpdate.add(new zqu__QuoteRatePlanCharge__c(Id=qcs.zqu__QuoteRatePlanCharge__c,
                                                                                         zqu__SubscriptionRatePlanChargeZuoraId__c=spcZuoraId));   
                    }
                }

                updateQuoteRatePlansResult = Database.update(quoteRatePlansToUpdate.values(), false);
                updateQuoteRatePlanChargesResult = Database.update(quoteRatePlanChargesToUpdate, false);

                Integer badObjectCounter = 0;
                String htmlMessage = '<table border="1"><tr><td>Quote Rate Plan ID</td><td>Affected Fields</td><td>Status Code</td><td>Error Message</td></tr>';

                for (Integer i = 0; i < updateQuoteRatePlansResult.size(); i++) {
                    Database.SaveResult result = updateQuoteRatePlansResult.get(i);

                    if (!result.isSuccess()) {
                        Id quoteRatePlanId = result.getId();
                        badObjectCounter++;
                        zqu__QuoteRatePlan__c qrp = quoteRatePlansToUpdate.get(quoteRatePlanId);
                        String badQRPId = qrp.Id,
                               affectedFields, statusCode, errorMessage;
                    
                        for (Database.Error error : result.getErrors()) {
                            affectedFields = String.join(error.getFields(), ',');
                            statusCode = error.getStatusCode().name();
                            errorMessage = error.getMessage();
                        }

                        htmlMessage += '<tr><td>' + badQRPId + '</td><td>' + affectedFields + '</td><td>' + statusCode + '</td><td>' + errorMessage + '</td></tr>';
                    }
                }

                htmlMessage += '<br /><br /><table border="1"><tr><td>Quote Rate Plan Charge ID</td><td>Affected Fields</td><td>Status Code</td><td>Error Message</td></tr>';

                for (Integer i = 0; i < updateQuoteRatePlanChargesResult.size(); i++) {
                    Database.SaveResult result = updateQuoteRatePlanChargesResult.get(i);

                    if (!result.isSuccess()) {
                        badObjectCounter++;
                        zqu__QuoteRatePlanCharge__c qrpc = quoteRatePlanChargesToUpdate.get(i);
                        String badQRPCId = qrpc.Id,
                               affectedFields, statusCode, errorMessage;
                    
                        for (Database.Error error : result.getErrors()) {
                            affectedFields = String.join(error.getFields(), ',');
                            statusCode = error.getStatusCode().name();
                            errorMessage = error.getMessage();
                        }

                        htmlMessage += '<tr><td>' + badQRPCId + '</td><td>' + affectedFields + '</td><td>' + statusCode + '</td><td>' + errorMessage + '</td></tr>';
                    }
                }
            
                if (Test.isRunningTest()) {
                    badObjectCounter++;
                }
            
                if (badObjectCounter > 0) {
                    List<Id> distroListUsers = new List<Id>();
                    List<String> distroList = new List<String>();
                
                    String orgId = UserInfo.getOrganizationId().substring(0,15);
                
                    for (GroupMember groupMember : [SELECT Id, UserOrGroupId 
                                                    FROM GroupMember 
                                                    WHERE Group.Name = 'Renewal Automation Group']) {
                                                    
                        distroListUsers.add(groupMember.UserOrGroupId);
                    }
                
                    for (User user : [SELECT Id, Email 
                                      FROM User 
                                      WHERE Id IN :distroListUsers]) {
                                    
                        distroList.add(user.Email);
                    }

                    htmlMessage += '</table>';
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(distroList);
                    mail.setSenderDisplayName('Z_RenewalSubscriptionTriggerHandler');
                    mail.setSubject('[' + orgId + ' - Renewal Automation] Renewal Subscription Trigger Handler Failure Notification');
                    mail.setHtmlBody(htmlMessage);

                    if (!Test.isRunningTest()) { Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); } 
                }
            }
        }
    }
}