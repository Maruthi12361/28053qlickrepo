/**
* Change log: 
*
* 2018-10-02    ext_vos CHG0034667: update list of attachments: add 'Attachment' files.
* 2019-05-05    AIN IT-1597 Updated for Support Portal Redesign
*/
public without sharing class QS_AttachmentListController {

    private static final String ERROR_MESSAGE = 'Something went wrong in retrieving the attachments. Please refresh the page and try again.';
    private static final String DOWNLOAD_URL_PART1 = '/sfc/servlet.shepherd/version/download/';
    private static final String DOWNLOAD_URL_PART2 = '?asPdf=false&operationContext=CHATTER';
    private static final String FILE_DOWNLOAD_URL = '/servlet/servlet.FileDownload?file=';

    public Id contentDocumentParentId {get; set;}
    
    public List<AttachmentList> attachmentList;
    
    private List<FeedItem> feedItemList;
    private List<Attachment> attachmentFileList;
    
    public Boolean showException {get; set;}
    public String errorMessageText {get; set;}
    
    public QS_AttachmentListController() {        
        getFeedItemList();
        getAttachmentList();
    }
    
    public List<FeedItem> getFeedItemList() {
        if (contentDocumentParentId != null) {
            showException = false;
            errorMessageText = '';
            try {
                feedItemList = [Select Visibility, Type, Title, ParentId, Id, ContentType, ContentFileName, RelatedRecordId 
                                From FeedItem 
                                where Visibility = 'AllUsers' 
                                    AND ParentId = :contentDocumentParentId 
                                    AND Type = 'ContentPost'];
           } catch (Exception ex) {
                showException = true;
                errorMessageText = ERROR_MESSAGE;
           }
        }        
        return feedItemList;
    }

    public List<Attachment> getAttachmentFiles() {
        if (contentDocumentParentId != null) {
            showException = false;
            errorMessageText = '';
            try {
                attachmentFileList = [Select Name, Id From Attachment where IsPrivate = false AND ParentId = :contentDocumentParentId];
            } catch (Exception ex) {
                showException = true;
                errorMessageText = ERROR_MESSAGE;
            }
        }        
        return attachmentFileList;
    }
    
    public List<AttachmentList> getAttachmentList() {
        String url = '';
        attachmentList = new List<AttachmentList>();
        // add list of attachments from chatter
        if (feedItemList == null || feedItemList.isEmpty()) {
            getFeedItemList();
        }
        if (feedItemList != null && !feedItemList.isEmpty()) {
            for (FeedItem feedItem : feedItemList) {
                if (feedItem.RelatedRecordId != null && feedItem.ContentType != null && feedItem.ContentFileName != null) {
                    url = DOWNLOAD_URL_PART1 + feedItem.RelatedRecordId + DOWNLOAD_URL_PART2;
                    attachmentList.add(new AttachmentList(feedItem.ContentFileName, url));
                }
            }
        }
        // add list of attached files
        if (attachmentFileList == null || attachmentFileList.isEmpty()) {
            getAttachmentFiles();
        }
        if (attachmentFileList != null && !attachmentFileList.isEmpty()) {
            for (Attachment attach : attachmentFileList) {
                if (attach.Name != null) {
                    url = FILE_DOWNLOAD_URL + attach.Id;
                    attachmentList.add(new AttachmentList(attach.Name, url));
                }
            }
        }
        return attachmentList;        
    }
    
    public class AttachmentList {
        
        public string url {get; set;}
        public String titleName {get; set;}        
        
        public AttachmentList(String titleName, string url) {
            this.titleName = titleName;
            this.url = url;
            
            
        }
    }
}