/**
 * 2014-03-14   TJG CR# 9027 https://eu1.salesforce.com/a0CD000000ZxGyw
 *              Verify that validation rules still work after the modifications
 * 2015-10-15   SLH CR# 41669 Removed references to OP024 validation rule 
 */
@isTest
private class TestOpportunityValidationRulesSellThrou {
    static final String ApiOnlyUserProfileId = '00e20000000zFfpAAE';    //Custom: Api Only User
    static final String OperationAdminProfId = '00e20000001ODnHAAW';    //Operations Administrator
    static final String StandardUserProfieId = '00e20000000yyV1AAI';    // Standard User
    static final String OppRecordTypeId = '01220000000J1KR'; //Qlikbuy CCS Standard
    //static final String OppRecordTypeId = '012f0000000CuglAAC'; //Qlikbuy CCS Standard II on QTNS
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    
    static testMethod void sellThroughPartnerTest() {
        QTTestUtils.GlobalSetUp();

        Boolean testSuccess = false;        
        User apiUser = [select Id from user where ProfileId = :ApiOnlyUserProfileId and alias <> 'qwebs' and IsActive=true limit 1];
        User opeUser = [select Id from user where ProfileId = :OperationAdminProfId and IsActive=true limit 1];
        Account TestResellerAccount = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Reseller Qlik Test Ltd.',
                Sub_Reseller__c = true,
                Navision_Status__c = 'Partner',
                QlikTech_Company__c = 'QlikTech Inc'                
            );
        insert TestResellerAccount;
 
         Contact TestContact = new Contact (
                AccountId = TestResellerAccount.Id,
                OwnerId = UserInfo.getUserId(),
                LastName = 'Wilson-Hemingway',
                FirstName = 'Daryl',
                Email = 'dwy@qlikview.com'
            );
            
        insert TestContact;
             
       Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(
            Name = 'Test Sell Through 1',
            RecordTypeId = AccRecordTypeId_EndUserAccount,
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Legal_Approval_Notes__c = 'Some legal stuff',
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
                
        Opportunity Opp = new Opportunity(
            RecordTypeId = OppRecordTypeId,
            Name = 'Test sell through Opp',
            CurrencyIsoCode = 'USD',
            Short_Description__c = 'Partner Reseller',
            StageName = 'Goal Identified',
            CloseDate = Date.today().addDays(7),
            Type = 'New Customer',
            AccountId = acc.Id,
            Sell_Through_Partner__c = TestResellerAccount.Id
        );

        System.RunAs(opeUser){
            String msg2find = 'An Account with Sub Reseller checked cannot be the Sell Through Partner';
            try {            
                insert Opp;
            } catch(DmlException ex) {
                String msg = ex.getMessage();
                testSuccess = msg.contains(msg2find);
                //System.assert(testSuccess, msg);
            }
            if (!testSuccess)
            {
                //System.assert(false, 'Test failed: ' + msg2find);
                return;
            }
            testSuccess = false;           
            //You need to add a Sell Through Partner and the Partner Contact to be able to set the Revenue Type = Reseller
            Opp.Revenue_Type__c = 'Reseller';
            Opp.Electronic_Acceptance__c = 'Yes';
            msg2find = 'An Account with Sub Reseller checked cannot be the Sell Through Partner'; 
            try {            
                insert Opp;
            } catch(DmlException ex) {
                String msg = ex.getMessage();
                testSuccess = msg.contains(msg2find);
                //System.assert(testSuccess, msg);
            }
            if (!testSuccess)
            {
                //System.assert(false, 'Test failed: ' + msg2find);
                return;
            }
            
            //Revenue Type Reseller must have the value Yes in Electronic Acceptance
            Opp.Sell_Through_Partner__c = Acc.Id;
            Opp.Partner_Contact__c = TestContact.Id;
            //Opp.Revenue_Type__c = 'Reseller';
            Opp.Electronic_Acceptance__c = 'No';
            msg2find = 'Revenue Type Reseller must have the value Yes in Electronic Acceptance';
            testSuccess = false;            
            try {            
                insert Opp;
            } catch(DmlException ex) {
                String msg = ex.getMessage();
                testSuccess = msg.contains(msg2find);
                //System.assert(testSuccess, msg);
            }
            if (!testSuccess)
            {
                //System.assert(false, 'Test failed: ' + msg2find);
                return;
            }
        }
        
        // API User is excluded from the validation rules, the insert, update should all work 
        System.RunAs(apiUser){
            Opp.Sell_Through_Partner__c = TestResellerAccount.Id;
            Opp.Revenue_Type__c = 'Reseller';
            Opp.Electronic_Acceptance__c = 'Yes';
            // An Account with Sub Reseller checked cannot be the Sell Through Partner
            try {            
                insert Opp;
            } catch(DmlException ex) {
                //System.assert(false, ex.getMessage());
            }
            
            //Revenue Type Reseller must have the value Yes in Electronic Acceptance

            Opp.Revenue_Type__c = 'Reseller';
            Opp.Electronic_Acceptance__c = 'No';                    
            try {            
                update Opp;
            } catch(DmlException ex) {
                //System.assert(false, ex.getMessage());
            }
            testSuccess = false;
            //You need to add a Sell Through Partner and the Partner Contact to be able to set the Revenue Type = Reseller
            Opp.Sell_Through_Partner__c = null;
            Opp.Electronic_Acceptance__c = 'Yes';
            try {            
                update Opp;
            } catch(DmlException ex) {
                //System.assert(false, ex.getMessage());
            }           
        }
    }
}