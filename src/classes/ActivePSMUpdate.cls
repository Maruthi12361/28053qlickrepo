/******************************************************

    Class: ActivePSMUpdate

    Changelog:
        2009-07-03  SSH     Created file
        2009-09-22  MBH     Added Referral PSM
        2012-09-20      Fluido  CR#5226 Exclude Academic Program Opportunity records from this trigger
        2013-03-07  TJG     Part of CR#7332 updates. Update
                                Active_EIS_on_Closed_Won_Name__c
                                Active_MIS_at_Closed_Won__c
                                Active_MIS_at_Closed_Won_Name__c
                                Referring_PSM_QM_at_Closed_Won
                                Referring_PSM_QM_Name_at_Closed_Won

        2013-04-04  SLH     CR# 7776 Qonnect Manager fields populated from Sell through account Qonnect Manager - now in OppUpdateAccOwnerName trigger
                                New Sell Through PSM/QM fields populated from ActivePSM
                            CR# 7775 Populate fields on chaneg to Closed Won only - allow people to edit the values

        2013-08-01  MTM     CR# 8422 Poplute PSM Nominated Id field as formula fields can not be created anymore due to limits

        2014-03-04  TJG     CR# 11205 Minor safety check added and make sure works for INSERT

        2014-03-07  TJG     CR# 10989 https://eu1.salesforce.com/a0CD000000f8AVT Sell Through Partner in the Deal Split Child record type should be read only

        2015-02-26  CAH     CR# 21620 rename CTW to QSG

        2015-07-20  SLH     CR# 45074 Populate Referring Sales Rep for OEM at Closed Won

        2017-03-28  Roman Dovbush (4front)  Fixing hardcoded profile IDs.

        2017-04-1-  MTM fix null reference errors
        2017-04-20  MTM fix List null errors QCW-2118
        2017-10-02  Viktor@4front QCW-3747 Sales Credit Information fields not populating upon Closed Won
******************************************************/
public class ActivePSMUpdate {


    /**********************************************************

        UpdatePSM

        -> opportunities - List of Opportunities

        Updates the Active_PSM__c and the Sell_Through_Partner__c
        with the Referring Contacts Owner.

    **********************************************************/
    private static final String SettingName = 'SteelbrickSettingsDetails';
    public static void UpdatePSM(Opportunity[] opportunities, Opportunity[] oldOpportunities)

    {
        system.debug('ActivePSMUpdate.UpdatePSM: In the code');

        List<String> Ids = new List<String>();
        List<String> uIds = new List<String>();
        Map<Id,Id> accOwnerUserId = new Map<Id,Id>();

        // EDIT 20.09.2012 : Exclude Academic Program Opportunity records from this trigger, Id from Custom Setting
        Id academidProgramOpportunityId;
        if(Academic_Program_Settings__c.getInstance('AcademicProgramSetting') != null) {
            academidProgramOpportunityId = Academic_Program_Settings__c.getInstance('AcademicProgramSetting').OpportunityRecordTypeId__c;
        }

        // 07-03-2014 CR# 10989. Prevent user from editting Partner Contact field for Deal Split Child opportunity
        Id dealSplitChildRecordTypeId;

        QTCustomSettings__c qtSettings = QTCustomSettings__c.getValues('Default');
        dealSplitChildRecordTypeId = qtSettings != null ? qtSettings.DealSplitChildRecordType__c :'01220000000DoEjAAK';
        if (null == dealSplitChildRecordTypeId)
        {
            dealSplitChildRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Deal_Split_Child').Id;
        }
        Steelbrick_Settings__c sbSetting = Steelbrick_Settings__c.getInstance(SettingName);

        Id currentUserProfileId = userinfo.getProfileId();
        if (null != oldOpportunities)
        {
            for (Integer i = 0; i < opportunities.size();i++)
            {
                Opportunity newOp = opportunities.get(i);
                Opportunity oldOp = oldOpportunities.get(i);
                //system.debug('newOps =' + newOp + ', oldOp=' + oldOp + ', recordTypeId=' + dealSplitChildRecordTypeId);
                // if partner contact is not blank, allow system administrator, operations administrator to modify but block anyone else
                if (sbSetting.Sys_Admin_ProfileId__c != currentUserProfileId
                    && !sbSetting.Ops_Admin_Profile_Id__c.contains(currentUserProfileId)
                    && oldOp.Partner_Contact__c != null && newOp.RecordTypeId == dealSplitChildRecordTypeId
                    && (newOp.Partner_Contact__c == null || newOp.Partner_Contact__c != oldOp.Partner_Contact__c))
                {
                    newOp.addError('Partner Contact is read only for Deal Split Child Opportunities.');
                }
            }
        }

        for (Opportunity o:opportunities){
            if(academidProgramOpportunityId != null && o.RecordTypeId == academidProgramOpportunityId){
                continue;
            }

            if (o.Partner_Contact__c !=null){
                ids.add(o.Partner_Contact__c);
            }
            //CR# 7332 Need to get Active_EIS and Active_MIS names via a map
            if (o.Active_EIS__c != null) {
                uids.add(o.Active_EIS__c);
            }
            if (o.Active_MIS__c != null) {
                uids.add(o.Active_MIS__c);
            }
            if(o.PSM_Nominated__c != null){
                o.PSM_Nominated_ID__c = o.PSM_Nominated__c;
            }
        }

        Map<Id, Contact> contactsMap = New Map<Id, Contact>([Select Id, OwnerId, AccountId, Account.OwnerId, Account_Type__c FROM Contact WHERE Id In :Ids]);

        for (Opportunity o:opportunities)
            {
                if(academidProgramOpportunityId != null && o.RecordTypeId == academidProgramOpportunityId){
                    continue;
                }
                if (o.Partner_Contact__c != null)
                {
                    system.debug('ActivePSMUpdate.UpdatePSM: Partner contact Id is ' + o.Partner_Contact__c);
                    if (o.Active_PSM__c == null)
                    {
                        system.debug('ActivePSMUpdate.UpdatePSM: PSM is null');
                        Contact pContact = contactsMap.get(o.Partner_Contact__c);

                        system.debug('ActivePSMUpdate.UpdatePSM: Partner contact is ' + pContact);
                        if (null != pContact)   // 2014-03-04 CR# 11205 added null test
                        {
                            o.Active_PSM__c = pContact.OwnerId;
                            o.Sell_Through_Partner__c = pContact.AccountId;
                        }
                    }
                } else if(String.isBlank(o.Partner_Contact__c)){
                    if(String.isNotBlank(o.Active_PSM__C)) o.Active_PSM__c = null;
                    if(String.isNotBlank(o.Sell_Through_Partner__c)) o.Sell_Through_Partner__c = null;

                }
                //CR# 7332
                System.Debug('CR 7332, Active_PSM__c = ' + o.Active_PSM__c);
                if (o.Active_PSM__c != null) {
                    uids.add(o.Active_PSM__c);
                }
                system.debug('[Viktor]' + o.Sell_Through_Partner__c + ', ' + o.Partner_Contact__c);
                if (o.Sell_Through_Partner__c != null && o.Partner_Contact__c !=null ) {
                    Contact pContact = contactsMap.get(o.Partner_Contact__c);
                    system.debug('[Viktor]' + pContact);
                    if ( pContact != null ) {
                        uids.add(pContact.Account.OwnerId);
                        accOwnerUserId.put(o.Sell_Through_Partner__c, pContact.Account.OwnerId);
                        system.debug('[Viktor]' + o.Sell_Through_Partner__c + ', ' + pContact.Account.OwnerId);
                    }
                }
            }
        //CR# 7332 Need to get Active_EIS and Active_MIS names via a map
        Map<Id, User> usersMap = New Map<Id, User>([Select Id, Name FROM User WHERE Id In :uIds]);

        //Changed to variable iterator to be able to access old list
        //for (Opportunity o:opportunities)
        for (Integer i = 0; i < opportunities.size();i++)
        {
                Opportunity o = opportunities.get(i);
                Opportunity oldOpp = null == oldOpportunities ? null : oldOpportunities.get(i);
                System.Debug('CR 7332, Active_EIS__c = ' + o.Active_EIS__c + ', Active_MIS__c = ' + o.Active_MIS__c);
                // CR 7332 - CLosed Won
                // CR 7775 - Only on changed to closed status

                if ((null == oldOpp || oldOpp.stageName != o.stageName) && ((o.StageName == 'Closed Won') || (o.StageName == 'OEM - Closed Won') || (o.StageName == 'OEM Pre-Pay Closed Won') || (o.StageName == 'Deal Split-closed Won') || (o.StageName == 'ER - Closed Won') || (o.StageName == 'QSG Closed Won'))){
                    if(usersMap != null)
                    {
                        if (o.Active_EIS__c != null) {
                            User pUserEIS = usersMap.get(o.Active_EIS__c);
                            if(pUserEIS != null)
                            {
                                o.Active_EIS_on_Closed_Won__c = pUserEIS.Id;
                                o.Active_EIS_on_Closed_Won_Name__c = pUserEIS.Name;
                            }
                        }
                        if (o.Active_MIS__c != null) {
                            User pUserMIS = usersMap.get(o.Active_MIS__c);
                            if(pUserMIS != null)
                            {
                                o.Active_MIS_at_Closed_Won__c = pUserMIS.Id;
                                o.Active_MIS_at_Closed_Won_Name__c = pUserMIS.Name;
                            }
                        }
                        system.debug('[Viktor]' + o.Sell_Through_Partner__c);
                        if (o.Sell_Through_Partner__c != null) {// QCW-3747 Viktor@4front changed Active_PSM__c -> Sell_Through_Partner__c
                            //System.Debug('CR 7332, Active_PSM__c = ' + o.Active_PSM__c);
                            User pUserPSM = usersMap.get(accOwnerUserId.get(o.Sell_Through_Partner__c)); // QCW-3747 Viktor@4front changed Active_PSM__c -> accOwnerUserId.get(o.Sell_Through_Partner__c
                            system.debug('[Viktor]' + pUserPSM);
                            if(pUserPSM != null)
                            {
                                // CR# 7776 New fields added to capture sell through
                                o.Sell_Through_PSM_QM_at_Closed_Won_ID__c = pUserPSM.Id;
                                o.Sell_Through_PSM_QM_at_Closed_Won__c = pUserPSM.Name;
                                //System.Debug('CR 7332, Active_PSM__c = ' + o.Active_PSM__c + ', pUserPSM.Name = ' + pUserPSM.Name);
                            }
                        }// QCW-3747 Viktor@4front changed Active_PSM__c -> Sell_Through_Partner__c ^

                    }
                    //System.Debug('CR 7332, Active_EIS.Name = ' + pUserEIS.Name + ', Active_MIS.Name = ' + pUserMIS.Name);
                }

            }
    }


    /**********************************************************

        UpdateReferralPSM

        -> opportunities - List of Opportunities

        Updates the Active_Referring_PSM__c with the Referring
        Contacts Owner.

    **********************************************************/
    public static void UpdateReferralPSM(Opportunity[] opportunities, opportunity[] oldOpportunities)
    {
        System.Debug('ActivePSMUpdate.UpdateReferralPSM: In the code');

        List<String> Ids = new List<String>();

        // EDIT 20.09.2012 : Exclude Academic Program Opportunity records from this trigger, Id from Custom Setting
        Id academidProgramOpportunityId;
        if(Academic_Program_Settings__c.getInstance('AcademicProgramSetting') != null) {
            academidProgramOpportunityId = Academic_Program_Settings__c.getInstance('AcademicProgramSetting').OpportunityRecordTypeId__c;
        }

        for (Opportunity o:opportunities){
            if(academidProgramOpportunityId != null && o.RecordTypeId == academidProgramOpportunityId){
                continue;
            }
            if (o.PRM_Referring_Contact__c !=null){
                ids.add(o.PRM_Referring_Contact__c);
            }
        }
        //CR 7332 Map no longer needed
        //Map<Id, Contact> contactsMap = New Map<Id, Contact>([Select Id, OwnerId, AccountId, Account_Type__c FROM Contact WHERE Id In :Ids]);

        //Changed to variable iterator to be able to access old list
        //for (Opportunity o:opportunities)
        for (Integer i = 0; i < opportunities.size();i++)
        {
            Opportunity o = opportunities.get(i);
            Opportunity oldOpp = null == oldOpportunities ? null : oldOpportunities.get(i);
            if(academidProgramOpportunityId != null && o.RecordTypeId == academidProgramOpportunityId){
                continue;
            }
            System.Debug('CR 7332, Inside opp loop');
            // CR 7332
            // CR 7775 - Only on changed to closed status

            if ((null == oldOpp || oldOpp.stageName != o.stageName) && ((o.StageName == 'Closed Won') || (o.StageName == 'OEM - Closed Won') || (o.StageName == 'OEM Pre-Pay Closed Won') || (o.StageName == 'Deal Split-closed Won') || (o.StageName == 'ER - Closed Won') || (o.StageName == 'QSG Closed Won'))){
                //System.Debug('CR 7332, Partner contact Id = ' + o.Referring_Contact__c);
                if (null != o.PRM_Referring_Contact__c){
                    //Contact pContact = contactsMap.get(o.PRM_Referring_Contact__c);
                    o.Referring_PSM_QM_at_Closed_Won__c = o.PRM_Id_Referring_PSM_QM__c;
                    o.Referring_PSM_QM_Name_at_Closed_Won__c = o.PRM_Referring_PSM_QM__c;
                    //System.Debug('CR 7332, pContact.Account.Owner.Id = ' + pContact.Account.Owner.Id + ', pContact.Account.Owner.Name = ' + pContact.Account.Owner.Name);
                }
            }
/* Active_Referring_PSM__c no longer used
            if (o.PRM_Referring_Contact__c != null)
            {
                System.Debug('ActivePSMUpdate.UpdateReferralPSM: Partner contact Id = ' + o.PRM_Referring_Contact__c);
                if (o.Active_Referring_PSM__c == null)
                {
                    Contact pContact = contactsMap.get(o.PRM_Referring_Contact__c);
                    System.Debug('ActivePSMUpdate.UpdateReferralPSM: Partner contact is ' + pContact);
                    //o.Active_Referring_PSM__c = pContact.OwnerId;
                }
            }
*/
        }

        System.Debug('ActivePSMUpdate.UpdateReferralPSM: Done!');
    }
}