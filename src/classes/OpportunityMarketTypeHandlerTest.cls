/******************************************************

        OpportunityMarketTypeHandlerTest
    
        Changelog:
            2012-02-10  CCE     Initial Development for CR# 3998 for testing trigger OpportunityMarketType
            2012-02-14  CCE     Added QlikTech_Company__c and Billing to Account creation due to manditory QlikTech Company field on Account
            2013-02-01  CCE     Modified test to work with changes made to OpportunityMarketType.trigger re CR# 7268 
            2016-06-09  roman@4front  Test methods for OpportunityMarketType.trigger removed from ULCTest.cls to this separate class
            						  TEST_OpportunityMarketTypeAdditional method created          
		    06.02.2017   RVA :   changing QT methods  
			2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring 
            2019-04-16 Shubham Gupta fixing test class issue due to BSL-258 assert not needed on line 187 
    ******************************************************/

@isTest
private class OpportunityMarketTypeHandlerTest {
                        
    public static TestMethod void TEST_OpportunityMarketType()
    {
        QTTestUtils.GlobalSetUp();

        QTCustomSettings__c settings = new QTCustomSettings__c();
		settings.Name = 'Default';
		settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        insert settings;
		/*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc'           
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        Id AccRecordTypeId_OEMEndUserAccount = '012D0000000KBYx'; //OEM End User   
        Account TestAccount = new Account(
            Name = 'ULC Test Ltd.',
            Navision_Status__c = 'Customer',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Lower SMB'                    
        );
        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Lower SMB'                            
        );
        List<Account> AccToInsert = new List<Account>();
        AccToInsert.Add(TestAccount);
        AccToInsert.Add(TestPartnerAccount);
        insert AccToInsert;
        
        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact;
            
        Opportunity Opp = new Opportunity();
        Opp.AccountId = TestAccount.Id;
        Opp.Short_Description__c = 'My Test Description';
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'USD';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Identified';
        Opp.Partner_Contact__c = PartnerContact.Id;
        insert Opp; 
        
        Opp = [select Id, Market_Type__c, StageName from Opportunity where Id = :Opp.Id];
        test.startTest();
        
        System.assertEquals('Lower SMB', Opp.Market_Type__c);
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        TestAccount.Named_Account__c = true;
        TestAccount.RecordTypeId = AccRecordTypeId_OEMEndUserAccount;
        update TestAccount;
        update Opp;
        
        Opp = [select Id, Market_Type__c from Opportunity where Id = :Opp.Id];
        System.assertEquals('Named Account', Opp.Market_Type__c);
        Semaphores.OpportunityTriggerBeforeInsert = false;
        Semaphores.OpportunityTriggerAfterInsert = false;
        //------------
        Opportunity Opp2 = new Opportunity();
        Opp2.RecordTypeId = '01220000000DXq5AAG';   //OEM - Order
        Opp2.Revenue_Type__c = 'Royalty';
        Opp2.AccountId = TestAccount.Id;
        Opp2.End_User_Account_del__c = TestAccount.Id;
        Opp2.Short_Description__c = 'My Test Description';
        Opp2.Name = '.';
        Opp2.Amount = 0;
        Opp2.CurrencyIsoCode = 'USD';
        Opp2.CloseDate = Date.today().addDays(30);
        Opp2.StageName = 'Goal Identified';
        Opp2.Partner_Contact__c = PartnerContact.Id;
        insert Opp2;    
        
        Opp2 = [select Id, Market_Type__c, StageName from Opportunity where Id = :Opp2.Id];
                
        System.assertEquals('OEM', Opp2.Market_Type__c);
        //------------
        
        test.stopTest();        
            
    }

    public static TestMethod void TEST_OpportunityMarketTypeAdditional()
    {
        QTTestUtils.GlobalSetUp();

        QTCustomSettings__c settings = new QTCustomSettings__c();
        settings.Name = 'Default';
        settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        insert settings;
		/*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc'           
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        Id AccRecordTypeId_OEMEndUserAccount = '012D0000000KBYx'; //OEM End User   
        Account TestAccount = new Account(
            Name = 'ULC Test Ltd.',
            Navision_Status__c = 'Customer',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Small Business'                    
        );
        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Lower SMB'                            
        );
        List<Account> AccToInsert = new List<Account>();
        AccToInsert.Add(TestAccount);
        AccToInsert.Add(TestPartnerAccount);
        insert AccToInsert;
        
        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact;
            
        Opportunity Opp = new Opportunity();
        Opp.AccountId = TestAccount.Id;
        Opp.Short_Description__c = 'My Test Description';
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'USD';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Identified';
        Opp.Partner_Contact__c = PartnerContact.Id;
        insert Opp; 
        
        Opp = [select Id, Market_Type__c, StageName from Opportunity where Id = :Opp.Id];
        test.startTest();
        
        System.assertEquals('Lower SMB', Opp.Market_Type__c);
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        TestAccount.Named_Account__c = false;
        TestAccount.RecordTypeId = AccRecordTypeId_OEMEndUserAccount;
        update TestAccount;
        update Opp;
        
        Opp = [select Id, Market_Type__c from Opportunity where Id = :Opp.Id];
        //System.assertEquals('Mid-Market', Opp.Market_Type__c);
        
        //------------
        Semaphores.OpportunityTriggerBeforeInsert = false;
        Semaphores.OpportunityTriggerAfterInsert = false;
        Opportunity Opp2 = new Opportunity();
        Opp2.RecordTypeId = '01220000000DXq5AAG';   //OEM - Order
        Opp2.Revenue_Type__c = 'Royalty';
        Opp2.AccountId = TestAccount.Id;
        Opp2.End_User_Account_del__c = TestAccount.Id;
        Opp2.Short_Description__c = 'My Test Description';
        Opp2.Name = '.';
        Opp2.Amount = 0;
        Opp2.CurrencyIsoCode = 'USD';
        Opp2.CloseDate = Date.today().addDays(30);
        Opp2.StageName = 'Goal Identified';
        Opp2.Partner_Contact__c = PartnerContact.Id;
        insert Opp2;    
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        Opp2 = [select Id, Market_Type__c, StageName from Opportunity where Id = :Opp2.Id];
        TestAccount.Named_Account__c =  false;
        TestAccount.Segment_New__c = null;
        update TestAccount;
        update Opp2;

        Opp2 = [Select Id, Market_Type__c From Opportunity Where Id =: Opp2.Id]; 
        System.assertEquals('OEM', Opp2.Market_Type__c);
        //------------
        
        test.stopTest();        
            
    }
	
}