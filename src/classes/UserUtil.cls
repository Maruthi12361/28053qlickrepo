/**********************************************************************************
*	Class UserUtil
*
*	Description:
*
* 	DML operations on certain sObjects can’t be mixed with other sObjects in the same transaction.
* 	So Update the User/Contact record in a @future annotated method.
*
*	ChangeLog:
*
* 	Initial development:
*	20130530	MTM	https://eu1.salesforce.com/a0CD000000YHtiu
***********************************************************************************/

public class UserUtil {
 /*   
	private static boolean recordUpdated = false;
    
    public static boolean IsRecordUpdated()
    {
        return recordUpdated;
    }
    
    @future
	public static void UpdateUser(Id contactId, string roleName)
	{
		User[] users = [select Id, UserRoleId from User u where u.ContactId = :contactId];
                
        UserRole[] roles = [select Name, Id from UserRole where Name = :roleName];
        
		if (users.Size() > 0 && roles.Size() > 0)
		{
			System.Debug('User = ' + users[0].Id);
			System.Debug('User role= ' + roles[0].Name);            
	        users[0].UserRoleId = roles[0].Id;
            
            recordUpdated = true;
	        update users;
		}
	}
    
    @future
	public static void UpdateContact(Id userRoleId, Id contactId)
	{
        UserRole [] userRoles = [select Name from UserRole where Id = :userRoleId];
        
        Contact [] contacts = [select Id, Account.Name from Contact where Id = :contactId];
        
        if(userRoles.Size() > 0 && contacts.Size() > 0)
        {
            string roleName = userRoles[0].Name.substringAfter(contacts[0].Account.Name).trim();
            contacts[0].Partner_Portal_Role__c = roleName;
            
            recordUpdated = true;
            update contacts;
        }
    }
*/
}