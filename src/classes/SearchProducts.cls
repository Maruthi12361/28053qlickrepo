/***************************************************************************************************************************************

	Changelog:
		2012-02-14  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
							to testSearchProducts() and testSearchProducts2()
****************************************************************************************************************************************/

public class SearchProducts 
{
	private List<Account_License__c> accountLicenses = null;
	private List<OpportunityLineItem> existingOppLineItems = null;
	public String OpportunityId =  null;
	public String OpportunityName='';
	private final Opportunity opportunity;
	private Id priceBook;
	private Id iAccountid;
	private List<Id> PBEId = new List<Id>();
	
	public Map<Id,String> maptag = new Map<Id,String>();
	
	private List<SelectOption> pricebookOptions;
			
	private String productFamily = '';
	
	private Boolean firstSearchPerformed = false;
	
	private String CurrencyISOCode = '';
	
	private List<OpportunityLineItemUIElement> oliUIs;	
		
	public List<PricebookEntryUIElement> selectedPricebookEntries = new List<PricebookEntryUIElement>();
	
	private Map<Id,Product2> productMap = new Map<Id,Product2>();
	private String selectedPriceBookId = '';
	private Boolean productsAddedToThisOpp = false;
	private String oliNav;
	private Double partnerMargin = null;
	
	//------------------------------------------
	// This is the controller constructor
	public SearchProducts() {
		
		// MHG 100713 added try-catch to stop unhandled exceptions
		try 
		{
			opportunity = [select id, name, Pricebook2Id,AccountId, CurrencyIsoCode, End_User_Account_del__c from Opportunity where id = :getOpportunityId()];
				
			OpportunityName = opportunity.Name;
	
			// MHG 100216 added End_User_Account_del__c check
			if (opportunity.End_User_Account_del__c == null)
			{
				iAccountid = opportunity.AccountId;
			}
			else
			{
				iAccountId = opportunity.End_User_Account_del__c;
			}
	
			CurrencyISOCode = opportunity.CurrencyIsoCode;
			
			this.priceBook = opportunity.Pricebook2Id;
			
			for(OpportunityLineItem oOpportunityLineItem : getExistingOppLineItems())
			{
				PBEId.add(oOpportunityLineItem.PricebookEntryId);
			}
		}
		catch (System.Exception Ex)
		{
			System.debug('SearchProduct - Cought exception - ' + Ex.getMessage());		
		}
		
		// Called in constructor - so only called once
		productMap = new Map<Id,Product2>([Select ProductCode, Navision_Type__c, Name__c, Name, Id, Family, Description From Product2 where Family <> 'Promo Code']);
	}
	
	public String getSelectedPriceBookId()
    {
        return selectedPriceBookId;           
    }
    
    public void setSelectedPriceBookId(String value)
    {
       selectedPriceBookId = value;
    }
	
	private Double getUserPartnerMargin()
	{
		
		if (null == partnerMargin)
		{
			User u = [Select id, Partner_Margin__c from User where Id = :UserInfo.getUserId()];
			
			if (u.Partner_Margin__c != null)
			{
				partnerMargin = u.Partner_Margin__c;
			}
			else
			{
				partnerMargin = 0;
			}
			
		}
		
		return partnerMargin;
	}

	public String getOpportunityName()
    {
        return this.OpportunityName;           
    }
    
    public void setOpportunityName(String value)
    {
        this.OpportunityName=value;
    }
    
    public String getOpportunityId()
    {
    	if (null == OpportunityId)
    	{
    		OpportunityId = ApexPages.currentPage().getParameters().get('id');
    	}	
    	
    	return OpportunityId;
    }
	
	public PageReference selectPricebook() 
	{
		if (null != selectedPriceBookId)
		{
			opportunity.Pricebook2Id = selectedPriceBookId;
			update opportunity;
		}
		
		this.priceBook = opportunity.Pricebook2Id;
					
		return Page.SearchProducts;
	}
	
	public List<SelectOption> getPricebookOptions() {
		
		List<Pricebook2> pricebooks = [Select Id, Name from Pricebook2 where IsActive = true];
		this.pricebookOptions = new List<SelectOption>();
				
		for(Pricebook2 pricebook:pricebooks){
		
			this.pricebookOptions.add(new SelectOption(pricebook.Id, pricebook.Name));
		}
		
		return pricebookOptions;
	}
	
	public string getPriceBook() 
	{
		return this.priceBook;
	}
	
	public void setPriceBook(string priceBook) 
	{
		this.priceBook = priceBook;
	}
	
   	public String getProductFamily() {
		return productFamily;
	}

	public void setProductFamily(String productFamily) {
		this.productFamily = productFamily;
	}
	
    public List<PricebookEntryUIElement> getSelectedPricebookEntries() 
    {   
    	this.selectedPricebookEntries = new List<PricebookEntryUIElement>();   	
    	 	
        List<PricebookEntry> pricebookEntries;
        List<id> Productid = new List<id>();
        
        if (productFamily != '')
        {
        	firstSearchPerformed = true;
        }
        
        // We also need to look to see if they already have a Product of type Product added to the Opp - 
        // or have any licenses - 
        Boolean canSearchForTags = false;
        
        if (accountHasLookupLicenses())
        {
        	// Can search for tags as the account has lookup licenses
        	canSearchForTags = true;
        }
        else
        {
        	System.debug('No Lookup Licenses - need to look at existing Line Items');
        	canSearchForTags = opportunityHasNonTagLineItems();
        }
        
        System.debug('First Search Performed - '+firstSearchPerformed);
        if (firstSearchPerformed)
        {
        	String searchString = productFamily;
	        searchString = productFamily.replace('*', '');
	        searchString = searchString.replace('%', '');
	        
	        searchString = '%' + searchString + '%';
	        System.debug('Searching for '+searchString);
        
        	pricebookEntries = [Select Id, IsActive, Name, Pricebook2Id, Product2Id, Product2.Description, Product2.Family, ProductCode, UnitPrice from PricebookEntry where IsActive = true and CurrencyISOCode = :CurrencyISOCode and Pricebook2Id = :priceBook and Name Like :searchString order by ProductCode];
        	System.debug('Search found '+pricebookEntries.size()+' records');
        }
        else
        {
        	// No Values in here!
        	// pricebookEntries = new List<PricebookEntry>();
        	pricebookEntries = [Select Id, IsActive, Name, Pricebook2Id, Product2Id, Product2.Description, Product2.Family, ProductCode, UnitPrice from PricebookEntry where IsActive = true and CurrencyISOCode = :CurrencyISOCode and Pricebook2Id = :priceBook order by ProductCode];
        	System.debug('Search found '+pricebookEntries.size()+' records on screen first open');
        }
         
        for(PricebookEntry pricebookEntry: pricebookEntries)
        {
        	Productid.add(pricebookEntry.Product2Id);
        }
        
        for(Product2 oProduct : [SELECT Id, Navision_Type__c from Product2 where Id in :Productid])
        {
        	maptag.put(oProduct.Id,oProduct.Navision_Type__c);
        }
        
        for(PricebookEntry pricebookEntry: pricebookEntries){
        
        	PricebookEntryUIElement pricebookEntryUIElement = new PricebookEntryUIElement();
        	
        	pricebookEntryUIElement.setPbeIsSelected(false); 
		 	pricebookEntryUIElement.setPbe(pricebookEntry);
        	
        	if (canSearchForTags)
        	{
        		if (productsAddedToThisOpp)
        		{
        			// Not Allowed to Search for Products any more...
        			if(maptag.get(pricebookEntry.Product2Id) != 'Product')
        			{
        				this.selectedPricebookEntries.add(pricebookEntryUIElement);
        			}
        		}
        		else
        		{
        			this.selectedPricebookEntries.add(pricebookEntryUIElement);
        		}
        	}
        	else
        	{
        		// Just add Products where the type is NOT Tag
        		if(maptag.get(pricebookEntry.Product2Id) != 'Tag')
			    {
			        this.selectedPricebookEntries.add(pricebookEntryUIElement);
			    }
        	}
        }
        
        return this.selectedPricebookEntries;
    }
    
    
    private Boolean opportunityHasNonTagLineItems()
    {
    	System.debug('No Lookup Licenses - need to look at existing Line Items');
        // We need to see if there are any Products added to the opportunity -- 
        
        List<OpportunityLineItem> lineItems = getExistingOppLineItems();
        	
        for (OpportunityLineItem li:lineItems)
        {
        	if (null != li.PricebookEntry.Product2Id)
        	{
        		Product2 p2 = productMap.get(li.PricebookEntry.Product2Id);
        		
        		if (null != p2)
        		{
        			if (p2.Navision_Type__c != 'Tag')
        			{
        				System.debug('Found Existing Line Item with Product '+p2.Name +' type '+p2.Navision_Type__c);
        				return true;
        			}
        			else
        			{
        				
        			}
        		}
        	}
        }
        
        return false;
    }
    
	public PageReference searchProducts() 
	{
		return null;
	}
	
	public string getOliNav()
	{ 
		return oliNav; 
	}
 	public void setOliNav(string p) 
 	{ 
 		oliNav = p;
 	}
	
	public PageReference selectProducts() 
	{							
		// need to figure out in here if a Product Was Added - 
		if(selectedPricebookEntries != null)
	    {
	    	for(PricebookEntryUIElement pbe:selectedPricebookEntries)
	    	{	        	
	        	if(pbe.getPbeIsSelected())
	        	{
			        if(productMap.get(pbe.getPbe().Product2Id).Navision_Type__c != 'Tag')
			        {
			        	productsAddedToThisOpp = true;
			        }
	        	}
	    	}
	    }
		
		return Page.AddProduct;
	}
	
	public String getLicenseColumnName()
	{
		String colName = '';
		
		Boolean render = false;
		// if we have Licenses to render -
		if (null != oliUIs)
		{
			for (OpportunityLineItemUIElement uie: oliUIs)
			{
				if (uie.getRender())
				{
					render = true;
				}	
			}
		}
		
		if (render && accountHasLookupLicenses())
		{
			// Do not render is the opp has non tag line items -- or just had products added
			if (opportunityHasNonTagLineItems() || productsAddedToThisOpp)
			{
				// we do not show - as the Tag Should be associated to the Product Type Line Item -
				// not the existing Account License
				
				// Here we set the render flag to false for all items---
				for (OpportunityLineItemUIElement uie: oliUIs)
				{	
					uie.setrender(false);
				} 
			}
			else
			{
				colName = 'License Number';
			}
		}
		
		return colName;
	}
	
	public PageReference cancelSelection() {
		PageReference opptyPage;
		if (opportunity != null)
		{
			opptyPage = new PageReference('/' + opportunity.id);
		}
		else
		{
			opptyPage = new PageReference('/');
		}
		opptyPage.setRedirect(true);
		return opptyPage;
	}
	
	public List<SelectOption> getTaskLst()
	{
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','Please Select'));
		for(Account_License__c License : getAccountLookupLicenses())
		{
			options.add(new SelectOption(License.Id,License.Name));
		}
		
		return options;
	}
	
	public List<Account_License__c> getAllAccountLicenses()
	{
		if (accountLicenses == null)
		{
			accountLicenses = [Select Name, Id, Lookup_License__c From Account_License__c where Account__c = :iAccountid LIMIT 9000];
		} 
		 
		return accountLicenses;
	}
	
	public List<Account_License__c> getAccountLookupLicenses()
	{
		 List<Account_License__c> lookupLicenses = new List<Account_License__c>();
		 
		 for (Account_License__c lic:getAllAccountLicenses())
		 {
		 	if (lic.Lookup_License__c)
		 	{
		 		lookupLicenses.add(lic);
		 	}
		 }
		 
		 return  lookupLicenses;
	}
	
	public Boolean accountHasLookupLicenses()
	{
		return getAccountLookupLicenses().size() != 0;
	}
	
	public List<OpportunityLineItem> getExistingOppLineItems()
	{
		if (existingOppLineItems == null)
		{
			existingOppLineItems = [Select UnitPrice, Quantity, PricebookEntry.ProductCode, PricebookEntryId, PricebookEntry.Product2Id,CurrencyIsoCode,
	    	Partner_Margin__c, OpportunityId, Name__c, ListPrice, Discounted_Price__c, Discount__c, Description, Account_License__r.Name, Account_License__c From OpportunityLineItem where OpportunityId = :getOpportunityId()];
		}
		
		return existingOppLineItems;
	}
		   
    public List<OpportunityLineItemUIElement> getOliUIs() 
    {
     	if (this.oliUIs == null)
     	{
	    	this.oliUIs = new List<OpportunityLineItemUIElement>();	    	
	    	
	    	List<OpportunityLineItem> existingLineItems = getExistingOppLineItems();
	    	
	    	for (OpportunityLineItem o: existingLineItems)
	    	{
	    		OpportunityLineItemUIElement oliUI = new OpportunityLineItemUIElement();
	
			    //oliUI.OpportunityId = o.OpportunityId;
			    //oli.UnitPrice = o.UnitPrice;
			    //oli.Quantity = o.Quantity;
			    //oli.PricebookEntryId = o.PricebookEntryId;
			        	
			    oliUI.setOliProductName(o.name__c);
			    oliUI.setOli(o);
			    oliUI.setOliNav(o.Account_License__c);
			    oliUI.setOliProductCode(o.PricebookEntry.ProductCode);
			    oliUI.setRender(canRenderLineItemLicenseLookup(o.PricebookEntry.Product2Id));
			    
			    this.oliUIs.add(oliUI);
	    	}
	    	
	    	if(selectedPricebookEntries != null)
	    	{
	    		for(PricebookEntryUIElement pbe:selectedPricebookEntries)
	    		{	        	
	        		if(pbe.getPbeIsSelected())
	        		{
			        	OpportunityLineItemUIElement oliUI = new OpportunityLineItemUIElement();
			        	
			        	OpportunityLineItem oli = new OpportunityLineItem();
			        	oli.Partner_Margin__c = getUserPartnerMargin();
			        	// oli.CurrencyIsoCode = opportunity.CurrencyIsoCode;
			        	oli.OpportunityId = opportunity.id;
			        	oli.UnitPrice = pbe.getPbe().UnitPrice;
			        	// oli.ListPrice = pbe.getPbe().UnitPrice;
			        	oliUI.setPriceBookListPrice(pbe.getPbe().UnitPrice);
			        	oliUI.setCurrCode(opportunity.CurrencyIsoCode);
			        	oli.Quantity = 1;
			        	oli.PricebookEntryId = pbe.getPbe().Id;	
			        	
			        	oliUI.setOliProductName(pbe.getPbe().name);
			        	oliUI.setOliProductCode(pbe.getPbe().ProductCode);
			        	oliUI.setOli(oli);
			        	oliUI.setRender(canRenderLineItemLicenseLookup(pbe.getPbe().Product2Id));
			        	
			        	this.oliUIs.add(oliUI);
	        		}
	        	}
	    	}	        
    	}
    	
        return this.oliUIs;
    }
    
    private Boolean canRenderLineItemLicenseLookup(String Product2Id)
    {
    
    	String prodType = productMap.get(Product2Id).Navision_Type__c;
    	
    	if (prodType == 'Tag')
    	{
    		// We could render the lookup unless 
    		if (opportunityHasNonTagLineItems())
    		{
    			return false;
    		}
    		
    		if (productsAddedToThisOpp)
    		{
    			return false;
    		}
    		
    		if (accountHasLookupLicenses())
    		{
    			return true;
    		}
    	}	
    	
    	return false;
    }
    
    public void setOliUIs(List<OpportunityLineItemUIElement> oliUIs) 
    {				
		this.oliUIs = oliUIs;
	}
	
	public PageReference saveProducts() {
										
		try{
			
			List<OpportunityLineItem> olis2Create = new List<OpportunityLineItem>();
			List<OpportunityLineItem> olis2Update = new List<OpportunityLineItem>();
			
			for(OpportunityLineItemUIElement oliUI:this.oliUIs){
				
				OpportunityLineItem oli = oliUI.getOli();
				oli.Account_License__c = oliUI.getOliNav();
				
				// 2009-10-29 MHG : Changed from 0 -> -10
				// 2009-11-20 MHG : Changed from -10 -> -25
				// 2010-06-03 MHG : Removed
				/*
				if(oli.Quantity <= -25){
					
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Quantity must be more than zero');
   					ApexPages.addMessage(msg);
   					System.debug(LoggingLevel.ERROR, 'Quantity must be more than zero.');
					return null;
				}
				*/
				if(oli.UnitPrice == null){
					
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unit Price is required');
   					ApexPages.addMessage(msg);
   					System.debug(LoggingLevel.ERROR, 'Unit Price is required.');
					return null;
				}
				
				// TODO -  This is where we need to see if the License Lookup was rendered - 
				// and check if the user selected a value
				if (oliUI.getRender())
				{
					if (null == oli.Account_License__c)
					{
						ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select an Account License Value');
   						ApexPages.addMessage(msg);
   						System.debug(LoggingLevel.ERROR, 'Please select an Account License Value');
						return null;
					}
				}
				else
				{
					oli.Account_License__c = null;
				}
				
				if (oli.Id == null)
				{
					olis2Create.add(oli);
				}
				else
				{
					olis2Update.add(oli);
				}
			}
			
			// TODO - reset any Lookups if Products Added to this opp - done above - 
			
			if (olis2Create.size() > 0)
			{
				insert olis2Create; 
			}
			
			if (olis2Update.size() > 0)
			{
				update olis2Update; 
			}
			
			// Set these back to null - will be queried again if needed
			existingOppLineItems = null;
			
			// 2010-08-10 MHG : Added null pointer check
			PageReference opptyPage;	
			if (opportunity != null)
			{
				opptyPage = new PageReference('/' + opportunity.id);
			}
			else
			{
				opptyPage = new PageReference('/');
			}
			opptyPage.setRedirect(true);
			return opptyPage;
		}
		catch(DmlException ex){
			ApexPages.addMessages(ex);
		}
		
		return null;		
	}
	
	public PageReference saveAndMoreProducts() {
				
		try{
			if (null == saveProducts())
			{
				// There were errors on the page - return null
				return null;
			}			
				
			ResetVariables();
			productFamily = '';
			return Page.SearchProducts;
		}
		catch(DmlException ex){
			ApexPages.addMessages(ex);
		}
		
		return null;		
	}
	
	public PageReference cancelSave() 
	{		
		// 2010-08-30 MHG : Added null pointer check
		PageReference opptyPage;	
		if (opportunity != null)
		{
			opptyPage = new PageReference('/' + opportunity.id);
		}
		else
		{
			opptyPage = new PageReference('/');
		}
		opptyPage.setRedirect(true);
		return opptyPage;
	}
	  
	public void ResetVariables() {
		selectedPricebookEntries = new List<PricebookEntryUIElement>();
		oliUIs = null;
	}
	
	public Boolean getRenderNoPricebookMessage()
	{	
		if (opportunity == null)
		{
			return true;
		}
		return opportunity.Pricebook2Id == null;
	}

/***************************************************************************************************************************************

	Changelog:
		2012-02-14  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
							
****************************************************************************************************************************************/
	

}