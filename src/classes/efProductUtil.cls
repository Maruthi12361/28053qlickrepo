//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  Registration product related methods.
//			The pattern here is; training, promo codes and registration
//			the 3 primary concerns for Registration have a Util class.
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efProductUtil {
	private efRegistrationManager regMan;
	private efProductWrapper[] registrationProducts;
	private efProductWrapper[] addOnProducts;	
	private Map<ID,efProductWrapper> registrationMap; 
	private Map<ID,efProductWrapper> addOnMap;

	public efProductUtil(efRegistrationManager regMan){
		this.regMan = regMan;
	}

    public efProductWrapper[] getRegistrationProducts(){
        if (registrationProducts == null){
        	populateRegistrationProducts();
        }        
        return registrationProducts;
    }
    
    public efProductWrapper[] getAddonProducts(){
        if (addOnProducts == null){
        	populateAddonProducts();
        }        
        return addOnProducts;
    }

    private void populateRegistrationProducts(){
    	registrationProducts = new List<efProductWrapper>();
    	registrationMap = new Map<ID,efProductWrapper>();
    	
    	populateProducts(registrationProducts,
    	                 registrationMap,
    	                 efConstants.PROD_FAMILY_REGISTRATION);           
    }
    
    private void populateAddOnProducts(){
    	addOnProducts = new List<efProductWrapper>();
    	addOnMap = new Map<ID,efProductWrapper>();
    	
    	populateProducts(addOnProducts,
    	                 addOnMap,
    	                 efConstants.PROD_FAMILY_ADDON);           
    }
    
    private void populateProducts(List<efProductWrapper> productList,
                                  Map<ID,efProductWrapper> productMap,
                                  String productFamily){
    	try{
    		Integer index = 0;
    		
	        for(Product2 p:[Select Id,
	                               Name,
	                               Description,
                                   Role__c,
                                   ProductCode,
	                               Campaign__c,
	                               StartDate__c,
	                               EndDate__c,
	                               Family,
	                               Balance__c,
	                               Used__c,
	                               Capacity__c,
	                               Promo_code_type__c,
	                               Discounts_Allowed__c,
	                               Discount_Type__c,
	                               Contest_Promo_Code__c,
	                               (
	                                    Select Id,
	                                           UnitPrice,
	                                           Product2.Campaign__c
	                                    From PricebookEntries
	                                    Where Pricebook2Id =:regMan.getEventWrapper().getPriceBookId()
	                               )
	                        From Product2
	                        Where Family = :productFamily And
	                        Event__c=:regMan.getEventWrapper().getEvent().Id And
	                        isActive = true And
	                        Valid_Attendee_Types__c includes (:regMan.getFinalUserType()) And
                            StartDate__c <= Today And
                            EndDate__c >= Today
	                        Limit 1000]){
	        	String id = p.ID;
	        	
	            efProductWrapper pi = new efProductWrapper(p);
	            pi.setCounter(index++);
	            productList.add(pi);
	            productMap.put(p.ID,pi);
	        }
    	} catch (Exception e){
    		System.debug('FINDME: Error populating product family: ' + productFamily + ', ' + e.getMessage());
    	}
    }

    private List<efProductWrapper> filterSelected(List<efProductWrapper> products){
        List<efProductWrapper> selectedProducts = new List<efProductWrapper>();
         
        for (efProductWrapper pi:products){
            if (pi.getIsSelected()){
                selectedProducts.add(pi);
            }
        }        
        return selectedProducts;
    }
    
    public List<efProductWrapper> getSelectedAddOns(){
        return filterSelected(getAddOnProducts());
    }
    
    public efProductWrapper getAddOnProductById(String idVal){
        if (addOnProducts == null){ populateAddOnProducts(); }
        
        for (efProductWrapper p : addOnProducts){
			if (p.getProduct().Id==idVal)
				return p;
        }        
        return null; 		   	
    }
}