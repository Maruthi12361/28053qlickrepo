/****************************************************** 

Class: Test_Account_SetSupportOffice 

This class tests the Account_SetSupportOffice trigger 

Changelog: 
2012-07-18 CCE Initial creation 
2014-08-22 CCE Updated test for CR# 14397 https://eu1.salesforce.com/a0CD000000jXhIa
2016-02-04 OSO Updated test for CR# 74965 https://eu1.salesforce.com/a0CD000000xS7ly
2016-02-04 UIN Updated test for CR# 84892 https://eu1.salesforce.com/a0CD000000yQWEh
2016-02-04 UIN Updated test for CR# 100281  https://eu1.salesforce.com/a0CD000000zfsHW
2017-12-13 Pramod Kumar V test class coverage
17.02.2020  ext_bad IT-2497   Added Legacy Enterprise Support Entitlement Process
******************************************************/
@isTest  //(SeeAllData=true)
private class Account_SetSupportOfficeTest {

    //Test Data Setup Method
    @testSetup static void  createTestData() {

        //Populate the Custom setting that holds the list of users email addresses
        string toAddresses = 'tester1@qlik.com.sandbox,tester2@qlik.com.sandbox';
        QTCustomSettings__c mycs = QTCustomSettings__c.getValues('Default');
        if(mycs == null) {
            mycs = new QTCustomSettings__c(Name = 'Default');
            mycs.null_Support_Office_Email__c = toAddresses;
            insert mycs;
        }

        Entitlement_Settings__c settings4 = new Entitlement_Settings__c();
        settings4.X24x7_Business_Hours_Name__c = '24x7';
        settings4.Premium_Entitlement_Process_Name__c = 'Legacy Enterprise Support SLA';
        settings4.Premium_Support_Level__c = 'Enterprise';
        settings4.Standard_Entitlement_Process_Name__c = 'Basic Support SLA';
        settings4.Standard_Support_Level__c = 'Basic';
        settings4.Signature_Entitlement_Process_Name__c = 'Signature Support SLA';
        settings4.Enterprise_Entitlement_Process_Name__c = 'Enterprise Support SLA';
        settings4.Hold_Account_Name__c = 'UnMatched License Hold Account';
        settings4.Email_Message_Phrase_Communication__c = 'CUSTOMER COMMUNICATION';
        settings4.Email_Message_Phrase_First_Response__c = 'FIRST RESPONSE';
        insert settings4;

        //Create QlikTech Companies
        List<Id> QTCompany = new Id[5];
        QlikTech_Company__c qtComp_AUS = QTTestUtils.createMockQTCompany('QlikTech Australia Pty Ltd', 'AUS', 'Australia','APAC');
        QTCompany[0] = qtComp_AUS.Id;
        //QlikTech_Company__c qtComp_HUN = QTTestUtils.createMockQTCompany('QlikTech Benelux', 'HUN', 'Hungary', 'SEEMEA');
        //QTCompany[1] = qtComp_HUN.Id;
        //QlikTech_Company__c qtComp_BRA = QTTestUtils.createMockQTCompany('QlikTech Brazil', 'BRA', 'Brazil','NEMEA');
        //QTCompany[2] = qtComp_BRA.Id;
        QlikTech_Company__c qtComp_CAN = QTTestUtils.createMockQTCompany('QlikTech Corporation', 'CAN', 'Canada', 'AMERICAS');
        QTCompany[1] = qtComp_CAN.Id;
        QlikTech_Company__c qtComp_JPN = QTTestUtils.createMockQTCompany('QlikTech Denmark ApS11', 'JPN', 'JAPAN', 'APAC');
        QTCompany[2] = qtComp_JPN.Id;
        QlikTech_Company__c qtComp_ISL = QTTestUtils.createMockQTCompany('QlikTech Denmark ApS', 'ISL', 'Iceland', 'Nordics');
        QTCompany[4] = qtComp_ISL.Id;
        QlikTech_Company__c qtComp_FRA = QTTestUtils.createMockQTCompany('QlikTech Denmark ApS222', 'FRA', 'FRANCE', 'EMEA');
        QTCompany[3] = qtComp_FRA.Id;
        //Pramod
        QlikTech_Company__c qtComp_IND = QTTestUtils.createMockQTCompany('QlikTech India Pvt Ltd', 'IND', 'INDIA','APAC');
        QTCompany[2] = qtComp_IND.Id;

        //Create some Support Offices
        List<Support_Office__c> spList = new List<Support_Office__c>();
        spList.add(new Support_Office__c( Name = 'Sydney'));
        spList.add(new Support_Office__c( Name = 'Lund'));
        spList.add(new Support_Office__c( Name = 'Raleigh'));
        spList.add(new Support_Office__c( Name = 'Tokyo'));
        spList.add(new Support_Office__c( Name = 'Bangalore'));
        insert spList;

        //Create and insert the Accounts
        List<Account> accList = new List<Account>();
        string accName = '';
        FOR (Integer I = 0; I<QTCompany.size(); I++) {
            accName = 'Test Account for SetSupportOffice' + I;
            accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Customer', Billing_Country_Code__c = QTCompany[I], Support_Office__c = null) );
        }
        insert accList;
    }

    @isTest
    static void checkSupportOfficeMapping(){//Read all the Accounts except the last one and check to see if the Support Office field has been populated
        List<Account> testActs = [select id,name,Billing_Country_Code__r.name,Billing_Country_Code_Country__c,Support_Office__r.name from account where name like 'Test Account for SetSupportOffice%'];
        List<Support_Office__c> spList =[select id from Support_Office__c];
        List<QlikTech_Company__c> qTList =[select id from QlikTech_Company__c];
        Test.startTest();
        system.assertEquals(testActs.size(), 5);
        system.assertEquals(spList.size(), 5);
        system.assertEquals(qTList.size(), 6);
        AccountSetSupportOfficeHandler accHandler = new AccountSetSupportOfficeHandler();
        for(Account a: testActs){
            //system.assertEquals(a.Billing_Country_Code__r.name, 'AUS');
            if(a.Billing_Country_Code__r.name == 'AUS')
                system.assertEquals(a.Support_Office__r.name, 'Sydney');
            else if(a.Billing_Country_Code__r.name == 'HUN')
                system.assertEquals(a.Support_Office__r.name, 'Lund');
            else if(a.Billing_Country_Code__r.name== 'JPN')
                system.assertEquals(a.Support_Office__r.name, 'Tokyo');
            else if(a.Billing_Country_Code__r.name == 'BRA')
                system.assertEquals(a.Support_Office__r.name, 'Lund');
            else if(a.Billing_Country_Code__r.name == 'CAN')
                system.assertEquals(a.Support_Office__r.name, 'Raleigh');
            else if(a.Billing_Country_Code__r.name == 'ISL')
                system.assertEquals(a.Support_Office__c, null);
            else if(a.Billing_Country_Code__r.name == 'FRA')
                system.assertEquals(a.Support_Office__r.name, 'Lund');
            else if(a.Billing_Country_Code__r.name == 'INDIA')
                system.assertEquals(a.Support_Office__r.name, 'Bangalore');
        }
        testActs[2].Billing_Country_Code__c = qTList[2].Id;
        testActs[2].Support_Office__c = null;
        update testActs[2];
        Test.stopTest();
    }
}