//*********************************************************/
// Author: Mark Cane&
// Creation date: 18/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efTrainingUtil{
	private efRegistrationManager regMan;
	private List<efProductWrapper> classesProducts;
	private List<efClassWrapper> classList;
		
	public Boolean selectedTrainingInvalidated {get; set;}    
	public boolean isTrainingSelected = false;


	// Initialisation.
	
	public efTrainingUtil(efRegistrationManager regMan){
		this.regMan = regMan;
		selectedTrainingInvalidated = false;
	}
	// End intialisation.	


	// Training methods.
	
	public efProductWrapper[] getClasses(){
		efProductWrapper[] classes;
		try{
            classes = getClassesProducts();
				
			if(classes!=null && classes.size()>0){
				Integer counter=0;
				for(Integer i=0;i<classes.size();i++){											
					classes[i].setCounter(counter);
					counter++;
				}
			}
		}
		catch(Exception e){}
		return classes;
	}
	
	private efProductWrapper[] getClassesProducts(){
		if(classesProducts!=null && classesProducts.size()>0)
			return classesProducts;
		else{	
			Product2[] products = [Select Id,
			                              Family,
			                              Name,
			                              Role__c,
			                              Class_Type__c,
			                              Description,
			                              StartDate__c,
			                              EndDate__c,
			                              Balance__c,
			                              Used__c,
			                              Capacity__c,
			                              Room__r.Name,
			                              (
			                                   Select Id,
			                                          UnitPrice,
			                                          Product2.Campaign__c
			                                   From PricebookEntries
			                                   Where Pricebook2Id=:regMan.getEventWrapper().getPriceBookId()
			                              )
			                       From Product2
			                       Where Family=:efConstants.PROD_FAMILY_CLASS
			                       And Event__c=:regMan.getEventWrapper().getEvent().Id
			                       And isActive=true
			                       Order By startdate__c];			    
			if(products!=null && products.size()>0){
				Integer counter=0;
				classesProducts = new List<efProductWrapper>(); 
				
				for(Integer i=0;i<products.size();i++){
					efProductWrapper pi = new efProductWrapper(products[i]);
					pi.setCounter(counter); 
					counter++;
					classesProducts.add(pi);
				}
			}
    		return classesProducts;
		}	
	}
	
	public void setClassesByGroup(List<efClassWrapper> c){
		classList = c;
	}
	
	public List<efClassWrapper> getClassesByGroup(){
    	if(this.classList!=null) return classList;
    	
    	List<efProductWrapper> pRecords = getClasses();
    	         
        Map<String, efClassWrapper> classMap = new Map<String, efClassWrapper>();
        if(pRecords!=null && pRecords.size()>0){
            for(efProductWrapper p: pRecords){
                String groupName = p.getFormatedDate();
                                      
                if(!efUtility.isNull(groupName)){
                    if(classMap.containsKey(groupName)){
                        efClassWrapper c = classMap.get(groupName);
                        c.addClass(p);
                        classMap.put(groupName,c);
                    } else{
                        efClassWrapper c = new efClassWrapper();
                        c.setGroupName(groupName);
                        c.setGroupDate(p.getProduct().startdate__c);
                        c.addClass(p);
                        classMap.put(groupName,c);
                    }
                }
            }

            if(!classMap.isEmpty()){
                classList = classMap.values();
            }
            return sortClassesByGroup(classList);
        }        
        return classList;
    }
    
    private List<efClassWrapper> sortClassesByGroup(List<efClassWrapper> cgList){
        for (Integer i=0;i<cgList.size();i++){
            for(Integer j=0;j<cgList.size();j++){
                if(cgList.get(i).getGroupDate() < cgList.get(j).getGroupDate() ){
                    efClassWrapper cTemp = cgList.get(i);
                    cgList.set(i,cgList.get(j));
                    cgList.set(j,cTemp);
                }
            }
        }        
        return cgList;      
    } 
	
	// End Training methods.
	
	
	// Obsolete methods.

    public Boolean validateTrainingInventory(){
    	Boolean error = false;
    	Map<Id,efProductWrapper> productMap = new Map<Id,efProductWrapper>();
        List<efProductWrapper> selectedTraining = getTrainingOfInterest();
    	
    	for (efProductWrapper pi:selectedTraining){
    		productMap.put(pi.getID(), pi);
    	}
    	
        for (Product2 p:[select Id,
                                Used__c,
                                Capacity__c,
                                ProductCode,
                                Balance__c
                         from Product2
                         where Id IN :productMap.keySet()]){
            if (p.Balance__c <= 0 && !productMap.get(p.Id).getIsClassFull()){
            	// The training capacity has been filled before the
            	// attendee confirmed his purchase
                error = true;
                productMap.get(p.Id).setIsSelected(false);
            }

            productMap.get(p.Id).getProduct().Used__c = p.Used__c;
        }
        
        selectedTrainingInvalidated = error;
        return !error;
    }
    
    public boolean getIsTrainingSelected(Boolean includeFullTraining){    	
        isTrainingSelected = false;
        
        if(this.classList!=null && this.classList.size()>0){   
            for(efClassWrapper cg: classList){
                if(cg.getGroupClasses() != null && cg.getGroupClasses().size() > 0){
                    List<efProductWrapper> p2i = cg.getGroupClasses();
                    
                    if(p2i!=null && p2i.size()>0){
                        for(efProductWrapper p:p2i){
                            if (p.getIsSelected() &&
                                (!p.getIsClassFull() ||
                                 includeFullTraining)){
                                isTrainingSelected = true;
                                break;
                            }                                 
                        }
                    }
                }                
                if (isTrainingSelected)
                    break;
            }
        }
        if (!isTrainingSelected){}
        return  this.isTrainingSelected;
    }

    public boolean getIsTrainingSelected(){
        return getIsTrainingSelected(false);
	}
	
    public boolean getShowClasses(){
		// TODO & : link to navigator.    	
		return true;
	}
	
    public List<efProductWrapper> getTrainingOfInterest(){
		List<efProductWrapper> selectedTraining = new List<efProductWrapper>();

		if(this.classList!=null && this.classList.size()>0){	
			for(efClassWrapper cg: classList){
				if(cg.getGroupClasses()!=null && cg.getGroupClasses().size()>0){
					List<efProductWrapper> p2i = cg.getGroupClasses();

					if(p2i!=null && p2i.size()>0){
						for(efProductWrapper p:p2i){
							if (p.getIsSelected()){
								selectedTraining.add(p);
							}
						}
					}
				}
			}
		}                
		return selectedTraining;				
	}	

    public boolean getShowClassesForAttendee(){	
		// TODO & : come back to this
		return true;
    }
}