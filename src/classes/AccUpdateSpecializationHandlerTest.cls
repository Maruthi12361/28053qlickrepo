/**     * File Name:AccUpdateSpecializationHandler_Test
        * Description : This handler class is used to check the account specialization field on insert,update or delete of approved specialization records
        * @author : Pramod Kumar
        * Modification Log ===============================================================

*/
@isTest(seealldata=false) 
public class AccUpdateSpecializationHandlerTest{
    static testMethod void AccUpdateSpecializationHandlerTest(){
        Account acc=new Account(name ='test');
        insert acc;
        Specialization__c spl=new Specialization__c(Account__c=acc.id,Designation_Status__c='Approved',Specialization_Category__c='Technology',Specialization__c='Mobility');
        Test.startTest();
            insert spl;
            System.assertEquals('Approved', spl.Designation_Status__c);
            System.assertNotEquals('In Process', spl.Designation_Status__c);
            System.assertNotEquals(Null, spl.Designation_Status__c);
            spl.Specialization_Category__c='Technology';
            spl.Specialization__c='Security';
            update spl;
            System.assertEquals('Technology', spl.Specialization_Category__c);
            System.assertEquals('Security', spl.Specialization__c);
            delete spl;
            System.assertEquals('Technology', spl.Specialization_Category__c);
            System.assertEquals('Security', spl.Specialization__c);
       Test.stopTest();
    }
}