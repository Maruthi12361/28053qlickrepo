/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_ProfilePageControllerTest {
    
    @isTest static void test_method_one() {
        

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        insert testContact;

        String profileId = [select id from Profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1].Id;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        Attachment attachment = new Attachment();
        attachment.Name = 'Attach';
        attachment.body = Blob.valueof('Some random String');
        
        system.runas(communityUser){
            QS_ProfilePageController controller = new QS_ProfilePageController();
            controller.attachment = attachment;
            User objUser = controller.objUser;
            controller.objUser = objUser;
            boolean IsPartner = controller.IsPartner;
            controller.IsPartner = IsPartner;
            controller.EditEnvironment();
            controller.UpdateProfile();
            controller.getUserPersona();
            controller.getPersonasList();
            //controller.SaveBut();
            controller.getUserprofilePic();
            controller.UploadUserProfilePic();
        }
    }   
}