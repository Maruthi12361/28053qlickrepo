/**********************************************
*	QTException, Qliktech exception to display custom errors.
*	Extends Extension
*   
*	Change Log:
*	20130703 	RDZ		CR# 8165 https://eu1.salesforce.com/a0CD000000YI6DP
*						Used for QTTestUtils.cls
*
*
***************************************************/
public class QTException extends Exception {

}