/**     * File Name:AccChannelProgramHandler
        * Description : This handler class is used to create channel program members for accounts if the erp statu is changed to partner.
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 April 15th 2020 RamakrishnaKini     Added new trigger logic.
*/
public class AccChannelProgramHandler{
    
    static List<Account> lAccs = new List<Account>();
    
    public AccChannelProgramHandler() {
    
    }
    
    /*  @Description :This common method is used to update billing country code fields on account on before update trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap 
        @parameter oInputMap : Trigger.old Account map
    */
    public static void onAfterUpdate(List<Account> inputList, Map<id, Account> inputMap, Map<id, Account> oInputMap) {
        Map<Id, List<String>> qTechCompIdAccId  = new Map<Id, List<String>>();
        Set<Id> mChannelPrgPartId = new Set<ID>();
        
        for(ChannelProgramMember cp: [SELECT Id, PartnerId FROM ChannelProgramMember where partnerid in:inputMap.keyset()]){
            mChannelPrgPartId.add(cp.PartnerId);
        }
        
        for(Account acc: inputList){
            if(acc.isPartner && (mChannelPrgPartId.isEmpty() || !mChannelPrgPartId.contains(acc.Id)) && String.isNotBlank(acc.Billing_Country_Code__c) && String.isNotBlank(acc.Navision_Status__c) && 'Partner'.equalsIgnoreCase(acc.Navision_Status__c)
                && inputMap.get(acc.Id).Navision_Status__c != oInputMap.get(acc.Id).navision_Status__c){
                if(qTechCompIdAccId.containsKey(acc.Billing_Country_Code__c))
                    qTechCompIdAccId.get(acc.Billing_Country_Code__c).add(acc.Id);
                else
                    qTechCompIdAccId.put(acc.Billing_Country_Code__c, new List<String>{acc.Id});   
            }   
        }
        
        if(!qTechCompIdAccId.isEmpty())
            createChannelProgramMembers(qTechCompIdAccId);
    }
    
    /**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/
    
    /*  @Description :This method is used to fetch qliktech company details for accounts which have billing country populated
            and update the billing country code lookup
        @parameter inputList: Accounts whose billing country is populated
    */
    private static void createChannelProgramMembers(Map<Id, List<String>> mQTechCompIdAccId){
        
        Map<String, String> mChannelProgNameId = new Map<String, String>();
        Map<Id, String> mQCompany  = new Map<Id,String>();
        List<ChannelProgramMember> lCPMember = new List<ChannelProgramMember>();
        
        for(QlikTech_Company__c qc: [select id, QlikTech_Company_Name__c, Market_Name__c from QlikTech_Company__c where id in :mQTechCompIdAccId.keySet()]){
            if(!mQCompany.containsKey(qc.Id)){
                if(String.isNotBlank(qc.Market_Name__c))
                    mQCompany.put(qc.Id, qc.QlikTech_Company_Name__c + ' - ' + qc.Market_Name__c);
                else
                    mQCompany.put(qc.Id, qc.QlikTech_Company_Name__c);
            }
        }
        if(!mQCompany.isEmpty()){
            for(ChannelProgram cp: [select id,name from ChannelProgram where name in :mQCompany.values()]){
                if(!mChannelProgNameId.containsKey(cp.name))
                   mChannelProgNameId.put(cp.name, cp.Id); 
            }
        }
        
        if(!mChannelProgNameId.isEmpty()){
            for(Id compId: mQTechCompIdAccId.keyset()){
                for(Id accId: mQTechCompIdAccId.get(compId)){
                    ChannelProgramMember cpmem = new ChannelProgramMember();
                    system.debug('jhjhjhjh' + accId);
                    cpmem.partnerId = accId;
                    cpmem.ProgramId = mChannelProgNameId.get(mQCompany.get(compId));
                    lCPMember.add(cpmem);
                }
            }
        }
        
        if(!lCPMember.isEmpty())
            insert lCPMember;
    }
}