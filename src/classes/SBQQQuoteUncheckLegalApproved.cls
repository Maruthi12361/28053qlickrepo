/**
 *	14-06-2017	Created by Oleksandr@4Front for QCW-2103.
 *
 *  06/11/2017 QCW-4218 Viktor@4Front fix handler.
 */

public class SBQQQuoteUncheckLegalApproved {
    public void uncheckLegalApproved(Map<id, SBQQ__Quote__c> SBQQQuoteMapOld,
            List<SBQQ__Quote__c> SBQQQuoteListNew) {

        for (SBQQ__Quote__c q : SBQQQuoteListNew) {

            SBQQ__Quote__c oldQuote = SBQQQuoteMapOld.get(q.Id);
            if ((q.Select_Billing_Address__c != oldQuote.Select_Billing_Address__c)
                    || (q.Shipping_Address__c != oldQuote.Shipping_Address__c)) {
                q.Legal_Approval_Triggered__c = false;
            }
        }
    }
}