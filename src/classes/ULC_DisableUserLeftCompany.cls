/********************************************************

ULC_DisableUserLeftCompany
Apex implementation of the web service class within ULCv3.

ChangeLog:
2015-02-10	AIN 	Initial implementation
2015-04-30	AIN 	CR# 35548 - rename username on user, added some length checks
2016-08-17 AIN      Added exception handling when updating records
***********************************************************/

public with sharing class ULC_DisableUserLeftCompany  {
	
	//Class is run from scheduler ULC_DisableUserLeftCompanySchedUser
	public static void DeactivateSFDCUsers()
	{
		system.debug('DeactivateSFDCUsers start');
		string shortDatePart = ToDateString(system.today(), false);
		string datePart = ToDateString(system.today(), true);
		List<User> users = [Select Id, UserName, IsActive, FederationIdentifier, Alias, Email, CommunityNickname From User Where IsActive = true and Contact.Left_Company__c = true LIMIT 100];

		if(users.size() == 0)
		{
			system.debug('No active users with left company flag found');
			return;
		}

		for(User user : users)
		
		{
			system.debug('Id:' + user.Id + ';Prev IsActive:' + user.IsActive + ';New IsActive:' + false);
			user.IsActive = false;

			system.debug('Id:' + user.Id + ';Prev INT_NetSuite_InternalID__c:' + user.INT_NetSuite_InternalID__c + ';New INT_NetSuite_InternalID__c:' + '');
			user.INT_NetSuite_InternalID__c = '';

			if (user.FederationIdentifier != null && user.FederationIdentifier.Length() > 0)
            {
            	string newFederationIdentifier = shortDatePart + '_' + user.FederationIdentifier;
            	if (newFederationIdentifier.Length() > 512)
                    newFederationIdentifier = newFederationIdentifier.Substring(0, 512);
            	system.debug('Id:' + user.Id + ';Prev FederationIdentifier:' + user.FederationIdentifier + ';New FederationIdentifier:' + newFederationIdentifier);
				user.FederationIdentifier = newFederationIdentifier;
            }

			string newUsername = shortDatePart + '_' + user.Username;
            if (newUsername.Length() > 80)
                    newUsername = newUsername.Substring(0, 80);
			system.debug('Id:' + user.Id + ';Prev Username:' + user.Username + ';New Username:' + newUsername);                    
			user.Username = newUsername;

            string newAlias = shortDatePart + '_' + user.Alias;
			if (newAlias.Length() > 8)
                    newAlias = newAlias.Substring(0, 8);
            system.debug('Id:' + user.Id + ';Prev Alias:' + user.Alias + ';New Alias:' + newAlias);
			user.Alias = newAlias;

            string newEmail = shortDatePart + '_' + user.Email;
			system.debug('Id:' + user.Id + ';Prev Email:' + user.Email + ';New Email:' + newEmail);
			user.Email = newEmail;

			string newNickname = datePart + '_' + user.CommunityNickname;
			if (newNickname.Length() > 40)
                    newNickname = newNickname.Substring(0, 40);
			system.debug('Id:' + user.Id + ';Prev CommunityNickname:' + user.CommunityNickname + ';New CommunityNickname:' + newNickname);
			user.CommunityNickname = newNickname;
		}
		try 
		{ 
			update users; 
		} 
		catch(DmlException ex) 
		{ 
			system.debug('Exception when updating user records: ' + ex.getMessage()); 
		} 
		
		
	}
	public static String STATUS_LEFT_COMPANY = 'Disabled - Left Company';
	private static string IdQlikTechSSO = '0012000000I7QJhAAN';
	//Class is run from scheduler ULC_DisableUserLeftCompanySchedULC
	public static void DeactivateULCDetails()
	{
		string shortDatePart = ToDateString(system.today(), false);
		string datePart = ToDateString(system.today(), true);
		string newULCName;
		string newULCPassword;

		List<ULC_Details__c> ULCDetails = [Select Id, ULCName__c, ULC_Password__c, ULCStatus__c, contactId__r.Account.Id from ULC_Details__c Where ContactId__r.Left_Company__c = true and ULCStatus__c != :STATUS_LEFT_COMPANY LIMIT 100];

		if(ULCDetails.size() == 0)
		{
			system.debug('No ULC details found that needs updating');
			return;
		}
		for(ULC_Details__c ULCDetail : ULCDetails)
		{
			system.debug('Id:' + ULCDetail.Id + ';ULCName__c:' + ULCDetail.ULCName__c + 'Prev Status:' + ULCDetail.ULCStatus__c + ';New Status:' + STATUS_LEFT_COMPANY);
			ULCDetail.ULCStatus__c = STATUS_LEFT_COMPANY;

			if(ULCDetail.contactId__r != null && ULCDetail.contactId__r.Account.Id!=null && ULCDetail.contactId__r.Account.Id != IdQlikTechSSO)
			{
				//User if not part of QlikTech SSO
				/*newULCName = shortDatePart + '_' + ULCDetail.ULCName__c;
				if (newULCName.Length() > 20)
                	newULCName = newULCName.Substring(0, 20);
				system.debug('Id:' + ULCDetail.Id + ';Prev ULCName__c:' + ULCDetail.ULCName__c + ';New ULCName__c:' + newULCName);
				ULCDetail.ULCName__c = newULCName;*/

				newULCPassword = datePart + '_' + ULCDetail.ULC_Password__c;
				if (newULCPassword.Length() > 50)
                	newULCPassword = newULCPassword.Substring(0, 50);
				system.debug('Id:' + ULCDetail.Id + ';Prev ULC_Password__c:' + ULCDetail.ULC_Password__c + ';New ULC_Password__c:' + newULCPassword);
				ULCDetail.ULC_Password__c = newULCPassword;

			}
		}

		try 
		{ 
			update ULCDetails; 
		} 
		catch(DmlException ex) 
		{ 
			system.debug('Exception when updating ULCDetail records: ' + ex.getMessage()); 
		} 
	}
	//Class is run from scheduler ULC_DisableUserLeftCompanySchedULC
	public static void DeactivateAssignedULCLevels()
	{
		List<Assigned_ULC_Level__c> assignedULCLevels = [Select Id, Status__c From Assigned_ULC_Level__c Where ContactId__r.Left_Company__c = true and Status__c != :STATUS_LEFT_COMPANY LIMIT 100];

		if(assignedULCLevels.size() == 0)
		{
			system.debug('No assigned ULC levels found that needs updating');
			return;
		}
		for(Assigned_ULC_Level__c assignedULCLevel : assignedULCLevels)
		{
			system.debug('Id:' + assignedULCLevel.Id + ';Prev Status:' + assignedULCLevel.Status__c + ';New Status:' + STATUS_LEFT_COMPANY);
			assignedULCLevel.Status__c = STATUS_LEFT_COMPANY;
		}
		try 
		{ 
			update assignedULCLevels; 
		} 
		catch(DmlException ex) 
		{ 
			system.debug('Exception when updating assignedULCLevels records: ' + ex.getMessage()); 
		} 
	}
	private static string ToDateString(Date indate, boolean longYear)
	{
		string year, month, day;
		year = string.valueof(indate.year());
		if(!longYear)
			year = year.right(2);
		month = string.valueof(indate.month());
		if(month.length() < 2 )
			month = '0' + month;
		day = string.valueof(indate.day());
		if(day.length() < 2 )
			day = '0' + day;
		return  year+month+day;
	}
}