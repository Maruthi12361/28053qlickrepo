@isTest
private class TestProblemCase
{
    static testMethod void testCreateProblemCase()
    {
        //Setup test data
        Case c = new Case();
        c.OwnerId = UserInfo.getUserId();
        c.Bug_ID__c = 'Test Bug ID';
        insert c;
        
        Test.startTest();
        //Test controller with empty case
        Test.setCurrentPageReference(Page.CreateProblemCase);
        Case emptyCase = new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(emptyCase);
        CreateProblemCaseContExt cpcce = new CreateProblemCaseContExt(sc);
        PageReference pr = cpcce.initPage();
        System.assertEquals(null, pr);
        //Test controller with valid case
        System.currentPageReference().getParameters().put('id', c.Id);
        sc = new ApexPages.StandardController(c);
        cpcce = new CreateProblemCaseContExt(sc);
        pr = cpcce.initPage();
       // System.assert(pr.getUrl().contains(c.Id));
    }
}