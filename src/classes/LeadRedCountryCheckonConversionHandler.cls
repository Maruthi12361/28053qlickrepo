public class LeadRedCountryCheckonConversionHandler{
    private static final String SettingName = 'SteelbrickSettingsDetails';
    public class myException extends Exception {}
	public LeadRedCountryCheckonConversionHandler() {
        
    }

    public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
        for (Integer i = 0; i < triggerNew.size(); i++) {
            Lead newLead = triggerNew[i];
            Lead oldLead = triggerOld[i];
             /*Steelbrick_Settings__c sbSetting = Steelbrick_Settings__c.getInstance(SettingName); 
               if(Test.isRunningTest() && sbSetting == null){
                QuoteTestHelper.createCustomSettings();
                sbSetting = Steelbrick_Settings__c.getInstance(SettingName); 
            }*/
            //String redCountries = sbSetting .RedCountry__c;
            String redCountries = System.Label.VC_Red_Country;
            if (trigger.isUpdate && newLead.IsConverted && !oldLead.IsConverted){
                if(String.isNotBlank(newLead.Country) && redCountries.contains(newLead.Country)){
                    newLead.addError( new myException('Lead Conversion for Restricted country leads is not allowed.'));
                }    
            }
        }
    }
}