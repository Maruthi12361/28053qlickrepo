<!--
* Change log:
*
*   2019-09-30 ext_bad	IT-2173: Edit At_Risk_Escalation component.
-->
<apex:component id="EditAtRiskEscalationComponent" controller="EditAtRiskEscalationController" allowDML="true">
    <apex:attribute type="AddAtRiskEscalationByAccountController" name="escFromAccount"
                    assignTo="{!escFromAccountController}"
                    required="false" description="AddAtRiskEscalationByAccountController"/>

    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"/>

    <script type="text/javascript">
        function disableFields() {

        }
    </script>

    <apex:form >
        <apex:pageBlock title="Escalation {!escalation.Name}">
            <apex:pageBlockSection columns="2" rendered="{!!existingRiskEscalation}">
                <apex:inputField required="true" value="{!escalation.Escalated_By_Name__c}"/>
                <apex:inputField value="{!escalation.Date_of_Esculation__c}"/>
                <apex:inputField required="true" value="{!escalation.Team__c}"/>
                <apex:pageBlockSectionItem >
                    <label style="font-weight: bold; color: #4a4a56;">Attached Documents:</label>&nbsp;
                    <apex:inputFile value="{!attachmentFile.Body}" fileName="{!attachmentFile.Name}" id="file"/>
                </apex:pageBlockSectionItem>
                <apex:inputField required="true" value="{!escalation.Reason__c}"/>
                <apex:inputTextarea value="{!escalation.Comments__c}"/>
                <apex:inputField value="{!escalation.Escalated_Reason__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" rendered="{!existingRiskEscalation}">
                <apex:outputField value="{!escalation.Name}"/>
                <apex:outputField value="{!escalation.Account__c}"/>
                <apex:outputField value="{!escalation.Team__c}"/>
                <apex:outputField value="{!escalation.Date_of_Esculation__c}"/>
                <apex:outputField value="{!escalation.Reason__c}"/>
                <apex:outputField value="{!escalation.Comments__c}"/>
                <apex:outputLabel ></apex:outputLabel>
                <apex:outputField value="{!escalation.Escalated_Reason__c}"/>
            </apex:pageBlockSection>
        </apex:pageBlock>
        <apex:pageBlock title="Follow-up" rendered="{!existingRiskEscalation}">
            <apex:actionFunction name="rerenderConfirmedReason" id="rerenderConfirmedReason" reRender="section,sectionRejected"/>
            <apex:outputPanel rendered="{!!rejected}">
                <apex:pageBlockSection columns="2" id="section">
                    <apex:inputField value="{!escalation.ICSM_Owner__c}"/>
                    <apex:inputField value="{!escalation.Completion_date__c}"/>
                    <apex:inputField value="{!escalation.Escalation_reason_correct__c}"
                                     onChange="rerenderConfirmedReason()"/>
                    <apex:inputTextarea value="{!escalation.Comments_for_second_section__c}"/>
                    <apex:inputField value="{!escalation.ICSM_Confirmed_Reason__c}" id="confirmedReason"
                                     rendered="{!escalation.Escalation_reason_correct__c == 'no'}"/>
                </apex:pageBlockSection>
                <apex:pageBlockSection columns="1">
                    <apex:selectList value="{!escalation.Status__c}" multiSelect="false" size="1">
                        <apex:selectOptions value="{!statusOptions}"/>
                    </apex:selectList>
                    <apex:inputField value="{!escalation.Reason_for_second_section__c}"/>
                </apex:pageBlockSection>
                <apex:pageBlockSection columns="2" rendered="{!escalation.Rejected__c}">
                    <apex:outputField value="{!escalation.Rejected__c}"/>
                    <apex:outputField value="{!escalation.Rejected_Date__c}"/>
                </apex:pageBlockSection>
            </apex:outputPanel>
            <apex:outputPanel rendered="{!rejected}">
                <apex:pageBlockSection columns="2" id="sectionRejected">
                    <apex:outputField value="{!escalation.ICSM_Owner__c}"/>
                    <apex:outputField value="{!escalation.Completion_date__c}"/>
                    <apex:outputField value="{!escalation.Escalation_reason_correct__c}"/>
                    <apex:outputField value="{!escalation.Comments_for_second_section__c}"/>
                    <apex:outputField value="{!escalation.ICSM_Confirmed_Reason__c}" id="confirmedReason"
                                     rendered="{!escalation.Escalation_reason_correct__c == 'no'}"/>
                </apex:pageBlockSection>
                <apex:pageBlockSection columns="1">
                    <apex:outputField value="{!escalation.Status__c}"/>
                    <apex:outputField value="{!escalation.Reason_for_second_section__c}"/>
                </apex:pageBlockSection>
                <apex:pageBlockSection columns="2" rendered="{!escalation.Rejected__c}">
                    <apex:outputField value="{!escalation.Rejected__c}"/>
                    <apex:outputField value="{!escalation.Rejected_Date__c}"/>
                </apex:pageBlockSection>
            </apex:outputPanel>
            <apex:outputPanel id="popup" styleClass="popup" style="display:none">
                <apex:outputPanel styleClass="customPopup" layout="block" id="rejectPopup">
                    <apex:pageBlock title="Rejected Reason" id="reasonBlock">
                        <apex:pageMessages />
                        <apex:pageBlockSection >
                            <apex:inputTextarea value="{!escalation.Rejected_Reason__c}"
                                                style="width: 350px; height: 100px;"/>
                        </apex:pageBlockSection>
                        <apex:pageBlockButtons >
                            <apex:commandButton value="Save" action="{!saveReason}" reRender="reasonBlock"/>
                            <apex:commandButton value="Cancel" reRender="popup"/>
                        </apex:pageBlockButtons>
                    </apex:pageBlock>
                </apex:outputPanel>
            </apex:outputPanel>

        </apex:pageBlock>

        <div class="b2" align="center" draggable="false">
            <apex:commandButton value="Save" action="{!save}"/>
            <apex:commandButton value="Reject" onClick="showPopup()" reRender="empty"
                                rendered="{!existingRiskEscalation && !escalation.Rejected__c}"/>
            <apex:commandButton value="Cancel" immediate="true" action="{!cancel}"/>
        </div>

        <style type='text/css'>
            .customPopup {
                background-color: lightgrey;
                border-style: solid;
                border-radius: 10px;
                border-width: 1px;
                left: 55%;
                position: fixed;
                padding: 5px;
                box-shadow: 0px 3px 7px rgba(1, 1, 1, 5);
                background: #fff;
                width: 600px;
                margin-left: -200px;
                top: 10px;
                max-width: 700px;
            }
        </style>
        <script type="text/javascript">
            var jqEnv;
            if (typeof $ === 'undefined')
                jqEnv = jQuery.noConflict();
            else
                jqEnv = $.noConflict();

            function showPopup() {
                jqEnv('.popup').css('display', 'block');
            }
        </script>

    </apex:form>
</apex:component>