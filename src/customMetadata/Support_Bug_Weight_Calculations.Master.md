<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Master</label>
    <protected>false</protected>
    <values>
        <field>Case_Base_Weight__c</field>
        <value xsi:type="xsd:double">230.0</value>
    </values>
    <values>
        <field>Case_Escalation_Proactive_Red__c</field>
        <value xsi:type="xsd:double">500.0</value>
    </values>
    <values>
        <field>Case_Escalation_Proactive_Yellow__c</field>
        <value xsi:type="xsd:double">250.0</value>
    </values>
    <values>
        <field>Case_Escalation_Reactive_Red__c</field>
        <value xsi:type="xsd:double">1000.0</value>
    </values>
    <values>
        <field>Case_Escalation_Reactive_Yellow__c</field>
        <value xsi:type="xsd:double">600.0</value>
    </values>
    <values>
        <field>Case_Weight_Per_Associated_Case__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Entitlement_Signature_Support_SLA__c</field>
        <value xsi:type="xsd:double">60.0</value>
    </values>
    <values>
        <field>Priority_High__c</field>
        <value xsi:type="xsd:double">20.0</value>
    </values>
    <values>
        <field>Priority_Medium_Low__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Priority_Urgent__c</field>
        <value xsi:type="xsd:double">50.0</value>
    </values>
    <values>
        <field>Product_Saas_1__c</field>
        <value xsi:type="xsd:string">,Qlik Sense Cloud,Qlik Cloud Services,</value>
    </values>
    <values>
        <field>Product_Saas_Value__c</field>
        <value xsi:type="xsd:double">70.0</value>
    </values>
    <values>
        <field>Service_Class_1__c</field>
        <value xsi:type="xsd:double">426.0</value>
    </values>
    <values>
        <field>Service_Class_2__c</field>
        <value xsi:type="xsd:double">150.0</value>
    </values>
    <values>
        <field>Service_Class_3__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Severity_1__c</field>
        <value xsi:type="xsd:double">100.0</value>
    </values>
    <values>
        <field>Severity_2__c</field>
        <value xsi:type="xsd:double">60.0</value>
    </values>
    <values>
        <field>Severity_3__c</field>
        <value xsi:type="xsd:double">20.0</value>
    </values>
</CustomMetadata>
