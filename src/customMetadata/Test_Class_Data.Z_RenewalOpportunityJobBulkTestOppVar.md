<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Z_RenewalOpportunityJobBulkTestOppVar</label>
    <protected>false</protected>
    <values>
        <field>Numeric_Variable_Value__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Test_Class_Name__c</field>
        <value xsi:type="xsd:string">Z_RenewalOpportunityJobBulkTest</value>
    </values>
    <values>
        <field>Variable_Name__c</field>
        <value xsi:type="xsd:string">NumberOfOpps</value>
    </values>
</CustomMetadata>
