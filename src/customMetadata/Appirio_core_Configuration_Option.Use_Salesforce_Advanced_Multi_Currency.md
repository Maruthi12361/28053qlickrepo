<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Use_Salesforce_Advanced_Multi_Currency</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">USD</value>
    </values>
    <values>
        <field>Name__c</field>
        <value xsi:type="xsd:string">Use_Salesforce_Advanced_Multi_Currency</value>
    </values>
    <values>
        <field>appirio_core_Allow_Multiple_Values__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>appirio_core_Config_Group_Name__c</field>
        <value xsi:type="xsd:string">Currencies</value>
    </values>
    <values>
        <field>appirio_core_Config_Group__c</field>
        <value xsi:type="xsd:string">Currencies</value>
    </values>
    <values>
        <field>appirio_core_Description__c</field>
        <value xsi:type="xsd:string">This configuration determines whether Salesforce&apos;s Advanced Multi-Currency and DatedConversionRate or Appirio&apos;s custom Currency Rate tables should be used for multi-currency conversions.</value>
    </values>
    <values>
        <field>appirio_core_Picklist_Values__c</field>
        <value xsi:type="xsd:string">true false</value>
    </values>
    <values>
        <field>appirio_core_Type__c</field>
        <value xsi:type="xsd:string">Boolean</value>
    </values>
    <values>
        <field>appirio_core_Version__c</field>
        <value xsi:type="xsd:string">1.1</value>
    </values>
</CustomMetadata>
