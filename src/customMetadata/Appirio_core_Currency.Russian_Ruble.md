<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Russian Ruble</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">USD</value>
    </values>
    <values>
        <field>Name__c</field>
        <value xsi:type="xsd:string">Russian Ruble</value>
    </values>
    <values>
        <field>Owner__c</field>
        <value xsi:type="xsd:string">QT Qtmetadata</value>
    </values>
    <values>
        <field>appirio_core_Currency_Code__c</field>
        <value xsi:type="xsd:string">RUB</value>
    </values>
    <values>
        <field>appirio_core_Is_Corporate_Currency__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
