<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Use_Dated_Exchange_Rates</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">USD</value>
    </values>
    <values>
        <field>Name__c</field>
        <value xsi:type="xsd:string">Use_Dated_Exchange_Rates</value>
    </values>
    <values>
        <field>appirio_core_Allow_Multiple_Values__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>appirio_core_Config_Group_Name__c</field>
        <value xsi:type="xsd:string">Currencies</value>
    </values>
    <values>
        <field>appirio_core_Config_Group__c</field>
        <value xsi:type="xsd:string">Currencies</value>
    </values>
    <values>
        <field>appirio_core_Description__c</field>
        <value xsi:type="xsd:string">Determines whether dated exchange rates should be used for multi-currency conversions. Is true by default but may be set to false if multiple currencies are not being used in the app.</value>
    </values>
    <values>
        <field>appirio_core_Picklist_Values__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>appirio_core_Type__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>appirio_core_Version__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
</CustomMetadata>
