<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CER-34537</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">USD</value>
    </values>
    <values>
        <field>Name__c</field>
        <value xsi:type="xsd:string">CER-34537</value>
    </values>
    <values>
        <field>appirio_core_Currency__c</field>
        <value xsi:type="xsd:string">Canadian_Dollar</value>
    </values>
    <values>
        <field>appirio_core_Effective_Date__c</field>
        <value xsi:type="xsd:date">2018-01-01</value>
    </values>
    <values>
        <field>appirio_core_Rate__c</field>
        <value xsi:type="xsd:double">1.298701</value>
    </values>
</CustomMetadata>
