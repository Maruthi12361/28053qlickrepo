<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MR4</label>
    <protected>false</protected>
    <values>
        <field>Controlling_field_API_Name__c</field>
        <value xsi:type="xsd:string">Partner_Category__c</value>
    </values>
    <values>
        <field>Controlling_field_Value__c</field>
        <value xsi:type="xsd:string">Master Reseller &amp; Distribution</value>
    </values>
    <values>
        <field>Dependant_field_API_Name__c</field>
        <value xsi:type="xsd:string">Partner_Type__c</value>
    </values>
    <values>
        <field>Dependant_field_Value__c</field>
        <value xsi:type="xsd:string">MR - Elite Reseller</value>
    </values>
    <values>
        <field>Object_Name_for_Dependent_Picklist__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
</CustomMetadata>
