/***************************************************************************************************************************************
*
*  Log Changes:
*  Initial:		MHG     Initial trigger to add user to CPQ
*
*  2011-11-08: 	RDZ 	Changes related to CR# 3342: Trigger CPQ update if any of the mapped fields are changed
*						https://eu1.salesforce.com/a0CD000000FyLHs
*						Adding static method to check if specific fields have been changed and then the call to
*						update the cpq user.
*  2014_10_13:  CCE     Added test so we don't do callout when running test methods
*  30.03.2020   ext_bjd ITRM-409 Added flag ENABLE_USER_TRIGGER to bypass trigger
***************************************************************************************************************************************/

trigger AddCPQUser on User (before insert, before update) {

	Set<ID> Profiles = new Set<ID>();
	Map<string, Boolean> UserIdsUpdateRequired = new Map<string, Boolean> ();
	System.debug('AddCPQUser: Starting');

    if (CustomUserTriggerHandler.ENABLE_USER_TRIGGER) {

        for (User user : Trigger.New) {
            if (!Profiles.contains(user.ProfileId)) {
                Profiles.add(user.ProfileId);
            }
        }

        if (Profiles.size() > 0) {
            List<string> UserIdsToUpdate = new List<string>();

            if (Trigger.isUpdate) {
                UserIdsUpdateRequired = ULC.IsRequiredCPQUserUpdate(Trigger.New, Trigger.old, Profiles);
            } else {
                for (User u : Trigger.new) {
                    UserIdsUpdateRequired.Put(u.Id, true);
                }
            }

            for (integer i = 0; i < Trigger.New.size(); i++) {
                User user = Trigger.New[i];

                if (Trigger.isInsert) {
                    UserIdsToUpdate.add(user.Id);
                } else {
                    if (UserIdsUpdateRequired.containsKey(user.Id)) {
                        UserIdsToUpdate.add(user.Id);
                    }
                }
            }

            if (UserIdsToUpdate.size() > 0) {
                System.debug('AddCPQUser: Updating these IDs - ' + UserIdsToUpdate);
                if (!Test.isRunningTest()) {
                    ULC.CallUpsertCPQUser(UserIdsToUpdate);
                }    //CCE 2014_10_13 added test so we don't do callout when running test methods
                //ULC.CallUpsertCPQUser(UserIdsToUpdate);
            }
        }

        if (!UserIdsUpdateRequired.isEmpty()) {
            for (User user : Trigger.New) {
                if (UserIdsUpdateRequired.containsKey(user.Id)) {
                    user.Trigger_CPQ_user_creation__c = false;
                    user.CPQ_User_Status__c = '';
                }
            }
        }
    }
}