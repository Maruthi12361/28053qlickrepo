/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Linus Löfberg
* @version        1.0
* @created        2019-06-04
* @modified       2019-06-04
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* [IH - 2020-05-21] - RTR-596, now calls the Z_RenewalSubscriptionTriggerHandler
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
trigger Z_SubscriptionTrigger on Zuora__Subscription__c (after insert, after update, before insert, before update) {

    Z_SubscriptionTriggerHandler handler = new Z_SubscriptionTriggerHandler(Trigger.isExecuting, Trigger.size);

    if (Trigger.isInsert && Trigger.isBefore) {
        handler.OnBeforeInsert(Trigger.new);
    } else if (Trigger.isInsert && Trigger.isAfter) {
        handler.OnAfterInsert(Trigger.new);
        Z_RenewalSubscriptionTriggerHandler.handleAfterInsert(Trigger.newMap);
    }
    //  else if (Trigger.isUpdate && Trigger.isBefore) {
    //     handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    // } else if (Trigger.isUpdate && Trigger.isAfter) {
    //     handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    // }
}