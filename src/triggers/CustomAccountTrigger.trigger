/*
 File Name: CustomAccountTrigger

 2018-01-01 Viktor@4Front QCW-4609, QCW-4760 Trigger consolidation.
 2018-04-01 | AYS | BMW-739 | Added AccountSharingHandler class
 29/5/2018 BSL-418 done some modifications trigger consolidation   shubham gupta
 11/12/2018 BSL-1216 adding method to dummy update child opportunities(Open oppties only) in after update trigger.
 09/01/2019 BSL-1224 Added DunsAccountHandler on before update and After Update shubham gupta
 2019-03-04 BAD - Qlik Commerce - removing sharing - and readding it again...
 2020-02-27 - OTQ-44 Shubham Gupta QdiCheckHandler.qdiCheckUpdate added in after update call.
*/
trigger CustomAccountTrigger on Account (
        before insert, before update,
        after insert, after update,
        before delete, after delete) {
// add necessary events when it will be needed - , after undelete
        if (Trigger.isInsert) { // Insert
            if (Trigger.isBefore) { // before Insert
                if (!Semaphores.CustomAccountTriggerBeforeInsert) {
                    Semaphores.CustomAccountTriggerBeforeInsert = true;

                    System.debug(LoggingLevel.DEBUG,'[AccountAddressMappingHandler.onBeforeInsert]');
                    AccountAddressMappingHandler.onBeforeInsert(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[AccountBillingCountryCodeHandler.onBeforeInsert]');
                    AccountBillingCountryCodeHandler.onBeforeInsert(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[AccountSetSupportOfficeHandler.onBeforeInsert]');
                    AccountSetSupportOfficeHandler.onBeforeInsert(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleUpdateGroupEmpRangeAccount]');
                    CustomAccountTriggerHandler.handleUpdateGroupEmpRangeAccount(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleSetNamedAccountDates]');
                    CustomAccountTriggerHandler.handleSetNamedAccountDates(Trigger.new, null);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            } else if (Trigger.isAfter) { // after Insert
                if (!Semaphores.CustomAccountTriggerAfterInsert) {
                    Semaphores.CustomAccountTriggerAfterInsert = true;

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountNavisionStatusChange]');
                    CustomAccountTriggerHandler.handleAccountNavisionStatusChange(Trigger.new, null);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountSelfServeChange]');
                    CustomAccountTriggerHandler.handleAccountSelfServeChange(Trigger.new, null);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate]');
                    CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate(Trigger.new, null, Trigger.isInsert, false, false, false, Trigger.isAfter);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountSharingRulesOnPartnerPortalAccounts]');
                    CustomAccountTriggerHandler.handleAccountSharingRulesOnPartnerPortalAccounts(Trigger.new, null);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                }
            }
        } else if (Trigger.isUpdate) {// Update
            if (Trigger.isBefore) { // before Update
                if (!Semaphores.CustomAccountTriggerBeforeUpdate) {
                    Semaphores.CustomAccountTriggerBeforeUpdate = true;

                    System.debug(LoggingLevel.DEBUG,'[AccountAddressMappingHandler.onBeforeUpdate]');
                    AccountAddressMappingHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[AccountBillingCountryCodeHandler.onBeforeUpdate]');
                    AccountBillingCountryCodeHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[AccountSetSupportOfficeHandler.onBeforeUpdate]');
                    AccountSetSupportOfficeHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountPartnerRecordUpdate]');
                    CustomAccountTriggerHandler.handleAccountPartnerRecordUpdate(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleUpdateGroupEmpRangeAccount]');
                    CustomAccountTriggerHandler.handleUpdateGroupEmpRangeAccount(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleSetNamedAccountDates]');
                    CustomAccountTriggerHandler.handleSetNamedAccountDates(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    //System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountLockCheck]');
                    //CustomAccountTriggerHandler.handleAccountLockCheck(Trigger.new, Trigger.old);
                    //System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[PartnerAccountTerminationHandler.TerminatePartnerAccounts]');
                    PartnerAccountTerminationHandler.TerminatePartnerAccounts(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[AccountSharingHandler.onBeforeUpdate]');
                    AccountSharingHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[DunsHandlerAccount.populateCustomer]');
                    DunsHandlerAccount.populateCustomer(Trigger.new,Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    
                }
            } else if (Trigger.isAfter) { // after Update
                if (!Semaphores.CustomAccountTriggerAfterUpdate) {
                    Semaphores.CustomAccountTriggerAfterUpdate = true;
                    //QCW-1125 Used to update primary quotes of account on account sic code change
                    System.debug(LoggingLevel.DEBUG,'[AccountIndustrySectorFunctionHandler.onAfterUpdate]');
                    AccountIndustrySectorFunctionHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    
                    System.debug(LoggingLevel.DEBUG,'[AccChannelProgramHandler.onAfterUpdate]');
                    AccChannelProgramHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleLegalApprovalRequestSubmitForApproval]');
                    CustomAccountTriggerHandler.handleLegalApprovalRequestSubmitForApproval(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    
                    System.debug(LoggingLevel.DEBUG,'[AccountEcustomsScreeningTriggerHandler.onAfterUpdate]');
                    AccountEcustomsScreeningTriggerHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountUpdateULCTrigger]');
                    AccountUpdateULCHelper.updateULC(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleUpdateAssingedLevels]');
                    CustomAccountTriggerHandler.handleUpdateAssingedLevels(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountNavisionStatusChange]');
                    CustomAccountTriggerHandler.handleAccountNavisionStatusChange(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountSelfServeChange]');
                    CustomAccountTriggerHandler.handleAccountSelfServeChange(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate]');
                    CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate(Trigger.new, Trigger.old, false, Trigger.isUpdate, false, false, Trigger.isAfter);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountSharingRulesOnPartnerPortalAccounts]');
                    CustomAccountTriggerHandler.handleAccountSharingRulesOnPartnerPortalAccounts(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    //System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountSharingRulesOnPartnerPortalAccounts]');
                    //Method to Dummy update all the child opportunities so that Deal Desk Support field can be updated by workflows called:
                    //OPP: SDD Set Deal Desk Support To High Touch
                    //OPP: SDD Clear Deal Desk Support
                    CustomAccountTriggerHandler.handleDealDeskSupportUpdate(Trigger.new, Trigger.oldMap);
                    System.debug(LoggingLevel.DEBUG,'[DunsHandlerAccount.dunsChangesAfter]');
                    DunsHandlerAccount.dunsChangesAfter(Trigger.new,Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    
                    System.debug(LoggingLevel.DEBUG,'[QdiCheckHandler.QdiCheckHandler]');
                    QdiCheckHandler.qdiCheckUpdate(Trigger.new, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            }
        } else if (Trigger.isDelete) { // Delete
            if (Trigger.isBefore) { // before Delete
                if (!Semaphores.CustomAccountTriggerBeforeDelete) {
                    Semaphores.CustomAccountTriggerBeforeDelete = true;
                //TODO EXECUTE
                CustomAccountTriggerHandler.handleAccountControlDelete(Trigger.old);
                //CustomAccountTriggerHandler.handleAccountLockCheck(null, Trigger.old);
                //CustomAccountTriggerHandler.handleAccountBeforeDelete(Trigger.old);
                
                System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate]');
                CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate(null, Trigger.old, false, false, Trigger.isDelete, Trigger.isBefore, false);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            } else if (Trigger.isAfter) { // after Delete
                if (!Semaphores.CustomAccountTriggerBeforeDelete) {
                    Semaphores.CustomAccountTriggerBeforeDelete = true;
                //TODO EXECUTE
                //CustomAccountTriggerHandler.handleAccountAfterDelete();
                System.debug(LoggingLevel.DEBUG,'[CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate]');
                CustomAccountTriggerHandler.handleAccountAfterInsertOrUpdate(null, Trigger.old, false, false, Trigger.isDelete, false, Trigger.isAfter);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
            }
        }
        } else if (Trigger.isUnDelete) { // Undelete
            //TODO EXECUTE
        }

}