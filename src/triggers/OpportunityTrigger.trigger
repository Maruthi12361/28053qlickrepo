/****
===== Rodion Vakulovskyi : 28.02.2017 : Changes to Trigger.Moved From CustomOpportunityTrigger
===== Rodion Vakulovskyi : 15.03.2017 : SOQL error
      17-Mar-2017 MTM Set QCW-1633 OEM End user account for OEM rec deal
      27-Mar-2017 MTM Set QCW-1633 OEM deals set subscription values
      29.07.2017 Rodion Vakulvoskyi
    2017-06-14 Rodion Vakulvoskyi added static flag to get rid of soql query limit
    2017-06-22 Rodion Vakulvoskyi added static flag to get rid of soql query limit
    2017-06-27 Rodion Vakulvoskyi syncing code between Prod and DEV/QA
    2017-09-27 QCW-3804 - Nikhil Jain - Populating Subscription Start Date for MSP 
  2017-06-11 Anjuna Baby Q2CW-3635 Update Quotes of Closed Won opportunities
    2017-09-25 Shubham Gupta QCW-2849 added New Handler to implement deal split Line Items creation line 47
    2017-11-23 Reshma Ravi QCW-4215 Populating fields on opportunity closed won
  2017-12-07 Anjuna Baby QCW-4486 Update forecastcatagory as omitted for DS opportunites on before update
  2018-11-07 ext_bad CHG0034322 populate SendToRegion field
  2019-1-09 BSL-1261 shubham Gupta Added NewCustomerHandler on after update trigger
  2019-02-20 BAD - Qlik Commerce - removing sharing
  2019-07-26 Anjuna Baby - Qlik Commerce - Added new logic to support a delegate process for when PAM's are on holiday so that the approvals can be reassigned to another person.
  2020-02-26 DOB - OTQ-8: To fix SOQL error.
  2020-06-11 DKF - DTR-600 - Added new handler, OppClosedLostSigUpdateHandler
**/
trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    //TriggerFactory.createTriggerDispatcher(Opportunity.sObjectType);
    private static String FUNC_STR = 'OpportunityTrigger on Opportunity ';

    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            System.debug(FUNC_STR + 'This is before insert start.');
            System.debug('Semaphore is: ' + Semaphores.OpportunityTriggerBeforeInsert);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            if(!Semaphores.OpportunityTriggerBeforeInsert) {
                Semaphores.OpportunityTriggerBeforeInsert = true;
                // query for Opp and Currency
                OpportunityExecDealsVisibilityHandler.handle(Trigger.new, true, false);
                //updating Opp Source based on logged in user type  Navneet kumar
                oppSourceHandler.handle(Trigger.new, true);
                // query for Currency
                // query for OpportunityLineItems and NS_Quotes__r
                //OpportunityManageForecastProductsHandler.hanleInsert(Trigger.old, Trigger.new, Trigger.isBefore, Trigger.isAfter);
                OpportunityMarketTypeHandler.handle(Trigger.new);
                OpportunityUpdateReferralHandler.handle(Trigger.oldMap, Trigger.new, true, false);
                OppCreatedByNameHandler.handle(Trigger.old, Trigger.new);
                OppGIAcceptanceCtrlHandler.handle(Trigger.oldMap, Trigger.new);
                OppPartnerContactHandler.handle(Trigger.old, Trigger.new);
                OpportunityUpdateOppOwnerIntIdHandler.handleInsert(Trigger.new);
                OppIfPartnerDealRecPSMAtClosedWonHandler.handle(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
                OppVoucherManagementCreationHandler.handle(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate);
                //OpportunitySetDefaultValueHandler.handle(Trigger.new);
                //===============Rodion changes==================
                OpportunityReCalcForecastAmountHandler.onBeforeInsert(trigger.new, Trigger.newMap);
                OEMPartnerDiscountHandler.onBeforeInsert(trigger.new);
                OpportunitySetDefaultValueHandler.handle(Trigger.new);
                OpportunitySetDefaultValueHandler.SetOEMEndUserAccount(Trigger.New);
                //================end of Rodion changes==========
                //updating Payout_Partner_Account__c based on Payout_Partner_Contact__c  Navneet kumar
                OpportunityPartnerprogramHandler.Inserthandler(Trigger.new);
                //OpportunityPopulatePricebookHandler.handle(Trigger.new);
                //QCW-2948 -- Populating the Type on Opportunity
                OpportunitySetDefaultValueHandler.SetOpportunityType(Trigger.new);
                OppPopulateSendToRegionHandler.populateSendToRegion(Trigger.new);
                //BSL-2204
                DelegateApprovalRequest.handle(Trigger.new, Trigger.new);
            }
            System.debug(FUNC_STR + 'This is before insert end.');
        }
        if(Trigger.isAfter) {
            System.debug(FUNC_STR + 'This is after insert start.');
            System.debug('Semaphore is: ' + Semaphores.OpportunityTriggerAfterInsert);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            if(!Semaphores.OpportunityTriggerAfterInsert) {
                Semaphores.OpportunityTriggerAfterInsert = true;
                //OpportunityLegalCompanyCheckHandler.handle(Trigger.new);
                OpportunityWhenEISCreateOppsHandler.handle(Trigger.new);
                //OpportunityManageForecastProductsHandler.hanleInsert(Trigger.old, Trigger.new, false, true);
                //OppSharingRulesOnSellThroughPtnerHandler.handleInsert(Trigger.new);


                OppUpdateContactSOIHandler.handle(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isUpdate, Trigger.isInsert, Trigger.isAfter, Trigger.isBefore, Trigger.isDelete);
                OpportunityLastModifiedHandler.handle(Trigger.new, Trigger.old, true, false);
                //03.02.2017
                if(!Test.isRunningTest()){
                    OpportunityForecastProductsHandler.afterInsert(Trigger.new);
                }

                OppDealSplitScheduleHandler.onAfterInsert(Trigger.new,Trigger.newMap);

            }
            System.debug(FUNC_STR + 'This is after insert end.');
        }
    }
    if(Trigger.isUpdate) {
        if(Trigger.isBefore) {
            System.debug(FUNC_STR + 'This is before update start.');
            System.debug('Semaphore is: ' + Semaphores.OpportunityTriggerBeforeUpdate);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            if(!Semaphores.OpportunityTriggerBeforeUpdate) {
                Semaphores.OpportunityTriggerBeforeUpdate = true;
                OpportunityMandatorySoIOnUpdateHandler.handle(Trigger.old, Trigger.new, Trigger.newMap);
                //OppHasSmallBusinessEdServerHandler.handle(Trigger.new);
                OpportunityExecDealsVisibilityHandler.handle(Trigger.new, false, true);
                OpportunityMarketTypeHandler.handle(Trigger.new);
                OpportunityUpdateReferralHandler.handle(Trigger.oldMap, Trigger.new, false, true);
                OppCreatedByNameHandler.handle(Trigger.old, Trigger.new);
                OppGIAcceptanceCtrlHandler.handle(Trigger.oldMap, Trigger.new);
                OppOnlyBdrManagerCanChangeBdrGdHandler.handle(Trigger.old, Trigger.new);
                OppPartnerContactHandler.handle(Trigger.old, Trigger.new);
                OpportunityPreviousOwnerHandler.handle(Trigger.old, Trigger.new);
                //OppPreventUpdateForecastAmountsHandler.handle(Trigger.old, Trigger.new, Trigger.oldMap);
                //OppUpdateAccOwnerNameHandler.handle(Trigger.oldMap, Trigger.new); //QCW-4215
                OpportunitySetDefaultValueHandler.SetOEMEndUserAccount(Trigger.New);
                //OpportunitySetDefaultValueHandler.handle(Trigger.new);
                //OpportunityManageForecastProductsHandler.hanleUpdate(Trigger.old, Trigger.new, true, false);
                OpportunityUpdateSolutionsProfileHandler.handle(Trigger.new, Trigger.old);
            }
            //QCW-4215
            OppUpdateAccOwnerNameHandler.handle(Trigger.oldMap, Trigger.new);
            if (!Semaphores.OpportunityTriggerClosedEmail) {
                OpportunityClosedHandler.sendEmail(Trigger.newMap, Trigger.oldMap);
            }
            //OppSharingRulesOnSellThroughPtnerHandler.handleUpdate(Trigger.old, Trigger.new);
            if(!Semaphores.OpportunityTriggerBeforeUpdate2) {
                Semaphores.OpportunityTriggerBeforeUpdate2 = true;
                OpportunityUpdateOppOwnerIntIdHandler.handleUpdate(Trigger.new, Trigger.oldMap);
                OpportunityPartnerprogramHandler.Updatehandler(Trigger.new , Trigger.newMap , Trigger.oldMap);
                OppIfPartnerDealRecPSMAtClosedWonHandler.handle(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
                OppVoucherManagementCreationHandler.handle(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate);
                /*******Only On QA, DEV and not on Prod*******
                OpportunityDealCloseWonHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
                **/
                //OpportunityDealCloseWonHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
                OEMPartnerDiscountHandler.onBeforeUpdate(trigger.new);
                OpportunityReCalcForecastAmountHandler.onBeforeUpdate(trigger.new, Trigger.newMap, trigger.old, Trigger.oldMap);
                //logic for name assigning for opportunity
                List<Opportunity> listForOpps = new List<Opportunity>();
                for (Opportunity item : trigger.new) {
                    if (item.AccountId != Trigger.oldMap.get(item.id).AccountId) {
                        listForOpps .add(item);
                    }
                }
                if (!listForOpps.isEmpty()) {
                    OppCustomTriggerHandler.onBeforeUpdate(listForOpps);
                }
                OpportunitySetDefaultValueHandler.BeforeUpdate(trigger.new, Trigger.oldMap);
                OppPopulateSendToRegionHandler.populateSendToRegion(Trigger.new);
                /*******Only On QA, DEV and not on Prod*******
                OpportunityPopulatePricebookHandler.handle(Trigger.new);
                **/
                //OpportunityPopulatePricebookHandler.handle(Trigger.new);
            }
            //OpportunitySetDefaultValueHandler.setSubscriptionDateMSP(Trigger.new,Trigger.oldMap);//QCW-3804
            //QCW-4486 Update forecast category of closed deal splits
            UpdateClosedOppQuotesHandler.OnBeforeUpdate(Trigger.new);

            System.debug(FUNC_STR + 'This is before update end.');
        }
        if(Trigger.isAfter) {
            System.debug(FUNC_STR + 'This is after update start.');
            System.debug('Semaphore is: ' + Semaphores.OpportunityTriggerAfterUpdate);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            //03.02.2017
            if(!Semaphores.OpportunityTriggerAfterUpdate) {
                Semaphores.OpportunityTriggerAfterUpdate = true;
                if (Z_RenewalOpportunityTriggerHandler.runRenewalOpportunityTrigger) {
                    Z_RenewalOpportunityTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.new);
                }
                if(!Test.isRunningTest()){
                    OpportunityForecastProductsHandler.afterUpdate(Trigger.new, Trigger.oldMap);
                }
                //OpportunityLegalCompanyCheckHandler.handle(Trigger.new);
                OpportuntyChildCloneHandler.handle(Trigger.new);
                //OpportunityManageForecastProductsHandler.hanleUpdate(Trigger.old, Trigger.new, false, true);
                OppCPQAfterUpdateAccountWriteBackHandler.handle(Trigger.new, Trigger.oldMap);
                OppUpdateContactSOIHandler.handle(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isUpdate, Trigger.isInsert, Trigger.isAfter, Trigger.isBefore, Trigger.isDelete);
            
                OpportunitySetDefaultValueHandler.handleCloseDateForQuote(Trigger.new,Trigger.newMap,Trigger.oldMap);            
            
            }
            NewCustomerHandler.processCustomerFlag(Trigger.new,Trigger.oldMap);
            OpportunityLastModifiedHandler.handle(Trigger.new, Trigger.old, false, true);
            //DTR-600 Update Signature Success on Closed Lost
            OppClosedLostSigUpdateHandler.OppClosedLostSignatureUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
            if(!Semaphores.OpportunityTriggerAfterUpdate2) {
                Semaphores.OpportunityTriggerAfterUpdate2 = true;
                OppCustomTriggerHandler.processBillingSchedule(Trigger.new, Trigger.oldMap);
            }else{//QCW-3804
                List<Opportunity> listForOpps = new List<Opportunity>();
                for (Opportunity opp: trigger.new){
                    if(opp.Revenue_Type__c != NULL && opp.Revenue_Type__c != '' && opp.Revenue_Type__c.containsIgnoreCase('MSP') && opp.CloseDate != Trigger.Oldmap.get(opp.Id).CloseDate){
                        listForOpps.add(opp);
                    }
                }
                if(!listForOpps.isEmpty()){
                    OppCustomTriggerHandler.FLAGTOCHECK = false;
                    OppCustomTriggerHandler.processBillingSchedule(listForOpps, Trigger.oldMap);
                }
            }

            //Q2CW-3635 Update Quotes of Closed Won opportunities
            UpdateClosedOppQuotesHandler.OnAfterUpdate(Trigger.new);
            
            System.debug(FUNC_STR + 'This is after update end.');
        }
    }
    if(Trigger.isDelete) {
        if(Trigger.isBefore) {
            System.debug(FUNC_STR + 'This is before delete start.');
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            OppUpdateContactSOIHandler.handle(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isUpdate, Trigger.isInsert, Trigger.isAfter, Trigger.isBefore, Trigger.isDelete);

            System.debug(FUNC_STR + 'This is before delete end.');
        }
        if(Trigger.isAfter) {

        }
    }
    if(Trigger.isUndelete) {

    }

}