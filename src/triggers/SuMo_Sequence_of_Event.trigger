trigger SuMo_Sequence_of_Event on Sequence_of_Event__c (after insert, after update) {
  success.SuMoTriggerHandler handler = new success.SuMoTriggerHandler();
  if(Trigger.isInsert && Trigger.isAfter){
    if(success.SuMoObjectSettings.getInstance().objectIsEnabled('Sequence_of_Event__c')){
      handler.OnInsert(Trigger.new) ;
    }
  } else if(Trigger.isUpdate && Trigger.isAfter){
    if(success.SuMoObjectSettings.getInstance().objectIsEnabled('Sequence_of_Event__c')){
      handler.OnUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }
  }
}