/************************************************************************

	TriggerPortalControlOnLevelModification
	
	This trigger will do an outbound call to the ULC-webservice on
	modification of the Assigned ULC-level	
		
	Changelog:
		2010-01-13	MHG 	GoLive of ULCv3
		2011-02-15  MHG		CR# 1470 - Trigger portal control on modification of QlikBuy level
							https://eu1.salesforce.com/a0CD000000CAvAD
							
*************************************************************************/ 
trigger TriggerPortalControlOnLevelModification on Assigned_ULC_Level__c (after insert, after update) 
{

	Set<ID> Contacts = new Set<ID>();
	Set<ID> PortalLevels = new Set<ID>();
	List<Assigned_ULC_Level__c> AssingedLevelsToUpdate = new List<Assigned_ULC_Level__c>();
	
	for (ULC_Level__c Level : [select Id from ULC_Level__c where name like 'CP%' or name like 'PRM%' or name = 'QlikBuy'])	// name = 'QlikBuy' is not case sensetive
	{
		PortalLevels.Add(Level.Id);
	}
	
	if (PortalLevels.size() == 0) return;

	for (integer i = 0; i < Trigger.New.size(); i++)
	{
		System.debug(Trigger.New[i]);
		if (Trigger.New[i].Status__c == 'Approved - No trigger')
		{
			Assigned_ULC_Level__c Level = Trigger.New[i].clone();
			Level.Status__c = 'Approved';
			AssingedLevelsToUpdate.add(Level);		
		}
		else
		{
			if (Trigger.isUpdate && Trigger.Old[i].Status__c == 'Approved - No trigger') 
			{
				continue;
			}
			else
			{
				if (PortalLevels.contains(Trigger.New[i].ULCLevelId__c))
				{
						Contacts.Add(Trigger.New[i].ContactId__c);				
				}
			}
		}
	}
	System.debug('Assigned levels' + AssingedLevelsToUpdate);
	if (AssingedLevelsToUpdate.size() > 0)
	{
		update AssingedLevelsToUpdate;		
	}
	
	System.debug('Contacts' + Contacts);	
	if (Contacts.size() > 0)
	{

		List<Contact> ToUpdate = new List<Contact>();		

		for (Contact contact : [select Id, TriggerPortalControl__c from Contact where Id in :Contacts])
		{
			if (!contact.TriggerPortalControl__c)
			{
				contact.TriggerPortalControl__c = true;
				ToUpdate.Add(contact);
			}			
		}
		System.debug('ToUpdate' + ToUpdate);		
		if (ToUpdate.size() > 0)
		{
			update ToUpdate;			
		}
	}

}