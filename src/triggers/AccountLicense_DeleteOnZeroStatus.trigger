/******************************************************

	Trigger: AccountLicense_DeleteOnZeroStatus
	
	Initiator: Steve Smith 
	
	Changelog:
		2009-10-20	MHG		Created file
				
******************************************************/
trigger AccountLicense_DeleteOnZeroStatus on Account_License__c (after update) {

	List<Account_License__c> toDelete = new List<Account_License__c>();
	
	for(Account_License__c lic : Trigger.new) {
		
		if (lic.Status__c != 0)
			toDelete.Add(new Account_License__c(ID = lic.ID));
		
	}

	if (toDelete.size() > 0)
		delete(toDelete);
}