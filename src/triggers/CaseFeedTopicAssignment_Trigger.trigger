/**************************************************
* Author: Madhav Kakani Fluido Denmark
* Trigger to assign a topic also to a Case if the Case happens to be the parent of the FeedItem
* Change Log:
* 2015-07-07 Madhav Kakani: CR# 20197 - add topic to the related parent
*        Initial Development
* 2018-01-30 ext_vos CHG0032996: skip 'topic' logic for Portal users, formatted code.
**************************************************/
trigger CaseFeedTopicAssignment_Trigger on TopicAssignment (after insert, after delete) {
    List<TopicAssignment> lstTopics = null;
    
    // ignore an assignment of topic for Portal users
    Set<String> portalUserTypes = new Set<String>{'PowerPartner', 'CSPLitePortal', 'CustomerSuccess', 'PowerCustomerSuccess'};
    if (portalUserTypes.contains(UserInfo.getUserType())) {
        return;
    }

    if (Trigger.isInsert) {
        lstTopics = Trigger.new;
    } else if (Trigger.isDelete) {
        lstTopics = Trigger.old;
    }
    
    // Collect all topics whose EntityId is a FeedItem 
    Map<Id, Id> feedItemIds = new Map<Id, Id>(); // Map of topic id and entity id
    for (TopicAssignment ta : lstTopics) {
        if (ta.EntityType == 'FeedItem') {
            feedItemIds.put(ta.TopicId, ta.EntityId);
        }
    }
    if (feedItemIds.isEmpty()) {
        return;
    }
         
    // Get a list of all the matching feed items
    Map<Id, FeedItem> mapFeedItems = new Map<Id, FeedItem>(); // Map of feed item id and feed item
    for (FeedItem fi : [SELECT Id, ParentId FROM FeedItem WHERE Id IN :feedItemIds.values()]) {
        mapFeedItems.put(fi.Id, fi);
    }    
    if (mapFeedItems.isEmpty()) {
        return;
    }

    // Check if the FeedItem parent is a Case, and if yes, assign the topic to the Case
    for (TopicAssignment ta : lstTopics) {
        Id feeditemId = feedItemIds.get(ta.TopicId);
        if (feeditemId == null) {
            continue;
        }        
        FeedItem fi = mapFeedItems.get(feeditemId);
        if (fi == null || fi.ParentId == null) {
            continue;
        }        
        // Assign or unassign the topic only for Case records
        String obName = fi.ParentId.getSObjectType().getDescribe().getName();
        if (obName != null && obName.endsWithIgnoreCase('Case')) {
            if (Trigger.isInsert) {
                ConnectApi.Topics.assignTopic(null, fi.ParentId, ta.TopicId);
            } else if (Trigger.isDelete) { 
                ConnectApi.Topics.unassignTopic(null, fi.ParentId, ta.TopicId);
            }
        }        
    }
}