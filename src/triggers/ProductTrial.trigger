// --------------------------------------------------------------------------------------------------------------------------
// Created by Leonardo Grauer - 16/03/20
//
// OTQ - 330 - For new customers when the new eval process is used that the ERP status of the account is set to Trial License
// --------------------------------------------------------------------------------------------------------------------------
trigger ProductTrial on Product_Trial__c (after insert) {

    if (Trigger.isInsert){
        ProductTrialTriggerHelper.updateAccountERPStatus(Trigger.new);
    }//if

}//trigger