trigger CustomUserTrigger on User (before insert, before update, after insert, after update) {
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            CustomUserTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if (Trigger.isAfter) {
            CustomUserTriggerHandler.onAfterInsert(Trigger.new);
        }
    }
    if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            CustomUserTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
        if (Trigger.isAfter) {

        }
    }
}