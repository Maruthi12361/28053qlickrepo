trigger SpecificationAssignApprover on Specification__c (before insert, before update) {

	ChangeControlUtil.AssignApproverToSpecification(Trigger.New);

}