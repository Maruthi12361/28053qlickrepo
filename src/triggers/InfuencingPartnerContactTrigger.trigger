/******************************************************

    Class: InfuencingPartnerContactTrigger
    
     Changelog:
    Kumar Navneet
    20/04/17    
    For updating Influencing currency from Opportunity currency
    
    Changelog:
        2012-05-21  CCE     Created file
        2013=03-06  TJG     Partner of CR 7332 updates. We need to make sure that 
                            the value associated to the “Referral PSM/QM” field on 
                            the Influencing Partners related list is driven by the 
                            name of the Account Owner of the partner added and not 
                            that of the Contact Owner
		2020-05-02	DOB		Invoke Approval process depending on criteria 
******************************************************/
trigger InfuencingPartnerContactTrigger on Influencing_Partner__c (before insert, before update, after insert, after update)
{
    system.debug('InfuencingPartnerContactTrigger: Starting');
    
    if(Trigger.isBefore){
        if(Trigger.isInsert && !Semaphores.InfluencingPartnerBeforeInsert) {
            Semaphores.InfluencingPartnerBeforeInsert = true;
            InfuencingPartnerHandler.Inserthandler(Trigger.new);
        }else if(Trigger.isUpdate && !Semaphores.InfluencingPartnerBeforeUpdate){
            Semaphores.InfluencingPartnerBeforeUpdate = true;
            InfuencingPartnerHandler.updatePartnerAccountHandler(Trigger.new);
        }
    }
    
    if(Trigger.isAfter){
        if(Trigger.isInsert && !Semaphores.InfluencingPartnerAfterInsert) {
            Semaphores.InfluencingPartnerAfterInsert = true;
            InfuencingPartnerHandler.invokeApprovalProcessHandler(Trigger.new, Trigger.OldMap);
        }else if(Trigger.isUpdate && !Semaphores.InfluencingPartnerAfterUpdate){
            Semaphores.InfluencingPartnerAfterUpdate = true;
            InfuencingPartnerHandler.invokeApprovalProcessHandler(Trigger.new, Trigger.OldMap);
        }
    }
    
   
    system.debug('InfuencingPartnerContactTrigger: Finishing');
}