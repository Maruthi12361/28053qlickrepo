// SOIUpdateContact
// Updates a contact's Is Closed Won SOI field when the Opp stage is Closed Won for certain record types     
// Changelog:
// 2013-04-02  Madhav Kakani - Fluido Oy Initial development for CR# 7356
// 2018-10-18  CCE CHG0034877 BMW-1074 Update Contact Status field on Contact from SOI
//
trigger SOIUpdateContact on Sphere_of_Influence__c (after insert, after update, after delete) {
    List<RecordType> lstRec = [SELECT Id from RecordType WHERE SobjectType='Opportunity'
                                AND IsActive=true
                                AND Name NOT IN('OEM - Order','OEM - Run Rate',
                                'OEM - Standard Sales Process','OEM Prepay',
                                'OEM Recruitment (CCS)')];
    //System.Debug('SOIUpdateContact: lstRec = ' + lstRec);                            
    //Get a Set of Contact Id's from the list of Sphere_of_Influence__c
    Map<Id, String> ctIDs = new Map<Id, String>();
    Map<Id, String> ctIDs2 = new Map<Id, String>();
    Set<Id> oppId = new Set<Id>();
    
    if(Trigger.isDelete) {
        for(Sphere_of_Influence__c soi : Trigger.old) {
            // get a list of all Opps where this contact is part of the SOI
            List<Sphere_of_Influence__c> lstTmp = [SELECT Opportunity__c FROM Sphere_of_Influence__c
                                                    WHERE Contact__c = :soi.Contact__c];

            for(Sphere_of_Influence__c x : lstTmp) { // skip the current opp because the contact is being removed from it
                if(x.Opportunity__c != soi.Opportunity__c) oppId.add(x.Opportunity__c);
            }                

            List<Opportunity> lstOpp = [SELECT StageName FROM Opportunity
                                        WHERE Id IN :oppId AND RecordTypeId IN :lstRec];
                                                
            // Now check the Opps to see if any one of them is Closed Won
            Boolean bClosed = false;
            for(Opportunity y : lstOpp) {
                if(y.StageName == 'Closed Won') { // Atleast one is a closed won opp
                    bClosed = true;
                    break; 
                }
            }

            if(bClosed == true) ctIDs.put(soi.Contact__c, 'Yes'); 
            else ctIDs.put(soi.Contact__c, 'No');
        }
    }
    else {
        for(Sphere_of_Influence__c soi : Trigger.new) {
            // Get a list of all Opportunities where this contact is part of the SOI
            List<Sphere_of_Influence__c> lstTmp = [SELECT Opportunity__c FROM Sphere_of_Influence__c
                                                    WHERE Contact__c = :soi.Contact__c];
            //System.Debug('SOIUpdateContact: lstTmp = ' + lstTmp);
            for(Sphere_of_Influence__c x : lstTmp) oppId.add(x.Opportunity__c);
            
            List<Opportunity> lstOpp = [SELECT StageName FROM Opportunity
                                        WHERE Id IN :oppId AND RecordTypeId IN :lstRec];
            //System.Debug('SOIUpdateContact: lstOpp = ' + lstOpp);                                        
            // Now check the Opps to see if any one of them is Closed Won
            Boolean bClosed = false;
            for(Opportunity y : lstOpp) {
                if(y.StageName == 'Closed Won') { // Atleast one is a closed won opp
                    bClosed = true;
                    break; 
                }
            }

            if(bClosed == true) ctIDs.put(soi.Contact__c, 'Yes'); 
            else ctIDs.put(soi.Contact__c, 'No');
            //System.Debug('SOIUpdateContact: ctIDs = ' + ctIDs);
            if (Trigger.isInsert && soi.Contact__c != null && soi.From_Lead__c == true) {
                ctIDs2.put(soi.Contact__c, soi.Opportunity_Stage__c);
            }
            //System.Debug('SOIUpdateContact: ctIDs2 = ' + ctIDs2);
        }
    }    

    List<Contact> lstCt = [SELECT Is_Closed_Won_SOI__c, Contact_Status__c FROM Contact where Id IN :ctIDs.keySet() OR Id IN :ctIDs2.keySet()];
    //System.Debug('SOIUpdateContact: lstCt = ' + lstCt);
    for(Integer i = 0; i < lstCt.size(); i++) {
        lstCt[i].Is_Closed_Won_SOI__c = ctIDs.get(lstCt[i].Id);
        if (ctIDs2.get(lstCt[i].Id) != null) {
            lstCt[i].Contact_Status__c = ctIDs2.get(lstCt[i].Id);
        }
        //System.Debug('SOIUpdateContact: Is_Closed_Won_SOI__c = ' + lstCt[i].Is_Closed_Won_SOI__c);
        //System.Debug('SOIUpdateContact: Contact_Status__c = ' + lstCt[i].Contact_Status__c);
    }
    
    if(lstCt.size() > 0) update lstCt;

}