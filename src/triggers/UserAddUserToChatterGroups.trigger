/*******************************************************************
* Trigger UserAddUserToChatterGroups
*
* Trigger for adding employees to designated chatter groups.
* 
* Change Log:
* 2014-04-25   AIN (Andreas Nilsson)	 	Initial Development
*											CR# 11324
* 2017-04-26   AIN (Andreas Nilsson)        Added logic to avoid SOQL queries
*
* Trigger adds employees to the chatter groups listed in the helper class UserAddUserToChatterGroups
*
* 30.03.2020  ext_bjd                       ITRM-409 Added flag ENABLE_USER_TRIGGER to bypass trigger
*
**********************************************************************/

trigger UserAddUserToChatterGroups on User (after insert) {

	if(Semaphores.UserAddUserToChatterGroupsHasRun) {
		return;
	}
	if (CustomUserTriggerHandler.ENABLE_USER_TRIGGER) {
		Semaphores.UserAddUserToChatterGroupsHasRun = true;
		boolean isEmployee = false;
		Set<Id> userRoleIdsFromTrigger = new Set<Id>();
		Set<Id> idsOfEmployees = new Set<Id>();
		List<UserRole> userRoles = new List<UserRole>();
		Map<ID, UserRole> UsersToUserRoles = new Map<ID, UserRole>();
		List<User> usersToCheck = new List<User>();

		for (User userObj : trigger.new) {
			if (userobj.IsActive &&
					(userObj.FederationIdentifier == null ? true : userObj.FederationIdentifier.indexOf('ext_') == -1) &&
					(userObj.Email.indexOf('qliktech.com') >= 0 || userObj.Email.indexOf('qlikview.com') >= 0 || userObj.Email.indexOf('qlik.com') >= 0))

				usersToCheck.add(userObj);
			userRoleIdsFromTrigger.add(userObj.UserRoleId);
		}
		if (userRoleIdsFromTrigger.size() > 0)
			userRoles = [select id, name from UserRole where Id in :userRoleIdsFromTrigger];

		for (UserRole userRole : userRoles)
			UsersToUserRoles.put(userRole.Id, userRole);


		for (User userObj : usersToCheck) {
			system.debug('Checking user with id: ' + userObj.id);

			//Is user active, has a @qlik email and is not a consultant?

			isEmployee = true;
			//List of exceptions so we don't add users that represent groups of employees like helpdesk.
			for (string emailException : UserAddUserToChatterGroups.EmailExceptions) {
				if (userObj.Email.indexOf(emailException) == 0) {
					system.debug('Email ' + userObj.Email + ' was in the list of exceptions, will not be added to chatter');
					isEmployee = false;
					break;
				}
			}
			//Userrole can not be a single signon hold account
			if (userObj.UserRoleId != null) {
				if (UsersToUserRoles.containsKey(userObj.UserRoleId)) {
					UserRole userRole = UsersToUserRoles.get(userObj.UserRoleId);
					system.debug('User role is: ' + userRole.id + ', Name: ' + userRole.Name);
					if (userRole.Name.indexOf('QlikTech Single Signon Hold Account') >= 0) {
						isEmployee = false;
					}
				} else {
					system.debug('UserRole  ' + userObj.UserRoleId + ' not found in Map!');
				}
			} else
					system.debug('User ' + userObj.id + ' does not have a role defined.');


			if (isEmployee) {
				idsOfEmployees.add(userObj.id);
				system.debug('User with Id: ' + userObj.Id + ' is an employee, will be added to chatter groups');
			} else
					system.debug('User with Id: ' + userObj.Id + ' is not an employee');
		}

		if (idsOfEmployees.size() == 0)
			return;

		system.debug('Adding users to chatter groups');
		UserAddUserToChatterGroups.AddUserToChatterGroups(idsOfEmployees, UserAddUserToChatterGroups.ChatterGroups);
	}
	
}