/********************************************************
* TRIGGER: WebActivityTrigger
* DESCRIPTION: Trigger for Web Activity
*
* CHANGELOG:    
    2018-10-15 - BAD - Added Initial logic
*********************************************************/
trigger WebActivityTrigger on Web_Activity__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if(Trigger.isInsert){
        if(Trigger.isBefore){

            WebActivityHandler.handleBeforeInsert(Trigger.new);

        }
        if(Trigger.isAfter){

            WebActivityHandler.handleAfterInsert(Trigger.new);

        }
    }

    if(Trigger.isUpdate){
        if(Trigger.isBefore){
            if (System.isBatch()){
                WebActivityHandler.handleBeforeInsert(Trigger.new);
            }
        }
        if(Trigger.isAfter){
        }
    }


}