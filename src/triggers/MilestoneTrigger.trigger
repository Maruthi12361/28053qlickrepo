/****************************************************************
*
* MilestoneTrigger
*
* 2019-04-24 extbad IT-1742 Update pse__Assignments after change 'Closed for Timecard Entry' field
* 2020-03-17 extbad IT-2578 send email for Signature Success Approved Milestones
*
*****************************************************************/
trigger MilestoneTrigger on pse__Milestone__c (before update) {

    if (Trigger.isUpdate) {
        if (!Semaphores.MilestoneTriggerAfterUpdate) {
            Semaphores.MilestoneTriggerAfterUpdate = true;
            MilestoneHandler.updateAssignments(Trigger.newMap, Trigger.oldMap);
            MilestoneHandler.updateTimecards(Trigger.newMap, Trigger.oldMap);
            MilestoneHandler.processSignatureSuccessMilestones(Trigger.newMap, Trigger.oldMap);
        }
    }
}