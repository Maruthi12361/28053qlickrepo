/*
 File Name: BugTrigger

 2020-05-06 AIN IT-2767 creation of trigger framework
*/

trigger BugTrigger on Bugs__c (after insert, before update, before delete) {
    if(trigger.isInsert){
        if(trigger.isAfter){
            BugToCase.BugToCase(trigger.new);
        }
    }
    if(trigger.isUpdate){
        if(trigger.isBefore){
            BugWeightRecalculator.recalculateBugWeight(trigger.new, trigger.oldMap);
        }
    }
}