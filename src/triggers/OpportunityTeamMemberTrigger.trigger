/**
 *	Description		:	This trigger is used to handle pre and post opertaion on OpportunityTeamMember.
 *
 *	Created By		:	ext_nup
 *
 *	Created Date	:	12/13/2017
 *
 *	Version			:	V_1.0
 *
 *	Revision Log	:	V_1.0 - Created
**/ 
 
trigger OpportunityTeamMemberTrigger on OpportunityTeamMember (after delete, after insert, after update) {
	
	//Checking for event type
	if(Trigger.isAfter) {
		
		//checking for DML operation
		if(Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete) {
			
			//Calling helper method 
			OpportunityTeamMemberTriggerHelper.populateExecutiveSponsor(Trigger.New  , Trigger.oldMap);
		}
		
	} 
}