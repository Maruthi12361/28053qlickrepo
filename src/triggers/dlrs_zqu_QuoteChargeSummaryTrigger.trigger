/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_zqu_QuoteChargeSummaryTrigger on zqu__QuoteChargeSummary__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(zqu__QuoteChargeSummary__c.SObjectType);
}