/*********************************************
Change Log:

2018-07-09 ext_bad CHG0034165: create a trigger to populate Qlikview_Support_Escalation__c fields.
2020-05-05 ain  IT-2767
********************************************/
trigger CaseEscalationTrigger on Qlikview_Support_Escalation__c (before insert, after insert, before update, after update, after delete ) {
    system.debug('CaseEscalationTrigger start');
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            CaseEscalationHandler.populateFields(Trigger.new);
            CaseEscalationHandler.populateContactRole(Trigger.new, new Map<Id, Qlikview_Support_Escalation__c>());
        } else if(Trigger.isAfter) {
            system.debug('CaseEscalationTrigger after insert');
            CaseEscalationHandler.sendEmails(Trigger.new);
            CaseEscalationHandler.updateBugWeight(trigger.new, trigger.oldMap);
        }
    } else if(Trigger.isUpdate){
        if(Trigger.isBefore){
            for (Qlikview_Support_Escalation__c esc : Trigger.new) {
                if (esc.From_Quick_Action__c) {
                    esc.From_Quick_Action__c = false;
                }
            }
            CaseEscalationHandler.populateContactRole(Trigger.new, Trigger.oldMap);
        }
        else if(Trigger.isAfter){
            system.debug('CaseEscalationTrigger after update');
            CaseEscalationHandler.updateBugWeight(trigger.new, trigger.oldMap);
        }
        
    }
    else if(Trigger.isDelete){
        if(Trigger.isAfter){
            system.debug('CaseEscalationTrigger after delete');
            CaseEscalationHandler.updateBugWeight(trigger.new, trigger.oldMap);
        }
    }
    system.debug('CaseEscalationTrigger end');
}