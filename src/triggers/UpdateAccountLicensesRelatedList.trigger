/*    Changelog:
        2016-02-02  IRN     CR33068 partnershare project - Created file 
*/
trigger UpdateAccountLicensesRelatedList on NS_Support_Contract_Item__c (after insert, after update) {
    System.debug('---UpdateAccountLicensesRelatedList---' +AccountLicensesRelatedList.isUpdated);
  if(!AccountLicensesRelatedList.isUpdated){
    AccountLicensesRelatedList accountLicenseRL = new AccountLicensesRelatedList();
      accountLicenseRL.populateList(Trigger.New);  
  }
}