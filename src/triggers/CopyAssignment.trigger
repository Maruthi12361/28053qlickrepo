// trigger to copy/update/delete the Assignment object - CR#9634
// Change log:
// October 10, 2013 - Madhav Kakani - Fluido Oy
trigger CopyAssignment on pse__Assignment__c (after insert, after update, after delete) {
    if(Trigger.isAfter && Trigger.isInsert) {
        Set<Id> projIds = new Set<Id>(); // collect the list of projects ids
        for(pse__Assignment__c ass : Trigger.new) projIds.add(ass.pse__Project__c);
        
        // Get the list of matching Project_SE objects
        List<Project_SE__c> projSE = [SELECT Master_Project_Id__c, Id FROM Project_SE__c
                                        WHERE Master_Project_Id__c IN :projIds];

        // create a map of master project id and projSE ids
        Map<Id, Id> mapProj = new Map<Id, Id>();
        for(Project_SE__c proj: projSE) mapProj.put(proj.Master_Project_Id__c, proj.Id);
        
        List<Assignment_SE__c> lstAssignSE = new List<Assignment_SE__c>();

        for(pse__Assignment__c ass : Trigger.new) {
            Assignment_SE__c ase = new Assignment_SE__c();
            ase.Name = ass.Name;
            ase.Resource__c = ass.pse__Resource__c;
            ase.Master_Assignment_Id__c = ass.Id;
            ase.Project_SE__c = mapProj.get(ass.pse__Project__c);
            ase.End_Date__c = ass.pse__End_Date__c;
            lstAssignSE.add(ase);
        }
        if((lstAssignSE != null) && (lstAssignSE.size() > 0)) database.insert(lstAssignSE, false);
    }
    else if(Trigger.isAfter && Trigger.isUpdate) {
        Set<Id> assIds = new Set<Id>(); // collect the list of Assignment ids
        for(pse__Assignment__c ass : Trigger.new) assIds.add(ass.Id);

        // Get the list of matching Assignment_SE objects
        List<Assignment_SE__c> lstASE = [SELECT Id, Master_Assignment_Id__c, Name, Resource__c, End_Date__c FROM Assignment_SE__c
                                        WHERE Master_Assignment_Id__c IN :assIds];

        // create a map of master assignment id and assignment SEs
        Map<Id, Assignment_SE__c> mapAss = new Map<Id, Assignment_SE__c>();
        for(Assignment_SE__c ase : lstASE) mapAss.put(ase.Master_Assignment_Id__c, ase);
        
        List<Assignment_SE__c> lstAssignSE = new List<Assignment_SE__c>();
        for(pse__Assignment__c ass : Trigger.new) {
            Assignment_SE__c ase = mapAss.get(ass.Id);
            if(ase == null) continue;
            ase.Name = ass.Name;
            ase.Resource__c = ass.pse__Resource__c;
            ase.End_Date__c = ass.pse__End_Date__c;
            lstAssignSE.add(ase);
        }
        if((lstAssignSE != null) && (lstAssignSE.size() > 0)) database.update(lstAssignSE, false);
    }
    else if(Trigger.isAfter && Trigger.isDelete) {
        Set<Id> assIds = new Set<Id>(); // collect the list of Assignment ids
        for(pse__Assignment__c ass : Trigger.old) assIds.add(ass.Id);

        // get the list of matching assignment SE objects
        List<Assignment_SE__c> lstAssignSE = [SELECT Id FROM Assignment_SE__c WHERE Master_Assignment_Id__c IN :assIds];        
        if((lstAssignSE != null) && (lstAssignSE.size() > 0)) database.delete(lstAssignSE, false);
    }
  
}