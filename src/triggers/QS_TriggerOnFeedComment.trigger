/**************************************************
* Change Log:
* 2019-12-17 extbad: exclude Attunity Closed Cases  to external users IT-2329
* 2019-12-17 Vlad Nareyko: new validation IT-2321
*
**************************************************/
trigger QS_TriggerOnFeedComment on FeedComment (after insert, before insert, before update) {
    if (Trigger.isBefore) {
        if (!ChatterPostsAccessValidator.isUserHaveAccess()) {
            ChatterPostsAccessValidator.setErrors(Trigger.new);
        }
    }
    if (Trigger.isAfter && Trigger.isInsert) {
        QS_FeedCommentTriggerHandler.afterTriggerHandler(ChatterPostsAccessValidator.excludeAttunityClosedCases(Trigger.newMap));
    }
}