/**************************************************
*
* Change Log:
* 
* 2013-10-10    CCE Initial development. CR# 9755 - Follow-Up and Lead Lifecycle Changes - 9.12.13
*                   https://eu1.salesforce.com/a0CD000000bSsB7 "Create trigger for NVM activities logged
*                   where type is blank". Test class is in TaskSetTypeTest.cls
*                   Note: Contains Org dependancies
* 2014-04-03   MTM Add new ContactWorld User to trigger TaskSetType https://eu1.salesforce.com/a3R/e?CF00ND0000003i0lu
* 2014-06-11   SLH Add new ContactWorld User APAC to trigger TaskSetType https://eu1.salesforce.com/005D0000004CBhH
* 2016-06-06   CCE CR# 86352 - adding new features and combining with TaskUpdateTypeField.trigger and Task_cant_be_saved_when_Type_is_Select validation rule
*                  Most of this has been moved into a helper class (TaskSetTypeHelper.class) so that we can control the order and reduce the SOQL quereies.
* 2018-11-01   CCE CHG0034917 BMW-1077 Fix #Follow-Up Attempt counter not always counting
**************************************************/
trigger TaskSetType on Task (after insert, after update) {
    System.debug('TaskSetType: Starting... oldMap>' + Trigger.oldMap + ' newMap>' + Trigger.newMap + ' isInsert>' + Trigger.isInsert + ' isUpdate>' + Trigger.isUpdate);
    System.debug('TaskSetType: Starting... newMap size = ' + Trigger.newMap.size());
    
    if (!Semaphores.TriggerHasRun('TaskSetType'))
    {
        System.debug('TaskSetType: Running... oldMap>' + Trigger.oldMap + ' newMap>' + Trigger.newMap + ' isInsert>' + Trigger.isInsert + ' isUpdate>' + Trigger.isUpdate);
        TaskSetTypeHelper TaskHelper = new TaskSetTypeHelper(Trigger.oldMap, Trigger.newMap, Trigger.isInsert, Trigger.isUpdate);
        TaskHelper.ProcessTasks();
    }
    else if (!Semaphores.TriggerHasRun('TaskSetType2')) //Added for BMW-1077
    {
        System.debug('TaskSetType: Running2... oldMap>' + Trigger.oldMap + ' newMap>' + Trigger.newMap + ' isInsert>' + Trigger.isInsert + ' isUpdate>' + Trigger.isUpdate);
        TaskSetTypeHelper TaskHelper = new TaskSetTypeHelper(Trigger.oldMap, Trigger.newMap, Trigger.isInsert, Trigger.isUpdate);
        TaskHelper.ProcessTasks();
    }
    System.debug('TaskSetType: SOQL Queries = ' +  Limits.getQueries());
    System.debug('TaskSetType: Finishing');    
}