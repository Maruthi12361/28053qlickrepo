/************************************************************************************
*
*	ChangeControlUpdateUATHistory
*	Trigger to move the contents of the UAT Evidence field to the UAT Evidence History field with aggrigated history.
*	CR# 10110 - Add Release Management to CR Process
*	
*	2014-11-13 		AIN 	Initial Development
*
*************************************************************************************/
trigger ChangeControlUpdateUATHistory on SLX__Change_Control__c (before update) {

	Map<Id, boolean> oldChangeSet = new Map<Id, boolean>();
	for(SLX__Change_Control__c c : trigger.old) {
		oldChangeSet.put(c.Id, c.Submitted_to_requestor__c);

	}


	for(SLX__Change_Control__c c : trigger.new) {

		if(c.Submitted_to_requestor__c == true && (c.UATEvidence__c != null && c.UATEvidence__c != ''))
		{
			if(oldChangeSet.containsKey(c.Id))
			{
				//If Submitted_to_requestor__c has been set to true during this transaction
				if(oldChangeSet.get(c.Id) != c.Submitted_to_requestor__c)
				{
					Datetime now = DateTime.Now();

					string historyTemplate = '{{{Date}}}<br>{{{UATEvidence}}}<br>>>>>>>>>>>>>>>>>>>>>>>>><br>{{{UATEvidenceHistory}}}';

					historyTemplate = historyTemplate.replace('{{{Date}}}', now.format('yyyyMMdd'));
					historyTemplate = historyTemplate.replace('{{{UATEvidence}}}', c.UATEvidence__c);
					if(c.UATEvidence_History__c != null)
						historyTemplate = historyTemplate.replace('{{{UATEvidenceHistory}}}', c.UATEvidence_History__c);
					else
						historyTemplate = historyTemplate.replace('{{{UATEvidenceHistory}}}', '');

					
					c.UATEvidence_History__c =  historyTemplate;
					c.UATEvidence__c = null;
				}
			}
			
			
		}

		
    	
  	}
	
}