/**************************************************
* Author: Alberto de Diego from Fluido Oy
*
* Change Log:
* 2019-12-17 extbad: exclude Attunity Closed Cases  to external users IT-2329
* 2019-12-17 Vlad Nareyko: new validation IT-2321
* 2016-06-07 Andreas Nilsson: Class reactivated after winter 16, moved logic to helper class.
* 2015-04-09 Madhav Kakani: CR# 20197 - add topic to the related parent
* 2013-05-28 Madhav Kakani: Placed a check for empty map
* 2013-01-16 Madhav Kakani: Made changes to use Case object
* 2012-11-15 Alberto de Diego: CR# 4623
*        Initial Development
*
**************************************************/

trigger FeedItemArticleComment on FeedItem (after insert, before insert, before update) {
    if (Trigger.isBefore) {
        if (!ChatterPostsAccessValidator.isUserHaveAccess()) {
            ChatterPostsAccessValidator.setErrors(Trigger.new);
        }
    }
    if (Trigger.isAfter && Trigger.isInsert) {
        FeedItemArticleComment.HandleTrigger(ChatterPostsAccessValidator.excludeAttunityClosedCases(Trigger.newMap).values());
    }
}