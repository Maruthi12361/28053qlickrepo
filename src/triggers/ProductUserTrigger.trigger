/********************************************************
* CLASS: ProductUserTrigger
* DESCRIPTION: Handler class for Product User
*
* CHANGELOG:    
*   2019-11-28 - BAD - Added Initial logic
*********************************************************/
trigger ProductUserTrigger on Product_User__c (before insert) {


    if (Trigger.isInsert && Trigger.isBefore) {
        ProductUserHandler.OnBeforeInsert(Trigger.new);
    }

}