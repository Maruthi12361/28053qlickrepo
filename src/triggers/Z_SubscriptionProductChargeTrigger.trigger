/**     
 * File Name : Z_SubscriptionProductChargeTrigger
 * Description : This is a trigger to run handlers for DML operations against Zuora__SubscriptionProductCharge__c objects
 * @author : Inki Hong
 * Modification Log ======================================================================
 * Ver     Date         Author         Modification
 * 1       05.28.2020   Inki Hong      Created new trigger (RTR-602)
 */

trigger Z_SubscriptionProductChargeTrigger on Zuora__SubscriptionProductCharge__c (after insert, after update) {

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            Z_RenewalSPCTriggerHandler.handleAfterInsert(Trigger.newMap);
        } 
        
        if (Trigger.isUpdate) {
            Z_RenewalSPCTriggerHandler.handleAfterUpdate(Trigger.newMap);
        }
    }
}