/*
	BSL-1950 Custom Trigger created by Shubham Gupta to Call Z_QuoteChargeSummaryHandler
	DTR-479 DOB The prior quantity and the adjustment quantity are incorrect on salesforce quote page
*/

trigger Z_QuoteChargeSummaryTrigger on zqu__QuoteChargeSummary__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

        if (Trigger.isInsert) {
            if (Trigger.isBefore) {
                 if (!Semaphores.Z_QuoteSummaryTriggerBeforeInsert) {
                    Semaphores.Z_QuoteSummaryTriggerBeforeInsert = true;
                    Z_QuoteChargeSummaryHandler.updatePreviousQuantityFieldWithOriginalQuantity(Trigger.new); 
                }
            }

            if(Trigger.isAfter){
                if (!Semaphores.Z_QuoteSummaryTriggerAfterInsert) {
                    Semaphores.Z_QuoteSummaryTriggerAfterInsert = true;
                    
                   
                }
            }
        }

        if(Trigger.isUpdate){
            if (Trigger.isBefore) {
                if (!Semaphores.Z_QuoteSummaryTriggerBeforeUpdate) {
                    Semaphores.Z_QuoteSummaryTriggerBeforeUpdate = true;
                }
            }

            if(Trigger.isAfter){
                system.debug('@@@trigger ran');
                Z_QuoteChargeSummaryHandler.updateQSPAjustmentOnQuote(Trigger.new);
                if (!Semaphores.Z_QuoteSummaryTriggerAfterUpdate) {
                    Semaphores.Z_QuoteSummaryTriggerAfterUpdate = true;
                      
               }
            }
                
        }
}