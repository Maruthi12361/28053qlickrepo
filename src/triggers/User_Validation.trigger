/************************************************************************

	User_Validation
	
	This trigger will validate the data on the user object up on save. 
	The validations done here are validations that can't be done via 
	ordinary validation rules.  	
		
	Changelog:
		2011-03-08	MHG 	Created method
							CR #1473 - Update CPQ User mapping
							https://eu1.salesforce.com/a0CD000000CAvAW
		30.03.2020  ext_bjd ITRM-409 Added flag ENABLE_USER_TRIGGER to bypass trigger

*************************************************************************/
trigger User_Validation on User (before insert, before update) {
    if (CustomUserTriggerHandler.ENABLE_USER_TRIGGER) {
        Set<string> Countries = new Set<string>();
        for (User user : Trigger.New) {
            if (user.Country != '' && user.Country != null && !Countries.contains(user.Country.toLowerCase())) {
                Countries.Add(user.Country.toLowerCase());
            }
        }

        Set<string> ValidatedCountries = new Set<string>();
        if (Countries.size() > 0) {
            for (QlikTech_Company__c QT : [select Country_Name__c from QlikTech_Company__c where Country_Name__c in :Countries]) {
                if (QT.Country_Name__c != '' && QT.Country_Name__c != null && !ValidatedCountries.contains(QT.Country_Name__c.toLowerCase())) {
                    ValidatedCountries.Add(QT.Country_Name__c.toLowerCase());
                }
            }
        }

        for (integer i = 0; i < Trigger.New.size(); i++) {
            if (Trigger.New[i].ContactId == null && Trigger.New[i].Country != '' && Trigger.New[i].Country != null && !ValidatedCountries.Contains(Trigger.New[i].Country.toLowerCase())) {
                Trigger.New[i].Country.addError('Invalid country - the supplied country must exist in the QlikTech Company table');
            }
        }
    }
}