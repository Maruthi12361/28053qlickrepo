/******************************************************

	Trigger: UpdateNFROppTimeLimit
	
	Initiator: CR# 16147 New Opportunity Record Type for NFR requests 
	
	Changelog:
		2015-07-10	MTM		Created file	
		
******************************************************/
trigger UpdateNFROppTimeLimit on Account_License__c (after insert, after update, before update) 
{
    map<Id, Date> oppTimeLimitMap = new map<Id, Date>();
    List<Opportunity> oppToUpdate = new List<Opportunity>();
  //  List<Opportunity> allOpp = new List<Opportunity>();

    for(Account_License__c lic : Trigger.new) {       
		if(!oppTimeLimitMap.containsKey(lic.Opportunity__c) && lic.Time_Limit__c != null)
		{
            oppTimeLimitMap.put(lic.Opportunity__c, lic.Time_Limit__c);  
		}  
    }

  // allOpp = [select Id, Revenue_Type__c, Time_Limit__c from Opportunity where Id =:oppTimeLimitMap.keySet()];

   for(Opportunity opp : [select Id, Revenue_Type__c, Time_Limit__c from Opportunity where Id =:oppTimeLimitMap.keySet()])
   {
       if(opp.Revenue_Type__c == 'Reseller Not For Resale' ||
	      opp.Revenue_Type__c == 'OEM Not For Resale' )
       {
           opp.Time_Limit__c = oppTimeLimitMap.get(opp.Id);
           System.debug('opp.Time_Limit__c' + opp.Time_Limit__c);
           oppToUpdate.add(opp);
       }   
   }
   if(!oppToUpdate.isEmpty()) update oppToUpdate;
}