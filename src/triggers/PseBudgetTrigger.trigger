/**********************************************************
* Name:			PseBudgetTrigger
* Object:      	pse__Budget__c
* Author:		ext_vos
* Date:			2017-11-16
* Description: 	Handles all actions for Budget.
*               
* Changes Log:
*    Date     author    CR#    Description
*************************************************************/
trigger PseBudgetTrigger on pse__Budget__c (after insert, after update) {
	if (Trigger.isAfter) {
		if (Trigger.isInsert && !Semaphores.PseBudgetTriggerAfterInsert) {
			Semaphores.PseBudgetTriggerAfterInsert = true;
			PseBudgetTriggerHandler.handleAfterInsert(Trigger.newMap);
		} else if (Trigger.isUpdate && !Semaphores.PseBudgetTriggerAfterUpdate) {
			Semaphores.PseBudgetTriggerAfterUpdate = true;
			PseBudgetTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.newMap);
		}
	}
}