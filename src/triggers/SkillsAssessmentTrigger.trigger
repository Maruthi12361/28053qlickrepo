/**
*
* Change log:
*
*   2019-08-28 ext_bad  CHG0036677: Initial implementation.
*/
trigger SkillsAssessmentTrigger on Skills_Assessment__c (before insert) {

    if (Trigger.isBefore && Trigger.isInsert) {
        SkillsAssessmentHandler.onBeforeInsert(Trigger.new);
    }
}