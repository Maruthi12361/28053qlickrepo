/*
// 2016-09-20   tjg     Diabled existing handlers as they are not needed.
//                      Incoporated 2 handlers from CustomOppProductTrigger.trigger
// Rodion Vakulvsokyi 14.03.2017
2017-06-02 MTM Set QCW-1633 OEM Subscription deals
2017-06-22 Rodion Vakulvoskyi added static flag to get rid of soql query limit
2017-07-25 Rodion Vakulvoskyi QCW-2695
2017-09-15 UIN QCW-2494 Async Schedule creation changes. Changed signature of afterupdate in schedule creation handler
2017-09-29 QCW-877 Shubham Gupta Added fix by putting OppProductForecastAmountCalcHandler.onAfterUpdate outside semaphore
on line 52
2017-10-03 AIN Added call to OppProductPopulateProductGrpHandler for QCW-2862
2017-10-03 Added OppDealSplitScheduleHandler QCW-2849
2018-10-08 Linus Löfberg BSL-873
*/

trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    private static Map<ID, Opportunity> mIdDealSplitOpp = new Map<ID, Opportunity>();

    if (Trigger.isInsert) {
        if(Trigger.isBefore) {
            //OpportunityLockFromOppProductsHandler.handle(Trigger.new, Trigger.old, Trigger.isDelete);
            //UpdatePriceHandler.handle(Trigger.new, Trigger.isInsert);
        }
        if (Trigger.isAfter) {
            if (!Semaphores.OpportunityLineItemTriggerAfterInsert) {
                Semaphores.OpportunityLineItemTriggerAfterInsert = true;
                //efOLIInsertUpdateTriggerHandler.handle(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
                OpportunityScheduleCreationHandler.onAfterInsert(Trigger.new, Trigger.newMap);
                //removed by Rodion Vakulvsokyi 14.03.2017
                //OppProductForecastAmountCalcHandler.onAfterInsert(trigger.new, trigger.newMap);
                OppProductRollupMaker.onAfterInsert(Trigger.new);
                OppProductPopulateProductGrpHandler.onAfterInsert(trigger.new, trigger.newMap);
            }
        }
    }

    if (Trigger.isUpdate) {
        if(Trigger.isBefore) {
            //OpportunityLockFromOppProductsHandler.handle(Trigger.new, Trigger.old, Trigger.isDelete);
            //UpdatePriceHandler.handle(Trigger.new, Trigger.isInsert);
        }
        if (Trigger.isAfter) {
            if (!Semaphores.OpportunityLineItemTriggerAfterUpdate) {
                Semaphores.OpportunityLineItemTriggerAfterUpdate = true;
                //efOLIInsertUpdateTriggerHandler.handle(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
                //OpportunityScheduleCreationHandler.onAfterUpdate(Trigger.new, Trigger.newMap);//UIN changes for QCW-2494
                OpportunityScheduleCreationHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);//UIN changes for QCW-2494
                System.debug('After update');
                OppProductRollupMaker.onAfterUpdate(Trigger.new, Trigger.oldMap);
                OppProductPopulateProductGrpHandler.onAfterUpdate(trigger.new);
            }
            mIdDealSplitOpp = OppProductForecastAmountCalcHandler.onAfterUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            system.debug('Limits of jobs' + Limits.getQueueableJobs());
            system.debug('semapore SplitDealOpptyProduct '+ Semaphores.SplitDealOpptyProduct);
            system.debug('semapore NoTwiceRun '+ Semaphores.NoTwiceRun);
            if (!Semaphores.SplitDealOpptyProduct && !mIdDealSplitOpp.isEmpty() && !Semaphores.NoTwiceRun) {
                Semaphores.SplitDealOpptyProduct = true;
                system.debug('after asunc is called' + Semaphores.SplitDealOpptyProduct);
                OppDealSplitScheduleHandler.onAfterUpdate(mIdDealSplitOpp);
                mIdDealSplitOpp.clear();
            }
        }
    }

    if(Trigger.isDelete) {

        if (Trigger.isBefore) {
            //OpportunityLockFromOppProductsHandler.handle(Trigger.new, Trigger.old, Trigger.isDelete);
        }
        if (Trigger.isAfter) {
            OppProductForecastAmountCalcHandler.onAfterDelete(trigger.old, trigger.oldMap);
            OppProductRollupMaker.onAfterDelete(Trigger.old);
            OppProductPopulateProductGrpHandler.onAfterDelete(trigger.old, trigger.oldMap);
        }
    }

    if (Trigger.isUndelete) {

    }
}