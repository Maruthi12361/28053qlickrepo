/************************************************************************
*
*   CountCustomerSurveysForContact
*   
*   We need to set a checkbox on the Contact if the Contact has at least one Customer Survey in its Related List.
*   A way of doing this is to count the number of Customer Surveys in the Related list and then set the checkbox if the count is greater than zero
*   This trigger also updates the checkbox if a Customer Survey is deleted.
*       
*   Changelog:
*       2014-10-10  CCE     Initial development for CR# 13717 - Flag for Customer Survey - https://eu1.salesforce.com/a0CD000000j3vD6
*
*************************************************************************/
trigger CountCustomerSurveysForContact on Customer_Survey__c (after insert, after update, before delete) {
    
    Set<ID> ids = new Set<ID>();
    if(Trigger.isInsert) {
        ids = Trigger.newMap.keySet();
        System.debug('CountCustomerSurveysForContact: ids from newMap= ' + ids);
    } else {
        ids = Trigger.oldMap.keySet();
        System.debug('CountCustomerSurveysForContact: ids from oldMap = ' + ids);
    }
    System.debug('CountCustomerSurveysForContact: ids = ' + ids);

    //Get a list of all the Customer Surveys that are in the IDs list - we need this as our Ids could be coming from an after trigger
    List<Customer_Survey__c> cs = [SELECT Id, Contact__c FROM Customer_Survey__c WHERE Id in :ids]; 
    System.debug('CountCustomerSurveysForContact: cs.size() = ' + cs.size() + ' cs = ' + cs);
    
    //Build a list of all the Contact Ids that are mentioned in the Customer Surveys
    List<Id> ContactIds = new List<Id>();
    for (Customer_Survey__c c : cs) {
        ContactIds.add(c.Contact__c);
    }
    System.debug('CountCustomerSurveysForContact: ContactIds = ' + ContactIds);

    //Get a list of all the Customer Surveys that have a Contact Id that is in our list of Contacts - we will use this to build our frequency map (i.e. count the related list records)
    List<Customer_Survey__c> csCon = [SELECT Id, Contact__c FROM Customer_Survey__c WHERE Contact__c in :ContactIds];
    System.debug('CountCustomerSurveysForContact: csCon.size() = ' + csCon.size() + ' csCon = ' + csCon);
    
    //create a map which we will populate with the frequency of each Customer_Survey__c per Contact
    Map<Id, Integer> freq = new Map<Id, Integer>();
    for (Customer_Survey__c c : csCon) {
        Integer count = freq.get(c.Contact__c);
        System.debug('CountCustomerSurveysForContact: count = ' + count);          
        freq.put(c.Contact__c, (count == null) ? 1 : count+1);  
    }
    System.debug('CountCustomerSurveysForContact: freq = ' + freq);
    //if the trigger is a Delete then we need to subtract 1 from each Contact frequency count
    // - this is because we need to use the 'before delete' to do the counting (to get the Contact information)
    // and it hasn't actually been deleted at that point.
    if (Trigger.isDelete) {
        for (Id key : freq.keySet()) {
            Integer cnt = freq.get(key);
            cnt--;
            freq.put(key, cnt);
        }
        System.debug('CountCustomerSurveysForContact: freq (after a delete) = ' + freq);    
    }

    //Now we update the Contacts
    List<Contact> contactsToUpdate = new List<Contact>();
    List<Contact> conList = [SELECT Id, Customer_Survey_Count__c, Responded_to_Customer_Survey__c FROM Contact WHERE Id in :freq.keySet()];
    for (Contact co : conList) {
        co.Customer_Survey_Count__c = freq.get(co.Id);
        co.Responded_to_Customer_Survey__c = false;
        if (co.Customer_Survey_Count__c > 0) { co.Responded_to_Customer_Survey__c = true; }
        contactsToUpdate.add(co);
    }
    if (contactsToUpdate.size() > 0) { update contactsToUpdate; }   
}