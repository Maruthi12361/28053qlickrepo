/**************************************************************** 
* 
* Sales Channel  
* 
* Trigger to prevent hanging sales chanel with latest contract with no contract related list
* 
* 2017-06-19 SAN : QCW-2747
*
*****************************************************************/ 
trigger LatestContractUpdate on Sales_Channel__c (before update) {
    static Boolean flagForBeforeUpdate = false;
    
    if(!flagForBeforeUpdate){
        for (Sales_Channel__c a:trigger.new){
            if(a.Latest_Contract__c != null ){
                System.debug('Trigger LatestContractUpdate :');
                Map<Id, Contract> sc = new Map<Id, Contract>([select Id, Sales_Channel__c from Contract where Sales_Channel__c =:a.Id limit 1]);
    
                if (sc.isEmpty()){
                    a.Latest_Contract__c = null;
                 }
            }
        }
        flagForBeforeUpdate = true;
    }

}