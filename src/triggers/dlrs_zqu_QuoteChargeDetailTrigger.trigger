/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_zqu_QuoteChargeDetailTrigger on zqu__QuoteChargeDetail__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(zqu__QuoteChargeDetail__c.SObjectType);
}