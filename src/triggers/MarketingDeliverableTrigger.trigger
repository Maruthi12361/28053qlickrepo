/******************************************************

    Changelog:
        2017-05-25  CCE     CHG0031108 - Updates to Operations Request - Initial development
                            When a Markerting Deliverable is added to an Operations Request record of Marketo Support 
                            Requests record type we set a flag on the record (a workflow will then use this flag to send an email)
                        
******************************************************/
trigger MarketingDeliverableTrigger on Marketing_Deliverable__c (after insert) {

    // Get List of Object Ids to notify on
    Set<Id> objectIds = new Set<Id>();
    
    // Loop through list of Markerting Deliverables, and if Markerting Deliverable is associated to object we want to notify on, add Parent Id to objectIds set
    for(Marketing_Deliverable__c a:trigger.new){
         String keyPrefix = String.valueOf(a.Marketing_Request__c).substring(0, 3);
         
         if(keyPrefix == 'a3c'){  //a3c = Operations Request object prefix
            objectIds.add(a.Marketing_Request__c);
         }
    }
    
    // Get the objects we want to notify on, and set the Send Attachment Notification Email field to True 
    // This will to fire the workflow rule to send the email
    if(objectIds.size() > 0){
        List<Marketing_Request__c> mr = [SELECT Id, RecordTypeId FROM Marketing_Request__c WHERE Id IN :objectIds];
        
        for(Marketing_Request__c obj:mr){
            if (obj.RecordTypeId == '012D0000000KJMD') { //Marketo Support Requests record type
                obj.Send_Marketing_Deliverable_Email__c = true;
            }
        }
        
        if(mr.size() > 0){
            update mr;
        }       
    }     
}