trigger ReferralTrigger on Referral__c (before update) {
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            if(!Semaphores.ReferralTriggerHandlerBeforeUpdate){
                Semaphores.ReferralTriggerHandlerBeforeUpdate = true;
                ReferralTriggerController.validateTermsConditionsBeforeApproval(Trigger.New, Trigger.OldMap);
                ReferralTriggerController.validateOpportunityAssociationBeforeApproval(Trigger.New, Trigger.OldMap);
                ReferralTriggerController.validateRejectionReasonForRejectedReferral(Trigger.New, Trigger.OldMap);
                //OTQ-221 2020-03-11 Update PAM Fields in Opportunity
                ReferralTriggerController.updatePAMFieldsInOppty(Trigger.New, Trigger.OldMap); 
            }
        }
    }
}