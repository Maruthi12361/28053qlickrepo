/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Linus Löfberg
* @version        1.0
* @created        2019-10-17
* @modified       2019-10-17
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
*
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
trigger Z_SubscriptionRatePlanTrigger on Zuora__SubscriptionRatePlan__c (after insert, after update, before insert, before update) {
    Z_SubscriptionRatePlanTriggerHandler handler = new Z_SubscriptionRatePlanTriggerHandler(Trigger.isExecuting, Trigger.size);

    if (Trigger.isInsert && Trigger.isBefore) {
        handler.OnBeforeInsert(Trigger.new);
    } else if (Trigger.isInsert && Trigger.isAfter) {
        handler.OnAfterInsert(Trigger.new);
    }
    //  else if (Trigger.isUpdate && Trigger.isBefore) {
    //     handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    // } else if (Trigger.isUpdate && Trigger.isAfter) {
    //     handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    // }
}