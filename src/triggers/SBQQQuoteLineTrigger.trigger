/**
* Trigger: SBQQQuoteLineTrigger.
* @description Main trigger to handle different login on different events.
* 
* Changelog:
*	2016-11-17 : Roman Dovbush (4front) : Initial development.
*/


trigger SBQQQuoteLineTrigger on SBQQ__QuoteLine__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

	SBQQQuoteLineTriggerHandler handler = new SBQQQuoteLineTriggerHandler(Trigger.isExecuting, Trigger.size);
    try{
        if(Trigger.isInsert && Trigger.isBefore){
            handler.OnBeforeInsert(Trigger.new);
        }
        else if(Trigger.isInsert && Trigger.isAfter){
            handler.OnAfterInsert(Trigger.new);        
        }
        
        else if(Trigger.isUpdate && Trigger.isBefore){
            handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
        else if(Trigger.isUpdate && Trigger.isAfter){
            handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);        
        }
        
        else if(Trigger.isDelete && Trigger.isBefore){
            handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
        }
        else if(Trigger.isDelete && Trigger.isAfter){
            handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        }
        
        else if(Trigger.isUnDelete){
            handler.OnUndelete(Trigger.new);    
        }
    }catch(Exception e){
        system.debug('gggggg'+e.getStackTraceString() +e.getMessage());
    }
}