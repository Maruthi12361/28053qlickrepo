/*********************************************
LuminaryProfile_UpdateCountryName 
Description: Based on the Nominee country populate Country code and Company Name.
             
Log History:
2013-08-22    KMH    Initial Development
**********************************************/
trigger LuminaryProfile_UpdateCountryName on Luminary_Profile__c (Before Insert,Before update) {

    //Prepare Luminary Profile Nominee Country List 
    Set<String> countryNamelist = new Set<String>();
      
    for(Luminary_Profile__c LpObj : Trigger.new)
    {
        countryNamelist.add(LpObj.Nominee_Country__c);            
    }
    if(countryNamelist.size()>0)
    {
    	List<QlikTech_Company__c>  QTList = new List<QlikTech_Company__c> ([SELECT Id,Country_Name__c,Name,QlikTech_Company_Name__c FROM QlikTech_Company__c WHERE Country_Name__c IN:countryNameList]);
    	Map<String,QlikTech_Company__c>  QTMap = new Map<String,QlikTech_Company__c> ();
    	    	
    	for (QlikTech_Company__c mlist: QTList)
    	{
    	  	QTMap.put(mlist.Country_Name__c, mlist);    
        }
           	 
    	for(Luminary_Profile__c Lp : Trigger.New)
    	{
    		if( QTMap.containsKey(Lp.Nominee_Country__c) )
    		
    		{
    			QlikTech_Company__c qt = QTMap.get(Lp.Nominee_Country__c);    		  		
    			Lp.Country_Code__c =qt.Name;
    			Lp.QlikTech_Company__c =qt.QlikTech_Company_Name__c;
    			
    			//System.debug('Trigger com:'+Lp.Nominee_Country__c);
    			
    		}   		
    	}   	
    }

}