/***************************************************
Trigger: Assignment_Sharing 
Object: pse__Assignment__c 
Description: 
        Assignment record will be shared with a partner (subcontractor) 
        when partner(subcontractor) is the “Recourse” assigned to the Assignment

Change Log:
 2016-01-12 TJG CR# 42998 https://eu1.salesforce.com/a0CD000000uWSH5?srPos=0&srKp=a0C
            Partner Sharing project. Now update contact sharing when owner changes
******************************************************/
trigger Assignment_Sharing on pse__Assignment__c (after insert, after update) {

	List<Id> asmIds = new List<Id>();
	
	if (Trigger.isUpdate)
	{
		for (integer i = 0; i < Trigger.New.Size(); i++)
		{
			if (Trigger.New[i].pse__Resource__c != Trigger.Old[i].pse__Resource__c)
			{
				asmIds.Add(Trigger.New[i].Id);
			}
		}
		
	}
	
	if (Trigger.isInsert)
	{
		for (integer i = 0; i < Trigger.New.Size(); i++)
		{
			if (Trigger.New[i].pse__Resource__c != null)
			{
				asmIds.Add(Trigger.New[i].Id);
			}
		
		}
	}
	
	System.Debug('Assignment_Sharing.trigger: About to update ' + asmIds.size() + 'records');
	
	if (asmIds.size() > 0)
	{		
		if (!Test.isRunningTest())
			ApexSharingRules.UpdateAssignmentSharing(asmIds);
		else if (ApexSharingRules.TestingParnershare)
			ApexSharingRules.UpdateAssignmentSharingForTest(asmIds);
	}
}