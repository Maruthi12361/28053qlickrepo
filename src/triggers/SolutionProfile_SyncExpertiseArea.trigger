/******************************************************************************
*
* SolutionProfile_SyncExpertiseArea
*
* Trigger to update Expertise Area field on related Opportunity
* based on the changed value on Solution Profile.
* Needs a trigger as this is a cross-object synchronization.
*
* 2014-11-13 Fluido: Trigger created (CR# 17632)
*                    Tested by SolutionProfile_SyncExpertiseAreaTest Apex class
*
******************************************************************************/

trigger SolutionProfile_SyncExpertiseArea on Solution_Profiles__c (
    after insert, after update, after delete
) {
    final Integer SIZE = 60; // size of the Expertise_Area__c field on Opportunity

    // Opportunities to sync
    Set<Id> oppIds = new Set<Id>();

    if (Trigger.isAfter && Trigger.isInsert) {
        // After-insert processing
        for (Solution_Profiles__c sp : Trigger.new) {
            if (sp.Opportunity_Name__c != null && sp.Expertise_Area__c != null) {
                // Sync is needed for an Opportunity
                oppIds.add(sp.Opportunity_Name__c);
            }
        }
    } else if (Trigger.isAfter && Trigger.isUpdate) {
        // After-update processing
        for (Solution_Profiles__c sp : Trigger.new) {
            Solution_Profiles__c oldSp = Trigger.oldMap.get(sp.Id);
            if ((sp.Opportunity_Name__c != oldSp.Opportunity_Name__c) ||
                (sp.Expertise_Area__c != oldSp.Expertise_Area__c)
            ) {
                if (oldSp.Opportunity_Name__c != null) {
                    // Sync is needed for an Opportunity
                    oppIds.add(oldSp.Opportunity_Name__c);
                }
                if (sp.Opportunity_Name__c != null) {
                    // Sync is needed for an Opportunity
                    oppIds.add(sp.Opportunity_Name__c);
                }
            }
        }
    } else if (Trigger.isAfter && Trigger.isDelete) {
        // After-delete processing
        for (Solution_Profiles__c sp : Trigger.old) {
             if (sp.Opportunity_Name__c != null && sp.Expertise_Area__c != null) {
                // Sync is needed for an Opportunity
                oppIds.add(sp.Opportunity_Name__c);
            }
        }
    }

    // Update identified Opportunities; multiple expertise areas must be handled:
    List<Opportunity> opps = [
        SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id IN :oppIds
    ];
    // 1. Collect all current expertise areas on related Solution Profiles
    Map<Id, Set<String>> oppToEAs = new Map<Id, Set<String>>(); // <OppId, Set<ExpertiseArea>>
    for (Solution_Profiles__c sp : [
        SELECT Opportunity_Name__c, Expertise_Area__c FROM Solution_Profiles__c
        WHERE Opportunity_Name__c IN :oppIds AND Expertise_Area__c != null
    ]) {
        Set<String> eas = oppToEAs.get(sp.Opportunity_Name__c);
        if (eas == null) {
            // First expertise area to be synced on the Opportunity
            eas = new Set<String>();
        }
        eas.add(sp.Expertise_Area__c.trim());
        oppToEAs.put(sp.Opportunity_Name__c, eas);
    }
    // 2. Process all collected expertise areas per Opportunity
    for (Opportunity o : opps) {
        String result = '';
        Set<String> expAreas = oppToEAs.get(o.Id);
        if (expAreas != null) {
            // Update expertise areas info based on the field size
            List<String> expAreasList = new List<String>(expAreas);
            expAreasList.sort(); // let's sort first
            for (String ea : expAreasList) {
                result += ea + ','; // let's comma separate
            }
            result = result.substring( // let's trim
                0,
                (SIZE > result.length() ? result.length() : SIZE)-1
            );
        }
        o.Expertise_Area__c = result.length() > 0 ? result : null;
    }
    // 3.Perform the update of identified Opportunities
    if (opps.size() > 0) {
        update(opps);
    }
}