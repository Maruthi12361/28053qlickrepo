/************************************************************************

    PreventNewQuoteOnClosedOpps
    
    Trigger to stop creation of Quotes if Opportunity is Closed 
        
    Changelog:
        2011-10-05  MHG     Created trigger
                            CR# 2971 - Prevent new Quote if Opp is Closed
                            https://eu1.salesforce.com/a0CD000000EXBhN

*************************************************************************/
trigger PreventNewQuoteOnClosedOpps on Quote__c (before insert) {

    Set<string> OppIDs = new Set<string>();
    
    for (Quote__c Q : Trigger.new)
    {
        if (Q.Opportunity__c != null && !OppIDs.contains(Q.Opportunity__c))
        {
            OppIDs.add(Q.Opportunity__c);
        }       
    }
    
    for (Opportunity O : [select Id, StageName from Opportunity where Id in :OppIDs])
    {
        if (O.StageName == 'Closed Won' || O.StageName == 'Closed Lost')
        {
            for (Quote__c Q : Trigger.new)
            {
                Q.addError('You cannot add Quotes to a Closed Opportunity');
            }
        }
    }

}