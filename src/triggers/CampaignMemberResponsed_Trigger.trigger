/*****************************************************
Trigger: CampaignMemberResponsed_Trigger
Object: Contact
Description: CR# 19391 – Update Inquiry Start Date in the related Lead object when Responded=true
Description: CR# 20789 - Telemarketing Follow-Up Process – Update TM_Follow_Up_Required__c field to true
Description: CR# 32086 - Add Field to Campaign Member Table - Retreive and store lead/contact stus on campaign response.

Change Log:
20141213   Madhav Kakani Fluido Denmark
2015-08-04 CR# 32086 - Linus Löfberg @ 4front
2015-09-02 CCE CR# 56780 - Bug fix to take account of Converted leads 
2018-04-03 CCE CHG0033380 - Trigger to Mark "Deleted In Marketo" on Lead/Contact as FALSE on CampaignMember HasResponded = true
******************************************************/

trigger CampaignMemberResponsed_Trigger on CampaignMember (after insert, after update) {

    // CR #32086
    Set<Id> leadIds = new Set<Id>();
    Set<Id> ctIds = new Set<Id>();
    Map<Id, CampaignMember> cmsToUpdateMap = new Map<Id, CampaignMember>();
    Map<Id, CampaignMember> cmsMapResponded = new Map<Id, CampaignMember>();
    Map<Id, CampaignMember> clonedCMsMap = new Map<Id, CampaignMember>();
    List<CampaignMember> cms = [SELECT Id, LeadId, ContactId, Responded_Lead_Contact_Status__c, HasResponded, Lead.IsConverted FROM CampaignMember WHERE Id IN :Trigger.new];
    //List<CampaignMember> cms = [SELECT Id, LeadId, ContactId, Responded_Lead_Contact_Status__c, HasResponded FROM CampaignMember WHERE Id IN :Trigger.new];

    for(CampaignMember cm : cms) {
        System.debug('Value of HasResponded: ' + cm.HasResponded);
        if (Trigger.isUpdate) {
            CampaignMember oldCM = Trigger.oldMap.get(cm.Id);
            if ((cm.LeadId != null) && (cm.Lead.IsConverted == false)) {    //CCE CR# 56780
            //if (cm.LeadId != null) {
                leadIds.add(cm.LeadId);
                if (cm.HasResponded && !oldCM.HasResponded) {
                    cmsMapResponded.put(cm.LeadId, cm);
                }
                if (cm.HasResponded && !oldCM.HasResponded && String.isBlank(cm.Responded_Lead_Contact_Status__c)) {
                    cmsToUpdateMap.put(cm.LeadId, cm);
                }
            } else if (cm.ContactId != null) {
                ctIds.add(cm.ContactId);
                if (cm.HasResponded && !oldCM.HasResponded) {
                    cmsMapResponded.put(cm.ContactId, cm);
                }
                if (cm.HasResponded && !oldCM.HasResponded && String.isBlank(cm.Responded_Lead_Contact_Status__c)) {
                    cmsToUpdateMap.put(cm.ContactId, cm);
                }
            }
        } else if (Trigger.isInsert) {
            if ((cm.LeadId != null) && (cm.Lead.IsConverted == false)) {    //CCE CR# 56780
            //if (cm.LeadId != null) {
                leadIds.add(cm.LeadId);
                if (cm.HasResponded) {
                    cmsMapResponded.put(cm.LeadId, cm);
                }
                if (cm.HasResponded && String.isBlank(cm.Responded_Lead_Contact_Status__c)) {
                    cmsToUpdateMap.put(cm.LeadId, cm);
                }
            } else if(cm.ContactId != null) {
                ctIds.add(cm.ContactId);
                if (cm.HasResponded) {
                    cmsMapResponded.put(cm.ContactId, cm);
                }
                if (cm.HasResponded && String.isBlank(cm.Responded_Lead_Contact_Status__c)) {
                    cmsToUpdateMap.put(cm.ContactId, cm);
                }
            }
        }
    }

    System.debug('CampaignMembers to update: ' + cmsToUpdateMap);
    System.debug('CampaignMemberResponsed_Trigger cmsMapResponded: ' + cmsMapResponded);
    
    // Update the Inquiry Start Date timestamp only if it is not already set
    if(!leadIds.isEmpty()) {
        List<Lead> lstLeads = [SELECT Id, Inquiry_Start_Date__c, AQL_Start_Date__c, Status, Deleted_In_Marketo__c FROM Lead WHERE (Id IN :leadIds) OR (Id IN :cmsToUpdateMap.keySet()) OR (Id IN :cmsMapResponded.keySet())];
        System.debug('CampaignMemberResponsed_Trigger List of leads: ' + lstLeads);
        for(Integer i = 0; i < lstLeads.size(); i++) {
            if (leadIds.contains(lstLeads[i].Id) && lstLeads[i].Inquiry_Start_Date__c == null && lstLeads[i].AQL_Start_Date__c == null) {
                lstLeads[i].Inquiry_Start_Date__c = Date.today();
            }
            if (cmsMapResponded.containsKey(lstLeads[i].Id)) {
                lstLeads[i].Deleted_In_Marketo__c = false;
            }
            if (cmsToUpdateMap.containsKey(lstLeads[i].Id)) {
                CampaignMember clonedCM = cmsToUpdateMap.get(lstLeads[i].Id).clone(true, true, true, true);
                clonedCM.Responded_Lead_Contact_Status__c = lstLeads[i].Status;
                clonedCMsMap.put(cmsToUpdateMap.get(lstLeads[i].Id).Id, clonedCM);
            }
        }
        if(!lstLeads.isEmpty()) update lstLeads;
    }

    // Update the Inquiry Start Date timestamp only if it is not already set
    if(!ctIds.isEmpty()) {
        List<Contact> lstCts = [SELECT Id, Inquiry_Start_Date__c, AQL_Start_Date__c, MQL_Start_Date__c, SAL_Start_Date__c, SQL_Start_Date__c, Contact_Status__c, Deleted_In_Marketo__c FROM Contact WHERE (Id IN :ctIds) OR (Id IN :cmsToUpdateMap.keySet()) OR (Id IN :cmsMapResponded.keySet())];
        System.debug('CampaignMemberResponsed_Trigger List of Contacts: ' + lstCts);
        for(Integer i = 0; i < lstCts.size(); i++) {
            if (ctIds.contains(lstCts[i].Id) && lstCts[i].Inquiry_Start_Date__c == null && lstCts[i].AQL_Start_Date__c == null && lstCts[i].MQL_Start_Date__c == null && lstCts[i].SAL_Start_Date__c == null && lstCts[i].SQL_Start_Date__c == null) { 
                lstCts[i].Inquiry_Start_Date__c = Date.today();
            }            
            if (cmsMapResponded.containsKey(lstCts[i].Id)) {
                lstCts[i].Deleted_In_Marketo__c = false;
            }
            if (cmsToUpdateMap.containsKey(lstCts[i].Id)) {
                CampaignMember clonedCM = cmsToUpdateMap.get(lstCts[i].Id).clone(true, true, true, true);
                clonedCM.Responded_Lead_Contact_Status__c = lstCts[i].Contact_Status__c;
                clonedCMsMap.put(cmsToUpdateMap.get(lstCts[i].Id).Id, clonedCM);
            }
        }
        if(!lstCts.isEmpty()) update lstCts;
    }

    // Clone Campaign Members that have been updated with new Lead Contact Statuses.
    // The method of cloning is used because of HasResponded checkbox being set by a managed trigger and the status updates can not be handled in a before trigger.
    if (clonedCMsMap.size() > 0) {
        update clonedCMsMap.values();
    }
}

/*
//CR #19391
trigger CampaignMemberResponsed_Trigger on CampaignMember (after insert, after update) {
    Set<Id> leadIds = new Set<Id>();
    Set<Id> ctIds = new Set<Id>();

    for(CampaignMember cm : Trigger.new) {
        if( (cm.HasResponded == true) && (cm.LeadId != null) ) leadIds.add(cm.LeadId);
        if( (cm.HasResponded == true) && (cm.ContactId != null) ) ctIds.add(cm.ContactId);
    }
    
    // Update the Inquiry Start Date timestamp only if it is not already set
    if(!leadIds.isEmpty()) {
        List<Lead> lstLeads = [SELECT Id, Inquiry_Start_Date__c FROM Lead WHERE Id=:leadIds AND Inquiry_Start_Date__c=null AND AQL_Start_Date__c=null];
        for(Integer i = 0; i < lstLeads.size(); i++) {
            lstLeads[i].Inquiry_Start_Date__c = Date.today();
        }
        if(!lstLeads.isEmpty()) update lstLeads;
    }

    // Update the Inquiry Start Date timestamp only if it is not already set
    if(!ctIds.isEmpty()) {
        List<Contact> lstCts = [SELECT Id, Inquiry_Start_Date__c FROM Contact WHERE Id=:ctIds
        AND Inquiry_Start_Date__c=null AND AQL_Start_Date__c=null AND MQL_Start_Date__c=null AND SAL_Start_Date__c=null AND SQL_Start_Date__c=null];
        for(Integer i = 0; i < lstCts.size(); i++) {
            lstCts[i].Inquiry_Start_Date__c = Date.today();
        }
        if(!lstCts.isEmpty()) update lstCts;
    }
*/

/*
    // CR# 20789 - Telemarketing Follow-Up Process
    // STEP 1 – Update TM_Follow_Up_Required__c field to true  
    Set<Id> furleadIds = new Set<Id>(); // follow-up required lead ids
    Set<Id> furctIds = new Set<Id>(); // follow-up required contact ids

    for(CampaignMember cm : [SELECT Id, LeadId, ContactId FROM CampaignMember WHERE Campaign.Type='TM - Telemarketing' AND Campaign.StartDate < TODAY]) {
        if(cm.LeadId != null) furleadIds.add(cm.LeadId);
        if(cm.ContactId != null) furctIds.add(cm.ContactId);
    }

    // Lead: Update the TM_Follow_Up_Required__c field to true only if it is not already set
    if(!furleadIds.isEmpty()) {
        List<Lead> lstLeads = [SELECT Id, TM_Follow_Up_Required__c FROM Lead WHERE Id=:furleadIds AND TM_Follow_Up_Required__c = false];
        for(Integer i = 0; i < lstLeads.size(); i++) {
            lstLeads[i].TM_Follow_Up_Required__c = true;
        }
        if(!lstLeads.isEmpty()) update lstLeads;
    }

    // Contact: Update the TM_Follow_Up_Required__c field to true only if it is not already set
    if(!furctIds.isEmpty()) {
        List<Contact> lstCts = [SELECT Id, TM_Follow_Up_Required__c FROM Contact WHERE Id=:furctIds AND TM_Follow_Up_Required__c = false];
        for(Integer i = 0; i < lstCts.size(); i++) {
            lstCts[i].TM_Follow_Up_Required__c = true;
        }
        if(!lstCts.isEmpty()) update lstCts;
    }

    // STEP 2 – Update TM_Follow_Up_Required__c field to false
    for(CampaignMember cm : [SELECT Id, LeadId, ContactId FROM CampaignMember WHERE Status IN ('Reached - Not Now', 'Reached - Never', 'Reached - GI', 'Reached - Respond to Offer')]) {
        if(cm.LeadId != null) furleadIds.add(cm.LeadId);
        if(cm.ContactId != null) furctIds.add(cm.ContactId);
    }
*/