/************************************************************************
*
*   User_DeactivatedUpdateProfile
*   
*   This trigger will make sure the User account will populate the Last profile field
*   and set the Profile to Archive User whenever the user is deactivated 
*  
*   Changelog:
*       2012-10-23  SAN     Created method
*                           CR# 6208 - Archive User Process
*                           https://eu1.salesforce.com/a0CD000000NKAzu
*		2013-02-25	SAN		Added extra filter to make sure no deactivated Partner is archived
*		2014-03-30  MTM     CR 11399 Trigger and ULC change to handle @qlik.com email addresses
*	    30.03.2020  ext_bjd ITRM-409 Added flag ENABLE_USER_TRIGGER to bypass trigger
*************************************************************************/
trigger User_DeactivatedUpdateProfile on User (before update) {
    System.Debug('Starting User_DeactivatedUpdateProfile');
	if (CustomUserTriggerHandler.ENABLE_USER_TRIGGER) {
		for (Integer i = 0; i < trigger.new.size(); i++) {
			if (trigger.new[i].IsActive != trigger.old[i].IsActive && !trigger.new[i].IsActive && trigger.new[i].UserType == 'Standard') {
				String Email = trigger.new[i].Email;
				String strProfile = trigger.old[i].Profile__c;
				if ((Email.containsIgnoreCase('@qlikview.com') || Email.containsIgnoreCase('@qlik.com')) && !strProfile.contains('PRM'))// only do this for our own employee
				{
					Profile p = [SELECT Id FROM Profile WHERE Name = 'Archive User'];
					User user = trigger.new[i];
					user.Last_Profile__c = user.Profile__c;
					user.ProfileId = p.Id;
					System.Debug('User_DeactivatedUpdateProfile: ' + user.ProfileId);
				}
			}
		}
	}
    System.Debug('Finishing User_DeactivatedUpdateProfile');
}