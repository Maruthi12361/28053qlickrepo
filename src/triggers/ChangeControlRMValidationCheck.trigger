/********************************************************************************************
*	2014-06-25 	AIN 	Initial development
*						Updates the field RM_Validation_Check__c to reflect the custom setting QTRMCustomSet__c
*						Is used in validation rules to check if they should activate.
*						Currently used in check_development_info and RM_data_must_be_populated
**********************************************************************************************/
trigger ChangeControlRMValidationCheck on SLX__Change_Control__c (before insert, before update) 
{
	string recordTypeId;
	string recordTypeIdShort;
	string type;
	QTRMCustomSet__C customSettings = QTRMCustomSet__C.getOrgDefaults();
	string recordTypesToCheck = customSettings.CRRecordTypes__c;
	string excludedTypesToCheck = customSettings.CRExcludeTypes__c;

	boolean isIncludeRecordTypes;
	boolean isExcludeTypes;
	for(SLX__Change_Control__c change : trigger.new)
	{
		recordTypeId = change.RecordTypeId;
		recordTypeIdShort = recordTypeId.substring(0,15);
		type = change.Type__c != null ? change.Type__c : '';
		isIncludeRecordTypes = (recordTypesToCheck != null && recordTypesToCheck.indexOf(recordTypeIdShort) != -1);
		isExcludeTypes = (excludedTypesToCheck != null && excludedTypesToCheck.indexOf(type) != -1);

		change.RM_Validation_Check__c = (isIncludeRecordTypes && !isExcludeTypes);
	}
}