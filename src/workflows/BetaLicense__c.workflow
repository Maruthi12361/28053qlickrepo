<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_License_Email</fullName>
        <description>Send License Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactID__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ULC_Templates/Beta_License</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Beta_License</fullName>
        <field>Email_License__c</field>
        <literalValue>0</literalValue>
        <name>Update Beta License</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Send License</fullName>
        <actions>
            <name>Send_License_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Beta_License</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Email_License__c), Email_License__c = true, ContactID__c != null)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
