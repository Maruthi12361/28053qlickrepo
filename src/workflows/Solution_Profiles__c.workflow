<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Partner_Expertise_Manager_All_Solution_Profiles_Are_Approved</fullName>
        <ccEmails>QlikPartnerNetwork@qlik.com; dnn@qlik.com;ttg@qlik.com</ccEmails>
        <description>Alert Partner Expertise Manager All Solution Profiles Are Approved</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Expertise_Area/Solution_Profiles_Complete</template>
    </alerts>
    <fieldUpdates>
        <fullName>Solutio_Profile_Reject</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Solutio Profile Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Solution_Profile_Approve</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Solution Profile Approve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
