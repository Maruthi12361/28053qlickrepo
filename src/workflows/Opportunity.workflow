<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_new_opportunity_has_been_created_by_your_partner</fullName>
        <description>A new opportunity has been created by your partner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Partner_Opportunity_Registration_Request_Requires_Your_Attention_End_User_Owner</template>
    </alerts>
    <alerts>
        <fullName>Academic_Program_Survey_Alert</fullName>
        <ccEmails>academicprogram@qlik.com</ccEmails>
        <description>Academic Program Survey Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Academic_Contact_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>academicprogram@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Academic_Program/Academic_Program_Survey_Email</template>
    </alerts>
    <alerts>
        <fullName>Academic_Program_Survey_Reminder_Alert</fullName>
        <ccEmails>academicprogram@qlik.com</ccEmails>
        <description>Academic Program Survey Reminder Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Academic_Contact_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>academicprogram@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Academic_Program/Academic_Program_Survey_Reminder_Email</template>
    </alerts>
    <alerts>
        <fullName>Academic_Program_Survey_Request</fullName>
        <ccEmails>academicprogram@qlik.com</ccEmails>
        <description>Academic Program Survey Request</description>
        <protected>false</protected>
        <recipients>
            <field>Academic_Contact_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>academicprogram@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Academic_Program/Academic_Program_Survey_Email</template>
    </alerts>
    <alerts>
        <fullName>Academic_Program_Welcome_Email_Student</fullName>
        <ccEmails>emailtosalesforce@1anatwijat3ybco7jgomrr5zsnc8onai46f4fql5q1dfv6g6ti.2-igpxea4.eu1.le.salesforce.com</ccEmails>
        <ccEmails>DL-AcademicProgramNoReply@qlik.com</ccEmails>
        <description>Academic Program Welcome Email (Student)</description>
        <protected>false</protected>
        <recipients>
            <field>Academic_Contact_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>academicprogram@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Academic_Program/Student_Welcome_Email_QS_Desktop</template>
    </alerts>
    <alerts>
        <fullName>Alert_New_and_Previous_Opportunity_owner_when_Opportunity_owner_has_changed</fullName>
        <description>Alert New and Previous Opportunity owner when Opportunity owner has changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Previous_owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_Opportunity_Owner_changed_New_opportunity_owner</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Closed_Won_Opportunity</fullName>
        <description>Attunity Closed Won Opportunity</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>eae@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>edn@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>edw@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eeb@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eeu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eex@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ejv@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>enh@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>enw@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eog@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tun@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yen@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Closed_Won_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Gold_Client_Closed_Won</fullName>
        <description>Attunity Gold Client - Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>eef@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ens@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ezm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fbh@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Gold_Client_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_Close_Date_Change</fullName>
        <description>Attunity Opportunity Close Date Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Active_PSM__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Opportunity_Close_Date_Change</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_Close_Date_Past_Due</fullName>
        <description>Attunity Opportunity Close Date Past Due</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Opportunity_Close_Date_Past_Due</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_Close_Date_in_7_Days</fullName>
        <description>Attunity Opportunity Close Date in 7 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Opportunity_Close_Date_in_7_Days</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_Goal_Identified_AMERICAS</fullName>
        <description>Attunity Opportunity Goal Identified AMERICAS</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>eeb@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Opportunity_Stage_Change</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_Goal_Identified_EMEA</fullName>
        <description>Attunity Opportunity Goal Identified EMEA</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>fah@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Opportunity_Stage_Change</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_Stage_Change</fullName>
        <description>Attunity Opportunity Stage Change</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Active_PSM__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Opportunity_Stage_Change</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_Stage_Change_Probability_50</fullName>
        <description>Attunity Opportunity Stage Change Probability 50+</description>
        <protected>false</protected>
        <recipients>
            <recipient>ink@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Opportunity_Stage_Change</template>
    </alerts>
    <alerts>
        <fullName>Attunity_Opportunity_alert</fullName>
        <ccEmails>70caea0d.qliktech.com@amer.teams.ms</ccEmails>
        <description>Attunity Opportunity alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>cpu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Admin_Internal_Emails/Attunity_Opp_alert</template>
    </alerts>
    <alerts>
        <fullName>Create_SoE_XL_file_trigger_email</fullName>
        <ccEmails>downloads2@qliktech.com</ccEmails>
        <description>Create SoE XL file trigger email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>CCS_System_Templates/CCS_Trigger_for_SoE_to_XL</template>
    </alerts>
    <alerts>
        <fullName>Discount_Approval_rejected</fullName>
        <description>Discount Approval rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Discount_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Discount_Approval_required</fullName>
        <description>Discount Approval required</description>
        <protected>false</protected>
        <recipients>
            <recipient>byj@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Discount_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Discount_Approved</fullName>
        <description>Discount Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Opportunity_Final_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Discount_Approved_Les</fullName>
        <description>Discount Approved Les</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Opportunity_Final_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Pearson_for_certification_request</fullName>
        <ccEmails>pearsonvuevoucherstore@pearson.com</ccEmails>
        <description>Email Pearson for certification request</description>
        <protected>false</protected>
        <recipients>
            <recipient>boy@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>dl-qlikcredits@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Signature_Opportunities/Signature_Opps_Certification_Request</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_asking_if_a_Technology_Partner_is_involved</fullName>
        <ccEmails>DL-GlobalTechPartner@qlik.com</ccEmails>
        <description>Email notification asking if a Technology Partner is involved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>dl-globaltechpartner@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Global_Tech_Partner/Tech_Partners_for_DI_opportunities</template>
    </alerts>
    <alerts>
        <fullName>FirstremindertoOpportunityOwnerApplication</fullName>
        <description>First reminder to Opportunity Owner: Application</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Opportunity_Remind_Account_Profiling</template>
    </alerts>
    <alerts>
        <fullName>New_Subscription_Opportunity_Created</fullName>
        <ccEmails>63ed5b15.qliktech.com@amer.teams.ms</ccEmails>
        <description>New Subscription Opportunity Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>cpu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmb@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vmr@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Subscription_Opportunity_2</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Academic_Program_Account_incorrect_Opp</fullName>
        <ccEmails>academicprogram@qlik.com</ccEmails>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <ccEmails>smm@qlik.com</ccEmails>
        <description>Notification of Academic Program Account incorrect Opp</description>
        <protected>false</protected>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Admin_Internal_Emails/Academic_Program_account_invalid_Opp</template>
    </alerts>
    <alerts>
        <fullName>Opp_Email_to_Opp_Owner_when_Nav_Status_changed</fullName>
        <description>Opp: Email to Opp Owner when Nav Status changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Quote_To_Order/Opp_Change_of_Status</template>
    </alerts>
    <alerts>
        <fullName>Opp_Email_to_Opp_Owner_when_Opp_is_rejected</fullName>
        <description>Opp: Email to Opp Owner when Opp is rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Quote_To_Order/Opportunity_Rejected_From_Navision</template>
    </alerts>
    <alerts>
        <fullName>Opp_End_Webservice_Error_message</fullName>
        <ccEmails>sysdev@qlikview.com</ccEmails>
        <description>Opp: End Webservice Error message</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Admin_Internal_Emails/SYS_Webservice_Error_mail</template>
    </alerts>
    <alerts>
        <fullName>Opp_Nav_trigger_email_for_OEM_accounts</fullName>
        <ccEmails>downloads2@qliktech.com</ccEmails>
        <description>Opp: Nav trigger email for OEM accounts</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Account_Orbis_send_OEM_to_Navision</template>
    </alerts>
    <alerts>
        <fullName>Opp_Notify_Opp_owner_of_Spec_License_terms_approved</fullName>
        <description>Opp: Notify Opp owner of Spec License terms approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Opp_Notify_Opp_owner_of_Spec_License_terms_approved</template>
    </alerts>
    <alerts>
        <fullName>Opp_Notify_legal_of_Spec_License_terms</fullName>
        <ccEmails>americas.legal@qlik.com</ccEmails>
        <description>Opp: Notify legal of Spec License terms</description>
        <protected>false</protected>
        <recipients>
            <recipient>aok@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kol@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kos@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rgr@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Opp_Notify_legal_of_Spec_License_terms</template>
    </alerts>
    <alerts>
        <fullName>Opp_POC_change_America</fullName>
        <description>Opp POC change America</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>eeu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ejr@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eof@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Attunity_PoC_Daily_Recap_notification</template>
    </alerts>
    <alerts>
        <fullName>Opp_POC_change_non_America</fullName>
        <description>Opp POC change non-America</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>eeu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ezw@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>irc@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Attunity_PoC_Daily_Recap_notification</template>
    </alerts>
    <alerts>
        <fullName>OpportunityApproved</fullName>
        <description>Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Opportunity_Final_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>OpportunityRejected</fullName>
        <description>Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Discount_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Email_Alert_to_Owner_for_Finance_Rejected_Stage</fullName>
        <description>Opportunity: Email Alert to Owner for Finance Rejected Stage</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Opportunity_Finance_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Fulfilment_margin_notification</fullName>
        <description>Opportunity Fulfilment margin notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_fulfillment</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Reg_Approval_Notification</fullName>
        <description>Opportunity Reg Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Your_Qlik_Opportunity_Registration_is_Approved</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Reg_Rejection_Notification</fullName>
        <description>Opportunity Reg Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Your_Qlik_Opportunity_Registration_is_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_is_being_reviewed</fullName>
        <description>Opportunity is being reviewed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Your_Opportunity_Registration_is_Pending_Review</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_with_QDI_products_Closed_Won</fullName>
        <description>Opportunity with QDI products Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>QDI_services</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Signature_Opportunities/Opp_with_QDI_products_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>OpportunityisApproved</fullName>
        <description>Opportunity is Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Opportunity_Final_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>PRMOpptyNotifyCCU</fullName>
        <ccEmails>djs@qlikview.com</ccEmails>
        <description>PRM: Oppty: Notify CCU</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/Partner_Deal_Closed_Won_Lost</template>
    </alerts>
    <alerts>
        <fullName>PRMOpptyNotifyPSU</fullName>
        <description>PRM: Oppty: Notify PSU</description>
        <protected>false</protected>
        <recipients>
            <recipient>psu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/Partner_Deal_Closed_Won_Lost</template>
    </alerts>
    <alerts>
        <fullName>Partner_Opportunity_Registration_AutoApproved</fullName>
        <description>Partner Opportunity Registration AutoApproved</description>
        <protected>false</protected>
        <recipients>
            <field>Active_PSM__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Partner_Opportunity_Registration_AutoApproved</template>
    </alerts>
    <alerts>
        <fullName>QCC_Onsite</fullName>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <description>QCC Onsite</description>
        <protected>false</protected>
        <recipients>
            <field>Send_to_DL_QT_Region__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Admin_Internal_Emails/QCC_Onsite_Bundle_Notification</template>
    </alerts>
    <alerts>
        <fullName>QCC_Onsite_Purchased</fullName>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <description>QCC Onsite Purchased</description>
        <protected>false</protected>
        <recipients>
            <field>Send_to_DL_QT_Region__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Admin_Internal_Emails/QCC_Onsite_Bundle_Notification</template>
    </alerts>
    <alerts>
        <fullName>Quote_Status_is_Approved</fullName>
        <description>Quote Status is Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Opp_Approved_Quote</template>
    </alerts>
    <alerts>
        <fullName>SecondRemindertoOpportunityOwnerApplication</fullName>
        <description>Second Reminder to Opportunity Owner: Application</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Opportunity_Second_Reminder_Application_Profile</template>
    </alerts>
    <alerts>
        <fullName>Send_Quote_Email_Alert</fullName>
        <description>Send Quote Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Correspondence_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Qlikbuy_Templates/Qlikbuy_Send_Quote</template>
    </alerts>
    <alerts>
        <fullName>Send_notification_to_PSM_when_opp_is_set_to_closed_won</fullName>
        <description>Send notification to PSM when opp is set to closed won</description>
        <protected>false</protected>
        <recipients>
            <field>Active_PSM__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Order_Placed_Active_PSM</template>
    </alerts>
    <alerts>
        <fullName>Signature_Opp_Internal_Notification</fullName>
        <ccEmails>Data@qlik.com</ccEmails>
        <ccEmails>DL-QlikCredits@qlik.com</ccEmails>
        <description>Signature Opp Internal Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>aip@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cre@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cri@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dal@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dtc@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dvl@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fkt@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fmg@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhb@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jnw@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ngi@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Signature_Opportunities/Signature_Opps_Notification</template>
    </alerts>
    <alerts>
        <fullName>Training_Card_creating</fullName>
        <description>Training Card creating</description>
        <protected>false</protected>
        <recipients>
            <recipient>smm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Voucher_Templates/Training_Card_creating</template>
    </alerts>
    <alerts>
        <fullName>Training_Card_not_created</fullName>
        <description>Training Card not created</description>
        <protected>false</protected>
        <recipients>
            <recipient>smm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Internal_Emails/Training_Card_Missing</template>
    </alerts>
    <alerts>
        <fullName>Voucher_Issue_Notification</fullName>
        <ccEmails>smm@qlikview.com</ccEmails>
        <description>Voucher Issue Notification</description>
        <protected>false</protected>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Voucher_Templates/Voucher_Voucher_Issue_Notification</template>
    </alerts>
    <alerts>
        <fullName>Voucher_Send_voucher_details</fullName>
        <description>Voucher:  Send voucher details</description>
        <protected>false</protected>
        <recipients>
            <field>Correspondence_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Voucher_Templates/Voucher_Voucher_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Voucher_Send_voucher_details_INTERNAL</fullName>
        <description>Voucher: Send voucher details INTERNAL</description>
        <protected>false</protected>
        <recipients>
            <field>Send_to_DL_QT_Region__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Voucher_Templates/Voucher_Voucher_Confirmation_Internal</template>
    </alerts>
    <fieldUpdates>
        <fullName>Calculate_Deal_Split_maint_Value</fullName>
        <field>Deal_Split_Maint_Value__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Maintenance_Value__c * Deal_Split_Percentage__c</formula>
        <name>Calculate Deal Split maint Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Is_services_Opportunity</fullName>
        <description>Check pse_is_services_opportunity when is_services_opportunity__c is checked</description>
        <field>pse__Is_Services_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Check Is services Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Qlik_Sales_in_Control</fullName>
        <description>Check the Qlik Sales in Control checkbox when the workflow criteria is met.</description>
        <field>Qlik_Sales_In_Control__c</field>
        <literalValue>1</literalValue>
        <name>Check Qlik Sales in Control</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Registered_and_Protected_Opp</fullName>
        <description>CR# 39855</description>
        <field>Registered_and_Protected_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Check Registered and Protected Opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_GI_Acceptance_Start_Date</fullName>
        <description>Clearing this field will also clear the GI Acceptance Duration field.</description>
        <field>GI_Acceptance_Start_Date__c</field>
        <name>Clear GI Acceptance Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_GI_Acceptance_Status</fullName>
        <field>GI_Acceptance_Status__c</field>
        <name>Clear GI Acceptance Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_GI_Acceptance_Stop_Date</fullName>
        <field>GI_Acceptance_Stop_Date__c</field>
        <name>Clear GI Acceptance Stop Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_GI_Disqualified_Reason</fullName>
        <field>GI_Disqualified_Reason__c</field>
        <name>Clear GI Disqualified Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_GI_Qualification_Start_Date</fullName>
        <description>Clearing this field will also clear the GI Qualification Duration field.</description>
        <field>GI_Qualification_Start_Date__c</field>
        <name>Clear GI Qualification Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_GI_Qualification_Stop_Date</fullName>
        <field>GI_Qualification_Stop_Date__c</field>
        <name>Clear GI Qualification Stop Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_GI_Rejected_Reason</fullName>
        <field>GI_Rejected_Reason__c</field>
        <name>Clear GI Rejected Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Nurture_on_Closed_Won</fullName>
        <description>CR 19851 https://eu1.salesforce.com/a0CD000000nRm3a
When a deal is Closed Won make sure the &apos;Nurture&apos; field is automatically un-checked.
&apos;Nurtured&apos; opportunities do not show up in Sales 360.</description>
        <field>Pre_GI_Stage__c</field>
        <literalValue>0</literalValue>
        <name>Clear Nurture on Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date</fullName>
        <field>CloseDate</field>
        <formula>Today() + 0</formula>
        <name>Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Opportunity_Currency</fullName>
        <description>Sets the Xactly currency to the opportunity currency code</description>
        <field>Xactly_Currency__c</field>
        <formula>TEXT( CurrencyIsoCode )</formula>
        <name>Copy Opportunity Currency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Correspondence_Email_Populate</fullName>
        <description>The field will be populated using the Accepted by Email Address, otherwise License Recepient Email Address, otherwise Account Billing Contact Email address</description>
        <field>Correspondence_Email_Address__c</field>
        <name>Correspondence Email Populate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_SoE_XL_field_reset</fullName>
        <description>Reset to False</description>
        <field>Create_SoE_Excel_File__c</field>
        <literalValue>0</literalValue>
        <name>Create SoE XL field reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ECUSTOMS__RPS_ACC_Trigger</fullName>
        <field>ECUSTOMS__Screening_Trigger__c</field>
        <literalValue>1</literalValue>
        <name>RPS_ACC_Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GI_Acceptance_Start_Date</fullName>
        <description>Used to update the &quot;GI Acceptance Start Date&quot; field with the Date that the &quot;GI Acceptance Status&quot; picklist has changed.</description>
        <field>GI_Acceptance_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>GI Acceptance Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GI_Acceptance_Stop_Date</fullName>
        <field>GI_Acceptance_Stop_Date__c</field>
        <formula>TODAY()</formula>
        <name>GI Acceptance Stop Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GI_Qualification_Start_Date</fullName>
        <description>Used to update the &quot;GI Qualification Start Date&quot; field with the Date that the &quot;GI Acceptance Status&quot; picklist has changed.</description>
        <field>GI_Qualification_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>GI Qualification Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GI_Qualification_Stop_Date</fullName>
        <field>GI_Qualification_Stop_Date__c</field>
        <formula>TODAY()</formula>
        <name>GI Qualification Stop Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Game_Changer_Approved_checked</fullName>
        <field>Game_Changer_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Game Changer Approved checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Game_Changer_Approved_unchecked</fullName>
        <field>Game_Changer_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Game Changer Approved unchecked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Acc_2Key_Play_date</fullName>
        <field>Access_to_Key_Players_Negotiated_Date__c</field>
        <formula>today()</formula>
        <name>Ideal Pipeline Date - Acc 2Key Play date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Champ_Letter_Conf</fullName>
        <field>Champion_Letter_Confi_w_Champion_Date__c</field>
        <formula>today()</formula>
        <name>Ideal Pipeline Date - Champ Letter Conf</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_CostBen_Ana_Date</fullName>
        <field>Cost_Benefit_analysis_Presented_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - CostBen Ana. Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_DM_Agreed_SoE_Date</fullName>
        <field>DM_Agreed_to_Sequence_of_Events_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - DM Agreed SoE Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Dec_Maker_Iden_Date</fullName>
        <description>Ideal Pipeline Date  Decision Makers Identify Date</description>
        <field>Decision_makers_s_Identified_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date  Dec Maker Iden Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Goal_Documented</fullName>
        <field>Goal_Documented_Date__c</field>
        <formula>today()</formula>
        <name>Ideal Pipeline Date - Goal Documented</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Impl_Plan_Date</fullName>
        <field>Implementation_Plan_Discussed_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - Impl Plan Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_L_T_P_W_C_Date</fullName>
        <field>L_T_P_Process_discussed_docd_W_C_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - L/T/P W/C Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Neg_WS_compl_Date</fullName>
        <field>Negotiation_worksheet_completed_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - Neg WS compl Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Opp_SucdocAgr_Date</fullName>
        <field>Opp_Success_documented_agreed_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - Opp SucdocAgr Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Pre_Dec_RevCl_Date</fullName>
        <field>Pre_Decision_Review_close_att_w_DM_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - Pre-Dec RevCl Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_ProofAc_KeyPL_Date</fullName>
        <field>Proof_Accepted_by_Key_Players_SIB_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - ProofAc KeyPL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Prop_Deliv_DM_Date</fullName>
        <field>Proposal_delivered_to_DM_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - Prop Deliv DM Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Sol_De_key_pl_date</fullName>
        <field>Solutions_Developed_Key_Players_Date__c</field>
        <formula>today()</formula>
        <name>Ideal Pipeline Date - Sol De key pl date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Solution_Dev_w_Pot</fullName>
        <field>Solution_developed_w_Potent_Champ_date__c</field>
        <formula>today()</formula>
        <name>Ideal Pipeline Date - Solution Dev w Pot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ideal_Pipeline_Date_Verbal_appr_DMDate</fullName>
        <field>Verbal_approval_from_DM_Date__c</field>
        <formula>Today()</formula>
        <name>Ideal Pipeline Date - Verbal appr DMDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Locked_in_Approval_to_FALSE</fullName>
        <description>Update &quot;Is Locked in Approval&quot; field to FALSE</description>
        <field>Is_Locked_in_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Is Locked in Approval to FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Locked_in_Approval_to_TRUE</fullName>
        <description>Set Field to TRUE</description>
        <field>Is_Locked_in_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Is Locked in Approval to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legal_Approval_Triggered_True</fullName>
        <description>Sets Legal Approval Triggered to True on all NFR deals</description>
        <field>Legal_Approval_Triggered__c</field>
        <literalValue>1</literalValue>
        <name>Legal Approval Triggered True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Maint_percent_none</fullName>
        <description>Set default maintenance percent to none</description>
        <field>Default_Maintance_Percentage__c</field>
        <name>Maint percent none</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Maint_term_months_null</fullName>
        <description>Set maintenance term months to null</description>
        <field>Maintenance_Term_months__c</field>
        <name>Maint term months null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Maintenance_type_none</fullName>
        <description>Set maintenance type to none</description>
        <field>Maintenance_Type__c</field>
        <name>Maintenance type none</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_INT_Update_Closed_Lost</fullName>
        <description>CR 18963: set Closed Lost integration flag</description>
        <field>INT_Update_Closed_Lost__c</field>
        <literalValue>1</literalValue>
        <name>NS: INT Update - Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_Set_Finance_Approved</fullName>
        <field>Finance_Approved__c</field>
        <literalValue>1</literalValue>
        <name>NS: Set Finance Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_Update_Opp_to_Closed_NFR</fullName>
        <field>StageName</field>
        <literalValue>Closed NFR</literalValue>
        <name>NS Update Opp to Closed NFR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_Update_Opp_to_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>NS Update Opp to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_Update_Opp_to_OEM_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>OEM - Closed Won</literalValue>
        <name>NS Update Opp to OEM Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_Update_Opp_to_OEM_Prepay_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>OEM Pre-Pay Closed Won</literalValue>
        <name>NS Update Opp to OEM Prepay Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_Update_SLT_PO_Approved_Field_Changed</fullName>
        <description>NS Field Update</description>
        <field>SLT_PO_Approved_field_Changed__c</field>
        <literalValue>1</literalValue>
        <name>NS: Update SLT/PO Approved Field Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Step_standard_field_update</fullName>
        <description>Updates standard field Next step with the value from teh custom field Next_Step_custom__C</description>
        <field>NextStep</field>
        <formula>LEFT(Next_Step_custom__c, 255)</formula>
        <name>Next Step standard field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_a_High_Touch</fullName>
        <field>Deal_Desk_Support__c</field>
        <name>Not a High Touch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_Mint_information</fullName>
        <description>sets the value in the mint information field on the opportunity</description>
        <field>Mint_information__c</field>
        <formula>&quot;Account more rating:&quot; &amp; BR() 
&amp; 
Account.More_Rating__c &amp; BR() 
&amp; 
&quot; Account Default Probability:&quot; &amp; BR() 
&amp; 
text(Account.Default_probability__c) &amp; BR() 
&amp; 
&quot; Account Confidence Level&quot; &amp; BR() 
&amp; 
text (Account.Confidence_Level__c)</formula>
        <name>OPP:Mint information</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OppSetNavisionStatustoPendingSend</fullName>
        <field>Send_to_Navision__c</field>
        <literalValue>1</literalValue>
        <name>Opp: Set Navision Status to Pending Send</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OppSetOppAppdtoFALSEwhennotwon</fullName>
        <description>Set Opportunity Approved to FALSE if stage not Closed won and was previously set to TRUE</description>
        <field>Opportunity_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Opp: Set Opp Appd to FALSE when not won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OppSetOpportunityName</fullName>
        <description>Set Opportunity Name to first 60 characters of Acc Name + Short Description</description>
        <field>Name</field>
        <formula>LEFT(Account.Name , 44) &amp; &quot; - &quot; &amp; Short_Description__c &amp;&quot; - &quot;&amp; OppNumber__c</formula>
        <name>Opp: Set Opportunity Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OppSettoApprovedifwithinownlimit</fullName>
        <description>Set Opportunity to Approved if within own limits</description>
        <field>Opportunity_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Opp: Set to Approved if within own limit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Populate_Maint_value_number_field</fullName>
        <description>populates the Maint value number field which is used by the grand total field, we use this as the Maintenance value field can&apos;t be referenced by the grand total field directly as the compiled code is too big</description>
        <field>Maint_value_Number__c</field>
        <formula>Maintenance_Value__c</formula>
        <name>Opp: Populate Maint value number field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Pre_GI_Stage_checked_as_default</fullName>
        <description>Checked as default when the BDR EMEA group creates an opportunity from both lead conversion and contact/ account opp creation</description>
        <field>Pre_GI_Stage__c</field>
        <literalValue>1</literalValue>
        <name>Opp: Pre GI Stage checked as default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Create_Eval_to_Null_SHARED</fullName>
        <field>Create_Eval_Licenses__c</field>
        <literalValue>0</literalValue>
        <name>Opp: Set Create Eval to Null - SHARED</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Eval_Expiry_Date_to_Null</fullName>
        <field>Eval_Expiry_Date__c</field>
        <name>Opp: Set Eval Expiry Date to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Eval_Sent_Date_to_Null_SHARED</fullName>
        <field>Eval_Expiry_Date__c</field>
        <name>Opp: Set Eval Sent Date to Null - SHARED</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Fin_Approved_to_null</fullName>
        <field>Finance_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Opp: Set Fin Approved to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Fin_Comments_to_null</fullName>
        <field>Finance_Comments__c</field>
        <name>Opp: Set Fin Comments to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Mint_Info_in_mint_Info_at_CA</fullName>
        <description>Sets the value in the mint Info at credit approved field to the value in the mint information field. Consequently locks the mint information field by adding a value to the mint information at finance approved field</description>
        <field>Mint_info_on_CA__c</field>
        <formula>Mint_information__c</formula>
        <name>Opp: Set Mint Info in mint Info at CA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Nav_status_to_null</fullName>
        <field>Navision_Status__c</field>
        <name>Opp:Set Nav status to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_PO_Number_to_null</fullName>
        <field>Customer_PO_Number__c</field>
        <name>Opp: Set PO Number to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Partner_Bonus_field</fullName>
        <field>Partner_Bonus__c</field>
        <literalValue>1</literalValue>
        <name>Opp: Set Partner Bonus field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Sent_to_Nav_to_null</fullName>
        <field>Sent_To_Navision__c</field>
        <name>Opp: Set Sent to Nav to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_record_type_to_academic</fullName>
        <description>Sets the record type to academic record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Academic_Program_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp: Set record type to academic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Consult_Forecast_Amount_DSC</fullName>
        <field>Consultancy_Forecast_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Consultancy_Forecast_Amount__c *  Deal_Split_Percentage__c</formula>
        <name>Opp: Update Consult Forecast Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Educatio_Forecast_Amount_DSC</fullName>
        <field>Education_Forecast_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Education_Forecast_Amount__c *  Deal_Split_Percentage__c</formula>
        <name>Opp: Update Educatio Forecast Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Licence_Amount_DSC</fullName>
        <description>Update license amount based on parent opportunity</description>
        <field>Amount</field>
        <formula>Deal_Split_Parent_Opportunity__r.Amount * Deal_Split_Percentage__c</formula>
        <name>Opp:Update Licence Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_License_Forecast_Amount_DSC</fullName>
        <field>License_Forecast_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.License_Forecast_Amount__c *  Deal_Split_Percentage__c</formula>
        <name>Opp: Update License Forecast Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Misc_Forecast_Amount_DSC</fullName>
        <field>Misc_Forecast_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Misc_Forecast_Amount__c *  Deal_Split_Percentage__c</formula>
        <name>Opp: Update Misc Forecast Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Requirements_Met_to_false</fullName>
        <field>Requirements_met__c</field>
        <literalValue>0</literalValue>
        <name>Opp: Update Requirements Met to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Support_Forecast_Amount_DSC</fullName>
        <field>Support_Forecast_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Support_Forecast_Amount__c *  Deal_Split_Percentage__c</formula>
        <name>Opp: Update Support Forecast Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Total_Maintenance_Amount_DSC</fullName>
        <field>Total_Maintenance_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Total_Maintenance_Amount__c * Deal_Split_Percentage__c</formula>
        <name>Opp: Update Total Maintenance Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_requirements_met</fullName>
        <description>This is a temporary workaround - Using this field to trigger a time based workflow to force re-update of Deal Split Child opps.  This is needed to make the Opportunity_managerForecastProducts trigger run.</description>
        <field>Requirements_met__c</field>
        <literalValue>1</literalValue>
        <name>Opp: Update Requirements Met to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OpportunitySettoApproved</fullName>
        <field>Opportunity_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Set to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Partner_Bonus_field</fullName>
        <field>Partner_Bonus__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity - Update Partner Bonus field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Partner_Bonus</fullName>
        <field>Partner_Bonus__c</field>
        <literalValue>0</literalValue>
        <name>PRM Partner Bonus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_Margin_check</fullName>
        <field>PRM_Margin_Approval__c</field>
        <literalValue>1</literalValue>
        <name>PRM Referral Margin check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Voucher_Send_to_DL_QT_Region</fullName>
        <description>Populate the field &apos;Send to DL QT Region&apos;</description>
        <field>Send_to_DL_QT_Region__c</field>
        <formula>/* Update when adding a new Region in Qliktech Company */ 
/* CR 8112 - https://eu1.salesforce.com/a0CD000000YHQMT */ 
/*CR# 70768 - TTG *Edited AMERICAS to N AMERICA test*/ 
/* CHG0030655 - completely new Mappings */ 
CASE( 
Account.Billing_Country_Code__r.QlikTech_Region__c, 

&apos;AF&apos;, &apos;DL-TrainingNotificationAF@qlikview.com&apos;, 
&apos;ANZ&apos;, &apos;DL-TrainingNotificationANZ@qlikview.com&apos;, 
&apos;ASEAN&apos;,&apos;DL-TrainingNotificationASEAN@qlikview.com&apos;, 
&apos;BENELUX&apos;, &apos;DL-TrainingNotificationBENELUX@qlikview.com&apos;, 
&apos;CANADA&apos;, &apos;DL-TrainingNotificationCANADA@qlikview.com&apos;, 
&apos;CHINA&apos;, &apos;DL-TrainingNotificationCHINA@qlikview.com&apos;, 
&apos;DACH&apos;, &apos;DL-TrainingNotificationDACH@qlikview.com&apos;, 
&apos;EE&apos;, &apos;DL-TrainingNotificationEE@qlikview.com&apos;, 
&apos;EEMR&apos;, &apos;DL-TrainingNotificationEEMR@qlikview.com&apos;, 
&apos;Finland&apos;, &apos;DL-TrainingNotificationFINLAND@qlikview.com&apos;, 
&apos;FRANCE&apos;, &apos;DL-TrainingNotificationFRANCE@qlikview.com&apos;, 
&apos;HK/TW&apos;, &apos;DL-TrainingNotificationHKTW@qlikview.com&apos;, 
&apos;IBERIA&apos;, &apos;DL-TrainingNotificationIBERIA@qlikview.com&apos;, 
&apos;INDIA&apos;, &apos;DL-TrainingNotificationINDIA@qlikview.com&apos;, 
&apos;ITALY&apos;, &apos;DL-TrainingNotificationITALY@qlikview.com&apos;, 
&apos;JAPAN&apos;, &apos;DL-TrainingNotificationJAPAN@qlikview.com&apos;, 
&apos;KOREA&apos;, &apos;DL-TrainingNotificationKOREA@qlikview.com&apos;, 
&apos;LATAM&apos;, &apos;DL-TrainingNotificationLATAM@qlikview.com&apos;, 
&apos;ME&apos;, &apos;DL-TrainingNotificationME@qlikview.com&apos;, 
&apos;RUSSIA&apos;, &apos;DL-TrainingNotificationRUSSIA@qlikview.com&apos;, 
&apos;SAD&apos;, &apos;DL-TrainingNotificationSAD@qlikview.com&apos;, 
&apos;SAF&apos;, &apos;DL-TrainingNotificationSAF@qlikview.com&apos;, 
&apos;Scandinavia&apos;, &apos;DL-TrainingNotificationSCANDINAVIA@qlikview.com&apos;, 
&apos;UK&apos;, &apos;DL-TrainingNotificationUK@qlikview.com&apos;, 
&apos;USA&apos;, &apos;DL-TrainingNotificationUSA@qlikview.com&apos;, 
&apos;MENA&apos;, &apos;DL-TrainingNotificationMENA@qlikview.com&apos;, 
&apos;MEA&apos;, &apos;DL-TrainingNotificationMEA@qlikview.com&apos;, 
&apos;SPI&apos;, CASE( 
Account.Billing_Country_Code__r.QlikTech_Sub_Region__c,
&apos;Italy&apos;, &apos;DL-TrainingNotificationITALY@qlikview.com&apos;,
&apos;DL-TrainingNotificationIBERIA@qlikview.com&apos;), 
&apos;No_QT_Region_In_Qliktech_Company@qlikview.com&apos; 

)</formula>
        <name>Populate Voucher Send to DL QT Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QCC_Email_Sent</fullName>
        <description>Update checkbox to checked when email sent to Academic Contact on Academic Program Opportunity</description>
        <field>QCC_Access_Granted__c</field>
        <literalValue>1</literalValue>
        <name>QCC Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Segment_on_creation</fullName>
        <field>Segment_on_creation__c</field>
        <formula>TEXT(Account.Segment_New__c )</formula>
        <name>Segment on creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetOpportunitytoApproved</fullName>
        <field>Opportunity_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Opportunity to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Acc_Owner</fullName>
        <field>Acc_Owner_at_Closed_Won__c</field>
        <formula>Account.OwnerId</formula>
        <name>Set Acc Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Acc_OwnerName</fullName>
        <description>QCW-4215</description>
        <field>Acc_Owner_Name__c</field>
        <formula>Account.Owner.FirstName + &apos; &apos; + Account.Owner.LastName</formula>
        <name>Set Acc Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_BDR_Manager_Grade_date</fullName>
        <description>Timestamp for changes to the BDR Manager Grade field.</description>
        <field>BDR_Manager_grade_date__c</field>
        <formula>NOW()</formula>
        <name>Set BDR Manager Grade date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_GI_Acceptance_Status_to_Accepted</fullName>
        <field>GI_Acceptance_Status__c</field>
        <literalValue>GI Accepted</literalValue>
        <name>Set GI Acceptance Status to Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_GI_Acceptance_Status_to_Rejected</fullName>
        <field>GI_Acceptance_Status__c</field>
        <literalValue>GI Rejected</literalValue>
        <name>Set GI Acceptance Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Goal_Discovery_Type_to_New</fullName>
        <description>Set Goal Discovery Type to New</description>
        <field>Goal_Discovery_Type__c</field>
        <literalValue>New</literalValue>
        <name>Set Goal Discovery Type to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Goal_Discovery_Type_to_Recycled</fullName>
        <description>Set Goal Discovery Type to Recycled</description>
        <field>Goal_Discovery_Type__c</field>
        <literalValue>Recycled</literalValue>
        <name>Set Goal Discovery Type to Recycled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_High_Touch</fullName>
        <field>Deal_Desk_Support__c</field>
        <literalValue>High Touch</literalValue>
        <name>Set High Touch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Main_calc_List</fullName>
        <description>when the Revenue Type field is set to Subscription, the Maintence Calculation field is set to List</description>
        <field>Maintenance_Calculation__c</field>
        <literalValue>List</literalValue>
        <name>Set Main calc = List</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Named_Acc_at_Closed_Won</fullName>
        <field>Named_Acc_At_Closed_Won__c</field>
        <formula>if( 
 Account.Named_Account__c = True, 
&quot;Yes&quot;, 
&quot;No&quot; 
)</formula>
        <name>Set Named Acc at Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Navision_Status_to_Resent</fullName>
        <field>Navision_Status__c</field>
        <literalValue>Resent</literalValue>
        <name>Set Navision Status to Resent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_to_Academic_Closed_Won_when_Clos</fullName>
        <field>StageName</field>
        <literalValue>AP - Closed Won</literalValue>
        <name>Set Opp to Academic Closed Won when Clos</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_to_QSG_Closed_Won_when_Closed_Da</fullName>
        <field>StageName</field>
        <literalValue>QSG Closed Won</literalValue>
        <name>Set Opp to QSG Closed Won when Closed Da</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Qualifying_SI</fullName>
        <field>Qualifying_SI__c</field>
        <formula>If ( 
Referring_Contact__r.Account.Qualifying_SI__c = True, 
&quot;Yes&quot;, 
&quot;No&quot; 
)</formula>
        <name>Set Qualifying SI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sell_Through_PAMName</fullName>
        <description>QCW-4215</description>
        <field>Sell_Through_PSM_QM_at_Closed_Won__c</field>
        <formula>Sell_Through_Partner__r.Owner.FirstName +&apos; &apos;+ Sell_Through_Partner__r.Owner.LastName</formula>
        <name>Set Sell Through PAM name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sell_Through_PAM_Id</fullName>
        <description>QCW-4215</description>
        <field>Sell_Through_PSM_QM_at_Closed_Won_ID__c</field>
        <formula>Sell_Through_Partner__r.OwnerId</formula>
        <name>Set Sell Through PAM Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Send_Email_Trigger_to_True</fullName>
        <description>Set Send Email trigger to true when the Navision Status is Finance Processed or Posted, The field finance approved is true and the Send Quote Email Triggered is null (does not have a date)</description>
        <field>Send_Quote_Email_Trigger__c</field>
        <literalValue>1</literalValue>
        <name>Set Send Email Trigger to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Send_Quote_Email_Triggered_Datetime</fullName>
        <description>Set the datetime when the quote email was sent</description>
        <field>Send_Quote_Email_Triggered__c</field>
        <formula>NOW()</formula>
        <name>Set Send Quote Email Triggered Datetime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Spec_Pricing_Agrmnt_Terms_in_Place</fullName>
        <field>Special_Pricing_Agreement_Terms_in_Place__c</field>
        <formula>if( 
AND( 
Account.Special_Pricing_Account__c = True, 
Account.Special_Pricing_End_Date__c &gt; TODAY() 
), 
&quot;Yes&quot;, 
&quot;No&quot; 
)</formula>
        <name>Set Spec Pricing Agrmnt Terms in Place</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_To_Goal_Rejected</fullName>
        <field>StageName</field>
        <literalValue>Goal Rejected</literalValue>
        <name>Set Stage To Goal Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Territory_Credit_At_Closed_Won</fullName>
        <description>Update &apos;Territory Credit At Closed Won&apos; from &apos;Territory Credit&apos; ( Market_Type__c )</description>
        <field>Territory_Credit_At_Closed_Won__c</field>
        <formula>Market_Type__c</formula>
        <name>Set Territory Credit At Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Voucher_Status_to_Issue_From_Pending</fullName>
        <description>Set Voucher Status from pending to Issue, if there is no movement after an hour</description>
        <field>VoucherStatus__c</field>
        <literalValue>Vouchers Issue</literalValue>
        <name>Set Voucher Status to Issue From Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sets_stage_to_Goal_Confirmed</fullName>
        <field>StageName</field>
        <literalValue>Goal Confirmed</literalValue>
        <name>Sets stage to Goal Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sets_stage_to_Goal_Discovery</fullName>
        <field>StageName</field>
        <literalValue>Goal Discovery</literalValue>
        <name>Sets stage to Goal Discovery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Set_to_Registration_Pending_Approv</fullName>
        <description>Update Stage to &quot;Registration Pending Approval&quot;</description>
        <field>StageName</field>
        <literalValue>Registration Pending Approval</literalValue>
        <name>Stage Set to Registration Pending Approv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Bypass_Rules_Checkbox</fullName>
        <description>Unchecks bypass rules checkbox after triggers has run.</description>
        <field>Bypass_Rules__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Bypass Rules Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Is_Services_Opp</fullName>
        <description>Uncheck pse_is_services_opportunity</description>
        <field>pse__Is_Services_Opportunity__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Is Services Opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date_when_Opp_is_Closed_Won</fullName>
        <description>When Opportunity is Closed Won the Close Date should be set to current date same as Closed Date filed.</description>
        <field>CloseDate</field>
        <formula>now()</formula>
        <name>Update Close Date when Opp is Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ClosedDate</fullName>
        <field>Closed_date_for_conga__c</field>
        <formula>NOW()</formula>
        <name>Update ClosedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Copy_Email_Field</fullName>
        <field>Copy_of_Email_Field__c</field>
        <formula>Email__c</formula>
        <name>Update Copy Email Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Deal_Split_Child_Training_Amount</fullName>
        <field>Deal_Split_Child_Training_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Total_Training_Amount__c *  Deal_Split_Percentage__c</formula>
        <name>Update Deal Split Child Training Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Rejected</fullName>
        <description>Update an Opportunity that has been rejected by Qlik to &quot;Registration Rejected&quot;</description>
        <field>StageName</field>
        <literalValue>Registration Rejected</literalValue>
        <name>Update Stage: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subscription_Forecast_ACV_amount</fullName>
        <field>Subscription_Forecast_ACV_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Subscription_Forecast_ACV_Amount__c  *  Deal_Split_Percentage__c</formula>
        <name>Update Subscription Forecast ACV amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subscription_Forecast_Amount_DSC</fullName>
        <field>Subscription_Forecast_Amount__c</field>
        <formula>Deal_Split_Parent_Opportunity__r.Subscription_Forecast_Amount__c  *  Deal_Split_Percentage__c</formula>
        <name>Update Subscription Forecast Amount DSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_Role</fullName>
        <field>Original_Created_by_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Update original created by Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_function</fullName>
        <field>Original_Created_by_Function__c</field>
        <formula>IF ( INCLUDES ( $User.Function__c, &quot;Sales&quot; ), &quot;Sales; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Marketing&quot; ), &quot;Marketing; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Alliance&quot; ), &quot;Alliance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;BDR&quot; ), &quot;BDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iBDR&quot; ), &quot;iBDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ISR&quot; ), &quot;ISR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;EIS&quot; ), &quot;EIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;MIS&quot; ), &quot;MIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SMBIS&quot; ), &quot;SMBIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;PRM&quot; ), &quot;PRM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Support&quot; ), &quot;Support; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Training&quot; ), &quot;Training; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Finance&quot; ), &quot;Finance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;HR&quot; ), &quot;HR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;IT&quot; ), &quot;IT; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;R&amp;D&quot; ), &quot;R&amp;D; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iPM&quot; ), &quot;iPM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SDR - MR&quot; ), &quot;SDR - MR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;N3&quot; ), &quot;N3; &quot;,null )&amp;
IF ( INCLUDES ( $User.Function__c, &quot;NIS&quot; ), &quot;NIS; &quot;,null )&amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ServiceSource&quot; ), &quot;ServiceSource; &quot;,null )</formula>
        <name>Update original created by function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Voucher_Update_Voucher_Sent_Date</fullName>
        <field>Voucher_Mail_Triggered__c</field>
        <formula>NOW()</formula>
        <name>Voucher: Update Voucher Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>populate_QDIPartnerCheck_field_on_oppty</fullName>
        <field>QDIPartnerCheck__c</field>
        <formula>IF( OR(TEXT(Revenue_Type__c) = &apos;Reseller&apos;, TEXT(Revenue_Type__c) = &apos;Fulfillment&apos;, TEXT(Revenue_Type__c) = &apos;Distributor&apos;), IF( ISBLANK( TEXT( Sell_Through_Partner__r.QDIPartnerApproved__c ) ) , &apos;value not selected&apos;, TEXT(Sell_Through_Partner__r.QDIPartnerApproved__c)) , &apos;non partner deal&apos;)</formula>
        <name>populate QDIPartnerCheck field on oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Margin_Expiry_Date</fullName>
        <field>Margin_Expiry_Date__c</field>
        <formula>CreatedDate + 210</formula>
        <name>update Margin Expiry Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Opp_stage_to_Goal_Identified</fullName>
        <field>StageName</field>
        <literalValue>Goal Identified</literalValue>
        <name>update Opp stage to Goal Identified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Partner_Opp_Reg_Approval_Date</fullName>
        <field>Partner_Opp_Reg_Approval_Date__c</field>
        <formula>CreatedDate</formula>
        <name>update Partner Opp Reg Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_maint_value_for_deal_split_child</fullName>
        <field>Amount</field>
        <formula>Deal_Split_Parent_Opportunity__r.Amount * Deal_Split_Percentage__c</formula>
        <name>update maint value for deal split child</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Academic Program Survey Complete</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>AP - Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Academic Program Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Short_Description__c</field>
            <operation>equals</operation>
            <value>QS and QCC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>greaterOrEqual</operation>
            <value>6/1/2017</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Academic_Program_Survey_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>182</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Academic_Program_Survey_Reminder_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>212</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Academic Program Welcome Email %28Student%29</fullName>
        <actions>
            <name>Academic_Program_Welcome_Email_Student</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>QCC_Email_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>AP - Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Academic Program Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Primary_Job_Function_of_contact__c</field>
            <operation>equals</operation>
            <value>Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.QCC_Access_Granted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Academic Program account invalid Opp</fullName>
        <actions>
            <name>Notification_of_Academic_Program_Account_incorrect_Opp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>notContain</operation>
            <value>Bilge Kara,Bobby Sacco,Sarah Miller,Jennifer Moore,Jennifer Danckaert,Lucy Bannister,Pankaj Muthe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type_As_Text__c</field>
            <operation>equals</operation>
            <value>Academic Program Account</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Alert Acc OwnerAlert when Partner creates a new opp</fullName>
        <actions>
            <name>A_new_opportunity_has_been_created_by_your_partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 5 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Owner_is_Partner__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Open_Opportunities__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Pending_Validation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Qlik MSP Opportunity</value>
        </criteriaItems>
        <description>Alert Acc Owner and Qonnect Manager when Partner user creates a new opp
CR# 11369</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Alert PSM that Partner%27s End User Account has placed an Order</fullName>
        <actions>
            <name>Send_notification_to_PSM_when_opp_is_set_to_closed_won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Active_PSM__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email alert that will go to PSM&apos;s when their partner accounts have an end user that has placed an order.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>App Profile%3A Opp%3A Remind Opportunity Owner to capture Application details</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Direct / Reseller - Std Sales Process,PRM - Reseller Std Sales Process</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>This workflow reminds Opportunity Owner to capture application information after a deal has been Closed/Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FirstremindertoOpportunityOwnerApplication</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>CaptureapplicationdetailsforClosedWonOpportunity</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SecondRemindertoOpportunityOwnerApplication</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>ReminderCaptureapplicationdetailsforopportunity</name>
                <type>Task</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Attunity Closed Won Opportunity</fullName>
        <actions>
            <name>Attunity_Closed_Won_Opportunity</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Product_Category__c</field>
            <operation>equals</operation>
            <value>Data Integration,Cross Product</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>ER - Closed Won,OEM - Closed Won,OEM Pre-Pay Closed Won,Closed Won,QSG Closed Won,AP - Closed Won</value>
        </criteriaItems>
        <description>Email for Opp Closed Won which include Data Integration products

BSL-2792</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Gold Client Closed Won</fullName>
        <actions>
            <name>Attunity_Gold_Client_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>Attunity Gold Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>ER - Closed Won,OEM - Closed Won,OEM Pre-Pay Closed Won,Closed Won,QSG Closed Won,AP - Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>Qlik Gold Client</value>
        </criteriaItems>
        <description>Email for Opp Closed Won for Gold Client

BSL-2793</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Opportunity Close Date Change</fullName>
        <actions>
            <name>Attunity_Opportunity_Close_Date_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email alert to the Partner Manager and the Owner of the record when the Close Date is updated

BSL-2794</description>
        <formula>ISCHANGED( CloseDate ) &amp;&amp;   OR( ISPICKVAL( Product_Category__c , &quot;Data Integration&quot;), ISPICKVAL( Product_Category__c , &quot;Cross Product&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Opportunity Close Date Past Due Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>ER - Closed Won,ER - Closed Lost,OEM - Closed Won,OEM - Closed Lost,Deal Split-closed Won,Closed Won,Closed Lost,AP - Closed Won,AP - Closed Lost,Closed NFR,Registration Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Category__c</field>
            <operation>equals</operation>
            <value>Data Integration,Cross Product</value>
        </criteriaItems>
        <description>This rule sends an email notification when the opportunity close date is about to pass or has already passed

BSL-2790</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Attunity_Opportunity_Close_Date_Past_Due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Attunity_Opportunity_Close_Date_in_7_Days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Attunity Opportunity Goal Identified AMERICAS</fullName>
        <actions>
            <name>Attunity_Opportunity_Goal_Identified_AMERICAS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>BSL-2795</description>
        <formula>AND(   ISCHANGED( StageName ),   OR(     ISPICKVAL( StageName, &apos;Goal Identified&apos; ),     ISPICKVAL( StageName, &apos;Goal Confirmed&apos; ),     ISPICKVAL( StageName, &apos;Goal Rejected&apos; )   ),   OR(     ISPICKVAL( Product_Category__c, &apos;Data Integration&apos; ),      ISPICKVAL( Product_Category__c, &apos;Cross Product&apos; )   ),    OR(      CONTAINS( QlikTech_Company__c, &apos;Inc&apos; ),      CONTAINS( QlikTech_Company__c, &apos;Corporation&apos; ),      CONTAINS( QlikTech_Company__c, &apos;Brazil&apos; ),      CONTAINS( QlikTech_Company__c, &apos;LATAM&apos; ),      CONTAINS( QlikTech_Company__c, &apos;Mexico&apos; )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Opportunity Goal Identified EMEA</fullName>
        <actions>
            <name>Attunity_Opportunity_Goal_Identified_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>BSL-2795</description>
        <formula>AND(   ISCHANGED( StageName ),   OR(     ISPICKVAL( StageName, &apos;Goal Identified&apos; ),     ISPICKVAL( StageName, &apos;Goal Confirmed&apos; ),     ISPICKVAL( StageName, &apos;Goal Rejected&apos; )   ),   OR(     ISPICKVAL( Product_Category__c, &apos;Data Integration&apos; ),      ISPICKVAL( Product_Category__c, &apos;Cross Product&apos; )   ),    OR(      NOT(        CONTAINS( QlikTech_Company__c, &apos;Inc&apos; )      ),      NOT(        CONTAINS( QlikTech_Company__c, &apos;Corporation&apos; )      ),      NOT(        CONTAINS( QlikTech_Company__c, &apos;Brazil&apos; )      ),      NOT(        CONTAINS( QlikTech_Company__c, &apos;LATAM&apos; )      ),      NOT(        CONTAINS( QlikTech_Company__c, &apos;Mexico&apos; )      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Opportunity Stage Change</fullName>
        <actions>
            <name>Attunity_Opportunity_Stage_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>BSL-2801</description>
        <formula>AND(   ISCHANGED( StageName ),   OR(     ISPICKVAL( Product_Category__c, &apos;Data Integration&apos;),     ISPICKVAL( Product_Category__c, &apos;Cross Product&apos;)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Opportunity Stage Change Probability 50%2B</fullName>
        <actions>
            <name>Attunity_Opportunity_Stage_Change_Probability_50</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>50</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Category__c</field>
            <operation>equals</operation>
            <value>Data Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Category__c</field>
            <operation>equals</operation>
            <value>Cross Product</value>
        </criteriaItems>
        <description>BSL-2796</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Opportunity alert</fullName>
        <actions>
            <name>Attunity_Opportunity_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email notification when an Opportunity has potential for Attunity products.</description>
        <formula>OR((ISPICKVAL(Product_Category__c ,&quot;Data Integration&quot;)),
(INCLUDES ( Included_Products__c , &quot;Attunity Replicate&quot;)),
(INCLUDES ( Included_Products__c , &quot;Attunity Compose&quot;)),
(INCLUDES ( Included_Products__c , &quot;Attunity Enterprise Manager (AEM)&quot;)),
(INCLUDES ( Included_Products__c , &quot;Attunity Gold Client&quot;)),
(INCLUDES ( Included_Products__c , &quot;Attunity Visibility&quot;)),
(INCLUDES ( Included_Products__c , &quot;Attunity RepliWeb&quot;)),
(INCLUDES ( Included_Products__c , &quot;Attunity Connect&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto Legal Approval on NFR deals</fullName>
        <actions>
            <name>Legal_Approval_Triggered_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Not For Resale</value>
        </criteriaItems>
        <description>Auto Legal Approval to bypass eCustoms for all NFR deals</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CCS - Create SoE XL file - trigger</fullName>
        <actions>
            <name>Create_SoE_XL_file_trigger_email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Create_SoE_XL_field_reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Create_SoE_Excel_File__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Creates the email that triggers orbis to send the SoEs</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Various GI Fields</fullName>
        <actions>
            <name>Clear_GI_Acceptance_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Acceptance_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Acceptance_Stop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Disqualified_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Qualification_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Qualification_Stop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Rejected_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If opp stage is updated to GD clear the 
GI Acceptance Status, GI Rejected Reason, GI Disqualified Reason, GI Acceptance Duration, GI Acceptance Start/Stop Date, GI Qualification Duration, GI Qualification Start/Stop Date fields.</description>
        <formula>AND  (      ISCHANGED(StageName),     ISPICKVAL(StageName, &quot;Goal Discovery&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ClosedDateUpdated</fullName>
        <actions>
            <name>Update_ClosedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(StageName) &amp;&amp; NOT( ISBLANK(Deal_Split_Parent_Opportunity__c )) &amp;&amp; CONTAINS(TEXT(StageName), &apos;Won&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Currency change</fullName>
        <actions>
            <name>Copy_Opportunity_Currency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is triggered when the opportunity currency is changed</description>
        <formula>ISCHANGED( CurrencyIsoCode )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ECUSTOMS__OppProbability</fullName>
        <actions>
            <name>ECUSTOMS__RPS_ACC_Trigger</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Account.ECUSTOMS__RPS_Status__c  = &apos;&apos; ,Account.ECUSTOMS__RPS_Status__c  = &apos;Pending&apos;) , Probability &gt; 0.40)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GI Acceptance Start Date</fullName>
        <actions>
            <name>Clear_GI_Acceptance_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Acceptance_Stop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Disqualified_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Qualification_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Qualification_Stop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_GI_Rejected_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GI_Acceptance_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to update the &quot;GI Acceptance Start Date&quot; field with the current date. Start when Opp Stage reaches Goal Identified.</description>
        <formula>AND (      ISCHANGED(StageName),     ISPICKVAL(StageName, &apos;Goal Identified&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GI Acceptance Stop Date</fullName>
        <actions>
            <name>GI_Acceptance_Stop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to update the &quot;GI Acceptance Stop Date&quot; field with the current date. Stop when GI Acceptance Status is GI Accepted or GI Rejected or GI Disqualified.</description>
        <formula>AND (      ISCHANGED(GI_Acceptance_Status__c),     OR      (          ISPICKVAL(GI_Acceptance_Status__c, &apos;GI Accepted&apos;),          ISPICKVAL(GI_Acceptance_Status__c, &apos;GI Rejected&apos;),          ISPICKVAL(GI_Acceptance_Status__c, &apos;GI Disqualified&apos;)      )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GI Qualification Start Date</fullName>
        <actions>
            <name>Clear_GI_Qualification_Stop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GI_Qualification_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>To update the &quot;GI Qualification Start Date&quot; field with the current date when GI Acceptance Status field is updated to GI Accepted.</description>
        <formula>AND (      ISCHANGED(GI_Acceptance_Status__c),     ISPICKVAL(GI_Acceptance_Status__c, &apos;GI Accepted&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GI Qualification Stop Date</fullName>
        <actions>
            <name>GI_Qualification_Stop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>To update the &quot;GI Qualification Stop Date&quot; field with the current date when GI Acceptance Status field is updated to GI Disqualified or GI Rejected or Opp stage is updated to Goal Confirmed, Champion, Prove Value, Negotiating, Closed Won or Closed Lost.</description>
        <formula>OR (     AND     (          ISCHANGED(GI_Acceptance_Status__c),         OR         (             ISPICKVAL(GI_Acceptance_Status__c, &apos;GI Disqualified&apos;),             ISPICKVAL(GI_Acceptance_Status__c, &apos;GI Rejected&apos;)         )     ),     AND     (          ISCHANGED(StageName),         OR         (             ISPICKVAL(StageName, &apos;Goal Confirmed&apos;),             ISPICKVAL(StageName, &apos;Champion&apos;),             ISPICKVAL(StageName, &apos;Prove Value&apos;),             ISPICKVAL(StageName, &apos;Negotiating&apos;),             ISPICKVAL(StageName, &apos;Closed Won&apos;),             ISPICKVAL(StageName, &apos;Closed Lost&apos;)         )     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Access to Key Players Negotiated</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Acc_2Key_Play_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Access_to_Key_Players_Negotiated__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Champion Letter Confirmed with Champion Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Champ_Letter_Conf</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Champion_Letter_Confirmed_w_Champion__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Cost%2FBenefit Analysis Presented Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_CostBen_Ana_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Cost_Benefit_ROI_TCO_analysis_Presented__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - DM Agreed to Sequence of Events Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_DM_Agreed_SoE_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.DM_Agreed_to_Sequence_of_Events__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Decision Makers Identify Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Dec_Maker_Iden_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Decision_makers_s_Identified__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Goal Documented</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Goal_Documented</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Goal_Documented__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Implementation Plan Discussed Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Impl_Plan_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Implementation_Plan_Discussed__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - L%2FT%2FP process Discussed %26 Doced W%2FC Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_L_T_P_W_C_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.L_T_P_Process_discussed_docd_w_Cust__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Negotiation worksheet Complete Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Neg_WS_compl_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Negotiation_worksheet_completed__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Opp%2E Success documented %26 Agreed Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Opp_SucdocAgr_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opp_Success_Metrics_documented_agreed__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Pre-Decision Review close att w DM</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Pre_Dec_RevCl_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pre_Decision_Review_close_attmptd_wth_DM__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Proof Accepted by Key Players %28SIB%29 Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_ProofAc_KeyPL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Proof_Accepted_by_Key_Players_SIB__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Proposal Delivered to DM Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Prop_Deliv_DM_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pre_Decision_Review_close_attempted_with__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Solution Dev w Potential Champion Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Solution_Dev_w_Pot</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Solution_Developed_w_Potential_Champion__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Solutions Developed - Key Players Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Sol_De_key_pl_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Solutions_Developed_Key_Players_s__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ideal Pipeline Date - Verbal approval from DM Date</fullName>
        <actions>
            <name>Ideal_Pipeline_Date_Verbal_appr_DMDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Verbal_approval_from_DM__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mint information</fullName>
        <actions>
            <name>OPP_Mint_information</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) or(3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Mint_info_on_CA__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Deal Split-closed Won,Negotiating,Closed Won,Prove Value,ER - Closed Won,OEM - Closed Won,OEM Pre-Pay Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mint_info_on_CA__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mint_information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>populates the mint information field when an opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A INT Update - Closed Lost</fullName>
        <actions>
            <name>NS_INT_Update_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>ER - Closed Lost,OEM - Closed Lost,Closed Lost,Opportunity was created in error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.INT_NetSuite_InternalID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Number_of_Quotes__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>CR18963: Used to trigger Netsuite updates from SFDC Opportunity Record for Closed Lost opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Finance Approved</fullName>
        <actions>
            <name>NS_Set_Finance_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>OEM Prepay,OEM - Order,OEM Recruitment (CCS),Academic Program Opportunity,Qlikbuy CCS Standard II</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>ER - Closed Won,OEM - Closed Won,OEM Pre-Pay Closed Won,Deal Split-closed Won,Closed Won,CTW Closed Won</value>
        </criteriaItems>
        <description>!!! Needs to be edited at Go-Live with the opp record types we want this to happen for.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Opp to Academic Closed Won when Closed Date is Populated</fullName>
        <actions>
            <name>Set_Opp_to_Academic_Closed_Won_when_Clos</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Closed_date_for_conga__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>Academic Program</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Opp to Closed NFR when Closed Date is Populated</fullName>
        <actions>
            <name>NS_Update_Opp_to_Closed_NFR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Closed_date_for_conga__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>OEM Not For Resale,Reseller Not For Resale</value>
        </criteriaItems>
        <description>Set Opportunity  to Closed NFR when Closed Date is Populated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Opp to Closed Won when Closed Date is Populated</fullName>
        <actions>
            <name>NS_Update_Opp_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Closed_date_for_conga__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>Direct,Direct Subscription,Reseller,Reseller Subscription,Fulfillment,Reseller SA,Services – Deferred,Services – Prepaid,Direct Sense,Reseller Sense,Fulfillment Sense,Reseller Services-Deferred</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Opp to OEM Closed Won when Closed Date is Populated</fullName>
        <actions>
            <name>NS_Update_Opp_to_OEM_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Closed_date_for_conga__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>OEMSubscription,OEMRoyalty,OEMPerpetual,OEMDevelop,OEMSubs_Renewal,OEM,OEMPerpetual Sense,OEMDevelop Sense,OEMSubscription Sense</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Opp to OEM Prepay Closed Won when Closed Date is Populated</fullName>
        <actions>
            <name>NS_Update_Opp_to_OEM_Prepay_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Closed_date_for_conga__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>OEMPrepay</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Opp to QSG Closed Won when Closed Date is Populated</fullName>
        <actions>
            <name>Set_Opp_to_QSG_Closed_Won_when_Closed_Da</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Closed_date_for_conga__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>QSG,Direct QSG,Reseller QSG</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Update SLT%2FPO Approved Field Changed</fullName>
        <actions>
            <name>NS_Update_SLT_PO_Approved_Field_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISCHANGED(Special_License_terms_approved__c),  ISCHANGED(Pricing_options_approved__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP%3A SDD Clear Deal Desk Support</fullName>
        <actions>
            <name>Not_a_High_Touch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Account.Named_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.New_Customer__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Deal_Desk_Support__c</field>
            <operation>equals</operation>
            <value>High Touch</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.License_Amount_USD__c</field>
            <operation>lessThan</operation>
            <value>100000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Subscription_Forecast_ACV_Amount_USD__c</field>
            <operation>lessThan</operation>
            <value>50000</value>
        </criteriaItems>
        <description>BSL-1216 Subscription Deal Desk journey, this workflow is being triggered by Account trigger(handleDealDeskSupportUpdate) to capture account changes. Note The field is being set by another workflow called OPP: SDD Set Deal Desk Support To High Touch</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP%3A SDD Set Deal Desk Support To High Touch</fullName>
        <actions>
            <name>Set_High_Touch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>((1 AND 2) OR 3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Account.Named_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.New_Customer__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.License_Amount_USD__c</field>
            <operation>greaterOrEqual</operation>
            <value>100000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Subscription_Forecast_ACV_Amount_USD__c</field>
            <operation>greaterOrEqual</operation>
            <value>50000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Deal_Desk_Support__c</field>
            <operation>notEqual</operation>
            <value>High Touch</value>
        </criteriaItems>
        <description>BSL-1216 Subscription Deal Desk journey, this workflow is being triggered by Account trigger(handleDealDeskSupportUpdate) to capture account changes. Note there is another workflow called OPP: SDD Clear Deal Desk Support to clear it.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp %3A Segment on creation</fullName>
        <actions>
            <name>Segment_on_creation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Segment on Creation field should be populated with the Segment value from the Account</description>
        <formula>NOT( ISPICKVAL( Account.Segment_New__c , &quot;&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp Closed Lost</fullName>
        <actions>
            <name>Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp POC Change Alert America</fullName>
        <actions>
            <name>Opp_POC_change_America</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>BSL-2733 Used for notification to Solutions Engineering when POC fields are changed. Intended for short-term following Attunity integration</description>
        <formula>AND( OR(ISCHANGED( POC_Progress__c), ISCHANGED( POC_Daily_Recap__c )),  OR( CONTAINS(QlikTech_Company__c ,&quot;Inc&quot;), CONTAINS(QlikTech_Company__c ,&quot;Corporation&quot;), CONTAINS(QlikTech_Company__c ,&quot;Brazil&quot;), CONTAINS(QlikTech_Company__c ,&quot;LATAM&quot;), CONTAINS(QlikTech_Company__c ,&quot;Mexico&quot;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp POC Change Alert non-America</fullName>
        <actions>
            <name>Opp_POC_change_non_America</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>BSL-2733 Used for notification to Solutions Engineering when POC fields are changed on Opps in Americas. Intended for short-term following Attunity integration</description>
        <formula>AND( OR(ISCHANGED( POC_Progress__c), ISCHANGED( POC_Daily_Recap__c )),  NOT(OR( CONTAINS(QlikTech_Company__c ,&quot;Inc&quot;), CONTAINS(QlikTech_Company__c ,&quot;Corporation&quot;), CONTAINS(QlikTech_Company__c ,&quot;Brazil&quot;), CONTAINS(QlikTech_Company__c ,&quot;LATAM&quot;), CONTAINS(QlikTech_Company__c ,&quot;Mexico&quot;) )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Alert New and Previous Opportunity Owners when Opportunity owner is changed</fullName>
        <actions>
            <name>Alert_New_and_Previous_Opportunity_owner_when_Opportunity_owner_has_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow that sends out newly created e-mail template &quot;Notification Opportunity Owner changed - New opportunity owner&quot; to alert New and Previous Opportunity owner when Opportunity owner is changed. Applies only to Services Opportunities</description>
        <formula>AND(ISCHANGED(OwnerId), Is_Services_Opportunity__c = True)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Closed Won Rules</fullName>
        <actions>
            <name>Clear_Nurture_on_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Create_Eval_to_Null_SHARED</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Eval_Sent_Date_to_Null_SHARED</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (       ISPICKVAL(StageName  , &quot;Closed Won&quot;)  ,       ISCHANGED(StageName  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A GD Type set New</fullName>
        <actions>
            <name>Set_Goal_Discovery_Type_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Goal Discovery Type field to New if new Opportunity.</description>
        <formula>ISPICKVAL(StageName, &apos;Goal Discovery&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A GD Type set Recycled</fullName>
        <actions>
            <name>Set_Goal_Discovery_Type_to_Recycled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Goal Discovery Type field to Recycled if the Opportunity Stage changes to Goal Discovery.</description>
        <formula>AND (    ISCHANGED(StageName),    ISPICKVAL(StageName, &apos;Goal Discovery&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Game Changer Approved checked</fullName>
        <actions>
            <name>Game_Changer_Approved_checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Game Changed Approved check box is checked on the Account and Opportunity is OEM - Closed Won, field on the Opportunity should have the same value.</description>
        <formula>AND(Account.Game_Changer_Approved__c  = True, ISPICKVAL(StageName, &apos;OEM - Closed Won&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Game Changer Approved unchecked</fullName>
        <actions>
            <name>Game_Changer_Approved_unchecked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Game Changed Approved check box is unchecked on the Account, field on the Opportunity should have the same value.</description>
        <formula>Account.Game_Changer_Approved__c = False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Internal%3A SetDefaultStage</fullName>
        <actions>
            <name>Sets_stage_to_Goal_Discovery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the default Stage value when an Internal User creates an Opportunity</description>
        <formula>AND (     LEFT(RecordTypeId, 15) = &quot;012D0000000KEKO&quot;,    /*Qlikbuy CCS Standard II*/     NOT     (         OR         (              INCLUDES($User.Function__c, &quot;PRM&quot;),             INCLUDES($User.Function__c, &quot;Alliance&quot;)         )     )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Is Services Opp - Check</fullName>
        <actions>
            <name>Check_Is_services_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Is_Services_Opportunity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Check pse_is_services_opportunity when is_services_opportunity__c is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Is Services Opp - Uncheck</fullName>
        <actions>
            <name>Uncheck_Is_Services_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Is_Services_Opportunity__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Uncheck pse_is_sevices_opportunity when is_services_opportunity__c is unchecked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Mail Owner to say Nav Status has changed</fullName>
        <actions>
            <name>Opp_Email_to_Opp_Owner_when_Nav_Status_changed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(  AND(  ISCHANGED( Navision_Status__c ),  OR (  ISPICKVAL(Navision_Status__c, &apos;Eval License Created&apos;) ,  ISPICKVAL(Navision_Status__c, &apos;Posted&apos;)  )  ) , AND( ISCHANGED( Sent_To_Navision__c ) , Sent_To_Navision__c &lt;&gt; null)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Mail Owner to say Opp rejected</fullName>
        <actions>
            <name>Opp_Email_to_Opp_Owner_when_Opp_is_rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If the Opportunity is rejected then send a mail to the owner to let them know</description>
        <formula>AND(      ISCHANGED( Navision_Status__c ),      ISPICKVAL( Navision_Status__c , &quot;Rejected&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A New Academic Lead is created</fullName>
        <actions>
            <name>Opp_Set_record_type_to_academic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Academic Program Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Academic Program Opportunity</value>
        </criteriaItems>
        <description>Sets the record type to Academic when the accounts record type is academic</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Notify Opp owner of Spec License terms approved</fullName>
        <actions>
            <name>Opp_Notify_Opp_owner_of_Spec_License_terms_approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>CR 18320: Notifies Opp owner when Special License Terms have been approved by Legal</description>
        <formula>ISCHANGED( Special_License_terms_approved__c )  &amp;&amp;  Special_License_terms_approved__c = true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Notify legal of Spec License terms</fullName>
        <actions>
            <name>Opp_Notify_legal_of_Spec_License_terms</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>CR 18320: Notify Legal team when there is an opportunity with special license terms added or updated</description>
        <formula>OR( AND( ISNEW(), NOT(ISBLANK(Special_License_terms_requested__c))), AND (NOT(ISBLANK(Special_License_terms_requested__c)), ISCHANGED(Special_License_terms_requested__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Opp Creation</fullName>
        <actions>
            <name>Opp_Set_Create_Eval_to_Null_SHARED</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Eval_Expiry_Date_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Eval_Sent_Date_to_Null_SHARED</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Fin_Approved_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Fin_Comments_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Nav_status_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Set_Sent_to_Nav_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Navision_Status__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Navision_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A PO Number Null except OEM-Order</fullName>
        <actions>
            <name>Opp_Set_PO_Number_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Navision_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Navision_Status__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>OEM - Order</value>
        </criteriaItems>
        <description>Clear PO Number except on OEM - Order</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Populate Maint value number field</fullName>
        <actions>
            <name>Opp_Populate_Maint_value_number_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Maintenance_Value__c</field>
            <operation>greaterThan</operation>
            <value>,USD 0,EUR 0,NOK 0,SEK 0,CHF 0,JPY 0,AUD 0,GBP 0,DKK 0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Populate original created by function and role</fullName>
        <actions>
            <name>Update_original_created_by_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_original_created_by_function</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17</booleanFilter>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>ISR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>BDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iBDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>EIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Alliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>MIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>SMBIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>IT</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>R&amp;D</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iPM,SDR - MR,N3,ServiceSource,NIS</value>
        </criteriaItems>
        <description>Used to eliminate the need to create new user profiles when users change roles within SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Pre GI Stage checked as default</fullName>
        <actions>
            <name>Opp_Pre_GI_Stage_checked_as_default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Checked as default when the BDR EMEA group creates an opportunity from both lead conversion and contact/ account opp creation.</description>
        <formula>CONTAINS(&apos;acf|asy|amr|abk|alv|abt|aru|avo|cpi|cgr|dru|doa|egr|epc|efa|fsa|ghi|gdy1|hnu|hma|hme|jva|jab|jec|kbl|lhs|lbi|mve|mer|mlc|mwt|nka|pza|pmo|rro|rsu|wlf|cmt|cnw|akh|kcp|nho|asb|arp|jeb|mtl|nwd|gdy|lds|hyi|bsh|rnu|sso|fpi|jib|agm|anu|abs|ach|naa|bki|bch|cfe|dhl|ean|fal|gmr|jhp|jek|jdk|kgl|lyg|lma|mik|nvs|odl|rra|scn|lgr|seh|saf|sdi|sja|smf|sck|tye|uma|wpa|EXT_EYI|EXT_JIP|EXT_KHR|EXT_IRO|jtw|zle&apos;, CreatedBy.Alias)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A QDI Products Closed Won</fullName>
        <actions>
            <name>Opportunity_with_QDI_products_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4 OR 5) AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>QDI Customer Success Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>QDI Lite CDC Startup Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>QDI CDC Startup Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>QDI Data Lake Startup Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>QDI Data Warehouse Startup Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>contains</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Zuora_Quote_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email notification for closed won opportunities with QDI products.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Qlik Sales in Control</fullName>
        <actions>
            <name>Check_Qlik_Sales_in_Control</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow checks if an opportunity if an opportunity is not a Reseller revenue type if it is not a reseller revenue type the &quot;Qlik Sales in Control&quot; box will be checked.</description>
        <formula>NOT(  OR(  ISPICKVAL(Revenue_Type__c,&quot;Reseller&quot;),  ISPICKVAL(Revenue_Type__c,&quot;Reseller Sense&quot;),  ISPICKVAL(Revenue_Type__c, &quot;Fullfillment&quot;),  ISPICKVAL(Revenue_Type__c, &quot;Fullfillment Sense&quot;),  ISPICKVAL(Revenue_Type__c, &quot;&quot;)  )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Reset Sent To Nav date</fullName>
        <actions>
            <name>Opp_Set_Sent_to_Nav_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Finance_Approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Reset the sent to Navision date when the Finance Approved goes to false</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Send OEM To Navision</fullName>
        <actions>
            <name>Opp_Nav_trigger_email_for_OEM_accounts</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends the end customer to Navision for an OEM Opp</description>
        <formula>AND(  End_User_Account_del__c &lt;&gt; &quot;&quot;,  ISPICKVAL( End_User_Account_del__r.Navision_Status__c , &quot;&quot;), ISBLANK(  End_User_Account_del__r.Navision_Customer_Number__c  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Send Quote Email</fullName>
        <actions>
            <name>Send_Quote_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Correspondence_Email_Populate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Send_Email_Trigger_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Send_Quote_Email_Triggered_Datetime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Navision_Status__c</field>
            <operation>equals</operation>
            <value>Finance Processed,Posted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Finance_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Send_Quote_Email_Trigger__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Send_Quote_Email_Triggered__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Electronic_Acceptance__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Quote_Status__c</field>
            <operation>equals</operation>
            <value>Order Placed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>Direct</value>
        </criteriaItems>
        <description>If Navision Status = Finance Processed or Posted, Finance Approved = True and Send Quote Email Trigger = True This workflow will do the Workflow actions.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Acc Owner at Closed Won</fullName>
        <actions>
            <name>Set_Acc_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Acc_OwnerName</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Named_Acc_at_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Qualifying_SI</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Sell_Through_PAMName</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Sell_Through_PAM_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Spec_Pricing_Agrmnt_Terms_in_Place</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Territory_Credit_At_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>contains</operation>
            <value>Deal Split-closed Won,OEM - Closed Won,ER - Closed Won,Closed Won,OEM Pre-Pay Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Close Date for Goal Rejected</fullName>
        <actions>
            <name>Update_Close_Date_when_Opp_is_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Goal Rejected</value>
        </criteriaItems>
        <description>Used to set the Close Date if Opp Stage goes to Goal Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Mint information at credit approved</fullName>
        <actions>
            <name>Opp_Set_Mint_Info_in_mint_Info_at_CA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>locks the mint information field by adding a value to the mint information at finance approved field, thus preventing the opp: Mint information workflow triggering</description>
        <formula>And  (  Credit_Approved__c = True,  ISBLANK(Mint_info_on_CA__c)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Nav Status to Resent</fullName>
        <actions>
            <name>Set_Navision_Status_to_Resent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(     AND(         Finance_Approved__c  = true,       ISCHANGED(  Finance_Approved__c  )      ),     ISPICKVAL(Navision_Status__c, &quot;Rejected&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Navision Status to Pending Send on SiB</fullName>
        <actions>
            <name>OppSetNavisionStatustoPendingSend</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Prove Value,ER - Present Contracts,Trial,Present Contracts,Academic Program - Closed,Quote,Academic Program - Open,SiB,ER - Closed Won,ER - Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Navision_Customer_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>CCS - Customer Centric Selling</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCS - Customer Centric Selling</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiating,Closed Won,Prove Value</value>
        </criteriaItems>
        <description>Set Navision Status to Pending Send for Opportunities at SiB stage or higher if account does not already have a Navision customer number.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Next Step</fullName>
        <actions>
            <name>Next_Step_standard_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Standard field is no longer availble to end users but it&apos;s still used in reports and apps. Therefore value from the new field Next_step__c should be copied over to the standard field.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Opp Approved to FALSE when Stage not closed won</fullName>
        <actions>
            <name>OppSetOppAppdtoFALSEwhennotwon</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>ER - Closed Won,Closed Won,Academic Program - Closed,Academic Program - Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If Opp stage not closed won, opportunity approved = FALSE if it was previously TRUE (for when opportunities are cloned from approved opportunities)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Opportunity Name</fullName>
        <actions>
            <name>OppSetOpportunityName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Short_Description__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Academic Program Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Deal Split Child,Qlik Online Purchase,OEM Customer Order Deal Split Child,OEM Partner Order Deal Split Child</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set Partner Bonus field per Partner Type</fullName>
        <actions>
            <name>Opp_Set_Partner_Bonus_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>BSL-1124 - Modified the logic since existing logic is out of date and is not flagging opportunities correctly as being eligible for SPD</description>
        <formula>AND(  			OR( TEXT( Revenue_Type__c)=&apos;Reseller&apos;, 				TEXT( Revenue_Type__c)=&apos;Distributor&apos;) ,   			OR( TEXT(Opportunity_Source__c)=&apos;Partner Created&apos; , 			TEXT(Opportunity_Source__c)=&apos;Partner Registration&apos;)  		)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Set to Closed Won if within own limits</fullName>
        <actions>
            <name>OppSettoApprovedifwithinownlimit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>ER - Closed Won,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Within_Own_Limit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Profile__c</field>
            <operation>notContain</operation>
            <value>PRM</value>
        </criteriaItems>
        <description>If Opp is within standard sign off limit (i.e. &lt;=$50k and &lt; 10% or &gt;$50k and &lt; 20%)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Signature Subscriptions</fullName>
        <actions>
            <name>Signature_Opp_Internal_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>Signature Large</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>Signature Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>Signature Small</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>contains</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Zuora_Quote_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email notifications when Signature Opp is Closed Won. OTQ-438</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Signature Subscriptions Package 2 %26 3</fullName>
        <actions>
            <name>Email_Pearson_for_certification_request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>Signature Large</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Included_Products__c</field>
            <operation>includes</operation>
            <value>Signature Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>contains</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Zuora_Quote_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email to Pearson - Certification request for closed Won Signature Opp - Bundles 2 &amp; 3
OTQ-438</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Timestamp BDR Manager Grade changes</fullName>
        <actions>
            <name>Set_BDR_Manager_Grade_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Opp: Timestamp BDR Manager Grade changes</description>
        <formula>ISCHANGED(BDR_Manager_Grade__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Uncheck Bypass Rules Checkbox</fullName>
        <actions>
            <name>Uncheck_Bypass_Rules_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Bypass_Rules__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Unchecks bypass rules checkbox after triggers has run.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Update License value based on parent opp value %26 Deal split%25</fullName>
        <actions>
            <name>Opp_Update_Consult_Forecast_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Update_Educatio_Forecast_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Update_Licence_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Update_License_Forecast_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Update_Misc_Forecast_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Update_Support_Forecast_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Update_Total_Maintenance_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Subscription_Forecast_ACV_amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Subscription_Forecast_Amount_DSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Deal Split Child,OEM - Order Deal Split Child,OEM Recruitment Deal Split Child</value>
        </criteriaItems>
        <description>Updates opportunity value based on license value on parent opp and deal split percentage field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Update License value based on parent opp value %26 Deal split%25 Helper</fullName>
        <active>false</active>
        <description>This is a timed trigger that causes the PFF code to run on a DSC opportunity 1 hour after  Opp: Update License value based on parent opp value &amp; Deal split% has run.  Temp workaround until this can be implemented in Boomi.</description>
        <formula>RecordTypeId = &apos;01220000000DoEj&apos;/*Deal Split Child*/ &amp;&amp;  Requirements_met__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opp_Update_Consult_Forecast_Amount_DSC</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opp_Update_Educatio_Forecast_Amount_DSC</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opp_Update_License_Forecast_Amount_DSC</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opp_Update_Misc_Forecast_Amount_DSC</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opp_Update_Requirements_Met_to_false</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opp_Update_Support_Forecast_Amount_DSC</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opp%3A Webservice error logged</fullName>
        <actions>
            <name>Opp_End_Webservice_Error_message</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Used to send any webservice error that is added to the Opp</description>
        <formula>AND (     ISCHANGED( Integration_Webservice_Error_Message__c ) ,     NOT( ISBLANK(Integration_Webservice_Error_Message__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A no maintenance on service opps</fullName>
        <actions>
            <name>Maint_percent_none</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Maint_term_months_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Maintenance_type_none</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.pse__Is_Services_Opportunity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Make sure that maintenance is not applied to service opportuntiies</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3AFulfilment margin entered</fullName>
        <actions>
            <name>Opportunity_Fulfilment_margin_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert opportunity owner when fulfilment margin is entered.</description>
        <formula>Fulfillment_Margin__c &gt; 0</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set partner Bonus field</fullName>
        <actions>
            <name>Opportunity_Update_Partner_Bonus_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL(Sell_Through_Partner__r.Bonus_Margin_Program__c, &quot;Yes&quot;),  OR(  Account.Responsible_Partner__c&lt;&gt;NULL,  Deal_Reg_Partner_Account__c&lt;&gt;NULL  ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Finance Rejected</fullName>
        <actions>
            <name>Opportunity_Email_Alert_to_Owner_for_Finance_Rejected_Stage</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CR11539
Used to notify Opportunity Owner when Opp Stage is changed to Finance Rejected (CCS - Customer Centric Selling only)
Used RecordTypeID in formula instead of RecordType.Name due to reference limits.</description>
        <formula>AND( RecordTypeId=&quot;01220000000DugI&quot;, Is_Services_Opportunity__c = True, ISCHANGED(StageName), ISPICKVAL(StageName,&quot;Finance Rejected&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Override the Marketing Qualified Stage</fullName>
        <actions>
            <name>Sets_stage_to_Goal_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notContain</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Direct / Reseller - Std Sales Process,Direct / Reseller - Extension,CCS - Customer Centric Selling,Qlikbuy CCS Add on,Qlikbuy CCS Standard</value>
        </criteriaItems>
        <description>Skips the Marketing Qualified stage for all other users than Marketing</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Override the Marketing Qualified Stage 2</fullName>
        <actions>
            <name>update_Opp_stage_to_Goal_Identified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Alliances,Reseller</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Direct / Reseller - Std Sales Process,Direct / Reseller - Extension,CCS - Customer Centric Selling</value>
        </criteriaItems>
        <description>Sets the Opp stage to Goal Identified if Converted by a User with Alliances or Reseller Profile</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PRM - OPPTY Partner Opp Reg</fullName>
        <actions>
            <name>update_Margin_Expiry_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_Partner_Opp_Reg_Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Create_Date_Only__c &gt;= DATE(2013,01,01)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM Partner Bonnus</fullName>
        <actions>
            <name>PRM_Partner_Bonus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.PRM_Referring_PSM_QM__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - CCU</fullName>
        <actions>
            <name>PRMOpptyNotifyCCU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Nicolas MILLOT,Elodie GARNAUD,Claude-Henri Costes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - CMS</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Ron Stadler,Al Hughes,Mike Pasquariello,Kurt Shaffer,David Piech,Luca Loria,James Harder,Geoffrey Wayong,David Rubin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - ISR</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Wiebcke Lemcke,Nicole Schmeisser,Helmut Haslbeck,Guido Mitschke</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - JVR</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Jos Hosewol</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - KND</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Lars-Erik Johansson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - PCS</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Marek Grzebyk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - PSU</fullName>
        <actions>
            <name>PRMOpptyNotifyPSU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Pablo Robles,Daniel Vazquez</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify Status to Reseller Manager - SPR</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Geoffrey Wayong</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partners must put oppty in Registration</fullName>
        <actions>
            <name>Stage_Set_to_Registration_Pending_Approv</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Registration Pending Approval</value>
        </criteriaItems>
        <description>Via the Recently Viewed List view in communities, partners can click a &quot;New&quot; button which will allow them to enter the opportunity stage. If this happens this rule will update the stage to &quot;Registration Pending Approval&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>QCC Onsite Bundle Notification</fullName>
        <actions>
            <name>QCC_Onsite_Purchased</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>ER - Closed Won,OEM - Closed Won,Closed Won,QSG Closed Won,AP - Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.QCC_Onsite__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QDI Product Visibility For Partners</fullName>
        <actions>
            <name>populate_QDIPartnerCheck_field_on_oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule will populate Value in QDIPartnerCheck__c on opportunity based on the value on QDIPartnerApproved field on account for partner deals</description>
        <formula>AND(
  OR(
     RecordTypeId = $Setup.Q2CWSettings__c.SalesQCCSZuoraSubscription_OppRecordType__c,
     RecordTypeId = $Setup.Q2CWSettings__c.ZuoraRenewalTypeID__c
    ),
  OR(
    ISNEW(),
    ISCHANGED( Sell_Through_Partner__c ) 
   )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Registered and Protected Opp</fullName>
        <actions>
            <name>Check_Registered_and_Protected_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CR# 39855 - sets up criteria to check &quot;Registered and Protected Opportunity&quot; box on Opp</description>
        <formula>OR( NOT(ISBLANK(Deal_Reg_Partner_Account__c)), NOT(ISBLANK(Deal_Reg_Partner_Contact__c)), ISPICKVAL(CreatedBy.UserType,&quot;PowerPartner&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rev type %27subscription%27 set maint calc %3D List</fullName>
        <actions>
            <name>Set_Main_calc_List</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>Subscription</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set GI Acceptance Status to Accepted</fullName>
        <actions>
            <name>Set_GI_Acceptance_Status_to_Accepted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Opp Stage is updated from GI to Goal Confirmed, Champion, Prove Value, Negotiating, or Closed Won then update GI Acceptance Status to GI Accepted.</description>
        <formula>AND ( ISCHANGED(StageName), TEXT(GI_Acceptance_Status__c) &lt;&gt; &quot;GI Accepted&quot;, OR(          ISPICKVAL(StageName, &quot;Goal Confirmed&quot;),         ISPICKVAL(StageName, &quot;Champion&quot;),         ISPICKVAL(StageName, &quot;Prove Value&quot;),         ISPICKVAL(StageName, &quot;Negotiating&quot;),         ISPICKVAL(StageName, &quot;Closed Won&quot;) )         )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set GI Acceptance Status to Rejected</fullName>
        <actions>
            <name>Set_GI_Acceptance_Status_to_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Opp Stage is updated from GI to Goal Rejected then update GI Acceptance Status to GI Rejected.</description>
        <formula>AND (     ISCHANGED(StageName),     ISPICKVAL(PRIORVALUE(StageName), &quot;Goal Identified&quot;),     ISPICKVAL(StageName, &quot;Goal Rejected&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SetPartnerDefaultStageOnLeadConversion</fullName>
        <actions>
            <name>Sets_stage_to_Goal_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>PRM,Alliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsNotConvertedFromLead__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Qlikbuy CCS Standard II</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Subscription Opportunity Created</fullName>
        <actions>
            <name>New_Subscription_Opportunity_Created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales QCCS Zuora Subscription</value>
        </criteriaItems>
        <description>send an email when a new subscription opportunity is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Tech Partners Involved for DI opportunities</fullName>
        <actions>
            <name>Email_notification_asking_if_a_Technology_Partner_is_involved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>As a Data Integration Opportunity Owner when the Probability field surpasses 20% I would like to get an email notification asking me if a Technology Partner is involved from DL-GlobalTechPartner@qlik.com. OTQ-3</description>
        <formula>AND( Probability*100 &gt; 20,        OR(ISNULL(PRIORVALUE(Probability)),         PRIORVALUE(Probability)*100&lt;=20)      ,      OR( ISPICKVAL(Product_Category__c, &quot;Data Integration&quot;),         ISPICKVAL(Product_Category__c, &quot;Cross Product&quot;)         ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Deal Split Child Training Amount</fullName>
        <actions>
            <name>Update_Deal_Split_Child_Training_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Deal Split Child,OEM - Order Deal Split Child,OEM Recruitment Deal Split Child</value>
        </criteriaItems>
        <description>Update Deal Split Child Training Amount based on deal split percentage</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Reg Pending Approval when Internally created</fullName>
        <actions>
            <name>Sets_stage_to_Goal_Discovery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Registration Pending Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Is_Locked_in_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>notEqual</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>When an Internal user converts a Lead to an Opportunity or accidentally puts an opportunity into the very first stage, which is &quot;Registration Pending Approval&quot; this rule will update it to &quot;Goal Discovery&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage To GI Rejected</fullName>
        <actions>
            <name>Set_Stage_To_Goal_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.GI_Rejected_Reason__c</field>
            <operation>equals</operation>
            <value>Already Working,Incomplete/Inaccurate Info,Not Goal Identified Opportunity,Created in Error,Duplicate - Please Indicate Duplicate Link</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.GI_Disqualified_Reason__c</field>
            <operation>equals</operation>
            <value>No Response,No Business Goal,Duplicate - Please Indicate Duplicate Link,Qlik Cannot Provide a Solution</value>
        </criteriaItems>
        <description>Update the Stage field to Goal Rejected based on various values in the GI Rejected Reason and GI Disqualified Reason fields.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Voucher Populate Send to DL QT Region</fullName>
        <actions>
            <name>Populate_Voucher_Send_to_DL_QT_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Send_to_DL_QT_Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Populate the &quot;Send to DL QT Region&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Voucher Send</fullName>
        <actions>
            <name>Voucher_Send_voucher_details</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Voucher_Send_voucher_details_INTERNAL</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Correspondence_Email_Populate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Voucher_Update_Voucher_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Finance_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Navision_Status__c</field>
            <operation>contains</operation>
            <value>Finance Processed,Posted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Voucher_Mail_Triggered__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.VoucherStatus__c</field>
            <operation>equals</operation>
            <value>Vouchers Created and Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.VoucherEmailTrigger__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends the vouchers associated with the Opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Voucher Send QTNEXT</fullName>
        <actions>
            <name>Voucher_Send_voucher_details_INTERNAL</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Correspondence_Email_Populate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Voucher_Update_Voucher_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsClosedWon__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.VoucherStatus__c</field>
            <operation>equals</operation>
            <value>Vouchers Created and Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Voucher_Mail_Triggered__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.VoucherEmailTrigger__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends the vouchers associated with the Opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Voucher%3A Voucher Issue Notification</fullName>
        <actions>
            <name>Voucher_Issue_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.VoucherStatus__c</field>
            <operation>equals</operation>
            <value>Vouchers Issue</value>
        </criteriaItems>
        <description>Will send an email if Voucher Status on opp is Vouchers Issue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Voucher%3A Voucher Pending Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.VoucherStatus__c</field>
            <operation>equals</operation>
            <value>Vouchers Pending</value>
        </criteriaItems>
        <description>Will set status to Voucher Issue after an hour of pending status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Voucher_Status_to_Issue_From_Pending</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Voucher_Issue_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Voucher_Issue_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>if partner deal record PSM at Closed Won</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>ER - Closed Won,OEM - Closed Won,Deal Split-closed Won,Closed Won,CTW Closed Won,OEM Pre-Pay Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Qonnect_Manager_at_close__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>records the Qonnect Manager ID at closed won. (Now replaced by &apos;OpportunityIfPartnerDealRecordPSMAtClosedWon.trigger &apos;. It&apos;s 2 field updates, &apos;Opp: Add QM ID to QM at close field&apos; and &apos;Opp: Add Qonnect Manager Name&apos; have been removed. (CR# 6188)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>set Deal Split Maint Value</fullName>
        <actions>
            <name>Calculate_Deal_Split_maint_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Maintenance_Value__c</field>
            <operation>greaterOrEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Deal Split Child,OEM - Order Deal Split Child,OEM Recruitment Deal Split Child</value>
        </criteriaItems>
        <description>calculates maintenance value based on deal split parent amount * deal split percentage</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Revenue_Type__c</field>
            <operation>equals</operation>
            <value>OEM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>CaptureapplicationdetailsforClosedWonOpportunity</fullName>
        <assignedToType>owner</assignedToType>
        <description>You have successfully closed the opportunity {!Opportunity.Name}. 

Please consider capturing application information related to the opportunity.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Capture application details for Closed/Won Opportunity</subject>
    </tasks>
    <tasks>
        <fullName>ReminderCaptureapplicationdetailsforopportunity</fullName>
        <assignedToType>owner</assignedToType>
        <description>You have successfully closed the opportunity {!Opportunity.Name}. 

Please consider capturing application information related to the opportunity. 

If you have already entered application details, please disregard and close this task</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reminder: Capture application details for opportunity</subject>
    </tasks>
</Workflow>