<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Ops_the_Number_of_Books_Changed_for_Training_Detail</fullName>
        <ccEmails>evonna.wilke@qlikview.com</ccEmails>
        <description>Notify Ops the Number of Books Changed for Training Detail</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/PSE_Training_Details_Number_of_Books_Changed</template>
    </alerts>
    <alerts>
        <fullName>Notify_Service_Ops_of_New_Training_Detail</fullName>
        <ccEmails>USTraining@QlikView.com</ccEmails>
        <description>Notify Service Ops of New Training Detail</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/New_Training_Details</template>
    </alerts>
    <rules>
        <fullName>PSE Notify Service Ops of New Training Details</fullName>
        <actions>
            <name>Notify_Service_Ops_of_New_Training_Detail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9</booleanFilter>
        <criteriaItems>
            <field>Training_Detail__c.Designer_I_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Designer_II_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Developer_I_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Developer_II_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Developer_III_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Svr_Pub_I_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Svr_Pub_II_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Other_Books_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Detail__c.Set_Analysis_Book_count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Sends an email to service ops when a new Training Detail record is entered</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSE Training Details Number of Books Changed</fullName>
        <actions>
            <name>Notify_Ops_the_Number_of_Books_Changed_for_Training_Detail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Ops when the number of books has changed for a training detail.</description>
        <formula>(ISNEW() || ISCHANGED( Number_of_Books__c)) &amp;&amp; NOT(ISBLANK(Number_of_Books__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
