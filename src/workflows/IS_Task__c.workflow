<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CR_Task_Assigned</fullName>
        <description>CR Task Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SLX__ChangeControl/IS_Task_Assigned</template>
    </alerts>
    <alerts>
        <fullName>CR_Task_updated</fullName>
        <description>CR_Task_updated</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SLX__ChangeControl/CR_Task_Updated</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_assigned_to_ID</fullName>
        <field>Assigned_to_ID__c</field>
        <formula>Assigned_to__r.Id</formula>
        <name>Update assigned to ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CR Task Assigned</fullName>
        <actions>
            <name>CR_Task_Assigned</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_assigned_to_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Notifies the creator of a task against a CR or Component has been assigned to them</description>
        <formula>LEFT(Assigned_to__r.Id,15)  &lt;&gt;  LEFT(Assigned_to_ID__c,15)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CR Task Updated</fullName>
        <actions>
            <name>CR_Task_updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IS_Task__c.Status__c</field>
            <operation>equals</operation>
            <value>In Progress,More Info Required,Completed</value>
        </criteriaItems>
        <description>Notifies the creator of a task against a CR or Component that the status has been updated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
