<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Academic_Program_account_owner_assigned_incorrectly</fullName>
        <ccEmails>academicprogram@qlik.com</ccEmails>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <description>Academic Program account owner assigned incorrectly</description>
        <protected>false</protected>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Academic_Program/Academic_Program_account_owner_assigned_incorrectly</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_mail_to_say_the_trigger_has_happened</fullName>
        <ccEmails>ssh@qlikview.com</ccEmails>
        <description>Confirmation_mail_to_say_the_trigger_has_happened</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_Hardware_request_received</template>
    </alerts>
    <alerts>
        <fullName>Contact_Finance</fullName>
        <description>Contact Finance</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Internal_Emails/Account_Navision_MultipleMatches</template>
    </alerts>
    <alerts>
        <fullName>End_User_Acc_Pending_Validation_Created_Email_to_Creator</fullName>
        <description>End User Acc Pending Validation Created - Email to Creator</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Plan_Emails/End_User_Acc_Pending_Validation_Created</template>
    </alerts>
    <alerts>
        <fullName>Monitor_Send_email_when_Account_changed</fullName>
        <description>Monitor - Send email when Account changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>dlr@qlikview.com-retired</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ssh@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Internal_Emails/Modified_Account</template>
    </alerts>
    <alerts>
        <fullName>NAVSendMailToSupportWhenAccTypeChanges</fullName>
        <ccEmails>supaccinfo@qlikview.com</ccEmails>
        <description>NAV: Send mail to Support when Acc Type Changes</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Admin_Internal_Emails/Email_Support_Account_Type_Changed</template>
    </alerts>
    <alerts>
        <fullName>NavMissingdatainNavProcess</fullName>
        <ccEmails>ssh@qliktech.com</ccEmails>
        <description>Nav - Missing data in Nav Process</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Nav_Problem_Sending_To_Navision</template>
    </alerts>
    <alerts>
        <fullName>Partner_Termination</fullName>
        <description>Acc: Partner Termination notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Account_Partner_is_Terminated</template>
    </alerts>
    <alerts>
        <fullName>Partner_Training_Discount_Validation</fullName>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <description>Partner Training Discount Validation</description>
        <protected>false</protected>
        <recipients>
            <recipient>smm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforce-no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Partner_Training_Discount_Validation</template>
    </alerts>
    <alerts>
        <fullName>Q2O_Realign_Account_Licenses</fullName>
        <ccEmails>downloads@qliktech.com</ccEmails>
        <description>Q2O - Realign Account Licenses</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Quote_To_Order/Q2O_Realign_Account_Licenses</template>
    </alerts>
    <alerts>
        <fullName>Q2O_Send_email_to_Acc_Mgr_when_credit_status_changes</fullName>
        <description>Q2O - Send email to Acc Mgr when credit status changes</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Quote_To_Order/Q2O_Credit_Hold_Status</template>
    </alerts>
    <alerts>
        <fullName>QCC_Entitled_changed_email</fullName>
        <ccEmails>QCCEntitledChangeNotification@qlik.com</ccEmails>
        <description>QCC Entitled changed email</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Account_QCC_Entitled_Changed</template>
    </alerts>
    <alerts>
        <fullName>Sendemailtoaccountownerandccsshqliktechcom</fullName>
        <description>Nav: Send email to account owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Navision_Data_Incomplete</template>
    </alerts>
    <fieldUpdates>
        <fullName>ACC_Set_QT_Region</fullName>
        <field>QlikTech_Region__c</field>
        <formula>Billing_Country_Code__r.QlikTech_Region__c</formula>
        <name>ACC: Set QT Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AccSetQTCompanyName</fullName>
        <description>Set the QlikTech Company name based on the Country of the company</description>
        <field>QlikTech_Company__c</field>
        <formula>Billing_Country_Code__r.QlikTech_Company_Name__c</formula>
        <name>Acc: Set QT Company Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acc_Set_Pending_Validation</fullName>
        <field>Pending_Validation__c</field>
        <literalValue>1</literalValue>
        <name>Acc Set Pending Validation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acc_Update_Rapidi_Connector_Code</fullName>
        <field>Rapidi_Nav_Connector_Code__c</field>
        <formula>&apos;NAV0&apos; &amp; CASE(Upper( QlikTech_Company__c ), 
&apos;QLIKTECH UK LTD&apos;, &apos;01&apos;, 
&apos;QLIKTECH BENELUX&apos;, &apos;02&apos;, 
&apos;QLIKTECH FINLAND OY&apos;, &apos;03&apos;, 
&apos;QLIKTECH FRANCE SARL&apos;, &apos;04&apos;, 
&apos;QLIKTECH GMBH&apos;, &apos;05&apos;, 
&apos;QLIKTECH IBERIA SL&apos;, &apos;06&apos;, 
&apos;QLIKTECH INC&apos;, &apos;07&apos;, 
&apos;QLIKTECH INTERNATIONAL AB&apos;, &apos;08&apos;, 
&apos;QLIKTECH NORDIC AB&apos;, &apos;09&apos;, 
&apos;QLIKTECH DENMARK APS&apos;, &apos;11&apos;, 
&apos;QLIKTECH NORWAY AS&apos;, &apos;12&apos;, 
&apos;QLIKTECH JAPAN KK&apos;, &apos;13&apos;, 
&apos;QLIKTECH AUSTRALIA PTY LTD&apos;, &apos;14&apos;, 
&apos;QLIKTECH LATAM AB&apos;, &apos;15&apos;, 
&apos;QLIKTECH BRAZIL&apos;, &apos;16&apos;, 
&apos;QLIKTECH HONG KONG LTD&apos;, &apos;17&apos;, 
&apos;QLIKTECH INDIA PVT LTD&apos;, &apos;18&apos;, 
&apos;QLIKTECH INT.MARKETS AB&apos;, &apos;19&apos;, 
&apos;QLIKTECH SINGAPORE PTE LTD&apos;, &apos;20&apos;,
&apos;QLIKTECH ITALY S.R.L.&apos;,&apos;27&apos;, 
&apos;NONE&apos;)</formula>
        <name>Acc: Update Rapidi Connector Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Set_ERP_status_to_Trial_License</fullName>
        <description>NS needed field update</description>
        <field>Navision_Status__c</field>
        <literalValue>Trial License</literalValue>
        <name>Account Set ERP status to Trial License</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Billing_Country_update</fullName>
        <description>Updating Billing Country value from country code to country name, if Billing country was previously updated by Webcom user to country code.</description>
        <field>BillingCountry</field>
        <formula>PRIORVALUE( BillingCountry )</formula>
        <name>Billing Country update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INT_Set_Navision_Customer_Number</fullName>
        <field>Navision_Customer_Number__c</field>
        <formula>Integration_PotentialNavNumber__c</formula>
        <name>INT: Set Navision Customer Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INT_Set_Navision_Status</fullName>
        <field>Navision_Status__c</field>
        <literalValue>Navsion Updated</literalValue>
        <name>INT: Set Navision Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legal_Approval_Request_Granted</fullName>
        <description>Legal Approval Request Granted</description>
        <field>Legal_Approval_Status__c</field>
        <literalValue>Legal Approval Granted</literalValue>
        <name>Legal Approval Request Granted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legal_Approval_Request_Refused</fullName>
        <description>Legal Approval Request Refused</description>
        <field>Legal_Approval_Status__c</field>
        <literalValue>Legal Approval Refused</literalValue>
        <name>Legal Approval Request Refused</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAV_Clear_Nav_Cust_Num_Changed</fullName>
        <field>Nav_Cust_Num_Changed__c</field>
        <literalValue>0</literalValue>
        <name>NAV: Clear Nav Cust Num Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAV_Copy_Nav_Cust_Num2</fullName>
        <field>Original_Nav_Cust_Num__c</field>
        <formula>Navision_Customer_Number__c</formula>
        <name>NAV: Copy Nav Cust Num2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAV_Set_Nav_Customer_Num_Changed2</fullName>
        <field>Nav_Cust_Num_Changed__c</field>
        <literalValue>1</literalValue>
        <name>NAV: Set Nav Customer Num Changed2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAV_Set_Navision_Customer_No_Changed_Dat</fullName>
        <description>Sets Navision Customer No Changed to 1 Jan 1900</description>
        <field>Navision_Customer_No_Changed_Date__c</field>
        <formula>DATE(1900,1,1)</formula>
        <name>NAV - Set Navision Customer No Changed D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAV_clear_incomplete_record_when_nav_fie</fullName>
        <description>clears the navision status when at a status of incomplete send and the within nav field limits checkbox is unchecked</description>
        <field>Navision_Status__c</field>
        <name>NAV clear incomplete record when nav fie</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NS_Set_Segment_on_OEM_Accounts</fullName>
        <description>NS Field Update</description>
        <field>Segment_New__c</field>
        <literalValue>Enterprise - Target</literalValue>
        <name>NS: Set Segment on OEM/Academic Accounts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Onboarding_First_Agree_than_90_days</fullName>
        <description>Update Onboarding Status to Onboarding complete.</description>
        <field>Onboarding_Status__c</field>
        <literalValue>Onboarding Complete</literalValue>
        <name>Onboarding - First Agree &gt; than 90 days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Recruitment_Status_won_when_Contract</fullName>
        <field>Recruitment_Status__c</field>
        <literalValue>Won</literalValue>
        <name>PAR Recruitment Status won when Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Set_Partner_Status_to_Contracted</fullName>
        <field>Partner_Status__c</field>
        <literalValue>Contracted</literalValue>
        <name>PAR Set Partner Status to Contracted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Set_Partner_Status_to_Enable</fullName>
        <field>Partner_Status__c</field>
        <literalValue>Enable</literalValue>
        <name>PAR Set Partner Status to Enable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Set_Partner_Status_to_Lost</fullName>
        <field>Partner_Status__c</field>
        <literalValue>Lost</literalValue>
        <name>PAR Set Partner Status to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Set_Partner_Status_to_Recruit</fullName>
        <field>Partner_Status__c</field>
        <literalValue>Recruit</literalValue>
        <name>PAR Set Partner Status to Recruit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Set_Partner_Status_to_Terminated</fullName>
        <field>Partner_Status__c</field>
        <literalValue>Terminated</literalValue>
        <name>PAR Set Partner Status to Terminated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Set_Partner_Status_to_discontinued</fullName>
        <field>Partner_Status__c</field>
        <literalValue>Discontinue</literalValue>
        <name>PAR Set Partner Status to discontinued</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_set_rec_status_to_ongoing</fullName>
        <description>When a record is created, or when a record is edited and did not previously meet the rule criteria</description>
        <field>Recruitment_Status__c</field>
        <literalValue>Ongoing</literalValue>
        <name>PAR set rec status to ongoing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Q2O_Reset_Realign_License_flag_at_acco</fullName>
        <field>Realign_Licenses__c</field>
        <literalValue>0</literalValue>
        <name>Q2O - Reset Realign License flag at acco</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Academic_Account_Type</fullName>
        <description>Set Lead &quot;Academic Account Type&quot; to &quot;Academic Program Account&quot;</description>
        <field>Academic_Account_Type__c</field>
        <formula>&quot;Academic Program Account&quot;</formula>
        <name>Set Lead &quot;Academic Account Type&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Country_Code_for_Sharing</fullName>
        <description>Update Account field &quot;Billing Country Code for Sharing&quot; with the value from the Billing Country Code lookup field</description>
        <field>Billing_Country_Code_for_Sharing__c</field>
        <formula>Billing_Country_Code__r.Name</formula>
        <name>Update Billing Country Code for Sharing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Flagged_for_NS_update_on_Account</fullName>
        <field>Flagged_for_NS_update__c</field>
        <literalValue>1</literalValue>
        <name>Update Flagged for NS update on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Market_Name_Field</fullName>
        <description>Update Account field &quot;Market Name&quot; with the value from the Billing Country Code field &quot;Market Name&quot;</description>
        <field>Market_Name__c</field>
        <formula>Billing_Country_Code__r.Market_Name__c</formula>
        <name>Update Market Name Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Onboarding_First_Agreement_Less</fullName>
        <description>Time based Workflow: looks at value in First agreement date on account, and if less than 90 days ago sets Onboarding status to identified for onboarding.</description>
        <field>Onboarding_Status__c</field>
        <literalValue>Identified for Onboarding</literalValue>
        <name>Update Onboarding - First Agreement Less</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Partner_Training_Margin</fullName>
        <field>Partner_Training_Margin__c</field>
        <formula>0.3</formula>
        <name>Update Partner Training Margin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Partner_Training_Margin_Remove</fullName>
        <field>Partner_Training_Margin__c</field>
        <name>Update Partner Training Margin Remove</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Training_Margin</fullName>
        <field>Training_Margin__c</field>
        <formula>0.15</formula>
        <name>Update Training Margin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Training_Margin_20_special</fullName>
        <description>Limited BEERMA promotion 1st October to 31st December 2013</description>
        <field>Training_Margin__c</field>
        <formula>0.20</formula>
        <name>Update Training Margin - 20% special</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Training_Margin_Remove</fullName>
        <field>Training_Margin__c</field>
        <name>Update Training Margin Remove</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_Role</fullName>
        <field>Original_Created_by_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Update original created by Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_function</fullName>
        <field>Original_Created_by_Function__c</field>
        <formula>IF ( INCLUDES ( $User.Function__c, &quot;Sales&quot; ), &quot;Sales; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Marketing&quot; ), &quot;Marketing; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Alliance&quot; ), &quot;Alliance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;BDR&quot; ), &quot;BDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iBDR&quot; ), &quot;iBDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ISR&quot; ), &quot;ISR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;EIS&quot; ), &quot;EIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;MIS&quot; ), &quot;MIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SMBIS&quot; ), &quot;SMBIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;PRM&quot; ), &quot;PRM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Support&quot; ), &quot;Support; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Training&quot; ), &quot;Training; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Finance&quot; ), &quot;Finance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;HR&quot; ), &quot;HR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;IT&quot; ), &quot;IT; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;R&amp;D&quot; ), &quot;R&amp;D; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iPM&quot; ), &quot;iPM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SDR - MR&quot; ), &quot;SDR - MR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;N3&quot; ), &quot;N3; &quot;,null )&amp;
IF ( INCLUDES ( $User.Function__c, &quot;NIS&quot; ), &quot;NIS; &quot;,null )&amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ServiceSource&quot; ), &quot;ServiceSource; &quot;,null )</formula>
        <name>Update original created by function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Within_Nav_field_length_limits_FALSE</fullName>
        <field>Within_Nav_field_length_limits__c</field>
        <literalValue>0</literalValue>
        <name>Within Nav field length limits = FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Within_Nav_field_length_limits_TRUE</fullName>
        <field>Within_Nav_field_length_limits__c</field>
        <literalValue>1</literalValue>
        <name>Within Nav field length limits = TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>ECUSTOMS__RPSAccount_PROD</fullName>
        <apiVersion>40.0</apiVersion>
        <description>RPS screening (PROD) for accounts</description>
        <endpointUrl>https://sf.visualcompliance.com/RPS_SF_WS/AccountNotificationService_Managed.asmx</endpointUrl>
        <fields>BillingCity</fields>
        <fields>BillingCountry</fields>
        <fields>BillingState</fields>
        <fields>BillingStreet</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>qtweb@qliktech.com</integrationUser>
        <name>RPSAccount_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>ECUSTOMS__eCustoms_RPSAccount_OM</fullName>
        <apiVersion>32.0</apiVersion>
        <description>RPS Screening using eCustoms NameSpace using testsite</description>
        <endpointUrl>https://sftest.visualcompliance.com/RPS_SF_WS/AccountNotificationService_Managed.asmx</endpointUrl>
        <fields>AccountNumber</fields>
        <fields>BillingCountry</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>qtweb@qliktech.com</integrationUser>
        <name>eCustoms_RPSAccount_OM</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>ACC%3A Exists in Navision contact Finance</fullName>
        <actions>
            <name>Contact_Finance</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends an email to the owner if navision reports duplicate matches found</description>
        <formula>AND(ISCHANGED(Navision_Feedback__c), Navision_Feedback__c = &apos;Already exists, please contact finance&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC%3A Populate original created by function and role</fullName>
        <actions>
            <name>Update_original_created_by_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_original_created_by_function</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17</booleanFilter>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>ISR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>BDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iBDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>EIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Alliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>MIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>SMBIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>IT</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>R&amp;D</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iPM,SDR - MR,N3,ServiceSource,NIS</value>
        </criteriaItems>
        <description>Used to eliminate the need to create new user profiles when users change roles within SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Academic Program account reassignment rule</fullName>
        <actions>
            <name>Academic_Program_account_owner_assigned_incorrectly</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISCHANGED(OwnerId), Record_Type_As_Text__c  = &apos;Academic Program Account&apos;,  NOT(OR(     OwnerId =&apos;005D0000001ppXL&apos;,     OwnerId =&apos;00520000000z3Pv&apos;,     OwnerId =&apos;005D0000005tcQw&apos;,     OwnerId =&apos;005D000000BIQ5S&apos;,     OwnerId =&apos;005D0000005sXJt&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Acc - Update Partner Training Margin 4 OEM Acc</fullName>
        <actions>
            <name>Update_Partner_Training_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CR 18150 requires Partner Training Margin on OEM Partner Accounts to remain 30 %</description>
        <formula>ispickval(Navision_Status__c,&quot;Partner&quot;) &amp;&amp; Record_Type_As_Text__c = &apos;OEM Partner Account&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acc - Update Training Margin fields Add Margin</fullName>
        <actions>
            <name>Update_Partner_Training_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Training_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Record_Type_As_Text__c &lt;&gt; &apos;OEM Partner Account&apos; &amp;&amp; ispickval(Navision_Status__c,&quot;Partner&quot;) &amp;&amp; ((datevalue(LastModifiedDate) &lt; DATE(2013,10,01)) || (datevalue(LastModifiedDate) &gt; DATE(2013,12,31))  ||(Billling_Country_Code_Name__c &lt;&gt; &apos;BEL&apos; &amp;&amp; Billling_Country_Code_Name__c &lt;&gt; &apos;NLD&apos; &amp;&amp; Billling_Country_Code_Name__c &lt;&gt; &apos;LUX&apos;&amp;&amp; Billling_Country_Code_Name__c &lt;&gt; &apos;POL&apos; &amp;&amp; Billling_Country_Code_Name__c &lt;&gt; &apos;ARE&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acc - Update Training Margin fields Add Margin - Temp Q4 20%25 CR%23 9483</fullName>
        <actions>
            <name>Update_Partner_Training_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Training_Margin_20_special</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ispickval(Navision_Status__c,&quot;Partner&quot;)  &amp;&amp;  (Billling_Country_Code_Name__c  = &apos;BEL&apos; || Billling_Country_Code_Name__c  = &apos;NLD&apos; || Billling_Country_Code_Name__c  = &apos;LUX&apos;|| Billling_Country_Code_Name__c  = &apos;POL&apos;|| Billling_Country_Code_Name__c  = &apos;ARE&apos;)   &amp;&amp;  (datevalue(LastModifiedDate) &gt;=  DATE(2013,10,01)) &amp;&amp;  (datevalue(LastModifiedDate) &lt;=  DATE(2013,12,31))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acc - Update Training Margin fields Remove Margin</fullName>
        <actions>
            <name>Update_Partner_Training_Margin_Remove</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Training_Margin_Remove</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Navision_Status__c</field>
            <operation>notEqual</operation>
            <value>Partner</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acc Set Pending Validation</fullName>
        <actions>
            <name>Acc_Set_Pending_Validation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>End User Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>To set the Pending Validation on each new account created by internal users except for few profiles and when its not converted from Lead.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Acc%3A Billing Country update</fullName>
        <actions>
            <name>Billing_Country_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will set back Billing Country value from country code to country name, if Billing country was previously updated by Webcom user to country code.</description>
        <formula>LEN( BillingCountry ) = 3</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acc%3A Partner Termination notification</fullName>
        <actions>
            <name>Partner_Termination</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Partner_Status__c</field>
            <operation>equals</operation>
            <value>Terminated</value>
        </criteriaItems>
        <description>Notify to Account Owner if one of his Partner Accounts is terminated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acc%3A Set QT Company Name</fullName>
        <actions>
            <name>ACC_Set_QT_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AccSetQTCompanyName</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Acc_Update_Rapidi_Connector_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Navision_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set the QlikTech Company Name based on the Country of the company</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acc%3A Update Rapidi Connector Code</fullName>
        <actions>
            <name>Acc_Update_Rapidi_Connector_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Determines and Updates the field rapidi connector code on the account</description>
        <formula>OR(     ISNEW() ,     ISCHANGED(QlikTech_Company__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account QCC Entitled send email notification</fullName>
        <actions>
            <name>QCC_Entitled_changed_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), Record_Type_As_Text__c == &apos;Partner Account&apos;, ISCHANGED(QCC_Entitled__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ECUSTOMS__eCustoms Account RPS</fullName>
        <actions>
            <name>ECUSTOMS__RPSAccount_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>eCustoms  Account RPS Workflow Rule</description>
        <formula>OR(isNew(), isChanged( Name ), isChanged( BillingCity ), isChanged( BillingState ), isChanged( BillingCountry ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>INT%3A Complete Navision Process</fullName>
        <actions>
            <name>INT_Set_Navision_Customer_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>INT_Set_Navision_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (     ISCHANGED (Integration_Account_Send_Date__c),     NOT (  ISBLANK( Integration_Account_Send_Date__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor Inside Info Account</fullName>
        <actions>
            <name>Monitor_Send_email_when_Account_changed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.SLX_Account_ID__c</field>
            <operation>equals</operation>
            <value>A6UJ9A004JZD</value>
        </criteriaItems>
        <description>There is an issue with the Navision Status on Inside Info changing, this workflow is here to inform ssh and dlr when this happens</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NAV - Navision Status Changed</fullName>
        <actions>
            <name>NAVSendMailToSupportWhenAccTypeChanges</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>triggers mail to Support when account changes to Customer or Partner</description>
        <formula>AND( ISCHANGED( Navision_Status__c ) ,  OR(ISPICKVAL(Navision_Status__c,  &quot;Customer&quot;), ISPICKVAL(Navision_Status__c,  &quot;Partner&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NAV - Send Failed</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Navision_Status__c</field>
            <operation>equals</operation>
            <value>Send Failed</value>
        </criteriaItems>
        <description>Sends a mail to ssh when the send to Nav process failed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NAV clear incomplete record when nav fields outside limits</fullName>
        <actions>
            <name>NAV_clear_incomplete_record_when_nav_fie</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Navision_Status__c</field>
            <operation>contains</operation>
            <value>incomplete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Within_Nav_field_length_limits__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>clears the navision status when at a status of incomplete send and the within nav field limits checkbox is unchecked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Acc%3A Set ERP status to Trial License</fullName>
        <actions>
            <name>Account_Set_ERP_status_to_Trial_License</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used for NS</description>
        <formula>AND( No_of_Eval_deals__c &gt; 0, AND( NOT(ISPICKVAL( Navision_Status__c , &apos;Customer&apos;)), NOT(ISPICKVAL( Navision_Status__c , &apos;Partner&apos;))) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Set Segment on OEM%2FAcademic Accounts</fullName>
        <actions>
            <name>NS_Set_Segment_on_OEM_Accounts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Academic Program Account</value>
        </criteriaItems>
        <description>Sets the Segment to Enterprise for OEM/Academic accounts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Update Flagged for NS update on Account</fullName>
        <actions>
            <name>Update_Flagged_for_NS_update_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to trigger Netsuite updates from SFDC Account Record</description>
        <formula>OR( ISCHANGED( OwnerId ), ISCHANGED( Bonus_Margin_Program__c ), ISCHANGED( Regional_Bonus__c ), ISCHANGED( Partner_Margin__c ), ISCHANGED( Training_Margin__c ), ISCHANGED( Partner_Training_Margin__c ), ISCHANGED( Special_Pricing_Account__c ), ISCHANGED( Special_Pricing_End_Date__c ), ISCHANGED( Named_Account__c ) , ISCHANGED( Start_Date_for_Named_Account__c ) , ISCHANGED( End_Date_for_Named_Account__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Nav - Incomplete Data</fullName>
        <actions>
            <name>NavMissingdatainNavProcess</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Navision_Status__c</field>
            <operation>equals</operation>
            <value>Incomplete Record</value>
        </criteriaItems>
        <description>Sends an email to the Account Owner to tell them that the record they just sent did not go through</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nav%3A Send an email when Navision_Status%5F%5Fc field is set to %27Incomplete Record</fullName>
        <actions>
            <name>Sendemailtoaccountownerandccsshqliktechcom</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Navision_Status__c</field>
            <operation>equals</operation>
            <value>Incomplete Record</value>
        </criteriaItems>
        <description>Send an email to the owner of an Account every time Navision_Status__c field is set to &quot;Incomplete Record&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Onboarding - First Agreement Greater than 90 Days</fullName>
        <actions>
            <name>Onboarding_First_Agree_than_90_days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Time based Workflow: looks at value in First agreement date on account, and if more than 90 days ago sets Onboarding status to onboarding complete.</description>
        <formula>First_Agreement_Date__c  &lt; (today()-90)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Onboarding - First Agreement less than 90 Days</fullName>
        <actions>
            <name>Update_Onboarding_First_Agreement_Less</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Time based Workflow: looks at value in First agreement date on account, and if less than 90 days ago sets Onboarding status to identified for onboarding.</description>
        <formula>First_Agreement_Date__c &gt; (today()-90) &amp;&amp; ISPICKVAL(Onboarding_Status__c,&quot;&quot;) &amp;&amp; Responsible_Partner__c = &quot;&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PAR Recruitment Status won when Contract signed is checked</fullName>
        <actions>
            <name>PAR_Recruitment_Status_won_when_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Contract_Signed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PAR Set Partner Status to Enable</fullName>
        <actions>
            <name>PAR_Set_Partner_Status_to_Enable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5</booleanFilter>
        <criteriaItems>
            <field>Account.Qonnect_PGM_Entitlements__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Training_Performed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Profile_ROP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Technical_Due_Dilligence__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Technical_Training_Performed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PAR Set Partner Status to Lost</fullName>
        <actions>
            <name>PAR_Set_Partner_Status_to_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Recruitment_Status__c</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PAR Set Partner Status to Ongoing</fullName>
        <actions>
            <name>PAR_set_rec_status_to_ongoing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Partner_Status__c</field>
            <operation>equals</operation>
            <value>Recruit</value>
        </criteriaItems>
        <description>Sets Partner status to ongoing when the recruit checkbox is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PAR Set Partner Status to Recruit</fullName>
        <actions>
            <name>PAR_Set_Partner_Status_to_Recruit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>5 AND (1 OR 2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Account.Qualify_Assign__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Signed_NDA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Recruitment_Info__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Recruitment_Status__c</field>
            <operation>equals</operation>
            <value>Ongoing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type_As_Text__c</field>
            <operation>contains</operation>
            <value>Partner Account</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PAR Set Partner Status to Terminated</fullName>
        <actions>
            <name>PAR_Set_Partner_Status_to_Terminated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Terminated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PAR Set Partner Status to contracted</fullName>
        <actions>
            <name>PAR_Set_Partner_Status_to_Contracted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9</booleanFilter>
        <criteriaItems>
            <field>Account.Contract_Signed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Qonnect_Fee_Invoiced__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Support_Certification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Handbook__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Portal_Enabled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PRM_Enabled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.QlikTech_Website_Updated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Marketing_Plan__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.X90_days_pmg_follow_up__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PAR Set Partner Status to discontinued</fullName>
        <actions>
            <name>PAR_Set_Partner_Status_to_discontinued</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Discontinued__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Q2O - Credit Status Changed</fullName>
        <actions>
            <name>Q2O_Send_email_to_Acc_Mgr_when_credit_status_changes</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( Credit_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Q2O - Realign Account Licenses</fullName>
        <actions>
            <name>Q2O_Realign_Account_Licenses</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Q2O_Reset_Realign_License_flag_at_acco</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to attach the licenses to the correct account</description>
        <formula>AND( ISCHANGED(Realign_Licenses__c),  Realign_Licenses__c = true  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Q2O Nav number Changed Realign Lics</fullName>
        <actions>
            <name>Q2O_Realign_Account_Licenses</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Realigns the Licenses when the navision customer number changes at the account</description>
        <formula>AND ( ISCHANGED(  Navision_Customer_Number__c ) , Navision_Customer_Number__c &lt;&gt; &quot;&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Academic Account Type</fullName>
        <actions>
            <name>Set_Lead_Academic_Account_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Academic Program Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Academic_Account_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates the &quot;Academic Account Type&quot; to &quot;Academic Program Account&quot; based on it being in that record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Market Name on Account</fullName>
        <actions>
            <name>Update_Billing_Country_Code_for_Sharing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Market_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Market Name on Account based on the associated Billing Country Code</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Within Nav field length limits %3D FALSE</fullName>
        <actions>
            <name>Within_Nav_field_length_limits_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Verifies integration field lengths for Navision transfer fields and updated control field</description>
        <formula>OR(  	/* Field length limitation criteria */ 	LEN( Name ) &gt; 30 ,  	LEN( BillingCity ) &gt; 30 ,  	LEN( BillingPostalCode ) &gt; 10 ,  	LEN( ShippingCity ) &gt; 30 ,  	LEN( ShippingPostalCode ) &gt; 10  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Within Nav field length limits %3D TRUE</fullName>
        <actions>
            <name>Within_Nav_field_length_limits_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Verifies integration field lengths for Navision transfer fields and updated control field</description>
        <formula>AND(  	/* Field length limitation criteria */ 	LEN( Name ) &lt;= 30 ,  	LEN( BillingCity ) &lt;= 30 ,  	LEN( BillingPostalCode ) &lt;= 10 ,  	LEN( ShippingCity ) &lt;= 30 ,  	LEN( ShippingPostalCode ) &lt;= 10  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
