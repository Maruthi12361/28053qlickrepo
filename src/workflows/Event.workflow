<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Event_Type_EV_Presales_Activity</fullName>
        <field>Type</field>
        <literalValue>EV - Presales Activity</literalValue>
        <name>Event Type EV - Presales Activity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_Role</fullName>
        <field>Original_Created_by_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Update original created by Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_function</fullName>
        <field>Original_Created_by_Function__c</field>
        <formula>IF ( INCLUDES ( $User.Function__c, &quot;Sales&quot; ), &quot;Sales; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Marketing&quot; ), &quot;Marketing; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Alliance&quot; ), &quot;Alliance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;BDR&quot; ), &quot;BDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iBDR&quot; ), &quot;iBDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ISR&quot; ), &quot;ISR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;EIS&quot; ), &quot;EIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;MIS&quot; ), &quot;MIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SMBIS&quot; ), &quot;SMBIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;PRM&quot; ), &quot;PRM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Support&quot; ), &quot;Support; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Training&quot; ), &quot;Training; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Finance&quot; ), &quot;Finance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;HR&quot; ), &quot;HR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;IT&quot; ), &quot;IT; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;R&amp;D&quot; ), &quot;R&amp;D; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iPM&quot; ), &quot;iPM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SDR - MR&quot; ), &quot;SDR - MR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;N3&quot; ), &quot;N3; &quot;,null )&amp; 
IF ( INCLUDES ( $User.Function__c, &quot;NIS&quot; ), &quot;NIS; &quot;,null )&amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ServiceSource&quot; ), &quot;ServiceSource; &quot;,null )</formula>
        <name>Update original created by function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Event presales activity</fullName>
        <actions>
            <name>Event_Type_EV_Presales_Activity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Presales_Activity__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populates EV - Presales Activity into the Type field on the Event Activity when a pick list value from the Presales_Activity__c is selected.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Event%3A Populate original created by function and role</fullName>
        <actions>
            <name>Update_original_created_by_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_original_created_by_function</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17</booleanFilter>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>ISR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>BDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iBDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>EIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Alliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>MIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>SMBIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>IT</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>R&amp;D</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iPM,SDR - MR,N3,ServiceSource,NIS</value>
        </criteriaItems>
        <description>Used to eliminate the need to create new user profiles when users change roles within SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
