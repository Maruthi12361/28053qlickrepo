<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Support_Survey_Task_Notification</fullName>
        <description>Send Support Survey Task Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/Support_Survey_Task_Notification</template>
    </alerts>
    <rules>
        <fullName>Send Support Survey Task Notification</fullName>
        <actions>
            <name>Send_Support_Survey_Task_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>A workflow sending the email template &quot;Support Survey Task Notification&quot; to the &quot;Assigned To&quot; user, when that field is changed (and not blank).</description>
        <formula>AND (     OR     (         ISNEW(),         Assigned_To__c &lt;&gt; PRIORVALUE( Assigned_To__c )             ),     NOT(ISBLANK(Assigned_To__c))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
