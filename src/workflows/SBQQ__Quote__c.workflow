<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Attunity_Quote_Approvals_open_for_over_2_hours_CPQ</fullName>
        <ccEmails>Saradha.Sriram@qlik.com</ccEmails>
        <ccEmails>Venu.Martha@qlik.com</ccEmails>
        <description>Attunity Quote Approvals open for over 2 hours CPQ</description>
        <protected>false</protected>
        <recipients>
            <recipient>cpu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cwd@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cyt@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dfc@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eog@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pld@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Pending_Approval_Request_CPQ</template>
    </alerts>
    <alerts>
        <fullName>Order_Placed_Sales_Rep</fullName>
        <description>Order Placed: Sales Rep</description>
        <protected>false</protected>
        <recipients>
            <field>SBQQ__SalesRep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Order_Placed_Sales_Rep</template>
    </alerts>
    <alerts>
        <fullName>Prepaid_or_Complimentary_Consulting_Closed</fullName>
        <description>Prepaid or Complimentary Consulting Closed</description>
        <protected>false</protected>
        <recipients>
            <recipient>cah@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SteelBrick_Email_Templates/Complimentary_or_Prepaid_Closed</template>
    </alerts>
    <alerts>
        <fullName>Quote_Approval_is_Recalled</fullName>
        <description>Quote Approval is Recalled</description>
        <protected>false</protected>
        <recipients>
            <field>SBQQ__SalesRep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Recalled_Quote</template>
    </alerts>
    <alerts>
        <fullName>Quote_Approval_is_Rejected</fullName>
        <description>Quote Approval is Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>SBQQ__SalesRep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Rejected_Quote</template>
    </alerts>
    <alerts>
        <fullName>Quote_Status_is_Approved</fullName>
        <description>Quote Status is Approved</description>
        <protected>false</protected>
        <recipients>
            <field>SBQQ__SalesRep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Approved_Quote</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Bypass_Rules</fullName>
        <field>Bypass_Rules__c</field>
        <literalValue>0</literalValue>
        <name>Clear Bypass Rules</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Maintenance_is_Co_Termed</fullName>
        <field>Maintenance_is_Co_Termed__c</field>
        <literalValue>1</literalValue>
        <name>Maintenance is Co-Termed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Partner_Special_Term_Request_to_True</fullName>
        <description>Sets Partner Special Term Request to True</description>
        <field>Partner_Special_Term_Request__c</field>
        <literalValue>1</literalValue>
        <name>Set Partner Special Term Request to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Primary_to_False_on_Eval_quote</fullName>
        <field>SBQQ__Primary__c</field>
        <literalValue>0</literalValue>
        <name>Set Primary to False on Eval quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Support_Percentage</fullName>
        <description>Update Support Percentage to 23%</description>
        <field>Maintenance_Percentage__c</field>
        <literalValue>23%</literalValue>
        <name>Support Percentage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Discount_Description</fullName>
        <field>Partner_Discount_Description__c</field>
        <formula>IF((text(Revenue_Type__c)!=&apos;Reseller&apos;&amp;&amp; text(Revenue_Type__c)!=&apos;Distributor&apos;) || ( Resell_SPD_Discount__c  = 0 &amp;&amp;  New_Logo_Incentive__c = 0 &amp;&amp;  MR_Channel_Incentive__c  = 0), &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos;,  IF(Resell_SPD_Discount__c &gt; 0 &amp;&amp;  New_Logo_Incentive__c &gt; 0 &amp;&amp;  MR_Channel_Incentive__c  &gt; 0, &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos; + &apos;, SPD &apos; + TEXT( Resell_SPD_Discount__c * 100) + &apos;%&apos;+ &apos;, NLI &apos; + TEXT( New_Logo_Incentive__c * 100) + &apos;%&apos;+ &apos;, MRCI &apos; + TEXT( MR_Channel_Incentive__c * 100) + &apos;%&apos;  ,  IF(Resell_SPD_Discount__c = 0 &amp;&amp;  New_Logo_Incentive__c &gt; 0 &amp;&amp;  MR_Channel_Incentive__c  &gt; 0, &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos;+ &apos;, NLI &apos; + TEXT( New_Logo_Incentive__c * 100) + &apos;%&apos;+ &apos;, MRCI &apos; + TEXT( MR_Channel_Incentive__c * 100) + &apos;%&apos;,  IF(Resell_SPD_Discount__c &gt; 0 &amp;&amp;  New_Logo_Incentive__c &gt; 0 &amp;&amp;  MR_Channel_Incentive__c = 0, &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos; + &apos;, SPD &apos; + TEXT( Resell_SPD_Discount__c * 100) + &apos;%&apos;+ &apos;, NLI &apos; + TEXT( New_Logo_Incentive__c * 100) + &apos;%&apos;,  IF(Resell_SPD_Discount__c &gt; 0 &amp;&amp;  New_Logo_Incentive__c = 0 &amp;&amp;  MR_Channel_Incentive__c &gt; 0, &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos; + &apos;, SPD &apos; + TEXT( Resell_SPD_Discount__c * 100) + &apos;%&apos;+ &apos;, MRCI &apos; + TEXT( MR_Channel_Incentive__c * 100) + &apos;%&apos;,  IF(Resell_SPD_Discount__c &gt; 0 &amp;&amp;  New_Logo_Incentive__c = 0 &amp;&amp;  MR_Channel_Incentive__c  = 0, &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos; + &apos;, SPD &apos; + TEXT( Resell_SPD_Discount__c * 100) + &apos;%&apos;,  IF(Resell_SPD_Discount__c = 0 &amp;&amp;  New_Logo_Incentive__c &gt; 0 &amp;&amp;  MR_Channel_Incentive__c  = 0, &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos;+ &apos;, NLI &apos; + TEXT( New_Logo_Incentive__c * 100) + &apos;%&apos;, &apos;Partner Discount &apos;+TEXT(Partner_Discount_from_Partner_Account__c * 100)+&apos;%&apos;+ &apos;, MRCI &apos; + TEXT( MR_Channel_Incentive__c * 100) + &apos;%&apos;) ) ) ) ) ) )</formula>
        <name>Update Discount Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Partner_Discount_Description</fullName>
        <field>Partner_Discount_Description__c</field>
        <formula>IF(
text(Revenue_Type__c)!=&apos;Reseller&apos;&amp;&amp; text(Revenue_Type__c)!=&apos;Distributor&apos;,
Partner_Discount_Description__c,
IF( New_Logo_Incentive__c &gt;= 0.10 &amp;&amp; CONTAINS(Partner_Discount_Description__c,&apos;NLI &apos;), 
LEFT(Partner_Discount_Description__c,LEN(Partner_Discount_Description__c)-10) + &apos;, NLI &apos; + TEXT( New_Logo_Incentive__c * 100) + &apos;%&apos;, 

IF(New_Logo_Incentive__c &lt;= 0.10 &amp;&amp; CONTAINS(Partner_Discount_Description__c,&apos;NLI &apos;), 
LEFT(Partner_Discount_Description__c,LEN(Partner_Discount_Description__c)-9) + &apos;, NLI &apos; + TEXT(New_Logo_Incentive__c  * 100) + &apos;%&apos;, 

IF(New_Logo_Incentive__c &gt;= 0.10 &amp;&amp; NOT(CONTAINS(Partner_Discount_Description__c,&apos;NLI &apos;)), 
Partner_Discount_Description__c + &apos;, NLI &apos; + TEXT( New_Logo_Incentive__c * 100) + &apos;%&apos;, 
IF(New_Logo_Incentive__c &lt; 0.10 &amp;&amp; NOT(CONTAINS(Partner_Discount_Description__c,&apos;NLI &apos;)), 
Partner_Discount_Description__c + &apos;, NLI &apos; + TEXT( New_Logo_Incentive__c * 100) + &apos;%&apos;, 
LEFT(Partner_Discount_Description__c,LEN(Partner_Discount_Description__c)-9)
)
)
) 
) 
)</formula>
        <name>Update Partner Discount Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approved and True up or Remix</fullName>
        <actions>
            <name>Set_Partner_Special_Term_Request_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>SBQQ__Quote__c.Deal_Type__c</field>
            <operation>equals</operation>
            <value>Reseller</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Remix__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.True_Up__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Checks if the Quote is Approved and either True up or Remix is checkd</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attunity Perpetual Quote Approvals open for over 2 hours</fullName>
        <active>true</active>
        <description>BSL-2875 An email notification to trigger when quotes are waiting in approval pending state for over 2 hours. This is for Attunity Quotes only.</description>
        <formula>/* Previous version
AND(
  OR(
    ISPICKVAL(SBQQ__Opportunity2__r.Product_Category__c, &apos;Cross Product&apos;),
    ISPICKVAL(SBQQ__Opportunity2__r.Product_Category__c, &apos;Data Integration&apos;)
  ),
  ISPICKVAL(ApprovalStatus__c, &apos;Pending&apos;)
)
*/

AND(
  ISPICKVAL(SBQQ__Opportunity2__r.Product_Category__c, &apos;Data Integration&apos;),
  ISPICKVAL(ApprovalStatus__c, &apos;Pending&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Attunity_Quote_Approvals_open_for_over_2_hours_CPQ</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Channel Change Reset Maintenance Co-Term</fullName>
        <actions>
            <name>Maintenance_is_Co_Termed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the revenue type or the Sell Through partner changes, this checks the maintenance is co-termed box to rerun the MaintenanceSetter flow.</description>
        <formula>ISCHANGED(Sell_Through_Partner__c) || ISCHANGED(Revenue_Type__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Maintenance Level Changed</fullName>
        <actions>
            <name>Support_Percentage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>QCW-1276: Change in maintenance level must update maintenance percentage.</description>
        <formula>AND (NOT(ISBLANK(TEXT(  Maintenance_Level__c  ))),ISCHANGED( Maintenance_Level__c ),ISPICKVAL( Maintenance_Level__c, &quot;Enterprise&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NLI Set Partner Discount</fullName>
        <actions>
            <name>Update_Partner_Discount_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Every time NLI is modified we will update the partner discount description to reflect that change.</description>
        <formula>OR(ISCHANGED( New_Logo_Incentive__c) , ISCHANGED(Partner_Discount_Description__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPD Set Partner Discount</fullName>
        <actions>
            <name>Update_Discount_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Everytime SPD or NLI or MRCI is modified we will update the partner discount description to reflect that change.</description>
        <formula>OR( ISCHANGED(Resell_SPD_Discount__c) , ISCHANGED(Partner_Discount_Description__c),ISCHANGED( New_Logo_Incentive__c ),ISCHANGED( MR_Channel_Incentive__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Primary to False on Eval quote</fullName>
        <actions>
            <name>Set_Primary_to_False_on_Eval_quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Evaluation Quote</value>
        </criteriaItems>
        <description>Evaluation quote can not be a primary quote</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SpringCM Clear Bypass Rules</fullName>
        <actions>
            <name>Clear_Bypass_Rules</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clears the Bypass Rules checkbox whenever SpringCM utilizes it to bypass validation rules.</description>
        <formula>Bypass_Rules__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
