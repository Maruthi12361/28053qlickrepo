<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Technology_Partner_referral_Approval_Rejected</fullName>
        <description>Technology Partner referral Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Technology_Partner_referral_Approval_Request_Approved_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Technology_Partner_referral_Approval_Requested</fullName>
        <ccEmails>DL-GlobalTechPartner@qlik.com</ccEmails>
        <description>Technology Partner referral Approval Requested</description>
        <protected>false</protected>
        <recipients>
            <field>Approval_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Technology_Partner_referral_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Technology_Partner_referral_Approved</fullName>
        <description>Technology Partner referral Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Technology_Partner_referral_Approval_Request_Approved_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Technology_Partner_referral_Approved_Rejected</fullName>
        <ccEmails>DL-GlobalTechPartner@qlik.com</ccEmails>
        <description>Technology Partner referral Approved/Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Technology_Partner_referral_Approval_Request_Approved_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Approval_Date</fullName>
        <field>Approval_Date__c</field>
        <name>Clear Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approval_Status</fullName>
        <field>Approval_Status__c</field>
        <name>Clear Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_Approval_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Reject Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Date</fullName>
        <field>Approval_Date__c</field>
        <formula>Today()</formula>
        <name>Update Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Date_Final</fullName>
        <field>Approval_Date__c</field>
        <formula>Today()</formula>
        <name>Update Approval Date Final</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Requested</literalValue>
        <name>Update Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Final</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status Final</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Owner_Email</fullName>
        <field>Approval_email__c</field>
        <formula>Opportunity__r.Owner.Approval_Manager__r.Approval_Manager__r.Email</formula>
        <name>Update Opportunity Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
