<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Exclude_from_project_planner</fullName>
        <description>IT-2908:</description>
        <field>pse__Exclude_from_Project_Planner__c</field>
        <literalValue>1</literalValue>
        <name>Exclude from project planner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSA_Set_Include_in_Forecasting</fullName>
        <description>Sets the Include in Forecasting field to TRUE</description>
        <field>pse__Include_In_Forecasting__c</field>
        <literalValue>1</literalValue>
        <name>PSA Set Include in Forecasting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Project_Mark_Inactive</fullName>
        <description>Unselect &apos;Active&apos;.</description>
        <field>pse__Is_Active__c</field>
        <literalValue>0</literalValue>
        <name>PSE Project Mark Inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Project_Set_Close_for_Expense_Entry</fullName>
        <field>pse__Closed_for_Expense_Entry__c</field>
        <literalValue>1</literalValue>
        <name>PSE Project Set Close for Expense Entry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Project_Set_Close_for_Time_Entry</fullName>
        <field>pse__Closed_for_Time_Entry__c</field>
        <literalValue>1</literalValue>
        <name>PSE Project Set Close for Time Entry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PSA Set Include in Forecasting</fullName>
        <actions>
            <name>PSA_Set_Include_in_Forecasting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the &apos;Include in Forecasting&apos; checkbox to true if a Forecast curve is set on the project.</description>
        <formula>NOT(ISBLANK(pse__Forecast_Curve__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PSE Project Closed</fullName>
        <actions>
            <name>Exclude_from_project_planner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Project_Mark_Inactive</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Project_Set_Close_for_Expense_Entry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Project_Set_Close_for_Time_Entry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Proj__c.pse__Stage__c</field>
            <operation>equals</operation>
            <value>Cancelled,Completed</value>
        </criteriaItems>
        <description>When a project is Cancelled or Completed, mark as closed for time entry, project no longer active.  All assignments marked as closed via trigger.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSE Project Is Not Active</fullName>
        <actions>
            <name>PSE_Project_Set_Close_for_Expense_Entry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Project_Set_Close_for_Time_Entry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Proj__c.pse__Is_Active__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow will close the project for time and expense when the active check box is uncheck (false)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
