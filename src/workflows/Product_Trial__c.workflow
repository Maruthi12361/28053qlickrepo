<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_End_Date</fullName>
        <description>End Date will be pushed out by x no of Days.  End date is decided by the custom label Product_Trial_Extension_Days. End Date itself does not count in the x no of Days.</description>
        <field>End_Date__c</field>
        <formula>End_Date__c +  VALUE($Label.Product_Trial_Extension_Days)</formula>
        <name>Update End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Trial_Status_to_Expired</fullName>
        <description>When End Date + 1 day is reached on a Trial the Trial Status is changed to Expired</description>
        <field>Trial_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update Trial Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Expire Trial</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Product_Trial__c.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Started,Extended</value>
        </criteriaItems>
        <description>When end date + 1 day is reached the field Trial Status needs to be set to Expired</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Trial_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Product_Trial__c.End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
