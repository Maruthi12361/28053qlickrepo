<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PSA_Unset_Include_in_Status_Report</fullName>
        <field>Include_in_Status_Report__c</field>
        <literalValue>0</literalValue>
        <name>PSA Unset Include in Status Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PSA Unset Include in Status Report</fullName>
        <actions>
            <name>PSA_Unset_Include_in_Status_Report</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Issue__c.pse__Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Unsets the Include in Status report field when the Issue status goes to completed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
