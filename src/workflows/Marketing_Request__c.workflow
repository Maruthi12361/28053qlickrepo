<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Data_List_Pull_Needed_True_And_Region_Is_Americas</fullName>
        <description>Data List Pull Needed True And Region Is Americas</description>
        <protected>false</protected>
        <recipients>
            <recipient>ary@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cck@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Data_List_Pull_Requested</template>
    </alerts>
    <alerts>
        <fullName>Data_List_Pull_Needed_True_And_Region_Is_Not_Americas</fullName>
        <ccEmails>MarketoSupport@qlik.com</ccEmails>
        <description>Data List Pull Needed True And Region Is Not Americas</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Data_List_Pull_Requested</template>
    </alerts>
    <alerts>
        <fullName>New_Service_Ops_Request_Alert</fullName>
        <description>New Service Ops Request Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>rjn@qlikview.com-retired</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>usn@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~dge-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~ldl-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>QSM_Email/New_Service_Op_Request</template>
    </alerts>
    <alerts>
        <fullName>Notify_Web_Team_OR_Web_Request_status_change</fullName>
        <description>Notify Web Team OR Web Request status change</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ORWebRequestStatusChange</template>
    </alerts>
    <alerts>
        <fullName>Notify_requestor_OR_Web_Request_status_change</fullName>
        <description>Notify requestor OR Web Request status change</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ORWebRequestStatusChange</template>
    </alerts>
    <alerts>
        <fullName>Notify_requestor_OR_completed</fullName>
        <description>Notify requestor OR completed</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Operations_Request_Templates/ORCompletion</template>
    </alerts>
    <alerts>
        <fullName>Notify_requestor_OR_created</fullName>
        <description>Notify requestor OR created</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Operations_Request_Templates/NewORAcknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Notify_requestor_OR_rejected</fullName>
        <description>Notify requestor OR rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Operations_Request_Templates/ORRejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_requestor_OR_status_change</fullName>
        <description>Notify requestor OR status change</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Operations_Request_Templates/ORStatusChange</template>
    </alerts>
    <alerts>
        <fullName>OR_ESR_Creation_confirmation_email_to_Requestor</fullName>
        <description>OR_ESR: Creation confirmation email to Requestor</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/OR_ESR_Confirmation_to_Requestor</template>
    </alerts>
    <alerts>
        <fullName>OR_ESR_Request_Complete_Email_to_Requestor</fullName>
        <description>OR_ESR: Request Complete Email to Requestor</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/OR_ESR_Request_Complete_Email_to_Requestor</template>
    </alerts>
    <alerts>
        <fullName>OR_ESR_Status_Email_to_Requestor</fullName>
        <description>OR_ESR: Status Email to Requestor</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/OR_ESR_Status_Email_to_Requestor</template>
    </alerts>
    <alerts>
        <fullName>OR_ESR_When_an_Education_Service_Request_has_been_saved</fullName>
        <description>OR_ESR: When an Education Service Request has been saved</description>
        <protected>false</protected>
        <recipients>
            <recipient>usn11@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~dge-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~ldl-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/OR_ESR_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_For_Added_Attachment</fullName>
        <description>Send Email For Added Attachment</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Marketo_Support_Request_for_Added_Attachment</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_For_Added_Marketing_Deliverable</fullName>
        <description>Send Email For Added Marketing Deliverable</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Marketo_Support_Request_for_Added_Marketing_Deliverable</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_For_Added_Note</fullName>
        <description>Send Email For Added Note</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Marketo_Support_Request_for_Added_Note</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_For_New_Marketo_Support_Request</fullName>
        <description>Send Email For New Marketo Support Request</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/New_Marketo_Support_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_For_OR_Changes</fullName>
        <description>Send Email For OR Changes</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Marketo_Support_Request_For_OR_Changes</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_For_OR_Changes_2</fullName>
        <description>Send Email For OR Changes 2</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Marketo_Support_Request_For_OR_Changes_2</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Approval_Status_Changes</fullName>
        <description>Send Email When Approval Status Changes</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Operations_Request_Templates/Marketo_Support_Request_Status_Change</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Send_Attachment_Email_field</fullName>
        <description>Clear Send Attachment Email field</description>
        <field>Send_Attachment_Email__c</field>
        <literalValue>0</literalValue>
        <name>Clear Send Attachment Email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Send_Marketing_Deliverable_Email_f</fullName>
        <description>Clear Send Marketing Deliverable Email field</description>
        <field>Send_Marketing_Deliverable_Email__c</field>
        <literalValue>0</literalValue>
        <name>Clear Send Marketing Deliverable Email f</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Send_Note_Email_field</fullName>
        <description>Clear Send Note Email field</description>
        <field>Send_Note_Email__c</field>
        <literalValue>0</literalValue>
        <name>Clear Send Note Email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Send_OR_Change_Email_field</fullName>
        <description>Clear Send OR Change Email field</description>
        <field>Send_OR_Change_Email__c</field>
        <literalValue>0</literalValue>
        <name>Clear Send OR Change Email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Send_OR_Change_Email_field_2</fullName>
        <description>Clear Send OR Change Email field 2</description>
        <field>Send_OR_Change_Email_2__c</field>
        <literalValue>0</literalValue>
        <name>Clear Send OR Change Email field 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Closed_when_Completed</fullName>
        <field>Date_Closed__c</field>
        <formula>Today()</formula>
        <name>Date Closed when Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OR_Approval_Status_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>OR Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OR_Approval_Status_Approved_Go_Live</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved for Go Live</literalValue>
        <name>OR Approval Status Approved Go Live</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OR_Approval_Status_Denied</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Denied</literalValue>
        <name>OR Approval Status Denied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OR_Approval_Status_Denied_Go_Live</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Denied for Go Live</literalValue>
        <name>OR Approval Status Denied Go Live</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OR_Approval_Status_Pending_Go_Live</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending Go Live Approval</literalValue>
        <name>OR Approval Status Pending Go Live</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OR_Approval_Status_Submitted</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>OR Approval Status Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>Approval_Status_QSM__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Send_OR_Change_Email</fullName>
        <field>Send_OR_Change_Email__c</field>
        <literalValue>1</literalValue>
        <name>Set Send OR Change Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Send_OR_Change_Email_2</fullName>
        <field>Send_OR_Change_Email_2__c</field>
        <literalValue>1</literalValue>
        <name>Set Send OR Change Email 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Change_Date</fullName>
        <description>Update Approval Status Change Date</description>
        <field>Approval_Status_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Approval Status Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Approval_Status</fullName>
        <description>Update Previous Approval Status</description>
        <field>Previous_Approval_Status__c</field>
        <formula>TEXT(PRIORVALUE(Approval_Status__c))</formula>
        <name>Update Previous Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Assigned_To_User</fullName>
        <field>Previous_Assigned_To_User__c</field>
        <formula>(PRIORVALUE(Assigned_To_User__c))</formula>
        <name>Update Previous Assigned To User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Assigned_To_User_Text</fullName>
        <field>Previous_Assigned_To_User_Text__c</field>
        <formula>PRIORVALUE( Assigned_To_User_Text__c )</formula>
        <name>Update Previous Assigned To User Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Campaign_Launch_Date</fullName>
        <field>Previous_Campaign_Launch_Date__c</field>
        <formula>PRIORVALUE( Expected_Campaign_Launch_Date__c )</formula>
        <name>Update Previous Campaign Launch Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Campaign_type</fullName>
        <field>Previous_Campaign_type__c</field>
        <formula>TEXT(PRIORVALUE( Campaign_type__c ))</formula>
        <name>Update Previous Campaign type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Project_Description</fullName>
        <field>Previous_Project_Description__c</field>
        <formula>(PRIORVALUE( Project_Description__c ))</formula>
        <name>Update Previous Project Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Requestor</fullName>
        <field>Previous_Requestor__c</field>
        <formula>(PRIORVALUE( Requestor__c ))</formula>
        <name>Update Previous Requestor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Requestor_Text</fullName>
        <field>Previous_Requestor_Text__c</field>
        <formula>(PRIORVALUE( Requestor_Text__c ))</formula>
        <name>Update Previous Requestor Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Start_Date</fullName>
        <description>Update Previous Start Date</description>
        <field>Previous_Start_Date__c</field>
        <formula>PRIORVALUE(Start_Date__c)</formula>
        <name>Update Previous Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Target_Completion_Date</fullName>
        <field>Previous_Target_Completion_Date__c</field>
        <formula>PRIORVALUE( Target_Completion_Date__c )</formula>
        <name>Update Previous Target Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Type</fullName>
        <field>Previous_Type__c</field>
        <formula>TEXT(PRIORVALUE( Type__c ))</formula>
        <name>Update Previous Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>NewORAcknowledgement</fullName>
        <actions>
            <name>Notify_requestor_OR_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.OwnerId</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Services Request</value>
        </criteriaItems>
        <description>Notify request that OR has been created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OR%3A New Service Op Request</fullName>
        <actions>
            <name>New_Service_Ops_Request_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Services Request</value>
        </criteriaItems>
        <description>Email alert notifying Services Group that there is a new Service Op Request</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ORCompletion</fullName>
        <actions>
            <name>Notify_requestor_OR_completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Approval_Status_QSM__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Services Request</value>
        </criteriaItems>
        <description>Notify requestor that OR has been completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ORRejection</fullName>
        <actions>
            <name>Notify_requestor_OR_rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Approval_Status_QSM__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Services Request</value>
        </criteriaItems>
        <description>Notify requestor that OR has been rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ORStatusChange</fullName>
        <actions>
            <name>Notify_requestor_OR_status_change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify requestor OR status change</description>
        <formula>ISCHANGED(Approval_Status_QSM__c ) &amp;&amp; (NOT OR( ISPICKVAL(Approval_Status_QSM__c, &quot;Rejected&quot;),  ISPICKVAL(Approval_Status_QSM__c, &quot;Completed&quot;) )) &amp;&amp;   OR (RecordTypeId = &apos;012D0000000K9LoIAK&apos;, RecordTypeId = &apos;012D0000000K9Lo&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ORWebRequestCompleted</fullName>
        <actions>
            <name>Date_Closed_when_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Web Request</value>
        </criteriaItems>
        <description>Sets Date Closed field to current date if Approval status is changed to Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ORWebRequestStatusChange</fullName>
        <actions>
            <name>Notify_requestor_OR_Web_Request_status_change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Upon each change of Web Request status the requester is notified</description>
        <formula>ISCHANGED(Approval_Status__c ) &amp;&amp; RecordTypeId = &apos;012D0000000KEZf&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OR_ESR%3A ESR Confirmation to Requestor</fullName>
        <actions>
            <name>OR_ESR_Creation_confirmation_email_to_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Education Request</value>
        </criteriaItems>
        <description>Creation confirmation email to Requestor</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OR_ESR%3A ESR Status Email to Requestor</fullName>
        <actions>
            <name>OR_ESR_Status_Email_to_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status Email to Requestor</description>
        <formula>AND(  OR( AND(ISCHANGED(Approval_Status_QSM__c),  ISPICKVAL(Approval_Status_QSM__c, &quot;In Progress&quot;)),  AND(ISCHANGED(Approval_Status_QSM__c),  ISPICKVAL(Approval_Status_QSM__c, &quot;Rejected&quot;)), AND(ISCHANGED(Approval_Status_QSM__c),  ISPICKVAL(Approval_Status_QSM__c, &quot;On Hold&quot;)) ) ,RecordTypeId = &quot;012D0000000KAoZ&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OR_ESR%3A ESR Submitted</fullName>
        <actions>
            <name>OR_ESR_When_an_Education_Service_Request_has_been_saved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Education Request</value>
        </criteriaItems>
        <description>When an Education Service Request has been saved</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OR_ESR%3A Request Complete Email to Requestor</fullName>
        <actions>
            <name>OR_ESR_Request_Complete_Email_to_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Request Complete Email to Requestor</description>
        <formula>AND( AND(ISCHANGED(Approval_Status_QSM__c),  ISPICKVAL(Approval_Status_QSM__c, &quot;Completed&quot;)), RecordTypeId = &quot;012D0000000KAoZ&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email For New Marketo Support Request</fullName>
        <actions>
            <name>Send_Email_For_New_Marketo_Support_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketo Support Requests</value>
        </criteriaItems>
        <description>Send Email For New Marketo Support Request.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email For OR Changes</fullName>
        <actions>
            <name>Send_Email_For_OR_Changes</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Clear_Send_OR_Change_Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send email when certain values change for Marketo Support Request.</description>
        <formula>AND (   ISCHANGED(Send_OR_Change_Email__c),   Send_OR_Change_Email__c = True,   LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email For OR Changes 2</fullName>
        <actions>
            <name>Send_Email_For_OR_Changes_2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Clear_Send_OR_Change_Email_field_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send email when certain values change for Marketo Support Request.</description>
        <formula>AND (   ISCHANGED(Send_OR_Change_Email_2__c),   Send_OR_Change_Email_2__c = True,   LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email When Approval Status Changes</fullName>
        <actions>
            <name>Send_Email_When_Approval_Status_Changes</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send email when Approval Status changes for Marketo Support Request.</description>
        <formula>AND(     ISCHANGED( Approval_Status__c ),     LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email When Data List Pull Needed Changes And Region Is Americas</fullName>
        <actions>
            <name>Data_List_Pull_Needed_True_And_Region_Is_Americas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email when Data List Pull Needed changes for Marketo Support Request and Region is Americas.</description>
        <formula>AND(     ISCHANGED(Data_List_Pull_Needed__c),     Data_List_Pull_Needed__c == true,     ISPICKVAL(Region__c , &apos;Americas&apos;),     LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email When Data List Pull Needed Changes Not Americas</fullName>
        <actions>
            <name>Data_List_Pull_Needed_True_And_Region_Is_Not_Americas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email when Data List Pull Needed changes for Marketo Support Request and Region is not Americas.</description>
        <formula>AND(     ISCHANGED(Data_List_Pull_Needed__c),     Data_List_Pull_Needed__c == true,     NOT(ISPICKVAL(Region__c , &apos;Americas&apos;)),     LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email When Send Attachment Email Goes True</fullName>
        <actions>
            <name>Send_Email_For_Added_Attachment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Clear_Send_Attachment_Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send email when Send Attachment Email value change to True for Marketo Support Request.</description>
        <formula>AND (     ISCHANGED( Send_Attachment_Email__c),     Send_Attachment_Email__c = True,     LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email When Send Marketing Deliverable Email Goes True</fullName>
        <actions>
            <name>Send_Email_For_Added_Marketing_Deliverable</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Clear_Send_Marketing_Deliverable_Email_f</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send email when Marketing Deliverable Email value change to True for Marketo Support Request.</description>
        <formula>AND (     ISCHANGED(Send_Marketing_Deliverable_Email__c),     Send_Marketing_Deliverable_Email__c = True,     LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email When Send Note Email Goes True</fullName>
        <actions>
            <name>Send_Email_For_Added_Note</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Clear_Send_Note_Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send email when Send Note Email value change to True for Marketo Support Request.</description>
        <formula>AND (     ISCHANGED( Send_Note_Email__c),     Send_Note_Email__c = True,     LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Approval Status Change Date</fullName>
        <actions>
            <name>Update_Approval_Status_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Approval Status Change Date</description>
        <formula>OR (     ISCHANGED(Approval_Status__c),     ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Previous Approval Status</fullName>
        <actions>
            <name>Update_Previous_Approval_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Previous Approval Status</description>
        <formula>ISCHANGED(Approval_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Previous For OR Changes</fullName>
        <actions>
            <name>Set_Send_OR_Change_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Approval_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Assigned_To_User</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Assigned_To_User_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Campaign_Launch_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Target_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Previous For OR Changes</description>
        <formula>AND (   OR   (     ISCHANGED( Approval_Status__c ),     ISCHANGED( Start_Date__c ),     ISCHANGED( Assigned_To_User__c ),     ISCHANGED( Expected_Campaign_Launch_Date__c ),     ISCHANGED( Target_Completion_Date__c ),     ISCHANGED( Assigned_To_User_Text__c )   ),   LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Previous For OR Changes 2</fullName>
        <actions>
            <name>Set_Send_OR_Change_Email_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Campaign_type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Project_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Requestor</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Requestor_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Previous For OR Changes 2</description>
        <formula>AND (   OR   (     ISCHANGED( Requestor__c ),     ISCHANGED( Requestor_Text__c ),     ISCHANGED( Type__c ),     ISCHANGED( Project_Description__c ),     ISCHANGED( Campaign_type__c )    ),   LEFT(RecordTypeId, 15) == &apos;012D0000000KJMD&apos; /*Marketo Support Request*/ )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
