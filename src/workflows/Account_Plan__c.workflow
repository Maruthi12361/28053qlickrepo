<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Plan_Alert_new_Reviewer</fullName>
        <description>Account Plan: Alert new Reviewer</description>
        <protected>false</protected>
        <recipients>
            <field>Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Account_Plan_Emails/Alert_new_Account_plan_Reviewer</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_By</fullName>
        <description>Update the field with the user name of the user that checked the box &apos;Approved by Sales Productivity&apos;</description>
        <field>Approved_By__c</field>
        <formula>$User.Username</formula>
        <name>Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Date_Time</fullName>
        <description>Sets the date/time when the checkbox &apos;Approved By Sales Productivity&apos; is checked</description>
        <field>Approved_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Approved Date /Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Creation_date_on_Account_Plan</fullName>
        <field>Creation_Date__c</field>
        <formula>Today()</formula>
        <name>Set Creation date on Account Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_By_Sales_Prod_Date</fullName>
        <description>Update Date field when Approved by Sales Productivity is checked</description>
        <field>Approved_by_Sales_Productivity_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Approved By Sales Prod Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Review_Date</fullName>
        <description>Updated when &quot;Reviewer&quot; is updated</description>
        <field>Last_Review_Date__c</field>
        <formula>NOW()</formula>
        <name>Update &quot;Last Review Date&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Acc Plan - Approved By Sales Productivity</fullName>
        <actions>
            <name>Approved_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Approved_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Plan__c.Approved_by_Sales_Productivity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets the Approved By User and the Approved Date/Time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Plan%3A Alert new Account Plan Reviewer</fullName>
        <actions>
            <name>Account_Plan_Alert_new_Reviewer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Reviewer notification when reviewer is added to an Account Plan</description>
        <formula>OR(ISNEW(), ISCHANGED( Reviewer__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Plan%3A Approved By Sales Productivity Date</fullName>
        <actions>
            <name>Update_Approved_By_Sales_Prod_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Plan__c.Approved_by_Sales_Productivity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Set the Data field when &apos;Approved by Sales Productivity&apos; checkbox is checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Creation Date On Account Plan</fullName>
        <actions>
            <name>Set_Creation_date_on_Account_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Plan__c.Creation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update field %22Last Review Date%22</fullName>
        <actions>
            <name>Update_Last_Review_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updated when &quot;Reviewer&quot; is updated</description>
        <formula>ISCHANGED( Reviewer__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
