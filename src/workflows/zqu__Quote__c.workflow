<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Attunity_Quote_Approvals_open_for_over_2_hours</fullName>
        <ccEmails>Saradha.Sriram@qlik.com</ccEmails>
        <ccEmails>Venu.Martha@qlik.com</ccEmails>
        <description>Attunity Quote Approvals open for over 2 hours</description>
        <protected>false</protected>
        <recipients>
            <recipient>cpu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cwd@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cyt@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dfc@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eog@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pld@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_Pending_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>QDI_Email_Survey</fullName>
        <description>QDI Email Survey</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SteelBrick_Email_Templates/QDI_Survey</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_CQMT_To_1</fullName>
        <description>Set the Calculate Quote Metrics Through value to 1</description>
        <field>zqu__Calculate_Quote_Metrics_Through__c</field>
        <formula>&quot;1&quot;</formula>
        <name>Set CQMT To 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CQMT_To_EOT</fullName>
        <description>Set Calculate Quote Metric Through to EOT (End Of Term)</description>
        <field>zqu__Calculate_Quote_Metrics_Through__c</field>
        <formula>&quot;EOT&quot;</formula>
        <name>Set CQMT To EOT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_USD_Amount_to_ACV_USD</fullName>
        <field>ACV_USD__c</field>
        <formula>IF(OR(zqu__Previewed_SubTotal__c != 0, zqu__Previewed_Delta_TCV__c != 0 ),
ACV__c / CURRENCYRATE(TEXT(zqu__Currency__c)), 0)</formula>
        <name>Set USD Amount to ACV_USD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Charge_Expired</fullName>
        <field>zqu__Is_Charge_Expired__c</field>
        <literalValue>0</literalValue>
        <name>Update Is Charge Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NLI_Discount</fullName>
        <description>BSL-2274</description>
        <field>NLI_Discount__c</field>
        <formula>IF(NLI_Eligibility__c,
	IF(Revenue_Type__c == &quot;Reseller&quot;,
		CASE(Partner_Level__c,&quot;Select&quot;, 0.1, &quot;Elite&quot;, 0.15, 0),
	IF(Revenue_Type__c == &quot;Distributor&quot;,
		CASE(Second_Partner_Level__c,&quot;Select&quot;, 0.1, &quot;Elite&quot;, 0.15, 0),
	0)
	),
0)</formula>
        <name>Update NLI Discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Prorated_Term_GT12_Field</fullName>
        <field>Prorated_Term_GT12__c</field>
        <literalValue>1</literalValue>
        <name>Update Prorated Term GT12 Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Z_UpdateTargetDate</fullName>
        <field>zqu__InvoiceTargetDate__c</field>
        <formula>IF(ISPICKVAL(Billing_Frequency__c , &quot;Subscription Term&quot;),
null,

IF(zqu__SubscriptionTermStartDate__c &lt; zqu__StartDate__c, 
zqu__StartDate__c, zqu__SubscriptionTermStartDate__c
)
)</formula>
        <name>Z_UpdateTargetDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>zUpdate_Quote_Name</fullName>
        <field>Name</field>
        <formula>zqu__Number__c</formula>
        <name>Update Quote Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>zqu__UpdateSubTermDefault</fullName>
        <field>zqu__Subscription_Term_Type__c</field>
        <literalValue>Termed</literalValue>
        <name>UpdateSubTermDefault</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>zqu__ZQUpdateTermData_AutoRenew</fullName>
        <field>zqu__AutoRenew__c</field>
        <literalValue>0</literalValue>
        <name>ZQUpdateTermData_AutoRenew</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>zqu__ZQUpdateTermData_InitialTerm</fullName>
        <field>zqu__InitialTerm__c</field>
        <formula>0</formula>
        <name>ZQUpdateTermData_InitialTerm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>zqu__ZQUpdateTermData_RenewalTerm</fullName>
        <field>zqu__RenewalTerm__c</field>
        <formula>0</formula>
        <name>ZQUpdateTermData_RenewalTerm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Attunity Quote Approvals open for over 2 hours</fullName>
        <active>true</active>
        <description>BSL-2875 An email notification to trigger when quotes are waiting in approval pending state for over 2 hours. This is for Attunity Quotes only.</description>
        <formula>AND(
  ISPICKVAL(ApprovalStatus__c , &apos;Pending&apos;),
  Attunity_Products__c &gt; 0
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Attunity_Quote_Approvals_open_for_over_2_hours</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Billing Frequency NOT Prepaid Flow</fullName>
        <actions>
            <name>Set_CQMT_To_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>zqu__Quote__c.Billing_Frequency__c</field>
            <operation>notEqual</operation>
            <value>Subscription Term</value>
        </criteriaItems>
        <criteriaItems>
            <field>zqu__Quote__c.zqu__InitialTerm__c</field>
            <operation>greaterOrEqual</operation>
            <value>12</value>
        </criteriaItems>
        <criteriaItems>
            <field>zqu__Quote__c.zqu__SubscriptionType__c</field>
            <operation>notEqual</operation>
            <value>Renew Subscription</value>
        </criteriaItems>
        <description>If Billing Frequency is NOT Prepaid and the Initial Term is &gt;= 12, then the Calculate Quote Metrics Through is set to 1. IF Initial Term is &lt;12 Zuora will still calculate the metrics for 1 year which is inaccurate.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Billing Frequency Prepaid Flow</fullName>
        <actions>
            <name>Set_CQMT_To_EOT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>zqu__Quote__c.Billing_Frequency__c</field>
            <operation>equals</operation>
            <value>Subscription Term</value>
        </criteriaItems>
        <criteriaItems>
            <field>zqu__Quote__c.Billing_Frequency__c</field>
            <operation>notEqual</operation>
            <value>Subscription Term</value>
        </criteriaItems>
        <criteriaItems>
            <field>zqu__Quote__c.zqu__InitialTerm__c</field>
            <operation>lessThan</operation>
            <value>12</value>
        </criteriaItems>
        <description>If Billing Frequency is Prepaid, then the Calculate Quote Metrics Through is set to EOT (End Of Term). If Initial Term is &lt;12 &amp; Billing Frequency is Annual, this will allow the metrics to calculate for the specific amount of time left in the term.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Prorated Term is %3E%3D 12</fullName>
        <actions>
            <name>Update_Prorated_Term_GT12_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using this WFR to reduce Compilation Errors from ACV, QlikTCV, Annual Sub Fees and Initial Sub Fees Fields</description>
        <formula>Prorated_Term__c &gt;= 12</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QDI Survey Email Alert</fullName>
        <actions>
            <name>QDI_Email_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>zqu__Quote__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>zqu__Quote__c.QWC_or_QDM_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>To be sent to Quote Owners once a QDI Quote is in the Advanced Approvals Status of Approved.  This will enable the users to give feedback on multiple quotes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set USD ACV</fullName>
        <actions>
            <name>Set_USD_Amount_to_ACV_USD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISCHANGED( zqu__Previewed_SubTotal__c ), ISCHANGED( zqu__Previewed_Delta_TCV__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Charge Expired Field To False</fullName>
        <actions>
            <name>Update_Is_Charge_Expired</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>zqu__Quote__c.zqu__Status__c</field>
            <operation>equals</operation>
            <value>Sent to Z-Billing</value>
        </criteriaItems>
        <description>This will ensure that when Quotes are sent to z-Billing users will not see the &quot;Child Quote or Parent Quote or Readonly quote cannot be previewed&quot; error.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update NLI Discount</fullName>
        <actions>
            <name>Update_NLI_Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>BSL-2274 - populate NLI discount based on partner levels</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Quote Name</fullName>
        <actions>
            <name>zUpdate_Quote_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>zqu__Number__c &lt;&gt;  Name</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Z_AlignTargetDate</fullName>
        <actions>
            <name>Z_UpdateTargetDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Billing_Frequency__c , &quot;Subscription Term&quot;) || 
        zqu__StartDate__c &lt;&gt;  zqu__InvoiceTargetDate__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>zqu__ZQSubTermTypeAction</fullName>
        <actions>
            <name>zqu__ZQUpdateTermData_AutoRenew</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>zqu__ZQUpdateTermData_InitialTerm</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>zqu__ZQUpdateTermData_RenewalTerm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>zqu__Quote__c.zqu__Subscription_Term_Type__c</field>
            <operation>equals</operation>
            <value>Evergreen</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>zqu__ZQSubTermTypeDefaultAction</fullName>
        <actions>
            <name>zqu__UpdateSubTermDefault</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>zqu__Quote__c.zqu__Subscription_Term_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update the Subscription Term Type to Termed, if it is left blank.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>