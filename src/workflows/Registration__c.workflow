<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Synqronize2013RegisterNotify</fullName>
        <ccEmails>harmonize@qlikview.com</ccEmails>
        <description>Harmonize 2014 Registration Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Eventforce/SynqronizeRegisterNotify</template>
    </alerts>
    <rules>
        <fullName>Harmonize 2014 Registration Notification</fullName>
        <actions>
            <name>Synqronize2013RegisterNotify</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Registration__c.Event_ID_text__c</field>
            <operation>equals</operation>
            <value>a0iD0000005WYtz</value>
        </criteriaItems>
        <description>For sending a notification about a registration to the managing entity</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Synqronize 2013 Registration Notification</fullName>
        <actions>
            <name>Synqronize2013RegisterNotify</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Registration__c.Event_ID_text__c</field>
            <operation>equals</operation>
            <value>a0iD0000002jgA5</value>
        </criteriaItems>
        <description>For sending a notification about a registration to the managing entity</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
