<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Designation_Status</fullName>
        <field>Designation_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Designation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Designation_Status_to_Approved</fullName>
        <field>Designation_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Designation Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Designation_Status_to_Expired</fullName>
        <field>Designation_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update Designation Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Revert Designation Status to Approved</fullName>
        <actions>
            <name>Update_Designation_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>BSL-888</description>
        <formula>AND(ISPICKVAL(Designation_Status__c, &apos;Expired&apos;),  ISCHANGED( Expiration_Date__c ) , Expiration_Date__c &gt; TODAY())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Designation Status to Expired</fullName>
        <active>false</active>
        <description>BSL-888</description>
        <formula>AND(NOT( ISBLANK(Expiration_Date__c) ),Expiration_Date__c&gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Designation_Status_to_Approved</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Specialization__c.Expiration_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
