<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Escalation_Rejected</fullName>
        <description>Account Escalation Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Submitter_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Account_Escalation_Rejected</template>
    </alerts>
    <rules>
        <fullName>Account Escalation Rejected</fullName>
        <actions>
            <name>Account_Escalation_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>At_risk_escalation__c.Rejected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
