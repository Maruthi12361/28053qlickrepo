<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Status_Change_Date</fullName>
        <description>Triggers when picklist Status is updated on Product User</description>
        <field>Status_Change_Date__c</field>
        <formula>Today()</formula>
        <name>Update Status Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Status Change Date</fullName>
        <actions>
            <name>Update_Status_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Status picklist is changed, the field &quot;Status Change Date&quot; is updated</description>
        <formula>And( ISCHANGED(  Status__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
