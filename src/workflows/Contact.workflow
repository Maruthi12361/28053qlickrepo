<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CONSendusernamepasswordtoContactwhenULCstatusissettoResendPassword</fullName>
        <description>CON: Send username/password to Contact when ULC status is set to Resend Password</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/ULC_Resend_Password</template>
    </alerts>
    <alerts>
        <fullName>Contact_Send_Onboarding_Survey</fullName>
        <description>Contact - Send Onboarding Survey</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/Support_Onboarding_Workflow_Emails</template>
    </alerts>
    <alerts>
        <fullName>PORTALSendmailtoOwner</fullName>
        <description>PORTAL Send mail to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Portal_E_Mail_Templates/PORTAL_Update_Owner_when_status_changes</template>
    </alerts>
    <alerts>
        <fullName>PORTALtriggerfromContact</fullName>
        <ccEmails>downloads2@qliktech.com</ccEmails>
        <description>PORTAL trigger from Contact</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ULC_Trigger_Mails/Trigger_for_ULC_to_control_access_to_Portals</template>
    </alerts>
    <alerts>
        <fullName>Send_Account_Verification_Notification_Contact</fullName>
        <description>Send Account Verification Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ULC_Templates/New_User_On_Validation_Account</template>
    </alerts>
    <fieldUpdates>
        <fullName>CONSetULCStatustoActive</fullName>
        <field>ULC_Status__c</field>
        <literalValue>Active</literalValue>
        <name>CON: Set ULC Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CON_Update_Title_from_Job_Title</fullName>
        <description>Used to ensure that the standard Title in contact always updated from Job Title value, this is used for the OrgChartPlus</description>
        <field>Title</field>
        <formula>Job_Title__c</formula>
        <name>CON:Update Title from Job Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CULCDisableULCwhenContactleftComp</fullName>
        <description>Sets the ULC Status to Disabled - Left Company when a contact is flagged as left company</description>
        <field>ULC_Status__c</field>
        <literalValue>Disabled - Left Company</literalValue>
        <name>CULC: Disable ULC when Contact left Comp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Renewal_Date_60days_field</fullName>
        <description>Check Renewal Date less than 60days field</description>
        <field>Renewal_Date_less_than_60_Days__c</field>
        <literalValue>1</literalValue>
        <name>Check Renewal Date &lt; 60days field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Date_Of_Count_Update</fullName>
        <field>DateOfCountUpdate__c</field>
        <name>Clear Date Of Count Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Date_Of_Email_Count_Update</fullName>
        <field>DateOfEmailCountUpdate__c</field>
        <name>Clear Date Of Email Count Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Event_Follow_Up_Required_End_Date</fullName>
        <description>this ensures that when someone ends an event follow up and then needs to restart an event follow up that the end date will clear, starting the duration over.</description>
        <field>Event_Follow_Up_Completed_Date__c</field>
        <name>Clear Event: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Existing_Follow_Up_Req_End_Date</fullName>
        <field>Existing_Follow_Up_Completed_Date__c</field>
        <name>Clear Existing: Follow-Up Req End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Follow_Up_Disqualified_Reason</fullName>
        <description>Clear Follow-Up Disqualified Reason field</description>
        <field>Follow_Up_Disqualified_Reason__c</field>
        <name>Clear Follow-Up Disqualified Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Follow_Up_Rejected_Reason</fullName>
        <description>Clear Follow-Up Rejected Reason</description>
        <field>Follow_Up_Rejected_Reason__c</field>
        <name>Clear Follow-Up Rejected Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_High_Quality_Campaign_Response</fullName>
        <field>High_Quality_Campaign_Response__c</field>
        <literalValue>0</literalValue>
        <name>Clear High Quality Campaign Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_High_Quality_Campaign_Response_Rea</fullName>
        <field>High_Quality_Campaign_Response_Reason__c</field>
        <name>Clear High Quality Campaign Response Rea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Manual_Change_Product_Category</fullName>
        <description>Clear Manual Change Product Category</description>
        <field>Manual_Change_Product_Category__c</field>
        <literalValue>0</literalValue>
        <name>Clear Manual Change Product Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_New_Follow_Up_Required_End_Date</fullName>
        <field>New_Follow_Up_Completed_Date__c</field>
        <name>Clear New: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_New_Responder_Follow_Up_Required</fullName>
        <field>New_Responder_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Clear New Responder Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Number_Of_Follow_Up_Emails</fullName>
        <field>Number_of_Follow_Up_Emails__c</field>
        <formula>0</formula>
        <name>Clear Number Of Follow Up Emails</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Overall_Follow_Up_Req_End_Date</fullName>
        <field>Overall_Follow_Up_Completed_Date__c</field>
        <name>Clear Overall: Follow-Up Req End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Overall_Follow_Up_Required</fullName>
        <field>Overall_Follow_up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Clear Overall Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Route_Around_to_EIS_ISR</fullName>
        <field>Route_Around_to_EIS_ISR__c</field>
        <literalValue>0</literalValue>
        <name>Clear Route Around to EIS/ISR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Trial_Follow_Up_Required</fullName>
        <field>Trial_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Clear Trial Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Con_Set_MDF_TC_Accept_date</fullName>
        <description>Set MDF T&amp;C Accept date</description>
        <field>T_C_MDF_Accepted_Date__c</field>
        <formula>Today()</formula>
        <name>Con_Set MDF TC Accept date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_CC_Accepted_date</fullName>
        <description>Set Code of Conduct Accepted Date</description>
        <field>Code_of_Conduct_Accepted__c</field>
        <formula>TODAY()</formula>
        <name>Contact: Set CC Accepted date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Status_Duration_Change_Date</fullName>
        <description>Used to update the &quot;Contact Status Duration Change Date&quot; field with the Date that the &quot;Contact Status&quot; picklist has changed.</description>
        <field>Contact_Status_Duration_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Contact Status Duration Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Status_to_to_Qualified_B</fullName>
        <field>Contact_Status__c</field>
        <literalValue>Qualified - B</literalValue>
        <name>Contact Status to to Qualified - B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Follow_Up_Required_Start_Date</fullName>
        <field>Event_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Event: Follow-Up Required Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Existing_Follow_Up_Required_Start_Date</fullName>
        <field>Existing_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Existing: Follow-Up Required Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IV_Contact_Title_Change_Add_to_custom</fullName>
        <field>Job_Title__c</field>
        <formula>Title</formula>
        <name>IV - Contact Title Change Add to custom</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IV_Contact_Title_Change_Delete_standar</fullName>
        <field>Title</field>
        <name>IV - Contact Title Change Delete standar</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MobilePhone_System_Update</fullName>
        <description>Update field MobilePhone System</description>
        <field>MobilePhone_System__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(MobilePhone, &quot;(0)&quot;, &quot;&quot;), &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), &quot;.&quot;, &quot;&quot;)</formula>
        <name>MobilePhone System Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Follow_Up_Required_Start_Date</fullName>
        <field>New_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY ()</formula>
        <name>New: Follow-Up Required Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Responder_F_Up_Req_End_Date_Clear</fullName>
        <field>New_Responder_Follow_Up_Completed_Date__c</field>
        <name>New Responder: F-Up Req End Date Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Responder_F_Up_Req_End_Date_Set</fullName>
        <description>Update the New Responder: Follow-Up Completed Date field</description>
        <field>New_Responder_Follow_Up_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>New Responder: F-Up Req End Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Responder_F_Up_Req_Start_Date_Set</fullName>
        <field>New_Responder_Follow_Up_Req_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>New Responder: F-Up Req Start Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Number_of_Voicemails_0</fullName>
        <field>Number_of_Follow_Up_Voicemails__c</field>
        <formula>0</formula>
        <name>Number of Voicemails 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PCM_Clear</fullName>
        <description>Clear the PCM (Please Contact Me) field</description>
        <field>PCM__c</field>
        <literalValue>0</literalValue>
        <name>PCM Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone_Substituded_Update</fullName>
        <description>Update field Phone System</description>
        <field>Phone_System__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Phone, &quot;(0)&quot;, &quot;&quot;), &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), &quot;.&quot;, &quot;&quot;)</formula>
        <name>Phone System Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QSB_Trial_Start_Date_Set</fullName>
        <field>QSB_Trial_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>QSB Trial Start Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_follow_up_from</fullName>
        <description>Removes values in the Contact Follow up fromfield (date)</description>
        <field>Follow_up_from__c</field>
        <name>Remove follow up from</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contact_Status_To_F_Up_Disqualified</fullName>
        <description>Set Contact Status to Follow-Up Disqualified</description>
        <field>Contact_Status__c</field>
        <literalValue>Follow-Up Disqualified</literalValue>
        <name>Set Contact Status To F-Up Disqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contact_Status_To_Follow_Up_Rejected</fullName>
        <description>Set the Contact Status to Follow-Up Rejected</description>
        <field>Contact_Status__c</field>
        <literalValue>Follow-Up Rejected</literalValue>
        <name>Set Contact Status To Follow-Up Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Event_Follow_Up_Required_End_Date</fullName>
        <field>Event_Follow_Up_Completed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set Event: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Existing_Follow_Up_Required_End_Date</fullName>
        <field>Existing_Follow_Up_Completed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set Existing Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_FU_Disqualified_Reason_To_No_Respons</fullName>
        <description>Set Follow-Up Disqualified Reason to No Response</description>
        <field>Follow_Up_Disqualified_Reason__c</field>
        <literalValue>No Response</literalValue>
        <name>Set FU Disqualified Reason To No Respons</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Follow_Up_Rejected_Reason_To_Junk</fullName>
        <description>Set Follow-Up Rejected Reason to Junk</description>
        <field>Follow_Up_Rejected_Reason__c</field>
        <literalValue>Junk</literalValue>
        <name>Set Follow-Up Rejected Reason To Junk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_source_to_Employee</fullName>
        <description>Set the lead source value to Employee</description>
        <field>LeadSource</field>
        <literalValue>Employee</literalValue>
        <name>Set Lead source to Employee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Manual_Change_Product_Category</fullName>
        <description>Set Manual Change Product Category</description>
        <field>Manual_Change_Product_Category__c</field>
        <literalValue>1</literalValue>
        <name>Set Manual Change Product Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_N3_Flag</fullName>
        <field>Send_To_N3__c</field>
        <literalValue>1</literalValue>
        <name>Set N3 Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New_Follow_Up_Hidden</fullName>
        <field>New_Follow_Up_Hidden__c</field>
        <literalValue>1</literalValue>
        <name>Set New Follow Up Hidden</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New_Follow_Up_Required_End_Date</fullName>
        <field>New_Follow_Up_Completed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set New: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Req_Start_Date</fullName>
        <description>Set Overall: Follow-Up Req Start Date</description>
        <field>Overall_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set Overall: Follow-Up Req Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Req_to_True</fullName>
        <field>Overall_Follow_up_Required__c</field>
        <literalValue>1</literalValue>
        <name>Set Overall: Follow-Up Req to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Required_End_Date</fullName>
        <field>Overall_Follow_Up_Completed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set Overall:Follow Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Required_to_False</fullName>
        <field>Overall_Follow_up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Set Overall:Follow Up Required to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_1</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;1&quot;</formula>
        <name>Set Predictive Lead Numeric Score 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_2</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;2&quot;</formula>
        <name>Set Predictive Lead Numeric Score 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_3</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;3&quot;</formula>
        <name>Set Predictive Lead Numeric Score 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_4</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;4&quot;</formula>
        <name>Set Predictive Lead Numeric Score 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_5</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;5&quot;</formula>
        <name>Set Predictive Lead Numeric Score 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_A</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;A&quot;</formula>
        <name>Set Predictive Lead Score A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_B</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;B&quot;</formula>
        <name>Set Predictive Lead Score B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_C</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;C&quot;</formula>
        <name>Set Predictive Lead Score C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_D</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;D&quot;</formula>
        <name>Set Predictive Lead Score D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_E</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;E&quot;</formula>
        <name>Set Predictive Lead Score E</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Switchboard_System_Update</fullName>
        <description>Switchboard System Update</description>
        <field>Switchboard_System__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Switchboard__c, &quot;(0)&quot;, &quot;&quot;), &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), &quot;.&quot;, &quot;&quot;)</formula>
        <name>Switchboard System Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sync_status_update_by_Boomi</fullName>
        <field>Boomi_Status_N3_Integration__c</field>
        <literalValue>Ready for N3</literalValue>
        <name>Sync status update by Boomi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_End_Date_Clear</fullName>
        <field>Trial_Follow_Up_Completed_Date__c</field>
        <name>Trial: F-Up Req End Date Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_End_Date_Set</fullName>
        <field>Trial_Follow_Up_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Trial: F-Up Req End Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_Set</fullName>
        <field>Trial_Follow_Up_Required__c</field>
        <literalValue>1</literalValue>
        <name>Trial: F-Up Req Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_Start_Date_Set</fullName>
        <field>Trial_Follow_Up_Req_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Trial: F-Up Req Start Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_QSB_Trial_21_Clear</fullName>
        <field>Trial_QSB_Trial_greater_21__c</field>
        <literalValue>0</literalValue>
        <name>Trial: QSB Trial &gt; 21 Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_QSB_Trial_Clear</fullName>
        <field>QSB_Trial__c</field>
        <literalValue>0</literalValue>
        <name>Trial: QSB Trial Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trigger_PortalControl</fullName>
        <field>TriggerPortalControl__c</field>
        <literalValue>1</literalValue>
        <name>Trigger PortalControl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ULCUpdateULC_OldwithULC</fullName>
        <description>Used to ensure that the ULC old field contains a value, this is used for the Learn.com export</description>
        <field>ULC_Old_Name__c</field>
        <formula>ULC__c</formula>
        <name>ULC: Update ULC_Old with ULC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnCheck_Renewal_Date_60days_field</fullName>
        <description>UnCheck Renewal Date less than 60days field</description>
        <field>Renewal_Date_less_than_60_Days__c</field>
        <literalValue>0</literalValue>
        <name>UnCheck Renewal Date &lt; 60days field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatePortalControl</fullName>
        <field>TriggerPortalControl__c</field>
        <literalValue>1</literalValue>
        <name>Update Portal Control</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Change</fullName>
        <field>Account_Changed__c</field>
        <formula>Today() + 0</formula>
        <name>Update Account Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Follow_up_Required</fullName>
        <description>Update Event: Follow-up Required to False</description>
        <field>Event_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update Event: Follow-up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Existing_Follow_Up_Require</fullName>
        <description>Update Existing: Follow-Up Required</description>
        <field>Existing_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update Existing: Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Flagged_for_NS_update_on_Contact</fullName>
        <field>Flagged_for_NS_update__c</field>
        <literalValue>1</literalValue>
        <name>Update Flagged for NS update on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_City</fullName>
        <field>MailingCity</field>
        <formula>Account.BillingCity</formula>
        <name>Update Mailing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_Country</fullName>
        <field>MailingCountry</field>
        <formula>Account.BillingCountry</formula>
        <name>Update Mailing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_State</fullName>
        <field>MailingState</field>
        <formula>Account.BillingState</formula>
        <name>Update Mailing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_Street</fullName>
        <field>MailingStreet</field>
        <formula>Account.BillingStreet</formula>
        <name>Update Mailing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_Zip</fullName>
        <field>MailingPostalCode</field>
        <formula>Account.BillingPostalCode</formula>
        <name>Update Mailing Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Follow_Up_Required</fullName>
        <description>Update New: Follow-Up Required</description>
        <field>New_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update New: Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Partner_Owned_Contact</fullName>
        <description>If Partner created this Contact set to True (This will also be set to true upon Partner Lead Conversion</description>
        <field>Partner_Owned_Contact__c</field>
        <literalValue>1</literalValue>
        <name>Update Partner Owned Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Premier_Support_Email_Sent_date</fullName>
        <field>Premier_Support_Mail_Sent__c</field>
        <formula>TODAY()</formula>
        <name>Update Premier Support Email Sent date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Route_Around_to_EIS_ISR</fullName>
        <field>Route_Around_to_EIS_ISR__c</field>
        <literalValue>1</literalValue>
        <name>Update Route Around to EIS/ISR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_Role</fullName>
        <field>Original_Created_by_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Update original created by Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_function</fullName>
        <field>Original_Created_by_Function__c</field>
        <formula>IF ( INCLUDES ( $User.Function__c, &quot;Sales&quot; ), &quot;Sales; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Marketing&quot; ), &quot;Marketing; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Alliance&quot; ), &quot;Alliance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;BDR&quot; ), &quot;BDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iBDR&quot; ), &quot;iBDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ISR&quot; ), &quot;ISR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;EIS&quot; ), &quot;EIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;MIS&quot; ), &quot;MIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SMBIS&quot; ), &quot;SMBIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;PRM&quot; ), &quot;PRM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Support&quot; ), &quot;Support; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Training&quot; ), &quot;Training; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Finance&quot; ), &quot;Finance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;HR&quot; ), &quot;HR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;IT&quot; ), &quot;IT; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;R&amp;D&quot; ), &quot;R&amp;D; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iPM&quot; ), &quot;iPM; &quot;,null ) &amp;
IF ( INCLUDES ( $User.Function__c, &quot;SDR - MR&quot; ), &quot;SDR - MR; &quot;,null ) &amp;
IF ( INCLUDES ( $User.Function__c, &quot;N3&quot; ), &quot;N3; &quot;,null )&amp; 
IF ( INCLUDES ( $User.Function__c, &quot;NIS&quot; ), &quot;NIS; &quot;,null )&amp;
IF ( INCLUDES ( $User.Function__c, &quot;ServiceSource&quot; ), &quot;ServiceSource; &quot;,null )</formula>
        <name>Update original created by function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>ECUSTOMS__RPSContact_PROD</fullName>
        <apiVersion>40.0</apiVersion>
        <description>RPS call for Contacts (PROD)</description>
        <endpointUrl>https://sf.visualcompliance.com/RPS_SF_WS/ContactNotificationService_Managed.asmx</endpointUrl>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MailingCity</fields>
        <fields>MailingCountry</fields>
        <fields>MailingState</fields>
        <fields>MailingStreet</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>qtweb@qliktech.com</integrationUser>
        <name>RPSContact_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>ECUSTOMS__eCustoms_RPSContact_OM</fullName>
        <apiVersion>32.0</apiVersion>
        <description>eCustoms RPS Contact WS call to test server</description>
        <endpointUrl>https://sftest.visualcompliance.com/RPS_SF_WS/ContactNotificationService_Managed.asmx</endpointUrl>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MailingCountry</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>qtweb@qliktech.com</integrationUser>
        <name>eCustoms_RPSContact_OM</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CON%3A Send username%2Fpassword when password reset requested</fullName>
        <actions>
            <name>CONSendusernamepasswordtoContactwhenULCstatusissettoResendPassword</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.ULC_Status__c</field>
            <operation>equals</operation>
            <value>Resend Password</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CON%3A Set ULC Status to Active</fullName>
        <actions>
            <name>CONSetULCStatustoActive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Approved_ULC_Levels__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.ULC_Status__c</field>
            <operation>notEqual</operation>
            <value>Disabled - General,Active,Disabled - Left Company</value>
        </criteriaItems>
        <description>Set ULC Status to Active, if the contact has at least one Approved ULC access level</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CON%3AUpdate Title from Job Title</fullName>
        <actions>
            <name>CON_Update_Title_from_Job_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Job_Title__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update standard field: Title from custom field: Job Title, whenever it&apos;s updated or created</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Disable ULC when Contact left Company</fullName>
        <actions>
            <name>CULCDisableULCwhenContactleftComp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>disables access to the ULC for a contact when they have left the company</description>
        <formula>AND( ISPICKVAL(ULC_Status__c, &apos;Active&apos;)  = True,  Left_Company__c = True)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Follow-Up Disqualified Reason</fullName>
        <actions>
            <name>Clear_Follow_Up_Disqualified_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Status changes from Follow-Up Disqualified to something else clear the Follow-Up Disqualified Reason</description>
        <formula>AND
(
   ISCHANGED(Contact_Status__c),
   ISPICKVAL(PRIORVALUE(Contact_Status__c), &quot;Follow-Up Disqualified&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Follow-Up Rejected Reason</fullName>
        <actions>
            <name>Clear_Follow_Up_Rejected_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Status changes from Follow-Up Rejected to something else clear the Follow-Up Rejected Reason</description>
        <formula>AND
(
   ISCHANGED(Contact_Status__c),
   ISPICKVAL(PRIORVALUE(Contact_Status__c), &quot;Follow-Up Rejected&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear HQL Response</fullName>
        <actions>
            <name>Clear_High_Quality_Campaign_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_High_Quality_Campaign_Response_Rea</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Overall Follow-Up Required field is set to false, then clear High Quality Campaign Response and High Quality Campaign Response Reason fields.</description>
        <formula>AND
(
   ISCHANGED(Overall_Follow_up_Required__c),
   Overall_Follow_up_Required__c = FALSE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Update Route Around to EIS%2FISR</fullName>
        <actions>
            <name>Clear_High_Quality_Campaign_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_High_Quality_Campaign_Response_Rea</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Route_Around_to_EIS_ISR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Overall Follow-Up Required field is set to false, then clear Route Around EIS/ISR, High Quality Campaign Response and High Quality Campaign Response Reason fields.</description>
        <formula>OR (   AND   (     ISCHANGED(Event_Follow_Up_Required__c),     Event_Follow_Up_Required__c = FALSE,     Existing_Follow_Up_Required__c = FALSE,     New_Follow_Up_Required__c = FALSE   ),   AND   (     ISCHANGED(Existing_Follow_Up_Required__c),     Event_Follow_Up_Required__c = FALSE,     Existing_Follow_Up_Required__c = FALSE,     New_Follow_Up_Required__c = FALSE   ),   AND   (     ISCHANGED(New_Follow_Up_Required__c),     Event_Follow_Up_Required__c = FALSE,     Existing_Follow_Up_Required__c = FALSE,     New_Follow_Up_Required__c = FALSE   ),   AND   (     ISCHANGED(Overall_Follow_up_Required__c),     Overall_Follow_up_Required__c = FALSE   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Con_Set MDF TC Accept date</fullName>
        <actions>
            <name>Con_Set_MDF_TC_Accept_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.T_C_MDF_Accepted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When MDF T&amp;C is approved, set MDF accept date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Send Onboarding Survey Mail</fullName>
        <actions>
            <name>Contact_Send_Onboarding_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Onboarding_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact Follow Up</fullName>
        <actions>
            <name>Clear_Date_Of_Count_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Date_Of_Email_Count_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Number_Of_Follow_Up_Emails</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Number_of_Voicemails_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>
            AND 
            (
              OR
              (
                ISCHANGED(New_Responder_Follow_Up_Required__c) &amp;&amp; New_Responder_Follow_Up_Required__c = TRUE,
                ISCHANGED(Trial_Follow_Up_Required__c) &amp;&amp; Trial_Follow_Up_Required__c = TRUE,
                ISCHANGED(Overall_Follow_up_Required__c) &amp;&amp; Overall_Follow_up_Required__c = TRUE
              ),
              NOT
              (
                AND
                (
                  ISNEW(),
                  IsNotConvertedFromLead__c = FALSE
                )
              )
            )
        </formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact SoI Clears FUR</fullName>
        <actions>
            <name>Clear_New_Responder_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Overall_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Trial_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear the New Responder, Trial and Overall Follow-up Required fields when the &quot;In Sphere of Influence Count&quot; field increases and New Responder, Trial or Overall Follow-up Required fields is True</description>
        <formula>AND
(     
  OR
  (
    New_Responder_Follow_Up_Required__c,
    Trial_Follow_Up_Required__c,
    Overall_Follow_up_Required__c
  ),
  In_Sphere_of_Influence_Count__c &gt; PRIORVALUE(In_Sphere_of_Influence_Count__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Status Duration Change Date</fullName>
        <actions>
            <name>Contact_Status_Duration_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update the &quot;Contact Status Duration Change Date&quot; field with the current date when the Contact Status changes.</description>
        <formula>AND (     NOT(AND(ISNEW(), NOT(IsNotConvertedFromLead__c))),         OR      (          ISNEW(),          ISCHANGED(Contact_Status__c)      ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Account change date</fullName>
        <actions>
            <name>Update_Account_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>this will automatically generate a time stamp each time the contact changes accounts.</description>
        <formula>IF(  ISCHANGED( AccountId ),  TRUE,NULL)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Check Renewal Date Less Than 60 Days field</fullName>
        <active>true</active>
        <description>Check the field &quot;Renewal Date less than 60 Days&quot;</description>
        <formula>NOT( ISBLANK( Renewal_Date__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Renewal_Date_60days_field</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contact.Renewal_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contact%3A Phone number System fields update</fullName>
        <actions>
            <name>MobilePhone_System_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Phone_Substituded_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Switchboard_System_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow for System field 
Phone System
MobilePhone System
SwitchBoard System</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Populate original created by function and role</fullName>
        <actions>
            <name>Update_original_created_by_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_original_created_by_function</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17</booleanFilter>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>ISR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>BDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iBDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>EIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Alliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>MIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>SMBIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>IT</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>R&amp;D</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iPM,SDR - MR,N3,ServiceSource,NIS</value>
        </criteriaItems>
        <description>Used to eliminate the need to create new user profiles when users change roles within SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Same Day Clear Of FollowUp</fullName>
        <actions>
            <name>Update_Event_Follow_up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Existing_Follow_Up_Require</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_New_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(      ISPICKVAL(Contact_Status__c, &apos;Lead - In Qualification&apos;),      Same_Day_Clear_Of_Follow_Up_Flags__c = TODAY()  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set CC Accepted date</fullName>
        <actions>
            <name>Contact_Set_CC_Accepted_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Code_of_Conduct__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Code of Conduct is checked, set Code of Conduct Accepted Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set follow-up to False for some Status values</fullName>
        <actions>
            <name>Clear_New_Responder_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Overall_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Trial_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_QSB_Trial_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Contact: Set follow-up to False for some Status values (CR# 35709)</description>
        <formula>AND 
(
  OR
  (
    AND
    (
      ISCHANGED(Contact_Status__c),
      OR
      (
        ISPICKVAL(Contact_Status__c, &quot;Follow-Up Failed&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Follow-Up Flagged in Error&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Goal Identified&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Goal Rejected&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Goal Confirmed&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Champion&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Prove Value&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Negotiating&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Closed Won&quot;),
        ISPICKVAL(Contact_Status__c, &quot;Closed Lost&quot;)
      )
    ),
    AND
    ( /* CR 70248*/
      ISPICKVAL(Contact_Status__c, &quot;Follow-Up Rejected&quot;),
      ISCHANGED(Follow_Up_Rejected_Reason__c),
      OR
      (
        ISPICKVAL(Follow_Up_Rejected_Reason__c, &quot;Already Working&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c, &quot;Junk&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c, &quot;Invalid Job Function&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c, &quot;Competitor&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c, &quot;Student&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c, &quot;Partner/Consultant&quot;)
      ),
      OR
      ( 
        ISPICKVAL(PRIORVALUE(Follow_Up_Rejected_Reason__c), &quot;Incorrect Routing&quot;),
        ISPICKVAL(PRIORVALUE(Follow_Up_Rejected_Reason__c), &quot;Incomplete/Inaccurate Info&quot;)
      )
    )
  ),
  OR
  (
    New_Responder_Follow_Up_Required__c = True,
    Trial_Follow_Up_Required__c = True,
    Overall_Follow_up_Required__c = True
  ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set follow-up to False with Certain Lead - Not Now reasons</fullName>
        <actions>
            <name>Update_Event_Follow_up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Existing_Follow_Up_Require</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_New_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Contact: Set follow-up to False with Certain Lead - Not Now reasons (CR# 7076)</description>
        <formula>ISCHANGED( Contact_Status__c ) &amp;&amp;  ispickval(Contact_Status__c, &quot;Lead - Not Now&quot;) &amp;&amp;  OR((Event_Follow_Up_Required__c = True) ,(New_Follow_Up_Required__c = True), (Existing_Follow_Up_Required__c = True)) &amp;&amp;  OR(  ispickval(Lead_Not_Now_Reason__c, &quot;Student&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;Too Early in Buying Cycle&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;Partner&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;No/Bad Phone Number&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;Information Only&quot;),  ispickval(Unqualified_Reasonm__c, &quot;No e-mail and phone&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A UnCheck Renewal Date Less Than 60 Days field</fullName>
        <actions>
            <name>UnCheck_Renewal_Date_60days_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>UnCheck the field &quot;Renewal Date less than 60 Days&quot;</description>
        <formula>OR( ISBLANK(Renewal_Date__c), Renewal_Date__c - Today() &gt; 60 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ContactLead - Primary section filled in</fullName>
        <actions>
            <name>Contact_Status_to_to_Qualified_B</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.G_P_N_linked_to_person_s_job__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.G_P_N_Relevant_to_our_Solution__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Goal_Problem_Need_Description__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Is_the_person_paid_on_this__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the Primary Qualification section is filled in completely the Contact Status turns to Qualified - B (De-activated as part of CR 6546 https://eu1.salesforce.com/a0CD000000NL228)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ECUSTOMS__eCustoms Contact RPS</fullName>
        <actions>
            <name>ECUSTOMS__RPSContact_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>eCustoms Contact RPS Screening</description>
        <formula>OR(ISNEW(),ISCHANGED(FirstName),ISCHANGED(LastName),ISCHANGED(MailingStreet),ISCHANGED(MailingCity),ISCHANGED(MailingState),ISCHANGED(MailingCountry))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Event%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Set_Event_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Event: Follow-Up Required is changed from True to False, Event: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED( Event_Follow_Up_Required__c), Event_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Event%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_Event_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Event_Follow_Up_Required_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISCHANGED( Event_Follow_Up_Required__c ), Event_Follow_Up_Required__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Existing%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Set_Existing_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Existing: Follow-Up Required is changed from True to False, Existing: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED( Existing_Follow_Up_Required__c ), Existing_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Existing%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_Existing_Follow_Up_Req_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Existing_Follow_Up_Required_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISCHANGED( Existing_Follow_Up_Required__c ), Existing_Follow_Up_Required__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IV - Contact Title Change</fullName>
        <actions>
            <name>IV_Contact_Title_Change_Add_to_custom</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IV_Contact_Title_Change_Delete_standar</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Title</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Changes standard field to custom field for the insideview application</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NS%3A Update Flagged for NS update on Contact</fullName>
        <actions>
            <name>Update_Flagged_for_NS_update_on_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to trigger Netsuite updates from SFDC Contact Record</description>
        <formula>OR( ISCHANGED( Title ) , ISCHANGED( FirstName ) , ISCHANGED( LastName ) , ISCHANGED( Email ), ISCHANGED( Phone ) , ISCHANGED( MobilePhone ) , ISCHANGED( QlikTech_Company__c ), ISCHANGED( Country_Code__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Responder%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>New_Responder_F_Up_Req_End_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When New Responder: Follow-Up Required is changed from True to False, New Responder: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED(New_Responder_Follow_Up_Required__c), New_Responder_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Responder%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>New_Responder_F_Up_Req_End_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_Responder_F_Up_Req_Start_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When New Responder: Follow-Up Required is changed from False to True, New Responder: Follow-Up Required Start Date is set.</description>
        <formula>AND( ISCHANGED(New_Responder_Follow_Up_Required__c), New_Responder_Follow_Up_Required__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Set_New_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When New: Follow-Up Required is changed from True to False, New: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED( New_Follow_Up_Required__c), New_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_New_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_Follow_Up_Required_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISCHANGED( New_Follow_Up_Required__c ), New_Follow_Up_Required__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Overall Follow-Up Required Flow End</fullName>
        <actions>
            <name>Clear_Manual_Change_Product_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PCM_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Overall_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When all Overall Follow-Up Required transitions to false the Overall Follow-Up Required End Date will be set.</description>
        <formula>AND
(
   ISCHANGED(Overall_Follow_up_Required__c),
   Overall_Follow_up_Required__c = FALSE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Overall Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_Overall_Follow_Up_Req_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Overall_Follow_Up_Req_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Overall: Follow-Up Required field is ticked the Overall Follow-Up start date will be set. and the End date cleared.</description>
        <formula>AND
(
ISCHANGED(Overall_Follow_up_Required__c),
Overall_Follow_up_Required__c = TRUE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PORTAL trigger from Contact</fullName>
        <actions>
            <name>PORTALtriggerfromContact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Trigger from the contact level to Orbis to start the portal access routine</description>
        <formula>OR(ISCHANGED( Allow_Partner_Portal_Access__c ) ,ISCHANGED( Partner_Portal_Role__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PORTAL%3A Send mail to Owner</fullName>
        <actions>
            <name>PORTALSendmailtoOwner</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send a mail to the owner to let them know the status has changed</description>
        <formula>AND(  Allow_Partner_Portal_Access__c  = True, ISCHANGED( Partner_Portal_Status__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM - Partner User Created requires Forecasting setup</fullName>
        <actions>
            <name>Update_Premier_Support_Email_Sent_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Allow_Partner_Portal_Access__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Partner_Portal_Status__c</field>
            <operation>contains</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.ULC_Password__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Premier_Support_Mail_Sent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sends PA a mail to notify him that a Portal User has been created and may require adding to Forecast, workflow, Public group etc</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QSB Trial Flow Start</fullName>
        <actions>
            <name>QSB_Trial_Start_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_QSB_Trial_21_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the QSB Trial field is set we set the &quot;QSB Trial Start Date&quot; field and clear the &quot;Trial: QSB Trial greater 21&quot; field in case it has been set previously.</description>
        <formula>OR
(
   ISCHANGED(QSB_Trial__c) &amp;&amp; QSB_Trial__c = TRUE,
   ISNEW() &amp;&amp; QSB_Trial__c = TRUE &amp;&amp; IsNotConvertedFromLead__c = TRUE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove Follow up from if Contact status</fullName>
        <actions>
            <name>Remove_follow_up_from</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>notEqual</operation>
            <value>Contacted-Additional Work Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Follow_up_from__c</field>
            <operation>greaterOrEqual</operation>
            <value>1/1/1970</value>
        </criteriaItems>
        <description>Remove follow up from if contact status is not Contacted-Additional Work required</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set FU Disqualified for Lattice Scoring</fullName>
        <actions>
            <name>Set_Contact_Status_To_F_Up_Disqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_FU_Disqualified_Reason_To_No_Respons</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>19 AND ((1 AND 2 AND 3) OR (4 AND 5 AND 6) OR (7 AND 8 AND 9) OR (10 AND 11 AND 12) OR (13 AND 14 AND 15) OR (16 AND 17 AND 18))</booleanFilter>
        <criteriaItems>
            <field>Contact.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>A</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>6</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>B</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>C</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>D</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>E</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Voicemails__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Number_of_Follow_Up_Emails__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>equals</operation>
            <value>Follow-Up Attempt 1,Follow-Up Attempt 2,Follow-Up Attempt 3,Follow-Up Attempt 4,Follow-Up Attempt 5</value>
        </criteriaItems>
        <description>Set FU Disqualified for Lattice Scoring</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Mailing Address From Account</fullName>
        <actions>
            <name>Update_Mailing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5</booleanFilter>
        <criteriaItems>
            <field>Contact.MailingCity</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingStreet</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingPostalCode</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Manual Change Product Category</fullName>
        <actions>
            <name>Set_Manual_Change_Product_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Manual Change Product Category field</description>
        <formula>AND (    ISPICKVAL(PRIORVALUE(Product_Category__c), &apos;Cross Product&apos;),    ISPICKVAL(Product_Category__c, &apos;Data Integration&apos;),    OR(New_Responder_Follow_Up_Required__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c),    NOT(OR($User.FirstName = &apos;Integration&apos; &amp;&amp; $User.LastName = &apos;User&apos;, $User.FirstName = &apos;Marketo&apos; &amp;&amp; $User.LastName = &apos;@Qlikview&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set N3 Flag for sync from SFDC to N3</fullName>
        <actions>
            <name>Set_N3_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sync_status_update_by_Boomi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.OwnerId</field>
            <operation>equals</operation>
            <value>N3 API user</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>equals</operation>
            <value>Follow-Up Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Overall_Follow_up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This flag will be used by Boomi to pick the leads and sync them to N3.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set New Follow Up Hidden on Contact</fullName>
        <actions>
            <name>Set_New_Follow_Up_Hidden</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Overall_Follow_up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Set to True when Overall Follow-Up Required is True</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Pedictive Lead Score A</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the A values for the Predictive Lead Score Grade</description>
        <formula>AND   (       OR       (     ISCHANGED(contactGrade6sense__c),     ISCHANGED(contactProfileFit6sense__c),     ISCHANGED(X6sense_Account_Buying_Stage__c),     ISCHANGED(contactScoreDate6sense__c)       ),   OR   (     AND     (       OR       (         contactGrade6sense__c = &apos;A&apos;,         contactGrade6sense__c = &apos;B&apos;       ),       AND       (         contactProfileFit6sense__c = &apos;Strong&apos;,         OR         (           X6sense_Account_Buying_Stage__c = &apos;Decision&apos;,           X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;         )       )     ),     AND     (       contactGrade6sense__c = &apos;C&apos;,       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (             X6sense_Account_Buying_Stage__c = &apos;Target&apos;,             X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;,             X6sense_Account_Buying_Stage__c = &apos;Decision&apos;           )         ),         AND         (           X6sense_Account_Buying_Stage__c = &apos;Target&apos;,           OR           (             contactProfileFit6sense__c = &apos;Moderate&apos;,             contactProfileFit6sense__c = &apos;Weak&apos;           )         )       )     )   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Pedictive Lead Score B</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_B</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the B values for the Predictive Lead Score Grade</description>
        <formula>AND   (       OR   (     ISCHANGED(contactGrade6sense__c),     ISCHANGED(contactProfileFit6sense__c),     ISCHANGED(X6sense_Account_Buying_Stage__c),     ISCHANGED(contactScoreDate6sense__c)   ),   OR   (     AND      (       OR       (         contactGrade6sense__c = &apos;A&apos;,         contactGrade6sense__c = &apos;B&apos;       ),       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (              X6sense_Account_Buying_Stage__c = &apos;Target&apos;,             X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;           )         ),               AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           OR           (              X6sense_Account_Buying_Stage__c = &apos;Decision&apos;,             X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;           )         )       )     ),     AND      (       contactGrade6sense__c = &apos;C&apos;,             OR       (         AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           OR           (              X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;,             X6sense_Account_Buying_Stage__c = &apos;Decision&apos;           )         ),               AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           OR           (              X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;           )         )       )     ),     AND      (       OR       (         contactGrade6sense__c = &apos;D&apos;,         ISBLANK(contactGrade6sense__c)        ),       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (              X6sense_Account_Buying_Stage__c = &apos;Target&apos;,             X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;           )         ),               AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           X6sense_Account_Buying_Stage__c = &apos;Target&apos;         )       )     )   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Pedictive Lead Score C</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_C</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the C values for the Predictive Lead Score Grade</description>
        <formula>AND  (    OR    (      ISCHANGED(contactGrade6sense__c),     ISCHANGED(contactProfileFit6sense__c),     ISCHANGED(X6sense_Account_Buying_Stage__c),     ISCHANGED(contactScoreDate6sense__c)   ),   OR    (      AND      (        OR        (          contactGrade6sense__c = &apos;A&apos;,          contactGrade6sense__c = &apos;B&apos;       ),       OR       (         AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           OR           (             X6sense_Account_Buying_Stage__c = &apos;Target&apos;,             X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;,             X6sense_Account_Buying_Stage__c = &apos;Decision&apos;,             X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;           )         ),         AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           OR           (             X6sense_Account_Buying_Stage__c = &apos;Target&apos;,             X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;           )         )       )     ),     AND     (        contactGrade6sense__c = &apos;C&apos;,       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;         ),         AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           X6sense_Account_Buying_Stage__c = &apos;Decision&apos;         )       )     ),      AND      (        OR        (          contactGrade6sense__c = &apos;D&apos;,          ISBLANK(contactGrade6sense__c)        ),        OR       (         AND          (            contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (             X6sense_Account_Buying_Stage__c = &apos;Decision&apos;,             X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;           )         ),           AND          (            contactProfileFit6sense__c = &apos;Moderate&apos;,           OR           (             X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;,             X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;,             X6sense_Account_Buying_Stage__c = &apos;Decision&apos;           )         ),          AND          (            contactProfileFit6sense__c = &apos;Weak&apos;,            X6sense_Account_Buying_Stage__c = &apos;Awareness&apos;          )        )      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Pedictive Lead Score D</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_D</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the D values for the Predictive Lead Score Grade</description>
        <formula>AND  (    OR    (      ISCHANGED(contactGrade6sense__c),      ISCHANGED(contactProfileFit6sense__c),      ISCHANGED(X6sense_Account_Buying_Stage__c),      ISCHANGED(contactScoreDate6sense__c)    ),   OR    (      AND      (        contactGrade6sense__c = &apos;C&apos;,        contactProfileFit6sense__c = &apos;Weak&apos;,       X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;     ),      AND      (        OR        (          contactGrade6sense__c = &apos;D&apos;,         ISBLANK(contactGrade6sense__c)       ),        OR        (          AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;         ),         AND          (            contactProfileFit6sense__c = &apos;Weak&apos;,           OR            (              X6sense_Account_Buying_Stage__c = &apos;Decision&apos;,              X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;            )          )        )      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Pedictive Lead Score E</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_E</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the E values for the Predictive Lead Score Grade</description>
        <formula>AND  (    OR    (      ISCHANGED(contactGrade6sense__c),      ISCHANGED(contactProfileFit6sense__c),      ISCHANGED(X6sense_Account_Buying_Stage__c),      ISCHANGED(contactScoreDate6sense__c)    ),    OR    (     AND     (        contactGrade6sense__c = &apos;C&apos;,       contactProfileFit6sense__c = &apos;Moderate&apos;,       X6sense_Account_Buying_Stage__c = &apos;Purchase&apos;     ),      AND     (        OR        (          contactGrade6sense__c = &apos;D&apos;,         ISBLANK(contactGrade6sense__c)       ),       contactProfileFit6sense__c = &apos;Weak&apos;,       OR        (          X6sense_Account_Buying_Stage__c = &apos;Target&apos;,          X6sense_Account_Buying_Stage__c = &apos;Consideration&apos;        )      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set lead source to Employee</fullName>
        <actions>
            <name>Set_Lead_source_to_Employee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Account_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <description>Set lead source to employee where account type = Employee</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Trial Set Trial Follow-Up Required</fullName>
        <actions>
            <name>Trial_F_Up_Req_End_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_F_Up_Req_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_F_Up_Req_Start_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.QSB_Trial__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Overall_Follow_up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets the Trial F-Up Required field if QSB Trial and Overall Follow-Up Required is set</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Trial%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Trial_F_Up_Req_End_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_QSB_Trial_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Trial: Follow-Up Required is changed from True to False, Trial: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED(Trial_Follow_Up_Required__c), Trial_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Trigger Portal Control</fullName>
        <actions>
            <name>UpdatePortalControl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Email )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ULC PortalControl</fullName>
        <actions>
            <name>Trigger_PortalControl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(  ISCHANGED( Allow_Partner_Portal_Access__c),  AND(  Allow_Partner_Portal_Access__c = true,  ISCHANGED( Partner_Portal_Role__c)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ULC%3A New Potential Match - Contact</fullName>
        <actions>
            <name>Send_Account_Verification_Notification_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS( Account.RecordTypeId , &quot;01220000000IvTI&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ULC%3A Populate ULC Old name</fullName>
        <actions>
            <name>ULCUpdateULC_OldwithULC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.ULC_Old_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.ULC__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Run the update to copy data from ULC name to ULC Old Name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Partner Owned Contact</fullName>
        <actions>
            <name>Update_Partner_Owned_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Partner Owned Contact field to TRUE if a Partner created the Contact</description>
        <formula>OR( CONTAINS($Profile.Name, &quot;PRM&quot;), ISPICKVAL(LeadSource, &quot;PART - Partner&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Route Around to EIS%2FISR</fullName>
        <actions>
            <name>Update_Route_Around_to_EIS_ISR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.High_Quality_Campaign_Response__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If High Quality Campaign Response field is set to True, then set EIS/ISR Route Around to true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status If Set To Junk</fullName>
        <actions>
            <name>Set_Contact_Status_To_Follow_Up_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Follow_Up_Rejected_Reason_To_Junk</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If previous Contact Status was Follow-Up Required or Follow-Up Attempt 1,2 or 3 and Contact Status is updated to Junk, then update status to follow-up rejected with a follow-up rejected reason of Junk.</description>
        <formula>AND (     ISCHANGED(Contact_Status__c),     OR     (         ISPICKVAL(PRIORVALUE(Contact_Status__c), &quot;Follow-Up Required&quot;),         ISPICKVAL(PRIORVALUE(Contact_Status__c), &quot;Follow-Up Attempt 1&quot;),         ISPICKVAL(PRIORVALUE(Contact_Status__c), &quot;Follow-Up Attempt 2&quot;),         ISPICKVAL(PRIORVALUE(Contact_Status__c), &quot;Follow-Up Attempt 3&quot;)     ),     ISPICKVAL(Contact_Status__c, &quot;Junk&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
