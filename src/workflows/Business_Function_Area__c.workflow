<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Functional_Area_Name</fullName>
        <field>Name</field>
        <formula>TEXT( Function__c )</formula>
        <name>Update Functional Area Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Aerospace_Defense</fullName>
        <field>Industry__c</field>
        <literalValue>Aerospace &amp; Defense</literalValue>
        <name>Update Industry - Aerospace &amp; Defense</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Automotive</fullName>
        <field>Industry__c</field>
        <literalValue>Automotive</literalValue>
        <name>Update Industry - Automotive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Consumer_Products</fullName>
        <field>Industry__c</field>
        <literalValue>Consumer Products</literalValue>
        <name>Update Industry - Consumer Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Education_Non_Profits</fullName>
        <field>Industry__c</field>
        <literalValue>Education &amp; Non-Profits</literalValue>
        <name>Update Industry -Education &amp; Non-Profits</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Engineering_Construct</fullName>
        <field>Industry__c</field>
        <literalValue>Engineering, Construction &amp; Real Estate</literalValue>
        <name>Update Industry - Engineering, Construct</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Government</fullName>
        <field>Industry__c</field>
        <literalValue>Government</literalValue>
        <name>Update Industry - Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Healthcare</fullName>
        <field>Industry__c</field>
        <literalValue>Healthcare</literalValue>
        <name>Update Industry - Healthcare</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_High_Tech</fullName>
        <field>Industry__c</field>
        <literalValue>High Tech</literalValue>
        <name>Update Industry - High Tech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Industrial_Machinery</fullName>
        <field>Industry__c</field>
        <literalValue>Industrial Machinery</literalValue>
        <name>Update Industry - Industrial Machinery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Insurance</fullName>
        <field>Industry__c</field>
        <literalValue>Insurance</literalValue>
        <name>Update Industry - Insurance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Media</fullName>
        <field>Industry__c</field>
        <literalValue>Media</literalValue>
        <name>Update Industry - Media</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Medical_Devices</fullName>
        <field>Industry__c</field>
        <literalValue>Medical Devices</literalValue>
        <name>Update Industry - Medical Devices</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Mill_Products</fullName>
        <field>Industry__c</field>
        <literalValue>Mill Products</literalValue>
        <name>Update Industry - Mill Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Natural_Resources</fullName>
        <field>Industry__c</field>
        <literalValue>Natural Resources</literalValue>
        <name>Update Industry - Natural Resources</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Oil_Gas_Chemicals</fullName>
        <field>Industry__c</field>
        <literalValue>Oil, Gas &amp; Chemicals</literalValue>
        <name>Update Industry - Oil, Gas &amp; Chemicals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Pharmaceuticals</fullName>
        <field>Industry__c</field>
        <literalValue>Pharmaceuticals</literalValue>
        <name>Update Industry - Pharmaceuticals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Retail_Banking</fullName>
        <field>Industry__c</field>
        <literalValue>Retail Banking</literalValue>
        <name>Update Industry - Retail Banking</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Retail_Wholesale_Dis</fullName>
        <field>Industry__c</field>
        <literalValue>Retail &amp; Wholesale Distribution</literalValue>
        <name>Update Industry - Retail &amp; Wholesale Dis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Securities_Investmen</fullName>
        <field>Industry__c</field>
        <literalValue>Securities &amp; Investments</literalValue>
        <name>Update Industry - Securities &amp; Investmen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Services</fullName>
        <field>Industry__c</field>
        <literalValue>Services</literalValue>
        <name>Update Industry - Services</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Telecommunications</fullName>
        <field>Industry__c</field>
        <literalValue>Telecommunications</literalValue>
        <name>Update Industry - Telecommunications</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Transportation_Logis</fullName>
        <field>Industry__c</field>
        <literalValue>Transportation &amp; Logistics</literalValue>
        <name>Update Industry - Transportation &amp; Logis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Travel_Hospitality</fullName>
        <field>Industry__c</field>
        <literalValue>Travel, Hospitality &amp; Entertainment</literalValue>
        <name>Update Industry - Travel, Hospitality &amp;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Industry_Utilities</fullName>
        <field>Industry__c</field>
        <literalValue>Utilities</literalValue>
        <name>Update Industry - Utilities</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Industry Field - Aerospace %26 Defense</fullName>
        <actions>
            <name>Update_Industry_Aerospace_Defense</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Aerospace &amp; Defense</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Automotive</fullName>
        <actions>
            <name>Update_Industry_Automotive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Automotive</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Consumer Products</fullName>
        <actions>
            <name>Update_Industry_Consumer_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Consumer Products</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Education %26 Non-Profits</fullName>
        <actions>
            <name>Update_Industry_Education_Non_Profits</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Education &amp; Non-Profits</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Engineering%2C Construction %26 Real Estate</fullName>
        <actions>
            <name>Update_Industry_Engineering_Construct</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>contains</operation>
            <value>Engineering,Construction &amp; Real Estate</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Government</fullName>
        <actions>
            <name>Update_Industry_Government</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Government</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Healthcare</fullName>
        <actions>
            <name>Update_Industry_Healthcare</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Healthcare</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - High Tech</fullName>
        <actions>
            <name>Update_Industry_High_Tech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>High Tech</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Industrial Machinery</fullName>
        <actions>
            <name>Update_Industry_Industrial_Machinery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Industrial Machinery</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Insurance</fullName>
        <actions>
            <name>Update_Industry_Insurance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Insurance</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Media</fullName>
        <actions>
            <name>Update_Industry_Media</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Media</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Medical Devices</fullName>
        <actions>
            <name>Update_Industry_Medical_Devices</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Medical Devices</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Mill Products</fullName>
        <actions>
            <name>Update_Industry_Mill_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Mill Products</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Natural Resources</fullName>
        <actions>
            <name>Update_Industry_Natural_Resources</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Natural Resources</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Oil%2C Gas %26 Chemicals</fullName>
        <actions>
            <name>Update_Industry_Oil_Gas_Chemicals</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Oil,Gas &amp; Chemicals</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Pharmaceuticals</fullName>
        <actions>
            <name>Update_Industry_Pharmaceuticals</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Pharmaceuticals</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Retail %26 Wholesale Distribution</fullName>
        <actions>
            <name>Update_Industry_Retail_Wholesale_Dis</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Retail &amp; Wholesale Distribution</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Retail Banking</fullName>
        <actions>
            <name>Update_Industry_Retail_Banking</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Retail Banking</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Securities %26 Investments</fullName>
        <actions>
            <name>Update_Industry_Securities_Investmen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Securities &amp; Investments</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Services</fullName>
        <actions>
            <name>Update_Industry_Services</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Services</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Telecommunications</fullName>
        <actions>
            <name>Update_Industry_Telecommunications</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Telecommunications</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Transportation %26 Logistics</fullName>
        <actions>
            <name>Update_Industry_Transportation_Logis</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Transportation &amp; Logistics</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Travel%2C Hospitality %26 Entertainment</fullName>
        <actions>
            <name>Update_Industry_Travel_Hospitality</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>contains</operation>
            <value>Travel,Hospitality &amp; Entertainment</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Industry Field - Utilities</fullName>
        <actions>
            <name>Update_Industry_Utilities</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Industry_Formula_Field__c</field>
            <operation>equals</operation>
            <value>Utilities</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update functional Area Name</fullName>
        <actions>
            <name>Update_Functional_Area_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Function_Area__c.Function__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
