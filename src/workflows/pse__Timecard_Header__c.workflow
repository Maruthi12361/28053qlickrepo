<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Submitter_of_Rejection</fullName>
        <description>Notify Submitter of Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>pse__Resource__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/Timecard_Rejected</template>
    </alerts>
    <alerts>
        <fullName>PSE_Timecard_Send_Approval_Email</fullName>
        <description>PSE Timecard Send Approval Email</description>
        <protected>false</protected>
        <recipients>
            <field>pse__Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/PSE_Timecard_Approval_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Cost_Rate_Timecard</fullName>
        <field>Cost_Rate_PSE_Time__c</field>
        <formula>pse__Assignment__r.Cost_Rate_PSE__c</formula>
        <name>Cost Rate Timecard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Recall</fullName>
        <field>pse__Status__c</field>
        <literalValue>Saved</literalValue>
        <name>PSE Timecard Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Reject_Timecard</fullName>
        <field>pse__Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>PSE Timecard Reject Timecard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Set_Approved_Flag</fullName>
        <field>pse__Approved__c</field>
        <literalValue>1</literalValue>
        <name>PSE Timecard Set Approved Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Set_Include_in_FI_Flag</fullName>
        <field>pse__Include_In_Financials__c</field>
        <literalValue>1</literalValue>
        <name>PSE Timecard Set Include in FI Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Set_Status_to_Approved</fullName>
        <field>pse__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>PSE Timecard Set Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Set_Submitted_Flag</fullName>
        <field>pse__Submitted__c</field>
        <literalValue>1</literalValue>
        <name>PSE Timecard Set Submitted Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Status_Saved</fullName>
        <field>pse__Status__c</field>
        <literalValue>Saved</literalValue>
        <name>PSE Timecard Status Saved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Unset_Approved_Flag</fullName>
        <field>pse__Approved__c</field>
        <literalValue>0</literalValue>
        <name>PSE Timecard Unset Approved Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Unset_Include_in_FI_Flag</fullName>
        <field>pse__Include_In_Financials__c</field>
        <literalValue>0</literalValue>
        <name>PSE Timecard Unset Include in FI Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Timecard_Unset_Submitted_Flag</fullName>
        <field>pse__Submitted__c</field>
        <literalValue>0</literalValue>
        <name>PSE Timecard Unset Submitted Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Check_Approved_Checkbox</fullName>
        <field>pse__Approved__c</field>
        <literalValue>1</literalValue>
        <name>Timecard: Check Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Check_Include_In_Financials</fullName>
        <field>pse__Include_In_Financials__c</field>
        <literalValue>1</literalValue>
        <name>Timecard: Check Include In Financials</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Check_Submitted_Checkbox</fullName>
        <field>pse__Submitted__c</field>
        <literalValue>1</literalValue>
        <name>Timecard: Check Submitted Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Uncheck_Approved_Checkbox</fullName>
        <field>pse__Approved__c</field>
        <literalValue>0</literalValue>
        <name>Timecard: Uncheck Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Uncheck_Include_In_Financials</fullName>
        <field>pse__Include_In_Financials__c</field>
        <literalValue>0</literalValue>
        <name>Timecard: Uncheck Include In Financials</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Uncheck_Submitted</fullName>
        <field>pse__Submitted__c</field>
        <literalValue>0</literalValue>
        <name>Timecard: Uncheck Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Update_Status_to_Approved</fullName>
        <field>pse__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Timecard: Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timecard_Update_Status_to_Rejected</fullName>
        <field>pse__Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Timecard: Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cost Rate Timecard</fullName>
        <actions>
            <name>Cost_Rate_Timecard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Timecard_Header__c.Cost_Rate_PSE_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Created as part of IT-1032 to update cost Rate fields on Assignment</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PSE Timecard Status Set Saved%2CRejected</fullName>
        <actions>
            <name>PSE_Timecard_Unset_Approved_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Timecard_Unset_Include_in_FI_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Timecard_Unset_Submitted_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timecard_Uncheck_Approved_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timecard_Uncheck_Include_In_Financials</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timecard_Uncheck_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Timecard_Header__c.pse__Status__c</field>
            <operation>equals</operation>
            <value>Saved,Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>pse__Timecard_Header__c.pse__Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This will unset the submitted check box on the timecard when the status is set to a value corresponding to not submitted. Customers may choose to change the values in the status drop down</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSE Timecard Status Set to Approved</fullName>
        <actions>
            <name>PSE_Timecard_Set_Approved_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Timecard_Set_Include_in_FI_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Timecard_Set_Submitted_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timecard_Check_Approved_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timecard_Check_Include_In_Financials</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timecard_Check_Submitted_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Timecard_Header__c.pse__Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>This workflow will mimic the approval process. When the timecard status is set to Approved the approved flag will get set along with the include in financials checkbox</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSE Timecard Status Set to Submitted</fullName>
        <actions>
            <name>PSE_Timecard_Set_Submitted_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Timecard_Check_Submitted_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Timecard_Header__c.pse__Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>This will set the submitted check box on the timecard when the status is set to a value corresponding to submitted. Customers may choose to change the values in the status drop down</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
