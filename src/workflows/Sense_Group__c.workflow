<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Sub_Id_Field</fullName>
        <description>Field update to enforce uniqueness.</description>
        <field>Sub_Id__c</field>
        <formula>Subscription_Id__c</formula>
        <name>Update Unique Sub Id Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Unqiue Subscription For Sense Group Rule</fullName>
        <actions>
            <name>Update_Unique_Sub_Id_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK( Subscription_Id__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
