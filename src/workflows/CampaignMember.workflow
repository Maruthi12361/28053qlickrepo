<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_First_Responded_Date_Admin</fullName>
        <field>First_Responded_Date_Admin__c</field>
        <formula>NOW()</formula>
        <name>Update First Responded Date Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update First Responded Date Admin</fullName>
        <actions>
            <name>Update_First_Responded_Date_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(     ISBLANK( First_Responded_Date_Admin__c ),     NOT(ISBLANK(FirstRespondedDate)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
