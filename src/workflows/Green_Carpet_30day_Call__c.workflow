<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GreenCarpet_Set_Date_Closed</fullName>
        <description>GreenCarpet Set Date Closed when Status Complete</description>
        <field>Date_Closed__c</field>
        <formula>Today()</formula>
        <name>GreenCarpet_Set_Date_Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GreenCarpet_Set_Date_Opened</fullName>
        <field>Date_Opened__c</field>
        <formula>CreatedDate</formula>
        <name>GreenCarpet_Set_Date_Opened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GreenCarpet_Set_Date_Closed</fullName>
        <actions>
            <name>GreenCarpet_Set_Date_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Green_Carpet_30day_Call__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>GreenCarpet Set Date Closed when Status is Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GreenCarpet_Set_Date_Opened</fullName>
        <actions>
            <name>GreenCarpet_Set_Date_Opened</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>GreenCarpet Set Date Opened</description>
        <formula>ISBLANK(Date_Opened__c)  || 
Date_Opened__c  &lt;&gt; DATEVALUE(CreatedDate)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
