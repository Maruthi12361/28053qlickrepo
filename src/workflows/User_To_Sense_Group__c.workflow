<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Member_Sub_Type_Field_Update</fullName>
        <field>Member_Subscription_Type__c</field>
        <formula>TEXT(Sense_Group_Id__r.Subscription_Id__r.Subscription_Type__c)</formula>
        <name>Member Sub Type Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Member Subscription Type</fullName>
        <actions>
            <name>Member_Sub_Type_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Implemented as part of QE-1300</description>
        <formula>NOT(ISBLANK( Sense_Group_Id__r.Subscription_Id__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
