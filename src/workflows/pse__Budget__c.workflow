<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PO_Number_update_Budget</fullName>
        <field>PO_Number__c</field>
        <formula>&quot;Signature Success&quot;</formula>
        <name>PO Number update Budget</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO_Number_update_Budget1</fullName>
        <field>PO_Number__c</field>
        <formula>&quot;na&quot;</formula>
        <name>PO Number update Budget</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO_Number_update_Budget2</fullName>
        <field>PO_Number__c</field>
        <formula>&quot;na&quot;</formula>
        <name>PO Number update Budget</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Signature Success-PO Number Budget-default</fullName>
        <actions>
            <name>PO_Number_update_Budget2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISPICKVAL(pse__Project__r.pse__Project_Type__c, &apos;Signature Success&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Signature Success-PO Number%09Budget</fullName>
        <actions>
            <name>PO_Number_update_Budget</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PO_Number_update_Budget1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(pse__Project__r.pse__Project_Type__c, &apos;Signature Success&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
