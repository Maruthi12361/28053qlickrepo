<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PSA_Set_Forecast_Category</fullName>
        <field>Forecast_Category__c</field>
        <formula>pse__Forecast_Setup_Category__r.pse__Label__c</formula>
        <name>PSA Set Forecast Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PSA Set Forecast Category</fullName>
        <actions>
            <name>PSA_Set_Forecast_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the value of the Forecast Category field from the Category label value on the Forecast detail record</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
