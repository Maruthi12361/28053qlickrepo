<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_Resource_Request_Has_Been_Created</fullName>
        <description>A Resource Request Has Been Created</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Resource_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/Resource_Request_Has_Been_Created_Custom</template>
    </alerts>
    <alerts>
        <fullName>Resource_Request_for_Europe_region</fullName>
        <ccEmails>DL-EMEAResourceManagement@qlik.com</ccEmails>
        <description>Resource Request for Europe region</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/Resource_Request_Has_Been_Created_Custom</template>
    </alerts>
    <fieldUpdates>
        <fullName>PSA_Populate_External_Resource</fullName>
        <field>External_Resource__c</field>
        <literalValue>1</literalValue>
        <name>PSA Populate External Resource</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSA_Set_Requested_Date</fullName>
        <description>Sets the Requested Date field to TODAY</description>
        <field>Requested_Date__c</field>
        <formula>TODAY()</formula>
        <name>PSA Set Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSA_Set_Status_Hold</fullName>
        <field>pse__Status__c</field>
        <literalValue>Hold</literalValue>
        <name>PSA Set Status Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PSA Populate External Resource</fullName>
        <actions>
            <name>PSA_Populate_External_Resource</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>pse__Resource_Request__c.pse__Resource_Role__c</field>
            <operation>equals</operation>
            <value>Contractor</value>
        </criteriaItems>
        <criteriaItems>
            <field>pse__Resource_Request__c.pse__Resource_Role__c</field>
            <operation>equals</operation>
            <value>Nearshore</value>
        </criteriaItems>
        <description>This rule sets the External Resource field to true if the Resource role is set to &quot;Contractor&quot; or &quot;Nearshore&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PSA Set Requested Date</fullName>
        <actions>
            <name>PSA_Set_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Requested date to the current date when the Status field is set to &quot;Ready to Staff&quot;</description>
        <formula>ISPICKVAL(pse__Status__c, &apos;Ready to Staff&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSA Set Status Hold</fullName>
        <actions>
            <name>PSA_Set_Status_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Resource_Request__c.pse__Resource_Held__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PSE Send Email Alert of a Europe resource request</fullName>
        <actions>
            <name>Resource_Request_for_Europe_region</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>IT-2059: Notify Service Ops when New Resource Request is created</description>
        <formula>AND(OR((ISPICKVAL(pse__Status__c,&quot;Draft&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Ready to Staff&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Hold&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Tentative&quot;))), 
(pse__Region__r.Resource_Api__c == &quot;EUROPE&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PSE Send Email Alert of a New Resource Request</fullName>
        <actions>
            <name>A_Resource_Request_Has_Been_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Service Ops when New Resource Request is created</description>
        <formula>AND(OR((ISPICKVAL(pse__Status__c,&quot;Draft&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Ready to Staff&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Hold&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Tentative&quot;))), 
(pse__Region__r.Resource_Api__c &lt;&gt; &quot;EUROPE&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
