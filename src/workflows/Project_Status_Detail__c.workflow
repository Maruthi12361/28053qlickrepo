<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PSA_Unset_Include_in_Status_Report</fullName>
        <description>Unchecks the &apos;Include in Status Report&apos; field when the Open/Closed field = Closed</description>
        <field>Include_in_Status_Report__c</field>
        <literalValue>0</literalValue>
        <name>PSA Unset Include in Status Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PSA Unset Include in Status Report</fullName>
        <actions>
            <name>PSA_Unset_Include_in_Status_Report</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Status_Detail__c.Open_Closed__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Unchecks the &apos;Include in Status Report&apos; field when the Open/Closed field = Closed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
