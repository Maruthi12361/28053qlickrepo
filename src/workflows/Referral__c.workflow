<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Referral_Acceptance_Email</fullName>
        <description>Referral Acceptance Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Your_Qlik</template>
    </alerts>
    <alerts>
        <fullName>Referral_Invoice_for_Payment_Email</fullName>
        <description>Referral Invoice for Payment Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Your_Qlik_Referral_is_Ready_for_Invoice</template>
    </alerts>
    <alerts>
        <fullName>Referral_Rejection_Email</fullName>
        <description>Referral Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Your_Qlik_Reject</template>
    </alerts>
    <alerts>
        <fullName>Referral_Submission_Email</fullName>
        <description>Referral Submission Email</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Partner_Referral_Registration_Request_Requires_Your_Attention</template>
    </alerts>
    <alerts>
        <fullName>Your_Qlik_Referral_Registration_is_Pending_Review</fullName>
        <description>Your Qlik Referral Registration is Pending Review</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Approval_Email_Templates/Your_Qlik_Referral_Registration_is_Pending_Review</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Referral_Status</fullName>
        <description>When a user selects the Submit for Approval button, the Referral status updates to Pending.</description>
        <field>Referral_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Update Referral Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Status_to_Accepted</fullName>
        <description>Update Referral Status to Accepted upon approval</description>
        <field>Referral_Status__c</field>
        <literalValue>Pending SSD Approval</literalValue>
        <name>Update Referral Status to Pending SSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Status_to_Draft</fullName>
        <field>Referral_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update Referral Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Status_to_Rejected</fullName>
        <description>Update Referral Status to Rejected upon rejection fo the approval process</description>
        <field>Referral_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Referral Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
