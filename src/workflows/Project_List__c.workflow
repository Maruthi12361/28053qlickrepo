<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_PMO_about_IS_Required</fullName>
        <ccEmails>DL-IS-PMO@qlik.com</ccEmails>
        <ccEmails>DL-SystemsManagers@qlik.com</ccEmails>
        <description>Notify PMO about &quot;IS Required&quot;</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PMO_Project_Emails/Is_Required_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Date_Completed</fullName>
        <description>Clear &quot;Date Completed&quot; field</description>
        <field>Date_Completed__c</field>
        <name>Clear &quot;Date Completed&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Date_Completed</fullName>
        <description>Populate &quot;Date Completed&quot; field</description>
        <field>Date_Completed__c</field>
        <formula>NOW()</formula>
        <name>Populate &quot;Date Completed&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Clear %22Date Completed%22</fullName>
        <actions>
            <name>Clear_Date_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_List__c.Phase__c</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify PMO about %22IS Required%22</fullName>
        <actions>
            <name>Notify_PMO_about_IS_Required</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_List__c.IS_Required__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Notify PMO when &quot;IS Required&quot; field is true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Systems corporate services of new Project</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Project_List__c.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>HR,Finance,IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project_List__c.Approved_by__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>notifies the corporate services team when a new project is raised in their business areas.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set %22Date Completed%22</fullName>
        <actions>
            <name>Populate_Date_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_List__c.Phase__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
