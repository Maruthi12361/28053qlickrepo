<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MD_is_being_handled</fullName>
        <description>MD is being handled</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Request/Marketing_Deliverable</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_automatically</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved automatically</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Requested_Due_Date_for_MR</fullName>
        <field>Requested_Due_Date__c</field>
        <formula>TODAY() + 3</formula>
        <name>Requested Due Date for MR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status = Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Denied</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Denied</literalValue>
        <name>Status = Denied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Recalled</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Status = Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Sent_for_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Status = Sent for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Date_on_Marketing_Delivera</fullName>
        <description>Update Closed Date on Marketing Deliverable when Approval Status is equals to Recalled or Denied or Completed</description>
        <field>Date_Closed__c</field>
        <formula>TODAY()</formula>
        <name>Update Closed Date on Marketing Delivera</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approves automatically Marketing Deliverable</fullName>
        <actions>
            <name>Approved_automatically</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>The Marketing Deliverable called Target / Response Upload is automatically approved if Affected # Records is below 3000</description>
        <formula>AND(  ISPICKVAL( Approval_Status__c, &quot;New&quot;),  RecordTypeId = &apos;012D0000000JrqP&apos;,  Affected_Records__c &lt;= 3000  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fills in the Closed Date when Status set to Closed</fullName>
        <actions>
            <name>Update_Closed_Date_on_Marketing_Delivera</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Fills in the Closed Date when &quot;Approval Status&quot; values equals Completed or Recalled or Denied</description>
        <formula>OR( ISPICKVAL(Approval_Status__c , &quot;Completed&quot;), ISPICKVAL(Approval_Status__c , &quot;Recalled&quot;), ISPICKVAL(Approval_Status__c , &quot;Denied&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MD is being handled</fullName>
        <actions>
            <name>MD_is_being_handled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Deliverable__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>In Process</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Requested Due Date</fullName>
        <actions>
            <name>Requested_Due_Date_for_MR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Add 3 days to the current date ti set the Requested Due Dates</description>
        <formula>CreatedById &lt;&gt; &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
