<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Reminder_to_IS_Resource</fullName>
        <description>Reminder to IS Resource</description>
        <protected>false</protected>
        <recipients>
            <field>IS_Resource_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IS_Resource_Request/Reminder_to_IS_Resource</template>
    </alerts>
    <fieldUpdates>
        <fullName>ISA_Calculate_Assigned_Until_date_time</fullName>
        <description>Calculate &quot;Assigned Until&quot; date/time by adding value from &quot;Assigned Days&quot; field.</description>
        <field>Assigned_Until__c</field>
        <formula>Assigned_From__c    +  Assigned_Days__c</formula>
        <name>ISA:Calculate &quot;Assigned Until&quot; date/time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISA_Populate_IS_Resource_Team_text</fullName>
        <description>Update &quot;IS Resource Team text&quot; value</description>
        <field>IS_Resource_Team_text__c</field>
        <formula>IS_Resource__r.IS_Team__r.Name</formula>
        <name>ISA: Populate IS Resource Team text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISA_Populate_IS_Resource_email</fullName>
        <field>IS_Resource_email__c</field>
        <formula>IS_Resource__r.Email__c</formula>
        <name>ISA: Populate IS Resource email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISA_Populate_requested_from_date</fullName>
        <description>When triggered, adds a the value on the resource request Earliest Start date Field to the requested from field on the IS Assignment.</description>
        <field>Requested_From__c</field>
        <formula>IS_Resource_request__r.Earliest_start_date__c</formula>
        <name>ISA: Populate requested from date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISA_Populate_requested_to_date</fullName>
        <description>Populates the IS Assignment requested to date based on the value of the required by field on the resource request if the field is left blank.</description>
        <field>Requested_Until__c</field>
        <formula>IS_Resource_request__r.Required_by__c</formula>
        <name>ISA: Populate requested to date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>ISA Populate IS Resource email and team_text</fullName>
        <actions>
            <name>ISA_Populate_IS_Resource_Team_text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ISA_Populate_IS_Resource_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>IS_Assignment__c.IS_Resource_Team_text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>IS_Assignment__c.IS_Resource_Team_text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ISA%3A Populate %22Assigned Until%22 date%2Ftime field</fullName>
        <actions>
            <name>ISA_Calculate_Assigned_Until_date_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates &quot;Assigned Until&quot; date/time field, based on the change of value in the assigned from and assigned until fields on the assignment</description>
        <formula>OR
(
ISCHANGED
   ( Assigned_Days__c ),
ISCHANGED
   (Assigned_From__c ),
Not
   (
   ISBLANK
      (
      Assigned_Days__c
      ) 
   )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ISA%3A Populate requested from date</fullName>
        <actions>
            <name>ISA_Populate_requested_from_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IS_Assignment__c.Requested_From__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If requested from is blank on the IS Assignment populate with the equivalent from the resource request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISA%3A Populate requested to date</fullName>
        <actions>
            <name>ISA_Populate_requested_to_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IS_Assignment__c.Requested_Until__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If requested from is blank on the IS Assignment populate with the equivalent from the resource request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISA%3A Reminder to IS Resource</fullName>
        <active>true</active>
        <criteriaItems>
            <field>IS_Assignment__c.Assigned_From__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>IS_Assignment__c.Status__c</field>
            <operation>equals</operation>
            <value>Resource Confirmed,Soft Booked</value>
        </criteriaItems>
        <criteriaItems>
            <field>IS_ResourceRequest__c.Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>IS_ResourceRequest__c.Category__c</field>
            <operation>notEqual</operation>
            <value>Sick,Annual Leave</value>
        </criteriaItems>
        <criteriaItems>
            <field>IS_Assignment__c.Assigned_From__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>reminder sent to booked IS Resource 1 hour before booking starts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_to_IS_Resource</name>
                <type>Alert</type>
            </actions>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
