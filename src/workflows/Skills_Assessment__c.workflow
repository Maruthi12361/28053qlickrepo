<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SA_Email_User_Data_Architect</fullName>
        <description>SA: Email User [Data Architect]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_Data_Architect</template>
    </alerts>
    <alerts>
        <fullName>SA_Email_User_Data_Literacy</fullName>
        <description>SA: Email User [Data Literacy]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_Data_Literacy</template>
    </alerts>
    <alerts>
        <fullName>SA_Email_User_Developer</fullName>
        <description>SA: Email User [Developer]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_Developer</template>
    </alerts>
    <alerts>
        <fullName>SA_Email_User_Enterprise_Architect</fullName>
        <description>SA: Email User [Enterprise Architect]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_Enterprise_Architect</template>
    </alerts>
    <alerts>
        <fullName>SA_Email_User_Qlik_Administrator</fullName>
        <description>SA: Email User [Qlik Administrator]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_Qlik_Administrator</template>
    </alerts>
    <alerts>
        <fullName>SA_Email_User_SysAdmin</fullName>
        <description>SA: Email User [SysAdmin]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_System_Administrator</template>
    </alerts>
    <alerts>
        <fullName>SA_Email_User_UI_Designer</fullName>
        <description>SA: Email User [UI Designer]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_UI_Designer</template>
    </alerts>
    <alerts>
        <fullName>SA_Email_User_Visualization_Author</fullName>
        <description>SA: Email User [Visualization Author]</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skills_Assessments/SkillsAssessments_Visualization_Author</template>
    </alerts>
    <fieldUpdates>
        <fullName>SA_Update_Email_Sent_Field</fullName>
        <description>Skills Assessments: Update Email Sent Field</description>
        <field>Email_Sent__c</field>
        <formula>NOW()</formula>
        <name>SA: Update Email Sent Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SA%3A Send Score to User %5BData Architect%5D</fullName>
        <actions>
            <name>SA_Email_User_Data_Architect</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [Data Architect]</description>
        <formula>RecordType.DeveloperName = &apos;Data_Architects&apos; &amp;&amp;  ISBLANK(Email_Sent__c) &amp;&amp;  NOT(ISBLANK(Email__c)) &amp;&amp;  ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SA%3A Send Score to User %5BData Literacy%5D</fullName>
        <actions>
            <name>SA_Email_User_Data_Literacy</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [Data Literacy]</description>
        <formula>RecordTypeId = &apos;012D0000000kiQ5&apos; &amp;&amp; ISBLANK(Email_Sent__c) &amp;&amp; NOT(ISBLANK(Email__c)) &amp;&amp; ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SA%3A Send Score to User %5BDesigner%5D</fullName>
        <actions>
            <name>SA_Email_User_UI_Designer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [Designer]</description>
        <formula>RecordTypeId = &apos;012D0000000KFAK&apos; &amp;&amp; 
ISBLANK(Email_Sent__c) &amp;&amp; 
NOT(ISBLANK(Email__c)) &amp;&amp; 
ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SA%3A Send Score to User %5BDeveloper%5D</fullName>
        <actions>
            <name>SA_Email_User_Developer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [Developer]</description>
        <formula>RecordTypeId = &apos;012D0000000KFAI&apos; &amp;&amp; 
ISBLANK(Email_Sent__c) &amp;&amp; 
NOT(ISBLANK(Email__c)) &amp;&amp; 
ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SA%3A Send Score to User %5BEnterprise Architect%5D</fullName>
        <actions>
            <name>SA_Email_User_Enterprise_Architect</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [Enterprise Architect]</description>
        <formula>RecordType.DeveloperName = &apos;Enterprise_Architects&apos; &amp;&amp;  ISBLANK(Email_Sent__c) &amp;&amp;  NOT(ISBLANK(Email__c)) &amp;&amp;  ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SA%3A Send Score to User %5BQlik Administrator%5D</fullName>
        <actions>
            <name>SA_Email_User_Qlik_Administrator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [Qlik Administrator]</description>
        <formula>RecordType.DeveloperName = &apos;Qlik_Administrators&apos; &amp;&amp;  ISBLANK(Email_Sent__c) &amp;&amp;  NOT(ISBLANK(Email__c)) &amp;&amp;  ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SA%3A Send Score to User %5BSysAdmin%5D</fullName>
        <actions>
            <name>SA_Email_User_SysAdmin</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [System Administrator]</description>
        <formula>RecordTypeId = &apos;012D0000000KFAJ&apos; &amp;&amp; 
ISBLANK(Email_Sent__c) &amp;&amp; 
NOT(ISBLANK(Email__c)) &amp;&amp; 
ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SA%3A Send Score to User %5BVisualization Author%5D</fullName>
        <actions>
            <name>SA_Email_User_Visualization_Author</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SA_Update_Email_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Skills Assessment: Send Email to User when a record is created [Visualization Author]</description>
        <formula>RecordType.DeveloperName = &apos;Visualization_Authors&apos; &amp;&amp;  ISBLANK(Email_Sent__c) &amp;&amp;  NOT(ISBLANK(Email__c)) &amp;&amp;  ISBLANK(Groupnumber__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
