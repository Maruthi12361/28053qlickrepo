<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PSE_Milestone_Check_ApprovedBilling</fullName>
        <field>pse__Approved_for_Billing__c</field>
        <literalValue>1</literalValue>
        <name>PSE Milestone Check ApprovedBilling</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Milestone_Check_ClosedTimeEntry</fullName>
        <field>pse__Closed_for_Time_Entry__c</field>
        <literalValue>1</literalValue>
        <name>PSE Milestone Check ClosedTimeEntry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Milestone_Uncheck_ClosedTimeEntry</fullName>
        <field>pse__Closed_for_Time_Entry__c</field>
        <literalValue>0</literalValue>
        <name>PSE Milestone Uncheck ClosedTimeEntry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Milestone_Unchk_ApprovedBilling</fullName>
        <field>pse__Approved_for_Billing__c</field>
        <literalValue>0</literalValue>
        <name>PSE Milestone Unchk ApprovedBilling</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PSE Milestone Status Set to Approved</fullName>
        <actions>
            <name>PSE_Milestone_Check_ApprovedBilling</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Milestone_Check_Approved_Chkbx</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Milestone_Check_ClosedTimeEntry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Milestone_Check_Include_In_FI_Chkbx</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Milestone__c.pse__Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>This workflow will mimic the approval process. When the milestones status is set to Approved the approved flag will get set along with the include in financials checkbox</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSE Milestone Status Set to Planned%2C Open%2C Canceled</fullName>
        <actions>
            <name>PSE_Milestone_Uncheck_Approved_Chkbx</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Milestone_Uncheck_ClosedTimeEntry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Milestone_Unchk_ApprovedBilling</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PSE_Milestone_Unchk_Include_In_FI_Chkbx</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Milestone__c.pse__Status__c</field>
            <operation>equals</operation>
            <value>Canceled,Planned,Open</value>
        </criteriaItems>
        <description>This workflow will mimic the rejection process. When the milestones status is set to Planned, Open, Canceled the approved flag will get unset along with the include in financials checkbox</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
