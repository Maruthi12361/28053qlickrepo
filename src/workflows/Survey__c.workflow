<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Signature_Type_Incident</fullName>
        <description>Signature Type Incident</description>
        <protected>false</protected>
        <recipients>
            <recipient>Signature_Type_Incident</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Bangalore_Type_Incident</fullName>
        <description>Support Off Bangalore Type Incident</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Bangalore_Type_Incident</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Bangalore_Type_Service_Req</fullName>
        <description>Support Off Bangalore Type Service Req</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Bangalore_Type_Service_Req</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Lund_Type_Incident</fullName>
        <description>Support Off Lund Type Incident</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Lund_Type_Incident</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Lund_Type_Service_Req</fullName>
        <description>Support Off Lund Type Service Req</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Lund_Type_Service_Req</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Raleigh_Type_Incident</fullName>
        <description>Support Off Raleigh Type Incident</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Raleigh_Type_Incident</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Raleigh_Type_Service_Req</fullName>
        <description>Support Off Raleigh Type Service Req</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Raleigh_Type_Service_Req</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Sydney_Type_Incident</fullName>
        <description>Support Off Sydney Type Incident</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Sydney_Type_Incident</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Sydney_Type_Service_Req</fullName>
        <description>Support Off Sydney Type Service Req</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Sydney_Type_Service_Req</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Tokyo_Type_Incident</fullName>
        <description>Support Off Tokyo Type Incident</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Tokyo_Type_Incident</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Support_Off_Tokyo_Type_Service_Req</fullName>
        <description>Support Off Tokyo Type Service Req</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Off_Tokyo_Type_Service_Req</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/VoCC_Notification</template>
    </alerts>
    <rules>
        <fullName>Change Blanks to 0</fullName>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8</booleanFilter>
        <criteriaItems>
            <field>Survey__c.Survey_Question_1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.Survey_Question_2a__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.Survey_Question_2b__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.Survey_Question_2c__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.Survey_Question_2d__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.Survey_Question_3a__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.Survey_Question_3b__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.Survey_Question_5__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Signature Type Incident</fullName>
        <actions>
            <name>Signature_Type_Incident</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (     Case_ID__r.Premier_Support__c = true,     TEXT(Case_ID__r.Type) = &apos;Incident&apos;,     LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/,    OR (  OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;),  OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;),  NOT( ISBLANK(Support_Survey_Q6__c) ),  OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)   )    )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Bangalore Type Incident</fullName>
        <actions>
            <name>Support_Off_Bangalore_Type_Incident</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (Case_ID__r.Premier_Support__c = false, Case_ID__r.Account.Support_Office__r.Name = &apos;Bangalore&apos;,    TEXT(Case_ID__r.Type) = &apos;Incident&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),   OR   (     OR   (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;),     OR   (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;),       NOT( ISBLANK(Support_Survey_Q6__c) ),     OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6) )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Bangalore Type Service Req</fullName>
        <actions>
            <name>Support_Off_Bangalore_Type_Service_Req</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Account.Support_Office__r.Name = &apos;Bangalore&apos;,    TEXT(Case_ID__r.Type) = &apos;Service Request&apos;,    OR (     LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/,     LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/    ),  OR (     OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;),     OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;),     NOT( ISBLANK(Support_Survey_Q6__c)),     OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6) )   )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Lund Type Incident</fullName>
        <actions>
            <name>Support_Off_Lund_Type_Incident</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Premier_Support__c = false, Case_ID__r.Account.Support_Office__r.Name = &apos;Lund&apos;,    TEXT(Case_ID__r.Type) = &apos;Incident&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),   OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )   )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Lund Type Service Req</fullName>
        <actions>
            <name>Support_Off_Lund_Type_Service_Req</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Account.Support_Office__r.Name = &apos;Lund&apos;,    TEXT(Case_ID__r.Type) = &apos;Service Request&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),     OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )   )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Raleigh Type Incident</fullName>
        <actions>
            <name>Support_Off_Raleigh_Type_Incident</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Premier_Support__c = false, Case_ID__r.Account.Support_Office__r.Name = &apos;Raleigh&apos;,    TEXT(Case_ID__r.Type) = &apos;Incident&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),   OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Raleigh Type Service Req</fullName>
        <actions>
            <name>Support_Off_Raleigh_Type_Service_Req</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Account.Support_Office__r.Name = &apos;Raleigh&apos;,    TEXT(Case_ID__r.Type) = &apos;Service Request&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),     OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Sydney Type Incident</fullName>
        <actions>
            <name>Support_Off_Sydney_Type_Incident</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Premier_Support__c = false, Case_ID__r.Account.Support_Office__r.Name = &apos;Sydney&apos;,    TEXT(Case_ID__r.Type) = &apos;Incident&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),   OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Sydney Type Service Req</fullName>
        <actions>
            <name>Support_Off_Sydney_Type_Service_Req</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Account.Support_Office__r.Name = &apos;Sydney&apos;,    TEXT(Case_ID__r.Type) = &apos;Service Request&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),      OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Tokyo Type Incident</fullName>
        <actions>
            <name>Support_Off_Tokyo_Type_Incident</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Premier_Support__c = false, Case_ID__r.Account.Support_Office__r.Name = &apos;Tokyo&apos;,    TEXT(Case_ID__r.Type) = &apos;Incident&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),   OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Off Tokyo Type Service Req</fullName>
        <actions>
            <name>Support_Off_Tokyo_Type_Service_Req</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to selected support personnel if certain criteria are met.</description>
        <formula>AND (    Case_ID__r.Account.Support_Office__r.Name = &apos;Tokyo&apos;,    TEXT(Case_ID__r.Type) = &apos;Service Request&apos;,    OR ( LEFT(RecordTypeId, 15) = &apos;012D0000000K1kE&apos; /*Record Type = Support Case*/, LEFT(RecordTypeId, 15) = &apos;012D0000000KEJu&apos; /*Record Type = Qoncierge Chat*/ ),     OR ( OR (CES__c = &apos;Strongly Disagree&apos;, CES__c = &apos;Disagree&apos;), OR (CSAT__c = &apos;Very Dissatisfied&apos;, CSAT__c = &apos;Dissatisfied&apos;), NOT( ISBLANK(Support_Survey_Q6__c) ), OR (NPS__c = 0, NPS__c = 1, NPS__c = 2, NPS__c = 3, NPS__c = 4, NPS__c = 5, NPS__c = 6)  )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
