<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>QVM_Product_Approved_on_Qlikmarket</fullName>
        <description>QVM Product Approved on Qlikmarket</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>QlikMarket_Templates/QVM_Product_Approved_on_Qlikmarket</template>
    </alerts>
    <alerts>
        <fullName>QVM_QlikMarket_Product_Submission_Failed</fullName>
        <description>QVM QlikMarket Product Submission Failed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>QlikMarket_Templates/QVM_Product_Rejected_on_Qlikmarket</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Rejection_Send_Email_Checkbox</fullName>
        <field>Rejection_Send_Email__c</field>
        <literalValue>0</literalValue>
        <name>Clear Rejection Send Email Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Approved_Rejected_Time_Stamp</fullName>
        <field>Approved_Rejected_On__c</field>
        <formula>NOW()</formula>
        <name>QVM Approved / Rejected Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Available_to_Publish</fullName>
        <description>Product goes into &quot;Available to Publish&quot; status.</description>
        <field>Status__c</field>
        <literalValue>Ready to Publish</literalValue>
        <name>QVM Available to Publish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Set_Status_to_Draft</fullName>
        <description>On rejection set the status from &quot;Submitted&quot; back to Draft.</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>QVM Set Status to &quot;Draft&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Submitted_Time_Stamp</fullName>
        <field>Submitted_For_Approval__c</field>
        <formula>NOW()</formula>
        <name>QVM Submitted Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rejection_Send_Email_Checkbox</fullName>
        <field>Rejection_Send_Email__c</field>
        <literalValue>1</literalValue>
        <name>Set Rejection Send Email Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Rejection Message Send</fullName>
        <actions>
            <name>QVM_QlikMarket_Product_Submission_Failed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Clear_Rejection_Send_Email_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send Rejection email when Rejection Comments, Rejection Reason and Rejection Send Email fields are populated.</description>
        <formula>AND(     Rejection_Send_Email__c == TRUE,     LEN(Rejection_Comment__c ) &lt;&gt; 0,     IF(ISBLANK(Rejection_Reason_multiselect__c), FALSE, TRUE)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
