<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Survey_Task_Send_Email_Notification</fullName>
        <description>Survey Task Send Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Survey_Task</template>
    </alerts>
    <fieldUpdates>
        <fullName>Uncheck_Send</fullName>
        <field>Send_Notification__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Customer Survey Task Postmortem Status email notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Cutomer_Survey_Task__c.Postmortem_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Survey Task Send Notification</fullName>
        <actions>
            <name>Survey_Task_Send_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Send</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Cutomer_Survey_Task__c.Send_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
