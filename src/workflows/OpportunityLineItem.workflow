<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PRMOpptyNotifyOpportunityOwner</fullName>
        <description>PRM: Oppty: Notify Opportunity Owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Idea_E_Mail_Templates/New_Idea_Notification_CONNECTORS</template>
    </alerts>
    <fieldUpdates>
        <fullName>CalculateNetSalesPrice</fullName>
        <description>Calculated net sales price applying discount and referral/partner margin</description>
        <field>UnitPrice</field>
        <formula>IF (ISNULL(Discount__c), 
    IF (ISNULL(Partner_Margin__c), 
         ListPrice, 
         (1- Partner_Margin__c ) * ListPrice
        ),
    IF (ISNULL(Partner_Margin__c),
       (1- Discount__c ) * ListPrice,
       (1- Discount__c )* (1- Partner_Margin__c ) * ListPrice 
       )
)</formula>
        <name>Calculate Net Sales Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Custom_Discount_field</fullName>
        <field>Discount__c</field>
        <formula>IF(Voucher_Code__c != null, Discount , SBQQ__QuoteLine__r.SBQQ__Discount__c)</formula>
        <name>Custom Discount field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Line_Description_Update</fullName>
        <field>Description</field>
        <formula>IF( Voucher_Code__c  != null,Description + &apos; -  &apos;+Voucher_Code__c,IF( Opportunity.RecordType.DeveloperName=&apos;Qlik_Online_Purchase&apos;,Description,SBQQ__QuoteLine__r.SBQQ__Description__c ))</formula>
        <name>Line Description Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Net_Before_MArgin</fullName>
        <field>Net_before_partner_margin_NS__c</field>
        <formula>IF(Voucher_Code__c != null, Net_Before_Partner_Margin__c ,SBQQ__QuoteLine__r.SBQQ__CustomerTotal__c)</formula>
        <name>Net Before MArgin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Prod_Partner_Margin</fullName>
        <field>Partner_Margin__c</field>
        <formula>IF(Voucher_Code__c != null, Partner_Margin__c ,SBQQ__QuoteLine__r.SBQQ__PartnerDiscount__c)</formula>
        <name>Opp Prod Partner Margin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSE_Set_IsServices_Flag_On_Opp_Products</fullName>
        <field>pse__IsServicesProductLine__c</field>
        <literalValue>1</literalValue>
        <name>PSE Set IsServices Flag On Opp Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Price</fullName>
        <description>QCW-2246: Updated logic to have correct (0) amount listed for forecasting purposes</description>
        <field>UnitPrice</field>
        <formula>/*if(SBQQ__QuoteLine__c != null,SBQQ__QuoteLine__r.SBQQ__NetPrice__c, UnitPrice)*/

if(SBQQ__QuoteLine__c != null,SBQQ__QuoteLine__r.SBQQ__NetTotal__c/Quantity, UnitPrice)</formula>
        <name>Sales Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Price</fullName>
        <field>UnitPrice</field>
        <formula>IF( 
    CPQ_List_Price__c &lt;&gt; Null 
, 
    (1-Discount__c ) * ListPrice 
, 
    (1-Discount__c ) * CPQ_List_Price__c 
)</formula>
        <name>Update Sales Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Price_from_NS</fullName>
        <field>UnitPrice</field>
        <formula>ListPrice * NS_Item_Price__c</formula>
        <name>Update Sales Price from NS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>NS%3A Update Sales Price</fullName>
        <actions>
            <name>Update_Sales_Price_from_NS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.NS_Item_Price__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used to set sales Price for NS</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Calculate Net Sales Price</fullName>
        <actions>
            <name>CalculateNetSalesPrice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.ProductCode</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>OEM - Standard Sales Process,OEM Prepay,OEM - Order Only,OEM - Run Rate</value>
        </criteriaItems>
        <description>Calculate the Net Sales price - applying discounts and referral/partner margins</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Opportunity%3A Notify if Partner Margin is different</fullName>
        <actions>
            <name>PRMOpptyNotifyOpportunityOwner</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>Partner_Margin__c &lt;&gt;  Opportunity.Account.Partner_Margin__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSE Set Opportunity Product IsServicesProductLine Flag</fullName>
        <actions>
            <name>PSE_Set_IsServices_Flag_On_Opp_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.pse__IsServicesProduct__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow will look at the product that has been added to the opportunity product line item. If the product is a services product it will check the check box on the opportunity product line item.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SB%3A Update Sales Price</fullName>
        <actions>
            <name>Custom_Discount_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Line_Description_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Net_Before_MArgin</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Prod_Partner_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to set sales Price for NS</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update List Price</fullName>
        <actions>
            <name>Update_Sales_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.CPQ_List_Price__c</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
