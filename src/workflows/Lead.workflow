<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_that_Partner_Referral_lead_has_been_sumitted</fullName>
        <description>Email that Partner Referral lead has been submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HelpDesk_emails/Partner_Referral_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Email_that_Partner_Referral_lead_has_been_sumitted_Internal</fullName>
        <description>Email that Partner Referral lead has been submitted (Internal)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>LeadSendDealRegacceptacenotificationtopartnercontact</fullName>
        <description>Lead: Send Deal Reg acceptace notification to partner contact</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/Partner_Deal_Registration_Accepted</template>
    </alerts>
    <alerts>
        <fullName>LeadSendDealRegrejectionnotificationtopartnercontact</fullName>
        <description>Lead: Send Deal Reg rejection notification to partner contact</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/Partner_Deal_Registration_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Lead_Zift_Qualified_Opportunity</fullName>
        <ccEmails>bad@qlik.com</ccEmails>
        <description>Lead: Zift Qualified Opportunity</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/Zift_Qualified_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>New_AP_application_confirmation</fullName>
        <description>New AP application confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>academicprogram@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Academic_Program/Confirmation_of_Application</template>
    </alerts>
    <alerts>
        <fullName>New_Lead_created</fullName>
        <description>New Lead created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>systems@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Academic_Program/New_Academic_Program_Lead</template>
    </alerts>
    <alerts>
        <fullName>PRMLeadNotifyLeadStatustoPartner</fullName>
        <description>PRM: Lead: Notify Lead Status to Partner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/Partner_Deal_Registration_Accepted_Rejected</template>
    </alerts>
    <alerts>
        <fullName>PRMLeadNotifyNewLeadtoARR</fullName>
        <description>PRM: Lead: Notify New Lead to ARR</description>
        <protected>false</protected>
        <recipients>
            <recipient>abd@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/New_Partner_Qualified_Lead</template>
    </alerts>
    <alerts>
        <fullName>PRMLeadNotifyNewLeadtoCCU</fullName>
        <ccEmails>djs@qlikview.com</ccEmails>
        <description>PRM: Lead: Notify New Lead to CCU</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/New_Partner_Qualified_Lead</template>
    </alerts>
    <alerts>
        <fullName>PRMLeadNotifyNewLeadtoPSU</fullName>
        <description>PRM: Lead: Notify New Lead to PSU</description>
        <protected>false</protected>
        <recipients>
            <recipient>psu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/New_Partner_Qualified_Lead</template>
    </alerts>
    <alerts>
        <fullName>PRMLeadNotifyNewLeadtoSKN</fullName>
        <description>PRM: Lead: Notify New Lead to SKN</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnv@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Portal_E_Mail_Templates/New_Partner_Qualified_Lead</template>
    </alerts>
    <alerts>
        <fullName>PRMOrbisTriggerforCountryUpdate</fullName>
        <ccEmails>downloads2@qliktech.com</ccEmails>
        <description>PRM: Orbis Trigger for Country Update</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/PRM_Orbis_Trigger_for_Country_Update</template>
    </alerts>
    <alerts>
        <fullName>PRMOrbistriggerforPartnerQualifiedstatus</fullName>
        <ccEmails>downloads2@qliktech.com</ccEmails>
        <description>PRM: Orbis trigger for Partner Qualified status</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/PRM_Partner_Qualified_Orbis_Trigger</template>
    </alerts>
    <alerts>
        <fullName>PRMPartnerQualifedmailtoPM</fullName>
        <description>PRM: Partner Qualifed mail to PM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/PRM_New_Partner_Qualified_Lead</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_ANZ</fullName>
        <description>PRM Qonnect Queue Owner - ANZ</description>
        <protected>false</protected>
        <recipients>
            <recipient>dcs@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gvnqm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Americas</fullName>
        <description>PRM Qonnect Queue Owner - Americas</description>
        <protected>false</protected>
        <recipients>
            <recipient>lca@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mft@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Benelux</fullName>
        <description>PRM Qonnect Queue Owner - Benelux</description>
        <protected>false</protected>
        <recipients>
            <recipient>gse-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~rdm-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_CE_EE</fullName>
        <description>PRM Qonnect Queue Owner - CE &amp; EE</description>
        <protected>false</protected>
        <recipients>
            <recipient>ars@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>aym@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~adz-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~nda-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~pcs-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_DACH</fullName>
        <description>PRM Qonnect Queue Owner - DACH</description>
        <protected>false</protected>
        <recipients>
            <recipient>kel-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~isr-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Denmark</fullName>
        <description>PRM Qonnect Queue Owner - Denmark</description>
        <protected>false</protected>
        <recipients>
            <recipient>~pjn-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~vhr-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Finland</fullName>
        <description>PRM Qonnect Queue Owner - Finland</description>
        <protected>false</protected>
        <recipients>
            <recipient>jko@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~hhs-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_France</fullName>
        <description>PRM Qonnect Queue Owner - France</description>
        <protected>false</protected>
        <recipients>
            <recipient>mgh@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~ago-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_GRC_CYP</fullName>
        <description>PRM Qonnect Queue Owner - GRC &amp; CYP</description>
        <protected>false</protected>
        <recipients>
            <recipient>ars@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~nda-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Iberia</fullName>
        <description>PRM Qonnect Queue Owner - Iberia</description>
        <protected>false</protected>
        <recipients>
            <recipient>psu@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~jba-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_India</fullName>
        <description>PRM Qonnect Queue Owner - India</description>
        <protected>false</protected>
        <recipients>
            <recipient>~lsa1-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~mrc-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Italy</fullName>
        <description>PRM Qonnect Queue Owner - Italy</description>
        <protected>false</protected>
        <recipients>
            <recipient>sef@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~mmb-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Japan</fullName>
        <description>PRM Qonnect Queue Owner - Japan</description>
        <protected>false</protected>
        <recipients>
            <recipient>efk@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yio@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_LATAM</fullName>
        <description>PRM Qonnect Queue Owner - LATAM</description>
        <protected>false</protected>
        <recipients>
            <recipient>jpi~retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tfaxxx@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_MEA</fullName>
        <description>PRM Qonnect Queue Owner - MEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>aym@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hhr@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~pcs-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Norway</fullName>
        <description>PRM Qonnect Queue Owner - Norway</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnv@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_RoW</fullName>
        <description>PRM Qonnect Queue Owner - RoW</description>
        <protected>false</protected>
        <recipients>
            <recipient>dar@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~usz-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_SENA</fullName>
        <description>PRM Qonnect Queue Owner - SENA</description>
        <protected>false</protected>
        <recipients>
            <recipient>efk@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lll@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_Sweden</fullName>
        <description>PRM Qonnect Queue Owner - Sweden</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnv@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Qonnect_Queue_Owner_UK</fullName>
        <description>PRM Qonnect Queue Owner - UK</description>
        <protected>false</protected>
        <recipients>
            <recipient>~lor-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~swo-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>PRM_Referral_OppReg_Rejection_Email</fullName>
        <description>PRM Referral OppReg Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Partner_Referral_Rejected</template>
    </alerts>
    <alerts>
        <fullName>PRM_Referral_lead_accepted</fullName>
        <description>PRM Referral lead accepted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Partner_Referral_Accepted</template>
    </alerts>
    <alerts>
        <fullName>PRM_Rejection_email_trigger</fullName>
        <ccEmails>downloads2@qlikview.com</ccEmails>
        <description>PRM Rejection email trigger</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/PRM_Partner_Rejected_Trigger</template>
    </alerts>
    <alerts>
        <fullName>Send_Deal_Rege_received_mail</fullName>
        <description>Send Deal Rege received mail</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/Partner_Deal_Registration_Received_English</template>
    </alerts>
    <alerts>
        <fullName>W2LPartnerdealThanksforthelead</fullName>
        <description>W2L: Partner deal Thanks for the lead</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Partner_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/Partner_Deal_Registration_Received_English</template>
    </alerts>
    <alerts>
        <fullName>W2LSendemailtoOrbistotriggerupdate</fullName>
        <ccEmails>downloads2@qliktech.com</ccEmails>
        <description>W2L: Send email to Orbis to trigger update</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Deal_Registration_Templates/W2L_Blank_To_Trigger_Orbis</template>
    </alerts>
    <fieldUpdates>
        <fullName>AP_Lead_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Academic_Program_queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>AP Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AP_Lead_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Academic_Program_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>AP Lead Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Boomi_status_update</fullName>
        <field>Boomi_Status_N3_Integration__c</field>
        <literalValue>Ready for N3</literalValue>
        <name>Boomi status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ChangeOwnertoGlobalMarketingOps</fullName>
        <field>OwnerId</field>
        <lookupValue>svc-arc@qlikview.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Change Owner to Global Marketing Ops</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Deal_Reg_Accepted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Date_Of_Count_Update</fullName>
        <field>DateOfCountUpdate__c</field>
        <name>Clear Date Of Count Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Date_Of_Email_Count_Update</fullName>
        <field>DateOfEmailCountUpdate__c</field>
        <name>Clear Date Of Email Count Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Event_Follow_Up_Required_End_Date</fullName>
        <description>this ensures that when someone ends an event follow up and then needs to restart an event follow up that the end date will clear, starting the duration over.</description>
        <field>Event_Follow_Up_Completed_Date__c</field>
        <name>Clear Event: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Existing_Follow_Up_Req_End_Date</fullName>
        <field>Existing_Follow_Up_Completed_Date__c</field>
        <name>Clear Existing: Follow-Up Req End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Follow_Up_Disqualified_Reason</fullName>
        <description>Clear Follow-Up Disqualified Reason field</description>
        <field>Follow_Up_Disqualified_Reason__c</field>
        <name>Clear Follow-Up Disqualified Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Follow_Up_Rejected_Reason</fullName>
        <description>Clear Follow-Up Rejected Reason</description>
        <field>Follow_Up_Rejected_Reason__c</field>
        <name>Clear Follow-Up Rejected Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_High_Quality_Campaign_Response</fullName>
        <field>High_Quality_Campaign_Response__c</field>
        <literalValue>0</literalValue>
        <name>Clear High Quality Campaign Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_High_Quality_Campaign_Response_Rea</fullName>
        <field>High_Quality_Campaign_Response_Reason__c</field>
        <name>Clear High Quality Campaign Response Rea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Manual_Change_Product_Category</fullName>
        <description>Clear the Manual Change Product Category field</description>
        <field>Manual_Change_Product_Category__c</field>
        <literalValue>0</literalValue>
        <name>Clear Manual Change Product Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_New_Follow_Up_Required_End_Date</fullName>
        <field>New_Follow_Up_Completed_Date__c</field>
        <name>Clear New: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_New_Responder_Follow_Up_Required</fullName>
        <field>New_Responder_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Clear New Responder Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Number_Of_Follow_Up_Emails</fullName>
        <field>Number_of_Follow_Up_Emails__c</field>
        <formula>0</formula>
        <name>Clear Number Of Follow Up Emails</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Overall_Follow_Up_Req_End_Date</fullName>
        <field>Overall_Follow_Up_Completed_Date__c</field>
        <name>Clear Overall: Follow-Up Req End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Overall_Follow_Up_Required</fullName>
        <field>Overall_Follow_up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Clear Overall Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Route_Around_to_EIS_ISR</fullName>
        <field>Route_Around_to_EIS_ISR__c</field>
        <literalValue>0</literalValue>
        <name>Clear Route Around to EIS/ISR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Trial_Follow_Up_Required</fullName>
        <field>Trial_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Clear Trial Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Reg_rejected_Record_type_change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Deal_Reg_Dependent_QT_Territories</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Deal Reg rejected Record type change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Domestic_Ultimate_DunsNumber</fullName>
        <field>Domestic_Ultimate_DUNS_Number__c</field>
        <formula>IF( 
NOT(ISBLANK(LeanData_Domestic_Ultimate_Dunsnumber__c)),LeanData_Domestic_Ultimate_Dunsnumber__c,Domestic_Ultimate_DUNS_Number__c)</formula>
        <name>Update Domestic Ultimate DunsNumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Follow_Up_Required_Start_Date</fullName>
        <field>Event_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Event: Follow-Up Required Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Existing_Follow_Up_Required_Start_Date</fullName>
        <field>Existing_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Existing: Follow-Up Required Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IV_Lead_Title_to_Job_title</fullName>
        <description>Add Standard title to custom job title</description>
        <field>Job_Title__c</field>
        <formula>Title</formula>
        <name>IV Lead Title to Job title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IV_Title_to_job_title_delete_standard</fullName>
        <description>Deletes Standard Title</description>
        <field>Title</field>
        <name>IV Title to job title delete standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Job_Title_Update</fullName>
        <field>Job_Title__c</field>
        <formula>&quot;Student&quot;</formula>
        <name>Job Title Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LeadSetLeadStatustoValidated</fullName>
        <description>Change Lead Status from Raw to Validated if Street and Industry are filled</description>
        <field>Status</field>
        <literalValue>Validated</literalValue>
        <name>Lead: Set Lead Status to Validated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Data_Validated_checked</fullName>
        <description>The check box &quot;Data Validation&quot; should be checked when 3 out of 4 fields have been completed. 
Mailing Address 
Employee Range 
Yearly Revenue 
SIC Code Name 
Applies to Standard Lead only</description>
        <field>Data_Validated__c</field>
        <literalValue>1</literalValue>
        <name>Lead: Data Validated checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Data_Validated_unchecked</fullName>
        <description>If less than 3 out of 4 (Mailing Address, Employee Range, SIC Code Name, Yearly Revenew), fields have been filled set Data Validated to False</description>
        <field>Data_Validated__c</field>
        <literalValue>0</literalValue>
        <name>Lead Data Validated unchecked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Database_Archival_Date_Reset</fullName>
        <description>Remove Date plus 6 months in Lead &quot;Database Archival Date&quot; date field.</description>
        <field>Database_Archival_Date__c</field>
        <name>Lead Database Archival Date Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Database_Archival_Date_Update</fullName>
        <description>Insert Date plus 6 months in Lead &quot;Database Archival Date&quot; date field.</description>
        <field>Database_Archival_Date__c</field>
        <formula>TODAY() + 180</formula>
        <name>Lead Database Archival Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Academic_Program_queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Academic_Program_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Duration_Change_Date</fullName>
        <description>Used to update the &quot;Lead Status Duration Change Date&quot; field with the Date that the &quot;Lead Status&quot; picklist has changed.</description>
        <field>Lead_Status_Duration_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Lead Status Duration Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_to_to_Qualified_B</fullName>
        <description>When the Primary Qualification section is filled in completely the Lead Status turns to Qualified - B</description>
        <field>Status</field>
        <literalValue>Qualified - B</literalValue>
        <name>Lead Status to to Qualified - B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Archived_Original_Owner_Name</fullName>
        <field>Archived_Original_Owner_ID__c</field>
        <formula>OwnerId</formula>
        <name>Lead Update Archived Original Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_OldRecordType</fullName>
        <field>OldRecordType__c</field>
        <formula>RecordTypeId</formula>
        <name>Lead Update OldRecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>svc-arc@qlikview.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Lead Update Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Record_Type_to_Archived</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Archived</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Update Record Type to Archived</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Reset_Workflow_Stage</fullName>
        <field>LeadSetOwnerWorkflowStage__c</field>
        <formula>0</formula>
        <name>Lead Update Reset Workflow Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Set_Workflow_Stage</fullName>
        <field>LeadSetOwnerWorkflowStage__c</field>
        <formula>1</formula>
        <name>Lead Update Set Workflow Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_set_Sent_To_Zift_date</fullName>
        <description>Sets the field &quot;Zift_Sent_To_Zift__c&quot; to todays date when picklist &quot;Zift_Distribution_Status__c&quot; is set to &quot;SentToZift&quot;</description>
        <field>Zift_Sent_To_Zift__c</field>
        <formula>NOW()</formula>
        <name>Lead: set Sent To Zift date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MobilePhone_System_Update</fullName>
        <description>Update field MobilePhone System</description>
        <field>MobilePhone_System__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(MobilePhone, &quot;(0)&quot;, &quot;&quot;), &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), &quot;.&quot;, &quot;&quot;)</formula>
        <name>MobilePhone System Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Follow_Up_Required_Start_Date</fullName>
        <field>New_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY ()</formula>
        <name>New: Follow-Up Required Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Responder_F_Up_Req_End_Date_Clear</fullName>
        <field>New_Responder_Follow_Up_Completed_Date__c</field>
        <name>New Responder: F-Up Req End Date Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Responder_F_Up_Req_End_Date_Set</fullName>
        <description>Update the New Responder: Follow-Up Completed Date field</description>
        <field>New_Responder_Follow_Up_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>New Responder: F-Up Req End Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Responder_F_Up_Req_Start_Date_Set</fullName>
        <field>New_Responder_Follow_Up_Req_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>New Responder: F-Up Req Start Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Number_of_Voicemails_0</fullName>
        <field>Number_of_Follow_Up_Voicemails__c</field>
        <formula>0</formula>
        <name>Number of Voicemails 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OEM_field_to_auto_populate</fullName>
        <description>when record type = OEM Lead then the checkbox will auto populate</description>
        <field>OEM__c</field>
        <literalValue>1</literalValue>
        <name>OEM field to auto populate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opt_In_update</fullName>
        <description>When the form is completed on wordpress they agree to opt-in to communications.</description>
        <field>Opt_In__c</field>
        <literalValue>1</literalValue>
        <name>Opt-In update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PCM_Clear</fullName>
        <description>Clear the PCM (Please Contact Me) field</description>
        <field>PCM__c</field>
        <literalValue>0</literalValue>
        <name>PCM Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRMLeadChangetoAcceptedType</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Deal_Reg_Accepted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PRM: Lead: Change to Accepted Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRMLeadPopulateQlikTechCo</fullName>
        <field>QlikTech_Company__c</field>
        <formula>CASE( $User.QlikTech_Company__c , &quot;QlikTech Benelux&quot;, &quot;QlikTech Benelux&quot;, 
&quot;QlikTech Denmark ApS&quot;, &quot;QlikTech Denmark ApS&quot;, 
&quot;QlikTech Finland OY&quot;, &quot;QlikTech Finland OY&quot;, 
&quot;QlikTech France SARL&quot;, &quot;QlikTech France SARL&quot;, 
&quot;QlikTech GmbH&quot;, &quot;QlikTech GmbH&quot;, 
&quot;QlikTech Iberia SL&quot;, &quot;QlikTech Iberia SL&quot;, 
&quot;QlikTech Inc&quot;, &quot;QlikTech Inc&quot;, 
&quot;QlikTech International AB&quot;, &quot;QlikTech International AB&quot;, 
&quot;QlikTech Nordic AB&quot;, &quot;QlikTech Nordic AB&quot;, 
&quot;QlikTech UK Ltd&quot;, &quot;QlikTech UK Ltd&quot;, &quot;QlikTech International AB&quot;)</formula>
        <name>PRM: Lead: Populate QlikTech Co</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRMSetLeadNotificationEmailStatus</fullName>
        <field>Lead_Notification_Email_Status__c</field>
        <formula>&quot;Requested&quot;</formula>
        <name>PRM: Set Lead Notification Email Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRMUpdateLeadNotificationEmailStat</fullName>
        <description>Update the Lead Notification email status to Sent to prevent the workflow being triggered again</description>
        <field>Lead_Notification_Email_Status__c</field>
        <formula>&quot;Sent&quot;</formula>
        <name>PRM: Update Lead Notification Email Stat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_Lead_Source</fullName>
        <field>LeadSource</field>
        <literalValue>PART - Partner</literalValue>
        <name>PRM Referral Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_OppReg_Rej_Never_Reason</fullName>
        <field>Unqualified_Reason__c</field>
        <literalValue>Partner Referral Opp Reg Rejected</literalValue>
        <name>PRM Referral OppReg Rej Never Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_OppReg_Rej_Set_New_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Qonnect_Referral_Opp_Reg_Rejected</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PRM Referral OppReg Rej Set New Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_OppReg_Rejctd_Status_Update</fullName>
        <field>Status</field>
        <literalValue>Lead - Never</literalValue>
        <name>PRM Referral OppReg Rejctd Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_Opp_Reg_Rejected_Date_Updat</fullName>
        <field>Referral_Opp_Reg_Rejected_Date__c</field>
        <formula>TODAY()</formula>
        <name>PRM Referral Opp Reg Rejected Date Updat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_TandC_Accepted_Date</fullName>
        <field>PRM_Referral_TandC_Accepted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PRM Referral TandC Accepted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Referral_Update_Accepted_by</fullName>
        <field>PRM_Referral_TandC_Accepted_by__c</field>
        <formula>$User.FirstName + &apos; &apos; + $User.LastName</formula>
        <name>PRM Referral Update Accepted by</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRM_Set_PartnerContactIdToUse</fullName>
        <field>PartnerContactIDToUse__c</field>
        <formula>&quot;update&quot;</formula>
        <name>PRM: Set PartnerContactIdToUse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Partner_Referral_Opp_Reg_Sourced</fullName>
        <field>Partner_Referral_Opp_Reg_Sourced__c</field>
        <literalValue>1</literalValue>
        <name>Partner Referral Opp Reg Sourced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Partner_Referral_Opp_Reg_Submit_Date</fullName>
        <field>Partner_Referral_Opp_Reg_Submit_Date__c</field>
        <formula>Today() + 0</formula>
        <name>Partner Referral Opp Reg Submit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone_System_Update</fullName>
        <description>Update field Phone System</description>
        <field>Phone_System__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Phone, &quot;(0)&quot;, &quot;&quot;), &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), &quot;.&quot;, &quot;&quot;)</formula>
        <name>Phone System Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QSB_Trial_Start_Date_Set</fullName>
        <field>QSB_Trial_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>QSB Trial Start Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_follow_up_date</fullName>
        <field>Follow_up_from__c</field>
        <name>Remove follow up date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetCountryCodetoMaptoContact</fullName>
        <field>Country_Code_Map_to_Contact__c</field>
        <formula>Country_Code__r.Name</formula>
        <name>Set Country Code to Map to Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetCountryCodetomaptoContacts</fullName>
        <field>Country_Code_Map_to_Contact__c</field>
        <formula>Country_Code__c</formula>
        <name>Set Country Code to map to Contacts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoAmericas</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegAmericas</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to Americas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoBenelux</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegBenelux</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to Benelux</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoDACH</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegDACH</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to DACH</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoDenmark</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegDenmark</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to Denmark</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoFrance</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegFrance</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoIM1</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegInternational</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to IM1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoIM2</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegInternational</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to IM2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoIberia</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegIberia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to Iberia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoNordics</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegNordics</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to Nordics</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRejectedLeadOwnertoUK</fullName>
        <field>OwnerId</field>
        <lookupValue>RejectedPartnerDealRegUK</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Rejected Lead Owner to UK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Annual_Revenue_to_Null</fullName>
        <field>AnnualRevenue</field>
        <name>Set Annual Revenue to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Anual_turnover_value_from_Annual_Rev</fullName>
        <field>Annual_Turnover__c</field>
        <formula>AnnualRevenue</formula>
        <name>Set Anual turnover value from Annual Rev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Event_Follow_Up_Required_End_Date</fullName>
        <field>Event_Follow_Up_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Event: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Existing_Follow_Up_Required_End_Date</fullName>
        <field>Existing_Follow_Up_Completed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set Existing Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_FU_Disqualified_Reason_To_No_Respons</fullName>
        <description>Set Follow-Up Disqualified Reason to No Response</description>
        <field>Follow_Up_Disqualified_Reason__c</field>
        <literalValue>No Response</literalValue>
        <name>Set FU Disqualified Reason To No Respons</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Follow_Up_Rejected_Reason_To_Junk</fullName>
        <description>Set Follow-Up Rejected Reason to Junk</description>
        <field>Follow_Up_Rejected_Reason__c</field>
        <literalValue>Junk</literalValue>
        <name>Set Follow-Up Rejected Reason To Junk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Academic_Lead_Type</fullName>
        <description>Set Lead &quot;Academic Lead Type&quot; to &quot;Academic Program Account&quot;</description>
        <field>Academic_Lead_Type__c</field>
        <formula>&quot;Academic Program Account&quot;</formula>
        <name>Set Lead &quot;Academic Lead Type&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Status_To_F_Up_Disqualified</fullName>
        <description>Set Lead Status to Follow-Up Disqualified</description>
        <field>Status</field>
        <literalValue>Follow-Up Disqualified</literalValue>
        <name>Set Lead Status To F-Up Disqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Status_To_Follow_Up_Rejected</fullName>
        <field>Status</field>
        <literalValue>Follow-Up Rejected</literalValue>
        <name>Set Lead Status To Follow-Up Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Manual_Change_Product_Category</fullName>
        <description>Set Manual Change Product Category</description>
        <field>Manual_Change_Product_Category__c</field>
        <literalValue>1</literalValue>
        <name>Set Manual Change Product Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_N3_Flag</fullName>
        <field>Send_To_N3__c</field>
        <literalValue>1</literalValue>
        <name>Set N3 Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New_Follow_Up_Hidden</fullName>
        <description>Check this box when record is created and any of the Follow Up fields are marked as TRUE.</description>
        <field>New_Follow_Up_Hidden__c</field>
        <literalValue>1</literalValue>
        <name>Set New Follow Up Hidden</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New_Follow_Up_Required_End_Date</fullName>
        <field>New_Follow_Up_Completed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set New: Follow-Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Req_Start_Date</fullName>
        <description>Set Overall: Follow-Up Req Start Date</description>
        <field>Overall_Follow_Up_Required_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Overall: Follow-Up Req Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Req_to_True</fullName>
        <description>Set Overall: Follow-Up Required to TRUE</description>
        <field>Overall_Follow_up_Required__c</field>
        <literalValue>1</literalValue>
        <name>Set Overall: Follow-Up Req to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Required_End_Date</fullName>
        <field>Overall_Follow_Up_Completed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Set Overall:Follow Up Required End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overall_Follow_Up_Required_to_False</fullName>
        <field>Overall_Follow_up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Set Overall:Follow Up Required to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_1</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;1&quot;</formula>
        <name>Set Predictive Lead Numeric Score 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_2</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;2&quot;</formula>
        <name>Set Predictive Lead Numeric Score 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_3</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;3&quot;</formula>
        <name>Set Predictive Lead Numeric Score 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_4</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;4&quot;</formula>
        <name>Set Predictive Lead Numeric Score 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Numeric_Score_5</fullName>
        <field>Predictive_Lead_Score_Numeric_Grade__c</field>
        <formula>&quot;5&quot;</formula>
        <name>Set Predictive Lead Numeric Score 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_A</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;A&quot;</formula>
        <name>Set Predictive Lead Score A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_B</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;B&quot;</formula>
        <name>Set Predictive Lead Score B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_C</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;C&quot;</formula>
        <name>Set Predictive Lead Score C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_D</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;D&quot;</formula>
        <name>Set Predictive Lead Score D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Predictive_Lead_Score_E</fullName>
        <field>Predictive_Lead_Score_Grade__c</field>
        <formula>&quot;E&quot;</formula>
        <name>Set Predictive Lead Score E</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Previous_Lead_Owner_Name</fullName>
        <field>Previous_Lead_Owner_Name__c</field>
        <formula>PRIORVALUE( Lead_Owner_name__c )</formula>
        <name>Set Previous Lead Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Switchboard_System_Update</fullName>
        <description>Switchboard System Update</description>
        <field>Switchboard_System__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(Switchboard__c, &quot;(0)&quot;, &quot;&quot;), &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), &quot;.&quot;, &quot;&quot;)</formula>
        <name>Switchboard System Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_End_Date_Clear</fullName>
        <field>Trial_Follow_Up_Completed_Date__c</field>
        <name>Trial: F-Up Req End Date Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_End_Date_Set</fullName>
        <field>Trial_Follow_Up_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Trial: F-Up Req End Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_Set</fullName>
        <field>Trial_Follow_Up_Required__c</field>
        <literalValue>1</literalValue>
        <name>Trial: F-Up Req Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_F_Up_Req_Start_Date_Set</fullName>
        <field>Trial_Follow_Up_Req_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Trial: F-Up Req Start Date Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_QSB_Trial_21_Clear</fullName>
        <field>Trial_QSB_Trial_greater_21__c</field>
        <literalValue>0</literalValue>
        <name>Trial: QSB Trial &gt; 21 Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_QSB_Trial_Clear</fullName>
        <field>QSB_Trial__c</field>
        <literalValue>0</literalValue>
        <name>Trial: QSB Trial Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateQTCompanyNameonLead</fullName>
        <field>QlikTech_Company__c</field>
        <formula>Country_Code__r.QlikTech_Company_Name__c</formula>
        <name>Update QT Company Name on Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_City</fullName>
        <field>City</field>
        <formula>IF( 
NOT(ISBLANK(LeanData_Mailing_City__c)),LeanData_Mailing_City__c,City)</formula>
        <name>Update City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Country</fullName>
        <field>Country</field>
        <formula>IF( 
NOT(ISBLANK(LeanData_Mailing_Country__c)),LeanData_Mailing_Country__c,Country)</formula>
        <name>Update Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Follow_up_Required</fullName>
        <description>Update Event: Follow-up Required to False</description>
        <field>Event_Follow_up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update Event: Follow-up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Existing_Follow_Up_Required</fullName>
        <description>Update Existing: Follow-Up Required to False</description>
        <field>Existing_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update Existing: Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LD_Segment</fullName>
        <field>LeanData_Segment__c</field>
        <formula>TEXT( Segment__c )</formula>
        <name>Update LD Segment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_Detail_Mirror</fullName>
        <field>Lead_Source_Detail_Mirror__c</field>
        <literalValue>WEB - Live Chat</literalValue>
        <name>Update Lead Source Detail Mirror</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_Detail_Original</fullName>
        <field>Lead_Source_Detail__c</field>
        <literalValue>WEB - Live Chat</literalValue>
        <name>Update Lead Source Detail Original</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_source</fullName>
        <field>LeadSource</field>
        <literalValue>WEB - Web Activity</literalValue>
        <name>Update Lead source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Follow_Up_Required</fullName>
        <description>Update New: Follow-Up Required to False</description>
        <field>New_Follow_Up_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update New: Follow-Up Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PostalCode</fullName>
        <field>PostalCode</field>
        <formula>IF( 
NOT(ISBLANK(LeanData_Mailing_Postcode__c)),LeanData_Mailing_Postcode__c,PostalCode)</formula>
        <name>Update PostalCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Route_Around_to_EIS_ISR</fullName>
        <field>Route_Around_to_EIS_ISR__c</field>
        <literalValue>1</literalValue>
        <name>Update Route Around to EIS/ISR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Standard_No_of_employees</fullName>
        <field>No_of_Employees__c</field>
        <formula>IF( 
NOT(ISBLANK(LeanData_No_of_Employees__c)),LeanData_No_of_Employees__c,No_of_Employees__c)</formula>
        <name>Update Standard No of employees</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_State</fullName>
        <field>State</field>
        <formula>IF( 
NOT(ISBLANK(LeanData_Mailing_State__c)),LeanData_Mailing_State__c,State)</formula>
        <name>Update State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Street</fullName>
        <field>Street</field>
        <formula>IF( 
NOT(ISBLANK(LeanData_Mailing_Street__c)),LeanData_Mailing_Street__c,Street)</formula>
        <name>Update Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_company_duns</fullName>
        <field>CompanyDunsNumber</field>
        <formula>IF(
NOT(ISBLANK(LeanData_CompanyDunsnumber__c)),LeanData_CompanyDunsnumber__c,CompanyDunsNumber)</formula>
        <name>Update company duns</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_Role</fullName>
        <field>Original_Created_by_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Update original created by Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_original_created_by_function</fullName>
        <field>Original_Created_by_Function__c</field>
        <formula>IF ( INCLUDES ( $User.Function__c, &quot;Sales&quot; ), &quot;Sales; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Marketing&quot; ), &quot;Marketing; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Alliance&quot; ), &quot;Alliance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;BDR&quot; ), &quot;BDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iBDR&quot; ), &quot;iBDR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ISR&quot; ), &quot;ISR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;EIS&quot; ), &quot;EIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;MIS&quot; ), &quot;MIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;SMBIS&quot; ), &quot;SMBIS; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;PRM&quot; ), &quot;PRM; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Support&quot; ), &quot;Support; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Training&quot; ), &quot;Training; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;Finance&quot; ), &quot;Finance; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;HR&quot; ), &quot;HR; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;IT&quot; ), &quot;IT; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;R&amp;D&quot; ), &quot;R&amp;D; &quot;,null ) &amp; 
IF ( INCLUDES ( $User.Function__c, &quot;iPM&quot; ), &quot;iPM; &quot;,null ) &amp;
IF ( INCLUDES ( $User.Function__c, &quot;SDR - MR&quot; ), &quot;SDR - MR; &quot;,null ) &amp;
IF ( INCLUDES ( $User.Function__c, &quot;N3&quot; ), &quot;N3; &quot;,null )&amp;
IF ( INCLUDES ( $User.Function__c, &quot;NIS&quot; ), &quot;NIS; &quot;,null )&amp; 
IF ( INCLUDES ( $User.Function__c, &quot;ServiceSource&quot; ), &quot;ServiceSource; &quot;,null )</formula>
        <name>Update original created by function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Date_reset</fullName>
        <field>Validation_Date__c</field>
        <name>Validation Date reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Date_update</fullName>
        <description>When Lead is validated (Data Validated checkbox checked) system will automatically set Validation Date to the current date</description>
        <field>Validation_Date__c</field>
        <formula>NOW()</formula>
        <name>Validation Date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>W2LRemovePartnerContactID</fullName>
        <description>Take the partnercontactIDToUse out of the Lead to prevent the trigger firing multiple times</description>
        <field>PartnerContactIDToUse__c</field>
        <name>W2L: Remove Partner ContactID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>W2LSettoMailSent</fullName>
        <description>Set the PartnerContqactIdToUse file to Mail Sent to prevent multiple triggers</description>
        <field>PartnerContactIDToUse__c</field>
        <formula>&quot;Mail Sent&quot;</formula>
        <name>W2L: Set to MailSent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Opp_Reg_Rejected_Date</fullName>
        <field>Opp_Reg_Rejected_Date__c</field>
        <formula>Today() + 0</formula>
        <name>update Opp Reg Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Partner_Opp_Reg_Date</fullName>
        <field>Partner_Opp_Reg_Submit_Date__c</field>
        <formula>Today() + 0</formula>
        <name>update Partner Opp Reg Submit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Partner_Opp_Reg_Sourced</fullName>
        <field>Partner_Opp_Reg_Sourced__c</field>
        <literalValue>1</literalValue>
        <name>update Partner Opp Reg Sourced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_QT_Originated_Contact</fullName>
        <field>QT_Originated_Contact__c</field>
        <literalValue>1</literalValue>
        <name>update QT Originated Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_QT_Originated_Date</fullName>
        <field>QT_Originated_Date__c</field>
        <formula>Today() + 0</formula>
        <name>update QT Originated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_QT_Originated_Date_Contact</fullName>
        <field>QT_Originated_Date_Contact__c</field>
        <formula>Today() + 0</formula>
        <name>update QT Originated Date Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Opp_Reg</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Record_type_to_Opp_Reg_Rejected</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Opp_Reg_rejected</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>update Record type to Opp Reg Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Academic Program Email Confirmation</fullName>
        <actions>
            <name>New_AP_application_confirmation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New_Lead_created</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opt_In_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>AP - Academic Program</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Web_Activity_Source__c</field>
            <operation>contains</operation>
            <value>Wordpress Student Application,Wordpress Professor Application</value>
        </criteriaItems>
        <description>Sends receipt confirmation of application from Wordpress</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Academic Program Web to Lead</fullName>
        <actions>
            <name>AP_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AP_Lead_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>AP - Academic Program</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>QlikTech Webservice</value>
        </criteriaItems>
        <description>To update the lead for AP compatibility</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Academic Program Web to Student Lead</fullName>
        <actions>
            <name>Job_Title_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>AP - Academic Program</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Simon Millard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Web_Activity_Source__c</field>
            <operation>equals</operation>
            <value>Wordpress Student Application</value>
        </criteriaItems>
        <description>To update the lead for AP compatibility</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Clear Follow-Up Disqualified Reason</fullName>
        <actions>
            <name>Clear_Follow_Up_Disqualified_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Status changes from Follow-Up Disqualified to something else clear the Follow-Up Disqualified Reason</description>
        <formula>AND
(
   ISCHANGED(Status),
   ISPICKVAL(PRIORVALUE(Status), &quot;Follow-Up Disqualified&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Follow-Up Rejected Reason</fullName>
        <actions>
            <name>Clear_Follow_Up_Rejected_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Status changes from Follow-Up Rejected to something else clear the Follow-Up Rejected Reason</description>
        <formula>AND
(
   ISCHANGED(Status),
   ISPICKVAL(PRIORVALUE(Status), &quot;Follow-Up Rejected&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear HQL Response</fullName>
        <actions>
            <name>Clear_High_Quality_Campaign_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_High_Quality_Campaign_Response_Rea</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Overall Follow-Up Required field is set to false, then clear High Quality Campaign Response and High Quality Campaign Response Reason fields.</description>
        <formula>AND    
(      
  ISCHANGED(Overall_Follow_up_Required__c),
  Overall_Follow_up_Required__c = FALSE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Update Route Around to EIS%2FISR</fullName>
        <actions>
            <name>Clear_High_Quality_Campaign_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_High_Quality_Campaign_Response_Rea</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Route_Around_to_EIS_ISR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Overall Follow-Up Required field is set to false, then clear Route Around EIS/ISR, High Quality Campaign Response and High Quality Campaign Response Reason fields.</description>
        <formula>OR  (    AND    (      ISCHANGED(Event_Follow_up_Required__c),      Event_Follow_up_Required__c = FALSE,      Existing_Follow_Up_Required__c = FALSE,      New_Follow_Up_Required__c = FALSE    ),    AND    (      ISCHANGED(Existing_Follow_Up_Required__c),      Event_Follow_up_Required__c = FALSE,      Existing_Follow_Up_Required__c = FALSE,      New_Follow_Up_Required__c = FALSE    ),    AND    (      ISCHANGED(New_Follow_Up_Required__c),      Event_Follow_up_Required__c = FALSE,      Existing_Follow_Up_Required__c = FALSE,      New_Follow_Up_Required__c = FALSE    ),    AND    (      ISCHANGED(Overall_Follow_up_Required__c),      Overall_Follow_up_Required__c = FALSE    )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Event%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Set_Event_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Event: Follow-Up Required is changed from True to False,  Event: Follow-Up Required End Date is set.</description>
        <formula>Event_Follow_up_Required__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_Event_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Event_Follow_Up_Required_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Event_Follow_up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Existing%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Set_Existing_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Existing: Follow-Up Required is changed from True to False, Existing: Follow-Up Required End Date is set.</description>
        <formula>Existing_Follow_Up_Required__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Existing%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_Existing_Follow_Up_Req_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Existing_Follow_Up_Required_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Existing_Follow_Up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IV - Lead Title Change</fullName>
        <actions>
            <name>IV_Lead_Title_to_Job_title</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IV_Title_to_job_title_delete_standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Title</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Inside View can only populate standard fields. This workflow takes the data from the standard field job and places it on the custom field Job title.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IV Annual Revenue to Annual Turnover</fullName>
        <actions>
            <name>Set_Annual_Revenue_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Anual_turnover_value_from_Annual_Rev</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.AnnualRevenue</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LD populate DunsNumber if empty</fullName>
        <actions>
            <name>Update_company_duns</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(LeanData_CompanyDunsnumber__c), LeanData_CompanyDunsnumber__c  &lt;&gt; &apos;&apos;, ISBLANK(CompanyDunsNumber) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LD populate fields from LD fields if not empty</fullName>
        <actions>
            <name>Domestic_Ultimate_DunsNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Standard_No_of_employees</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_company_duns</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(LeanData__Reporting_Matched_Account__c), LeanData__Reporting_Matched_Account__c&lt;&gt;&apos;&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD update Partner Referral Opp Reg Date</fullName>
        <actions>
            <name>Partner_Referral_Opp_Reg_Submit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified - B</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD update Partner Referral Opp Reg Sourced</fullName>
        <actions>
            <name>Partner_Referral_Opp_Reg_Sourced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Primary section filled in</fullName>
        <actions>
            <name>Lead_Status_to_to_Qualified_B</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.G_P_N_linked_to_person_s_job__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.G_P_N_Relevant_to_our_Solution__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Goal_Problem_Need_Description__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Is_the_person_paid_on_this__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the Primary Qualification section is filled in completely the Lead Status turns to Qualified - B</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Auto Flag Data Validated to True</fullName>
        <actions>
            <name>Lead_Data_Validated_checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The check box &quot;Data Validation&quot; should be checked when 3 out of 4 fields have been completed.
 Mailing Address 
No. of Employees 
Yearly Revenue 
SIC Code Name 
Applies to Standard Lead only</description>
        <formula>AND( RecordTypeId = &apos;01220000000DOzw&apos;,  IF( AND(ISBLANK(Street), ISBLANK(PostalCode), OR(ISBLANK(Country), ISBLANK(City))) , 0, 1) + IF( TEXT(No_of_Employees__c )== &apos;&apos; , 0, 1)  + IF( TEXT(Annual_Turnover__c) == &apos;&apos; , 0,1)  + IF( SIC_Code_Name__c == &quot;&quot; , 0,1) &gt;2  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Autoflag Data Validated to False</fullName>
        <actions>
            <name>Lead_Data_Validated_unchecked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The check box &quot;Data Validation&quot; should be unckecked when less than 3 out of 4 fields have been completed.
 Mailing Address 
No. of Employees 
Yearly Revenue 
SIC Code Name 
Applies to Standard Lead only</description>
        <formula>AND( RecordTypeId = &apos;01220000000DOzw&apos;,  IF( AND(ISBLANK(Street),ISBLANK( PostalCode ), OR(ISBLANK(Country), ISBLANK(City))) , 0, 1) + IF( TEXT( No_of_Employees__c ) == &apos;&apos; , 0,1)  + IF( TEXT(Annual_Turnover__c) == &apos;&apos; , 0,1)  + IF( SIC_Code_Name__c == &quot;&quot; , 0,1) &lt;3  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Follow Up</fullName>
        <actions>
            <name>Clear_Date_Of_Count_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Date_Of_Email_Count_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Number_Of_Follow_Up_Emails</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Number_of_Voicemails_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Lead.New_Responder_Follow_Up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Trial_Follow_Up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Overall_Follow_up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - ANZ</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_ANZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Americas</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Americas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Americas</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Benelux</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Benelux</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Benelux</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - CE %26 EE</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_CE_EE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - CE &amp; EE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - DACH</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_DACH</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - DACH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Denmark</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Denmark</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Denmark</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Finland</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Finland</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Finland</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - France</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_France</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - France</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - GRC %26 CYP</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_GRC_CYP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Greece &amp; Cyprus</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Iberia</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Iberia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Iberia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - India</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_India</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - India</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Italy</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Italy</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Italy</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Japan</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Japan</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Japan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - LATAM</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_LATAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - LATAM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - MEA</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_MEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - MEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Norway</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Norway</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Norway</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - RoW</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_RoW</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Leads - RoW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - SENA</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_SENA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - SENA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - Sweden</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_Sweden</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - Sweden</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Referral Queue Change - UK</fullName>
        <actions>
            <name>PRM_Qonnect_Queue_Owner_UK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Qonnect Referral Leads - UK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Status Duration Change Date</fullName>
        <actions>
            <name>Lead_Status_Duration_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update the &quot;Lead Status Duration Change Date&quot; field with the current date when the Lead Status changes.</description>
        <formula>OR  (      ISNEW(),      ISCHANGED(Status)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Update Lead Source</fullName>
        <actions>
            <name>Update_Lead_Source_Detail_Mirror</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Source_Detail_Original</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If WEB - Live Chat is selected the Lead Source  changes to WEB - Web Activity and Lead Source Detail is Populated with the WEB - Live Chat value.</description>
        <formula>AND(     OR (         LEFT(RecordTypeId, 15) = &apos;01220000000DOzw&apos;, /*Standard Lead RecordType*/         LEFT(RecordTypeId, 15) = &apos;012D0000000KE2F&apos;, /*Archived RecordType*/         LEFT(RecordTypeId, 15) = &apos;012D0000000JsW0&apos; /*OEM Lead RecordType*/     ),     ISPICKVAL(LeadSource, &quot;WEB - Live Chat&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Archival Date</fullName>
        <actions>
            <name>Lead_Database_Archival_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If &quot;Pending Database Archival&quot; check box is ticked, then set the &quot;Database Archival Date&quot; to date plus 6 months. All Lead record types. Visibility via page layouts.</description>
        <formula>Pending_Database_Archival__c  = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Archival Date Reset</fullName>
        <actions>
            <name>Lead_Database_Archival_Date_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If &quot;Pending Database Archival&quot; check box is un-ticked, then set the &quot;Database Archival Date&quot; to blank. All Lead record types. Visibility via page layouts.</description>
        <formula>Pending_Database_Archival__c  = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-Benelux</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoBenelux</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Netherlands,Belgium,Luxembourg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-DACH</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoDACH</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Austria,Germany,Switzerland</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-France</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoFrance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>France</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-IM1</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoIM1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>,Belarus,Bulgaria,Croatia,Czech Republic,Estonia,Hungary,Latvia,Lithuania,Poland,Romania,Russian Federation,Slovenia,Ukraine,Turkey,Israel,Algeria,Morocco,Tunisia,Angola,Bhutan,Burundi,Cameroon,Congo,Egypt,Gambia,Ivory Coast,Jordan,Namibia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-IM2</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoIM2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Senegal,Oman,Saudi Arabia,United Arab Emirates,Yemen,Bahrain,Australia,China,China-Taiwan,Hong Kong,Indonesia,Philippines,Singapore,New Zealand,Thailand,Malaysia,Taiwan,Korea,Vietnam,Macau,Japan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-Iberia</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoIberia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Portugal,Spain</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-Nordics</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoNordics</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Norway,Sweden,Finland</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Owner to Rejected Queue-UK</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoUK</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Ireland,United Kingdom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Lead Status to Validated if Address and Industry are filled</fullName>
        <actions>
            <name>LeadSetLeadStatustoValidated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Raw</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Street</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Industry_Description__c</field>
            <operation>notEqual</operation>
            <value></value>
        </criteriaItems>
        <description>Change Lead Status to Validated if Address and Industry are filled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Change Owner of %22False%22 Leads to Global Marketing Ops</fullName>
        <actions>
            <name>ChangeOwnertoGlobalMarketingOps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Lead: Change Owner of &quot;False&quot; Leads to Global Marketing Ops</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Email partner that Deal Reg ACCEPTED</fullName>
        <actions>
            <name>LeadSendDealRegacceptacenotificationtopartnercontact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <description>Email the partner contact that their deal registration was accepted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Email partner that Deal Reg REJECTED</fullName>
        <actions>
            <name>LeadSendDealRegrejectionnotificationtopartnercontact</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRM_Rejection_email_trigger</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRMSetLeadNotificationEmailStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Opp_Reg_Rejected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Original_Partner_OwnerId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email the partner contact that their deal registration was rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Email partner that WebSIte Deal Reg REJECTED</fullName>
        <actions>
            <name>LeadSendDealRegrejectionnotificationtopartnercontact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <description>Email the partner contact that their Website deal registration was rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Phone number System fields update</fullName>
        <actions>
            <name>MobilePhone_System_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Phone_System_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Switchboard_System_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow for System field 
Phone System
MobilePhone System
SwitchBoard System</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Populate original created by function and role</fullName>
        <actions>
            <name>Update_original_created_by_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_original_created_by_function</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17</booleanFilter>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>ISR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>BDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iBDR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>EIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Alliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>MIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>SMBIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>IT</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>R&amp;D</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Function__c</field>
            <operation>includes</operation>
            <value>iPM,SDR - MR,N3,ServiceSource,NIS</value>
        </criteriaItems>
        <description>Used to eliminate the need to create new user profiles when users change roles within SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Rejected Lead Owner to Denmark</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoDenmark</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Denmark,Iceland</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set Owner</fullName>
        <actions>
            <name>Lead_Update_Archived_Original_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Update_OldRecordType</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Update_Set_Workflow_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3) OR (4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>False,Lead - Never,Junk,Archived</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Follow-Up Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Follow_Up_Rejected_Reason__c</field>
            <operation>equals</operation>
            <value>Junk,Invalid Job Function,Competitor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Follow-Up Disqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Follow_Up_Disqualified_Reason__c</field>
            <operation>equals</operation>
            <value>No Fit,Left Company,Junk</value>
        </criteriaItems>
        <description>This WF is in 2 parts. This part saves the current Lead Owner and current Record Type. Then sets a numeric field to a value of 1 to trigger the second WF (Lead: Set Owner Stage 1)  which then updates the Lead Owner and Record Type with new values.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set Owner Stage 1</fullName>
        <actions>
            <name>Lead_Update_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Update_Record_Type_to_Archived</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Update_Reset_Workflow_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSetOwnerWorkflowStage__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>This WF is in 2 parts. (Lead: Set Owner is the first part). This part updates the Lead Owner and Record Type fields with new values and also sets a numeric field to a value of 0 to stop this WF repeating.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set QT Company Name</fullName>
        <actions>
            <name>UpdateQTCompanyNameonLead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set the QlikTech Company Name based on the Country of the company</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set Rejected Lead Owner to Americas</fullName>
        <actions>
            <name>SetRejectedLeadOwnertoAmericas</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Argentina,Brazil,Canada,Chile,Colombia,Dominican Republic,Mexico,Panama,Peru,United States,Venezuela</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set follow-up to False for some Status values</fullName>
        <actions>
            <name>Clear_New_Responder_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Overall_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Trial_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_QSB_Trial_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Lead: Set follow-up to False for some Status values (CR# 35709). Updated for CR# 58149. Updated for CR# 70248/82611</description>
        <formula>AND
(
  OR
  (
    AND
    (
      ISCHANGED(Status),
      OR
      (
        ISPICKVAL(Status, &quot;Follow-Up Failed&quot;),
        ISPICKVAL(Status, &quot;Follow-Up Flagged in Error&quot;),
        ISPICKVAL(Status, &quot;Lead - In Qualification&quot;),
        ISPICKVAL(Status, &quot;Lead - Not Now&quot;),
        ISPICKVAL(Status, &quot;Lead - Never&quot;),
        ISPICKVAL(Status, &quot;Junk&quot;),
        ISPICKVAL(Status, &quot;Qualified - B&quot;)
      )
    ),
    AND
    ( /* CR 70248*/
      ISPICKVAL(Status , &quot;Follow-Up Rejected&quot;),
      ISCHANGED(Follow_Up_Rejected_Reason__c),
      OR
      (
        ISPICKVAL(Follow_Up_Rejected_Reason__c , &quot;Already Working&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c , &quot;Junk&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c , &quot;Invalid Job Function&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c , &quot;Competitor&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c , &quot;Student&quot;),
        ISPICKVAL(Follow_Up_Rejected_Reason__c , &quot;Partner/Consultant&quot;)
      ),
      OR
      (
        ISPICKVAL(PRIORVALUE(Follow_Up_Rejected_Reason__c),&quot;Incorrect Routing&quot;),
        ISPICKVAL(PRIORVALUE(Follow_Up_Rejected_Reason__c),&quot;Incomplete/Inaccurate Info&quot;)
      )
    )
  ),
  OR
  (
    New_Responder_Follow_Up_Required__c = True,
    Trial_Follow_Up_Required__c = True,
    Overall_Follow_up_Required__c = True
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set follow-up to False with Certain Lead - Not Now reasons</fullName>
        <actions>
            <name>Update_Event_Follow_up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Existing_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_New_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Lead: Set follow-up to False with Certain Lead - Not Now reasons (CR# 7076)</description>
        <formula>ISCHANGED( Status ) &amp;&amp;  ispickval(Status, &quot;Lead - Not Now&quot;) &amp;&amp;  OR((Event_Follow_up_Required__c = True) ,(New_Follow_Up_Required__c = True), (Existing_Follow_Up_Required__c = True)) &amp;&amp;  OR(  ispickval(Lead_Not_Now_Reason__c, &quot;Student&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;Too Early in Buying Cycle&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;Partner&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;No/Bad Phone Number&quot;),  ispickval(Lead_Not_Now_Reason__c, &quot;Information Only&quot;),  ispickval(Unqualified_Reason__c, &quot;No e-mail and phone&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Validation Date</fullName>
        <actions>
            <name>Validation_Date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Lead is validated (Data Validated checkbox checked) system will automatically set Validation Date to the current date</description>
        <formula>AND(PRIORVALUE( Data_Validated__c ) =False, RecordTypeId = &apos;01220000000DOzw&apos;, IF( AND(ISBLANK(Street), ISBLANK(PostalCode), OR(ISBLANK(Country), ISBLANK(City))) , 0, 1) + IF( TEXT(No_of_Employees__c )== &apos;&apos; , 0, 1) + IF( TEXT(Annual_Turnover__c) == &apos;&apos; , 0,1) + IF( SIC_Code_Name__c == &quot;&quot; , 0,1) &gt;2 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Validation Date on Lead creation</fullName>
        <actions>
            <name>Validation_Date_reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>On Lead creation set Validation Date to null</description>
        <formula>Data_Validated__c = False</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Validation Date reset</fullName>
        <actions>
            <name>Validation_Date_reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Data Validated checkbox is unchecked system will automatically reset Validation Date to null</description>
        <formula>AND( RecordTypeId = &apos;01220000000DOzw&apos;, IF( AND(ISBLANK(Street),ISBLANK( PostalCode ), OR(ISBLANK(Country), ISBLANK(City))) , 0, 1) + IF( TEXT( No_of_Employees__c ) == &apos;&apos; , 0,1) + IF( TEXT(Annual_Turnover__c) == &apos;&apos; , 0,1) + IF( SIC_Code_Name__c == &quot;&quot; , 0,1) &lt;3 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Zift - send email when Qualified Opportunity</fullName>
        <actions>
            <name>Lead_Zift_Qualified_Opportunity</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Zift_Lead_Status__c</field>
            <operation>equals</operation>
            <value>Closed - Converted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Opp Reg</value>
        </criteriaItems>
        <description>When Partner working on a lead to an Opportunity, and partner sets a lead in Zift to &quot;Qualified Opportunity&quot;, then an email is sent to the Partner containing the link to the portal.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A set Sent To Zift date</fullName>
        <actions>
            <name>Lead_set_Sent_To_Zift_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Zift_Distribution_Status__c</field>
            <operation>equals</operation>
            <value>Send to Zift</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3APopulate Country Code to pass to Contacts</fullName>
        <actions>
            <name>SetCountryCodetomaptoContacts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Lead:Populate Country Code to pass to Contacts from ountry Code field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Responder%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>New_Responder_F_Up_Req_End_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When New Responder: Follow-Up Required is changed from True to False, New Responder: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED(New_Responder_Follow_Up_Required__c), New_Responder_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Responder%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>New_Responder_F_Up_Req_End_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_Responder_F_Up_Req_Start_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When New Responder: Follow-Up Required is changed from False to True, New Responder: Follow-Up Required Start Date is set.</description>
        <formula>AND( ISCHANGED(New_Responder_Follow_Up_Required__c), New_Responder_Follow_Up_Required__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Set_New_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When New: Follow-Up Required is changed from True to False, New: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED( New_Follow_Up_Required__c), New_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New%3A Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_New_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_Follow_Up_Required_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.New_Follow_Up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OEM field to auto populate when it is an OEM Lead</fullName>
        <actions>
            <name>OEM_field_to_auto_populate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Lead Record Type becomes &quot;OEM Lead&quot; then the OEM check box will auto populate - this will map to the contact record for contact tracking purposes.</description>
        <formula>($RecordType.Id ) = &quot;012D0000000JsW0&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Overall Follow-Up Required Flow End</fullName>
        <actions>
            <name>Clear_Manual_Change_Product_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PCM_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Overall_Follow_Up_Required_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When all Overall Follow-Up Required transitions to false the Overall Follow-Up Required End Date will be set.</description>
        <formula>AND
(
   ISCHANGED(Overall_Follow_up_Required__c),
   Overall_Follow_up_Required__c = FALSE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Overall Follow-Up Required Flow Start</fullName>
        <actions>
            <name>Clear_Overall_Follow_Up_Req_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Overall_Follow_Up_Req_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Overall: Follow-Up Required field is ticked the Overall Follow-Up start date will be set. and the End date cleared.</description>
        <formula>AND
(
ISCHANGED(Overall_Follow_up_Required__c),
Overall_Follow_up_Required__c = TRUE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM - LEAD QT Originated Date</fullName>
        <actions>
            <name>update_QT_Originated_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_QT_Originated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_QT_Originated_Date_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.QT_Originated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>updates the QT Originated Date with todays date when QT Originated is checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM - LEAD clone</fullName>
        <actions>
            <name>update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  ISNEW(),  Partner_Opp_Reg_Sourced__c  = True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM - LEAD update Opp Reg Rejected</fullName>
        <actions>
            <name>update_Opp_Reg_Rejected_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_Record_type_to_Opp_Reg_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Opp_Reg_Rejected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM - LEAD update Partner Opp Reg Date</fullName>
        <actions>
            <name>update_Partner_Opp_Reg_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified - B</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM - LEAD update Partner Opp Reg Sourced</fullName>
        <actions>
            <name>update_Partner_Opp_Reg_Sourced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM - Referral Rejected</fullName>
        <actions>
            <name>PRM_Referral_OppReg_Rejection_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRM_Referral_OppReg_Rej_Never_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_Referral_OppReg_Rej_Set_New_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_Referral_OppReg_Rejctd_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_Referral_Opp_Reg_Rejected_Date_Updat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Rejected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <description>Update various fields when the Partner Referral Opp Reg is rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM - Referral Submitted</fullName>
        <actions>
            <name>Email_that_Partner_Referral_lead_has_been_sumitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_that_Partner_Referral_lead_has_been_sumitted_Internal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Send_Submit_Email_For_Referral__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <description>Email the partner contact that their referral was submitted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM Referral Lead Source</fullName>
        <actions>
            <name>PRM_Referral_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM Referral Lead accepted</fullName>
        <actions>
            <name>PRM_Referral_lead_accepted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Accepted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM Referral TandC Ticked</fullName>
        <actions>
            <name>PRM_Referral_TandC_Accepted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_Referral_Update_Accepted_by</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Accepted_T_and_C__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Referral Opp Reg</value>
        </criteriaItems>
        <description>When the T&amp;C checkbox is ticked update a few fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Get Lead Owner Owners Email</fullName>
        <actions>
            <name>PRMOrbistriggerforPartnerQualifiedstatus</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRMSetLeadNotificationEmailStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_Set_PartnerContactIdToUse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Get the Email address of the Owner of the Lead Owner so the next Workflow (PRM: Send Partner Qualified email to Owner) can run</description>
        <formula>AND(     OR(           AND(             ISNEW() ,               ISPICKVAL(Status,&apos;Qualified - B&apos;)          ) ,          AND( ISCHANGED( Status ) ,            ISPICKVAL(Status,&apos;Qualified - B&apos;)         )     ),      IsBlank(ConvertedContactId)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Change to Accepted Layout</fullName>
        <actions>
            <name>PRMLeadChangetoAcceptedType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Deal_Reg_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Lead Status to Partner</fullName>
        <actions>
            <name>PRMLeadNotifyLeadStatustoPartner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg - Dependent &amp; QT Territories</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Deal Reg - Accepted,Deal Reg - Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Deal_Reg_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - ARR</fullName>
        <actions>
            <name>PRMLeadNotifyNewLeadtoARR</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Henning Wolter</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - CCU</fullName>
        <actions>
            <name>PRMLeadNotifyNewLeadtoCCU</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Nicolas MILLOT,Elodie GARNAUD,Claude-Henri Costes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - HMJ</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Roel Haverland,Ronald Donkers,Edward Pijpers,Bert Otter,Harry Stam,Robert Riedstra</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - ISR</fullName>
        <active>false</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Wiebcke Lemcke,Nicole Schmeisser,Helmut Haslbeck,Guido Mitschke,Kai Oesterling,Thomas Degenhardt,Fanny Schubert,Leon Karspeck</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Andreas Kammel</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - JVR</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Jos Hosewol,Erik Greefhorst,Edwin Quarles van Ufford,Mark Soomers,Ronald Schepers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - KND</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Lars-Erik Johansson,Henrik Borg</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - PCS</fullName>
        <active>false</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Marek Grzebyk,Bostjan Kozuh,Vladimir Rashev,Filip Mutafis,Grigory Zhukov,Andres Kukke,Tomas Trminek,Tamas Bakos,Anton Poretskov</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Tomasz Samagalski,Andres Kukke,Tamas Bakos,Anton Poretskov,Andrew Revutskiy</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - PSU</fullName>
        <actions>
            <name>PRMLeadNotifyNewLeadtoPSU</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>AMPARO CHILET LLORIS,Mario Felipe,Daniel Vázquez,José Antonio Ramos Rey,Juan Ochoa,Gonzalo Saez,José Alegre,Nacho Menzinger,Rafael Maroto Romero,Rakel Uranga</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Tamara Gonzalez Salgado,Rakel Uranga,Mateo Luis,José Manuel Férnández Macarrón</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - QZV</fullName>
        <active>false</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Andrey Krasnopolsky,Peter Travkin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Michael Korolyov</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - SKN</fullName>
        <actions>
            <name>PRMLeadNotifyNewLeadtoSKN</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Jan Griffin,Jonas Hammarberg,Daniel Göhlin,Björn-Ola Kronander,Peter Langelotz,Thomas Östensson,Robert Bladh,Eloisa Marie Westerlund,Sigvid Andersson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - SPR</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Geoffrey Wayong</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - SUL</fullName>
        <active>false</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Rob Hilferink,Muzaffer Yontem,BURCU VARLI,Ernest-Jan Horn,Willemijn Muis,Sefa Uysal,Mikel Snijders,Franklin Prosman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Dennis Lucas,Arend Jan de Jong,Cotiso Hanganu,Victor Petrache,Ionut Toader,Nicoleta Mares,Andrei Iordan,Hans Krevel</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Notify Qualified Lead to - WSN</fullName>
        <active>false</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Kim Rasmussen,Roger Adméus,Jesper Linde-Rahr,Per-Olov Jonsson,Peter Wallmark,Lars Erhardsson,Mattias Nilsson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Partner Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Mattias Roos,Andreas Wickell,Hans Eriksson</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Lead%3A Populate User QlikTech Company</fullName>
        <actions>
            <name>PRMLeadPopulateQlikTechCo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>Reseller</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Send Partner Qualified email to Owner</fullName>
        <actions>
            <name>PRMPartnerQualifedmailtoPM</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Deal_Rege_received_mail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRMUpdateLeadNotificationEmailStat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Notification_Email_Status__c</field>
            <operation>equals</operation>
            <value>Ready To Send</value>
        </criteriaItems>
        <description>Send the mail to the owner of the Lead owner (an internal QT person) to say that the Lead has gone to Partner Qualified</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Update Country TLA from Country</fullName>
        <actions>
            <name>PRMOrbisTriggerforCountryUpdate</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Deal Reg</value>
        </criteriaItems>
        <description>Takes the Country, does a look up in the Qliktech companies table and fetches back the TLA for the country, ie United Kingdom = GBR</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>QSB Trial Flow Start</fullName>
        <actions>
            <name>QSB_Trial_Start_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_QSB_Trial_21_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.QSB_Trial__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When the QSB Trial field is set we set the &quot;QSB Trial Start Date&quot; field and clear the &quot;Trial: QSB Trial greater 21&quot; field in case it has been set previously.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Remove follow up date if not Lead - not now</fullName>
        <actions>
            <name>Remove_follow_up_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Contacted-Additional Work Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Follow_up_from__c</field>
            <operation>greaterOrEqual</operation>
            <value>1/1/1970</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.CommunityNickname</field>
            <operation>notEqual</operation>
            <value>qtweb</value>
        </criteriaItems>
        <description>Remove follow up date if not Contacted-Additional Work Required</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Same Day Clear Of FollowUp</fullName>
        <actions>
            <name>Update_Event_Follow_up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Existing_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_New_Follow_Up_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(     ISPICKVAL(Status, &apos;Lead - In Qualification&apos;),     Same_Day_Clear_Of_Follow_Up_Flags__c = TODAY()    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Academic Lead Type</fullName>
        <actions>
            <name>Set_Lead_Academic_Lead_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Academic Program Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Academic_Lead_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set the &quot;Set Academic Lead Type&quot; field to the value &quot;Academic Program Account&quot; for all leads that are of the Academic Lead Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set FU Disqualified for Lattice Scoring</fullName>
        <actions>
            <name>Set_FU_Disqualified_Reason_To_No_Respons</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Lead_Status_To_F_Up_Disqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>19 AND ((1 AND 2 AND 3) OR (4 AND 5 AND 6) OR (7 AND 8 AND 9) OR (10 AND 11 AND 12) OR (13 AND 14 AND 15) OR (16 AND 17 AND 18))</booleanFilter>
        <criteriaItems>
            <field>Lead.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>A</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>6</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>B</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>C</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>D</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Predictive_Lead_Score_Grade__c</field>
            <operation>equals</operation>
            <value>E</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Voicemails__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_of_Follow_Up_Emails__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Follow-Up Attempt 1,Follow-Up Attempt 2,Follow-Up Attempt 3,Follow-Up Attempt 4,Follow-Up Attempt 5</value>
        </criteriaItems>
        <description>Set FU Disqualified for Lattice Scoring</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Manual Change Product Category</fullName>
        <actions>
            <name>Set_Manual_Change_Product_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Manual Change Product Category field</description>
        <formula>AND (    ISPICKVAL(PRIORVALUE(Product_Category__c), &apos;Cross Product&apos;),    ISPICKVAL(Product_Category__c, &apos;Data Integration&apos;),    OR(New_Responder_Follow_Up_Required__c, Trial_Follow_Up_Required__c, Overall_Follow_up_Required__c),    NOT(OR($User.FirstName = &apos;Integration&apos; &amp;&amp; $User.LastName = &apos;User&apos;, $User.FirstName = &apos;Marketo&apos; &amp;&amp; $User.LastName = &apos;@Qlikview&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set N3 Flag for sync from SFDC to N3</fullName>
        <actions>
            <name>Boomi_status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_N3_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>N3 API user</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Follow-Up Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Overall_Follow_up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This flag will be used by Boomi to pick the leads and sync them to N3.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Predictive Lead Score A</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the A values for the Predictive Lead Score Grade</description>
        <formula>AND   (       OR       (     ISCHANGED(contactGrade6sense__c),     ISCHANGED(contactProfileFit6sense__c),     ISCHANGED(accountBuyingStage6sense__c),     ISCHANGED(contactScoreDate6sense__c)       ),   OR   (     AND     (       OR       (         contactGrade6sense__c = &apos;A&apos;,         contactGrade6sense__c = &apos;B&apos;       ),       AND       (         contactProfileFit6sense__c = &apos;Strong&apos;,         OR         (           accountBuyingStage6sense__c = &apos;Decision&apos;,           accountBuyingStage6sense__c = &apos;Purchase&apos;         )       )     ),     AND     (       contactGrade6sense__c = &apos;C&apos;,       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (             accountBuyingStage6sense__c = &apos;Target&apos;,             accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;,             accountBuyingStage6sense__c = &apos;Decision&apos;           )         ),         AND         (           accountBuyingStage6sense__c = &apos;Target&apos;,           OR           (             contactProfileFit6sense__c = &apos;Moderate&apos;,             contactProfileFit6sense__c = &apos;Weak&apos;           )         )       )     )   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Predictive Lead Score B</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_B</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the B values for the Predictive Lead Score Grade</description>
        <formula>AND   (       OR   (     ISCHANGED(contactGrade6sense__c),     ISCHANGED(contactProfileFit6sense__c),     ISCHANGED(accountBuyingStage6sense__c),     ISCHANGED(contactScoreDate6sense__c)   ),   OR   (     AND      (       OR       (         contactGrade6sense__c = &apos;A&apos;,         contactGrade6sense__c = &apos;B&apos;       ),       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (              accountBuyingStage6sense__c = &apos;Target&apos;,             accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;           )         ),               AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           OR           (              accountBuyingStage6sense__c = &apos;Decision&apos;,             accountBuyingStage6sense__c = &apos;Purchase&apos;           )         )       )     ),     AND      (       contactGrade6sense__c = &apos;C&apos;,             OR       (         AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           OR           (              accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;,             accountBuyingStage6sense__c = &apos;Decision&apos;           )         ),               AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           OR           (              accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;           )         )       )     ),     AND      (       OR       (         contactGrade6sense__c = &apos;D&apos;,         ISBLANK(contactGrade6sense__c)        ),       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (              accountBuyingStage6sense__c = &apos;Target&apos;,             accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;           )         ),               AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           accountBuyingStage6sense__c = &apos;Target&apos;         )       )     )   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Predictive Lead Score C</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_C</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the C values for the Predictive Lead Score Grade</description>
        <formula>AND  (    OR    (      ISCHANGED(contactGrade6sense__c),     ISCHANGED(contactProfileFit6sense__c),     ISCHANGED(accountBuyingStage6sense__c),     ISCHANGED(contactScoreDate6sense__c)   ),   OR    (      AND      (        OR        (          contactGrade6sense__c = &apos;A&apos;,          contactGrade6sense__c = &apos;B&apos;       ),       OR       (         AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           OR           (             accountBuyingStage6sense__c = &apos;Target&apos;,             accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;,             accountBuyingStage6sense__c = &apos;Decision&apos;,             accountBuyingStage6sense__c = &apos;Purchase&apos;           )         ),         AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           OR           (             accountBuyingStage6sense__c = &apos;Target&apos;,             accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;           )         )       )     ),     AND     (        contactGrade6sense__c = &apos;C&apos;,       OR       (         AND         (           contactProfileFit6sense__c = &apos;Strong&apos;,           accountBuyingStage6sense__c = &apos;Purchase&apos;         ),         AND         (           contactProfileFit6sense__c = &apos;Weak&apos;,           accountBuyingStage6sense__c = &apos;Decision&apos;         )       )     ),      AND      (        OR        (          contactGrade6sense__c = &apos;D&apos;,          ISBLANK(contactGrade6sense__c)        ),        OR       (         AND          (            contactProfileFit6sense__c = &apos;Strong&apos;,           OR           (             accountBuyingStage6sense__c = &apos;Decision&apos;,             accountBuyingStage6sense__c = &apos;Purchase&apos;           )         ),           AND          (            contactProfileFit6sense__c = &apos;Moderate&apos;,           OR           (             accountBuyingStage6sense__c = &apos;Awareness&apos;,             accountBuyingStage6sense__c = &apos;Consideration&apos;,             accountBuyingStage6sense__c = &apos;Decision&apos;           )         ),          AND          (            contactProfileFit6sense__c = &apos;Weak&apos;,            accountBuyingStage6sense__c = &apos;Awareness&apos;          )        )      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Predictive Lead Score D</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_D</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the D values for the Predictive Lead Score Grade</description>
        <formula>AND  (    OR    (      ISCHANGED(contactGrade6sense__c),      ISCHANGED(contactProfileFit6sense__c),      ISCHANGED(accountBuyingStage6sense__c),      ISCHANGED(contactScoreDate6sense__c)    ),   OR    (      AND      (        contactGrade6sense__c = &apos;C&apos;,        contactProfileFit6sense__c = &apos;Weak&apos;,       accountBuyingStage6sense__c = &apos;Purchase&apos;     ),      AND      (        OR        (          contactGrade6sense__c = &apos;D&apos;,         ISBLANK(contactGrade6sense__c)       ),        OR        (          AND         (           contactProfileFit6sense__c = &apos;Moderate&apos;,           accountBuyingStage6sense__c = &apos;Purchase&apos;         ),         AND          (            contactProfileFit6sense__c = &apos;Weak&apos;,           OR            (              accountBuyingStage6sense__c = &apos;Decision&apos;,              accountBuyingStage6sense__c = &apos;Purchase&apos;            )          )        )      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Predictive Lead Score E</fullName>
        <actions>
            <name>Set_Predictive_Lead_Numeric_Score_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Predictive_Lead_Score_E</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Using values in the 6Sense fields set the E values for the Predictive Lead Score Grade</description>
        <formula>AND  (    OR    (      ISCHANGED(contactGrade6sense__c),      ISCHANGED(contactProfileFit6sense__c),      ISCHANGED(accountBuyingStage6sense__c),      ISCHANGED(contactScoreDate6sense__c)    ),    OR    (     AND     (        contactGrade6sense__c = &apos;C&apos;,       contactProfileFit6sense__c = &apos;Moderate&apos;,       accountBuyingStage6sense__c = &apos;Purchase&apos;     ),      AND     (        OR        (          contactGrade6sense__c = &apos;D&apos;,         ISBLANK(contactGrade6sense__c)       ),       contactProfileFit6sense__c = &apos;Weak&apos;,       OR        (          accountBuyingStage6sense__c = &apos;Target&apos;,          accountBuyingStage6sense__c = &apos;Consideration&apos;        )      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Previous Lead Owner Name</fullName>
        <actions>
            <name>Set_Previous_Lead_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Trial Set Trial Follow-Up Required</fullName>
        <actions>
            <name>Trial_F_Up_Req_End_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_F_Up_Req_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_F_Up_Req_Start_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.QSB_Trial__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Overall_Follow_up_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets the Trial F-Up Required field if QSB Trial and Overall Follow-Up Required is set</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Trial%3A Follow-Up Required Flow End</fullName>
        <actions>
            <name>Trial_F_Up_Req_End_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trial_QSB_Trial_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Trial: Follow-Up Required is changed from True to False, Trial: Follow-Up Required End Date is set.</description>
        <formula>AND( ISCHANGED(Trial_Follow_Up_Required__c), Trial_Follow_Up_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Route Around to EIS%2FISR</fullName>
        <actions>
            <name>Update_Route_Around_to_EIS_ISR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.High_Quality_Campaign_Response__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If High Quality Campaign Response field is set to True, then set EIS/ISR Route Around to true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status If Set To Junk</fullName>
        <actions>
            <name>Set_Follow_Up_Rejected_Reason_To_Junk</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Lead_Status_To_Follow_Up_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If previous Lead Status was Follow-Up Required or Follow-Up Attempt 1,2 or 3 and Lead Status is updated to Junk, then update status to follow-up rejected with a follow-up rejected reason of Junk.</description>
        <formula>AND (     ISCHANGED(Status),     OR     (         ISPICKVAL(PRIORVALUE(Status), &quot;Follow-Up Required&quot;),         ISPICKVAL(PRIORVALUE(Status), &quot;Follow-Up Attempt 1&quot;),         ISPICKVAL(PRIORVALUE(Status), &quot;Follow-Up Attempt 2&quot;),         ISPICKVAL(PRIORVALUE(Status), &quot;Follow-Up Attempt 3&quot;)     ),     ISPICKVAL(Status, &quot;Junk&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>W2L%3A Send out %27Received%27 Mails</fullName>
        <actions>
            <name>W2LPartnerdealThanksforthelead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>W2LSettoMailSent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends out the mails to say the lead has been received</description>
        <formula>OR( AND( ISNEW(), PartnerContactIDToUse__c = &apos;Send Mail&apos;) , AND(  ISCHANGED( PartnerContactIDToUse__c )  ,  PartnerContactIDToUse__c = &apos;Send Mail&apos;  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>W2L%3A Update Partner Contact ID</fullName>
        <actions>
            <name>W2LSendemailtoOrbistotriggerupdate</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.PartnerContactIDToUse__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.PartnerContactIDToUse__c</field>
            <operation>notContain</operation>
            <value>Mail</value>
        </criteriaItems>
        <description>Send email to Orbis to call webservice to update the value in Partner Contact ID</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
