<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sales_Ops_License_Transfer_Request_Email</fullName>
        <description>Sales Ops License Transfer Request Email</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Ops_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>License_Transfer_Requests/Sales_Ops_Email_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Status_Under_Review</fullName>
        <field>Transfer_Status__c</field>
        <name>Status Under Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
