<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-KnowledgePublishing</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Knowledge Management</label>
    <tabs>standard-File</tabs>
    <tabs>standard-KnowledgePublishing</tabs>
    <tabs>standard-Knowledge</tabs>
    <tabs>standard-Case</tabs>
    <tabs>Forecasting_App</tabs>
    <tabs>Marketing_Asset_NEW__c</tabs>
    <tabs>Sales_Tools</tabs>
    <tabs>Quote_Approvals</tabs>
    <tabs>Support_Survey_Task__c</tabs>
    <tabs>Address__c</tabs>
</CustomApplication>
